﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace ProductionMonitor
{
	public class Log
	{
		private DataTable dtb_log;
		private int lenth = 10000;
		private string name;
		private int days_to_store = 10;
		private bool isforced = false;
		private DateTime _startrun;
		private Stopwatch _sw;
		private StringBuilder _sblog;

		public Log(string Name)
			: this(Name, 10000) { }
		public Log(string Name, int Lenth)
		{
			name = Name;
			lenth = Lenth;
			// 5000000 - это 5 Mb, если размер лога превышает этот размер то происходит очистка
			// с предварительной проверкой на сохранение
			_sblog = new StringBuilder(100000, 5000000);

			dtb_log = new DataTable("Log");
			dtb_log.Columns.Add("Date", typeof(string));
			dtb_log.Columns.Add("Log", typeof(string));

			_startrun = DateTime.Now;
			_sw = Stopwatch.StartNew();
		}

		private void CheckLenth(string logentry)
		{
			if (dtb_log.Rows.Count > lenth)
				Clear();
			else if (_sblog.Length + logentry.Length >= _sblog.MaxCapacity)
				Clear();
		}

		public DateTime AddEntry(string logentry)
		{
			CheckLenth(logentry);

			DateTime dt = _startrun.Add(_sw.Elapsed);
			DataRow dr = dtb_log.NewRow();
			dr[0] = string.Format("{0:G}.{1:000}", dt, dt.Millisecond);
			dr[1] = logentry;
			dtb_log.Rows.Add(dr);

			_sblog.AppendLine(String.Format("{0,-20} {1}", dr[0], logentry));

			return dt;
		}

		public string Save(string HeaderEntry, bool NeedToShow)
		{
			string result = "";
			
			string fileName = name + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
			string fullName = Directory.GetParent(Application.UserAppDataPath).FullName + "\\" + fileName;

			StringBuilder sb = new StringBuilder();
			string strT = new string('_', 20);
			string strTT = new string('=', 20);

			sb.AppendLine("IF ANY PROBLEM THEN PLEASE SEND THIS REPORT TO SUPPORT.");
			sb.AppendLine(Environment.NewLine);

			sb.AppendLine(strT + " ENVIRONMENT ");
			sb.AppendLine(String.Format("{0,-20} {1}", "OSVersion", Environment.OSVersion));
			sb.AppendLine(String.Format("{0,-20} {1}", "UserDomainName", Environment.UserDomainName));
			sb.AppendLine(String.Format("{0,-20} {1}", "MachineName", Environment.MachineName));
			sb.AppendLine(String.Format("{0,-20} {1}", "Framework", Environment.Version));
			sb.AppendLine(String.Format("{0,-20} {1}", "System started", Environment.TickCount / 60000));
			sb.AppendLine(String.Format("{0,-20} {1}", "Network is present", SystemInformation.Network.ToString()));
			sb.AppendLine(Environment.NewLine);

			sb.AppendLine(strT + " STATE ");
			sb.AppendLine(String.Format("{0,-20} {1}", "Program Version", Application.ProductVersion));
			sb.AppendLine(String.Format("{0,-20} {1}", "Title", Application.ProductName));
			sb.AppendLine(String.Format("{0,-20} {1}", "CurrentDirectory", Environment.CurrentDirectory));
			sb.AppendLine(String.Format("{0,-20} {1}", "PC_Time", DateTime.Now.ToString()));
			sb.AppendLine(Environment.NewLine);

			sb.Append(HeaderEntry);

			sb.AppendLine(Environment.NewLine);

			sb.AppendLine(strT + " Log table: \r\n");

			// копировать, потому что во время последующей операции сохранения может быть вызван метод добавляющий новую строку в лог
			////DataTable dtb = dtb_log.Copy();

			////foreach (DataRow dr in dtb.Rows)
			////   sb.AppendLine(String.Format("{0,-20} {1}", dr[0], dr[1]));
			sb.Append(_sblog.ToString());

			using (StreamWriter sw = File.CreateText(fullName)) {
				try {
					sw.Write(sb.ToString());
					if (NeedToShow)
						Process.Start(fullName);
				} catch (Exception ex){
					result = "The previous log was not saved: " + ex.Message;
				} finally {
					sw.Close();
					result = "The previous log was saved to path: " + fullName;
				}
			}
			ClearOldLogs();
			return result;
		}
		public void Clear()
		{
			string wassaved = "";
			if (isforced)
				wassaved = Save("", false);

			dtb_log.Clear();
			_sblog.Remove(0, _sblog.Length);

			if (wassaved != "")
				AddEntry(wassaved);
		}
		public void Close()
		{
			if (isforced)
				Save("", false);
		}
		public void ClearOldLogs()
		{
			try {
				string pattern = Directory.GetParent(Application.UserAppDataPath).FullName;
				foreach (string logname in Directory.GetFiles(pattern, name + "*")) {
					FileInfo fi = new FileInfo(logname);
					if (fi.Extension == ".txt" && DateTime.Now.Subtract(fi.CreationTime).Days > days_to_store) {
						fi.Delete();
						Debug.WriteLine("Deleted: " + logname);
					}
				}
			} catch { }
		}

		public int Lenth
		{
			get { return lenth; }
			set { lenth = value; }
		}
		public bool IsForced
		{
			get { return isforced; }
			set { isforced = value; }
		}
		public DataTable LogTable
		{
			get { return dtb_log; }
		}
	}

	public class SuccessLog
	{
		#region | declare       |

		private int lenth = 3;
		private Queue<bool> results;
		private Queue<bool> last3results = new Queue<bool>(3);
		private bool last_success = false;
		private bool thereiscuccess = true;
		private bool thereisfault = false;
		private int threshold = 2;
		private int resultscount = 0;
		private int successcount = 0;
		private bool ok = true;
		private long totalsuccess = 0;
		private long totalattempts = 0;

		#endregion

		/// <summary>
		/// По умолчанию создается очередь из трех попыток и допустима одна ошибка
		/// </summary>
		public SuccessLog()
		{
			lenth = 3;
			threshold = 2;
			results = new Queue<bool>(lenth);
			last3results.Enqueue(true);

		}
		/// <summary>
		/// Порог вычисляется по длине очереди /2 с округлением в большую сторону
		/// </summary>
		/// <param name="Lenth">Минимальная длина очереди = 3</param>
		public SuccessLog(int Lenth)
		{
			lenth = Lenth < 3 ? 3 : Lenth;
			threshold = (int)Math.Ceiling((float)lenth / 2F);
			results = new Queue<bool>(lenth);
			last3results.Enqueue(true);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Lenth">Количество отслеживаемых событий</param>
		/// <param name="Threshold">Количество успешных событий, ниже которого устанавливается статус Ok=false</param>
		public SuccessLog(int Lenth, int Threshold)
		{
			if (Lenth < 3)
				lenth = 3;

			if (Threshold < 1)
				threshold = 1;
			else if (Threshold > lenth)
				threshold = lenth;
			else
				threshold = Threshold;

			results = new Queue<bool>(lenth);
		}

		/// <summary>
		/// Добавление успешной операции в очередь
		/// </summary>
		public void AddSuccess()
		{
			AddResult(true);
		}
		/// <summary>
		/// Добавление ошибки в очередь
		/// </summary>
		public void AddFault()
		{
			AddResult(false);
		}
		/// <summary>
		/// добавление нового результата в очередь, при этом самый старый результат выталкивается из очереди 
		/// </summary>
		/// <param name="Result"></param>
		public void AddResult(bool Result)
		{
			last_success = Result;
			if (results.Count >= lenth)
				results.Dequeue();
			results.Enqueue(Result);

			if (last3results.Count >= 3)
				last3results.Dequeue();
			last3results.Enqueue(Result);

			totalattempts++;
			if (last_success) totalsuccess++;

			thereiscuccess = results.Contains(true);
			thereisfault = results.Contains(false);

			successcount = 0;
			foreach (bool b in results.ToArray())
				if (b)
					successcount++;

			float realthreshold = threshold;
			if (results.Count < lenth)
				realthreshold = threshold * ((float)results.Count / lenth);

			ok = successcount >= realthreshold || last_success;
		}
		public void Clear()
		{
			results.Clear();
		}

		public bool LastResult
		{
			get { return last_success; }
		}
		/// <summary>
		/// Есть как минимум один успешный результат
		/// </summary>
		public bool ThereIsSuccess
		{
			get { return thereiscuccess; }
		}
		/// <summary>
		/// Есть как минимум одна ошибка
		/// </summary>
		public bool ThereIsFault
		{
			get { return thereisfault; }
		}
		/// <summary>
		/// Порог ошибок не превышен или последняя попытка была успешной
		/// </summary>
		public bool Ok
		{
			get { return ok; }
		}
		/// <summary>
		/// Количество успешных попыток в очереди
		/// </summary>
		public int SuccessCount
		{
			get { return successcount; }
		}
		/// <summary>
		/// Состояние в процентах, определяется отношением успешных попыток к общему числу попыток
		/// </summary>
		public float Health
		{
			get { return (float)successcount * 100 / results.Count; }
		}
		/// <summary>
		/// Учитываются все успешные попытки за время жизни объекта
		/// </summary>
		public int TotalHealth
		{
			get { return (int)((double)totalsuccess * 100 / totalattempts); }
		}
		/// <summary>
		/// Очередь заполнена
		/// </summary>
		public bool FullQueue
		{
			get { return results.Count == lenth; }
		}
		/// <summary>
		/// Дела совсем плохи: очередь полная и в ней ни одной успешной попытки
		/// </summary>
		public bool IsDead
		{
			get
			{
				return results.Count == lenth &&		// очередь полная
					!thereiscuccess;						// в очереди ни одной успешной попытки 
			}
		}
		public int Lenth
		{
			get { return lenth; }
		}
		/// <summary>
		/// Из трех последних результатов все безуспешные
		/// </summary>
		public bool LastThreeIsFault
		{
			get { return !last3results.Contains(true); }
		}
	}
}

