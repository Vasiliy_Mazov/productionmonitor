﻿using System.Diagnostics;
using System;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Collections.Generic;

namespace ProductionMonitor
{

	public class ConfigCounters
	{
		#region | fields                |

		protected DataSet dsconfig;
		protected TreeNodeCollection treenodes;
		protected List<UnitCounters> roots = new List<UnitCounters>();

		#endregion

		public ConfigCounters()
		{
			string FileSiteLine = "pm_.cfg";
			string strPathFile = Application.StartupPath + "\\" + FileSiteLine;

			////// если доступ пользователя к папке программфайлс закрыт, то копируем в локальные папку пользователя
			////string newPlace = Application.LocalUserAppDataPath + "\\" + FileSiteLine;
			////File.Copy(strPathFile, newPlace, true);

			// loading configuration from encrypted  siteline.dat
			try {
				byte[] V = new byte[] { 240, 179, 254, 93, 132, 104, 83, 205 };
				byte[] K = new byte[] { 216, 30, 31, 1, 225, 95, 3, 134 };

				using (SymmetricAlgorithm Alg = new DESCryptoServiceProvider()) {
					ICryptoTransform Cryptor = Alg.CreateDecryptor(K, V);
					using (FileStream fs = new FileStream(strPathFile, FileMode.Open)) {
						using (CryptoStream cs = new CryptoStream(fs, Cryptor, CryptoStreamMode.Read)) {
							using (BinaryReader br = new BinaryReader(cs)) {
								UTF8Encoding enc = new UTF8Encoding();
								string xmlStr = enc.GetString(br.ReadBytes(Convert.ToInt32(fs.Length)));
								using (StringReader sr = new StringReader(xmlStr)) {
									dsconfig = new DataSet();
									dsconfig.ReadXml(sr, XmlReadMode.ReadSchema);
								}
							}
						}
					}
				}
			} catch (Exception ex) {
				throw (new Exception("The configuration file is corrupted or missing : " + strPathFile + "\r\n\r\n" + ex.Message));
			} 
			//dsconfig.Relations.Add("rel", 
			treenodes = null;
			Initialize();
		}
		public ConfigCounters(DataSet dataset, TreeNodeCollection tn)
		{
			// процедура испульзуется только в конфигураторе (Production Monitor Config)
			dsconfig = dataset;
			treenodes = tn;
			Initialize();
			Debug.WriteLine(BaseUnitConnection);
		}
		
		public void LoadTree(TreeNodeCollection tnc, int id)
		{
			if (tnc == null) return;
			TreeNode tn;
			foreach (UnitCounters un in Units.Values) {
				if (un.ParentID == id) {
					tn = new TreeNode(un.LineName);
					tn.Tag = un;
					tnc.Add(tn);
					LoadTree(tn.Nodes, un.ID);
				}
			}
		}
		public void LoadEventCounters()
		{

		}

		#region | private functions     |
		private void Initialize()
		{
			Connections = new Dictionary<string, string>();
			Units = new Dictionary<int, UnitCounters>();
			LoadUnits();
			if (treenodes != null) {
				treenodes.Clear();	// если есть узлы, значит это вьюер или конфигуратор
				LoadTree(treenodes, 0);
			}
		}
		private void LoadUnits()
		{
			UnitCounters unit;
			string srv, conn = "";
			int id, parentid = 0;
			DataRow r;

			// перебираем все юниты
            if (!dsconfig.Tables["Unit"].Columns.Contains("sort"))
            {
                dsconfig.Tables["Unit"].Columns.Add("sort", typeof(int));
            }

			foreach (DataRow rr in dsconfig.Tables["Unit"].Select("","sort", DataViewRowState.CurrentRows)) {
				//if 
				id = Convert.ToInt32(rr["ID"]);
				parentid = rr["PID"] is DBNull ? 0 : Convert.ToInt32(rr["PID"]);
				r = rr.GetParentRow("ServLine");
				if (r != null) {
#if DEBUG
					srv = "localhost";
					conn = "integrated security=SSPI;data source=localhost";
#else
					srv = r["Serv"].ToString();
					conn = r["Conn"].ToString();
#endif
					if (!Connections.ContainsKey(srv))
						Connections.Add(srv, conn);
				}
				if (r == null) {
					// ссылки на сервер нет, значит это простой узел без собственных данных
					unit = new UnitCounters(id, parentid, rr["Name"].ToString(), "");
				} else if (rr["db"] == DBNull.Value || rr["db"].ToString() == "") {
					// ссылка на сервер есть, но нет имени базы, значит это вариант "одна база - много линий"
					conn = conn + ";Initial catalog=" + C.DB;
					unit = new UnitCounters(id, parentid, rr["Name"].ToString(), conn);
				} else {
					// есть имя базы, значит это вариант "одна база - одна линия"
					conn = conn + ";Initial catalog=" + rr["db"].ToString();
					unit = new UnitCounters(id, parentid, rr["Name"].ToString(), conn);
				}
				// Для уменьшения размера конфига все булевые поля со значением NULL преобразовываются в TRUE
				// расставляем предварительные значения, 
				// 0 - если использовать свои таблицы, 
				// -1 - будет определено в SetParentsData()
				if (rr["gkpi"] != DBNull.Value) unit.ParentIdKpi    = !(bool)rr["gkpi"] ? 0 : -1;
				if (rr["gdwn"] != DBNull.Value) unit.ParentIdStops  = !(bool)rr["gdwn"] ? 0 : -1;
				if (rr["gcfg"] != DBNull.Value) unit.ParentIdConfig = !(bool)rr["gcfg"] ? 0 : -1;
				if (rr["gsku"] != DBNull.Value) unit.ParentIdSku    = !(bool)rr["gsku"] ? 0 : -1;
				if (rr["gshf"] != DBNull.Value) unit.ParentIdShifts = !(bool)rr["gshf"] ? 0 : -1;
				if (rr["gstore"] != DBNull.Value)
					unit.GlobalStore = (int)rr["gstore"];
				else
					unit.GlobalStore = -1;

				if (rr["PID"] is DBNull)
					RootNode = id;

				Units.Add(id, unit);
			}

			List<UnitCounters> orphans  = new List<UnitCounters>();
			// определяем родителей
			foreach (UnitCounters u in Units.Values) {
				if (u.ParentID > 0)
					u.Parent = Units[u.ParentID];	// 
				else if (u.ParentID == 0)
					roots.Add(u);						// корневые узлы
				else if (C.TestEnvironment)
					u.Parent = Units[u.ParentID];	// отрицательные - это тестовые
				else
					orphans.Add(u);
			}
			
			foreach (UnitCounters u in orphans)
				Units.Remove(u.ID);

			// заполняем детей
			foreach (UnitCounters u in Units.Values)
				if (u.ParentID != 0 && u.Parent != null)
					u.Parent.Childs.Add(u);

			// заполняем корневые юниты, можно вызывать только после заполнения детей
			foreach (UnitCounters u in roots) {
				u.Level = 0;
				SetParentsData(u);
			}

		}
		private void SetParentsData(UnitCounters u)
		{
			if (u.Parent != null)
				u.Level = u.Parent.Level + 1;
			// рекурсия идет от верхнего узла к нижнему, поэтому значения родителей всегда известны
			// u.ParentIdxxx = 0; означает 
			// 
			if (u.ParentIdKpi == 0 && u.GlobalStore == -1) u.ParentIdKpi = u.ID;		// берем свои таблицы
			else if (u.ParentIdKpi == 0) u.ParentIdKpi = u.GlobalStore;
			else if (u.ParentID != 0) u.ParentIdKpi = u.Parent.ParentIdKpi;
			else {
				u.ParentIdKpi = u.ID;

			}
			if (u.ParentIdConfig == 0 && u.GlobalStore == -1) u.ParentIdConfig = u.ID;
			else if (u.ParentIdConfig == 0) u.ParentIdConfig = u.GlobalStore;
			else if (u.ParentID != 0) u.ParentIdConfig = u.Parent.ParentIdConfig;
			else u.ParentIdConfig = u.ID;

			if (u.ParentIdStops == 0 && u.GlobalStore == -1) u.ParentIdStops = u.ID;
			else if (u.ParentIdStops == 0) u.ParentIdStops = u.GlobalStore;
			else if (u.ParentID != 0) u.ParentIdStops = u.Parent.ParentIdStops;
			else u.ParentIdStops = u.ID;

			if (u.ParentIdSku == 0 && u.GlobalStore == -1) u.ParentIdSku = u.ID;
			else if (u.ParentIdSku == 0) u.ParentIdSku = u.GlobalStore;
			else if (u.ParentID != 0) u.ParentIdSku = u.Parent.ParentIdSku;
			else u.ParentIdSku = u.ID;

			if (u.ParentIdShifts == 0 && u.GlobalStore == -1) u.ParentIdShifts = u.ID;
			else if (u.ParentIdShifts == 0) u.ParentIdShifts = u.GlobalStore;
			else if (u.ParentID != 0) u.ParentIdShifts = u.Parent.ParentIdShifts;
			else u.ParentIdShifts = u.ID;

			foreach (UnitCounters ch in u.Childs )
				SetParentsData(ch);
		}
		#endregion

		#region | public properties     |
		public DataSet DS	{ get { return dsconfig; }	}
		public IDictionary<string, string> Connections { get; private set; }
		public IDictionary<int, UnitCounters> Units	{ get; private set;}
       // public IDictionary<int, UnitCounters> Units_counters { get; private set; }
		public string BaseUnitConnection
		{
			get
			{
				return Units[Units[RootNode].GlobalStore].Connection;
			}
		}
		public int RootNode { get; private set; }

		#endregion

	}

	public class UnitCounters
	{
		#region | fields        |

		private int _id;
		private int _parentid = 0;
		private string _linename = "";
		private string _linenamedb = ""; // менять не надо! по умолчанию нужна пустая строка
		private string _connection;
		private UnitCounters _parent;
		private int _globalStore = -1;
		private int _rootStore;
		private int _level;

		private int _parIdKpi = -1;
		private int _parIdStops = -1;
		private int _parIdConfig = -1;
		private int _parIdSku = -1;
		private int _parIdShifts = -1;
		private string _parKpi;
		private string _parStops;
		private string _parConfig;
		private string _parSku;
		private string _parShifts;
		private string _dbname = "";
		private int _parts = 1;
		private UnitKindCounters _kind = UnitKindCounters.Node;

		#endregion

		public UnitCounters(int id)
		{
			this._id = id;
			Childs = new List<UnitCounters>();
		}
		public UnitCounters(int unit, int parentunit, string name, string conn)
		{
			_id = unit;
			_parentid = parentunit;
			_linename = name;
			_connection = conn;
			_kind = UnitKindCounters.Line;
			Childs = new List<UnitCounters>();
		}

		/// <summary>
		/// Список подузлов
		/// </summary>
		public IList<UnitCounters> Childs
		{
			get;
			private set;
		}
		/// <summary>
		/// Собственный ID
		/// </summary>
		public int ID
		{
			get { return _id; }
		}
		/// <summary>
		/// Родительский ID
		/// </summary>
		public int ParentID
		{
			get { return _parentid; }
			set { _parentid = value; }
		}
		/// <summary>
		/// У корневого уровня Level=0, каждый последующий подуровень +1
		/// </summary>
		public int Level
		{
			get { return _level; }
			set { _level = value; }
		}
		/// <summary>
		/// Имя линии из конфигурационного файла
		/// </summary>
		public string LineName
		{
			get { return _linename; }
		}
		/// <summary>
		/// Имя линии из базы данных
		/// </summary>
		public string LineNameFromDB
		{
			get { return _linenamedb; }
			set { _linenamedb = value; }
		}
		/// <summary>
		/// Строка SQL соединения
		/// </summary>
		public virtual string Connection
		{
			get { return _connection; }
		}
		/// <summary>
		/// Ссылка на родительский юнит
		/// </summary>
		public UnitCounters Parent
		{
			get { return _parent; }
			set { _parent = value; }
		}
		/// <summary>
		/// определяется по юниту верхнего уровня из конфигурационного файла
		/// </summary>
		public int GlobalStore
		{
			get { return _globalStore; }
			set { _globalStore = value; }
		}
		/// <summary>
		/// определяется по юниту верхнего уровня из конфигурационного файла
		/// </summary>
		public int RootStore
		{
			get { return _rootStore; }
			set { _rootStore = value; }
		}
		/// <summary>
		/// определяется по собственному идентификатору конфигурационного файла
		/// </summary>
		public int OwnStore{ get; set; }
		public string DatabaseName{ get; set; }
		public int ParentIdKpi
		{
			get { return _parIdKpi; }
			set
			{
				_parIdKpi = value;
				_parKpi = _parIdKpi.ToString() + "_gkpi";
			}
		}
		public int ParentIdStops
		{
			get { return _parIdStops; }
			set
			{
				_parIdStops = value;
				_parStops = _parIdStops.ToString() + "_gdwn";
			}
		}
		public int ParentIdConfig
		{
			get { return _parIdConfig; }
			set
			{
				_parIdConfig = value;
				_parConfig = _parIdConfig.ToString() + "_gcfg";
			}
		}
		public int ParentIdSku
		{
			get { return _parIdSku; }
			set
			{
				_parIdSku = value;
				_parSku = _parIdSku.ToString() + "_gsku";
			}
		}
		public int ParentIdShifts
		{
			get { return _parIdShifts; }
			set
			{
				_parIdShifts = value;
				_parShifts = _parIdShifts.ToString() + "_gshf";
			}
		}

		public string ParentKpi
		{
			get { return _parKpi; }
		}
		public string ParentStops
		{
			get { return _parStops; }
		}
		public string ParentConfig
		{
			get { return _parConfig; }
		}
		public string ParentSku
		{
			get { return _parSku; }
		}
		public string ParentShifts
		{
			get { return _parShifts; }
		}
		public virtual UnitKindCounters Kind
		{
			get { return _kind; }
			set { _kind = value; }
		}
		public int Parts 
		{
			get; 
			set; 
		}
		public string GetLongUnitName_(bool GetFullName)
		{
			return GetLongUnitName_(GetFullName, "\\");
		}
		public string GetLongUnitName_(bool GetFullName, string splitter)
		{
			return GetLongUnitName_(GetFullName, splitter, this);
		}
		private string GetLongUnitName_(bool GetFullName, string splitter, UnitCounters unit)
		{
			string retval;
			if (unit.Parent == null)
				retval = unit.LineName;
			else if (!GetFullName)
				retval = unit.Parent.LineName + splitter + unit.LineName;
			else
				retval = GetLongUnitName_(true, splitter, unit._parent) + splitter + unit.LineName;
			retval = retval.Replace("/", "_");
			return retval;
		}

	}


	public enum UnitKindCounters
	{
		/// <summary>
		/// Просто счетчик, резервная опция
		/// Connection == "" и нет детей
		/// </summary>
		Counter,
		/// <summary>
		/// Просто счетчик, резервная опция
		/// Connection == "" и нет детей
		/// </summary>
		EventCounter,
		/// <summary>
		/// Счетчик второго типа, фиксирует только
		/// время (с точностью до секунды) перехода 
		/// из 1 в 0 и назад
		/// </summary>
		Line,
		/// <summary>
		/// Сайт или регион или зона.
		/// </summary>
		Node,
		/// <summary>
		/// Неизвестно
		/// </summary>
		Unknown
	}
   
}
     
