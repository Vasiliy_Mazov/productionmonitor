using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace ProductionMonitor
{
	public class SendExceptionClickEventArgs : System.EventArgs
	{
		public DialogResult DialogResult;
		public Exception UnhandledException;
		public bool SendReport;
		//public bool IgnoreNextTime;

		public SendExceptionClickEventArgs(DialogResult DialogResultArg, Exception ExceptionArg, bool SendReportArg)
		{
			this.DialogResult = DialogResultArg;     // TRUE if user clicked on "Send Error Report" button and FALSE if on "Don't Send"
			this.UnhandledException = ExceptionArg;         // Used to store captured exception
			this.SendReport = SendReportArg;                // Contains user's request: should the App to be restarted or not
			//this.IgnoreNextTime = IgnoreNextTimeArg;
		}
	}

	/// <summary>
	/// Class for catching unhandled exception with UI dialog.
	/// </summary>
	public class UnhandledExceptionDlg
	{
		private bool _dosend = true;
		private List<string> stack = new List<string>();

		/// <summary>
		/// Set to true if you want to restart your App after falure
		/// </summary>
		public bool SendReport
		{
			get { return _dosend; }
			set { _dosend = value; }
		}

		public delegate void SendExceptionClickHandler(object sender, SendExceptionClickEventArgs args);

		//public delegate void ShowErrorReportHandler(object sender, System.EventArgs args);

		/// <summary>
		/// Occurs when user clicks on "Send Error report" button
		/// </summary>
		public event SendExceptionClickHandler OnSendExceptionClick;

		/// <summary>
		/// Occurs when user clicks on "click here" link lable to get data that will be send
		/// </summary>
		public event SendExceptionClickHandler OnShowErrorReport;

		/// <summary>
		/// Default constructor
		/// </summary>
		public UnhandledExceptionDlg()
		{
			// Add the event handler for handling UI thread exceptions to the event:
			Application.ThreadException += new ThreadExceptionEventHandler(ThreadExceptionFunction);

			// Set the unhandled exception mode to force all Windows Forms errors to go through our handler:
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

			// Add the event handler for handling non-UI thread exceptions to the event:
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionFunction);
		}

		/// <summary>
		/// Handle the UI exceptions by showing a dialog box
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ThreadExceptionFunction(Object sender, ThreadExceptionEventArgs e)
		{
			// Suppress the Dialog in Debug mode:
#if !DEBUG
			ShowUnhandledExceptionDlg(e.Exception);
#endif
		}

		/// <summary>
		/// Handle the UI exceptions by showing a dialog box
		/// </summary>
		/// <param name="sender">Sender Object</param>
		/// <param name="args">Passing arguments: original exception etc.</param>
		private void UnhandledExceptionFunction(Object sender, UnhandledExceptionEventArgs args)
		{
			// Suppress the Dialog in Debug mode:
#if !DEBUG
			ShowUnhandledExceptionDlg((Exception)args.ExceptionObject);
#endif
		}

		/// <summary>
		/// Raise Exception Dialog box for both UI and non-UI Unhandled Exceptions
		/// </summary>
		/// <param name="e">Catched exception</param>
		private void ShowUnhandledExceptionDlg(Exception e)
		{
			Exception unhandledException = e;

			if (unhandledException == null)
				unhandledException = new Exception("Unknown unhandled Exception was occurred!");
			string ex = e.Message + e.StackTrace;

			if (stack.Contains(ex))
				return;
			else
				stack.Add(ex);

			UnhandledExDlgForm exDlgForm = new UnhandledExDlgForm();
			try {
				exDlgForm.labelExceptionDate.Text = String.Format(Properties.Common.exLabelDate, DateTime.Now);
				exDlgForm.labelCaption.Text = Properties.Common.exLabelCaption;
				exDlgForm.labelDescription.Text = Properties.Common.exLabelDescription;
				exDlgForm.btAbort.Text = Properties.Common.exBtAbort;
				exDlgForm.btRetry.Text = Properties.Common.exBtRetry;
				exDlgForm.btIgnore.Text = Properties.Common.exBtIgnore;
				exDlgForm.labelLinkTitle.Text = Properties.Common.exLinkTitle;
				exDlgForm.linkLabelData.Text = Properties.Common.exLinkData;
				exDlgForm.labelTry.Text = Properties.Common.exLabelTry;

				string appName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
				exDlgForm.Text = appName;
				exDlgForm.labelTitle.Text = String.Format(Properties.Common.exLabelTitle, appName);
				exDlgForm.checkBoxSend.Text = String.Format(Properties.Common.exChbSendReport, appName);
				exDlgForm.checkBoxSend.Checked = this.SendReport;

				// Do not show link label if OnShowErrorReport is not handled
				exDlgForm.labelLinkTitle.Visible = (OnShowErrorReport != null);
				exDlgForm.linkLabelData.Visible = (OnShowErrorReport != null);

				// Disable the Button if OnSendExceptionClick event is not handled
				exDlgForm.btAbort.Enabled = (OnSendExceptionClick != null);

				// Attach reflection to checkbox checked status
				exDlgForm.checkBoxSend.CheckedChanged += delegate(object o, EventArgs ev)
				{
					this._dosend = exDlgForm.checkBoxSend.Checked;
				};

				// Handle clicks on report link label
				exDlgForm.linkLabelData.LinkClicked += delegate(object o, LinkLabelLinkClickedEventArgs ev)
				{
					if (OnShowErrorReport != null) {
						SendExceptionClickEventArgs ar = new SendExceptionClickEventArgs(DialogResult.OK, unhandledException, _dosend);
						OnShowErrorReport(this, ar);
					}
				};

				// Show the Dialog box:
				System.Windows.Forms.DialogResult res = exDlgForm.ShowDialog();

				if (OnSendExceptionClick != null) {
					SendExceptionClickEventArgs ar = new SendExceptionClickEventArgs(res, unhandledException, _dosend);
					OnSendExceptionClick(this, ar);
				}
			} finally {
				exDlgForm.Dispose();
			}
		}
	}
}
