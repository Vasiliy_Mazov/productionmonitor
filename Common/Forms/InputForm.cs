using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace ProductionMonitor
{
	/// <summary>
	/// Summary description for frmInput.
	/// </summary>
	public class InputForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btOk;
		private System.Windows.Forms.Button btCancel;
		private System.Windows.Forms.Label lb;
		private System.Windows.Forms.TextBox txt;
		private System.Windows.Forms.Label bevel;
		private System.Windows.Forms.NumericUpDown upd;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InputForm()
		{
			InitializeComponent();
		}
		public InputForm(string Description)
			: this()
		{
			this.Text = Description;
			lb.Text = Description;
			ResultS = txt.Text;
			txt.Select();
		}
		public InputForm(string Description, string SuggestedValue)
			: this()
		{
			lb.Text = Description;
			ResultS = SuggestedValue;
			txt.Text = SuggestedValue;
			txt.Select();
		}
		public InputForm(string Description, int SuggestedValue, int Min, int Max)
			: this()
		{
			ResultN = SuggestedValue;
			lb.Text = Description;
			upd.Minimum = Min;
			upd.Maximum = Max;
			upd.Value = SuggestedValue;
			upd.Visible = true;
			upd.Select();
			txt.Visible = false;
			this.Width = 237;
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btOk = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.lb = new System.Windows.Forms.Label();
			this.txt = new System.Windows.Forms.TextBox();
			this.bevel = new System.Windows.Forms.Label();
			this.upd = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.upd)).BeginInit();
			this.SuspendLayout();
			// 
			// btOk
			// 
			this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btOk.Location = new System.Drawing.Point(277, 57);
			this.btOk.Name = "btOk";
			this.btOk.Size = new System.Drawing.Size(75, 25);
			this.btOk.TabIndex = 0;
			this.btOk.Text = "Ok";
			this.btOk.Click += new System.EventHandler(this.btOk_Click);
			// 
			// btCancel
			// 
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancel.Location = new System.Drawing.Point(192, 57);
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size(75, 25);
			this.btCancel.TabIndex = 0;
			this.btCancel.Text = "Cancel";
			// 
			// lb
			// 
			this.lb.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lb.Location = new System.Drawing.Point(10, 14);
			this.lb.Name = "lb";
			this.lb.Size = new System.Drawing.Size(121, 30);
			this.lb.TabIndex = 2;
			this.lb.Text = "Label";
			// 
			// txt
			// 
			this.txt.Location = new System.Drawing.Point(137, 14);
			this.txt.Name = "txt";
			this.txt.Size = new System.Drawing.Size(213, 20);
			this.txt.TabIndex = 0;
			this.txt.Text = "Sample";
			// 
			// bevel
			// 
			this.bevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
							| System.Windows.Forms.AnchorStyles.Right)));
			this.bevel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.bevel.Location = new System.Drawing.Point(10, 47);
			this.bevel.Name = "bevel";
			this.bevel.Size = new System.Drawing.Size(342, 2);
			this.bevel.TabIndex = 4;
			this.bevel.Text = "label1";
			// 
			// upd
			// 
			this.upd.Location = new System.Drawing.Point(137, 14);
			this.upd.Name = "upd";
			this.upd.Size = new System.Drawing.Size(83, 20);
			this.upd.TabIndex = 1;
			this.upd.Visible = false;
			// 
			// frmInput
			// 
			this.AcceptButton = this.btOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size(364, 93);
			this.Controls.Add(this.upd);
			this.Controls.Add(this.bevel);
			this.Controls.Add(this.txt);
			this.Controls.Add(this.lb);
			this.Controls.Add(this.btOk);
			this.Controls.Add(this.btCancel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmInput";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Change value";
			((System.ComponentModel.ISupportInitialize)(this.upd)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private string ResultS = string.Empty;
		private int ResultN = 0;
		private void btOk_Click(object sender, System.EventArgs e)
		{
			if (upd.Visible)
				ResultN = Convert.ToInt32(upd.Value);
			else if (txt.Visible && txt.Text.Length > 0)
				ResultS = txt.Text;
		}

		public string ResS
		{
			get {	return ResultS; }
		}
		public int ResN
		{
			get {	return ResultN; }
		}
	}
}
