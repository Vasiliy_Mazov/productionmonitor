namespace ProductionMonitor
{
	partial class UnhandledExDlgForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panelTop = new System.Windows.Forms.Panel();
			this.labelTitle = new System.Windows.Forms.Label();
			this.panelDevider = new System.Windows.Forms.Panel();
			this.labelExceptionDate = new System.Windows.Forms.Label();
			this.labelCaption = new System.Windows.Forms.Label();
			this.labelDescription = new System.Windows.Forms.Label();
			this.btIgnore = new System.Windows.Forms.Button();
			this.btAbort = new System.Windows.Forms.Button();
			this.labelLinkTitle = new System.Windows.Forms.Label();
			this.linkLabelData = new System.Windows.Forms.LinkLabel();
			this.checkBoxSend = new System.Windows.Forms.CheckBox();
			this.labelTry = new System.Windows.Forms.Label();
			this.btRetry = new System.Windows.Forms.Button();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.panelTop.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.Window;
			this.panelTop.Controls.Add(this.labelTitle);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Size = new System.Drawing.Size(412, 63);
			this.panelTop.TabIndex = 0;
			// 
			// labelTitle
			// 
			this.labelTitle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelTitle.Location = new System.Drawing.Point(13, 13);
			this.labelTitle.Name = "labelTitle";
			this.labelTitle.Size = new System.Drawing.Size(387, 44);
			this.labelTitle.TabIndex = 0;
			this.labelTitle.Text = "\"{0}\" encountered a problem and needed to close";
			// 
			// panelDevider
			// 
			this.panelDevider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelDevider.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelDevider.Location = new System.Drawing.Point(0, 63);
			this.panelDevider.Name = "panelDevider";
			this.panelDevider.Size = new System.Drawing.Size(412, 2);
			this.panelDevider.TabIndex = 1;
			// 
			// labelExceptionDate
			// 
			this.labelExceptionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelExceptionDate.Location = new System.Drawing.Point(12, 68);
			this.labelExceptionDate.Name = "labelExceptionDate";
			this.labelExceptionDate.Size = new System.Drawing.Size(384, 23);
			this.labelExceptionDate.TabIndex = 2;
			this.labelExceptionDate.Text = "This error occured on {0}";
			// 
			// labelCaption
			// 
			this.labelCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelCaption.Location = new System.Drawing.Point(12, 91);
			this.labelCaption.Name = "labelCaption";
			this.labelCaption.Size = new System.Drawing.Size(387, 29);
			this.labelCaption.TabIndex = 3;
			this.labelCaption.Text = "Please tell Us about this problem";
			// 
			// labelDescription
			// 
			this.labelDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.labelDescription.Location = new System.Drawing.Point(12, 120);
			this.labelDescription.Name = "labelDescription";
			this.labelDescription.Size = new System.Drawing.Size(387, 29);
			this.labelDescription.TabIndex = 4;
			this.labelDescription.Text = "We have created an error report that you can send us. We will treat this report a" +
				 "s confidential and anonymous.";
			// 
			// btIgnore
			// 
			this.btIgnore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btIgnore.AutoSize = true;
			this.btIgnore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btIgnore.Location = new System.Drawing.Point(318, 3);
			this.btIgnore.Name = "btIgnore";
			this.btIgnore.Size = new System.Drawing.Size(60, 23);
			this.btIgnore.TabIndex = 6;
			this.btIgnore.Text = "&Ignore";
			this.btIgnore.UseVisualStyleBackColor = true;
			// 
			// btAbort
			// 
			this.btAbort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btAbort.AutoSize = true;
			this.btAbort.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.btAbort.Location = new System.Drawing.Point(252, 3);
			this.btAbort.Name = "btAbort";
			this.btAbort.Size = new System.Drawing.Size(60, 23);
			this.btAbort.TabIndex = 7;
			this.btAbort.Text = "&Abort";
			this.btAbort.UseVisualStyleBackColor = true;
			// 
			// labelLinkTitle
			// 
			this.labelLinkTitle.AutoSize = true;
			this.labelLinkTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.labelLinkTitle.Location = new System.Drawing.Point(3, 0);
			this.labelLinkTitle.Name = "labelLinkTitle";
			this.labelLinkTitle.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
			this.labelLinkTitle.Size = new System.Drawing.Size(253, 13);
			this.labelLinkTitle.TabIndex = 7;
			this.labelLinkTitle.Text = "To see what data this error report contains, please";
			// 
			// linkLabelData
			// 
			this.linkLabelData.AutoSize = true;
			this.linkLabelData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.linkLabelData.Location = new System.Drawing.Point(262, 0);
			this.linkLabelData.Name = "linkLabelData";
			this.linkLabelData.Size = new System.Drawing.Size(56, 13);
			this.linkLabelData.TabIndex = 8;
			this.linkLabelData.TabStop = true;
			this.linkLabelData.Text = "click here.";
			// 
			// checkBoxSend
			// 
			this.checkBoxSend.AutoSize = true;
			this.checkBoxSend.Checked = true;
			this.checkBoxSend.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxSend.Location = new System.Drawing.Point(15, 172);
			this.checkBoxSend.Name = "checkBoxSend";
			this.checkBoxSend.Size = new System.Drawing.Size(105, 17);
			this.checkBoxSend.TabIndex = 5;
			this.checkBoxSend.Text = "&Send error report";
			this.checkBoxSend.UseVisualStyleBackColor = true;
			// 
			// labelTry
			// 
			this.labelTry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
			this.labelTry.Location = new System.Drawing.Point(13, 194);
			this.labelTry.Name = "labelTry";
			this.labelTry.Size = new System.Drawing.Size(387, 29);
			this.labelTry.TabIndex = 9;
			// 
			// btRetry
			// 
			this.btRetry.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btRetry.AutoSize = true;
			this.btRetry.DialogResult = System.Windows.Forms.DialogResult.Retry;
			this.btRetry.Location = new System.Drawing.Point(186, 3);
			this.btRetry.Name = "btRetry";
			this.btRetry.Size = new System.Drawing.Size(60, 23);
			this.btRetry.TabIndex = 7;
			this.btRetry.Text = "&Retry";
			this.btRetry.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
							| System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel1.Controls.Add(this.btIgnore);
			this.flowLayoutPanel1.Controls.Add(this.btAbort);
			this.flowLayoutPanel1.Controls.Add(this.btRetry);
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(15, 235);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(381, 34);
			this.flowLayoutPanel1.TabIndex = 10;
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Controls.Add(this.labelLinkTitle);
			this.flowLayoutPanel2.Controls.Add(this.linkLabelData);
			this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 152);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(393, 21);
			this.flowLayoutPanel2.TabIndex = 11;
			// 
			// UnhandledExDlgForm
			// 
			this.AcceptButton = this.btIgnore;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btIgnore;
			this.ClientSize = new System.Drawing.Size(412, 271);
			this.ControlBox = false;
			this.Controls.Add(this.flowLayoutPanel2);
			this.Controls.Add(this.flowLayoutPanel1);
			this.Controls.Add(this.labelTry);
			this.Controls.Add(this.checkBoxSend);
			this.Controls.Add(this.labelDescription);
			this.Controls.Add(this.labelCaption);
			this.Controls.Add(this.labelExceptionDate);
			this.Controls.Add(this.panelDevider);
			this.Controls.Add(this.panelTop);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "UnhandledExDlgForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "UnhandledExDlgForm";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.UnhandledExDlgForm_Load);
			this.panelTop.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		internal System.Windows.Forms.Panel panelTop;
		internal System.Windows.Forms.Panel panelDevider;
		internal System.Windows.Forms.Label labelExceptionDate;
		internal System.Windows.Forms.Label labelCaption;
		internal System.Windows.Forms.Label labelDescription;
		internal System.Windows.Forms.Button btIgnore;
		internal System.Windows.Forms.Label labelTitle;
		internal System.Windows.Forms.CheckBox checkBoxSend;
		internal System.Windows.Forms.Label labelLinkTitle;
		internal System.Windows.Forms.LinkLabel linkLabelData;
		internal System.Windows.Forms.Button btAbort;
		internal System.Windows.Forms.Label labelTry;
		internal System.Windows.Forms.Button btRetry;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
	}
}
