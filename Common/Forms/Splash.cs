using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.ComponentModel;

namespace ProductionMonitor
{
	/// <summary>
	/// Description of Splash.
	/// </summary>
	public sealed class Splash : System.Windows.Forms.Form
	{

		#region | Designer stuff       |

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.txt = new System.Windows.Forms.TextBox();
            this.panel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt
            // 
            this.txt.BackColor = System.Drawing.Color.White;
            this.txt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txt.Location = new System.Drawing.Point(0, 120);
            this.txt.Multiline = true;
            this.txt.Name = "txt";
            this.txt.ReadOnly = true;
            this.txt.Size = new System.Drawing.Size(624, 27);
            this.txt.TabIndex = 0;
            this.txt.TabStop = false;
            this.txt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt.Click += new System.EventHandler(this.txt_Click);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.White;
            this.panel.Controls.Add(this.pictureBox1);
            this.panel.Controls.Add(this.txt1);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 53);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(624, 67);
            this.panel.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(624, 57);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.txt_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.ForestGreen;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(624, 35);
            this.panel1.TabIndex = 3;
            this.panel1.Click += new System.EventHandler(this.txt_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(624, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "SOFTWARE TOOLS THAT MONITOR YOUR SUPPLY CHAIN";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.txt_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 147);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(624, 51);
            this.panel2.TabIndex = 4;
            this.panel2.Click += new System.EventHandler(this.txt_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.ForestGreen;
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Controls.Add(this.panel);
            this.panel3.Controls.Add(this.txt);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(624, 198);
            this.panel3.TabIndex = 5;
            this.panel3.Click += new System.EventHandler(this.txt_Click);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(624, 51);
            this.label2.TabIndex = 1;
            //this.label2.Text = "2010-2014 Viravix Ukraine LLC\r\ne-mail: info@viravix.com";
            this.label2.Text = "2010-2014";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.txt_Click);
            // 
            // txt1
            // 
            this.txt1.BackColor = System.Drawing.Color.White;
            this.txt1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txt1.Font = new System.Drawing.Font("Courier New", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txt1.Location = new System.Drawing.Point(0, 57);
            this.txt1.Multiline = true;
            this.txt1.Name = "txt1";
            this.txt1.ReadOnly = true;
            this.txt1.Size = new System.Drawing.Size(624, 10);
            this.txt1.TabIndex = 5;
            this.txt1.TabStop = false;
            this.txt1.Click += new System.EventHandler(this.txt_Click);
            // 
            // Splash
            // 
            this.BackColor = System.Drawing.Color.DarkGreen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(628, 202);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Splash";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#region | Initialisation       |

		public Splash()
			: base()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			//	Size to the image so as to display it fully and position the form in the center screen with no border.
			//this.pictureBox1.Height = this.pictureBox1.Image.Height;
			//this.Width = this.pictureBox1.Image.Width + this.Padding.Left + this.Padding.Right;
			//this.Height = this.pictureBox1.Image.Height + txt.Height + this.Padding.Top + this.Padding.Bottom;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

			//	Force the splash to stay on top while the mainform renders but don't show it in the taskbar.
			//this.TopMost = true;
			this.ShowInTaskbar = false;

			//	Make the backcolour Fuchia and set that to be transparent
			//	so that the image can be shown with funny shapes, round corners etc.
			//this.BackColor = System.Drawing.Color.Fuchsia;
			//this.TransparencyKey = this.BackColor;

			//	Initialise a timer to do the fade out
			if (this.components == null) {
				this.components = new System.ComponentModel.Container();
			}
			this.fadeTimer = new System.Windows.Forms.Timer(this.components);
		}
		//public Splash(string label): this()
		//{
		//   this.txt.Text = label;
		//}
        public Splash(string _Caption, string _CurrentInfo, int fadeinTime )
			: this()
		{
            if (fadeinTime == 300)
                TopMost = true;
			Caption = _Caption;
			CurrentInfo = _CurrentInfo;
           // this.txt.Text = Caption + Environment.NewLine + CurrentInfo + Environment.NewLine + ""; // "wlad7777@mail.ru; wlad7777@mail.ru";
            this.txt.Text = "V." + Application.ProductVersion;
		}

		#endregion

		#region | Declare              |

		private static Splash Instance = null;
		private static BackgroundWorker BGWorkerSplash;
		
		private System.Windows.Forms.Timer fadeTimer;
		private TextBox txt;
        private Panel panel;
        private Panel panel1;
        private Label label1;
        private Panel panel2;
        private Panel panel3;
        private Label label2;
        private TextBox txt1;
		private PictureBox pictureBox1;

		//private string caption = "";
		//private string currentinfo = "";

		struct slideattributes
		{
			public Rectangle Rect;
			public int FadeStep;
			public int FadeTime;
		}

		#endregion

		#region | Static Methods       |

		public static void ShowSplash(int fadeinTime, string _Caption, string _CurrentInfo)
		{


           // progressBar1.Value += progressBar1.Value;
			if (fadeinTime < 40)
				fadeinTime = 40;
            //fadeinTime = 30;

			Caption = _Caption;
			CurrentInfo = _CurrentInfo;
			//	Only show if not showing already
			if (Instance == null) {
                Instance = new Splash(_Caption, _CurrentInfo, fadeinTime);
				Caption = _Caption;
				CurrentInfo = _CurrentInfo;

				//	Hide initially so as to avoid a nasty pre paint flicker
				Instance.Opacity = 0;
				Instance.Show();

				//	Process the initial paint events
				Application.DoEvents();

				// Perform the fade in
				if (fadeinTime > 0) {

					//	Set the timer interval so that we fade out at the same speed.
					int fadeStep = (int)System.Math.Round((double)fadeinTime / 20);  //20
                    Instance.fadeTimer.Interval = fadeStep;


					////// My stuff with slider control
					Rectangle r = new Rectangle();
					r.Location = Instance.txt.PointToScreen(new Point(-2, Instance.txt.Height));
					r.Location = new Point( Instance.Left , Instance.Top + Instance.Height + 5);
					r.Size = new Size(Instance.Width, 10);

					slideattributes sa = new slideattributes();
					sa.Rect = r;
					sa.FadeStep = fadeStep;
					sa.FadeTime = fadeinTime;
          

					BGWorkerSplash = new BackgroundWorker();
					BGWorkerSplash.WorkerReportsProgress = true;
					BGWorkerSplash.WorkerSupportsCancellation = true;
					BGWorkerSplash.DoWork += new DoWorkEventHandler(BGWorkerSplash_DoWork);
					BGWorkerSplash.RunWorkerCompleted += new RunWorkerCompletedEventHandler(BGWorkerSplash_RunWorkerCompleted);
					BGWorkerSplash.RunWorkerAsync(sa);
					
					// Fade in
					for (int i = 0; i <= fadeinTime; i += fadeStep) {
                        System.Threading.Thread.Sleep(fadeStep);  //
						Instance.Opacity += 0.05;
   
                      
					}

				} else {
					//	Set the timer interval so that we fade out instantly.
					Instance.fadeTimer.Interval = 1;
				}
				Instance.Opacity = 1;
				
				// ����� ����� ���� ����� �� ������� Esc
				Instance.Activate();
			}
		}
		public static void Fadeout()
		{
			//	Only fadeout if we are currently visible.
			if (Instance != null) {
				Instance.BeginInvoke(new MethodInvoker(Instance.Close));

				//	Process the Close Message on the Splash Thread.
				Application.DoEvents();
			}
		}
		public static void CurrentState(string message)
		{
			if (Instance != null) {
				//todo: ���� ��������� �� �������� ������ ����� � ���� �� ������� �� ��������, � ����� try ����� �� ������
				try {
                Instance.txt1.Text = message; //Caption + Environment.NewLine +
					Application.DoEvents();
				} catch { }
			}
		}
		public static string Caption { get; set; }
		public static string CurrentInfo { get; set; }

		#endregion

		#region | Close Splash Methods |

		protected override void OnClick(System.EventArgs e)
		{
			//	If we are displaying as a about dialog we need to provide a way out.
			this.Close();
		}
		protected override void OnKeyDown(KeyEventArgs e)
		{
			//	If we are displaying as a about dialog we need to provide a way out.
			if (e.KeyCode == Keys.Escape)
				this.Close();
			
		}
		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);
			
			//	Close immediatly is the timer interval is set to 1 indicating no fade.
			if (this.fadeTimer.Interval == 1) {
				e.Cancel = false;
				return;
			}

			//	Only use the timer to fade out if we have a mainform running otherwise there will be no message pump
			if (Application.OpenForms.Count > 1) {
				if (this.Opacity > 0) {
					e.Cancel = true;
					this.Opacity -= 0.05;

					//	use the timer to iteratively call the close method thereby keeping the GUI thread available for other processes.
					this.fadeTimer.Tick -= new System.EventHandler(this.FadeoutTick);
					this.fadeTimer.Tick += new System.EventHandler(this.FadeoutTick);
					this.fadeTimer.Start();
				} else {
					e.Cancel = false;
					this.fadeTimer.Stop();

					//	Clear the instance variable so we can reshow the splash, and ensure that we don't try to close it twice
					Instance = null;
				}
			} else {
				if (this.Opacity > 0) {
					//	Sleep on this thread to slow down the fade as there is no message pump running
                    System.Threading.Thread.Sleep(this.fadeTimer.Interval);  //
					Instance.Opacity -= 0.05;

					//	iteratively call the close method
					this.Close();
				} else {
					e.Cancel = false;

					//	Clear the instance variable so we can reshow the splash, and ensure that we don't try to close it twice
					Instance = null;
				}
			}
			if (BGWorkerSplash != null)
				BGWorkerSplash.CancelAsync();	


		}
		void FadeoutTick(object sender, System.EventArgs e)
		{
			this.Close();
		}
		private void txt_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		#endregion

		private static void BGWorkerSplash_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			BGWorkerSplash.DoWork -= BGWorkerSplash_DoWork;
			BGWorkerSplash.RunWorkerCompleted -= BGWorkerSplash_RunWorkerCompleted;
			BGWorkerSplash.Dispose();
			//System.Threading.Thread.Sleep(100);
			//this.Activate();
		}
		private static void BGWorkerSplash_DoWork(object sender, DoWorkEventArgs e)
		{
			slideattributes sa = (slideattributes)e.Argument;
			SlideForm operationSlideForm = new SlideForm(sa.Rect);
			operationSlideForm.Opacity = 0;
			operationSlideForm.Show();
			// Fade in
			for (int i = 0; i <= sa.FadeTime; i += sa.FadeStep) {
                System.Threading.Thread.Sleep(sa.FadeStep);//
				operationSlideForm.Opacity += 0.05;

			}
			// Waiting
			while (!BGWorkerSplash.CancellationPending) {
				Thread.Sleep(1);
				Application.DoEvents();
			}
			// Fade out
			for (int i = 0; i <= sa.FadeTime; i += sa.FadeStep) {
                System.Threading.Thread.Sleep(sa.FadeStep);//
				operationSlideForm.Opacity -= 0.05;
			}
			// �� �� ����� ��������� ���� ������� ��� ���� � ������� ������� ��������� ��� ����� ��������� �����
			//operationSlideForm.Hide();
			operationSlideForm.Close();
        }
	
	}
}
