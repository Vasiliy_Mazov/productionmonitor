using System;
using System.Drawing;
namespace ProductionMonitor
{

	public class SlideBar : System.Windows.Forms.Panel
	{
	
		#region | Declare |
		
		private System.ComponentModel.Container components = null; 
		internal System.Windows.Forms.Timer tm = new System.Windows.Forms.Timer();
		private Color Col1 = Color.Green;
        private System.Windows.Forms.ProgressBar progressBar1;		
		Random rnd = new Random(DateTime.Now.Second * DateTime.Now.DayOfYear);

		#endregion
         internal System.Windows.Forms.ProgressBar progressBar2;



		public SlideBar()
		{
			tm.Tick += new System.EventHandler(tm_Tick);
			this.Height = 5;
			this.Width = 200;
            //this.progressBar1.Height = 5;
          //  progressBar1.Width = 200;
          //  progressBar1.Value = 0;
         //   this.Controls.Add(progressBar1);
            tm.Interval = 20;// 20;

            progressBar2 = new System.Windows.Forms.ProgressBar();
            progressBar2.Height  = 10;
            //progressBar2.Width   = 400;
            progressBar2.Value   = 10;
            progressBar2.Maximum = 100;
            progressBar2.Step    = 1;
            progressBar2.Dock    = System.Windows.Forms.DockStyle.Fill;
            this.Controls.Add(progressBar2);
		}
		public SlideBar(Color Color1)
			: this()
		{
			Col1 = Color1;
			this.Height = 5;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && !(components == null))
				components.Dispose();
			base.Dispose(disposing);
		}

		private void tm_Tick(object sender, System.EventArgs e)
		{
         //   progressBar1.Value += progressBar1.Value;
            if (progressBar2.Value >= progressBar2.Maximum)
                progressBar2.Value = 0;
            progressBar2.Value = progressBar2.Value + 1;

			int w;
			Brush br;
			switch (rnd.Next(20)) {
				case 0:
					br = Brushes.Black;
					w = rnd.Next(this.Width / 9);
					break;
				case 1:
				case 2:
				case 3:
					br = Brushes.Green;
					w = rnd.Next(this.Width);
					break;
				case 4:
					br = Brushes.Gray;
					w = rnd.Next(this.Width / 2);
					break;
				case 5:
					br = Brushes.Gold;
					w = rnd.Next(this.Width / 3);
					break;
				case 6:
					br = Brushes.Red;
					w = rnd.Next(this.Width / 12);
					break;
				case 7:
					br = Brushes.Red;
					w = rnd.Next(this.Width / 6);
					break;
				default:
					w = rnd.Next(2);
					br = Brushes.Black;
					break;
			}
			Rectangle r = new Rectangle(rnd.Next(this.Width), 0, w + 1, this.Height);
			if (this.IsDisposed)
				return;
			//using (Graphics g = this.CreateGraphics()) {
			//	g.FillRectangle(br, r);

			//}

            
		}
		public void StartMoving()
		{
			this.BackColor = Col1;
			tm.Start();			// Starts moving the bar
		}
		public void StopMoving()
		{
			tm.Stop();			// Stops moving the bar
		}

        public void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(400, 23);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 0;
            this.progressBar1.Value = 70;
            this.ResumeLayout(false);

        }
	
	}

	public class SlideForm : System.Windows.Forms.Form
	{
		private System.ComponentModel.Container components = null;
		internal SlideBar LL;
		internal Size size;
      //  internal System.Windows.Forms.ProgressBar progressBar2;
		
		public SlideForm()
		{
			LL = new SlideBar();
			LL.Dock = System.Windows.Forms.DockStyle.Fill;
/*
            progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar2.Height = 4;
              progressBar2.Width = 400;
              progressBar2.Value = 10;
            LL.Controls.Add(progressBar2);
            */


			this.Controls.Add(LL);
          //  this.Controls.Add(this.progressBar1);
			this.ShowInTaskbar = false;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.TopMost = true;
			////this.BackColor = Color.DarkGreen;
			////this.Padding = new System.Windows.Forms.Padding(2);
			this.Load += new System.EventHandler(this.SlideForm_Load);
		}
		public SlideForm(Rectangle bounds)
			: this()
		{
			this.Bounds = bounds;
			this.size = bounds.Size;
		}
		public SlideForm(Size size)
			: this()
		{
			this.size = size;
			this.Size = size;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
				components.Dispose();
			base.Dispose(disposing);
		}
		private void SlideForm_Load(System.Object sender, System.EventArgs e)
		{
			// ������� ������!!! ������ �� ���� ������ �� ������ ������� Form_Load �� ��� �� ��������!!!
			this.Size = size;

			LL.StartMoving();
		}
	}

}
