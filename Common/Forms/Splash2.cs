﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.ComponentModel;

namespace ProductionMonitor.Forms
{
    public partial class Splash2 : Form
    {
        public Splash2()
        {
            InitializeComponent();
			//this.Text = String.Format("About {0}", AssemblyTitle);
			//this.labelProductName.Text = AssemblyProduct;
			//this.labelVersion.Text = String.Format("Version {0}", AssemblyVersion);
			//this.labelCopyright.Text = AssemblyCopyright;
			//this.labelCompanyName.Text = AssemblyCompany;
			//this.textBoxDescription.Text = AssemblyDescription;

			StartPosition = FormStartPosition.CenterScreen;
			FormBorderStyle = FormBorderStyle.None;

			//	Force the splash to stay on top while the mainform renders but don't show it in the taskbar.
			TopMost = true;
			ShowInTaskbar = false;

			//	Initialise a timer to do the fade out
			if (components == null)
				components = new Container();

			_fadeTimer = new System.Windows.Forms.Timer(components);
		}


        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0) {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "") {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0) {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
		#endregion

		#region | Declare              |

		private static Splash2 Instance = null;
		private System.Windows.Forms.Timer _fadeTimer;
		private Panel _panel;
		private PictureBox _picBox;
		private string _message;

		#endregion

		#region | Static Methods       |

		public static void ShowSplash(int fadeinTime)
		{

			if (fadeinTime < 40)
				fadeinTime = 40;
			//	Only show if not showing already
			if (Instance == null) {
				Instance = new Splash2();

				//	Hide initially so as to avoid a nasty pre paint flicker
				Instance.Opacity = 0;
				Instance.Show();

				//	Process the initial paint events
				Application.DoEvents();

				// Perform the fade in
				if (fadeinTime > 0) {

					//	Set the timer interval so that we fade out at the same speed.
					int fadeStep = (int)System.Math.Round((double)fadeinTime / 20);
					Instance._fadeTimer.Interval = fadeStep;

					// Fade in
					for (int i = 0; i <= fadeinTime; i += fadeStep) {
						Thread.Sleep(fadeStep);
						Instance.Opacity += 0.05;
					}

				} else {
					//	Set the timer interval so that we fade out instantly.
					Instance._fadeTimer.Interval = 1;
				}
				Instance.Opacity = 1;

				// Чтобы можно было выйти по нажатию Esc
				Instance.Activate();
			}
		}
		public static void Fadeout()
		{
			//	Only fadeout if we are currently visible.
			if (Instance != null) {
				Instance.BeginInvoke(new MethodInvoker(Instance.Close));

				//	Process the Close Message on the Splash Thread.
				Application.DoEvents();
			}
		}
		public static void Stop()
		{
			try {
				if (Instance != null) {
					Instance.BeginInvoke(new MethodInvoker(Instance.Dispose));
					Application.DoEvents();
				}
			} finally { }
		}
		public static void CurrentState(string message)
		{
			//if (Instance != null) {
			//	//todo: надо проверять из какокого потока вызов и если из другого то выходить, и тогда try будет не нужени
			//	try {
			//		Instance._message = message;
			//		Instance._picBox.Invalidate();
			//	} catch { }
			//}
		}

		#endregion

		#region | Close Splash Methods |

		protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
		{
			base.OnClosing(e);

			//	Close immediatly is the timer interval is set to 1 indicating no fade.
			if (_fadeTimer.Interval == 1) {
				e.Cancel = false;
				return;
			}

			//	Only use the timer to fade out if we have a mainform running otherwise there will be no message pump
			if (Application.OpenForms.Count > 1) {
				if (Opacity > 0) {
					e.Cancel = true;
					Opacity -= 0.05;

					//	use the timer to iteratively call the close method thereby keeping the GUI thread available for other processes.
					_fadeTimer.Tick -= FadeoutTick;
					_fadeTimer.Tick += FadeoutTick;
					_fadeTimer.Start();
				} else {
					e.Cancel = false;
					_fadeTimer.Stop();

					//	Clear the instance variable so we can reshow the splash, and ensure that we don't try to close it twice
					Instance = null;
				}
			} else {
				if (Opacity > 0) {
					//	Sleep on this thread to slow down the fade as there is no message pump running
					Thread.Sleep(_fadeTimer.Interval);
					Instance.Opacity -= 0.05;

					//	iteratively call the close method
					Close();
				} else {
					e.Cancel = false;

					//	Clear the instance variable so we can reshow the splash, and ensure that we don't try to close it twice
					Instance = null;
				}
			}
		}
		void FadeoutTick(object sender, System.EventArgs e)
		{
			Close();
		}
		private void AboutBox_Click(object sender, EventArgs e)
		{
			Close();
		}

		#endregion
	}
}
