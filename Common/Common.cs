using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Security.Cryptography;


namespace ProductionMonitor
{
	public static class C
	{
		public static string DB = "pm";
		public static Image ApplicationImage = ProductionMonitor.Properties.Common.AppLogo;
		public static Boolean TestEnvironment = File.Exists(Application.StartupPath + "\\TestEnvironment.txt");

		[FlagsAttribute]
		public enum State
		{
			DataLoading = 1,
			Idle = 2,
			Closing = 4,
            Reporting = 8,
		}

		#region | drawing       |
		public enum BorderStyle { Inset, Raised, Solid };
		public static void DrawRectangle(Graphics g, Rectangle r, BorderStyle s)
		{
			using (Pen gr = new Pen(SystemColors.ControlDark)) {
				using (Pen ws = new Pen(SystemColors.ControlLightLight)) {
					int h = r.Height;
					int w = r.Width;
					int l = r.Left;
					int t = r.Top;
					switch (s) {
						case BorderStyle.Solid:
							g.DrawRectangle(gr, r);
							break;
						case BorderStyle.Raised:
							g.DrawLine(ws, l, t, l, t + h); // left
							g.DrawLine(ws, l, t, l + w, t); // top
							g.DrawLine(gr, l + w - 1, t, l + w - 1, t + h); // right
							g.DrawLine(gr, l, t + h - 1, l + w, t + h - 1); // bottom
							break;
						case BorderStyle.Inset:
							g.DrawLine(gr, l, t, l, t + h); // left
							g.DrawLine(gr, l, t, l + w, t); // top
							g.DrawLine(ws, l + w, t, l + w, t + h); // right
							g.DrawLine(ws, l, t + h, l + w, t + h); // bottom
							break;
					}
				}
			}
		}
		#endregion

		#region | convert       |

		public static bool IsNull(object obj)
		{
			return obj == null || obj == DBNull.Value;
		}
		public static bool NotNull(object obj)
		{
			return !(obj == null || obj == DBNull.Value);
		}
		public static int GetInt(object obj)
		{
			return obj == null || obj == DBNull.Value ? 0 : Convert.ToInt32(obj);
			
			//int res;
			//if (obj == null || obj == DBNull.Value)
			//   return 0;
			//int.TryParse(obj.ToString(), out res);
			//return res;
		}
		public static bool GetBool(object obj)
		{
			return IsNull(obj) || obj == DBNull.Value ? false : Convert.ToBoolean(obj);
		}
		public static float GetFloat(object o)
		{
			float f = 0;
			float.TryParse(o.ToString(), out f);
			return GetFloat(f);
		}
		//public static float GetFloat(object o)
		//{
		//   return o == null || o == DBNull.Value ? 0 : Convert.ToSingle(o);
		//}
		public static float GetFloat(float s)
		{
			return (float.IsNaN(s) || float.IsInfinity(s)) ? 0 : s;
		}
		public static string GetCDateFilter(string field, DateTime dt1, DateTime dt2)
		{
			return string.Format("{0}>=#{1:M/d/yy H:m}# AND {0}<#{2:M/d/yy H:m}#", field, dt1, dt2);
		}
		public static string GetCDateString(DateTime datetime)
		{
			return string.Format("#{0:M/d/yy H:m}#", datetime);
		}
		public static DataRow GetRow(DataTable dtb, string flt)
		{
			if (dtb == null)
				return null;
			DataRow[] DR = dtb.Select(flt);
			if (DR.Length > 0) 
				return DR[0];
			return null;
		}
		public static string GetRowField(DataTable dtb, string filter, string field)
		{
			return  GetRowField(dtb, filter, "", field);
		}
		public static string GetRowField(DataTable dtb, string filter, string sort, string field)
		{
			if (dtb == null)
				return "";
			DataRow[] DR = dtb.Select(filter, sort);
			if (DR.Length > 0 && dtb.Columns.Contains(field))
				return DR[0][field].ToString();
			return "";
		}

		#endregion

		public static string GetAssemblyInfo(Assembly ass)
		{
			AssemblyTitleAttribute title;
			AssemblyCopyrightAttribute cr;

			title = (AssemblyTitleAttribute)AssemblyTitleAttribute.GetCustomAttribute(ass, typeof(AssemblyTitleAttribute));
			cr = (AssemblyCopyrightAttribute)AssemblyCopyrightAttribute.GetCustomAttribute(ass, typeof(AssemblyCopyrightAttribute));

            return title.Title + Environment.NewLine +
                "Version-: " + Application.ProductVersion + Environment.NewLine + " ";
			//	cr.Copyright;

		}
		public static string GetAssemblyShortInfo(Assembly ass, ref string Copyright)
		{
		   AssemblyTitleAttribute title;
		   AssemblyCopyrightAttribute cr;

		   title = (AssemblyTitleAttribute)AssemblyTitleAttribute.GetCustomAttribute(ass, typeof(AssemblyTitleAttribute));
		   cr = (AssemblyCopyrightAttribute)AssemblyCopyrightAttribute.GetCustomAttribute(ass, typeof(AssemblyCopyrightAttribute));

			Copyright = cr.Copyright;
			//return title.Title + Environment.NewLine +
			//   "Version: " + Application.ProductVersion;
			return title.Title + ",  Version: " + Application.ProductVersion;

		}

		#region | cryptography  |
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static string GetMd5Hash(string input, int salt)
		{
			return GetMd5Hash(input + salt.ToString());
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static string GetMd5Hash(string input)
		{
			// Create a new instance of the MD5CryptoServiceProvider object.
			using (MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider()) {
				// Convert the input string to a byte array and compute the hash.
				byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
				// Create a new Stringbuilder to collect the bytes and create a string.
				StringBuilder sBuilder = new StringBuilder();
				// Loop through each byte of the hashed data and format each one as a hexadecimal string.
				for (int i = 0; i < data.Length; i++)
					sBuilder.Append(data[i].ToString("x2"));
				// Return the hexadecimal string.
				return sBuilder.ToString();
			}
		}
		/// <summary>
		/// Verify a hash against a string.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="hash"></param>
		/// <param name="salt"></param>
		/// <returns></returns>
		public static bool VerifyMd5Hash(string input, string hash, int salt)
		{
			// Hash the input.
			string hashOfInput = GetMd5Hash(input, salt);

			if (0 == StringComparer.OrdinalIgnoreCase.Compare(hashOfInput, hash))
				return true;
			else
				return false;
		}
		/// <summary>
		/// Verify a hash against a string.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="hash"></param>
		/// <param name="salt"></param>
		/// <returns></returns>
		public static bool VerifyMd5Hash(string input, string hash)
		{
			// Hash the input.
			string hashOfInput = GetMd5Hash(input);

			if (0 == StringComparer.OrdinalIgnoreCase.Compare(hashOfInput, hash))
				return true;
			else
				return false;
		}
		#endregion

	}
	public class CustomProfessionalColors : ProfessionalColorTable
	{
		public override Color ToolStripGradientBegin
		{ get { return SystemColors.ControlLightLight; } }
		public override Color ToolStripGradientMiddle
		{ get { return SystemColors.ControlLight; } }
		public override Color ToolStripGradientEnd
		{ get { return SystemColors.Control; } }

		public override Color MenuStripGradientBegin
		{ get { return SystemColors.Control; } }
		public override Color MenuStripGradientEnd
		{ get { return SystemColors.ControlLightLight; } }

		public override Color StatusStripGradientBegin
		{ get { return SystemColors.Control; } }
		public override Color StatusStripGradientEnd
		{ get { return SystemColors.ControlLightLight; } }
	}
	public class StatusStripColors : ProfessionalColorTable
	{
		static Color StyleColor = Color.FromArgb(66, 156, 233);
		static Color White = Color.White;

		public override Color ToolStripGradientBegin => StyleColor;
		public override Color ToolStripGradientMiddle => StyleColor;
		public override Color ToolStripGradientEnd => StyleColor;

		public override Color MenuStripGradientBegin => StyleColor;
		public override Color MenuStripGradientEnd => StyleColor;

		public override Color StatusStripGradientBegin => StyleColor;
		public override Color StatusStripGradientEnd => StyleColor;


		public override Color ToolStripDropDownBackground => StyleColor;

		public override Color MenuItemSelected => StyleColor;
		public override Color MenuItemSelectedGradientBegin => StyleColor;
		public override Color MenuItemSelectedGradientEnd => StyleColor;
		public override Color MenuItemPressedGradientBegin => StyleColor;
		public override Color MenuItemPressedGradientEnd => StyleColor;

		public override Color ButtonSelectedBorder => White; //

		public override Color MenuBorder => StyleColor;
		public override Color MenuItemBorder => White; //
		public override Color CheckPressedBackground => White;
		public override Color CheckSelectedBackground => White;
		public override Color CheckBackground => White;

		public override Color ImageMarginGradientBegin => StyleColor;
		public override Color ImageMarginGradientEnd => StyleColor;
		public override Color ImageMarginGradientMiddle => StyleColor;
	}

	public class StatusStripRenderer : ToolStripProfessionalRenderer
	{
		public StatusStripRenderer() : base(new StatusStripColors()) { }
	}
}
