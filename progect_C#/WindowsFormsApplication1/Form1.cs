﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using C1.C1Excel;
using C1.Win.C1Chart;
using C1.Win.C1FlexGrid;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void c1Chart1_Load(object sender, EventArgs e)
        {

        }

        private void c1Chart2_Load(object sender, EventArgs e)
        {

        }

        private void c1FlexGrid1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {  /*
            TextBox tb = new TextBox();
            tb.Location = new Point(10, 10);
            tb.Size = new Size(100, 20);
            tb.Text = "test";
            this.groupBox1.Controls.Add(tb);
            */
              C1Chart c1Chart2 = new C1Chart();
          /*  c1Chart2.Location = new Point(10, 10);
            */
            c1Chart2.Size = new Size(600, 150);
           
            
            c1Chart2.Header.Style.Border.Color = SystemColors.ControlDark;
            c1Chart2.Header.Style.ForeColor    = SystemColors.ControlText;
               
            c1Chart2.ChartGroups[0].ChartData.SeriesList.Clear();
            c1Chart2.ChartGroups[1].ChartData.SeriesList.Clear();

            c1Chart2.ChartLabels.LabelsCollection.Clear();

            c1Chart2.Header.Style.Border.Color   = SystemColors.ControlDark;
            c1Chart2.Header.Style.BackColor      = Color.White;
            c1Chart2.Header.Style.ForeColor      = SystemColors.ControlText;
            c1Chart2.Header.Style.Border.Thickness = 2;
            c1Chart2.Header.Style.Border.Rounding.All = 0;
            c1Chart2.Header.Style.Opaque = true;

            c1Chart2.Header.Style.Border.Color = Color.Blue;
            c1Chart2.Header.Style.ForeColor = Color.Blue;

            
              ChartDataSeries ChartDat;
              ChartDat = new ChartDataSeries();
              ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
             // ChartDat.LineStyle.Color = MU.Kpi.Colors["TR"];


              double[] y = new double[1 * 20 + 10];
              double[] x = new double[1* 20 + 10];
            //  double m_MaxY = c1Chart2.MaxY;


              int pos;
              int[] seeds = new int[] {
				102, 102, 153,	102, 0, 102,
				205, 212, 197,	181, 95, 181,
				102, 102, 51,	51, 102, 153,
				156, 146, 115,	131, 177, 193,
				169, 182, 157,	225, 161, 121,
				153, 51,	102, 153, 102, 0,
				198, 101, 55,	102,	153, 205,
				186, 178, 157,	214, 167, 214,
				196, 99, 162,	160, 158, 181 };


           int length;
           length = 20;
              for (int i = 1; i <= length - 4; i = i + 4)
              {
                  x[i] = i ; //x[i - 1];
                  y[i] = 0; //0;
                  x[i + 1] = i  ; // chart.GetDouble((DateTime)dr[0]);
                  y[i + 1] = i;
                  x[i + 2] = i + 1; 
                  y[i + 2] = i;
                  x[i + 3] = i + 1;
                  y[i + 3] = 0;
                  
              }
              //x[length] = 0; //x[i - 1];
              y[length] = 0; //0;
              ChartDat.Y.CopyDataIn(y);
              ChartDat.X.CopyDataIn(x);


            
              pos = 10;
              int r = seeds[pos];
              int g = seeds[pos + 1];
              int b = seeds[pos + 2];
              Color c = Color.FromArgb(r, g, b);
              ChartDat.LineStyle.Color = c; // MU.Kpi.Colors["TR"];


              ChartDat.Tag = "TRM";
              int order = 0;
              c1Chart2.ChartGroups[0].ChartData.SeriesList.Insert(order, ChartDat);
            


            this.groupBox1.Controls.Add(c1Chart2);
            c1Chart2.Visible = true;

        }

        private void button2_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < 5; i++)
            {
                Button button33 = new Button();
                button33.Location = new Point(10 * i, 10 * i );
                button33.Name = "button33"  + i ;
               // button33.Visible = true;
                button33.Click += new EventHandler(button3_Click);
                button33.MouseMove +=new MouseEventHandler(button3_MouseMove);
                this.Controls.Add(button33);

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show((sender as Button).Name);
        }

        private void button3_MouseMove(object sender, MouseEventArgs e)
        {
           toolStripStatusLabel1.Text = string.Format("({0},{1})", e.X , e.Y) ;
        }

        private void sqlDataAdapter1_RowUpdated(object sender, System.Data.SqlClient.SqlRowUpdatedEventArgs e)
        {

        }

        private void dataSet21BindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            sqlConnection2.Close();
            sqlConnection2.Open();
            //sqlConnection1.Open();
            sqlDataAdapter1.Fill(dataSet21);
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }




    }
}
