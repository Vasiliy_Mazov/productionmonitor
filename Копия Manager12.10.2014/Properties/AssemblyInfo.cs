using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Production Monitor Manager")]
[assembly: AssemblyDescription("The unit for configurations.")]
[assembly: AssemblyCompany("Optimum Trend")]
[assembly: AssemblyProduct("Production Monitor")]
[assembly: AssemblyCopyright("Copyright � 2010-2014 by Alexey Piskunovich")]
[assembly: AssemblyTrademark("")]
[assembly: CLSCompliant(true)]
[assembly: Guid("8AEE3845-65C1-41F6-9AC8-86AD2641A893")]
[assembly: AssemblyVersion("3.1.1027.0")]

[assembly: ComVisibleAttribute(false)]
