﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Collections;
using System.ServiceProcess;

namespace ProductionMonitor
{
	[RunInstaller(true)]
	public class CollectorSrvInstaller : Installer
	{
		/// <summary>
		/// Public Constructor for WindowsServiceInstaller.
		/// - Put all of your Initialization code here.
		/// </summary>
		public CollectorSrvInstaller()
		{
			ServiceProcessInstaller serviceProcessInstaller = new ServiceProcessInstaller();
			ServiceInstaller serviceInstaller = new ServiceInstaller();

			//# Service Account Information
			serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
			serviceProcessInstaller.Username = null;
			serviceProcessInstaller.Password = null;

			//# Service Information
			serviceInstaller.DisplayName = "Production Monitor Collector Service";
			serviceInstaller.StartType = ServiceStartMode.Automatic;
			//serviceInstaller.Description = "Production Monitor Collector is designed to " +
			//   "collect data from the PLC and transfer to the database.";
			serviceInstaller.Description = "Provides data acquisition from the PLC " +
				"and transfer to the database.";


			//# This must be identical to the WindowsService.ServiceBase name
			//# set in the constructor of WindowsService.cs
			serviceInstaller.ServiceName = "CollectorSrv";

			this.Installers.Add(serviceProcessInstaller);
			this.Installers.Add(serviceInstaller);
		}
		protected override void OnAfterInstall(IDictionary savedState)
		{
			string autoStart = this.Context.Parameters["TEST"];
			if (autoStart.Equals("1")) {
				try {
					ServiceController sCtrl = new ServiceController("CollectorSrv");
					sCtrl.Start();
				} catch {
					//NaDa
				}
			}
			base.OnAfterInstall(savedState);
		}
	}
}

