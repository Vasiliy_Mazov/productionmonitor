using System.Diagnostics;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.Win32;
using ProductionMonitor;
using System.ServiceProcess;
using System.Threading;

namespace ProductionMonitor.Collector
{
	public sealed class frmCollector : System.Windows.Forms.Form
	{
		#region | frmCollector Declare  |

		private System.ComponentModel.IContainer components;
		internal System.Windows.Forms.NotifyIcon notify_icon;
		internal System.Windows.Forms.Timer timer;
		internal System.Windows.Forms.ToolTip tt;

		private ToolStrip ts;
		private ToolStripButton btSettings, btPLCSettings, btUpload, btHelp, btInfo, btExit, btMinimize;
		private ContextMenuStrip cmsNotify;
		private ToolStripMenuItem mnuSettings, mnuPLCSettings, mnuUpload, mnuShow, mnuExit;
		private ToolStripSeparator toolStripSeparator1;
		private StatusStrip sts;
		private ToolStripStatusLabel spCurrOperation, spAge, spTime;
		private Panel pn;

		public static System.Threading.Mutex InstanceMutex; // глобально видимый объект
		private System.Resources.ResourceManager m_rs = new System.Resources.ResourceManager(typeof(frmCollector));

		private Rectangle bounds;
		private Collector collector;
		private bool inuse = false;
		private bool should_to_exit = false;
		private int activity = 4800;
		const int activitytotal = 4800;
		private Splitter splitter1;
		private DataGridView dgvPLC;
		private DataGridView dgvProduct;
		private DataGridView dgvLog;
		private SplitContainer splitContainer1;
		private DataTable dtb_plc;
		private DataTable dtb_log;

		private DateTime _emptydate = new DateTime(1, 1, 1);

		private ToolStripButton btLog;
		private ToolStripSeparator toolStripSeparator2;
		private Queue<string> log = new Queue<string>();
		private ToolStripStatusLabel spDB;
		private Queue<DateTime> logtime = new Queue<DateTime>();
		private bool DontTrackSize = true;
		private bool there_is_service = false;
		private string srvname = "CollectorSrv";
		private bool juststarted = true;
		private bool lastSetMenuEnableValue = true;
		
		#endregion

		[STAThread]
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			if (IsAlreadyRunning())
				return; // если приложение уже запущено, выйти

			string cpr = "";
			Splash.ShowSplash(100, C.GetAssemblyShortInfo(Assembly.GetExecutingAssembly(), ref cpr), cpr);

			Application.Run(new frmCollector());
			InstanceMutex.Close(); // не забыть закрыть мьютекс.
		} //Main
		static public bool IsAlreadyRunning()
		{
			// Имя должно быть по возможности уникальным, чтобы не мешаться с другими приложениями.
			const string UniqueString = "ProductionMonitorCollector";

			// Возвращаемое значение True если мьютекс найден не был и был только что создан
			bool createdNew = false;
			try {
				// существует ли уже мьютекс с таким именем?
				InstanceMutex = new System.Threading.Mutex(false, UniqueString, out createdNew);
			} catch {
				return true;
			}
			return !createdNew;
		}

		#region | Windows Form Designer |

		public frmCollector()
		{
			//This call is required by the Windows Form Designer.
			InitializeComponent();
		}
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
				components.Dispose();
			base.Dispose(disposing);
		}
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCollector));
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.notify_icon = new System.Windows.Forms.NotifyIcon(this.components);
			this.cmsNotify = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.mnuShow = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuPLCSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuUpload = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
			this.tt = new System.Windows.Forms.ToolTip(this.components);
			this.ts = new System.Windows.Forms.ToolStrip();
			this.btSettings = new System.Windows.Forms.ToolStripButton();
			this.btPLCSettings = new System.Windows.Forms.ToolStripButton();
			this.btUpload = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.btLog = new System.Windows.Forms.ToolStripButton();
			this.btHelp = new System.Windows.Forms.ToolStripButton();
			this.btInfo = new System.Windows.Forms.ToolStripButton();
			this.btExit = new System.Windows.Forms.ToolStripButton();
			this.btMinimize = new System.Windows.Forms.ToolStripButton();
			this.sts = new System.Windows.Forms.StatusStrip();
			this.spCurrOperation = new System.Windows.Forms.ToolStripStatusLabel();
			this.spAge = new System.Windows.Forms.ToolStripStatusLabel();
			this.spTime = new System.Windows.Forms.ToolStripStatusLabel();
			this.spDB = new System.Windows.Forms.ToolStripStatusLabel();
			this.pn = new System.Windows.Forms.Panel();
			this.dgvPLC = new System.Windows.Forms.DataGridView();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.dgvLog = new System.Windows.Forms.DataGridView();
			this.dgvProduct = new System.Windows.Forms.DataGridView();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.cmsNotify.SuspendLayout();
			this.ts.SuspendLayout();
			this.sts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPLC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvLog)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// timer
			// 
			this.timer.Interval = 220;
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			// 
			// notify_icon
			// 
			this.notify_icon.ContextMenuStrip = this.cmsNotify;
			this.notify_icon.Icon = ((System.Drawing.Icon)(resources.GetObject("notify_icon.Icon")));
			this.notify_icon.Text = "Production Monitor Collector";
			this.notify_icon.DoubleClick += new System.EventHandler(this.mnuShow_Click);
			// 
			// cmsNotify
			// 
			this.cmsNotify.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShow,
            this.mnuSettings,
            this.mnuPLCSettings,
            this.mnuUpload,
            this.toolStripSeparator1,
            this.mnuExit});
			this.cmsNotify.Name = "contextMenuStrip1";
			this.cmsNotify.Size = new System.Drawing.Size(182, 120);
			// 
			// mnuShow
			// 
			this.mnuShow.Image = global::ProductionMonitor.Properties.Resources.toscrean;
			this.mnuShow.Name = "mnuShow";
			this.mnuShow.Size = new System.Drawing.Size(181, 22);
			this.mnuShow.Text = "Show";
			this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
			// 
			// mnuSettings
			// 
			this.mnuSettings.Image = global::ProductionMonitor.Properties.Resources.sett;
			this.mnuSettings.Name = "mnuSettings";
			this.mnuSettings.Size = new System.Drawing.Size(181, 22);
			this.mnuSettings.Text = "Program settings...";
			this.mnuSettings.Click += new System.EventHandler(this.btSett_Click);
			// 
			// mnuPLCSettings
			// 
			this.mnuPLCSettings.Image = global::ProductionMonitor.Properties.Resources.time;
			this.mnuPLCSettings.Name = "mnuPLCSettings";
			this.mnuPLCSettings.Size = new System.Drawing.Size(181, 22);
			this.mnuPLCSettings.Text = "PLC configuration...";
			this.mnuPLCSettings.Click += new System.EventHandler(this.btPLCSett_Click);
			// 
			// mnuUpload
			// 
			this.mnuUpload.Image = global::ProductionMonitor.Properties.Resources.down;
			this.mnuUpload.Name = "mnuUpload";
			this.mnuUpload.Size = new System.Drawing.Size(181, 22);
			this.mnuUpload.Text = "Download from PLC";
			this.mnuUpload.Click += new System.EventHandler(this.btUpload_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(178, 6);
			// 
			// mnuExit
			// 
			this.mnuExit.Image = global::ProductionMonitor.Properties.Resources.exit;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Size = new System.Drawing.Size(181, 22);
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.btExit_Click);
			// 
			// ts
			// 
			this.ts.Enabled = false;
			this.ts.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.ts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btSettings,
            this.btPLCSettings,
            this.btUpload,
            this.toolStripSeparator2,
            this.btLog,
            this.btHelp,
            this.btInfo,
            this.btExit,
            this.btMinimize});
			this.ts.Location = new System.Drawing.Point(3, 0);
			this.ts.Name = "ts";
			this.ts.Size = new System.Drawing.Size(842, 25);
			this.ts.TabIndex = 16;
			this.ts.Text = "toolStrip1";
			this.ts.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_MouseClick);
			// 
			// btSettings
			// 
			this.btSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btSettings.Image = global::ProductionMonitor.Properties.Resources.sett;
			this.btSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btSettings.Margin = new System.Windows.Forms.Padding(16, 1, 3, 2);
			this.btSettings.Name = "btSettings";
			this.btSettings.Size = new System.Drawing.Size(23, 22);
			this.btSettings.Text = "Settings";
			this.btSettings.ToolTipText = "Application settings...";
			this.btSettings.Click += new System.EventHandler(this.btSett_Click);
			// 
			// btPLCSettings
			// 
			this.btPLCSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btPLCSettings.Image = global::ProductionMonitor.Properties.Resources.time;
			this.btPLCSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btPLCSettings.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btPLCSettings.Name = "btPLCSettings";
			this.btPLCSettings.Size = new System.Drawing.Size(23, 22);
			this.btPLCSettings.Text = "PLC Settings";
			this.btPLCSettings.ToolTipText = "PLC Settings...";
			this.btPLCSettings.Click += new System.EventHandler(this.btPLCSett_Click);
			// 
			// btDownload
			// 
			this.btUpload.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btUpload.Image = global::ProductionMonitor.Properties.Resources.down;
			this.btUpload.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btUpload.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btUpload.Name = "btDownload";
			this.btUpload.Size = new System.Drawing.Size(23, 22);
			this.btUpload.Text = "Download";
			this.btUpload.ToolTipText = "Download from PLC";
			this.btUpload.Click += new System.EventHandler(this.btUpload_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(15, 0, 15, 0);
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btLog
			// 
			this.btLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btLog.Image = global::ProductionMonitor.Properties.Resources.log;
			this.btLog.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLog.Name = "btLog";
			this.btLog.Size = new System.Drawing.Size(23, 22);
			this.btLog.Text = "Log";
			this.btLog.ToolTipText = "Create log file...";
			this.btLog.Click += new System.EventHandler(this.btLog_Click);
			// 
			// btHelp
			// 
			this.btHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btHelp.Image = global::ProductionMonitor.Properties.Resources.help;
			this.btHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btHelp.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btHelp.Name = "btHelp";
			this.btHelp.Size = new System.Drawing.Size(23, 22);
			this.btHelp.Text = "Help";
			this.btHelp.ToolTipText = "Help...";
			this.btHelp.Click += new System.EventHandler(this.btHelp_Click);
			// 
			// btInfo
			// 
			this.btInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btInfo.Image = global::ProductionMonitor.Properties.Resources.info;
			this.btInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btInfo.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btInfo.Name = "btInfo";
			this.btInfo.Size = new System.Drawing.Size(23, 22);
			this.btInfo.Text = "About";
			this.btInfo.ToolTipText = "About program...";
			this.btInfo.Click += new System.EventHandler(this.btInfo_Click);
			// 
			// btExit
			// 
			this.btExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btExit.Image = global::ProductionMonitor.Properties.Resources.exit;
			this.btExit.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btExit.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btExit.Name = "btExit";
			this.btExit.Size = new System.Drawing.Size(23, 22);
			this.btExit.Text = "Exit";
			this.btExit.ToolTipText = "Exit";
			this.btExit.Click += new System.EventHandler(this.btExit_Click);
			// 
			// btMinimize
			// 
			this.btMinimize.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
			this.btMinimize.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btMinimize.Image = global::ProductionMonitor.Properties.Resources.totray;
			this.btMinimize.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btMinimize.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
			this.btMinimize.Name = "btMinimize";
			this.btMinimize.Size = new System.Drawing.Size(23, 22);
			this.btMinimize.Text = "Minimize";
			this.btMinimize.ToolTipText = "Minimize to tray";
			this.btMinimize.Click += new System.EventHandler(this.btMinimize_Click);
			// 
			// sts
			// 
			this.sts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spCurrOperation,
            this.spAge,
            this.spTime,
            this.spDB});
			this.sts.Location = new System.Drawing.Point(3, 324);
			this.sts.Name = "sts";
			this.sts.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
			this.sts.Size = new System.Drawing.Size(842, 22);
			this.sts.SizingGrip = false;
			this.sts.TabIndex = 21;
			this.sts.ShowItemToolTips = true;
			this.sts.Text = "sts";
			// 
			// spCurrOperation
			// 
			this.spCurrOperation.AutoSize = false;
			this.spCurrOperation.Name = "spCurrOperation";
			this.spCurrOperation.Size = new System.Drawing.Size(652, 17);
			this.spCurrOperation.Spring = true;
			this.spCurrOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.spCurrOperation.ToolTipText = "Current operation";
			// 
			// spAge
			// 
			this.spAge.AutoSize = false;
			this.spAge.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
			this.spAge.Name = "spAge";
			this.spAge.Size = new System.Drawing.Size(65, 17);
			this.spAge.ToolTipText = "Period of time for uploading data from the PLC";
			// 
			// spTime
			// 
			this.spTime.AutoSize = false;
			this.spTime.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
			this.spTime.Name = "spTime";
			this.spTime.Size = new System.Drawing.Size(50, 17);
			this.spTime.Text = "00:00";
			this.spTime.ToolTipText = "Time to next uploading";
			// 
			// spDB
			// 
			this.spDB.AutoSize = false;
			this.spDB.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
			this.spDB.Image = global::ProductionMonitor.Properties.Resources.db_q;
			this.spDB.Name = "spDB";
			this.spDB.Size = new System.Drawing.Size(60, 17);
			this.spDB.Text = "DB";
			this.spDB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.spDB.ToolTipText = "Database connection state";

			// 
			// pn
			// 
			this.pn.Dock = System.Windows.Forms.DockStyle.Top;
			this.pn.Location = new System.Drawing.Point(3, 25);
			this.pn.Name = "pn";
			this.pn.Size = new System.Drawing.Size(842, 2);
			this.pn.TabIndex = 23;
			this.pn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_MouseClick);
			// 
			// dgvPLC
			// 
			this.dgvPLC.AllowUserToAddRows = false;
			this.dgvPLC.AllowUserToDeleteRows = false;
			this.dgvPLC.AllowUserToResizeRows = false;
			this.dgvPLC.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgvPLC.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgvPLC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPLC.Dock = System.Windows.Forms.DockStyle.Top;
			this.dgvPLC.Location = new System.Drawing.Point(3, 27);
			this.dgvPLC.Name = "dgvPLC";
			this.dgvPLC.RowTemplate.Height = 18;
			this.dgvPLC.Size = new System.Drawing.Size(842, 59);
			this.dgvPLC.TabIndex = 24;
			this.dgvPLC.Tag = "PLC";
			this.dgvPLC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_MouseClick);
			this.dgvPLC.Paint += new System.Windows.Forms.PaintEventHandler(this.dgv_Paint);
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(3, 86);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(842, 3);
			this.splitter1.TabIndex = 26;
			this.splitter1.TabStop = false;
			// 
			// dgvLog
			// 
			this.dgvLog.AllowUserToAddRows = false;
			this.dgvLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgvLog.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgvLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvLog.Location = new System.Drawing.Point(0, 0);
			this.dgvLog.Name = "dgvLog";
			this.dgvLog.ReadOnly = true;
			this.dgvLog.RowTemplate.Height = 18;
			this.dgvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvLog.Size = new System.Drawing.Size(480, 235);
			this.dgvLog.TabIndex = 28;
			this.dgvLog.Tag = "Log";
			this.dgvLog.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_MouseClick);
			this.dgvLog.Paint += new System.Windows.Forms.PaintEventHandler(this.dgv_Paint);
			// 
			// dgvProduct
			// 
			this.dgvProduct.AllowUserToAddRows = false;
			this.dgvProduct.AllowUserToResizeRows = false;
			this.dgvProduct.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgvProduct.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvProduct.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgvProduct.Location = new System.Drawing.Point(0, 0);
			this.dgvProduct.Name = "dgvProduct";
			this.dgvProduct.ReadOnly = true;
			this.dgvProduct.RowTemplate.Height = 18;
			this.dgvProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgvProduct.Size = new System.Drawing.Size(358, 235);
			this.dgvProduct.TabIndex = 30;
			this.dgvProduct.Tag = "Product";
			this.dgvProduct.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_MouseClick);
			this.dgvProduct.Paint += new System.Windows.Forms.PaintEventHandler(this.dgv_Paint);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(3, 89);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.dgvProduct);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.dgvLog);
			this.splitContainer1.Size = new System.Drawing.Size(842, 235);
			this.splitContainer1.SplitterDistance = 358;
			this.splitContainer1.TabIndex = 31;
			// 
			// frmCollector
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(848, 346);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.dgvPLC);
			this.Controls.Add(this.pn);
			this.Controls.Add(this.ts);
			this.Controls.Add(this.sts);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmCollector";
			this.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Production Monitor Collector";
			this.Load += new System.EventHandler(this.this_Load);
			this.SizeChanged += new System.EventHandler(this.frm_SizeChanged);
			this.Shown += new System.EventHandler(this.frmCollector_Shown);
			this.VisibleChanged += new System.EventHandler(this.frm_VisibleChanged);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.this_Closing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_KeyDown);
			this.cmsNotify.ResumeLayout(false);
			this.ts.ResumeLayout(false);
			this.ts.PerformLayout();
			this.sts.ResumeLayout(false);
			this.sts.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvPLC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvLog)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		#region | At start/stop events  |

		private void this_Load(object sender, System.EventArgs e)
		{
			ToolStripManager.Renderer = new ToolStripProfessionalRenderer(new CustomProfessionalColors());
			bounds = this.Bounds; 
			dgvPLC.Scroll += new ScrollEventHandler(dgvPLC_Scroll);

			Debug.WriteLine("frmCollector.this_Load");
		}
		private void frmCollector_Shown(object sender, EventArgs e)
		{
			Splash.Fadeout();

			if (CheckService()) 
				StopService();
			InitTables();

			collector = new Collector(false);
			collector.LogEntry += new EventHandler(collector_LogEntry);
			timer.Start();

			if (collector.PLCs != null) {
				CheckPlcTable();
			} else {
				SetMenuEnable(true);
				btSettings.Enabled = false;
				btPLCSettings.Enabled = false;
				btUpload.Enabled = false;
				mnuSettings.Enabled = false;
				mnuPLCSettings.Enabled = false;
				mnuUpload.Enabled = false;
			}

			dgvLog.DataSource = dtb_log;		//dgvLog.DataSource = collector.LogTable.Copy();
			dgvLog.Columns[0].DefaultCellStyle.Format = "G";
			dgvLog.Columns[0].Width = 120;
			dgvLog.Columns[1].Width = 300;

		}
		private bool CheckService()
		{
			try {
				System.ServiceProcess.ServiceController[] services = System.ServiceProcess.ServiceController.GetServices();
				for (int i = 0; i < services.Length; i++)
					if (services[i].ServiceName == srvname)
						return true;
			} catch (Exception ex) {
				spCurrOperation.Text = ex.Message;
			}
			return false;
		}
		private void StopService()
		{
			string status = "Undefined";
			string msg = "Collector service status: ";
			try {
				ServiceController srv = new ServiceController(srvname);
				status = srv.Status.ToString();
				if (srv.Status == ServiceControllerStatus.Running || srv.Status == ServiceControllerStatus.Paused) {
					MessageBox.Show("Service will be stopped now. Once the program shut down the service will resumes its work.", "Windows Service will be stopped", MessageBoxButtons.OK, MessageBoxIcon.Information);
					there_is_service = true;
					srv.Stop();
					srv.WaitForStatus(ServiceControllerStatus.Stopped, new TimeSpan(0, 0, 5));
				}
				spCurrOperation.Text = msg + status;
			} catch (Exception ex) {
				spCurrOperation.Text = msg + ex.Message;
			}
		}
		private void InitTables()
		{
			dtb_plc = new DataTable("tbPLC");

			dtb_plc.Columns.Add("COM", typeof(string));
			dtb_plc.Columns.Add("COMReady", typeof(bool));
			dtb_plc.Columns.Add("PLC", typeof(int));
			dtb_plc.Columns.Add("PLCReady", typeof(bool));
			dtb_plc.Columns.Add("Counters", typeof(int));
			dtb_plc.Columns.Add("Rows", typeof(int));
			dtb_plc.Columns.Add("Date", typeof(DateTime));
			dtb_plc.Columns.Add("Readouts", typeof(double));
			dtb_plc.Columns.Add("Errors", typeof(double));
			dtb_plc.Columns.Add("CurrentState", typeof(string));

			dgvPLC.DataSource = dtb_plc;

            dgvPLC.Columns["COM"].Width = 50;
			dgvPLC.Columns["COMReady"].Width = 70;
			dgvPLC.Columns["PLC"].Width = 30;
			dgvPLC.Columns["PLCReady"].Width = 70;
			dgvPLC.Columns["Counters"].Width = 60;
			dgvPLC.Columns["Rows"].Width = 60;
			dgvPLC.Columns["Date"].Width = 112;
			dgvPLC.Columns["Date"].DefaultCellStyle.Format = "G";
			dgvPLC.Columns["Readouts"].Width = 60;
			dgvPLC.Columns["Errors"].Width = 60;
			dgvPLC.Columns["CurrentState"].Width = 200;

			//dgvPLC.Columns.Remove("COM");
            //DataGridViewComboBoxColumn col = new DataGridViewComboBoxColumn();
            //col.Items.AddRange(System.IO.Ports.SerialPort.GetPortNames());
            //////col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //col.DataPropertyName = "COM";
            //col.HeaderText = "COM";
            //col.Name = "COM";
            ////col.ReadOnly = true;
            //col.Width = 60;
            //col.MaxDropDownItems = 15;
            //col.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            //dgvPLC.Columns.Insert(0, col);

			dgvPLC.Columns["CurrentState"].HeaderText = "Last state info";

			dtb_log = new DataTable("tbLog");
			dtb_log.Columns.Add("Date", typeof(string));
			dtb_log.Columns.Add("Log", typeof(string));
			
			Application.DoEvents();
		}
		private void InitGrids()
		{
			dgvProduct.Columns[1].Width = 40;
			dgvProduct.Columns[2].Width = 40;
			dgvProduct.Columns[3].Width = 40;
			dgvProduct.Columns[4].Width = 80;
		}
		private void this_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (this.Visible && should_to_exit == false) {
				e.Cancel = true;
				btMinimize_Click(null, null);
			} 
		}

		#endregion

		private void timer_Tick(object sender, System.EventArgs e)
		{
			// если это первый проход то выполняем инициализацию коллектора
			if (juststarted) {
				juststarted = false;
				collector.Initialize();
				dgvProduct.DataSource = collector.ProductTable.Copy();
				InitGrids();
				spAge.Text = collector.TimerMins > 0 ? collector.Settings.Mins.ToString() + " minutes" : "On request";
				timer.Interval = 50;
				ts.Enabled = true;
			}

			// таймер тикает каждые 50 мс
			activity--;

			if (!collector.InProcess) {						// если текущее состояние пассивное
				if (activity % 20 != 0 && log.Count == 0) // и если прошло меньше секунды с последнего рефреша и лог пустой
					return;											// то можно выходить
			}

			// проверяем флаг для предотвращения повторного вызова
			if (inuse)
				return;

			inuse = true;

			try {
				if (!collector.InProcess) {
					SetMenuEnable(true);

					if (collector.TimerMins > 0) {
						spTime.Text = (collector.TimerTicks / 60).ToString("00") + ":" + (collector.TimerTicks % 60).ToString("00");
						if (collector.TimerTicks > 0 && collector.TimerTicks < 300)
							spCurrOperation.Text = new string('l', collector.TimerTicks);
					} else {
						spTime.Text = "00:00";
						spCurrOperation.Text = "Idle";
					}

				} else {
					SetMenuEnable(false);
					spTime.Text = "00:00";
					spCurrOperation.Text = collector.CurrentOperation;
				}

				// обновление грида состояния контроллеров	
				int n = 0;
				DataRow r;
				if (collector.PLCs != null) {
					CheckPlcTable();
					foreach (PLC plc in collector.PLCs.Values) {
						r = dtb_plc.Rows[n++];
						r["COM"] = plc.CommPort.PortName;
						r["COMReady"] = plc.CommPort.IsOpen;
						r["PLCReady"] = plc.LastReadoutOk;
						r["Counters"] = plc.CountersInPLC;
						r["Rows"] = plc.LastReadoutOk ? plc.Rows : -1;
						r["Date"] = plc.LastReadoutOk ? plc.CurrentPLCTime : _emptydate;
						r["Readouts"] = plc.ReadOuts;
						r["Errors"] = plc.Errors;
						r["CurrentState"] = plc.CurrentStateInfo;
					}
				}

				// если в логе появились новые записи, значит надо обновить внешний вид
				if (log.Count > 0) {
					// обновляем иконку с db в трее
					if (!collector.SqlIsAvailable)
						spDB.Image = global::ProductionMonitor.Properties.Resources.db_del;
					else if (collector.HasNoAnySqlConnection)
						spDB.Image = global::ProductionMonitor.Properties.Resources.db_q;
					else
						spDB.Image = global::ProductionMonitor.Properties.Resources.db;

					// в грид с логом добавляем строки из очереди
					DataRow dr;
					DateTime dt;
					while (log.Count > 0 && logtime.Count > 0) {
						dr = dtb_log.NewRow();
						dt = logtime.Dequeue();
						dr[0] = string.Format("{0:G}.{1:000}", dt, dt.Millisecond);
						dr[1] = log.Dequeue();
						dtb_log.Rows.Add(dr);
					}

					// в гриде с продуктом переходим на последнюю строчку
					if (dgvProduct.DataSource != null && !collector.InProcess &&
						(dgvProduct.DataSource as DataTable).Rows.Count != collector.ProductTable.Rows.Count) {
						dgvProduct.DataSource = collector.ProductTable.Copy();
						if (dgvProduct.Rows.Count > 0)
							dgvProduct.CurrentCell = dgvProduct[0, dgvProduct.Rows.Count - 1];
					}

					// в гриде с логом переходим на последнюю строчку
					dgvLog.CurrentCell = dgvLog[0, dgvLog.Rows.Count - 1];
				}

				Application.DoEvents();

				// прячем в трей если долго стоим без активности пользователя
				if (activity <= 0 && this.Visible)
					btMinimize_Click(null, null);

			} catch (Exception ex) {
				log.Enqueue("GUI exception: " + ex.Message);
				logtime.Enqueue(DateTime.Now);
			} finally {
				inuse = false;
			}
		}
		private void collector_LogEntry(object sender, EventArgs e)
		{
			Application.DoEvents();
			if (dtb_log.Rows.Count > 10000) dtb_log.Clear();

			LogEntryArgs le = e as LogEntryArgs;

			if (le.LogEntry == "Idle" || le.LogEntry == "") return;
			log.Enqueue(le.LogEntry);
			logtime.Enqueue(le.DateTime);

			Splash.CurrentState(le.LogEntry);

			Debug.WriteLine(le.LogEntry);
		}

		#region | Control Events        |

		private void frm_VisibleChanged(object sender, System.EventArgs e)
		{
			if (this.Visible) {
				this.Bounds = bounds;
				DontTrackSize = false;
				activity = activitytotal;
			}
		}
		private void frm_SizeChanged(object sender, EventArgs e)
		{
			if (DontTrackSize)
				return;
			bounds = this.Bounds;
		}
		private void frm_MouseClick(object sender, MouseEventArgs e)
		{
			activity = activitytotal;
		}
		private void frm_KeyDown(object sender, KeyEventArgs e)
		{
			activity = activitytotal;
		}
		private void dgv_Paint(object sender, PaintEventArgs e)
		{
			// рисует в верхнем углу грида подсказку
			e.Graphics.DrawString((sender as Control).Tag.ToString(), this.Font, Brushes.Black, new PointF(0, 0));
		}
		private void dgvPLC_Scroll(object sender, ScrollEventArgs e)
		{
			dgvPLC.Height = (dtb_plc.Rows.Count + 1) * dgvPLC.RowTemplate.Height + SystemInformation.HorizontalScrollBarHeight;
		}

		#endregion

		#region | Menu & Buttons        |

		private void mnuShow_Click(object sender, System.EventArgs e)
		{
			if (!this.Visible)
				this.Visible = true;
			this.Activate();
		}
		private void btSett_Click(object sender, System.EventArgs e)
		{
			if (collector.InProcess)
				return;
			try {
				frmSettings f = new frmSettings();
				f.ShowDialog(this);
				if (f.DialogResult == DialogResult.OK) {
					collector.TimerMins = f.Settings.Mins;
					spAge.Text = collector.TimerMins > 0 ? collector.Settings.Mins.ToString() + " minutes" : "On request";
					collector.Settings.MakeLogs = f.Settings.MakeLogs;
					collector.Log.IsForced = f.Settings.MakeLogs;
					collector.TimeAutoCorrection = f.Settings.TimeAutoCorrection;
					if (collector.Settings.BaudRate != f.Settings.BaudRate
						|| collector.Settings.ComTimeOut != f.Settings.ComTimeOut) {
						collector.Settings.BaudRate = f.Settings.BaudRate;
						collector.Settings.ComTimeOut = f.Settings.ComTimeOut;
						collector.InitializeHardware();
					}
				}
			} catch (Exception) { }
		}
		private void btUpload_Click(object sender, System.EventArgs e)
		{
			if (collector.InProcess) 
				return;
			collector.Routine();
		}
		private void btPLCSett_Click(object sender, System.EventArgs e)
		{
			if (collector.InProcess) 
				return;

			collector.DisableRoutine();

			frmPLCSettings frm = new frmPLCSettings(collector.COMs, collector.Settings.BaudRate, collector.Settings.ComTimeOut);
			frm.ShowDialog(this);
			if (frm.PlcHasChanged)
				collector.InitializeHardware();
			
			frm.Dispose();

			collector.EnableRoutine();
		}
		private void btInfo_Click(object sender, System.EventArgs e)
		{
			string cpr = "";
			Splash.ShowSplash(100, C.GetAssemblyShortInfo(Assembly.GetExecutingAssembly(), ref cpr), cpr);
		}
		private void btHelp_Click(object sender, System.EventArgs e)
		{
			Help.ShowHelp(this, "Collector.chm");
		}
		private void btExit_Click(object sender, System.EventArgs e)
		{
			DialogResult dr;
			dr = MessageBox.Show("Program will be closed." +
				"\r\n" + "Are you sure?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dr == DialogResult.Yes) {
				should_to_exit = true;
				collector.ShouldToExit();
				//_log.Close();
				Application.DoEvents();
				if (there_is_service) {
					try {
						ServiceController srv = new ServiceController(srvname);

						if (srv.Status == ServiceControllerStatus.Stopped) {
							srv.Start();
						} else if (srv.Status == ServiceControllerStatus.Paused) {
							srv.Continue();
						}

						srv.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 5));
						if (srv.Status == ServiceControllerStatus.Running) {
							MessageBox.Show("Service " + srvname + " was started!");
						}

					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					}
				}
				Application.Exit();
			}
		}
		private void btLog_Click(object sender, EventArgs e)
		{
			collector.MakeLog(true);
		}
		private void btMinimize_Click(object sender, System.EventArgs e)
		{
			DontTrackSize = true;

			int n, m, h, w, l, t;
			bounds = this.Bounds;
			m = 10;		// number of iterations, less > faster, more > slowly
			h = this.Height;
			w = this.Width;
			l = Screen.PrimaryScreen.WorkingArea.Width - this.Left - 200;
			t = Screen.PrimaryScreen.WorkingArea.Height - this.Top;
			for (n = 1; n <= m; n++) {
				this.Height = h * (m - n) / m;
				this.Width = w * (m - n) / m;
				this.Left = bounds.X + l * n / m;
				this.Top = bounds.Y + t * n / m;
				System.Threading.Thread.Sleep(1);
			}
			notify_icon.Visible = true;
			this.Visible = false;
		}

		#endregion

		private void CheckPlcTable()
		{
			DataRow r;
			if (collector != null && collector.PLCs != null) {
				if (collector.PLCs.Count != dtb_plc.Rows.Count) {
					dtb_plc.Rows.Clear();
					foreach (PLC plc in collector.PLCs.Values) {
						r = dtb_plc.NewRow();
						r["PLC"] = plc.NetworkAddress;
						r["PLCReady"] = plc.LastReadoutOk;
						r["Counters"] = plc.CountersInPLC;
						dtb_plc.Rows.Add(r);
					}
					dgvPLC.Height = (dtb_plc.Rows.Count + 1) * dgvPLC.RowTemplate.Height;
				}
			}
		}
		private void SetMenuEnable(bool en)
		{
			if (lastSetMenuEnableValue == en)
				return;
			else
				lastSetMenuEnableValue = en;

			foreach (ToolStripItem mi in ts.Items)
				mi.Enabled = en || (mi == btExit);

			foreach (ToolStripItem mi in cmsNotify.Items)
				mi.Enabled = en || (mi == mnuExit);

			btUpload.Enabled = en && collector.PLCs.Count > 0;
			mnuUpload.Enabled = en && collector.PLCs.Count > 0;

			dgvProduct.Enabled = en;

			if (en)
				SetCursDefault();
			else
				SetCursWait();
		}
		private void SetCursWait()
		{
			//Application.UseWaitCursor = true;
			this.Cursor = Cursors.WaitCursor;
			//Cursor.Current = Cursors.WaitCursor;
			//cursor_is_waiting = true;
			Application.DoEvents();
		}
		private void SetCursDefault()
		{
			//Application.UseWaitCursor = false;
			this.Cursor = Cursors.Default;
			//Cursor.Current = Cursors.Default;
			//cursor_is_waiting = false;
			Application.DoEvents();
			foreach (Control c in Controls) {
				c.Cursor = Cursors.Default;
				Debug.WriteLine(c.Name);
			}
			dgvLog.Cursor = Cursors.Default;
			dgvProduct.Cursor = Cursors.Default;
		}

	}
}


