using System.Diagnostics;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using Microsoft.Win32;
using System.Security.AccessControl;
using System.Security.Principal;
//using System.Xml.Serialization;

namespace ProductionMonitor.Collector
{
	public class frmSettings : System.Windows.Forms.Form
	{

		#region | Windows Form Designer generated code |
		public frmSettings()
		{
			//This call is required by the Windows Form Designer.
			InitializeComponent();

			//Add any initialization after the InitializeComponent() call

		}

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (!(components == null)) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.Container components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		internal System.Windows.Forms.Label Label1;
		internal System.Windows.Forms.Label Label2;
		internal System.Windows.Forms.Button btApply;
		internal System.Windows.Forms.Button btCancel;
		internal System.Windows.Forms.ComboBox cbReadOption;
		internal System.Windows.Forms.Label Label4;
		internal System.Windows.Forms.NumericUpDown udMins;
		internal System.Windows.Forms.ComboBox cbBaudRate;
		internal System.Windows.Forms.Button btDefault;
		internal System.Windows.Forms.Label Label5;
		internal ComboBox cbStoreOption;
		internal Label label3;
		internal Label label6;
		internal CheckBox chkLogs;
		internal Label label7;
		internal NumericUpDown udTimeOut;
		internal System.Windows.Forms.CheckBox chkTimeAuto;
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.Label1 = new System.Windows.Forms.Label();
			this.Label2 = new System.Windows.Forms.Label();
			this.btApply = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.cbReadOption = new System.Windows.Forms.ComboBox();
			this.Label4 = new System.Windows.Forms.Label();
			this.udMins = new System.Windows.Forms.NumericUpDown();
			this.cbBaudRate = new System.Windows.Forms.ComboBox();
			this.btDefault = new System.Windows.Forms.Button();
			this.Label5 = new System.Windows.Forms.Label();
			this.chkTimeAuto = new System.Windows.Forms.CheckBox();
			this.cbStoreOption = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.chkLogs = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.udTimeOut = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.udMins)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.udTimeOut)).BeginInit();
			this.SuspendLayout();
			// 
			// Label1
			// 
			this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label1.Location = new System.Drawing.Point(10, 59);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(165, 20);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "COM port baud rate";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label2.Location = new System.Drawing.Point(10, 35);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(165, 20);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "Readout interval, minutes";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btApply
			// 
			this.btApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btApply.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btApply.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btApply.Location = new System.Drawing.Point(216, 200);
			this.btApply.Name = "btApply";
			this.btApply.Size = new System.Drawing.Size(75, 20);
			this.btApply.TabIndex = 5;
			this.btApply.Text = "Apply";
			this.btApply.Click += new System.EventHandler(this.btApply_Click);
			// 
			// btCancel
			// 
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancel.Location = new System.Drawing.Point(136, 200);
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size(75, 20);
			this.btCancel.TabIndex = 6;
			this.btCancel.Text = "Cancel";
			this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
			// 
			// cbReadOption
			// 
			this.cbReadOption.Items.AddRange(new object[] {
            "Timer",
            "Request"});
			this.cbReadOption.Location = new System.Drawing.Point(181, 10);
			this.cbReadOption.Name = "cbReadOption";
			this.cbReadOption.Size = new System.Drawing.Size(110, 21);
			this.cbReadOption.TabIndex = 7;
			this.cbReadOption.SelectedIndexChanged += new System.EventHandler(this.cbReadOption_SelectedIndexChanged);
			// 
			// Label4
			// 
			this.Label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label4.Location = new System.Drawing.Point(10, 10);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(165, 20);
			this.Label4.TabIndex = 8;
			this.Label4.Text = "Readout mode";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// udMins
			// 
			this.udMins.Location = new System.Drawing.Point(181, 35);
			this.udMins.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
			this.udMins.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.udMins.Name = "udMins";
			this.udMins.Size = new System.Drawing.Size(52, 20);
			this.udMins.TabIndex = 9;
			this.udMins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.udMins.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			this.udMins.ValueChanged += new System.EventHandler(this.udMins_ValueChanged);
			// 
			// cbBaudRate
			// 
			this.cbBaudRate.Items.AddRange(new object[] {
            "Default",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
			this.cbBaudRate.Location = new System.Drawing.Point(181, 59);
			this.cbBaudRate.Name = "cbBaudRate";
			this.cbBaudRate.Size = new System.Drawing.Size(110, 21);
			this.cbBaudRate.TabIndex = 7;
			this.cbBaudRate.SelectedIndexChanged += new System.EventHandler(this.cbBaudRate_SelectedIndexChanged);
			// 
			// btDefault
			// 
			this.btDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btDefault.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btDefault.Location = new System.Drawing.Point(10, 200);
			this.btDefault.Name = "btDefault";
			this.btDefault.Size = new System.Drawing.Size(75, 20);
			this.btDefault.TabIndex = 6;
			this.btDefault.Text = "Default";
			this.btDefault.Click += new System.EventHandler(this.btDefault_Click);
			// 
			// Label5
			// 
			this.Label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Label5.Location = new System.Drawing.Point(10, 107);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(165, 20);
			this.Label5.TabIndex = 3;
			this.Label5.Text = "PLC time correction";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkTimeAuto
			// 
			this.chkTimeAuto.Location = new System.Drawing.Point(181, 112);
			this.chkTimeAuto.Name = "chkTimeAuto";
			this.chkTimeAuto.Size = new System.Drawing.Size(100, 15);
			this.chkTimeAuto.TabIndex = 10;
			this.chkTimeAuto.Text = "Auto";
			this.chkTimeAuto.CheckStateChanged += new System.EventHandler(this.chkTimeAuto_CheckStateChanged);
			// 
			// cbStoreOption
			// 
			this.cbStoreOption.Items.AddRange(new object[] {
            "Registry",
            "Application Settings"});
			this.cbStoreOption.Location = new System.Drawing.Point(181, 132);
			this.cbStoreOption.Name = "cbStoreOption";
			this.cbStoreOption.Size = new System.Drawing.Size(110, 21);
			this.cbStoreOption.TabIndex = 7;
			this.cbStoreOption.SelectedIndexChanged += new System.EventHandler(this.cbStoreOption_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label3.Location = new System.Drawing.Point(10, 132);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(165, 20);
			this.label3.TabIndex = 8;
			this.label3.Text = "Place for settings";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Location = new System.Drawing.Point(10, 157);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(165, 20);
			this.label6.TabIndex = 3;
			this.label6.Text = "To store program logs";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkLogs
			// 
			this.chkLogs.Location = new System.Drawing.Point(181, 161);
			this.chkLogs.Name = "chkLogs";
			this.chkLogs.Size = new System.Drawing.Size(100, 15);
			this.chkLogs.TabIndex = 10;
			this.chkLogs.CheckStateChanged += new System.EventHandler(this.chkLogs_CheckStateChanged);
			// 
			// label7
			// 
			this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label7.Location = new System.Drawing.Point(10, 83);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(165, 20);
			this.label7.TabIndex = 3;
			this.label7.Text = "COM port timeout, ms";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// udTimeOut
			// 
			this.udTimeOut.Location = new System.Drawing.Point(181, 83);
			this.udTimeOut.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
			this.udTimeOut.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.udTimeOut.Name = "udTimeOut";
			this.udTimeOut.Size = new System.Drawing.Size(52, 20);
			this.udTimeOut.TabIndex = 9;
			this.udTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.udTimeOut.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.udTimeOut.ValueChanged += new System.EventHandler(this.udTimeOut_ValueChanged);
			// 
			// frmSettings
			// 
			this.AcceptButton = this.btApply;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size(300, 228);
			this.ControlBox = false;
			this.Controls.Add(this.chkLogs);
			this.Controls.Add(this.chkTimeAuto);
			this.Controls.Add(this.udTimeOut);
			this.Controls.Add(this.udMins);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.cbStoreOption);
			this.Controls.Add(this.cbReadOption);
			this.Controls.Add(this.btCancel);
			this.Controls.Add(this.btApply);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.cbBaudRate);
			this.Controls.Add(this.btDefault);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.Label5);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Settings";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.frmSettings_Load);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmSettings_Paint);
			((System.ComponentModel.ISupportInitialize)(this.udMins)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.udTimeOut)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private CollectorSettings reg_settings;

		private void frmSettings_Load(object sender, System.EventArgs e)
		{
			reg_settings = new CollectorSettings();
			try {
				if (reg_settings.Mins == 0) {
					udMins.Value = 5;
					udMins.Enabled = false;
					cbReadOption.SelectedIndex = 1;
				} else {
					udMins.Value = reg_settings.Mins;
					udMins.Enabled = true;
					cbReadOption.SelectedIndex = 0;
				}
				if (reg_settings.ComTimeOut < 50)
					udTimeOut.Value = 50;
				else if (reg_settings.ComTimeOut > 2000)
					udTimeOut.Value = 2000;
				else
					udTimeOut.Value = reg_settings.ComTimeOut;

				chkTimeAuto.Checked = reg_settings.TimeAutoCorrection;
				switch (reg_settings.BaudRate) {
					case 0: cbBaudRate.SelectedIndex = 0; break;
					case 1200: cbBaudRate.SelectedIndex = 1; break;
					case 2400: cbBaudRate.SelectedIndex = 2; break;
					case 4800: cbBaudRate.SelectedIndex = 3; break;
					case 9600: cbBaudRate.SelectedIndex = 4; break;
					case 19200: cbBaudRate.SelectedIndex = 5; break;
					case 38400: cbBaudRate.SelectedIndex = 6; break;
					case 57600: cbBaudRate.SelectedIndex = 7; break;
					case 115200: cbBaudRate.SelectedIndex = 8; break;
					default: cbBaudRate.SelectedIndex = 8; break;
				}

				if (reg_settings.UseRegistry)
					cbStoreOption.SelectedIndex = 0;
				else
					cbStoreOption.SelectedIndex = 1;

				chkLogs.Checked = reg_settings.MakeLogs;
			} catch (Exception) {

			}
		}
		private void frmSettings_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			Form pn = (System.Windows.Forms.Form)sender;
			Graphics grf = e.Graphics;
			Pen dark = new Pen(SystemColors.ControlDark);
			Pen light = new Pen(SystemColors.ControlLightLight);
			int h = pn.ClientSize.Height;
			int w = pn.ClientSize.Width;
			grf.DrawLine(dark, 5, h - 37, w - 5, h - 37); // верх
			grf.DrawLine(light, 5, h - 36, w - 5, h - 36); // низ
		}
		
		private void udMins_ValueChanged(System.Object sender, System.EventArgs e)
		{
			reg_settings.Mins = System.Convert.ToInt32(udMins.Value);
		}
		private void udTimeOut_ValueChanged(object sender, EventArgs e)
		{
			reg_settings.ComTimeOut = System.Convert.ToInt32(udTimeOut.Value);
		}
		private void cbBaudRate_SelectedIndexChanged(System.Object sender, System.EventArgs e)
		{
			switch (cbBaudRate.SelectedIndex) {
				case 0: reg_settings.BaudRate = 57600; break;
				case 1: reg_settings.BaudRate = 1200; break;
				case 2: reg_settings.BaudRate = 2400; break;
				case 3: reg_settings.BaudRate = 4800; break;
				case 4: reg_settings.BaudRate = 9600; break;
				case 5: reg_settings.BaudRate = 19200; break;
				case 6: reg_settings.BaudRate = 38400; break;
				case 7: reg_settings.BaudRate = 57600; break;
				case 8: reg_settings.BaudRate = 115200; break;
			}
		}
		private void cbReadOption_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbReadOption.SelectedIndex == 0) {
				// Request
				reg_settings.Mins = System.Convert.ToInt32(udMins.Value);
				udMins.Enabled = true;
			} else {
				// Timer
				reg_settings.Mins = 0;
				udMins.Enabled = false;
			}
		}
		private void cbStoreOption_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cbStoreOption.SelectedIndex == 0) {
				reg_settings.UseRegistry = true;
			} else {
				reg_settings.UseRegistry = false;
			}

		}
		private void chkTimeAuto_CheckStateChanged(object sender, System.EventArgs e)
		{
			reg_settings.TimeAutoCorrection = chkTimeAuto.Checked;
		}
		private void chkLogs_CheckStateChanged(object sender, EventArgs e)
		{
			reg_settings.MakeLogs = chkLogs.Checked;
		}

		private void btCancel_Click(System.Object sender, System.EventArgs e)
		{
			this.Close();
		}
		private void btDefault_Click(System.Object sender, System.EventArgs e)
		{
			System.Configuration.SettingsPropertyCollection pc = global::ProductionMonitor.Properties.collsett.Default.Properties;

			reg_settings.BaudRate = Convert.ToInt32(pc["BaudRate"].DefaultValue);	// 115200; 
			//global::ProductionMonitor.collsett.Default.Properties.
			cbBaudRate.SelectedIndex = 8;

			reg_settings.Mins = Convert.ToInt32(pc["Mins"].DefaultValue);				// 5
			udMins.Value = reg_settings.Mins;
			udMins.Enabled = true;

			cbReadOption.SelectedIndex = 0;

			reg_settings.ComTimeOut = Convert.ToInt32(pc["ComTimeOut"].DefaultValue);	// 115200; 
			udTimeOut.Value = reg_settings.ComTimeOut;

			reg_settings.TimeAutoCorrection = Convert.ToBoolean(pc["TimeAutoCorrection"].DefaultValue);
			chkTimeAuto.Checked = reg_settings.TimeAutoCorrection;

			reg_settings.UseRegistry = Convert.ToBoolean(pc["UseRegistry"].DefaultValue);
			cbStoreOption.SelectedIndex = reg_settings.UseRegistry ? 0 : 1;

			reg_settings.MakeLogs = Convert.ToBoolean(pc["MakeLogs"].DefaultValue);
			chkLogs.Checked = reg_settings.MakeLogs;
		}
		private void btApply_Click(System.Object sender, System.EventArgs e)
		{
			reg_settings.SaveSettings();
			this.Close();
		}

		public CollectorSettings Settings
		{
			get { return reg_settings; }
		}

	}

	public class CollectorSettings
	{

		#region | fields                |

		private int baudrate = 115200;
		private int com_timeout = 100;
		private int mins = 5;
		private bool time_auto_correction = true;
		private bool use_registry = true;
		private bool make_logs = false;
		ProductionMonitor.Properties.collsett folder_settings;
		
		#endregion

		public CollectorSettings()
		{
			folder_settings = new ProductionMonitor.Properties.collsett();
			use_registry = folder_settings.UseRegistry;
			com_timeout = folder_settings.ComTimeOut;
			LoadAppSettings();
		}

		#region | properties            |

		public int BaudRate
		{
			get {	return baudrate; }
			set { 
				baudrate = value;
				folder_settings.BaudRate = value;
			}
		}
		public int ComTimeOut
		{
			get { return com_timeout; }
			set
			{
				com_timeout = value;
				folder_settings.ComTimeOut = value;
			}
		}
		public int Mins
		{
			get {	return mins; }
			set {	
				mins = value;
				folder_settings.Mins = value;
			}
		}
		public bool TimeAutoCorrection
		{
			get {	return time_auto_correction; }
			set {	
				time_auto_correction = value;
				folder_settings.TimeAutoCorrection = value;
			}
		}
		public bool UseRegistry
		{
			get { return use_registry; }
			set {
				use_registry = value;
				folder_settings.UseRegistry = value; }
		}
		public bool MakeLogs
		{
			get { return make_logs; }
			set
			{
				make_logs = value;
				folder_settings.MakeLogs = value;
			}
		}
		public bool ExtendedLog
		{
			get { return folder_settings.ExtendedLog; }
		}

		#endregion

		#region | public methods        |

		internal void SaveSettings()
		{
			folder_settings.Save();
			if (use_registry) {
				RegistryKey ProductionMonitorCollectorKey = GetRegistryBranch();
				if (ProductionMonitorCollectorKey != null) {
					ProductionMonitorCollectorKey.SetValue("Settings_BaudRate", this.baudrate);
					ProductionMonitorCollectorKey.SetValue("Settings_ComTimeOut", this.com_timeout);
					ProductionMonitorCollectorKey.SetValue("Settings_TimeAutoCorrection", this.time_auto_correction);
					ProductionMonitorCollectorKey.SetValue("Settings_Intervals", this.mins);
					ProductionMonitorCollectorKey.SetValue("Settings_MakeLogs", this.make_logs);
				}
			}
		}
		private void LoadAppSettings()
		{
			if (!use_registry) {
				baudrate = folder_settings.BaudRate;
				com_timeout = folder_settings.ComTimeOut;
				mins = folder_settings.Mins;
				time_auto_correction = folder_settings.TimeAutoCorrection;
				make_logs = folder_settings.MakeLogs;
			} else {

				RegistryKey ProductionMonitorCollectorKey = GetRegistryBranch();
				
				// если к реестру нет доступа то храним всё в application settings
				if (ProductionMonitorCollectorKey == null) {
					use_registry = false;
					LoadAppSettings();
					return;
				}

				string[] values = ProductionMonitorCollectorKey.GetValueNames();
				Boolean shouldSave = true;
				try {
					foreach (string val in values) {
						switch (val) {
							case "Settings_BaudRate":
								this.baudrate = Convert.ToInt32(ProductionMonitorCollectorKey.GetValue(val));
								shouldSave = false;
								break;
							case "Settings_ComTimeOut":
								this.com_timeout = Convert.ToInt32(ProductionMonitorCollectorKey.GetValue(val));
								shouldSave = false;
								break;
							case "Settings_TimeAutoCorrection":
								this.time_auto_correction = Convert.ToBoolean(ProductionMonitorCollectorKey.GetValue(val));
								shouldSave = false;
								break;
							case "Settings_Intervals":
								this.mins = Convert.ToInt32(ProductionMonitorCollectorKey.GetValue(val));
								shouldSave = false;
								break;
							case "Settings_MakeLogs":
								this.make_logs = Convert.ToBoolean(ProductionMonitorCollectorKey.GetValue(val));
								shouldSave = false;
								break;
						}
					}
				} catch { }
				if (shouldSave)
					SaveSettings();
			}
		}
		static public RegistryKey GetRegistryBranch()
		{

			SecurityIdentifier si1 = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
			RegistryAccessRule rar1 = new RegistryAccessRule(si1, RegistryRights.FullControl | RegistryRights.WriteKey, AccessControlType.Allow);
			SecurityIdentifier si2 = new SecurityIdentifier(WellKnownSidType.LocalSystemSid, null);
			RegistryAccessRule rar2 = new RegistryAccessRule(si2, RegistryRights.FullControl | RegistryRights.WriteKey, AccessControlType.Allow);
			SecurityIdentifier si3 = new SecurityIdentifier(WellKnownSidType.BuiltinAdministratorsSid, null);
			RegistryAccessRule rar3 = new RegistryAccessRule(si3, RegistryRights.FullControl | RegistryRights.WriteKey, AccessControlType.Allow);
			//RegistryAccessRule rar4 = new RegistryAccessRule("SYSTEM", RegistryRights.FullControl | RegistryRights.WriteKey, AccessControlType.Allow);

			RegistrySecurity rs = new RegistrySecurity();
			rs.AddAccessRule(rar1);
			rs.AddAccessRule(rar2);
			rs.AddAccessRule(rar3);
			//rs.AddAccessRule(rar4);

			RegistryKey ProductionMonitorCollectorKey;
			string subKey = "Software\\ProductionMonitor\\Collector";
			try {
				ProductionMonitorCollectorKey = Registry.LocalMachine.CreateSubKey(subKey, RegistryKeyPermissionCheck.Default, rs);
				return ProductionMonitorCollectorKey;
			} catch {
				//MessageBox.Show("Insufficient privileges for registry access. \r\rFor first time try to rerun this program with admin rights. \r\nProgram will be closed.", "Collector", MessageBoxButtons.OK, MessageBoxIcon.Error);
				//Application.Exit();
				return null;
			}
		}
		
		#endregion

	}
}
