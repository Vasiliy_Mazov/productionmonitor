using System.Diagnostics;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;

namespace ProductionMonitor.Collector
{
	public class frmPLCSettings : System.Windows.Forms.Form
	{

		#region | Windows Form Designer |

		public frmPLCSettings(Dictionary<string, SerialPort> COMs, int BaudRate, int ComTimeOut)
		{
			InitializeComponent();
			_COMs = COMs;
			this.baudrate = BaudRate;
			this.com_timeout = ComTimeOut;
		}

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) 
				if (!(components == null)) 
					components.Dispose();
			base.Dispose(disposing);
		}

		private System.ComponentModel.IContainer components;

		//Required by the Windows Form Designer

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		internal System.Windows.Forms.Button btSetTime;
		internal System.Windows.Forms.CheckBox chbFromPC;
		internal System.Windows.Forms.Timer timer;
		private GroupBox gbDateTime;
		private GroupBox gbLines;

		internal Button btSetLines;
		internal Button btSetCounter;
		private NumericUpDown nudLines;
		internal Button btCancelLines;
		internal Button btCancelCounter;
		internal Button btCancelTime;
		private NumericUpDown nudCounterValue;
		private ComboBox cbCounter;
		private GroupBox gbCountersSett;
		private Button btRefresh;
		private ListBox lbAddress;
		private Label lbPCTime;
		private Label label2;
		private DataGridViewTextBoxColumn Column1;
		private DataGridViewTextBoxColumn Column2;
		private DataGridViewButtonColumn Column3;
		private ComboBox cbCOMs;
		internal System.Windows.Forms.TextBox txtNewDate;
		private Dictionary<string, SerialPort> _COMs;
		private int baudrate = 115200;
		private int com_timeout = 100;

		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.btSetTime = new System.Windows.Forms.Button();
			this.txtNewDate = new System.Windows.Forms.TextBox();
			this.chbFromPC = new System.Windows.Forms.CheckBox();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.gbDateTime = new System.Windows.Forms.GroupBox();
			this.label2 = new System.Windows.Forms.Label();
			this.lbPCTime = new System.Windows.Forms.Label();
			this.btCancelTime = new System.Windows.Forms.Button();
			this.gbLines = new System.Windows.Forms.GroupBox();
			this.nudLines = new System.Windows.Forms.NumericUpDown();
			this.btCancelLines = new System.Windows.Forms.Button();
			this.btSetLines = new System.Windows.Forms.Button();
			this.cbCounter = new System.Windows.Forms.ComboBox();
			this.nudCounterValue = new System.Windows.Forms.NumericUpDown();
			this.btCancelCounter = new System.Windows.Forms.Button();
			this.btSetCounter = new System.Windows.Forms.Button();
			this.gbCountersSett = new System.Windows.Forms.GroupBox();
			this.btRefresh = new System.Windows.Forms.Button();
			this.lbAddress = new System.Windows.Forms.ListBox();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewButtonColumn();
			this.cbCOMs = new System.Windows.Forms.ComboBox();
			this.gbDateTime.SuspendLayout();
			this.gbLines.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudLines)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudCounterValue)).BeginInit();
			this.gbCountersSett.SuspendLayout();
			this.SuspendLayout();
			// 
			// btSetTime
			// 
			this.btSetTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btSetTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btSetTime.Location = new System.Drawing.Point(149, 86);
			this.btSetTime.Name = "btSetTime";
			this.btSetTime.Size = new System.Drawing.Size(84, 20);
			this.btSetTime.TabIndex = 2;
			this.btSetTime.Text = "Apply";
			this.btSetTime.Click += new System.EventHandler(this.btSetTime_Click);
			// 
			// txtNewDate
			// 
			this.txtNewDate.Location = new System.Drawing.Point(19, 46);
			this.txtNewDate.Name = "txtNewDate";
			this.txtNewDate.Size = new System.Drawing.Size(120, 20);
			this.txtNewDate.TabIndex = 0;
			this.txtNewDate.Click += new System.EventHandler(this.txtNewDate_Click);
			this.txtNewDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNewDate_KeyDown);
			// 
			// chbFromPC
			// 
			this.chbFromPC.Location = new System.Drawing.Point(145, 46);
			this.chbFromPC.Name = "chbFromPC";
			this.chbFromPC.Size = new System.Drawing.Size(95, 20);
			this.chbFromPC.TabIndex = 9;
			this.chbFromPC.Text = "Take from PC";
			this.chbFromPC.CheckedChanged += new System.EventHandler(this.chbFromPC_CheckedChanged);
			// 
			// timer
			// 
			this.timer.Interval = 1000;
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			// 
			// gbDateTime
			// 
			this.gbDateTime.Controls.Add(this.label2);
			this.gbDateTime.Controls.Add(this.lbPCTime);
			this.gbDateTime.Controls.Add(this.btCancelTime);
			this.gbDateTime.Controls.Add(this.chbFromPC);
			this.gbDateTime.Controls.Add(this.btSetTime);
			this.gbDateTime.Controls.Add(this.txtNewDate);
			this.gbDateTime.Enabled = false;
			this.gbDateTime.Location = new System.Drawing.Point(143, 12);
			this.gbDateTime.Name = "gbDateTime";
			this.gbDateTime.Size = new System.Drawing.Size(246, 115);
			this.gbDateTime.TabIndex = 2;
			this.gbDateTime.TabStop = false;
			this.gbDateTime.Text = "Date and time in PLC";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(145, 25);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(95, 20);
			this.label2.TabIndex = 10;
			this.label2.Text = "- PC Time";
			// 
			// lbPCTime
			// 
			this.lbPCTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbPCTime.Location = new System.Drawing.Point(19, 24);
			this.lbPCTime.Name = "lbPCTime";
			this.lbPCTime.Size = new System.Drawing.Size(120, 18);
			this.lbPCTime.TabIndex = 10;
			this.lbPCTime.Text = "label1";
			// 
			// btCancelTime
			// 
			this.btCancelTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancelTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancelTime.Location = new System.Drawing.Point(55, 86);
			this.btCancelTime.Name = "btCancelTime";
			this.btCancelTime.Size = new System.Drawing.Size(84, 20);
			this.btCancelTime.TabIndex = 1;
			this.btCancelTime.Text = "Cancel";
			this.btCancelTime.Click += new System.EventHandler(this.btCancelTime_Click);
			// 
			// gbLines
			// 
			this.gbLines.Controls.Add(this.nudLines);
			this.gbLines.Controls.Add(this.btCancelLines);
			this.gbLines.Controls.Add(this.btSetLines);
			this.gbLines.Enabled = false;
			this.gbLines.Location = new System.Drawing.Point(143, 133);
			this.gbLines.Name = "gbLines";
			this.gbLines.Size = new System.Drawing.Size(246, 86);
			this.gbLines.TabIndex = 3;
			this.gbLines.TabStop = false;
			this.gbLines.Text = "Number of counters (lines)";
			// 
			// nudLines
			// 
			this.nudLines.Location = new System.Drawing.Point(19, 19);
			this.nudLines.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.nudLines.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudLines.Name = "nudLines";
			this.nudLines.Size = new System.Drawing.Size(54, 20);
			this.nudLines.TabIndex = 0;
			this.nudLines.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudLines.ValueChanged += new System.EventHandler(this.nudValueChanged);
			// 
			// btCancelLines
			// 
			this.btCancelLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancelLines.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancelLines.Location = new System.Drawing.Point(55, 57);
			this.btCancelLines.Name = "btCancelLines";
			this.btCancelLines.Size = new System.Drawing.Size(84, 20);
			this.btCancelLines.TabIndex = 1;
			this.btCancelLines.Text = "Cancel";
			this.btCancelLines.Click += new System.EventHandler(this.btCancelLines_Click);
			// 
			// btSetLines
			// 
			this.btSetLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btSetLines.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btSetLines.Location = new System.Drawing.Point(149, 57);
			this.btSetLines.Name = "btSetLines";
			this.btSetLines.Size = new System.Drawing.Size(84, 20);
			this.btSetLines.TabIndex = 2;
			this.btSetLines.Text = "Apply";
			this.btSetLines.Click += new System.EventHandler(this.btSetLines_Click);
			// 
			// cbCounter
			// 
			this.cbCounter.FormattingEnabled = true;
			this.cbCounter.Location = new System.Drawing.Point(18, 20);
			this.cbCounter.Name = "cbCounter";
			this.cbCounter.Size = new System.Drawing.Size(97, 21);
			this.cbCounter.TabIndex = 0;
			this.cbCounter.SelectedIndexChanged += new System.EventHandler(this.cbCounter_SelectedIndexChanged);
			// 
			// nudCounterValue
			// 
			this.nudCounterValue.Location = new System.Drawing.Point(121, 21);
			this.nudCounterValue.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
			this.nudCounterValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudCounterValue.Name = "nudCounterValue";
			this.nudCounterValue.Size = new System.Drawing.Size(54, 20);
			this.nudCounterValue.TabIndex = 1;
			this.nudCounterValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudCounterValue.ValueChanged += new System.EventHandler(this.nudValueChanged);
			// 
			// btCancelCounter
			// 
			this.btCancelCounter.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancelCounter.Location = new System.Drawing.Point(55, 52);
			this.btCancelCounter.Name = "btCancelCounter";
			this.btCancelCounter.Size = new System.Drawing.Size(84, 20);
			this.btCancelCounter.TabIndex = 2;
			this.btCancelCounter.Text = "Cancel";
			this.btCancelCounter.Click += new System.EventHandler(this.btCancelCounter_Click);
			// 
			// btSetCounter
			// 
			this.btSetCounter.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btSetCounter.Location = new System.Drawing.Point(148, 52);
			this.btSetCounter.Name = "btSetCounter";
			this.btSetCounter.Size = new System.Drawing.Size(85, 20);
			this.btSetCounter.TabIndex = 3;
			this.btSetCounter.Text = "Apply";
			this.btSetCounter.Click += new System.EventHandler(this.btSetCounter_Click);
			// 
			// gbCountersSett
			// 
			this.gbCountersSett.Controls.Add(this.cbCounter);
			this.gbCountersSett.Controls.Add(this.btCancelCounter);
			this.gbCountersSett.Controls.Add(this.nudCounterValue);
			this.gbCountersSett.Controls.Add(this.btSetCounter);
			this.gbCountersSett.Enabled = false;
			this.gbCountersSett.Location = new System.Drawing.Point(143, 225);
			this.gbCountersSett.Name = "gbCountersSett";
			this.gbCountersSett.Size = new System.Drawing.Size(246, 81);
			this.gbCountersSett.TabIndex = 4;
			this.gbCountersSett.TabStop = false;
			this.gbCountersSett.Text = "Counter multiplier settings";
			// 
			// btRefresh
			// 
			this.btRefresh.Location = new System.Drawing.Point(12, 285);
			this.btRefresh.Name = "btRefresh";
			this.btRefresh.Size = new System.Drawing.Size(118, 21);
			this.btRefresh.TabIndex = 1;
			this.btRefresh.Text = "Refresh";
			this.btRefresh.UseVisualStyleBackColor = true;
			this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
			// 
			// lbAddress
			// 
			this.lbAddress.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.lbAddress.FormattingEnabled = true;
			this.lbAddress.Location = new System.Drawing.Point(12, 44);
			this.lbAddress.Margin = new System.Windows.Forms.Padding(10);
			this.lbAddress.Name = "lbAddress";
			this.lbAddress.Size = new System.Drawing.Size(118, 225);
			this.lbAddress.TabIndex = 5;
			this.lbAddress.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbAddress_DrawItem);
			this.lbAddress.SelectedIndexChanged += new System.EventHandler(this.lbAddress_SelectedIndexChanged);
			// 
			// Column1
			// 
			this.Column1.Frozen = true;
			this.Column1.HeaderText = "Counter";
			this.Column1.Name = "Column1";
			this.Column1.ReadOnly = true;
			this.Column1.Width = 60;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "Divider";
			this.Column2.Name = "Column2";
			this.Column2.Width = 60;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "Apply";
			this.Column3.Name = "Column3";
			this.Column3.Width = 60;
			// 
			// cbCOMs
			// 
			this.cbCOMs.FormattingEnabled = true;
			this.cbCOMs.Location = new System.Drawing.Point(12, 12);
			this.cbCOMs.Name = "cbCOMs";
			this.cbCOMs.Size = new System.Drawing.Size(118, 21);
			this.cbCOMs.TabIndex = 0;
			// 
			// frmPLCSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(404, 318);
			this.Controls.Add(this.cbCOMs);
			this.Controls.Add(this.lbAddress);
			this.Controls.Add(this.btRefresh);
			this.Controls.Add(this.gbCountersSett);
			this.Controls.Add(this.gbLines);
			this.Controls.Add(this.gbDateTime);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmPLCSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "PLC Configuration";
			this.TopMost = true;
			this.Shown += new System.EventHandler(this.frmPLCSettings_Shown);
			this.gbDateTime.ResumeLayout(false);
			this.gbDateTime.PerformLayout();
			this.gbLines.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.nudLines)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudCounterValue)).EndInit();
			this.gbCountersSett.ResumeLayout(false);
			this.ResumeLayout(false);

		}


		#endregion

		#region | Declarations          |

		private System.IO.Ports.SerialPort comport = new System.IO.Ports.SerialPort();
		private bool bTimeChanged = false;
		const string NO_CONN = "No connection...";
		private byte NodeID = 1;
		private ProductionMonitor.Collector.PLC plc;
		private bool plc_has_changed = false;
		public struct PlcAttributs
		{
			public string ComPort;
			public int NwAddress;
		}
		public Dictionary<PlcAttributs, PLC> plcs = new Dictionary<PlcAttributs, PLC>();

		#endregion

		private void frmPLCSettings_Shown(object sender, EventArgs e)
		{
			this.UseWaitCursor = true;

			// ��������� ������ ��������� ������� PLC �� 1 �� 16
			for (int n = 1; n <= 16; n++)
				lbAddress.Items.Add("  PLC  Address  " + n.ToString("00"));

			// ���������� ������ ��������� COM ������ � ������� � ��������� GUI
			string[] ports = System.IO.Ports.SerialPort.GetPortNames();
			cbCOMs.Items.AddRange(ports);
			if (cbCOMs.Items.Count > 0) {
				cbCOMs.SelectedItem = ports[0];
				comport = GetCOM(ports[0]);
			}
			cbCOMs.SelectedIndexChanged += new EventHandler(cbCOMs_SelectedIndexChanged);

			// �������� ������ ����� � ��� ����� �� ������� �������� ������������ PLC � ������� 1
			lbAddress.SelectedIndex = 0;
			timer.Start();
			plcs.Clear();
		}
		private SerialPort GetCOM(string comportname)
		{
			SerialPort port = null;
			if (_COMs.ContainsKey(comportname)) {
				port = _COMs[comportname];
				try {
					port.Open();
				} catch {
					//MakeLog(p, ex.Message);
				}
			} else {
				try {
					port = new SerialPort(comportname);
					port.BaudRate = baudrate;

					port.ReadTimeout = com_timeout;
					port.WriteTimeout = com_timeout;
					port.DataBits = 8;
					port.Parity = System.IO.Ports.Parity.None;
					port.StopBits = System.IO.Ports.StopBits.One;
					port.Handshake = System.IO.Ports.Handshake.None;

					port.Open();
					_COMs.Add(comportname, port);
				} catch {
					//MakeLog(p, ex.Message);
				}
			}
			return port;
		}
		private void GetPLC()
		{
			this.UseWaitCursor = true;

			// ������������ �����
			SetFormEnabled(false);
			plc = null;
			lbAddress.Refresh();

			// �������� ���������������� PLC �� ���������� ������
			NodeID = Convert.ToByte(lbAddress.SelectedIndex + 1);
			plc = new PLC(comport, NodeID, false);
			plc.InitializeWithoutConfigFile();

			// ������������� �������� �� ����������� ������������� PLC
			cbCounter.Items.Clear();
			for (int n = 1; n <= plc.CountersInPLC; n++)
				cbCounter.Items.Add("Counter " + n.ToString());

			if (plc.LastReadoutOk) {
				cbCounter.SelectedIndex = 0;
				nudLines.Value = plc.CountersInPLC;
			} else {
				nudLines.Value = 1;
				nudCounterValue.Value = 1;
				txtNewDate.Text = NO_CONN;
				cbCounter.Text = "";
			}

			// ������������� �����
			SetFormEnabled(plc.LastReadoutOk);
			SetEnabled();
			
			this.UseWaitCursor = false;
			Cursor.Current = Cursors.Default;

			lbAddress.Refresh();
		}
		private void SetEnabled()
		{
			//if (plc == null || !plc.LastReadoutOk)
			//   return;
			bool plcok = plc != null && plc.LastReadoutOk;

			bool b = bTimeChanged | chbFromPC.Checked;
			btSetTime.Enabled = b;
			btCancelTime.Enabled = b;

			b = plcok && nudLines.Value != plc.CountersInPLC;
			btSetLines.Enabled = b;
			btCancelLines.Enabled = b;

			b = plcok && nudCounterValue.Value != plc.PlcUnits[cbCounter.SelectedIndex + 1].PlcQuotientFromPLC;
			btSetCounter.Enabled = b;
			btCancelCounter.Enabled = b;
		}
		private void SetFormEnabled(bool enable)
		{
			gbLines.Enabled = enable;
			gbDateTime.Enabled = enable;
			gbCountersSett.Enabled = enable;
		}
		private void timer_Tick(object sender, System.EventArgs e)
		{
			// ������ ��� � �������
			lbPCTime.Text = DateTime.Now.ToString();
			if (!bTimeChanged) {
				if (plc != null && plc.LastReadoutOk)
					txtNewDate.Text = plc.CurrentPLCTime.ToString();
				else 
					txtNewDate.Text = NO_CONN;
			}
		}

		#region | Controls events       |

		private void lbAddress_SelectedIndexChanged(object sender, EventArgs e)
		{
			GetPLC();
		}
		private void cbCOMs_SelectedIndexChanged(object sender, EventArgs e)
		{
			comport = GetCOM(cbCOMs.SelectedItem.ToString());
			GetPLC();
		}
		private void txtNewDate_Click(object sender, System.EventArgs e)
		{
			bTimeChanged = true;
			SetEnabled();
		}
		private void txtNewDate_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			bTimeChanged = true;
			SetEnabled();
		}
		private void cbCounter_SelectedIndexChanged(object sender, EventArgs e)
		{
			nudCounterValue.Value = plc.PlcUnits[cbCounter.SelectedIndex + 1].PlcQuotientFromPLC;
			SetEnabled();
		}
		private void nudValueChanged(object sender, EventArgs e)
		{
			SetEnabled();
		}
		private void chbFromPC_CheckedChanged(object sender, EventArgs e)
		{
			SetEnabled();
		}
		private void btRefresh_Click(object sender, EventArgs e)
		{
			GetPLC();
		}

		#endregion

		#region | Buttons click         |

		private void btSetTime_Click(System.Object sender, System.EventArgs e)
		{
			DateTime d = Convert.ToDateTime(null);
			try {
				UseWaitCursor = true;
				if (chbFromPC.Checked)
					d = DateTime.Now;
				else {
					try { d = DateTime.Parse(txtNewDate.Text); } catch (Exception) { }
				}
				plc.SetCurrentTime(d);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			} finally {
				UseWaitCursor = false;
				Cursor.Current = Cursors.Default;
			}
			bTimeChanged = false;
			chbFromPC.Checked = false;
		}
		private void btCancelTime_Click(object sender, EventArgs e)
		{
			bTimeChanged = false;
			chbFromPC.Checked = false;
		}
		private void btSetLines_Click(object sender, EventArgs e)
		{
			int prevvalue = plc.CountersInPLC;
			int suggvalue = Convert.ToByte(nudLines.Value);
			if (prevvalue != suggvalue) {
				if (MessageBox.Show("����� ��������� ���������� ��������� ����� ���������� " +
					"������� �������� ������  �� ����������� ����������� �� ����� �������. " + 
					"�� ����������� �� ����������� ��������?", "Warning", 
					MessageBoxButtons.YesNo, MessageBoxIcon.Warning, 
					MessageBoxDefaultButton.Button2) == DialogResult.No) {
					nudLines.Value = prevvalue;
					return;
				} 
			}
			plc.SetCounters(suggvalue);
			nudLines.Value = plc.CountersInPLC;
			SetEnabled();
			if (prevvalue != plc.CountersInPLC) {
				PlcAttributs att = new PlcAttributs();
				att.ComPort = comport.PortName;
				att.NwAddress = plc.NetworkAddress;
				if (!plcs.ContainsKey(att))
					plcs.Add(att, plc);
				plc_has_changed = true;
			}
		}
		private void btCancelLines_Click(object sender, EventArgs e)
		{
			nudLines.Value = plc.CountersInPLC;
			SetEnabled();
		}
		private void btSetCounter_Click(object sender, EventArgs e)
		{
			int prevvalue = plc.PlcUnits[cbCounter.SelectedIndex + 1].PlcQuotientFromPLC;
			int suggvalue = Convert.ToInt32(nudCounterValue.Value);
			if (prevvalue != suggvalue) {
				if (MessageBox.Show("����� ���������� ������������ ���������� ��������� � ���, ��� " + Environment.NewLine +
					"�������� ������ �� ����������� ������� ������� � ���� ������. " + Environment.NewLine +
					"�������� �� ��������� ����� 1. ����� ���������� ��� �������� �� 60000 ���/���. " + Environment.NewLine +
					Environment.NewLine +	"�� ����������� �� ����������� ��������?", "Warning",
					MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
					MessageBoxDefaultButton.Button2) == DialogResult.No) {
					nudCounterValue.Value = prevvalue;
					return;
				}
			}

			plc.SetCounter(cbCounter.SelectedIndex + 1, suggvalue);
			nudCounterValue.Value = plc.PlcUnits[cbCounter.SelectedIndex + 1].PlcQuotientFromPLC;
			SetEnabled();
		}
		private void btCancelCounter_Click(object sender, EventArgs e)
		{
			nudCounterValue.Value = plc.PlcUnits[cbCounter.SelectedIndex + 1].PlcQuotientFromPLC;
			SetEnabled();
		}

		#endregion

		private void lbAddress_DrawItem(object sender, DrawItemEventArgs e)
		{
			if ((e.State & DrawItemState.Selected) != DrawItemState.Selected)
				e.DrawBackground();

			// Define the default color of the brush as black.
			Brush myBrush = Brushes.Black;

			// Draw the current item text based on the current Font and the custom brush settings.
			e.Graphics.DrawString(lbAddress.Items[e.Index].ToString(), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault);
			// If the ListBox has focus, draw a focus rectangle around the selected item.
			e.DrawFocusRectangle();

			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected) {
				Rectangle r = e.Bounds;
				r.Width--;
				r.Height--;
				e.Graphics.DrawRectangle(SystemPens.Highlight, r);
				Bitmap bm;
				if (plc != null && plc.LastReadoutOk)
					bm = global::ProductionMonitor.Properties.Resources.chekout;
				else
					bm = global::ProductionMonitor.Properties.Resources.denied;

				Point p = e.Bounds.Location;
				p.Offset(new Point(lbAddress.Width - 20, 0));
				e.Graphics.DrawImage(bm, p);
			}
		}

		public bool PlcHasChanged
		{
			get { return plc_has_changed; }
		}
		public Dictionary<PlcAttributs, PLC> ChangedPlcs
		{
			get { return plcs; }
		}
	
	}
}
