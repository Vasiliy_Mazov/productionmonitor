using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Production Monitor Collector")]
[assembly: AssemblyDescription("PLC Data acquisition module")]
[assembly: AssemblyCompany("Optimum Trend")]
[assembly: AssemblyProduct("Production Monitor")]
[assembly: AssemblyCopyright("Copyright � 2010-2014 by Alexey Piskunovich")]
[assembly: AssemblyTrademark("")]
[assembly: CLSCompliant(true)]

//The following GUID is for the ID of the typelib if this project is exposed to COM
//[assembly: Guid("3122F553-47C8-4CB3-8D4B-C4876AB6DDC0")]
[assembly: ComVisibleAttribute(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:

[assembly: AssemblyVersion("3.0.1031.0")]
