﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.Win32;
using ProductionMonitor;

namespace ProductionMonitor.Collector
{
	public class Collector
	{
		#region | fields                |

		static internal bool _extended_log = true;

		private int _timer_mins;
		private int _timer_ticks = 0;
		private System.Timers.Timer _timer;
		private bool _routine_inprocess = false;
		private CollectorSettings _settings;
		private Dictionary<string, PLC> _PLCs;
		private Dictionary<string, SerialPort> _COMs;
		private PLC _currplc;
		private CollectorConfig _config;
		private RegistryKey _regkey;

		private DataSet ds;
		private DataTable dtb_events;						//
		private DataTable dtb_product;					//
		private DataTable dtb_log = new DataTable("Log");
		private bool makeownlog = false;

		private SqlConnection conn;
		private SqlDataAdapter da_events;
		private SqlDataAdapter da_product;
		private SqlDataAdapter da_gaps;
		private SqlDataAdapter da_rozliv;
		private SqlDataAdapter da_eventCounter;
		private Log _log;

		public struct RegState
		{
			public int COM_Available;
			public int PLC_Available;
			public int SQL_Available;
		}
		private RegState LastState, CurrentState;
		private string current_operation = "Init";
		public event EventHandler LogEntry;
		//private bool shouldtoexit = false;
		//Queue<bool> sqlresults = new Queue<bool>(10);
		private SuccessLog dbconnectlog = new SuccessLog(3, 1);
		private List<PlcUnit> board_units = new List<PlcUnit>();
		private Tasks board;
		private bool needtoinit = false;


		#endregion
		
		public Collector(bool init)
		{
			needtoinit = init;
			// Create a timer with a one second interval.
			_timer = new System.Timers.Timer(1001);
			_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);

			_PLCs = new Dictionary<string, PLC>();
			_COMs = new Dictionary<string, SerialPort>();
			//_currport = new System.IO.Ports.SerialPort();
			_log = new Log("PM_Collector");

		}

		#region | private methods       |
		
		private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)		// тикает каждую секунду
		{
			// тикает каждую секунду (если точнее то каждую 1001 милисекунду)
			
			if (needtoinit) {
				_timer.Stop();
				Initialize();
				needtoinit = false;
				_timer.Start();
				return;
			}

			// проверяем флаг для предотвращения повторного вызова
			if (_routine_inprocess)
				return;

			// если в настройках стоит работа по таймеру
			if (_timer_mins > 0) {
				bool hastail = false;
				foreach (PLC p in PLCs.Values)
					if (p.HasTail) {
						hastail = true;
						break;
					}
				if (++_timer_ticks >= _timer_mins * 60 || hastail) {
					Routine();
					return;
				}
			} else
				_timer_ticks = 0;
			
			MakeLog("Idle");

		}
		private void MakeLog(object sender, string logentry)
		{
			string msg = "";

			if (sender == null) {
				msg = logentry;
			} else if (sender is string) {
				msg = sender.ToString() + " " + logentry;
			} else if (sender is Config) {
				msg = "Config: " + logentry;
			} else if (sender is Tasks) {
				msg = "Tasks: " + logentry;
			} else if (sender is System.IO.Ports.SerialPort) {
				msg = (sender as System.IO.Ports.SerialPort).PortName + ": " + logentry;
			} else if (sender is PLC) {
				PLC plc = sender as PLC;
				msg = plc.Name + ": " + logentry;
			} else if (sender is PlcUnit) {
				PlcUnit u = sender as PlcUnit;
				msg = string.Format("{0} ({1}A{2}D{3}):  {4}", new string[] { u.GetLongUnitName(false), u.HostPort, u.PlcAddress.ToString(), u.PlcInput.ToString(), logentry });
			}

			current_operation = msg;
			DateTime dte = DateTime.Now;
			if (msg != "Idle" && msg != "")
				dte = _log.AddEntry(msg);

			if (LogEntry != null)
				LogEntry(null, new LogEntryArgs(msg, dte));
		}
		private void MakeLog(string msg)
		{
			MakeLog(null, msg);
		}
		private void InitTb()
		{
			ds = new DataSet();

			dtb_events = new DataTable("tbEvents");
			DataColumn col = dtb_events.Columns.Add("EventID", typeof(Int32));
			col.AutoIncrement = true;
			col.AutoIncrementSeed = 0;
			col.AutoIncrementStep = 1;
			dtb_events.Columns.Add("UnitID", typeof(Int16));
			dtb_events.PrimaryKey = new DataColumn[] { dtb_events.Columns["UnitID"], dtb_events.Columns["EventID"] }; 

			dtb_product = new DataTable("tbProduct");
			dtb_product.Columns.Add("Date", typeof(DateTime));
			dtb_product.Columns.Add("UnitID", typeof(Int16));
			dtb_product.Columns.Add("Crop", typeof(Int16));
			dtb_product.Columns.Add("Seconds", typeof(byte));
			dtb_product.Columns.Add("EventID", typeof(Int32));
			dtb_product.PrimaryKey = new DataColumn[] { dtb_product.Columns["Date"], dtb_product.Columns["UnitID"] }; 

			ds.Tables.Add(dtb_events);
			ds.Tables.Add(dtb_product);
			DataRelation rel = ds.Relations.Add("event_product", 
				new DataColumn[] { dtb_events.Columns["EventID"], dtb_events.Columns["UnitID"]},
				new DataColumn[] { dtb_product.Columns["EventID"], dtb_product.Columns["UnitID"]});
			rel.ChildKeyConstraint.UpdateRule = Rule.Cascade;
			// нужна для обновления в случае использования отдельной базы данных для каждой линии


			//~~~~~~~~~~~~~~~~~ DataAdapters to shared database ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			da_events = new SqlDataAdapter();
			da_events.InsertCommand = new SqlCommand("INSERT tbEvents(LogDtID) VALUES(NULL) SET @Id = SCOPE_IDENTITY()");
			SqlParameter par = da_events.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 4, "EventID");
			par.Direction = ParameterDirection.Output;

			da_product = new SqlDataAdapter();
			da_product.InsertCommand = new SqlCommand("INSERT INTO tbProduct(Date, UnitID, Crop, Seconds, EventID) VALUES (@p1, @p2, @p3, @p4, @p5)");
			da_product.InsertCommand.Parameters.Add("@p1", SqlDbType.SmallDateTime, 4, "Date");
			da_product.InsertCommand.Parameters.Add("@p2", SqlDbType.SmallInt, 4, "UnitID");
			da_product.InsertCommand.Parameters.Add("@p3", SqlDbType.SmallInt, 4, "Crop");
			da_product.InsertCommand.Parameters.Add("@p4", SqlDbType.TinyInt, 4, "Seconds");
			da_product.InsertCommand.Parameters.Add("@p5", SqlDbType.Int, 4, "EventID");

			//da_product.UpdateCommand = new SqlCommand("UPDATE tbProduct SET Crop=@p3, Seconds=@p4 WHERE Date=@p1 AND UnitID=@p2", conn);
			//da_product.UpdateCommand.Parameters.Add("@p1", SqlDbType.SmallDateTime, 4, "Date");
			//da_product.UpdateCommand.Parameters.Add("@p2", SqlDbType.SmallInt, 4, "UnitID");
			//da_product.UpdateCommand.Parameters.Add("@p3", SqlDbType.SmallInt, 4, "Crop");
			//da_product.UpdateCommand.Parameters.Add("@p4", SqlDbType.TinyInt, 4, "Seconds");

			da_product.UpdateBatchSize = 1000;
			da_product.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

			//~~~~~~~~~~~~~~~~~ DataAdapters to single database ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			da_gaps = new SqlDataAdapter();
			//da_gaps.InsertCommand = new SqlCommand("INSERT l_gaps(Brand) VALUES((SELECT max(ID) FROM l_brands)) SET @Id = SCOPE_IDENTITY()");
			da_gaps.InsertCommand = new SqlCommand("c_insertgap");
			da_gaps.InsertCommand.CommandType = CommandType.StoredProcedure;
			da_gaps.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
			par = da_gaps.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 4, "EventID");
			par.Direction = ParameterDirection.Output;

			da_rozliv = new SqlDataAdapter();
			da_rozliv.InsertCommand = new SqlCommand("INSERT INTO l_rozliv(DT, Bottles, Seconds, Gap) VALUES (@p1, @p3, @p4, @p5)");
			da_rozliv.InsertCommand.Parameters.Add("@p1", SqlDbType.SmallDateTime, 4, "Date");
			da_rozliv.InsertCommand.Parameters.Add("@p3", SqlDbType.SmallInt, 4, "Crop");
			da_rozliv.InsertCommand.Parameters.Add("@p4", SqlDbType.TinyInt, 4, "Seconds");
			da_rozliv.InsertCommand.Parameters.Add("@p5", SqlDbType.Int, 4, "EventID");

			da_rozliv.UpdateBatchSize = 100;
			da_rozliv.InsertCommand.UpdatedRowSource = UpdateRowSource.None;

			//~~~~~~~~~~~~~~~~~ DataAdapters to single database ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			da_eventCounter = new SqlDataAdapter();
			da_eventCounter.InsertCommand = new SqlCommand("INSERT INTO e_events(DT, Id, Value) VALUES (@p1, @p2, @p3)");
			da_eventCounter.InsertCommand.Parameters.Add("@p1", SqlDbType.DateTime, 8, "DT");
			da_eventCounter.InsertCommand.Parameters.Add("@p2", SqlDbType.Int, 4, "Id");
			da_eventCounter.InsertCommand.Parameters.Add("@p3", SqlDbType.Int, 4, "Value");

			da_eventCounter.UpdateBatchSize = 100;
			da_eventCounter.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
			da_eventCounter.ContinueUpdateOnError = true;
		}
		private SerialPort GetCOM(string ComPortName)
		{
			//MakeLog("COM port connection...");

			SerialPort p = null;
			if (_COMs.ContainsKey(ComPortName)) {
				p = _COMs[ComPortName];
				try {
					if (!p.IsOpen) {
						p.Open();
						MakeLog(p, "serial port is opened");
					}
				} catch (Exception ex) {
					MakeLog(p, ex.Message);
				}
				return p;
			}

			string[] ports = System.IO.Ports.SerialPort.GetPortNames();
			foreach (string port in ports) {
				try {
					if (ComPortName == port) {
						p = new SerialPort(port);
						p.BaudRate = _settings.BaudRate;
						p.ReadTimeout = _settings.ComTimeOut;
						p.WriteTimeout = _settings.ComTimeOut;
						p.DataBits = 8;
						p.Parity = System.IO.Ports.Parity.None;
						p.StopBits = System.IO.Ports.StopBits.One;
						p.Handshake = System.IO.Ports.Handshake.None;
						p.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(comport_ErrorReceived);
						p.DataReceived += new SerialDataReceivedEventHandler(comport_DataReceived);
						_COMs.Add(port, p);
						if (!p.IsOpen) 
							p.Open();
						MakeLog(p, "serial port is opened");
						break;
					}
				} catch (Exception ex) {
					MakeLog(p, ex.Message);
				}
			}
			return p;
		}
		private void InitReg()
		{
			MakeLog("Init registry...");
			try {
				_regkey = CollectorSettings.GetRegistryBranch();
				if (_regkey != null) {
					_regkey.SetValue("", "Production Monitor Collector");		
					_regkey.SetValue("Program_Version", Application.ProductVersion);
					_regkey.SetValue("Last_Running_UTC", DateTime.Now.ToUniversalTime().ToString("yyyy.MM.dd HH:mm"));
					_regkey.SetValue("Program_Path", Application.StartupPath);
					_regkey.SetValue("Availability_PLC", 0);
					_regkey.SetValue("Availability_SQL", 0);
					_regkey.SetValue("Availability_COM", 0);
					_regkey.SetValue("User", Environment.UserDomainName + "\\" + Environment.UserName);
					LastState.COM_Available = 0;
					LastState.PLC_Available = 0;
					LastState.SQL_Available = 0;
					CurrentState.PLC_Available = 0;
				}
			} catch (Exception ex) {
				MakeLog("InitReg() Exception:" + ex.Message);
			}
			UpdateReg();
		}
		private void UpdateReg()
		{
			try {
				if (_regkey != null) {
					int comstate = 0;
					foreach (PLC plc in PLCs.Values) 
						if (plc.CommPort.IsOpen)
							comstate++;
					if (LastState.COM_Available != comstate) {
						LastState.COM_Available = comstate;
						_regkey.SetValue("Availability_COM", comstate);
					}

					if (LastState.PLC_Available != CurrentState.PLC_Available) {
						LastState.PLC_Available = CurrentState.PLC_Available;
						_regkey.SetValue("Availability_PLC", CurrentState.PLC_Available);
					}
					CurrentState.PLC_Available = 0;

					if (LastState.SQL_Available != dbconnectlog.SuccessCount) {
						LastState.SQL_Available = dbconnectlog.SuccessCount;
						_regkey.SetValue("Availability_SQL", dbconnectlog.SuccessCount);
					}

				}
			} catch (Exception ex) {
				MakeLog("Update registry exception: " + ex.Message);
			}
		}
		private void comport_ErrorReceived(object sender, System.IO.Ports.SerialErrorReceivedEventArgs e)
		{
			//MakeLog(sender, "Serial port error received: " + e.EventType.ToString());
		}
		private void comport_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			// процедуру можно использовать для прослущивания COM порта 'Prog'
			// контроллера ADAM, для этого достаточно задать скорость порта 57600 
			// и раскомментировать этот метод, а вызывать его надо только для этих целей, 
			// т.е. если этот ивент будет вызываться и обрабатываться (вызываться функция .ReadLine())
			// то функционирование стантдартрой операции чтения будет нарушено

			//SerialPort p = sender as SerialPort;
			//try {
			//   string l = p.ReadLine();
			//   l = l.Replace("\r", "");
			//   l = l.Replace("\n", "");
			//   MakeLog(sender, l);
			//} catch { }
		}
		private void GetProduction(PLC plc)
		{
			if (plc.Product == null)
				return;

			DataRow[] DR;
			int currevent;
			int curr_row_state;
			DateTime curr_row_date;
			DataRow dr_new_event, dr_new_product;
			int added_to_product = 0, added_to_event = 0;
			
			////foreach (PlcUnit un in plc.PlcUnits.Values)
			////   Debug.WriteLine("un.ID: " + un.ID.ToString() + "  un.LastDBRecord: " + un.LastDBRecord.ToString());

			PlcUnit u;
			foreach (int key in plc.PlcUnits.Keys) {
				u = plc.PlcUnits[key];
				if (u.DbState != PlcUnit.DbRel.OwnDB)
					continue;
				if (u.Kind != UnitKind.Line)
					continue;
				if (!u.DbInsertLog.ThereIsSuccess) {
					u.Initialize();
					continue;
				}
				DR = plc.Product.Select("UnitID=" + key.ToString(), "Date");
				foreach (DataRow dr in DR) {
					curr_row_date = Convert.ToDateTime(dr["Date"]);
					if (curr_row_date > plc.CurrentPLCTime) {
						MakeLog(plc, "Current plc datetime exceeds the current row datetime. CurrentPLCTime: " + plc.CurrentPLCTime.ToString() + "; curr_row_date: " + curr_row_date.ToString());

						continue;	// проверка на всякий случай
					}
					curr_row_state = Convert.ToInt32(dr["Crop"]) == 0 ? 0 : 1;
					if (u.LastDBRecord < curr_row_date) {
						if (u.NeedToAddFirstRow && u.LastState == curr_row_state && u.LastDBRecord.AddMinutes(1) == curr_row_date) {
							// не было разрыва во времени и не поменялось состояние линии 
							dr_new_event = dtb_events.NewRow();
							dr_new_event["UnitID"] = u.ID;
							dr_new_event["EventID"] = u.LastEventID;
							//if (!dtb_events.Rows.Contains(new object[] { dr_new_event["UnitID"], dr_new_event["EventID"] })) {
							dtb_events.Rows.Add(dr_new_event);
							dr_new_event.AcceptChanges();		// это значение уже есть в базе данных, поэтому не обновлять
							dr["EventID"] = u.LastEventID;
							//}
							u.NeedToAddFirstRow = false;
						} else if (u.LastDBRecord.AddMinutes(1) != curr_row_date
									|| u.LastState != curr_row_state) {
							// был разрыв во времени или поменялось состояние линии > следует добавить новый Event
							dr_new_event = dtb_events.NewRow();
							dr_new_event["UnitID"] = u.ID;
							dtb_events.Rows.Add(dr_new_event);
							currevent = Convert.ToInt32(dr_new_event["EventID"]);
							u.LastEventID = currevent;
							u.LastState = curr_row_state;
							dr["EventID"] = currevent;
							added_to_event++;
							u.NeedToAddFirstRow = false;
						} else
							dr["EventID"] = u.LastEventID;

						u.LastDBRecord = curr_row_date;
						dr_new_product = dtb_product.NewRow();
						dr_new_product.ItemArray = dr.ItemArray;
						dtb_product.Rows.Add(dr_new_product);
						added_to_product++;
					} 
				}
			}


			if (added_to_product == 0 && plc.LastReadoutOk) {
				//todone: в случае если считывается большой объем данных и используется пакетное считывание надо менять значение plc.LastPlcRecord
				//поскольку это свидетельствует о том что был пробел в работе контроллера или было изменено количество счетчиков в контроллере
				MakeLog(plc, "No one records have been added");
				plc.OmissionCounter++;
			} else if (added_to_product == 0) {
				//если ничего не добавлено и сюда попали значит LastReadoutOk = false
				//поэтому OmissionCounter не меняем, в следующий раз попробуем еще раз прочитать
				MakeLog(plc, string.Format("Will try to read another one time"));
			} else {
				MakeLog(plc, string.Format("Added to events table: {0}; to product table: {1}", added_to_event, added_to_product));
				plc.OmissionCounter = 0;			
			}

			// добавляем день чтобы предупредить возможную разницу часовых поясов
			plc.LastDbRecord = DateTime.Now.AddDays(1);
			foreach (PlcUnit un in plc.PlcUnits.Values) {
				if (plc.LastDbRecord > un.LastDBRecord && un.DbState == PlcUnit.DbRel.OwnDB)
					plc.LastDbRecord = un.LastDBRecord;
				if (_extended_log) {
					MakeLog(plc, "un.ID: " + un.ID.ToString() + "  un.LastDBRecord: " + un.LastDBRecord.ToString());
					MakeLog(plc, "LastPlcRecord: " + plc.LastDbRecord.ToString());
				}
			}

			plc.Product.AcceptChanges();
			plc.Product.Clear();
		}
		private void GetEvents(PLC plc)
		{
			PlcUnit u;
			foreach (int key in plc.PlcUnits.Keys) {
				u = plc.PlcUnits[key];
				if (u.Kind != UnitKind.EventCounter)
					continue;


			}
		}
		
		//~~~~~~~ events
		private void _LogEntry(object sender, EventArgs e)
		{
			MakeLog(sender, (e as LogEntryArgs).LogEntry);
		}

		#endregion

		#region | public methods        |

		public void Initialize()
		{
			_routine_inprocess = true;

			// если компьютер только стартанул то не торопимся и ждем три минуты
			if (Environment.TickCount > 0 && Environment.TickCount / 60000 < 3) {
				int startcounter = 12;
				while (startcounter >= 0) {
					MakeLog("Waiting for system start up...");
					System.Threading.Thread.Sleep(10000);
					startcounter--;
				}
			}

			MakeLog("Collector initialization....");
			InitTb();
			InitReg();

			#region | settings          |
			MakeLog("Load program settings...");
			_settings = new CollectorSettings();
			_timer_mins = _settings.Mins;		// if (timerMins==0) then read out by request only
			_log.IsForced = _settings.MakeLogs;
			_extended_log = _settings.ExtendedLog;

			#endregion

			#region | configuration     |
			MakeLog("Load configuration...");
			try {
				_config = new CollectorConfig();
				_config.LogEntry += new EventHandler(_LogEntry);
				_config.Initialize();
			} catch (Exception ex) {
				MakeLog("Exception: Configuration file was not loaded. " + ex.Message);
				return;
			}
			#endregion

			#region | database          |
			if (_config.CollectorSqlConnection != "")
				conn = new SqlConnection(_config.CollectorSqlConnection);
			else if (!_config.HasUnitSqlConnection) {
				MakeLog("There is no available SQL connection. May be SQL server is down or should to correct tbUnits.HostName values");
				//return;
			}
			foreach (PlcUnit u in _config.PlcUnits.Values)
				if (u.LineNameFromDB != "") {
					dbconnectlog.AddSuccess();
					break;
				}
			#endregion

			#region | board             |
			board_units.Clear();
			foreach (PlcUnit pu in _config.PlcUnits.Values) {
				if (pu.DbState == PlcUnit.DbRel.WithoutDB)
					continue;
				board_units.Add(pu);
			}

			board = new Tasks(board_units);
			board.LogEntry += new EventHandler(_LogEntry); ;
			board.GetRegSetiings();
			board.Routine();

			#endregion

			InitializeHardware();

			_routine_inprocess = false;

			_timer.Start();
			MakeLog("Collector initialization compleated");
		}
		public void InitializeHardware()
		{
			// очищаем коллекции если это уже не первая инициализация
			foreach (SerialPort p in _COMs.Values) {
				p.Close();
				p.Dispose();
			}
			_PLCs.Clear();
			_COMs.Clear();

			string plckey, comkey;
			// захватываем компорты
			foreach (PlcUnit unit in _config.PlcUnits.Values) {
				comkey = unit.HostPort;
				GetCOM(unit.HostPort);
			}

			// ищем контроллеры по конфигурационному файлу
			foreach (PlcUnit punit in _config.PlcUnits.Values) {
				plckey = punit.HostPort + "_" + punit.PlcAddress.ToString();
				if (_PLCs.ContainsKey(plckey)
					|| punit.DbState == PlcUnit.DbRel.WithoutDB
					|| punit.HostName.ToLower() != Environment.MachineName.ToLower()) {
					continue;
				}
				PLC plc;
				if (_COMs.ContainsKey(punit.HostPort))
					plc = new PLC(_COMs[punit.HostPort], punit.PlcAddress, _settings.TimeAutoCorrection, _config);
				else {
					SerialPort port = new SerialPort(punit.HostPort);
					_COMs.Add(punit.HostPort, port);
					plc = new PLC(port, punit.PlcAddress, true, _config);
				}
				plc.LogEntry += new EventHandler(_LogEntry);
				_PLCs.Add(plckey, plc);
				_currplc = plc;
				plc.Initialize();
			}

		}
		public void Routine()
		{
			// сбрасываем учет времени чтобы по завершению начать новый отсчет
			_timer_ticks = 0;

			if (_routine_inprocess) 
				return;

			// Устанавливаем флаг для запрета вызова при незаконченной операции
			_routine_inprocess = true;
			//shouldtoexit = false;

			#region | проверка на наличие ошибок |
			
			if (_config.HasNoAnySqlConnection){
				MakeLog("Try to initialize the collector another one time cause there is no any available SQL connection");
				Initialize();
				_routine_inprocess = false;
				return;
			}
			
			// если при предыдущей инициализации не достучались до баз данных то инициализируемся заново
			if (_PLCs.Count == 0){
				MakeLog("Another one time Initialization");
				Initialize();
				_routine_inprocess = false;
				return;
			}

			// Проверяем нет ли какой фатальной ошибки при сохранении в базу данных 
			int initcounter = 0, unitcounter = 0;
			foreach (PlcUnit pu in _config.PlcUnits.Values) {
				if (pu.DbState == PlcUnit.DbRel.WithoutDB)
					continue;
				unitcounter++;
				if (pu.DbInsertLog.IsDead || pu.NeedToInit) {
					MakeLog(pu, "Another one time InitializeHardware");
					pu.Initialize();
					initcounter++;
				}
			}
			
			// если инициализировали все юниты без изключения
			if (unitcounter == initcounter) {
				Initialize();
				_routine_inprocess = false;
				return;
			}
			
			// если инициализировали хотя бы один юнит
			if (initcounter > 0)
				InitializeHardware();
			#endregion


			foreach (PLC plc in _PLCs.Values) {
				_currplc = plc;
				if (GetCOM(plc.CommPort.PortName) == null)
					continue;
				try {
					if (!plc.CurrentStateOk) {
						MakeLog(plc, "Check PLC configuration... ");
						plc.CheckCurrentTime();
						if (plc.LastReadoutOk) plc.CheckCounters();
						if (plc.LastReadoutOk) CurrentState.PLC_Available++;
					}
					// в один блок if не объединять!!! т.е. состояние может измениться после запроса к PLC
					if (plc.CurrentStateOk) {
						MakeLog(plc, "Download... ");
						//plc.ReadRows(1500);
						plc.ReadFrom();
						if (plc.LastReadoutOk) CurrentState.PLC_Available++;
						GetProduction(plc);
						GetEvents(plc);
					}
				} catch (Exception ex) {
				   MakeLog(plc, "Routine error: " + ex.ToString());
				}
				SaveEventsToDB(plc);
			}
			SaveToDB();
			UpdateReg();
			_routine_inprocess = false;
		}
		public void SaveToDB()
		{
			string flt = "", fltsingle = "";
			int inserted1 = 0, inserted2 = 0;
			DataRow[] DR;
			foreach (PlcUnit pu in _config.PlcUnits.Values) {
				if (pu.DbState == PlcUnit.DbRel.OwnDB) {
					flt += pu.ID.ToString() + ",";

					fltsingle = "UnitID=" + pu.ID.ToString();

					// if there is no the new records > need to exit
					if (dtb_product.Select(fltsingle, "", DataViewRowState.Added).Length == 0
						&& dtb_events.Select(fltsingle, "", DataViewRowState.Added).Length == 0) {
						MakeLog(pu, "There is nothing to update into db...");
						continue;
					}

					MakeLog(pu,"Data transfer to db...");

					Application.DoEvents();
					//SqlTransaction trsingle = null;
					SqlConnection connsingle = new SqlConnection(pu.Connection);
					try {
						if (connsingle.State != ConnectionState.Open)
							connsingle.Open();

						//trsingle = connsingle.BeginTransaction();
						da_gaps.InsertCommand.Connection = connsingle;
						da_rozliv.InsertCommand.Connection = connsingle;

						//da_gaps.InsertCommand.Transaction = trsingle;
						//da_rozliv.InsertCommand.Transaction = trsingle;
						//da_gaps.ContinueUpdateOnError = true;
						//da_rozliv.ContinueUpdateOnError = true;

						inserted1 = da_gaps.Update(dtb_events.Select(fltsingle, "", DataViewRowState.Added));
						inserted2 = da_rozliv.Update(dtb_product.Select(fltsingle, "", DataViewRowState.Added));
						MakeLog(pu, string.Format("Inserted to events: {0}; to product: {1}", inserted1, inserted2));
						//trsingle.IsolationLevel == IsolationLevel.Unspecified;
						//trsingle.Commit();
						//MakeLog(pu, "Insertion is commited");

						DR = dtb_product.Select(fltsingle, "[Date] DESC");
						if (DR.Length>0 )
							pu.LastEventID = (int)DR[0]["EventID"];
						
						pu.DbInsertLog.AddSuccess();
					} catch (SqlException sqlex) {
						////if (trsingle != null) trsingle.Rollback();
						pu.DbInsertLog.AddFault();
						string msg;
						if (sqlex.Message.Length > 3000)
							msg = sqlex.Message.Remove(3000);
						else
							msg = sqlex.Message;
						MakeLog(pu, "DB update exception: " + msg);
					} catch (Exception ex) {
						////if (trsingle != null) trsingle.Rollback();
						string msg;
						if (ex.Message.Length > 3000)
							msg = ex.Message.Remove(3000);
						else
							msg = ex.Message;
						MakeLog(pu, "DB update exception: " + msg);
						if (pu.DbInsertLog.LastThreeIsFault)
							pu.NeedToInit = true;
					} finally {
						////if (trsingle != null) trsingle.Dispose();
						dbconnectlog.AddResult(connsingle.State == ConnectionState.Open);
						connsingle.Dispose();
					}
				}
			}
			if (flt != "") {
				flt = flt.Remove(flt.Length - 1);
				flt = "UnitID NOT IN(" + flt + ")";
			}

			#region | Вариант с использованием единой базы данных |
			if (conn != null) {
				// if there is no the new records > need to exit
				if (dtb_product.Select(flt, "", DataViewRowState.Added).Length == 0
					&& dtb_events.Select(flt, "", DataViewRowState.Added).Length == 0) {
					return;
				}
				SqlTransaction tr = null;
				try {
					if (conn.State != ConnectionState.Open)
						conn.Open();

					MakeLog("Data uploading to SQL database...");

					//int inserted1 = 0, inserted2 = 0;
					Application.DoEvents();
					tr = conn.BeginTransaction();
					da_events.InsertCommand.Connection = conn;
					da_product.InsertCommand.Connection = conn;


					da_events.InsertCommand.Transaction = tr;
					da_product.InsertCommand.Transaction = tr;
					//da_events.ContinueUpdateOnError = true;
					//da_product.ContinueUpdateOnError = true;

					inserted1 += da_events.Update(dtb_events.Select(flt, "", DataViewRowState.Added));
					inserted2 += da_product.Update(dtb_product.Select(flt, "", DataViewRowState.Added));
					MakeLog(string.Format("Inserted to events table: {0}; to product table: {1}", inserted1, inserted2));
					tr.Commit();
					MakeLog("Insertion is commited");

					foreach (PlcUnit u in _config.PlcUnits.Values) {
						DR = dtb_product.Select("UnitID=" + u.ID.ToString(), "[Date] DESC");
						if (DR.Length > 0)
							u.LastEventID = (int)DR[0]["EventID"];
					}

				} catch (Exception ex) {
					if (tr != null) tr.Rollback();
					MakeLog("DB update error: " + ex.ToString());
				} finally {
					if (tr != null) tr.Dispose();
					// for registry
					dbconnectlog.AddResult(conn.State == ConnectionState.Open);
				}
			}
			#endregion

			// обновление номера последнего события по результатам сохранения в db
			foreach (PLC plc in _PLCs.Values) {
				if (plc.PlcUnits == null) continue;
				foreach (PlcUnit un in plc.PlcUnits.Values) {
					DR = dtb_product.Select("UnitID=" + un.ID.ToString(), "Date DESC");
					if (DR.Length > 0) un.LastEventID = Convert.ToInt32(DR[0]["EventID"]);
				}
			}

			// чистка таблицы на случай если слишком много записей накопилось
			if (dtb_product.Select("", "", DataViewRowState.Added).Length == 0 && dtb_product.Rows.Count > 50000) {
				dtb_product.Rows.Clear();
				if (dtb_events.Select("", "", DataViewRowState.Added).Length == 0 && dtb_events.Rows.Count > 50000)
					dtb_events.Rows.Clear();
			}
		}
		public void SaveEventsToDB(PLC plc)
		{
			if (plc.HasTail) {
				MakeLog(plc, "EventCnt: db save was postponed due to there is data to upload from the PLC ...");
				return;
			}

			int inserted = 0;

			// if there is no the new records > need to exit
			if (plc.EventCounter.Select("", "", DataViewRowState.Added).Length == 0) {
				MakeLog(plc, "EventCnt: there is nothing to update into db ...");
				return;
			}

			MakeLog(plc, "EventCnt: data transfer to db...");

			Application.DoEvents();
			SqlConnection connsingle = new SqlConnection(_config.BaseUnitConnection);
			try {
				if (connsingle.State != ConnectionState.Open)
					connsingle.Open();

				da_eventCounter.InsertCommand.Connection = connsingle;
				for (int n = 0; n < 10; n++) {
					DataRow[] DR = plc.EventCounter.Select("", "DT", DataViewRowState.Added);
					if (DR.Length == 0)
						break;
					List<DataRow> rows = new List<DataRow>(100);
					int cnt = 0;
					foreach (DataRow drv in DR) {
						rows.Add(drv);
						if (cnt++ >= 99)
							break;
					}
					inserted += da_eventCounter.Update(rows.ToArray());
				}

				MakeLog(plc, string.Format("EventCnt: inserted to db - {0}", inserted));

				plc.DbInsertLog.AddSuccess();
			} catch (SqlException sqlex) {
				plc.DbInsertLog.AddFault();
				string msg;
				if (sqlex.Message.Length > 3000)
					msg = sqlex.Message.Remove(3000);
				else
					msg = sqlex.Message;
				MakeLog(plc, "EventCnt: DB update exception: " + msg);
			} catch (Exception ex) {
				string msg;
				if (ex.Message.Length > 3000)
					msg = ex.Message.Remove(3000);
				else
					msg = ex.Message;
				MakeLog(plc, "EventCnt: DB update events exception: " + msg);
			} finally {
				dbconnectlog.AddResult(connsingle.State == ConnectionState.Open);
				connsingle.Dispose();
			}

			// чистка таблицы на случай если слишком много записей накопилось
			if (plc.EventCounter.Select("", "", DataViewRowState.Added).Length == 0 && plc.EventCounter.Rows.Count > 5000) {
				DataRow[] DR = plc.EventCounter.Select("", "[DT] DESC");
				if (DR.Length > 0) {
					
					DateTime dt = (DateTime)DR[0]["DT"];
					int id = (int)DR[0]["Id"];
					int val = (int)DR[0]["Value"];
					
					plc.EventCounter.Clear();

					DataRow dr = plc.EventCounter.NewRow();
					dr["DT"] = dt;
					dr["Id"] = id;
					dr["Value"] = val;

					plc.EventCounter.Rows.Add(dr);
					plc.EventCounter.AcceptChanges();
				}
			}
		}
		public void DisableRoutine()
		{
			if (_timer != null)
				_timer.Enabled = false;
		}
		public void EnableRoutine()
		{
			if (_timer != null)
				_timer.Enabled = true;
		}
		public void ShouldToExit()
		{
			//shouldtoexit = true;
			if (_settings.MakeLogs)
				MakeLog(false);
			if (_currplc != null) _currplc.ShouldToExit();
		}
		public void MakeLog(bool ToShow)
		{
			string strT = new string('_', 20);
			string strTT = new string('=', 20);


			StringBuilder sb = new StringBuilder();
			sb.AppendLine(strT + " SETTINGS ");
			sb.AppendLine(String.Format("{0,-20} {1}", "Mins", _settings.Mins.ToString()));
			sb.AppendLine(String.Format("{0,-20} {1}", "TimeAutoCorrection", _settings.TimeAutoCorrection.ToString()));
			sb.AppendLine("\r\n");

			if (_COMs != null) {
				foreach (SerialPort com in _COMs.Values) {
					sb.AppendLine(strT + " " + com.PortName + " state:");
					sb.AppendLine(String.Format("{0,-20} {1}", "IsOpen", com.IsOpen.ToString()));
					sb.AppendLine(String.Format("{0,-20} {1}", "BaudRate", com.BaudRate.ToString()));
					sb.AppendLine(String.Format("{0,-20} {1}", "WriteTimeout", com.WriteTimeout.ToString()));
					sb.AppendLine(String.Format("{0,-20} {1}", "ReadTimeout", com.ReadTimeout.ToString()));
				}
			}
			if (_PLCs != null) {
				foreach (PLC plc in _PLCs.Values) {
					sb.AppendLine(strT + " A" + plc.NetworkAddress.ToString() + " state:");
					sb.AppendLine(String.Format("{0,-20} {1}", "LastReadoutOk", plc.LastReadoutOk.ToString()));
					sb.AppendLine(String.Format("{0,-20} {1}", "CountersInPLC", plc.CountersInPLC));
					sb.AppendLine(String.Format("{0,-20} {1}", "Rows", plc.Rows));
					sb.AppendLine(String.Format("{0,-20} {1}", "CurrentPLCTime", plc.CurrentPLCTime));
					sb.AppendLine(String.Format("{0,-20} {1}", "ReadOuts", plc.ReadOuts));
					sb.AppendLine(String.Format("{0,-20} {1}", "Errors", plc.Errors));
					sb.AppendLine(String.Format("{0,-20} {1}", "CurrentStateInfo", plc.CurrentStateInfo));
				}
			}

			_log.Save(sb.ToString(), ToShow);

		}

		#endregion

		#region | properties            |
		
		public CollectorSettings Settings
		{
			get { return _settings; }
			set { _settings = value; }
		}
		public Dictionary<string, PLC> PLCs
		{
			get { return _PLCs; }
		}
		public Dictionary<string, SerialPort> COMs
		{
			get { return _COMs; }
		}
		public DataTable ProductTable
		{
			get { return dtb_product; }
		}
		public int TimerMins
		{
			get { return _timer_mins; }
			set {
				_settings.Mins = value;
				_timer_mins = value; 
			}
		}
		public int TimerTicks
		{
			get
			{
				return TimerMins * 60 - _timer_ticks;
			}
		}
		public string CurrentOperation
		{
			get {	return current_operation; }
		}
		public bool InProcess
		{
			get { return _routine_inprocess; }
		}
		public bool HasNoAnySqlConnection
		{
			get { return _config == null || _config.HasNoAnySqlConnection; } 
		}
		public bool SqlIsAvailable
		{
			get { return dbconnectlog.ThereIsSuccess; }
		}
		public bool DoLog
		{
			set { makeownlog = value; }
		}
		public Log Log
		{
			get { return _log; }
		}
		public bool TimeAutoCorrection
		{
			get { return _settings.TimeAutoCorrection; }
			set
			{
				_settings.TimeAutoCorrection = value;
				foreach (PLC plc in PLCs.Values) 
					plc.TimeAutoCorrection = value;
			}
		}

		#endregion
	}
}
