﻿using System.Diagnostics;
using System;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Collections.Generic;
using ProductionMonitor;

namespace ProductionMonitor.Collector
{
	
	public class CollectorConfig: Config
	{
		#region | fields                |

		private Dictionary<int, PlcUnit> plcunits = new Dictionary<int, PlcUnit>();
		private string collector_sql_connection = "";
		private bool has_unit_sql_connection;

		public event EventHandler LogEntry;

		#endregion

		public CollectorConfig()
			: base() { }

		public void Initialize()
		{
			plcunits.Clear();

			////foreach (Unit un in base.Units.Values)
			////   Debug.WriteLine("un.ID: " + un.ID.ToString());

			foreach (Unit u in base.Units.Values) {
				PlcUnit pu = new PlcUnit(u.ID, u.ParentID, u.LineName, u.Connection);
				if (u.Connection != "") {
					pu.LogEntry += new EventHandler(pu_LogEntry);
					pu.Initialize();
					if (pu.DbState == PlcUnit.DbRel.OwnDB)
						has_unit_sql_connection = true;
				}
				if (pu.DbState != PlcUnit.DbRel.WithoutDB)
					plcunits.Add(u.ID, pu);
			}

			////foreach (PlcUnit un in plcunits.Values)
			////   Debug.WriteLine("un.ID: " + un.ID.ToString() + "  un.LastDBRecord: " + un.LastDBRecord.ToString());

			MakeLog("Units count in configuration file: " + plcunits.Count.ToString());

			InitializeEventCounters();
		}

		public void InitializeEventCounters()
		{
			SqlConnection cn = null;
			SqlCommand cmd = null;
			try {
				cn = new SqlConnection(base.BaseUnitConnection + ";Connect Timeout=5");
				cmd = new SqlCommand("SELECT * FROM e_cfg ORDER BY LineId " +
					"SELECT max(DT) DT, [Id] FROM e_events GROUP BY [Id]", cn);
				cn.Open();
				if (cn.State == ConnectionState.Open) {
					Debug.WriteLine(cn.State.ToString());
					using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)) {
						DataSet dts = new DataSet();
						dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "t1", "t2" });
						DataTable tb1 = dts.Tables["t1"];
						DataTable tb2 = dts.Tables["t2"];

						DataTable tbLinesId = tb1.DefaultView.ToTable("ids", true, "LineId");
						foreach (DataRow row in tbLinesId.Rows) {
							int id = (int)row[0];
							PlcUnit pu = new PlcUnit(id);

							pu.Initialize(tb1, tb2);
							if (pu.DbState == PlcUnit.DbRel.OwnDB)
								has_unit_sql_connection = true;

							plcunits.Add(id, pu);
						}
					}
				}
			} catch (Exception ex) {
				MakeLog(string.Format("InitializeEventCounters()  exception: {0}, DB: {1}", cn.DataSource, cn.Database));
				MakeLog(string.Format("Exception details: {0}", ex.Message));
				//needtoinit = true;
			} finally {
				if (cn != null) cn.Dispose();
				if (cmd != null) cmd.Dispose();
			}
		}


		#region | private functions     |

		private void pu_LogEntry(object sender, EventArgs e)
		{
			if (LogEntry != null)
				LogEntry(sender, e);
		}
		private void MakeLog(object obj, string msg)
		{
			if (LogEntry != null)
				LogEntry(obj, new LogEntryArgs(msg));
		}
		private void MakeLog(string msg)
		{
			if (LogEntry != null)
				LogEntry(this, new LogEntryArgs(msg));
		}

		#endregion

		#region | public properties     |

		public Dictionary<int, PlcUnit> PlcUnits
		{
			get { return plcunits; }
		}
		/// <summary>
		/// В случае использования 
		/// общей (т.е. более одной линии в базе) базы данных
		/// и при наличии нескольких таких баз данных делается попытка
		/// соединения с каждой базой и в случае успеха проверяется значение
		/// таблицы tbUnits поля HostName для каждой линии. Если HostName 
		/// совпадает с именем компьютера то эта база данных и сервер
		/// становятся соединением по умолчанию для коллектора.
		/// В конфиге оно должно быть одно. Если их несколько то берется 
		/// последнее из найденных.
		/// </summary>
		public string CollectorSqlConnection
		{
			get { return collector_sql_connection; }
		}
		public bool HasUnitSqlConnection
		{
			get { return has_unit_sql_connection; }
		}
		public bool HasNoAnySqlConnection
		{
			get { return collector_sql_connection == "" && !has_unit_sql_connection; }
		}

		#endregion

	}

	public class PlcUnit : Unit
	{
		#region | fields                |

		private string _hostName = "";
		private string _hostPort = "COM1";
		private bool _slow = false;
		
		private int _plcNumber = 1;
		private int _plcInput = 1;
		private int _plcQuotientFromDb = 1;
		private int _plcQuotientFromPlc = 1;
		private int _plcDivider = 1;
		private int _timeZoneDiff = 0;
	
		private int _lastEvent = -1;
		private int _lastState = -1;
		private DateTime _lastDbRecord = new DateTime(1, 1, 1);
		private DateTime _lastPlcRecord = new DateTime(1, 1, 1);
		private bool _needToAddFirstRow = false;
		private bool _needToInit = true;
		private SuccessLog _dbInsertLog = new SuccessLog(60);
		public event EventHandler LogEntry;

		public enum DbRel
		{
			WithoutDB,
			SharedDB,
			OwnDB
		}
		private DbRel dbstate = DbRel.WithoutDB;

		private int _lastInsertState;
		private DateTime _LastInsert;

		#endregion

		//~~~~~~~ constructors 
		public PlcUnit(int id)
			: base(id)
		{
			this._plcInput = id;
		}
		public PlcUnit(int unit, int parentunit, string name, string conn)
			: base(unit, parentunit, name, conn) { }

		public void Initialize()
		{
			SqlConnection cn = null;
			SqlCommand cmd = null;
			try {
				cn = new SqlConnection(Connection + ";Connect Timeout=5");
				cmd = new SqlCommand("SELECT * FROM c_cfg ORDER BY id " +
					"SELECT DT, Gap, Bottles FROM l_rozliv WHERE DT =(SELECT max(DT) FROM l_rozliv)", cn);
				cn.Open();
				if (cn.State == ConnectionState.Open) {
					Debug.WriteLine(cn.State.ToString());
					using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)) {
						DataSet dts = new DataSet();
						dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "t1", "t2" });
						DataTable tb1 = dts.Tables["t1"];
						dbstate = DbRel.OwnDB;
						if (tb1 != null) {
//#if DEBUG
//                     if (testattemptcounter++ < 12) throw new Exception("Just a test from PlcUnit.Initialize");
//#endif
							LineNameFromDB = C.GetRowField(tb1, "Name='Line Name'", "Value");
							_hostName = C.GetRowField(tb1, "Name='Host Name'", "Value");
							_hostPort = C.GetRowField(tb1, "Name='Host Port'", "Value");
							_plcNumber = C.GetInt(C.GetRowField(tb1, "Name='PLC Number'", "Value"));
							_plcInput = C.GetInt(C.GetRowField(tb1, "Name='PLC Input'", "Value"));
							_plcQuotientFromDb = C.GetInt(C.GetRowField(tb1, "Name='PLC Quotient'", "Value"));
							_slow = C.GetRowField(tb1, "Name='Line Type'", "Value") == "3";
							int res;
							if (int.TryParse(C.GetRowField(tb1, "Name='PLC Divider'", "Value"), out res))
								_plcDivider = res;

							_hostPort = _hostPort == "" ? "COM1" : _hostPort;
							_plcInput = _plcInput == 0 ? 1 : _plcInput;
							_plcQuotientFromDb = _plcQuotientFromDb < 0 || _plcQuotientFromDb > 4 ? 1 : _plcQuotientFromDb;
							//timezonediff = C.GetInt(C.GetRowField(tb1, "Name='Time Zone Diff'", "", "Value"));
							int.TryParse(C.GetRowField(tb1, "Name='Time Zone Diff'", "Value"), out _timeZoneDiff);

							_needToInit = false;
						}
						int prevevent = _lastEvent;

						DataTable tb2 = dts.Tables["t2"];
						if (tb2 != null) {
							_lastDbRecord = Convert.ToDateTime(C.GetRowField(tb2, "", "DT"));
							_lastEvent = C.GetInt(C.GetRowField(tb2, "", "Gap"));
							_lastState = C.GetInt(C.GetRowField(tb2, "", "Bottles"));
							_lastState = _lastState == 0 ? 0 : 1;
							if (prevevent == -1)
								this._needToAddFirstRow = true;
							MakeLog("Last db record: " + this._lastDbRecord);

						}
						_dbInsertLog.AddSuccess();
					}
				}
			} catch (Exception ex) {
				MakeLog(string.Format("Unit Initialize() SQL server exception: {0}, DB: {1}", cn.DataSource, cn.Database));
				MakeLog(string.Format("Exception details: {0}", ex.Message));
				_needToInit = true;
			} finally {
				if (cn != null) cn.Dispose();
				if (cmd != null) cmd.Dispose();
			}
		}

		public void Initialize(DataTable tb1, DataTable tb2)
		{
			try {

				dbstate = DbRel.OwnDB;
				if (tb1 != null) {
					LineNameFromDB = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Line Name'", ID), "Value");
					_hostName = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Host Name'", ID), "Value");
					_hostPort = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Host Port'", ID), "Value");
					_plcNumber = C.GetInt(C.GetRowField(tb1, string.Format("LineId={0} AND Name='PLC Number'", ID), "Value"));
					_plcInput = C.GetInt(C.GetRowField(tb1, string.Format("LineId={0} AND Name='PLC Input'", ID), "Value"));
					_plcQuotientFromDb = C.GetInt(C.GetRowField(tb1, string.Format("LineId={0} AND Name='PLC Quotient'", ID), "Value"));
					_slow = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Line Type'", ID), "Value") == "3";
					int res;
					if (int.TryParse(C.GetRowField(tb1, string.Format("LineId={0} AND Name='PLC Divider'", ID), "Value"), out res))
						_plcDivider = res;

					_hostPort = _hostPort == "" ? "COM1" : _hostPort;
					_plcQuotientFromDb = _plcQuotientFromDb < 0 || _plcQuotientFromDb > 4 ? 1 : _plcQuotientFromDb;

					int.TryParse(C.GetRowField(tb1, string.Format("LineId={0} AND Name='Time Zone Diff'", ID), "Value"), out _timeZoneDiff);
					if (int.TryParse(C.GetRowField(tb1, string.Format("LineId={0} AND Name='Parent Id'", ID), "Value"), out res))
						ParentID = res;

					Invert = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Invert'", ID), "Value") == "True";
					Kind = UnitKind.EventCounter;
					
					_needToInit = false;
				}
				int prevevent = _lastEvent;

				if (tb2 != null) {
					if (!DateTime.TryParse(C.GetRowField(tb2, "Id=" + ID, "DT"), out _lastDbRecord))
						_lastDbRecord = DateTime.Now.AddDays(-1);
					//_lastState = C.GetInt(C.GetRowField(tb2, "Id=" + ID, "Value"));
					//_lastState = _lastState == 0 ? 0 : 1;
					MakeLog("Last db record: " + _lastDbRecord);
				}
				_dbInsertLog.AddSuccess();

			} catch (Exception ex) {
				_needToInit = true;
			} 
		}

		private void MakeLog(string msg)
		{
			if (LogEntry != null)
				LogEntry(this, new LogEntryArgs(msg));
		}

		#region | public properties     |

		public string HostName
		{
			get { return _hostName; }
			set { _hostName = value; }
		}
		public string HostPort
		{
			get { return _hostPort; }
			set { _hostPort = value; }
		}
		public bool Slow
		{
			get { return _slow; }
			set { _slow = value; }
		}

		public int PlcAddress
		{
			get { return _plcNumber; }
			set { _plcNumber = value; }
		}
		public int PlcInput
		{
			get { return _plcInput; }
			set { _plcInput = value; }
		}
		public int PlcQuotientFromDB
		{
			get { return _plcQuotientFromDb; }
			set { _plcQuotientFromDb = value; }
		}
		public int PlcQuotientFromPLC
		{
			get { return _plcQuotientFromPlc; }
			set { _plcQuotientFromPlc = value; }
		}
		public int PlcDivider
		{
			get { return _plcDivider; }
			set { _plcDivider = value; }
		}
		/// <summary>
		/// Разница в минутах между часовыми поясами PLC и коллектором.
		/// Например, если в PLC время 15:00 а на компьютере с коллектором 16:00
		/// то значение будет -60
		/// Иными словами: сколько минут ко времени коллектора нужно добавить, чтобы получить время PLC 
		/// </summary>
		public int TimeZoneDiff
		{
			get { return _timeZoneDiff; }
		}
		public int LastEventID
		{
			get { return _lastEvent; }
			set { _lastEvent = value; }
		}
		public int LastState
		{
			get { return _lastState; }
			set { _lastState = value; }
		}
		public bool NeedToInit
		{
			get { return _needToInit; }
			set { _needToInit = value; }
		}
		public bool NeedToAddFirstRow
		{
			get { return _needToAddFirstRow; }
			set { _needToAddFirstRow = value; }
		}
		public DateTime LastDBRecord
		{
			get { return _lastDbRecord; }
			set { _lastDbRecord = value; }
		}
		public DateTime LastPLCRecord
		{
			get { return _lastPlcRecord; }
			set { _lastPlcRecord = value; }
		}
		public DbRel DbState
		{
			get { return dbstate; }
			set { dbstate = value; }
		}
		public bool Invert {get; private set;}

		public int LastInsertValue { get; set; }
		public DateTime LastInsertRec { get; set; }
		public DateTime LastCashRec { get; set; }

	/// <summary>
		/// Длина лога по умолчанию = 60
		/// </summary>
		public SuccessLog DbInsertLog
		{
			get { return _dbInsertLog; }
		}

		#endregion

	}

	public class LogEntryArgs : EventArgs
	{
		public LogEntryArgs(string entry)
		{
			LogEntry = entry;
		}
		public LogEntryArgs(string entry, DateTime datetime)
		{
			LogEntry = entry;
			DateTime = datetime;
		}
		public string LogEntry;
		public DateTime DateTime;
	}

}
