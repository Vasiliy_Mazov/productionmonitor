﻿using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Collections.Generic;
using ProductionMonitor.Collector;

namespace ProductionMonitor.Collector
{
	public class Tasks
	{

		#region | fields                |

		private System.Timers.Timer _timer;
		public event EventHandler LogEntry;
		private RegistryKey _regkey;
		private int _interval1 = ProductionMonitor.Properties.board.Default.Interval1;
		private int _ticks_counter = 0;
		private List<PlcUnit> units;

		#endregion

		public Tasks(List<PlcUnit> units)
		{
			// в милисекундах, т.е. тикает каждую минуту
			_timer = new System.Timers.Timer(60000);
			_timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);
			if (_interval1 > 0)
				_timer.Start();
			this.units = units;
		}
		public void Routine()
		{
			MakeLog("Update bw tables...");

			DateTime dt1 = DateTime.Now.Date;
			dt1 = dt1.AddDays(-dt1.Day + 1).AddMonths(-1);
			DateTime dt2 = dt1.AddMonths(1);
			DateTime dt3 = dt2.AddMonths(1);

			string res = "Updated rows: ";
			int totalupdated = 0;
			int nres;
			foreach (PlcUnit u in units) {
				nres = 0;
				try {
					using (SqlConnection conn = new SqlConnection(u.Connection)) {
						conn.Open();
						SqlCommand cmd = new SqlCommand("c_bw", conn);
						cmd.CommandTimeout = 100;
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4).Value = dt1;
						cmd.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4).Value = dt2;
						cmd.Parameters.Add("@year", SqlDbType.SmallInt, 2).Value = dt1.Year;
						cmd.Parameters.Add("@month", SqlDbType.SmallInt, 2).Value = dt1.Month;

						// Выполняем за предыдущий месяц
						nres = cmd.ExecuteNonQuery();

						// Выполняем за текущий месяц
						cmd.Parameters["@dt1"].Value = dt2;
						cmd.Parameters["@dt2"].Value = dt3;
						cmd.Parameters["@year"].Value = dt2.Year;
						cmd.Parameters["@month"].Value = dt2.Month;
						nres += cmd.ExecuteNonQuery();

						totalupdated += nres;
						res = res + Environment.NewLine + u.LineName + ": " + nres.ToString();

					}
				} catch (Exception ex) {
					MakeLog(ex.Message);
				} finally {
					MakeLog(u.LineName + ", updated rows: " + nres.ToString());
				}
			}

		}
		public void GetRegSetiings()
		{
			MakeLog("Get registry settings...");
			try {
				_regkey = CollectorSettings.GetRegistryBranch();
				_regkey.GetValue("Tasks_Interval1");
				if (_regkey != null) {
					if (_regkey.GetValue("Tasks_Interval1") == null) {
						_regkey.SetValue("Tasks_Interval1", _interval1);
					}
					_interval1 = C.GetInt(_regkey.GetValue("Tasks_Interval1", _interval1));
				}
			} catch (Exception ex) {
				MakeLog("InitReg() Exception:" + ex.Message);
			}
		}
		private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)		// тикает каждую секунду
		{
			if (_interval1 == 0 || _ticks_counter++ % _interval1 != 0)
				return;
			Routine();
		}
		private void MakeLog(string msg)
		{
			if (LogEntry != null)
				LogEntry(this, new LogEntryArgs(msg));
		}

	}
}
