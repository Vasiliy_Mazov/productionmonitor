﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Reflection;
using Microsoft.Win32;
using ProductionMonitor;

namespace ProductionMonitor.Collector
{
	public class PLC
	{
		#region | fields                |

		private string _keyName = "";							//
		private byte _plcAddress = 0;							//
		private bool _lastReadoutOk;							// last readout result 
		private int _countersInPlc = -1;						//
		private int _packetSize = 500;						// 1000 // для пакетного считывания данных установить больше нуля
		private bool _hasTail1 = false;						// при пакетном считывании обозначает что есть еще пакеты
		private bool _hasTail2 = false;						//
		private int _rows;										// 
		private long _timeDifference = -1;					// time difference bitween PC and PLC
		private long _timeDifferenceAbsolute = -1;		// absolute time difference bitween PC and PLC
		private DateTime _lastConfiguration;				// when PLC configuration was last time corrected
		private DataTable _dtbRawData1;						//
		private DataTable _dtbProduct;						//
		private DataTable _dtbEventCnt;
		private System.IO.Ports.SerialPort _comport;		//
		private bool _timeAutoCorrection;					//
		private int _timeAutoCorrectionLastMin = 61;		//
		private int _received;									//
		private CollectorConfig _collConfig;				//
		private Dictionary<int, PlcUnit> _validUnitsInPlc = new Dictionary<int, PlcUnit>();		//
		private int _timeZoneDiff = 0;
		private string _currStateInfo = "";					//
		private double _readouts = 0;							//
		private double _errors = 0;							//
		private bool _shouldToExit = false;
		private int _failedReadauts = 0;
		private double _lastTotMins = 0;
		private int _numSend = 2;
		private DateTime _lastDbEventRecord = DateTime.Now;
		private Dictionary<int, PlcUnit> _eventUnits = new Dictionary<int, PlcUnit>();
		private SuccessLog _dbInsertLog = new SuccessLog(60);

		public event EventHandler LogEntry;

		#endregion

		public PLC(System.IO.Ports.SerialPort CommPort, int PLCNetworkAddress, bool TimeAutoCorrection)
		{
			_comport = CommPort;
			_plcAddress = Convert.ToByte(PLCNetworkAddress);
			_timeAutoCorrection = TimeAutoCorrection;
			_timeAutoCorrectionLastMin = DateTime.Now.Minute;

			_keyName = _comport.PortName + "A" +  PLCNetworkAddress.ToString();
		}
		public PLC(System.IO.Ports.SerialPort CommPort, int PLCNetworkAddress, bool TimeAutoCorrection, CollectorConfig collConfig)
		{
			_comport = CommPort;
			_plcAddress = Convert.ToByte(PLCNetworkAddress);
			_timeAutoCorrection = TimeAutoCorrection;
			_timeAutoCorrectionLastMin = DateTime.Now.Minute;
			_collConfig = collConfig;

			_keyName = _comport.PortName + "A" +  PLCNetworkAddress.ToString();
		}

		#region | public methods        |
		
		public void Initialize()
		{
			MakeLog("Initialization...");

			CheckCurrentTime();
			////lastcounters.Add(3);
			////lastcounters.Add(4);

			Dictionary<int, PlcUnit> units_in_plc = null;

			// считываем конфигурацию которая прошита в PLC (заполняем units)
			if (_lastReadoutOk)
				units_in_plc = GetCounters();

			if (units_in_plc == null)
				return;

			LastDbRecord = DateTime.Now.AddDays(1);
			if (units_in_plc == null || units_in_plc.Count == 0) return;

			_validUnitsInPlc.Clear();
			PlcUnit realunit;

			// сравниваем с конфигурацией полученной из конфигурационного файла и DB
			foreach (PlcUnit configunit in _collConfig.PlcUnits.Values)
				if (configunit.PlcAddress == _plcAddress) {
					if (configunit.DbState == PlcUnit.DbRel.WithoutDB)
						continue;
					if (configunit.Kind == UnitKind.Line) {
						if (units_in_plc.ContainsKey(configunit.PlcInput) &&
								_comport.PortName == configunit.HostPort) {
							realunit = units_in_plc[configunit.PlcInput];
							if (configunit.HostName.ToLower() != Environment.MachineName.ToLower()) {
								MakeLog("Unit ID = " + configunit.ID + " has incompatible host name in database");
							} else {
								_validUnitsInPlc.Add(configunit.ID, configunit);
								configunit.PlcQuotientFromPLC = realunit.PlcQuotientFromPLC;
								configunit.LastPLCRecord = realunit.LastPLCRecord;
								if (this.LastDbRecord > configunit.LastDBRecord && configunit.DbState == PlcUnit.DbRel.OwnDB)
									this.LastDbRecord = configunit.LastDBRecord;
								MakeLog("unit.ID: " + configunit.ID.ToString() + "; DI" + configunit.PlcInput.ToString("00"));
							}
						} else
							MakeLog("DI" + configunit.PlcInput.ToString() + " is out of range");
					} else if (configunit.Kind == UnitKind.EventCounter) {
						if (_comport.PortName == configunit.HostPort && configunit.HostName.ToLower() == Environment.MachineName.ToLower()) {
							if (_lastDbEventRecord > configunit.LastDBRecord) 
								_lastDbEventRecord = configunit.LastDBRecord;
							if (configunit.PlcInput < 6 && configunit.PlcInput >= 0 && !_eventUnits.ContainsKey(configunit.PlcInput))
								_eventUnits.Add(configunit.PlcInput, configunit);
						}
					}
				}
			if (_lastReadoutOk)
				CheckCounters();
			InitTables();

			MakeLog("Initialization completed");
		}
		public void InitializeWithoutConfigFile()
		{
			CheckCurrentTime();
			if (_lastReadoutOk)
				_validUnitsInPlc = GetCounters();

			LastDbRecord = DateTime.Now;
			if (_validUnitsInPlc == null) return;

			if (_lastReadoutOk)
				CheckCounters();
			InitTables();
		}
		public void AddUnit(PlcUnit u)
		{
			PlcUnit realunit;

			Dictionary<int, PlcUnit> units_in_plc = null;

			// считываем конфигурацию которая прошита в PLC (заполняем units)
			if (_lastReadoutOk)
				units_in_plc = GetCounters();

			if (units_in_plc == null)
				return;

			if (u.PlcAddress == _plcAddress) {
				if (u.DbState == PlcUnit.DbRel.WithoutDB)
					return;
				if (units_in_plc.ContainsKey(u.PlcInput) &&
						_comport.PortName == u.HostPort) {
					realunit = units_in_plc[u.PlcInput];
					if (u.HostName.ToLower() != Environment.MachineName.ToLower()) {
						MakeLog("Unit ID = " + u.ID + " has incompatible host name in database");
					} else {
						_validUnitsInPlc.Add(u.ID, u);
						u.PlcQuotientFromPLC = realunit.PlcQuotientFromPLC;
						u.LastPLCRecord = realunit.LastPLCRecord;
						if (this.LastDbRecord > u.LastDBRecord && u.DbState == PlcUnit.DbRel.OwnDB)
							this.LastDbRecord = u.LastDBRecord;
						MakeLog("unit.ID: " + u.ID.ToString() + "; DI" + u.PlcInput.ToString("00"));
					}
				} else
					MakeLog("DI" + u.PlcInput.ToString() + " is out of range");
			}
		}

		public void CheckCurrentTime()
		{
			_readouts++;

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];
			string strDate = "";
			try {
				btOut[0] = _plcAddress;
				btOut[1] = 99;		// 'c'
				if (SendReceive(ref btOut, 2, ref btIn)) {
					for (int i = 2; i <= 7; i++)
						strDate = strDate + ToZero(btIn[i]).ToString("00");
					DateTime dt = DateTime.ParseExact(strDate, "yyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
					////lastplcrecord = dt;
					//throw new Exception("Exception from plc.CheckCurrentTime()");
					_timeDifference = (dt.Ticks - DateTime.Now.Ticks) / 10000000L;
					_timeDifferenceAbsolute = Math.Abs(_timeDifference);
					SetResult(true);
				}
			} catch (Exception ex) {
				MakeLog(ex.Message + "; string for DateTime.ParseExact: " + strDate);
				SetResult(false);
			}
			Debug.WriteLine("PLC Network Address: " + _plcAddress.ToString() + "; check date: " + CurrentPLCTime.ToString());
			//return last_readout_ok;
		}
		public void SetCurrentTime(DateTime dt)
		{
			byte[] btOut = new byte[20];
			byte[] btIn = new byte[20];
			string strDate = "";
			try {

				btOut[0] = _plcAddress;
				btOut[1] = 100;		// 'd'
				btOut[2] = Convert.ToByte(dt.Year - 2000);
				btOut[3] = Convert.ToByte(dt.Month);
				btOut[4] = Convert.ToByte(dt.Day);
				btOut[5] = UnZero(Convert.ToByte(dt.Hour));
				btOut[6] = UnZero(Convert.ToByte(dt.Minute));
				btOut[7] = UnZero(Convert.ToByte(dt.Second));
				if (SendReceive(ref btOut, 8, ref btIn)) {
					for (int i = 2; i <= 7; i++)
						strDate = strDate + ToZero(btIn[i]).ToString("00");
					DateTime dt2 = DateTime.ParseExact(strDate, "yyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
					//m_TimeDifference = Convert.ToInt32(DateTime.Now.Subtract(dt).Ticks / 10000000);
					_timeDifference = (dt.Ticks - DateTime.Now.Ticks) / 10000000L;
					_timeDifferenceAbsolute = Math.Abs(_timeDifference);
					SetResult(true);
				}
			} catch (Exception ex) {
				MakeLog(ex.Message);
				SetResult(false);
			}
		}

		public Dictionary<int, PlcUnit> GetCounters()
		{
			_readouts++;
			Dictionary<int, PlcUnit> units_in_plc = new Dictionary<int, PlcUnit>();

			////prevcounters.Clear();
			////if (lastcounters.Count > 0)
			////   prevcounters.AddRange(lastcounters);
			////lastcounters.Clear();

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];
			try {
				btOut[0] = _plcAddress;
				btOut[1] = 106;	// 'j'
				if (SendReceive(ref btOut, 2, ref btIn)) {
					_countersInPlc = btIn[2];
					_rows = ToZero(btIn[3]) << 8 | ToZero(btIn[4]);
					MakeLog("Counters in the PLS: " + _countersInPlc.ToString());

					for (int n = 1; n <= _countersInPlc; n++)
						units_in_plc.Add(n, new PlcUnit(n));

					SetResult(true);
				}
			} catch (Exception ex) {
				SetResult(false);
				MakeLog(ex.Message);
			}
			return units_in_plc;
		}
		public void CheckCounters()
		{
			_readouts++;

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];
			MakeLog("CheckCounters...");

			try {
				btOut[0] = _plcAddress;
				btOut[1] = 106;	// 'j'
				if (SendReceive(ref btOut, 2, ref btIn)) {
					_countersInPlc = btIn[2];
					_rows = ToZero(btIn[3]) << 8 | ToZero(btIn[4]);
					for (int n = 1; n <= _countersInPlc; n++)
						this.CheckCounter(n);
					SetResult(true);
				}
			} catch (Exception ex) {
				SetResult(false);
				MakeLog(ex.Message);
			}
		}
		public void SetCounters(int Counters)
		{
			int cnt;
			if (Counters < 0)
				cnt = 1;
			else if (Counters > 32)
				cnt = 32;
			else
				cnt = Counters;
			byte[] btOut = new byte[20];
			byte[] btIn = new byte[20];

			//m_CommPort.Open();
			btOut[0] = _plcAddress;
			btOut[1] = 105;		// 'i'
			btOut[2] = Convert.ToByte(cnt);		// 'i'
			if (SendReceive(ref btOut, 3, ref btIn))
				_countersInPlc = btIn[2];

			if (_countersInPlc != cnt)
				_lastConfiguration = DateTime.Now;
			CheckCounters();
		}

		public void CheckCounter(int CounterNumber)
		{
			PlcUnit pun = null;
			foreach (PlcUnit u in _validUnitsInPlc.Values)
				if (u.PlcInput == CounterNumber) {
					pun = u;
					break;
				}
			if (pun == null)
				return;

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];
			string log;
			try {
				btOut[0] = _plcAddress;
				btOut[1] = 109;	// 'm'
				btOut[2] = Convert.ToByte(CounterNumber);
				if (SendReceive(ref btOut, 3, ref btIn)) {
					if (btOut[2] == btIn[2]) {
						//m_UnitQuotient[CounterNumber - 1] = btIn[3];
						pun.PlcQuotientFromPLC = btIn[3];
						log = "Multiplier for PLC digital input DI" + (CounterNumber - 1).ToString() + " = " + pun.PlcQuotientFromPLC.ToString();
						MakeLog(log);
						if (pun.DbState != PlcUnit.DbRel.WithoutDB && pun.PlcQuotientFromPLC != pun.PlcQuotientFromDB) {
							log = "WARNING!!! Should to correct counter multiplier. Database value: " +
								pun.PlcQuotientFromDB.ToString() +
								"; PLC value: " + pun.PlcQuotientFromPLC.ToString() +
								". Values should be identical." +
								" UniID = " + pun.ID.ToString();
							MakeLog(log);
						}
						SetResult(true);
					}
				}
			} catch (Exception ex) {
				MakeLog(ex.Message);
				SetResult(false);
			}
		}
		public void SetCounter(int CounterNumber, int CounterValue)
		{
			PlcUnit plcunit = null;
			foreach (PlcUnit u in _validUnitsInPlc.Values)
				if (u.PlcInput == CounterNumber) {
					plcunit = u;
					break;
				}
			if (plcunit == null)
				return;

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];

			if (CounterValue < 1)
				CounterValue = 1;
			else if (CounterValue > 4)
				CounterValue = 4;
			try {
				btOut[0] = _plcAddress;
				btOut[1] = 107;	// 'k'
				btOut[2] = Convert.ToByte(CounterNumber);
				btOut[3] = Convert.ToByte(CounterValue);
				if (SendReceive(ref btOut, 4, ref btIn)) {
					if (btOut[2] == btIn[2])
						plcunit.PlcQuotientFromPLC = btIn[3];
					if (btOut[2] == btIn[2] && btOut[3] == btIn[3])
						SetResult(true);
				}
			} catch (Exception) {
				SetResult(false);
			}
		}
		public void ReadFrom()
		{
			if (_plcAddress == 0 ||
					_countersInPlc == -1 ||
					_timeDifferenceAbsolute == -1 ||
					_dtbRawData1 == null ||
					_dtbProduct == null) {
				Initialize();
				return;
			}

			_shouldToExit = false;

			// используем UniversalTime только по причине перехода на зимнее-летнее время
			//totmins = CurrentPLCTime.ToUniversalTime().Subtract(lastplcrecord.ToUniversalTime()).TotalMinutes;
			double totmins = CurrentPLCTime.Subtract(this.LastDbRecord).TotalMinutes;

			if (totmins > this._rows)
				totmins = this._rows;

			// если используется пакетное считывание и нужно прочитать больше чем размер пакета
			// и уже есть пробелы считывания
			int rowlenth = _countersInPlc * 2 + 3;
			int packetmins = 8 * _packetSize / rowlenth;
			if (_packetSize > 1 && totmins > packetmins) {
				if (OmissionCounter > 0) {
					if (totmins > packetmins * OmissionCounter) {
						totmins -= packetmins * OmissionCounter;
						_lastTotMins = totmins;
					} else
						totmins = packetmins;
				} else if (_lastTotMins > 0) {
					totmins = _lastTotMins - packetmins + 3;
					_lastTotMins = totmins;
				}
			} else
				_lastTotMins = 0;

			int n = Convert.ToInt32(Math.Ceiling(totmins + 0.5)) - 1;
			Debug.WriteLine("ReadFrom, m_LastPLCRecord: " + this.LastDbRecord.ToString() + "; n = " + n.ToString());
			if (Collector._extended_log) {
				MakeLog("CurrentPLCTime: " + CurrentPLCTime.ToString());
				MakeLog("lastplcrecord: " + this.LastDbRecord.ToString());
			}
			if (n > 65536)
				n = 65530;
			if (n < 1)
				return;
			//if (n == 1)
			//   n++;
			if (n % 256 == 255) n++;	// в запросах не должно быть чисел вида FF

			ReadRows1(Convert.ToUInt16(n));
			ReadRows2(1);
		}

		#endregion

		#region | private methods       |
		
		private void InitTables()
		{
			_dtbRawData1 = new DataTable("RowData");
			_dtbRawData1.Columns.Add("DT", typeof(DateTime));
			for (int n = 1; n <= _countersInPlc; n++) {
				_dtbRawData1.Columns.Add(n.ToString(), typeof(Int16));
				_dtbRawData1.Columns.Add(n.ToString() + "s", typeof(Int16));
			}

			_dtbProduct = new DataTable("tbProduct");
			_dtbProduct.Columns.Add("Date", typeof(DateTime));
			_dtbProduct.Columns.Add("UnitID", typeof(Int16));
			_dtbProduct.Columns.Add("Crop", typeof(Int16));
			_dtbProduct.Columns.Add("Seconds", typeof(byte));
			_dtbProduct.Columns.Add("EventID", typeof(Int32));
			_dtbProduct.PrimaryKey = new DataColumn[] { _dtbProduct.Columns["Date"], _dtbProduct.Columns["UnitID"] };

			_dtbEventCnt = new DataTable("tbEvent");
			_dtbEventCnt.Columns.Add("DT", typeof(DateTime));
			_dtbEventCnt.Columns.Add("Id", typeof(int));
			_dtbEventCnt.Columns.Add("Value", typeof(int));
			_dtbEventCnt.Columns.Add("Seconds", typeof(int));
			_dtbEventCnt.Columns.Add("Gap", typeof(int));
			_dtbEventCnt.PrimaryKey = new DataColumn[] { _dtbEventCnt.Columns["DT"], _dtbEventCnt.Columns["Id"] };

		}
		private void FillRawData1(byte[] btIn, int iLastByte)
		{
			_dtbRawData1.Clear();
			int iCurrRow, iCurrByte, iParsedRows = 0;
			byte seconds;

			for (int n = 0; n < iLastByte; n++)
				if (btIn[n] == 255)
					btIn[n] = 0;

			DateTime dt = new DateTime(2000, 1, 1);
			string strDate, flt ;
			int iRowLenth = _countersInPlc * 2 + 3;
			int m;
			string deb = "";
			DataRow dr, r;
			int k;
			//for ( m = 1 ; m <= iLastByte / iRowLenth; m++) {
			for (m = iLastByte / iRowLenth; m > 0; m--) {

				iCurrRow = iLastByte - m * iRowLenth;
				strDate = DateTime.Now.Year.ToString();
				strDate += (btIn[iCurrRow] & 0x0F).ToString("00");
				strDate += (btIn[iCurrRow + 1] >> 3).ToString("00");
				strDate += (((btIn[iCurrRow + 1] & 0x07) << 2) | (btIn[iCurrRow + 2] >> 6)).ToString("00");
				strDate += (btIn[iCurrRow + 2] & 0x3F).ToString("00");
				deb = strDate;
				if (DateTime.TryParseExact(strDate, "yyyyMMddHHmm", null, System.Globalization.DateTimeStyles.None, out dt)) {
					iParsedRows++;

					// проверяем не сменился ли год
					if (dt.Month > 10 && DateTime.Now.Month < 3)
						dt.AddYears(-1);

					dr = _dtbRawData1.NewRow();
					dr["dt"] = dt;
					if (Collector._extended_log) MakeLog("Parsed row/date: " + iParsedRows.ToString() + " / " + dt.ToString());

					deb = dt.ToString() + "\t:";

					foreach (PlcUnit unit in _validUnitsInPlc.Values) {
						k = unit.PlcInput - 1;
						iCurrByte = iCurrRow + 3 + k * 2;
						dr[k * 2 + 1] = (Convert.ToInt16(btIn[iCurrByte] << 2 | btIn[iCurrByte + 1] >> 6)) * unit.PlcQuotientFromPLC * unit.PlcDivider;
						seconds = Convert.ToByte(btIn[iCurrByte + 1] & 0x3F);
						if (unit.Slow && seconds < 60)
							seconds = 0;
						dr[k * 2 + 2] = seconds;
						deb += "\t" + Convert.ToInt16(dr[k * 2 + 1]).ToString("000") + "\t" + Convert.ToInt16(dr[k * 2 + 2]).ToString("00");

						if (unit.LineName != "") {
							r = _dtbProduct.NewRow();
							r[0] = dt;
							r[1] = Convert.ToInt16(unit.ID);
							r[2] = dr[k * 2 + 1];
							r[3] = dr[k * 2 + 2];
							flt = "Date=" + C.GetCDateString(dt) + " AND UnitID=" + unit.ID.ToString();
							//if (dtb_product.Select(flt).Length == 0) {
							if (!_dtbProduct.Rows.Contains(new object[] { r[0], r[1] })) {

								unit.LastPLCRecord = dt;
								_dtbProduct.Rows.Add(r);
								r.AcceptChanges();
								r.SetModified();
							}else
								if (Collector._extended_log) MakeLog("FillRowData:" + flt + " - already exist!" + dt.ToString());
						}
					}
					_dtbRawData1.Rows.Add(dr);
				}
				Debug.WriteLine(deb);
			}
			//lastplcrecord = dt;
			MakeLog("downloaded rows: " + (iLastByte / iRowLenth).ToString() + "; parsed rows: " + iParsedRows.ToString());
		}
		private bool FillRawData2(byte[] btIn)
		{
			// 3______3 2______2 1______1	0______0
			// ddddddMM MMDDDDDH HHHHmmmm mmSSSSSS
			// 20120819 23:43
			// 204,122,38,194,196,122,38,254

			int n = 0;
			byte val = 0;
			byte last;
			PlcUnit u;
			DateTime emptydt = new DateTime(1, 1, 1);
			bool lastFinded = false;
			while (n < 5) {
				val = (byte)((btIn[n + 3] & 0xFC) >> 2);
				
				string strDate = DateTime.Now.Year.ToString();

				// ToZero применять нельзя т.к. в старшем байте (ddddddMM) возможны оба значения (0 и 255),
				// НО!!! младший байт (mmSSSSSS) не может содержать 255 поэтому явно правим на 0
				last = btIn[n] == 0xFF ? (byte)0 : btIn[n];
				
				strDate += ((btIn[n + 3] & 0x03) << 2 | (btIn[n + 2] >> 6)).ToString("00");	// Месяц
				strDate += ((btIn[n + 2] & 0x3E) >> 1).ToString("00");								// День
				strDate += ((btIn[n + 2] & 0x01) << 4 | (btIn[n + 1] >> 4)).ToString("00");	// Час
				strDate += ((btIn[n + 1] & 0x0F) << 2 | (last >> 6)).ToString("00");	// Минуты
				strDate += (last & 0x3F).ToString("00");										// Секунды

				DateTime dt;
				if (DateTime.TryParseExact(strDate, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out dt)) {

					// Проверка на Новый год
					if (dt > DateTime.Now.AddDays(1))
						dt = dt.AddYears(-1);

					// Если такая дата уже есть значит запоминаем что надо выходить 
					if (_dtbEventCnt.Select(string.Format("DT=#{0:M/d/yy H:m:s}#", dt), "").Length > 0) 
						lastFinded = true;

					int res;
					foreach (int key in _eventUnits.Keys) {
						u = _eventUnits[key];
						res = val & (0x01 << key);
						res = res > 0 ? 1 : 0;

						if (dt < u.LastDBRecord)
							return true;

						if (u.LastInsertRec == emptydt || u.LastInsertRec < dt || res != u.LastInsertValue) {
							if (u.LastInsertRec != emptydt && u.LastInsertRec > dt && res != u.LastInsertValue && u.LastInsertRec > u.LastDBRecord) {
								DataRow drEv = _dtbEventCnt.NewRow();
								drEv["DT"] = u.LastInsertRec;
								drEv["Id"] = u.ID;
								drEv["Value"] = u.Invert ? (u.LastInsertValue == 1 ? 0 : 1) : u.LastInsertValue;
								_dtbEventCnt.Rows.Add(drEv);
							}

							u.LastInsertRec = dt;
							u.LastInsertValue = res;
						}

						Debug.WriteLine(dt.ToString());
					}
					if (lastFinded)
						return true;
				} else
					Debug.WriteLine("DateTime.TryParseExact FAULT!!!   strDate: " + strDate);

				n += 4;
			}
			return false;
		}
		private void CheckTimeAutoCorrection()
		{
			//int TimeZoneDiff = 0;
			// временная зона для контроллера определяется по первому попавшемуся юниту
			foreach (PlcUnit pu in this.PlcUnits.Values) {
				if (pu.TimeZoneDiff != 0) {
					//TimeZoneDiff = pu.TimeZoneDiff * 60;
					_timeZoneDiff = pu.TimeZoneDiff * 60;
					break;
				}
			}
			DateTime timetoset = DateTime.Now.AddSeconds(_timeZoneDiff);
			if (DateTime.Now.Minute == _timeAutoCorrectionLastMin) {		// только раз в час и при старте
				CheckCurrentTime();													// время обязательно проверяем каждый час чтоб не сдрейфовало
				if (_timeAutoCorrection) {											// только по свойству
					if (_lastReadoutOk && Math.Abs(_timeDifference - _timeZoneDiff) > 60) {	// и если разница больше минуты
						SetCurrentTime(DateTime.Now.AddSeconds(_timeZoneDiff));
						if (_lastReadoutOk)
							_timeAutoCorrectionLastMin = DateTime.Now.Minute;
						MakeLog("DateTime was corrected");
					}
				}
			}
		}
		private void MakeLog(object obj, string msg)
		{
			if (LogEntry != null)
				LogEntry(obj, new LogEntryArgs(msg));
		}
		private void MakeLog(string msg)
		{
			if (LogEntry != null)
				LogEntry(this, new LogEntryArgs(msg));
		}
		private void SetResult(bool res)
		{
			_lastReadoutOk = res;
			if (res)
				_failedReadauts = 0;
			else
				_failedReadauts++;
		}
		
		#endregion

		#region | properties            |
		
		public string Name
		{
			get { return _keyName; }
		}
		public int CountersInPLC
		{
			get { return _countersInPlc; }
		}
		public int Rows
		{
			get { return _rows; }
		}
		public int NetworkAddress
		{
			get { return _plcAddress; }
		}
		public bool LastReadoutOk
		{
			get { return _lastReadoutOk; }
		}
		public bool CurrentStateOk
		{
			get { return _failedReadauts < 3; }
		}
		public bool TimeAutoCorrection
		{
			get { return _timeAutoCorrection; }
			set { _timeAutoCorrection = value; }
		}
		public DateTime CurrentPLCTime
		{
			get { return DateTime.Now.AddSeconds(_timeDifference); }
		}
		public Dictionary<int, PlcUnit> PlcUnits
		{
			get { return _validUnitsInPlc; }
			//set { units_in_plc = value; }
		}
		public DataTable Product
		{
			get { return _dtbProduct; }
		}
		public DataTable EventCounter
		{
			get { return _dtbEventCnt; }
		}
		public string CurrentStateInfo
		{
			get { return _currStateInfo; }
		}
		public double Errors
		{
			get { return _errors; }
		}
		public double ReadOuts
		{
			get { return _readouts; }
		}
		public void ShouldToExit()
		{
			_shouldToExit = true;
		}
		public System.IO.Ports.SerialPort CommPort
		{
			get { return _comport; }
			set { _comport = value; }
		}
		public bool HasTail
		{
			get { return _hasTail1 || _hasTail2; }
		}
		public int OmissionCounter	{ get; set;	}
		public DateTime LastDbRecord { get; set; }
		public SuccessLog DbInsertLog
		{
			get { return _dbInsertLog; }
		}

		#endregion

		#region | Sending               |

		private void ReadRows1(ushort RowsToRead)
		{
			//113-q(запрос), 114-r(повтороно), 115-s(сегмент), 116-t(конец)

			long l = DateTime.Now.Ticks;
			int m = 0, n;
			//RowsToRead = 500;
			if (RowsToRead > _rows && _rows > 1)
				RowsToRead = Convert.ToUInt16(_rows - 1);
			CheckTimeAutoCorrection();

			byte[] btIn = new byte[20];
			byte[] btOut = new byte[20];
			byte[] btSum = new byte[500000];
			String deb;
			bool success = false;

			btOut[0] = _plcAddress;
			_numSend = 2;
			int iRowLenth = _countersInPlc * 2 + 3;
			int last_ind = 0;


			for (n = 0; n < 100000; n++) {
				if (n == 0) {
					btOut[1] = 113;						//'q' инициализация запроса данных
					btOut[2] = UnZero(Convert.ToByte(RowsToRead >> 8));
					btOut[3] = UnZero(Convert.ToByte(RowsToRead & 0x00FF));
					success = SendReceive(ref btOut, 4, ref btIn);
				} else {
					btOut[1] = 115;						//'s'	запрос следующего сегмента данных
					success = SendReceive(ref btOut, 2, ref btIn);
				}
				if (!success) {
					// чтобы постоянно не долбил скидываем чтобы подождал до следующего вызова по таймеру
					_hasTail1 = false;
					break;	// с нескольких попыток не смогли прочитать > нет соединения с PLC > выходим
				}

				deb = n.ToString() + ":  ";
				if ((btIn[0] == 115 || btIn[0] == 116)) {
					for (m = 0; m < _received - 5; m++) {
						btSum[n * 8 + m] = btIn[m + 3];
						deb += "\t" + btSum[n * 8 + m].ToString();
					}
					Debug.WriteLine(deb);
					_currStateInfo = "Received bytes: " + (n * 11).ToString();
				}
				if (btIn[0] == 116 || _shouldToExit) {						//'t' получен последний (текущий) сегмент данных
					last_ind = n * 8 + m;
					_hasTail1 = false;
					break;

				} else if (_packetSize > 0 && n > _packetSize) {
					iRowLenth = _countersInPlc * 2 + 3;
					last_ind = ((n * 8 + m) / iRowLenth) * iRowLenth;
					_hasTail1 = true;
					break;
				}
			}
			_currStateInfo = "Idle";
			Debug.WriteLine("TimeSpent: " + ((DateTime.Now.Ticks - l) / 10000).ToString());
			if (success && !_shouldToExit) FillRawData1(btSum, last_ind);
		}
		private void ReadRows2(ushort RowsToRead)
		{
			// Посылка
			// 110-n(запрос), 111-o(сегмент), 112-p(повтоно)
			// Ответ
			// 110-n(последний сегмент), 111-o(сегмент)

			long l = DateTime.Now.Ticks;
			int m = 0, n;

			byte[] btIn = new byte[15];
			byte[] btOut = new byte[15];
			byte[] btSum = new byte[15];
			String deb;
			bool success = false;

			btOut[0] = _plcAddress;
			_numSend = 2;

			for (n = 0; n < 1000; n++) {
				if (n == 0 && !_hasTail2)
					btOut[1] = 110;						//'n' инициализация запроса данных
				else
					btOut[1] = 111;						//'o'	запрос следующего сегмента данных
				
				success = SendReceive(ref btOut, 2, ref btIn);

				deb = n.ToString() + ":  ";
				if ((btIn[0] == 111)) {
					for (m = 0; m < _received - 4; m++) {
						btSum[m] = btIn[m + 2];
						deb += "\t" + btSum[m].ToString();
					}
					Debug.WriteLine(deb);
					_currStateInfo = "Received bytes (event counters): " + (n * 8).ToString();
				}

				_hasTail2 = !(FillRawData2(btSum));

				if (!_hasTail2 || btIn[0] == 110 || _shouldToExit) //'o' получен последний (текущий) сегмент данных
					break;
			}

			_currStateInfo = "Idle";

		}
		private bool SendReceive(ref byte[] btOut, int OutLen, ref byte[] btIn)
		{
			int currattempt = 3;
			int totalattempts = currattempt;
			int res;
			_readouts++;
			_received = 0;
			
			string debugRes = "";
			while (currattempt > 0) {
				if (_comport.BytesToRead > 0) {
					Debug.WriteLine("BytesToRead: " + _comport.BytesToRead.ToString());
					_comport.DiscardInBuffer();
					System.Threading.Thread.Sleep(1);
				}
				if (_comport.BytesToWrite > 0)
					_comport.DiscardOutBuffer();
				//Debug.WriteLine("BytesToRead: " + m_CommPort.BytesToRead.ToString());
				//Debug.WriteLine("BytesToWrite: " + m_CommPort.BytesToWrite.ToString());
				//System.Threading.Thread.Sleep(2);		
				SendWithCRC(ref btOut, OutLen);
				Application.DoEvents();

				//Ждем когда заполнится буфер
				int counter = 0;
				while (_comport.BytesToRead < 5 && counter < 100) {
					counter++;
					System.Threading.Thread.Sleep(5);
				}
				if (currattempt < totalattempts)
					System.Threading.Thread.Sleep(10); // если это уже не первая попытка то надо подождать подольше
				else
					System.Threading.Thread.Sleep(0);
				
				for (int b = 0; b < btIn.Length; b++)
					btIn[b] = 0;
				res = ReceiveWithCRCCheck(ref btIn);
				if (res > 0 && btOut[0] == btIn[1] && btOut[1] != 115 && btOut[1] != 114) {
					// это не данные
					_received = res;
					SetResult(true);
					break;
				} else if (res > 0 && btOut[0] == btIn[1] && (btOut[1] == 115 || btOut[1] == 114) && btIn[2] == _numSend - 1) {
					// пришла повторно таже строка!!!
					currattempt--;
					if (currattempt != 0 )
						continue;
				} else if (res > 0 && btOut[0] == btIn[1] && (btOut[1] == 115 || btOut[1] == 114) && btIn[2] == _numSend) {
					_numSend++;
					if (_numSend > 254)
						_numSend = 1;
					_received = res;
					SetResult(true);
					break;
				}

				currattempt--;
				if (btOut[1] == 115)
					btOut[1] = 114;	// 'r', для случая пересылки данных повторный запрос предыдущего сегмента
				if (btOut[1] == 111)
					btOut[1] = 112;	// 'p', для случая пересылки данных повторный запрос предыдущего сегмента
#if (DEBUG)
				Debug.WriteLine("plc readout fault!!! Attempt: " + currattempt.ToString());
				Debug.WriteLine("btIn[2]: " + btIn[2].ToString() + "   numSend: " + _numSend.ToString());

				debugRes = "";
				for (int n = 0; n < btIn.Length; n++)
					debugRes += "\t" + btIn[n].ToString();
				Debug.WriteLine("Flt:" + debugRes);
#endif
				if (currattempt <= 0) {
					_errors++;
					SetResult(false);
					_currStateInfo = "PLC READOUT FAULT!!!";
					Debug.WriteLine(_currStateInfo);
					MakeLog(_currStateInfo);
				}
			}
			return _lastReadoutOk;
		}
		private void SendWithCRC(ref byte[] btMsg, int len)
		{
			byte uchCRCHi = 0xFF; 			/* high byte of CRC initialized */
			byte uchCRCLo = 0xFF; 			/* low byte of CRC initialized */
			int uIndex; 						/* will index into CRC lookup table */
			for (int n = 0; n < len; n++) {
				uIndex = uchCRCLo ^ btMsg[n]; /* calculate the CRC */
				uchCRCLo = Convert.ToByte(uchCRCHi ^ auchCRCHi[uIndex]);
				uchCRCHi = auchCRCLo[uIndex];
			}
			uchCRCHi = UnZero(uchCRCHi);
			uchCRCLo = UnZero(uchCRCLo);
			btMsg[len] = uchCRCHi;
			btMsg[len + 1] = uchCRCLo;
			btMsg[len + 2] = 13;
			_comport.Write(btMsg, 0, len + 3);
		}
		private int ReceiveWithCRCCheck(ref byte[] btMsg)
		{
			int i = -1;
			try {
				i = _comport.Read(btMsg, 0, 15);
				if (i > 3) {
					byte uchCRCHi = 0xFF; 				// high byte of CRC initialized
					byte uchCRCLo = 0xFF; 				// low byte of CRC initialized
					int uIndex; 							// will index into CRC lookup table
					for (int n = 0; n < i - 2; n++) {
						uIndex = uchCRCLo ^ btMsg[n]; // calculate the CRC
						uchCRCLo = Convert.ToByte(uchCRCHi ^ auchCRCHi[uIndex]);
						uchCRCHi = auchCRCLo[uIndex];
					}
					uchCRCHi = UnZero(uchCRCHi);
					uchCRCLo = UnZero(uchCRCLo);
					if (uchCRCHi != btMsg[i - 2] || uchCRCLo != btMsg[i - 1])
						i = 0;
				} else
					i = 0;
			} catch (Exception ex) {
				_currStateInfo = ex.Message;
			}
			return i;
		}
		private static byte UnZero(byte b)
		{
			return (byte)(b == 0 ? 0xFF : b);
		}
		private static byte ToZero(byte b)
		{
			return (byte)(b == 0xFF ? 0 : b);
		}

		#region | CRC bytes |

		private static byte[] auchCRCHi = 
			{
				0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
				0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
				0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
				0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
				0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
				0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
				0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
				0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
				0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
				0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
				0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
				0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
				0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
				0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
				0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
				0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
				0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
				0x40
			};
		private static byte[] auchCRCLo = 
			{
				0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
				0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
				0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
				0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
				0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
				0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
				0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
				0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
				0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
				0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
				0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
				0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
				0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
				0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
				0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
				0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
				0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
				0x40
			};


		#endregion

		#endregion

	}
}
/*
			101 e		106 j		111 o		116 s		121 y
97  a		102 f		107 k		112 p		117 t		122 z
98  b		103 g		108 l		113 q		118 u
99  c		104 h		109 m		114 r		119 v
100 d		105 i		110 n		115 s		120 w

формат посылки и счетчиков второго типа
  'n' - запрос данных
  'o' - запрос следующего сегмента данных
  'p' - запрос сегмента повторно

ddddddMM MMDDDDDH HHHHmmmm mmSSSSSS
*/
