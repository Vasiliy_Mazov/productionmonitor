using System.Diagnostics;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Security.Cryptography;
using ProductionMonitor;
using System.ComponentModel;
using System.Collections.Generic;
using System.Threading;

namespace ProductionMonitor.Manager
{
	public sealed class frmManager : Form
	{
		[STAThread]
		public static void Main()
		{
			string cpr = "";

			#region | exception |

			// Create new instance of UnhandledExceptionDlg:
			UnhandledExceptionDlg exDlg = new UnhandledExceptionDlg { /* Uncheck "Restart App" check box by default:*/SendReport = false };

			// Add handling of OnShowErrorReport.
			// If you skip this then link to report details won't be showing.
			exDlg.OnShowErrorReport += (sender, ar) => MessageBox.Show(String.Format(
				"OS Version: {0}\nFramework: {1}\nProgram Version: {2}\nUserDomainName: {3}\nMachineName: {4}\nSystem started: {5:0}\nCurrentDirectory: {6}\nUnhandledException.Message:\n {7}\nUnhandledException.StackTrace:\n {8}\n", Environment.OSVersion, Environment.Version, Application.ProductVersion, Environment.UserDomainName, Environment.MachineName, Environment.TickCount / 60000, Environment.CurrentDirectory, ar.UnhandledException.Message, ar.UnhandledException.StackTrace));

			// Implement your sending protocol here. You can use any information from System.Exception
			exDlg.OnSendExceptionClick += delegate(object sender, SendExceptionClickEventArgs ar)
			{
				// User clicked on "Send Error Report" button:
				if (ar.SendReport) {
					try {
						string msg = "&body=";
						msg +=
							"OS Version: " + Environment.OSVersion + "\n" +
							"Framework: " + Environment.Version + "\n" +
							"Program Version: " + Application.ProductVersion + "\n" +
							"UserDomainName: " + Environment.UserDomainName + "\n" +
							"MachineName: " + Environment.MachineName + "\n" +
							"System started: " + (Environment.TickCount / 60000).ToString("0") + "\n" +
							"CurrentDirectory: " + Environment.CurrentDirectory + "\n" +
							"UnhandledException.Message:\n " + ar.UnhandledException.Message + "\n" +
							"UnhandledException.StackTrace:\n " + ar.UnhandledException.StackTrace + "\n";

						System.Diagnostics.Process.Start("mailto:wlad7777@mail.ru?subject=ProductionMonitor Exception" + msg);
						//MessageBox.Show("mail Send");
					} catch (Exception ex) {
						Debug.WriteLine(ex.ToString());
					}
				}
				// User wants to restart the App:
				if (ar.DialogResult == DialogResult.Retry) {
					//Console.WriteLine("The App will be restarted...");
					System.Diagnostics.Process.Start(System.Windows.Forms.Application.ExecutablePath);
					Application.Exit();
				} else if (ar.DialogResult == DialogResult.Abort)
					Application.Exit();

			};
			#endregion

			Splash.ShowSplash(100, C.GetAssemblyShortInfo(System.Reflection.Assembly.GetExecutingAssembly(), ref cpr), cpr);

			Application.EnableVisualStyles();
			//Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new frmManager());
		} //Main

		#region | Windows Form Designer |

		public frmManager()
		{
			InitializeComponent();
		}

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) 
					components.Dispose();
				
			
			base.Dispose(disposing);
		}

		private IContainer components;
		internal Panel Panel2;
		internal Panel pnTree;
		internal TreeView tree;
        internal TreeView tree2;
		private MenuItem menuItem1;
		internal TabPage tpData;
		internal Panel Panel6;
		internal TextBox txtQuery;
		internal TabPage tpScripts;
		internal TabPage tpConfig;
		private TabPage tpStat;
		private TextBox txtStat;
		internal Splitter splTree;
		internal TabControl tabs;
		internal Splitter splData;
		private Splitter splStat;
		private DataGrid grStat;
		private MenuStrip menuStrip1;
		private ToolStripMenuItem mnuFileToolStripMenuItem;
		private ToolStripMenuItem mnuSave_xml;
		private ToolStripMenuItem mnuSave_dat;
		private ToolStripSeparator toolStripSeparator2;
		private ToolStripMenuItem mnuSaveScript;
		private ToolStripMenuItem mnuSettings;
		private ToolStripSeparator toolStripSeparator1;
		private ToolStripMenuItem mnuExit;
		private ToolStripMenuItem mnuAbout;
		private ToolStripMenuItem mnuTimeConnection;
		private ToolStripMenuItem mnuTimeExec;
		private ContextMenuStrip cmTree;
		private StatusStrip statusStrip1;
		private ToolStripStatusLabel spCommon;
		private ToolStripStatusLabel spOperation;
		private ToolStripMenuItem mnuLoad_ds;
		private ToolStripMenuItem mnuLoad_xml;
		private ToolStripMenuItem mnuLoad_dat;
		private ToolStripSeparator toolStripSeparator3;
		private ContextMenuStrip cmList;
		private ToolStripMenuItem mnuAddScript;
		private ToolStripMenuItem mnuRename;
		private ToolStripMenuItem mnuDelete;
		private ToolStripMenuItem mnuReject;
		private ListBox list;
		private ToolStripMenuItem fileToolStripMenuItem;
		private ToolStripMenuItem saveToolStripMenuItem;
		private ToolStripSeparator toolStripSeparator4;
		private ToolStripMenuItem exitToolStripMenuItem;
		private ToolStripMenuItem fileToolStripMenuItem1;
		internal DataGrid grConfig;
		private ToolStripProgressBar progrBar;
		private ToolStripMenuItem mnuEdit;
		private ToolStripButton bindingNavigatorAddNewItem;
		private ToolStripLabel bindingNavigatorCountItem;
		private ToolStripButton bindingNavigatorDeleteItem;
		private ToolStripButton bindingNavigatorMoveFirstItem;
		private ToolStripButton bindingNavigatorMovePreviousItem;
		private ToolStripSeparator bindingNavigatorSeparator;
		private ToolStripTextBox bindingNavigatorPositionItem;
		private ToolStripSeparator bnSeparator1;
		private ToolStripButton bnMoveNextItem;
		private ToolStripButton bnMoveLastItem;
		private ToolStripSeparator bnSeparator2;
		private BindingNavigator bn;
		private ToolStripSeparator toolStripSeparator5;
		private ToolStripButton bnUndo;
		private ToolStripButton bnSave;
		private ToolStripButton bnFillHoles;
		private ImageList treeImageList;
		private Button btExec;
		private ToolStripSeparator toolStripSeparator6;
		private ToolStripMenuItem mnuReload;
		private BackgroundWorker dataWorker;
		private BackgroundWorker statWorker;
		private BackgroundWorker scriptWorker;
		internal TextBox dataResult;
		internal Splitter splBottom;
		internal TextBox txtScript;
		internal Splitter splitter1;
		internal TextBox scriptResult;
		internal DataGrid grData;
		private ToolStripMenuItem mnuUnCheck;

		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManager));
            this.Panel2 = new System.Windows.Forms.Panel();
            this.list = new System.Windows.Forms.ListBox();
            this.cmList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAddScript = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRename = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuSave_xml = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSave_dat = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSaveScript = new System.Windows.Forms.ToolStripMenuItem();
            this.btExec = new System.Windows.Forms.Button();
            this.mnuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.pnTree = new System.Windows.Forms.Panel();
            this.tree = new System.Windows.Forms.TreeView();
            this.cmTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuLoad_ds = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLoad_xml = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLoad_dat = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuUnCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.treeImageList = new System.Windows.Forms.ImageList(this.components);
            this.splTree = new System.Windows.Forms.Splitter();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tpConfig = new System.Windows.Forms.TabPage();
            this.grConfig = new System.Windows.Forms.DataGrid();
            this.tpData = new System.Windows.Forms.TabPage();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.grData = new System.Windows.Forms.DataGrid();
            this.splBottom = new System.Windows.Forms.Splitter();
            this.dataResult = new System.Windows.Forms.TextBox();
            this.bn = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bnSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bnMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bnMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bnSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.bnUndo = new System.Windows.Forms.ToolStripButton();
            this.bnSave = new System.Windows.Forms.ToolStripButton();
            this.bnFillHoles = new System.Windows.Forms.ToolStripButton();
            this.splData = new System.Windows.Forms.Splitter();
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.tpStat = new System.Windows.Forms.TabPage();
            this.grStat = new System.Windows.Forms.DataGrid();
            this.splStat = new System.Windows.Forms.Splitter();
            this.txtStat = new System.Windows.Forms.TextBox();
            this.tpScripts = new System.Windows.Forms.TabPage();
            this.txtScript = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.scriptResult = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuReload = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTimeConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTimeExec = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.spCommon = new System.Windows.Forms.ToolStripStatusLabel();
            this.spOperation = new System.Windows.Forms.ToolStripStatusLabel();
            this.progrBar = new System.Windows.Forms.ToolStripProgressBar();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataWorker = new System.ComponentModel.BackgroundWorker();
            this.statWorker = new System.ComponentModel.BackgroundWorker();
            this.scriptWorker = new System.ComponentModel.BackgroundWorker();
            this.Panel2.SuspendLayout();
            this.cmList.SuspendLayout();
            this.pnTree.SuspendLayout();
            this.cmTree.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tpConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grConfig)).BeginInit();
            this.tpData.SuspendLayout();
            this.Panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn)).BeginInit();
            this.bn.SuspendLayout();
            this.tpStat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grStat)).BeginInit();
            this.tpScripts.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel2
            // 
            this.Panel2.Controls.Add(this.list);
            this.Panel2.Controls.Add(this.btExec);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel2.Location = new System.Drawing.Point(634, 24);
            this.Panel2.Name = "Panel2";
            this.Panel2.Padding = new System.Windows.Forms.Padding(3, 20, 0, 2);
            this.Panel2.Size = new System.Drawing.Size(121, 323);
            this.Panel2.TabIndex = 3;
            // 
            // list
            // 
            this.list.ContextMenuStrip = this.cmList;
            this.list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list.IntegralHeight = false;
            this.list.Location = new System.Drawing.Point(3, 20);
            this.list.Name = "list";
            this.list.Size = new System.Drawing.Size(118, 301);
            this.list.TabIndex = 6;
            this.list.SelectedIndexChanged += new System.EventHandler(this.list_SelectedIndexChanged);
            this.list.Click += new System.EventHandler(this.list_Click);
            // 
            // cmList
            // 
            this.cmList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddScript,
            this.mnuRename,
            this.mnuDelete,
            this.mnuReject,
            this.toolStripSeparator6,
            this.mnuSave_xml,
            this.mnuSave_dat,
            this.mnuSaveScript});
            this.cmList.Name = "cmList";
            this.cmList.Size = new System.Drawing.Size(250, 164);
            this.cmList.Opening += new System.ComponentModel.CancelEventHandler(this.cmList_Opening);
            // 
            // mnuAddScript
            // 
            this.mnuAddScript.Image = global::ProductionMonitor.Properties.Resources._add;
            this.mnuAddScript.Name = "mnuAddScript";
            this.mnuAddScript.Size = new System.Drawing.Size(249, 22);
            this.mnuAddScript.Text = "Add template";
            this.mnuAddScript.Click += new System.EventHandler(this.mnuAddTScript_Click);
            // 
            // mnuRename
            // 
            this.mnuRename.Name = "mnuRename";
            this.mnuRename.Size = new System.Drawing.Size(249, 22);
            this.mnuRename.Text = "Rename template";
            this.mnuRename.Click += new System.EventHandler(this.mnuRename_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Image = global::ProductionMonitor.Properties.Resources._delete;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(249, 22);
            this.mnuDelete.Text = "Delete template";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuReject
            // 
            this.mnuReject.Image = global::ProductionMonitor.Properties.Resources._undo;
            this.mnuReject.Name = "mnuReject";
            this.mnuReject.Size = new System.Drawing.Size(249, 22);
            this.mnuReject.Text = "Reject all changes";
            this.mnuReject.Click += new System.EventHandler(this.mnuReject_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(246, 6);
            // 
            // mnuSave_xml
            // 
            this.mnuSave_xml.Image = global::ProductionMonitor.Properties.Resources._save;
            this.mnuSave_xml.Name = "mnuSave_xml";
            this.mnuSave_xml.Size = new System.Drawing.Size(249, 22);
            this.mnuSave_xml.Text = "Save config data as xml";
            this.mnuSave_xml.Click += new System.EventHandler(this.mnuSaveXml_Click);
            // 
            // mnuSave_dat
            // 
            this.mnuSave_dat.Image = global::ProductionMonitor.Properties.Resources._save;
            this.mnuSave_dat.Name = "mnuSave_dat";
            this.mnuSave_dat.Size = new System.Drawing.Size(249, 22);
            this.mnuSave_dat.Text = "Save config data as encrypted file";
            this.mnuSave_dat.Click += new System.EventHandler(this.nmuSaveDat_Click);
            // 
            // mnuSaveScript
            // 
            this.mnuSaveScript.Image = global::ProductionMonitor.Properties.Resources._save;
            this.mnuSaveScript.Name = "mnuSaveScript";
            this.mnuSaveScript.Size = new System.Drawing.Size(249, 22);
            this.mnuSaveScript.Text = "Save templates.xml";
            this.mnuSaveScript.Click += new System.EventHandler(this.mnuSaveScript_Click);
            // 
            // btExec
            // 
            this.btExec.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btExec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btExec.Location = new System.Drawing.Point(3, 0);
            this.btExec.Name = "btExec";
            this.btExec.Size = new System.Drawing.Size(118, 20);
            this.btExec.TabIndex = 3;
            this.btExec.Text = "Execute SQL";
            this.btExec.UseVisualStyleBackColor = true;
            this.btExec.Click += new System.EventHandler(this.btExec_Click);
            // 
            // mnuEdit
            // 
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Size = new System.Drawing.Size(37, 20);
            this.mnuEdit.Text = "Edit";
            // 
            // pnTree
            // 
            this.pnTree.Controls.Add(this.tree);
            this.pnTree.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnTree.Location = new System.Drawing.Point(3, 24);
            this.pnTree.Name = "pnTree";
            this.pnTree.Size = new System.Drawing.Size(180, 323);
            this.pnTree.TabIndex = 1;
            // 
            // tree
            // 
            this.tree.CheckBoxes = true;
            this.tree.ContextMenuStrip = this.cmTree;
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.FullRowSelect = true;
            this.tree.HideSelection = false;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.Size = new System.Drawing.Size(180, 323);
            this.tree.StateImageList = this.treeImageList;
            this.tree.TabIndex = 0;
            this.tree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tree_AfterCheck);
            // 
            // cmTree
            // 
            this.cmTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuLoad_ds,
            this.mnuLoad_xml,
            this.mnuLoad_dat,
            this.toolStripSeparator3,
            this.mnuUnCheck});
            this.cmTree.Name = "cmTree";
            this.cmTree.Size = new System.Drawing.Size(232, 98);
            this.cmTree.Opening += new System.ComponentModel.CancelEventHandler(this.cmTree_Opening);
            // 
            // mnuLoad_ds
            // 
            this.mnuLoad_ds.Name = "mnuLoad_ds";
            this.mnuLoad_ds.Size = new System.Drawing.Size(231, 22);
            this.mnuLoad_ds.Text = "Reload from the dataset";
            this.mnuLoad_ds.Click += new System.EventHandler(this.mnuLoad_ds_Click);
            // 
            // mnuLoad_xml
            // 
            this.mnuLoad_xml.Name = "mnuLoad_xml";
            this.mnuLoad_xml.Size = new System.Drawing.Size(231, 22);
            this.mnuLoad_xml.Text = "Reload from the xml file";
            this.mnuLoad_xml.Click += new System.EventHandler(this.mnuLoad_xml_Click);
            // 
            // mnuLoad_dat
            // 
            this.mnuLoad_dat.Name = "mnuLoad_dat";
            this.mnuLoad_dat.Size = new System.Drawing.Size(231, 22);
            this.mnuLoad_dat.Text = "Reload from the encrypted file";
            this.mnuLoad_dat.Click += new System.EventHandler(this.mnuLoad_dat_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(228, 6);
            this.toolStripSeparator3.Visible = false;
            // 
            // mnuUnCheck
            // 
            this.mnuUnCheck.Name = "mnuUnCheck";
            this.mnuUnCheck.Size = new System.Drawing.Size(231, 22);
            this.mnuUnCheck.Text = "Uncheck all nodes";
            this.mnuUnCheck.Visible = false;
            this.mnuUnCheck.Click += new System.EventHandler(this.mnuUnCheck_Click);
            // 
            // treeImageList
            // 
            this.treeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("treeImageList.ImageStream")));
            this.treeImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.treeImageList.Images.SetKeyName(0, "unchek_256.png");
            this.treeImageList.Images.SetKeyName(1, "chek_256.png");
            // 
            // splTree
            // 
            this.splTree.Location = new System.Drawing.Point(183, 24);
            this.splTree.Name = "splTree";
            this.splTree.Size = new System.Drawing.Size(3, 323);
            this.splTree.TabIndex = 1;
            this.splTree.TabStop = false;
            // 
            // menuItem1
            // 
            this.menuItem1.Index = -1;
            this.menuItem1.Text = "www";
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tpConfig);
            this.tabs.Controls.Add(this.tpData);
            this.tabs.Controls.Add(this.tpStat);
            this.tabs.Controls.Add(this.tpScripts);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.ItemSize = new System.Drawing.Size(84, 18);
            this.tabs.Location = new System.Drawing.Point(186, 24);
            this.tabs.Name = "tabs";
            this.tabs.Padding = new System.Drawing.Point(20, 3);
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(448, 323);
            this.tabs.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabs.TabIndex = 5;
            this.tabs.SelectedIndexChanged += new System.EventHandler(this.tabs_SelectedIndexChanged);
            // 
            // tpConfig
            // 
            this.tpConfig.Controls.Add(this.grConfig);
            this.tpConfig.Location = new System.Drawing.Point(4, 22);
            this.tpConfig.Name = "tpConfig";
            this.tpConfig.Padding = new System.Windows.Forms.Padding(1);
            this.tpConfig.Size = new System.Drawing.Size(440, 297);
            this.tpConfig.TabIndex = 0;
            this.tpConfig.Text = "Config";
            // 
            // grConfig
            // 
            this.grConfig.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grConfig.ContextMenuStrip = this.cmList;
            this.grConfig.DataMember = "";
            this.grConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grConfig.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grConfig.Location = new System.Drawing.Point(1, 1);
            this.grConfig.Name = "grConfig";
            this.grConfig.RowHeaderWidth = 25;
            this.grConfig.Size = new System.Drawing.Size(438, 295);
            this.grConfig.TabIndex = 1;
            // 
            // tpData
            // 
            this.tpData.Controls.Add(this.Panel6);
            this.tpData.Location = new System.Drawing.Point(4, 22);
            this.tpData.Name = "tpData";
            this.tpData.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.tpData.Size = new System.Drawing.Size(440, 297);
            this.tpData.TabIndex = 1;
            this.tpData.Text = "Data";
            this.tpData.Visible = false;
            // 
            // Panel6
            // 
            this.Panel6.Controls.Add(this.grData);
            this.Panel6.Controls.Add(this.splBottom);
            this.Panel6.Controls.Add(this.dataResult);
            this.Panel6.Controls.Add(this.bn);
            this.Panel6.Controls.Add(this.splData);
            this.Panel6.Controls.Add(this.txtQuery);
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel6.Location = new System.Drawing.Point(1, 0);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(439, 297);
            this.Panel6.TabIndex = 1;
            // 
            // grData
            // 
            this.grData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grData.DataMember = "";
            this.grData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grData.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grData.Location = new System.Drawing.Point(0, 73);
            this.grData.Name = "grData";
            this.grData.Size = new System.Drawing.Size(439, 177);
            this.grData.TabIndex = 8;
            // 
            // splBottom
            // 
            this.splBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splBottom.Location = new System.Drawing.Point(0, 250);
            this.splBottom.Name = "splBottom";
            this.splBottom.Size = new System.Drawing.Size(439, 2);
            this.splBottom.TabIndex = 7;
            this.splBottom.TabStop = false;
            // 
            // dataResult
            // 
            this.dataResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataResult.Location = new System.Drawing.Point(0, 252);
            this.dataResult.MaxLength = 65534;
            this.dataResult.MinimumSize = new System.Drawing.Size(0, 45);
            this.dataResult.Multiline = true;
            this.dataResult.Name = "dataResult";
            this.dataResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataResult.Size = new System.Drawing.Size(439, 45);
            this.dataResult.TabIndex = 6;
            // 
            // bn
            // 
            this.bn.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bn.CountItem = this.bindingNavigatorCountItem;
            this.bn.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bn.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.bn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bnSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bnSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.toolStripSeparator5,
            this.bnUndo,
            this.bnSave,
            this.bnFillHoles});
            this.bn.Location = new System.Drawing.Point(0, 48);
            this.bn.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bn.MoveLastItem = this.bnMoveLastItem;
            this.bn.MoveNextItem = this.bnMoveNextItem;
            this.bn.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bn.Name = "bn";
            this.bn.PositionItem = this.bindingNavigatorPositionItem;
            this.bn.Size = new System.Drawing.Size(439, 25);
            this.bn.TabIndex = 5;
            this.bn.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(45, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bnSeparator1
            // 
            this.bnSeparator1.Name = "bnSeparator1";
            this.bnSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bnMoveNextItem
            // 
            this.bnMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
            this.bnMoveNextItem.Name = "bnMoveNextItem";
            this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveNextItem.Text = "Move next";
            // 
            // bnMoveLastItem
            // 
            this.bnMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
            this.bnMoveLastItem.Name = "bnMoveLastItem";
            this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bnMoveLastItem.Text = "Move last";
            // 
            // bnSeparator2
            // 
            this.bnSeparator2.Name = "bnSeparator2";
            this.bnSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // bnUndo
            // 
            this.bnUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnUndo.Enabled = false;
            this.bnUndo.Image = global::ProductionMonitor.Properties.Resources._undo;
            this.bnUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnUndo.Name = "bnUndo";
            this.bnUndo.Size = new System.Drawing.Size(23, 22);
            this.bnUndo.Text = "toolStripButton1";
            this.bnUndo.ToolTipText = "Reject all changes";
            this.bnUndo.Click += new System.EventHandler(this.bindingNavigatorUndo_Click);
            // 
            // bnSave
            // 
            this.bnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnSave.Enabled = false;
            this.bnSave.Image = global::ProductionMonitor.Properties.Resources._save;
            this.bnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnSave.Name = "bnSave";
            this.bnSave.Size = new System.Drawing.Size(23, 22);
            this.bnSave.Text = "toolStripButton1";
            this.bnSave.ToolTipText = "Save to database";
            this.bnSave.Click += new System.EventHandler(this.bindingNavigatorSave_Click);
            // 
            // bnFillHoles
            // 
            this.bnFillHoles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bnFillHoles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bnFillHoles.Name = "bnFillHoles";
            this.bnFillHoles.Size = new System.Drawing.Size(23, 22);
            this.bnFillHoles.Text = "Fill Holes";
            this.bnFillHoles.ToolTipText = "Заполнение пробелов в таблице l_rozliv";
            this.bnFillHoles.Click += new System.EventHandler(this.bnFillHoles_Click);
            // 
            // splData
            // 
            this.splData.Dock = System.Windows.Forms.DockStyle.Top;
            this.splData.Location = new System.Drawing.Point(0, 45);
            this.splData.Name = "splData";
            this.splData.Size = new System.Drawing.Size(439, 3);
            this.splData.TabIndex = 4;
            this.splData.TabStop = false;
            // 
            // txtQuery
            // 
            this.txtQuery.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtQuery.Location = new System.Drawing.Point(0, 0);
            this.txtQuery.MinimumSize = new System.Drawing.Size(0, 45);
            this.txtQuery.Multiline = true;
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(439, 45);
            this.txtQuery.TabIndex = 1;
            // 
            // tpStat
            // 
            this.tpStat.Controls.Add(this.grStat);
            this.tpStat.Controls.Add(this.splStat);
            this.tpStat.Controls.Add(this.txtStat);
            this.tpStat.Location = new System.Drawing.Point(4, 22);
            this.tpStat.Name = "tpStat";
            this.tpStat.Size = new System.Drawing.Size(440, 297);
            this.tpStat.TabIndex = 4;
            this.tpStat.Text = "Statistic";
            // 
            // grStat
            // 
            this.grStat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grStat.CaptionFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.grStat.DataMember = "";
            this.grStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grStat.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grStat.Location = new System.Drawing.Point(0, 48);
            this.grStat.Name = "grStat";
            this.grStat.ReadOnly = true;
            this.grStat.Size = new System.Drawing.Size(440, 249);
            this.grStat.TabIndex = 2;
            // 
            // splStat
            // 
            this.splStat.Dock = System.Windows.Forms.DockStyle.Top;
            this.splStat.Location = new System.Drawing.Point(0, 45);
            this.splStat.Name = "splStat";
            this.splStat.Size = new System.Drawing.Size(440, 3);
            this.splStat.TabIndex = 1;
            this.splStat.TabStop = false;
            // 
            // txtStat
            // 
            this.txtStat.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtStat.Location = new System.Drawing.Point(0, 0);
            this.txtStat.Multiline = true;
            this.txtStat.Name = "txtStat";
            this.txtStat.Size = new System.Drawing.Size(440, 45);
            this.txtStat.TabIndex = 0;
            // 
            // tpScripts
            // 
            this.tpScripts.Controls.Add(this.txtScript);
            this.tpScripts.Controls.Add(this.splitter1);
            this.tpScripts.Controls.Add(this.scriptResult);
            this.tpScripts.Location = new System.Drawing.Point(4, 22);
            this.tpScripts.Name = "tpScripts";
            this.tpScripts.Size = new System.Drawing.Size(440, 297);
            this.tpScripts.TabIndex = 3;
            this.tpScripts.Text = "Scripts";
            this.tpScripts.Visible = false;
            // 
            // txtScript
            // 
            this.txtScript.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtScript.Location = new System.Drawing.Point(0, 0);
            this.txtScript.MaxLength = 65534;
            this.txtScript.Multiline = true;
            this.txtScript.Name = "txtScript";
            this.txtScript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtScript.Size = new System.Drawing.Size(440, 250);
            this.txtScript.TabIndex = 9;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 250);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(440, 2);
            this.splitter1.TabIndex = 8;
            this.splitter1.TabStop = false;
            // 
            // scriptResult
            // 
            this.scriptResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.scriptResult.Location = new System.Drawing.Point(0, 252);
            this.scriptResult.MaxLength = 65534;
            this.scriptResult.MinimumSize = new System.Drawing.Size(0, 45);
            this.scriptResult.Multiline = true;
            this.scriptResult.Name = "scriptResult";
            this.scriptResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.scriptResult.Size = new System.Drawing.Size(440, 45);
            this.scriptResult.TabIndex = 7;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.mnuEdit,
            this.mnuAbout});
            this.menuStrip1.Location = new System.Drawing.Point(3, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(752, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuReload,
            this.mnuSettings,
            this.toolStripSeparator1,
            this.mnuExit});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // mnuReload
            // 
            this.mnuReload.Name = "mnuReload";
            this.mnuReload.Size = new System.Drawing.Size(150, 22);
            this.mnuReload.Text = "Configuration";
            // 
            // mnuSettings
            // 
            this.mnuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTimeConnection,
            this.mnuTimeExec});
            this.mnuSettings.Name = "mnuSettings";
            this.mnuSettings.Size = new System.Drawing.Size(150, 22);
            this.mnuSettings.Text = "Settings";
            // 
            // mnuTimeConnection
            // 
            this.mnuTimeConnection.Name = "mnuTimeConnection";
            this.mnuTimeConnection.Size = new System.Drawing.Size(190, 22);
            this.mnuTimeConnection.Text = "Timeout connection...";
            this.mnuTimeConnection.Click += new System.EventHandler(this.mnuTimeConnection_Click);
            // 
            // mnuTimeExec
            // 
            this.mnuTimeExec.Name = "mnuTimeExec";
            this.mnuTimeExec.Size = new System.Drawing.Size(190, 22);
            this.mnuTimeExec.Text = "Timeout execution...";
            this.mnuTimeExec.Click += new System.EventHandler(this.mnuTimeExecution_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(147, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(150, 22);
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(60, 20);
            this.mnuAbout.Text = "About...";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // mnuFileToolStripMenuItem
            // 
            this.mnuFileToolStripMenuItem.Name = "mnuFileToolStripMenuItem";
            this.mnuFileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.mnuFileToolStripMenuItem.Text = "File";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // statusStrip1
            // 
            this.statusStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spCommon,
            this.spOperation,
            this.progrBar});
            this.statusStrip1.Location = new System.Drawing.Point(3, 347);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.statusStrip1.Size = new System.Drawing.Size(752, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // spCommon
            // 
            this.spCommon.AutoSize = false;
            this.spCommon.Name = "spCommon";
            this.spCommon.Size = new System.Drawing.Size(180, 17);
            // 
            // spOperation
            // 
            this.spOperation.AutoSize = false;
            this.spOperation.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.spOperation.Name = "spOperation";
            this.spOperation.Size = new System.Drawing.Size(557, 17);
            this.spOperation.Spring = true;
            this.spOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progrBar
            // 
            this.progrBar.MarqueeAnimationSpeed = 50;
            this.progrBar.Name = "progrBar";
            this.progrBar.Size = new System.Drawing.Size(200, 16);
            this.progrBar.Visible = false;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // dataWorker
            // 
            this.dataWorker.WorkerSupportsCancellation = true;
            this.dataWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.dataWorker_DoWork);
            this.dataWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.dataWorker_RunWorkerCompleted);
            // 
            // statWorker
            // 
            this.statWorker.WorkerReportsProgress = true;
            this.statWorker.WorkerSupportsCancellation = true;
            this.statWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.statWorker_DoWork);
            this.statWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.statWorker_RunWorkerCompleted);
            this.statWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.statWorker_ProgressChanged);
            // 
            // scriptWorker
            // 
            this.scriptWorker.WorkerReportsProgress = true;
            this.scriptWorker.WorkerSupportsCancellation = true;
            this.scriptWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.scriptWorker_DoWork);
            this.scriptWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.scriptWorker_RunWorkerCompleted);
            this.scriptWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.scriptWorker_ProgressChanged);
            // 
            // frmManager
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(758, 371);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.splTree);
            this.Controls.Add(this.pnTree);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "frmManager";
            this.Padding = new System.Windows.Forms.Padding(3, 0, 3, 2);
            this.Text = "Production Monitor Manager";
            this.Load += new System.EventHandler(this.Form_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmConfig_Closing);
            this.Panel2.ResumeLayout(false);
            this.cmList.ResumeLayout(false);
            this.pnTree.ResumeLayout(false);
            this.cmTree.ResumeLayout(false);
            this.tabs.ResumeLayout(false);
            this.tpConfig.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grConfig)).EndInit();
            this.tpData.ResumeLayout(false);
            this.Panel6.ResumeLayout(false);
            this.Panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn)).EndInit();
            this.bn.ResumeLayout(false);
            this.bn.PerformLayout();
            this.tpStat.ResumeLayout(false);
            this.tpStat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grStat)).EndInit();
            this.tpScripts.ResumeLayout(false);
            this.tpScripts.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		#region | Config Declaration    |

		const string STCFG = "pm.cfg";
		const string STXML = "pm.xml";
		private DataSet dsConfig;
        private DataSet dsConfig_counters;
		private DataSet dsTemplates;
		private DataTable tb = new DataTable();
		private DataTable tbQueries, tbStats, tbScripts;
		private DataSet ds = new DataSet();
		private SqlDataAdapter da;
		private SqlCommandBuilder cb;
		private DateTime m_DT;
		//private frmSplash spl;
		//List<TreeNode> unitstree = new List<TreeNode>();
		private string strTimeOut = "";
		private int ConnectionTimeOut = 1;
		private int CommandTimeOut = 30;
		private bool InUse;
		private Config config;
        private ConfigCounters config_counters;
		private Dictionary<Unit, TreeNode> unitstree = new Dictionary<Unit, TreeNode>();
		private bool allowsavedat;

		#endregion

		#region | At Start/Stop Events  |

		private void Form_Load(Object sender, EventArgs e)
		{
			Splash.Fadeout();

			ToolStripManager.Renderer = new ToolStripProfessionalRenderer(new CustomProfessionalColors());

			string winuser = Environment.UserDomainName.ToLower() + Environment.UserName.ToLower();
            if (winuser == "vxsuser")
				allowsavedat = true;

			Width = 1024;
			Height = 512;
			if (Screen.PrimaryScreen.Bounds.Width <= 1024)
				Left = 0;
			LoadLines();
			LoadScrips();
			mnuReload.DropDown = cmTree;
			mnuEdit.DropDown = cmList;
			tabs_SelectedIndexChanged(tpConfig, null);
			list.SelectedIndex = 0;
			AddCustomDataTableStyle();
			m_DT = DateTime.Now.Date;
			Thread.Sleep(10);
			////spl.HideWindow();
		}
		private void LoadLines()
		{
		//	try {
            dsConfig = new DataSet();

            DataTable tbL = new DataTable("Unit");
            DataTable tbSrv = new DataTable("Serv");

            DataColumn col = tbL.Columns.Add("id", typeof(int));

            // line
            col.Unique = true;
            col.AutoIncrement = true;
            col = tbL.Columns.Add("pid", typeof(int));
            col.AllowDBNull = true;
            col = tbL.Columns.Add("name", typeof(string));
            col.AllowDBNull = false;
            col = tbL.Columns.Add("srv", typeof(int));
            //col.AllowDBNull = false;
         col = tbL.Columns.Add("db", typeof(string));
         //col.AllowDBNull = false;
            col = tbL.Columns.Add("gkpi", typeof(bool));
            col = tbL.Columns.Add("gdwn", typeof(bool));
            col = tbL.Columns.Add("gcfg", typeof(bool));
            col = tbL.Columns.Add("gsku", typeof(bool));
            col = tbL.Columns.Add("gshf", typeof(bool));
            col = tbL.Columns.Add("gstore", typeof(int));
            col = tbL.Columns.Add("sort", typeof(int));

            // servers
            col = tbSrv.Columns.Add("ID", typeof(int));
            col.Unique = true;
            col.AutoIncrement = true;
            col = tbSrv.Columns.Add("Serv", typeof(string));
            col.AllowDBNull = false;
            col = tbSrv.Columns.Add("Conn", typeof(string));
            col.AllowDBNull = false;

            dsConfig.Tables.Add(tbL);
            dsConfig.Tables.Add(tbSrv);

            dsConfig.Relations.Add("ServLine", tbSrv.Columns["id"], tbL.Columns["srv"]);
            dsConfig.Relations.Add("ID_ParentID", tbL.Columns["id"], tbL.Columns["pid"]);
            dsConfig.DataSetName = "Sites";


			if (File.Exists(STXML))
				using (FileStream fs = new FileStream(STXML, FileMode.Open)) {
					// dsSiteLine.ReadXml(fs, XmlReadMode.ReadSchema);
					dsConfig.ReadXml(fs, XmlReadMode.IgnoreSchema);
					config = new Config(dsConfig, tree.Nodes);
				}

			GetUnitTree(tree.Nodes);
			dsConfig.AcceptChanges();
			grConfig.DataSource = dsConfig.Tables["Unit"];
			//} catch (Exception ex) {
			//   MessageBox.Show(ex.Message);
			//}
		}
		private void LoadScrips()
		{
			try {
				dsTemplates = new DataSet();
				DataColumn col;

				tbQueries = new DataTable("Query");
				col = tbQueries.Columns.Add("Query", typeof(string));
				col.Unique = true;
				tbQueries.Columns.Add("Comment", typeof(string));
				tbQueries.Columns.Add("Content", typeof(string));
				dsTemplates.Tables.Add(tbQueries);

				tbStats = new DataTable("Stats");
				col = tbStats.Columns.Add("Stat", typeof(string));
				col.Unique = true;
				tbStats.Columns.Add("Comment", typeof(string));
				tbStats.Columns.Add("Content", typeof(string));
				dsTemplates.Tables.Add(tbStats);

				tbScripts = new DataTable("Script");
				col = tbScripts.Columns.Add("Script", typeof(string));
				col.Unique = true;
				tbScripts.Columns.Add("Comment", typeof(string));
				tbScripts.Columns.Add("Content", typeof(string));
				dsTemplates.Tables.Add(tbScripts);

				if (File.Exists("templates.xml")) {
					using (FileStream fs = new FileStream("templates.xml", FileMode.Open)) {
						dsTemplates.ReadXml(fs, XmlReadMode.ReadSchema);
						tbQueries = dsTemplates.Tables["Query"];
						tbScripts = dsTemplates.Tables["Script"];
					}
				}

				txtQuery.DataBindings.Add("Text", tbQueries, "Content");
				txtStat.DataBindings.Add("Text", tbStats, "Content");
				txtScript.DataBindings.Add("Text", tbScripts, "Content");
				dsTemplates.AcceptChanges();
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}
		private void AddCustomDataTableStyle()
		{

			//// Стиль для таблицы Site
			//DataGridTableStyle tsSite = new DataGridTableStyle();
			//tsSite.MappingName = "Site";
			//grLines.TableStyles.Add(tsSite);

			//DataGridTextBoxColumn col = new DataGridTextBoxColumn();
			//col.MappingName = "ID";
			//col.HeaderText = "ID";
			//tsSite.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "St";
			//col.HeaderText = "State";
			//tsSite.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Site";
			//col.HeaderText = "Site";
			//col.Width = 200;
			//tsSite.GridColumnStyles.Add(col);

			////Стиль для таблицы Line
			//DataGridTableStyle tsLine = new DataGridTableStyle();
			//tsLine.MappingName = "Line";
			//grLines.TableStyles.Add(tsLine);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "ID";
			//col.HeaderText = "ID";
			//tsLine.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Site";
			//col.HeaderText = "Site";
			//tsLine.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Serv";
			//col.HeaderText = "Server";
			//tsLine.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Name";
			//col.HeaderText = "Line";
			//col.Width = 100;
			//tsLine.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "LName";
			//col.HeaderText = "LocalName";
			//col.Width = 100;
			//tsLine.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "DB";
			//col.HeaderText = "Database";
			//col.Width = 100;
			//tsLine.GridColumnStyles.Add(col);

			//// Стиль для таблицы Serv
			//DataGridTableStyle tsServ = new DataGridTableStyle();
			//tsServ.MappingName = "Serv";
			//grLines.TableStyles.Add(tsServ);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "ID";
			//col.HeaderText = "ID";
			//tsServ.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Serv";
			//col.HeaderText = "Serv";
			//tsServ.GridColumnStyles.Add(col);

			//col = new DataGridTextBoxColumn();
			//col.MappingName = "Conn";
			//col.HeaderText = "Conn";
			//col.Width = 500;
			//tsServ.GridColumnStyles.Add(col);

		} //AddCustomDataTableStyle
		private void frmConfig_Closing(object sender, CancelEventArgs e)
		{
			if (dsConfig.GetChanges() != null) {
				DialogResult res = MessageBox.Show("The config data was changed. Do you wish to exit without saving?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (res == DialogResult.No)
					e.Cancel = true;
			}
			if (tbScripts.GetChanges() != null) {
				DialogResult res = MessageBox.Show("The file with scripts was changed. Do you wish to exit without saving?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (res == DialogResult.No)
					e.Cancel = true;
			}
			if (tbStats.GetChanges() != null) {
				DialogResult res = MessageBox.Show("The file with statistic scripts was changed. Do you wish to exit without saving?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (res == DialogResult.No)
					e.Cancel = true;
			}
		}
		private void GetUnitTree(TreeNodeCollection nc)
		{
			foreach (TreeNode tn in nc) {
				unitstree.Add((Unit)tn.Tag, tn);
				if (tn.Nodes.Count > 0)
					GetUnitTree(tn.Nodes);
			}
		}

		#endregion

		#region | Menus and Buttons     |

		private void mnuExit_Click(Object sender, EventArgs e)
		{
			Close();
		}
		private void mnuAbout_Click(Object sender, EventArgs e)
		{
			string cpr = "";
			Splash.ShowSplash(300, C.GetAssemblyShortInfo(System.Reflection.Assembly.GetExecutingAssembly(), ref cpr), cpr);

		}
		private void nmuSaveDat_Click(Object sender, EventArgs e)
		{
#if DEBUG
			byte[] V = new byte[] { 240, 179, 254, 93, 132, 104, 83, 205 };
			byte[] K = new byte[] { 216, 30, 31, 1, 225, 95, 3, 134 };
			using (FileStream fs = new FileStream(STCFG, FileMode.Create)) {
				
				#region | As it was before	|
				//			byte[] V = new byte[8];
				//			byte[] K = new byte[8];
				//			String str = C.res.GetString("s1");
				//			int n1 = (int) C.res.GetString("s1").GetHashCode();
				//			for (int n = 0; n < 4; n++)
				//			{
				//				V[n] = (byte) (n1 % 255);
				//				n1 = (int) n1 / 255;
				//			}
				//			n1 = (int) C.res.GetString("s2").GetHashCode();
				//			for (int n = 4; n < 8; n++)
				//			{
				//				V[n] = (byte) (n1 % 255);
				//				n1 = (int) n1 / 255;
				//			}
				//			n1 = (int) C.res.GetString("s3").GetHashCode();
				//			for (int n = 0; n < 4; n++)
				//			{
				//				K[n] = (byte) (n1 % 255);
				//				n1 = (int) n1 / 255;
				//			}
				//			n1 = (int) C.res.GetString("s4").GetHashCode();
				//			for (int n = 4; n < 8; n++)
				//			{
				//				K[n] = (byte) (n1 % 255);
				//				n1 = (int) n1 / 255;
				//			}
				#endregion

				using (SymmetricAlgorithm Alg = new DESCryptoServiceProvider()) {
					ICryptoTransform Cryptor = Alg.CreateEncryptor(K, V);
					using (CryptoStream CrStream = new CryptoStream(fs, Cryptor, CryptoStreamMode.Write)) {
						using (StringWriter sw = new StringWriter()) {
							dsConfig.WriteXml(sw, XmlWriteMode.WriteSchema);
							string xmlStr = sw.ToString();
							byte[] b = System.Text.Encoding.UTF8.GetBytes(xmlStr);
							// Шифруем
							CrStream.Write(b, 0, b.Length);
						}
					}
				}
			}
#endif
		}
		private void mnuSaveXml_Click(Object sender, EventArgs e)
		{
			dsConfig.WriteXml(STXML, System.Data.XmlWriteMode.WriteSchema);
			dsConfig.AcceptChanges();
		}
		private void mnuLoad_ds_Click(object sender, EventArgs e)
		{
			config          = new Config(dsConfig, tree.Nodes);
            config_counters = new ConfigCounters(dsConfig_counters, tree2.Nodes);
		}
		private void mnuLoad_dat_Click(Object sender, EventArgs e)
		{

			tree.Nodes.Clear();
			Config config = new Config();
			config.LoadTree(tree.Nodes, 0);
           
            tree.Nodes.Clear();
            ConfigCounters config_counters = new ConfigCounters();
            config.LoadTree(tree.Nodes, 0);

			unitstree.Clear();
			GetUnitTree(tree.Nodes);
			dsConfig = config.DS;
			dsConfig.AcceptChanges();
			grConfig.DataSource = dsConfig.Tables[0];


            if ( !dsConfig.Tables[0].Columns.Contains("sort"))
            {
              dsConfig.Tables[0].Columns.Add("sort", typeof(int));
            }
		}
		private void mnuLoad_xml_Click(Object sender, EventArgs e)
		{
			LoadLines();
		}
		private void mnuSaveScript_Click(Object sender, EventArgs e)
		{
			tbQueries.RejectChanges();
			dsTemplates.WriteXml("templates.xml", System.Data.XmlWriteMode.IgnoreSchema);
			dsTemplates.AcceptChanges();
		}
		private void mnuUnCheck_Click(Object sender, EventArgs e)
		{
			foreach (TreeNode n in tree.Nodes)
				n.Checked = false;
		}
		private void cmTree_Opening(object sender, CancelEventArgs e)
		{
			mnuUnCheck.Enabled = tabs.SelectedTab.Text == "Scripts" || tabs.SelectedTab.Text == "Statistic";
		}
		private void cmList_Opening(object sender, CancelEventArgs e)
		{
			bool s = tabs.SelectedTab.Text == "Scripts" || tabs.SelectedTab.Text == "Statistic";
			mnuAddScript.Visible = s;
			mnuRename.Visible = s;
			mnuDelete.Visible = s;
			mnuSave_xml.Enabled = dsConfig.GetChanges() != null;
			mnuSaveScript.Enabled = tbScripts.GetChanges() != null || tbStats.GetChanges() != null;
			switch (tabs.SelectedTab.Text) {
				case "Config":
					mnuReject.Enabled = dsConfig.GetChanges() != null;
					mnuSave_dat.Visible = allowsavedat;
					mnuSave_xml.Visible = true;
					mnuSaveScript.Visible = false;
					break;
				case "Data":
					mnuReject.Enabled = tbQueries.GetChanges() != null;
					mnuSave_dat.Visible = false;
					mnuSave_xml.Visible = false;
					mnuSaveScript.Visible = false;
					break;
				case "Statistic":
					mnuReject.Enabled = tbStats.GetChanges() != null;
					mnuDelete.Enabled = list.Items.Count > 0;
					mnuRename.Enabled = list.Items.Count > 0;
					mnuSave_dat.Visible = false;
					mnuSave_xml.Visible = false;
					mnuSaveScript.Visible = true;
					break;
				case "Scripts":
					mnuReject.Enabled = tbScripts.GetChanges() != null;
					mnuDelete.Enabled = list.Items.Count > 0;
					mnuRename.Enabled = list.Items.Count > 0;
					mnuSave_dat.Visible = false;
					mnuSave_xml.Visible = false;
					mnuSaveScript.Visible = true;
					break;
			}
		}
		private void mnuTimeConnection_Click(object sender, EventArgs e)
		{
			using (InputForm f = new InputForm("Enter timeout connections, seconds", ConnectionTimeOut, 1, 30)) {
				f.ShowDialog(this);
				if (f.DialogResult != DialogResult.OK)
					return;
				ConnectionTimeOut = f.ResN;
				strTimeOut = ";timeout=" + ConnectionTimeOut;
			}
		}
		private void mnuTimeExecution_Click(object sender, EventArgs e)
		{
			string s = "Enter timeout for command execution, seconds";
			using (InputForm f = new InputForm(s, CommandTimeOut, 10, 200)) {
				f.ShowDialog(this);
				if (f.DialogResult == DialogResult.OK)
					CommandTimeOut = f.ResN;
			}
		}
		private void mnuAddTScript_Click(object sender, EventArgs e)
		{
			try {
				switch (tabs.SelectedTab.Text) {
					case "Data":
						DataRow r = tbQueries.NewRow();
						using (InputForm f = new InputForm("Enter the name for new query")) {
							f.ShowDialog(this);
							if (f.DialogResult != DialogResult.OK)
								return;
							r["Query"] = f.ResS;
						}
						r["Content"] = "-- Enter query content here";
						tbQueries.Rows.Add(r);
						break;
					case "Statistic":
						DataRow r2 = tbStats.NewRow();
						using (InputForm f2 = new InputForm("Enter the name for new query")) {
							f2.ShowDialog(this);
							if (f2.DialogResult != DialogResult.OK)
								return;
							r2["Stat"] = f2.ResS;
						}
						r2["Content"] = "-- Enter query content here";
						tbStats.Rows.Add(r2);
						break;
					case "Scripts":
						DataRow r1 = tbScripts.NewRow();
						using (InputForm f1 = new InputForm("Enter the name for new script")) {
							f1.ShowDialog(this);
							if (f1.DialogResult != DialogResult.OK)
								return;
							r1["Script"] = f1.ResS;
						}
						r1["Content"] = "-- Enter script content here";
						tbScripts.Rows.Add(r1);
						break;
					default:
						break;
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}
		private void mnuRename_Click(object sender, EventArgs e)
		{
			try {
				if (tabs.SelectedTab.Text == "Scripts") {
					DataRow r = tbScripts.Rows[BindingContext[tbScripts].Position];
					String scr = r["Script"].ToString();
					using (InputForm f = new InputForm("Enter new name:", scr)) {
						f.ShowDialog(this);
						if (f.DialogResult == DialogResult.OK)
							r["Script"] = f.ResS;
					}
				} else {
					DataRow r = tbStats.Rows[BindingContext[tbStats].Position];
					String scr = r["Stat"].ToString();
					using (InputForm f = new InputForm("Enter new name:", scr)) {
						f.ShowDialog(this);
						if (f.DialogResult == DialogResult.OK)
							r["Stat"] = f.ResS;
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}
		private void mnuDelete_Click(object sender, EventArgs e)
		{
			try {
				if (tabs.SelectedTab.Text == "Scripts") {
					//DataRow r = tbScripts.Rows[BindingContext[tbScripts].Position];
					DataRow r = tbScripts.Select("", "", DataViewRowState.Unchanged | DataViewRowState.Added |
						 DataViewRowState.ModifiedOriginal)[BindingContext[tbScripts].Position];
					r.Delete();
				} else {
					//DataRow r = tbStats.Rows[BindingContext[tbStats].Position];
					DataRow r = tbStats.Select("", "", DataViewRowState.Unchanged | DataViewRowState.Added |
						 DataViewRowState.ModifiedOriginal)[BindingContext[tbStats].Position];
					r.Delete();
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}

		}
		private void mnuReject_Click(object sender, EventArgs e)
		{
			switch (tabs.SelectedTab.Text) {
				case "Config":
					dsConfig.RejectChanges();
					break;
				case "Data":
					tbQueries.RejectChanges();
					break;
				case "Statistic":
					tbStats.RejectChanges();
					break;
				case "Scripts":
					tbScripts.RejectChanges();
					break;
			}
		}
		private void bindingNavigatorUndo_Click(object sender, EventArgs e)
		{
			if (ds != null)
				ds.RejectChanges();
		}
		private void bindingNavigatorSave_Click(object sender, EventArgs e)
		{
			try {
				//Если это таблица розлива, то строим команду ручками чтобы побыстрее работало
				if (ds.Tables[0].Columns.Contains("DT") && ds.Tables[0].Columns.Contains("Gap")) {
					da.InsertCommand = new SqlCommand("INSERT INTO l_rozliv(DT, Bottles, Seconds, Gap) VALUES (@p1, @p2, @p3, @p4)");
					da.InsertCommand.Parameters.Add("@p1", SqlDbType.SmallDateTime, 4, "DT");
					da.InsertCommand.Parameters.Add("@p2", SqlDbType.SmallInt, 4, "Bottles");
					da.InsertCommand.Parameters.Add("@p3", SqlDbType.TinyInt, 4, "Seconds");
					da.InsertCommand.Parameters.Add("@p4", SqlDbType.Int, 4, "Gap");

					da.UpdateBatchSize = 1000;
					da.InsertCommand.UpdatedRowSource = UpdateRowSource.None;
				}

				dataResult.Text = "Updated records: " + da.Update(ds);
			} catch (Exception ex) {
				dataResult.Text = ex.Message;
			}
		}
		private void bnFillHoles_Click(object sender, EventArgs e)
		{
			//try {

				DataTable dtb = ds.Tables[0];
				DataTable newrows = dtb.Clone();
				if (!dtb.Columns.Contains("DT") ||
					!dtb.Columns.Contains("bottles") ||
					!dtb.Columns.Contains("seconds") ||
					!dtb.Columns.Contains("gap"))
					return;

				DateTime pDT = new DateTime(1,1,1), DT;
				Int16 pBott = 0, Bott;
				Byte pSecs = 0, Secs;
				int pGap = 0, Gap;
				bool firstrec = true;

				int diff;
				DataRow newRow;
				//List<DataRow> DR = new List<DataRow>();
				DataRow[] allrows = dtb.Select("", "DT");
				foreach (DataRow dr in allrows) {
					DT = (DateTime)dr["DT"];
					Bott = (Int16)dr["Bottles"];
					Secs = (Byte)dr["Seconds"];
					Gap = (int)dr["Gap"];
					diff = (int)Math.Round(DT.Subtract(pDT).TotalMinutes);
					if (diff == 1 || firstrec) {
						firstrec = false;
					} else {
						for (int n = 1; n < diff; n++) {
							newRow = newrows.NewRow();
							newRow["DT"] = pDT.AddMinutes(n);
							newRow["Bottles"] = pBott;
							newRow["Seconds"] = pSecs;
							newRow["Gap"] = pGap;
							//DR.Add(newRow);
							newrows.Rows.Add(newRow);
						}
					}
					pDT = DT;
					pBott = Bott;
					pSecs = Secs;
					pGap = Gap;
				}

				dtb.BeginLoadData();
				//////foreach (DataRow dr in DR)
				//////   dtb.Rows.Add(dr);
				dtb.Merge(newrows,false);
				dtb.EndLoadData();
				//dtb.Load(newrows.CreateDataReader(), LoadOption.OverwriteChanges);

				//dataResult.Text = "Updated records: " + da.Update(ds).ToString();
			//} catch (Exception ex) {
			//   dataResult.Text = ex.Message;
			//}
		}

		#endregion

		private void tree_AfterCheck(object sender, TreeViewEventArgs e)
		{
			bool Cheked = e.Node.Checked;
			e.Node.BackColor = Color.White;
			foreach (TreeNode n in e.Node.Nodes)
				n.Checked = Cheked;
		}

		private static string GetNextScript(ref string str)
		{
			string strOut = string.Empty;
			string lower = str.ToLower();
			string delimStr = Environment.NewLine + "go";
			int n = lower.IndexOf(delimStr);
			if (n > 0) {
				strOut = str.Substring(0, n);
				str = str.Substring(n + 4);
			} else {
				if (str.Length > 5)
					strOut = str;
				str = string.Empty;
			}
			return strOut;
		}
		private void tabs_SelectedIndexChanged(object sender, EventArgs e)
		{
			//SuspendLayout();
			//DoubleBuffered = true;
			//tree.SuspendLayout();
			switch (tabs.SelectedTab.Text) {
				case "Config":
					btExec.Visible = false;
					tree.CheckBoxes = false;
					list.DataSource = null;
					list.Items.Clear();
					list.Items.Add("Lines");
					list.Items.Add("Servers");
					break;
				case "Data":
					btExec.Visible = true;
					tree.CheckBoxes = false;
					list.DataSource = tbQueries;
					list.DisplayMember = "Query";
					break;
				case "Statistic":
					btExec.Visible = true;
					tree.CheckBoxes = true;
					list.DataSource = tbStats;
					list.DisplayMember = "Stat";
					break;
				case "Scripts":
					btExec.Visible = true;
					tree.CheckBoxes = true;
					list.DataSource = tbScripts;
					list.DisplayMember = "Script";
					break;
			}
			//foreach (TreeNode n in tree.Nodes)
			//   n.Expand();
			if (tree.SelectedNode != null)
				tree.SelectedNode.EnsureVisible();
			////if (!scriptWorker.IsBusy)
			////   for (int n = 0; n < unitstree.Count; n++)
			////      if (!unitstree[n].BackColor.Equals(Color.White))
			////         unitstree[n].BackColor = Color.White;
			if (!scriptWorker.IsBusy)
				foreach (TreeNode tn in unitstree.Values)
					if (!tn.BackColor.Equals(Color.White))
						tn.BackColor = Color.White;


			//ResumeLayout();
			//tree.ResumeLayout();
		}
		private void list_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (list.SelectedItem == null)
				return;
			switch (tabs.SelectedTab.Text) {
				case "Config":
					switch (list.SelectedItem.ToString()) {
						case "Lines":
							dsConfig.Tables["Unit"].DefaultView.RowFilter = String.Empty;
							grConfig.DataSource = dsConfig.Tables["Unit"];
							break;
						case "Servers":
							grConfig.DataSource = dsConfig.Tables["Serv"];
							break;
					}
					break;
				case "Scripts":
					if (!scriptWorker.IsBusy) {
						foreach (TreeNode tn in unitstree.Values )
							tn.BackColor = Color.White;
						scriptResult.Text = string.Empty;
					}
					break;
			}
		}
		private void list_Click(object sender, EventArgs e)
		{
			if (tabs.SelectedTab.Text == "Data") {
				if (ds != null && tb.GetChanges() != null) {
					DialogResult res = MessageBox.Show("Data was changed. Save changes to database?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (res == DialogResult.Yes)
						bindingNavigatorSave_Click(bnSave, null);
				}
				ds = null;
				bn.BindingSource = null;
				grData.DataSource = null;
				grData.CaptionText = string.Empty;
				dataResult.Text = string.Empty;
				bnUndo.Enabled = false;
				bnSave.Enabled = false;
			}
		}
		private static void CustomStyle(DataGrid gr, DataTable t)
		{
			if (t == null)
				return;
			if (gr.TableStyles.Count == 0)
				gr.TableStyles.Add(new DataGridTableStyle());
			DataGridTableStyle ts = gr.TableStyles[0];
			ts.MappingName = t.TableName;
			ts.GridColumnStyles.Clear();
			DataGridTextBoxColumn col = new DataGridTextBoxColumn();
			DataGridBoolColumn colB = new DataGridBoolColumn();

			foreach (DataColumn c in t.Columns) {
				if (c.DataType == typeof(bool)) {
					colB = new DataGridBoolColumn();
					colB.MappingName = c.ColumnName;
					colB.HeaderText = c.ColumnName;
					colB.Width = 50;
					ts.GridColumnStyles.Add(colB);
				} else if (c.ColumnName == "Site/Line") {
					// стиль для первой колонки Statistic
					col = new DataGridTextBoxColumn();
					col.MappingName = c.ColumnName;
					col.HeaderText = c.ColumnName;
					col.Width = 120;
					ts.GridColumnStyles.Add(col);
				} else if (c.DataType == typeof(DateTime)) {
					col = new DataGridTextBoxColumn();
					col.MappingName = c.ColumnName;
					col.HeaderText = c.ColumnName;
					col.Width = 100;
					col.Format = "dd.MM.yyyy HH:mm";
					ts.GridColumnStyles.Add(col);
				} else if (c.DataType == typeof(string)) {
					col = new DataGridTextBoxColumn();
					col.MappingName = c.ColumnName;
					col.HeaderText = c.ColumnName;
					col.Width = 70;
					ts.GridColumnStyles.Add(col);
				} else {
					col = new DataGridTextBoxColumn();
					col.MappingName = c.ColumnName;
					col.HeaderText = c.ColumnName;
					col.Width = 50;
					ts.GridColumnStyles.Add(col);
				}
			}
		}

		private void btExec_Click(Object sender, EventArgs e)
		{
			if (dataWorker.IsBusy) {
				dataWorker.CancelAsync();
				btExec.Enabled = false;
				return;
			}
			if (statWorker.IsBusy) {
				statWorker.CancelAsync();
				btExec.Enabled = false;
				return;
			}
			if (scriptWorker.IsBusy) {
				scriptWorker.CancelAsync();
				btExec.Enabled = false;
				return;
			}
			string[] toWorker;
			switch (tabs.SelectedTab.Text) {
				case "Data": // Запрос данных 
					if (!(tree.SelectedNode.Tag is Unit))
						return;

					if (ds != null && tb.GetChanges() != null) {
						// пользователь изменил данные и не сохранил
						DialogResult res = MessageBox.Show("Data was changed. Save changes to database?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (res == DialogResult.Yes)
							bindingNavigatorSave_Click(bnSave, null);
						else if (res == DialogResult.Cancel)
							return;
						ds = null;
						bn.BindingSource = null;
						grData.DataSource = null;
						bnUndo.Enabled = false;
						bnSave.Enabled = false;
						bindingNavigatorPositionItem.ToolTipText = string.Empty;
					}
					if (tree.SelectedNode == null) {
						MessageBox.Show("Select line in tree before execution", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}
					dataResult.Text = string.Empty;
					if (tree != null && tree.SelectedNode != null)
						grData.CaptionText = String.Format("{0}, {1}", tree.SelectedNode.Parent.Text, tree.SelectedNode.Text);
					else
						grData.CaptionText = tree.SelectedNode.Text;
					spOperation.Text = String.Format("Data selection for {0}... ", grData.CaptionText);
					string q = txtQuery.Text; ;
					if (q.IndexOf("%dt%", 0) != -1) {
						using (InputForm f = new InputForm("Коррекция времени запроса %dt% (yyyyMMdd)", " dt>='" + m_DT.ToString("yyyyMMdd") + "' and dt<'" + m_DT.AddDays(1F).ToString("yyyyMMdd") + "'")) {
							f.ShowDialog();
							if (f.DialogResult != DialogResult.OK)
								return;
							q = q.Replace("%dt%", f.ResS);
							txtQuery.Text = q;
						}
					}
					bindingNavigatorPositionItem.ToolTipText = q;
					string conn = ((Unit)tree.SelectedNode.Tag).Connection;
					toWorker = new string[] { conn + strTimeOut, q };
					SetRunning(true);
					dataWorker.RunWorkerAsync(toWorker);
					break;
				case "Statistic": // Загрузка по одной записи из каждой отмеченной базы данных
					////bool empty = true;
					////for (int i = 0; i < unitstree.Count; i++)
					////   if (unitstree[i].Checked) {
					////      empty = false;
					////      break;
					////   }
					////empty = false;
					////if (empty) {
					////   MessageBox.Show("Check up lines in tree before execution", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
					////   break;
					////}
					grStat.TableStyles.Clear();
					grStat.CaptionText = list.Text;
					toWorker = new string[unitstree.Count * 2 + 2];
					toWorker[0] = txtStat.Text;
					int i = 0;
					foreach (Unit u in unitstree.Keys) {
						i++;
						if (unitstree[u].Checked && u.Kind == UnitKind.Line) {
							toWorker[i * 2] = u.Connection + strTimeOut;
							toWorker[i * 2 + 1] = u.GetLongUnitName(false);
						}
					}
					SetRunning(true);
					statWorker.RunWorkerAsync(toWorker);
					break;
				case "Scripts": // Выполнение скриптов (нет возвращаемого значения)
					scriptResult.Text = string.Empty;
					toWorker = new string[unitstree.Count * 2 + 2];
					toWorker[0] = txtScript.Text;
					int k = 0;
					foreach (Unit u in unitstree.Keys) {
						k++;
						if (unitstree[u].Checked && u.Kind == UnitKind.Line) {
							toWorker[k * 2] = u.Connection + strTimeOut;
							toWorker[k * 2 + 1] = u.GetLongUnitName(false);
						}
					}
					SetRunning(true);
					scriptWorker.RunWorkerAsync(toWorker);
					break;
			}
		}

		private void dataWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			SqlConnection cn = new SqlConnection();
			try {
				string[] s = e.Argument as string[];
				ds = new DataSet();
				cn.ConnectionString = s[0];
				da = new SqlDataAdapter(s[1], cn);
				cb = new SqlCommandBuilder(da);
				da.SelectCommand.CommandTimeout = CommandTimeOut;
				da.UpdateBatchSize = 0;
				da.Fill(ds);
			} catch (Exception ex) {
				e.Result = ex.Message;
			} finally {
				if (cn != null)
					cn.Close(); // Не применять Dispose!!! Connection нужен будет для сохранения изменений в базе данных
			}
		}
		private void dataWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			SetRunning(false);
			if (e.Result != null)
				dataResult.Text = e.Result.ToString();
			if (ds.Tables.Count > 0) {
				tb = ds.Tables[0];
				BindingSource bindSource = new BindingSource { DataSource = tb };
				bn.BindingSource = bindSource;
				bnSave.Enabled = false;
				bnUndo.Enabled = false;
				tb.RowChanged += tb_RowChanged;
				tb.RowDeleted += tb_RowChanged;
				grData.DataSource = bindSource;
				CustomStyle(grData, tb);
			}
		}
		private void tb_RowChanged(Object sender, DataRowChangeEventArgs e)
		{
			bool en = tb != null && tb.GetChanges() != null;
			bnSave.Enabled = en;
			bnUndo.Enabled = en;
		}

		private void statWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bw = sender as BackgroundWorker;

			string[] s = e.Argument as string[];
			SqlConnection cn = new SqlConnection();
			using (SqlCommand cmd = new SqlCommand { CommandText = s[0] }) {
				SqlDataReader dr;
				using (DataTable t = new DataTable("tb")) {
					t.Columns.Add("Site/Line", typeof(string));
					DataRow r = t.NewRow();
					for (int i = 1; i < s.Length / 2; i++)
						if (s[i * 2] != null) {
							if (bw.CancellationPending) {
								e.Cancel = true;
								return;
							}
							cn.ConnectionString = s[i * 2];
							cmd.Connection = cn;
							cmd.CommandTimeout = CommandTimeOut;
							r = t.NewRow();
							r[0] = s[i * 2 + 1];
							t.Rows.Add(r);
							try {
								cn.Open();
								dr = cmd.ExecuteReader();
								if (dr.Read()) {
									if (t.Columns.Count == 1)
										for (int j = 0; j < dr.FieldCount; j++)
											t.Columns.Add(dr.GetName(j), dr.GetFieldType(j));
									for (int j = 0; j < dr.FieldCount; j++)
										r[j + 1] = dr.GetValue(j);
								}
							} catch (Exception ex) {
								r.RowError = ex.Message;
							} finally {
								if (cn.State != ConnectionState.Closed)
									cn.Close();
							}
							bw.ReportProgress(i, t.Copy());
						}
				}
			}
		}
		private void statWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			if (InUse)
				return;
			InUse = true;
			DataTable t = e.UserState as DataTable;
			if (t != null) {
				grStat.DataSource = t;
				if (t.Rows.Count == 1 && t.Columns.Count > 1)
					CustomStyle(grStat, t);
			}
			InUse = false;
		}
		private void statWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			SetRunning(false);
			CustomStyle(grStat, grStat.DataSource as DataTable);
		}

		private void scriptWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			string[] s = e.Argument as string[];

			SqlConnection cn = new SqlConnection();
			using (SqlCommand cmd = new SqlCommand()) {
				int nSuccess = 0;
				int nFail = 0;
				int BatchCounter;
				int AffectCounter;
				string message;
				string Scripts;
				string currentScript;
				for (int i = 1; i < s.Length / 2; i++)
					if (s[i * 2] != null) {
						cn.ConnectionString = s[i * 2];
						cmd.Connection = cn;
						try {
							BatchCounter = 1;
							AffectCounter = 0;
							if (scriptWorker.CancellationPending) {
								e.Cancel = true;
								return;
							}
							scriptWorker.ReportProgress(1, String.Format("{0}: batch {1}... ", s[i * 2 + 1], BatchCounter));
							cn.Open();
							Scripts = s[0];
							do {
								if (scriptWorker.CancellationPending) {
									e.Cancel = true;
									return;
								}
								if (BatchCounter != 1)
									scriptWorker.ReportProgress(1, String.Format("{0}: batch {1}... ", s[i * 2 + 1], BatchCounter));
								currentScript = GetNextScript(ref Scripts);
								cmd.CommandText = currentScript;
								cmd.CommandTimeout = CommandTimeOut;
								AffectCounter = cmd.ExecuteNonQuery();
								if (AffectCounter == -1)
									message = String.Format(": batch {0} - complete", BatchCounter);
								else
									message = ": changed records: " + AffectCounter;
								scriptWorker.ReportProgress(2, message);
								BatchCounter++;
								Application.DoEvents();
							}
							while (Scripts.Length > 5);
							scriptWorker.ReportProgress(3 + i);
							cn.Close();
							nSuccess++;
						} catch (Exception ex) {
							if (cn.State != ConnectionState.Closed)
								cn.Close();
							scriptWorker.ReportProgress(3 + i, ex);
							nFail++;
						}
					}
				e.Result = String.Format("Successfully completed: {0}\r\nFailures: {1}", nSuccess, nFail);
			}
		}
		private void scriptWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			string res;
			switch (e.ProgressPercentage) {
				case 1:
					res = e.UserState as string;
					scriptResult.Text += res;
					spOperation.Text = res;
					break;
				case 2:
					scriptResult.Text += e.UserState as string + Environment.NewLine;
					break;
				default:
					////TreeNode node = unitstree[e.ProgressPercentage - 4];
					////if (e.UserState != null && e.UserState.GetType() == typeof(SqlException)) {
					////   scriptResult.Text += (e.UserState as Exception).Message + "\r\n";
					////   node.BackColor = Color.Pink;
					////} else
					////   node.BackColor = Color.PaleGreen;
					////node.EnsureVisible();
					break;
			}
		}
		private void scriptWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			SetRunning(false);
			if (!e.Cancelled)
				MessageBox.Show(e.Result.ToString(), "The script execution is completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
			else
				scriptResult.Text += Environment.NewLine + "OPERATION WAS ABORTED";
		}

		private void SetRunning(bool startOperation)
		{
			if (startOperation) {
				progrBar.Visible = true;
				progrBar.Style = ProgressBarStyle.Marquee;
				btExec.Text = "Abort";
				list.Enabled = false;
			} else {
				progrBar.Visible = false;
				progrBar.Style = ProgressBarStyle.Blocks;
				btExec.Text = "Execute SQL";
				spOperation.Text = string.Empty;
				btExec.Enabled = true;
				list.Enabled = true;
			}
		}
	}
}
