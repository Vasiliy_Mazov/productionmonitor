﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.Data;
using C1.C1Excel;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{
	class ExcelReport
	{
		public ExcelReport(List<MU> units, List<string> columns, string filter)
		{
			_lu = units;
			_filter = filter;
			_columns = columns;
			Build();

		}



        //public ExcelReport_Counters(List<MU> units, List<string> columns, string filter)
        //{
        //    _lu = units;
        //    _filter = filter;
        //    _columns = columns;
        //    Build();

        //}

		private List<MU> _lu;
		private List<string> _columns;
		private Dictionary<string, XLStyle> _xlstyles;
		private float _dpix;
		private string _filter;

        public void Build_counters()
        {
        }

		private void Build()
		{
			try {
				using (SaveFileDialog sd = new SaveFileDialog()) {
					sd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					if (!frmMonitor.use_easy_counter)
                     sd.FileName = MU.DtEnd.Date.ToString("dd.MM.yyyy") + ".xls";
                    else
                     sd.FileName = "Counters" + MU.DtEnd.Date.ToString("dd.MM.yyyy") + ".xls";
					sd.Filter = "Excel|*.xls";
					if (sd.ShowDialog() == DialogResult.OK && sd.FileName != "") {
						////this.ResumeLayout();
						Cursor.Current = Cursors.WaitCursor;
						Application.DoEvents();
						using (C1XLBook book = new C1XLBook()) {
                            if (!frmMonitor.use_easy_counter)
                            {
							FillStyles(book);
							Summary(book);
							Iterate(book);
							book.Save(sd.FileName);
                            }
                            else
                            {
                                if (_xlstyles != null)
                                    return;

                                _xlstyles = new Dictionary<string, XLStyle>();

                                XLStyle style0 = new XLStyle(book);
                                style0.Format = "0";

                                _xlstyles.Add("none", new XLStyle(book));
                                _xlstyles["none"].BorderBottom = XLLineStyleEnum.None;

                                _xlstyles.Add("order", new XLStyle(book));
                                _xlstyles["order"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("style00", new XLStyle(book));
                                _xlstyles["style00"].Format = "0.0";
                                _xlstyles["style00"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("style00gray", new XLStyle(book));
                                _xlstyles["style00gray"].Format = "0.0";
                                _xlstyles["style00gray"].ForeColor = Color.Gray;
                                _xlstyles["style00gray"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("style00red", new XLStyle(book));
                                _xlstyles["style00red"].Format = "0.0";
                                _xlstyles["style00red"].ForeColor = Color.Red;
                                _xlstyles["style00red"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("style00green", new XLStyle(book));
                                _xlstyles["style00green"].Format = "0.0";
                                _xlstyles["style00green"].ForeColor = Color.Green;
                                _xlstyles["style00green"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("bhead", new XLStyle(book));
                                _xlstyles["bhead"].Font = frmMonitor._fontBold;
                                _xlstyles["bhead"].ForeColor = Color.Black;
                                _xlstyles["bhead"].WordWrap = true;
                                _xlstyles["bhead"].AlignVert = XLAlignVertEnum.Bottom;
                                _xlstyles["bhead"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("bheadu", new XLStyle(book));
                                //xlstyles["bheadu"].Font = fontbold;
                                _xlstyles["bheadu"].Font = new Font("Arial", 10, FontStyle.Bold | FontStyle.Underline);
                                //xlstyles["bheadu"].ForeColor = Color.Gray;

                                _xlstyles.Add("head", new XLStyle(book));
                                _xlstyles["head"].ForeColor = Color.Black;
                                _xlstyles["head"].WordWrap = true;
                                _xlstyles["head"].AlignVert = XLAlignVertEnum.Bottom;

                                _xlstyles.Add("perc", new XLStyle(book));
                                _xlstyles["perc"].Format = "0.0%";
                                _xlstyles["perc"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("hl", new XLStyle(book));
                                _xlstyles["hl"].Format = "0.00";
                                _xlstyles["hl"].BorderBottom = XLLineStyleEnum.Hair;

                                _xlstyles.Add("grperc", new XLStyle(book));
                                _xlstyles["grperc"].Format = "0.0%";
                                _xlstyles["grperc"].AlignHorz = XLAlignHorzEnum.Right;
                                //xlstyles["grperc"].BackColor = Color.FromArgb(153, 204, 0);
                                _xlstyles["grperc"].BackColor = Color.PaleGreen;
                                _xlstyles["grperc"].BackPattern = XLPatternEnum.Gray50;
                                _xlstyles["grperc"].PatternColor = Color.LightGray;

                                _xlstyles.Add("green", new XLStyle(book));
                                _xlstyles["green"].AlignHorz = XLAlignHorzEnum.Right;
                                _xlstyles["green"].BackColor = Color.FromArgb(0, 255, 0);

                                _xlstyles.Add("right", new XLStyle(book));
                                _xlstyles["right"].AlignHorz = XLAlignHorzEnum.Right;
                                _xlstyles["right"].BorderBottom = XLLineStyleEnum.Dotted;


                               // FillStyles(book);
                             Iterate_Counters(book);
                             book.Save(sd.FileName);
                            }
						}
						System.Diagnostics.Process.Start(sd.FileName);
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			} finally {
				Cursor.Current = Cursors.Default;
			}
		}

		private void Summary(C1XLBook book)
		{
			//book.Load(f);
			book.Sheets[0].Name = "Summary";

			XLSheet sh = book.Sheets[0];
			sh.ShowGridLines = false;

			int i, row = 3, temprow, col = 1;

			// В первой строке обозначаем отображаемый интервал времени
			sh[0, col].Value = MU.DtStart.ToString("g") + "   -   " + MU.DtEnd.AddMinutes(-1).ToString("g");
			sh[0, col].Style = _xlstyles["bheadu"];

			// настраиваем ширину колонок
			sh.Columns[0].Width = 200;
			sh.Columns[1].Width = 4000;
			for (i = 2; i <= 100; i++)
				sh.Columns[i].Width = 1000;

			#region | Проценты     |

			//оформляем заголовки строк для процентных KPI
			string rowheader = "";
			foreach (string key in _columns) {

				if (!MU.Kpi.Names.ContainsKey(key) || !MU.Kpi.Measure.ContainsKey(key) || MU.Kpi.Measure[key] != 2)
					continue;
				//if (MonitorUnit.kpi.Short.ContainsKey(key))
				//   rowheader = MonitorUnit.kpi.Short[key];
				//else
				if (MU.Kpi.Names.ContainsKey(key))
					rowheader = MU.Kpi.Names[key];

				rowheader += ",% ";
				sh[row, col].Style = _xlstyles["order"];
				sh[row++, col].Value = rowheader;
			}

			// заполняем таблицу данными
			DateTime tm1 = MU.DtStart.Date.AddDays(-MU.DtStart.Date.DayOfYear + 1);
			DataRow dr;
			double tval = 0, rval = 0, yval = 0;
			//int line;
			int ln = 0;
			foreach (MU unit in _lu) {
				if (unit.IsEmpty() || unit.Error != "")
					continue;
				col = ln * 3 + 2;
				sh.MergedCells.Add(1, ln * 3 + 2, 1, 3);
				sh[1, col].Value = unit.GetLongUnitName(false);
				sh[1, col].Style = _xlstyles["bhead"];
				sh[1, col + 1].Style = _xlstyles["bhead"];
				sh[1, col + 2].Style = _xlstyles["bhead"];
				sh[2, col].Value = "Target";
				sh[2, col + 1].Value = "Range";
				sh[2, col + 2].Value = "Total " + tm1.Year;
				sh[2, col].Style = _xlstyles["order"];
				sh[2, col + 1].Style = _xlstyles["order"];
				sh[2, col + 2].Style = _xlstyles["order"];

				////////grLines.DrawToBitmap(,
				////////sh.MergedCells.Add(3, line * 3 + 2, 1, 3);
				//////Image im = grLines.CreateImage(line + 1, grLines.Cols["chart"].SafeIndex, line + 1, grLines.Cols["chart"].SafeIndex);
				//////XLPictureShape pic = new XLPictureShape(im);
				////////pic.Rotation = 30.0f;
				////////pic.LineColor = Color.DarkRed;
				////////pic.LineWidth = 10;
				//////pic.TopClip = C1XLBook.PixelsToTwips(grLines.Rows[0].Height + 5);
				//////pic.LeftClip = C1XLBook.PixelsToTwips(grLines.Cols[0].Width + 10);
				////////pic.TopClip = pic.Rectangle.Height - C1XLBook.PixelsToTwips(grLines.Rows[1].Height);
				////////pic.LeftClip = pic.Rectangle.Width - C1XLBook.PixelsToTwips(grLines.Cols["chart"].Width);
				//////sh[3, col].Value = pic;

				dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", unit.ID, tm1.Year));
				row = 3;

				// перебор колонок grLines
				foreach (string key in _columns) {
					tval = 0;
					if (!MU.Kpi.Names.ContainsKey(key) || !MU.Kpi.Measure.ContainsKey(key) || MU.Kpi.Measure[key] != 2)
						continue;
					// выбираем только проценты
					if (unit.TgtsYears.ContainsKey(MU.Kpi.ID[key]) &&
							unit.TgtsYears[MU.Kpi.ID[key]].ContainsKey(MU.DtEnd.Year)) {
						tval = unit.TgtsYears[MU.Kpi.ID[key]][MU.DtEnd.Year];
						sh[row, col].Value = tval;
					}
					sh[row, col].Style = _xlstyles["style00gray"];
					rval = C.GetFloat(MU.Total.Rows[unit.L][key]);
					sh[row, col + 1].Value = rval;
					yval = C.GetFloat(dr[key]);
					sh[row, col + 2].Value = yval;
					if (key.StartsWith("K")) {
						sh[row, col + 1].Style = tval == 0 || rval == 0 ? _xlstyles["style00"] : rval >= tval ? _xlstyles["style00green"] : _xlstyles["style00red"];
						sh[row, col + 2].Style = tval == 0 || yval == 0 ? _xlstyles["style00"] : yval >= tval ? _xlstyles["style00green"] : _xlstyles["style00red"];
					} else if (key.StartsWith("k")) {
						sh[row, col + 1].Style = tval == 0 ? _xlstyles["style00"] : rval <= tval ? _xlstyles["style00green"] : _xlstyles["style00red"];
						sh[row, col + 2].Style = tval == 0 ? _xlstyles["style00"] : yval <= tval ? _xlstyles["style00green"] : _xlstyles["style00red"];
					} else {
						sh[row, col + 1].Style = _xlstyles["style00"];
						sh[row, col + 2].Style = _xlstyles["style00"];
					}

					row++;
				}
				ln++;
			}

			#endregion | Проценты     |

			#region | Time Losses  |

			//оформляем заголовки строк для процентных KPI
			col = 1;
			sh[row, col].Style = _xlstyles["bhead"];
			sh[row++, col].Value = "Time Losses";
			temprow = row;

			foreach (string key in MU.Kpi.RootsAll.Keys) {
				if (key == "TR") continue;
				if (MU.Kpi.Names.ContainsKey(key))
					rowheader = MU.Kpi.Names[key];

				if (_xlstyles.ContainsKey(key)) {
					sh[row, col].Style = _xlstyles[key];
					sh[row, col + 1].Style = _xlstyles[key];
				}

				sh[row++, col].Value = rowheader;
			}
			////if (MonitorUnit.kpi.Names.ContainsKey("tNF")) {
			////   sh[row, col].Value = MonitorUnit.kpi.Names["tNF"];
			////   if (xlstyles.ContainsKey("tNF"))
			////      sh[row, col].Style = xlstyles["tNF"];
			////   row++;
			////}

			// заполняем таблицу данными
			ln = 0;
			foreach (MU unit in _lu) {
				if (unit.IsEmpty() || unit.Error != "")
					continue;
				col = ln * 3 + 2;
				dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", unit.ID, tm1.Year));

				row = temprow;

				// проставляем значения на первом (сводном) листе
				foreach (string key in MU.Kpi.RootsAll.Keys) {
					if (key == "TR") continue;
					tval = 0;
					sh[row, col + 1].Value = Utils.GetTimeSpanString(C.GetFloat(MU.Total.Rows[unit.L][key]));
					sh[row, col + 2].Value = Utils.GetTimeSpanString(dr[key]);
					if (_xlstyles.ContainsKey(key)) {
						sh[row, col].Style = _xlstyles[key];
						sh[row, col + 1].Style = _xlstyles[key];
						sh[row, col + 2].Style = _xlstyles[key];
					} else {
						sh[row, col + 1].Style = _xlstyles["right"];
						sh[row, col + 2].Style = _xlstyles["right"];
					}
					row++;
				}
				ln++;
			}

			#endregion | Time Losses  |

			#region | Production   |

			Dictionary<string, int> brandrows = new Dictionary<string, int>();
			int r;
			string brandname;
			sh[row, 1].Value = "Production";
			sh[row, 1].Style = _xlstyles["bhead"];
			ln = 0;
			foreach (MU unit in _lu) {
				if (unit.IsEmpty() || unit.Error != "")
					continue;
				col = ln * 3 + 2; dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", unit.ID, tm1.Year));
				foreach (DataRow drb in unit.DataSet.Tables["BrandName"].Select("Vlm>0 ", "")) {
					brandname = drb["Name"].ToString();
					if (brandrows.ContainsKey(brandname))
						r = brandrows[brandname];
					else {
						row++;
						sh[row, 1].Value = String.Format("{0}, {1}", brandname, Res.strMeasure);
						sh[row, 1].Style = _xlstyles["order"];
						brandrows.Add(brandname, row);
						r = row;
					}
					sh[r, col + 1].Value = C.GetFloat(drb["Vlm"]);
					sh[r, col + 2].Value = C.GetFloat(unit.YearsDS.Tables["year"].Compute("sum(Vlm)", String.Format("Brand={0} AND Yr={1}", drb["ID"], tm1.Year)));
				}
				ln++;
			}
			// стиль назначаем когда уже знаем сколько строк
			foreach (int rr in brandrows.Values) {
				ln = 0;
				foreach (MU unit in _lu) {
					if (unit.IsEmpty() || unit.Error != "")
						continue;
					col = ln * 3 + 2; sh[rr, col].Style = _xlstyles["hl"];
					sh[rr, col + 1].Style = _xlstyles["hl"];
					sh[rr, col + 2].Style = _xlstyles["hl"];
					ln++;
				}
			}

			// Заполняем итог Hl
			row++;
			sh[row, 1].Value = "Total production, Hl";
			sh[row, 1].Style = _xlstyles["order"];
			ln = 0;
			foreach (MU unit in _lu) {
				if (unit.IsEmpty() || unit.Error != "")
					continue;
				col = ln * 3 + 2; ;
				dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", unit.ID, tm1.Year));

				sh[row, col + 1].Value = C.GetFloat(MU.Total.Rows[unit.L]["Vlm"]);
				sh[row, col + 2].Value = C.GetFloat(dr["Vlm"]);
				sh[row, col].Style = _xlstyles["hl"];
				sh[row, col + 1].Style = _xlstyles["hl"];
				sh[row, col + 2].Style = _xlstyles["hl"];
				ln++;
			}

			#endregion | Production   |
		}

        private void Iterate_Counters(C1XLBook book)
        {
            XLSheet sh;

            MU unit;
            book.Sheets.Clear();
            sh = book.Sheets.Add("Counters");
            for (int line = 0; line < _lu.Count; line++)
            {
                unit = _lu[line];
                if (!unit.IsEmpty_Counter())
                {
                    string sheetname = unit.GetLongUnitName_counters(false, "|"); //unit.LineName; //
                    string sheetheader = unit.GetLongUnitName_counters(false, ",  ");
                    string sheetdate = MU.DtStart.ToString("g") + "  -  " + MU.DtEnd.AddMinutes(-1).ToString("g");

                    sh.Rows[0].Style = _xlstyles["bheadu"];
                    sh[0, 0].Value = sheetdate;

                    int ii = 0;
                    ii = line + 2;
                    sh[ii, 1].Value = sheetheader;

                    sh[ii, 2].Value = "Название";
                    sh[ii, 3].Value = "max";
                    sh[ii, 4].Value = "min";
                    sh[ii, 5].Value = "Среднее";

                    // {
                    DataRow[] DR = unit.DataSet.Tables["Counters_kpi"].Select();
                    foreach (DataRow dr in DR)
                    {
                        //DataRow row1 = frmMonitor. ds_counters.Tables["Counters_kpi"].NewRow();
                        sh[ii, 2].Value = (string)dr["name"];
                        // row1["id"] = (int)dr["id"];
                        try
                        {
                            sh[ii, 3].Value = dr["max_"];
                            sh[ii, 4].Value = dr["min_"];
                            sh[ii, 5].Value = dr["avg_"];

                        }

                        catch (Exception)
                        {
                            //  throw;
                        }
                    }
                }
            }
        }
        		private void Iterate(C1XLBook book)
		{
			XLSheet sh;

			MU unit;
			for (int line = 0; line < _lu.Count; line++) {
				unit = _lu[line];
				if (!unit.IsEmpty()) {
					string sheetname = unit.GetLongUnitName(false, "|"); //unit.LineName; //
					string sheetheader = unit.GetLongUnitName(false, ",  ");
					string sheetdate = MU.DtStart.ToString("g") + "  -  " + MU.DtEnd.AddMinutes(-1).ToString("g");
					//if (sheetname.Length > 31) sheetname = bu.LineName;

					// создаем лист для 'KPI Analyse'
					sh = book.Sheets.Add(sheetname + " (a)");
					Analyse(line, sh);
					sh.Rows[0].Style = _xlstyles["bheadu"];
					sh[0, 0].Value = sheetdate;
					sh[0, 1].Value = sheetheader;

                    //// создаем лист 'Detailed'
                    //sh = book.Sheets.Add(sheetname + " (b)");
                    //Detailed(line, sh);
                    //sh.Rows[0].Style = _xlstyles["bheadu"];
                    //sh[0, 1].Value = sheetdate;
                    //sh[0, 4].Value = sheetheader;
				}
			}
		}

		private void FillStyles(C1XLBook book)
		{
			if (_xlstyles != null)
				return;

			_xlstyles = new Dictionary<string, XLStyle>();
			foreach (string key in MU.Kpi.Colors.Keys) {
				_xlstyles.Add(key, new XLStyle(book));
				_xlstyles[key].BackColor = MU.Kpi.Colors[key];

				// индивидуальная 'заточка' под конкретные цвета (в обход ограничений цветовой гаммы встроенной в Excel)
				switch (MU.Kpi.Colors[key].ToArgb()) {
					case -3768140:			//"tSp":
						_xlstyles[key].BackColor = Color.LightGray;
						_xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						_xlstyles[key].PatternColor = Color.Pink;
						break;
					case -2130729728:		//"tQl":
						_xlstyles[key].BackColor = Color.NavajoWhite;
						//xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						//xlstyles[key].PatternColor = Color.Orange;
						break;
					case -3670016:			//"tt6": //
						_xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						_xlstyles[key].PatternColor = Color.Brown;
						break;
					case -6598576:			//"tt4": //Changeover Time
						_xlstyles[key].BackColor = Color.Brown;
						_xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						_xlstyles[key].PatternColor = Color.Gray;
						break;
					case -4343957:			//"tt2": //NONA
						_xlstyles[key].BackColor = Color.YellowGreen;
						_xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						_xlstyles[key].PatternColor = Color.LightGray;
						break;
					case -5192482:			//"tt1": //
						_xlstyles[key].BackPattern = XLPatternEnum.Gray50;
						_xlstyles[key].PatternColor = Color.LightBlue;
						break;
				}
				if (MU.Kpi.Colorsfont.ContainsKey(key))
					_xlstyles[key].ForeColor = MU.Kpi.Colorsfont[key];
				_xlstyles[key].AlignHorz = XLAlignHorzEnum.Right;
			}

			XLStyle style0 = new XLStyle(book);
			style0.Format = "0";

			_xlstyles.Add("none", new XLStyle(book));
			_xlstyles["none"].BorderBottom = XLLineStyleEnum.None;

			_xlstyles.Add("order", new XLStyle(book));
			_xlstyles["order"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("style00", new XLStyle(book));
			_xlstyles["style00"].Format = "0.0";
			_xlstyles["style00"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("style00gray", new XLStyle(book));
			_xlstyles["style00gray"].Format = "0.0";
			_xlstyles["style00gray"].ForeColor = Color.Gray;
			_xlstyles["style00gray"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("style00red", new XLStyle(book));
			_xlstyles["style00red"].Format = "0.0";
			_xlstyles["style00red"].ForeColor = Color.Red;
			_xlstyles["style00red"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("style00green", new XLStyle(book));
			_xlstyles["style00green"].Format = "0.0";
			_xlstyles["style00green"].ForeColor = Color.Green;
			_xlstyles["style00green"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("bhead", new XLStyle(book));
			_xlstyles["bhead"].Font = frmMonitor._fontBold;
			_xlstyles["bhead"].ForeColor = Color.Black;
			_xlstyles["bhead"].WordWrap = true;
			_xlstyles["bhead"].AlignVert = XLAlignVertEnum.Bottom;
			_xlstyles["bhead"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("bheadu", new XLStyle(book));
			//xlstyles["bheadu"].Font = fontbold;
			_xlstyles["bheadu"].Font = new Font("Arial", 10, FontStyle.Bold | FontStyle.Underline);
			//xlstyles["bheadu"].ForeColor = Color.Gray;

			_xlstyles.Add("head", new XLStyle(book));
			_xlstyles["head"].ForeColor = Color.Black;
			_xlstyles["head"].WordWrap = true;
			_xlstyles["head"].AlignVert = XLAlignVertEnum.Bottom;

			_xlstyles.Add("perc", new XLStyle(book));
			_xlstyles["perc"].Format = "0.0%";
			_xlstyles["perc"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("hl", new XLStyle(book));
			_xlstyles["hl"].Format = "0.00";
			_xlstyles["hl"].BorderBottom = XLLineStyleEnum.Hair;

			_xlstyles.Add("grperc", new XLStyle(book));
			_xlstyles["grperc"].Format = "0.0%";
			_xlstyles["grperc"].AlignHorz = XLAlignHorzEnum.Right;
			//xlstyles["grperc"].BackColor = Color.FromArgb(153, 204, 0);
			_xlstyles["grperc"].BackColor = Color.PaleGreen;
			_xlstyles["grperc"].BackPattern = XLPatternEnum.Gray50;
			_xlstyles["grperc"].PatternColor = Color.LightGray;

			_xlstyles.Add("green", new XLStyle(book));
			_xlstyles["green"].AlignHorz = XLAlignHorzEnum.Right;
			_xlstyles["green"].BackColor = Color.FromArgb(0, 255, 0);

			_xlstyles.Add("right", new XLStyle(book));
			_xlstyles["right"].AlignHorz = XLAlignHorzEnum.Right;
			_xlstyles["right"].BorderBottom = XLLineStyleEnum.Dotted;
		}

		private void Detailed(int line, XLSheet sh)
		{
			DataRowView drv;
			DataView dv;
			int row, col;

			sh.ShowGridLines = false;
			sh.Rows[1].Style = _xlstyles["bhead"];
			sh.Columns[0].Width = 200;
			sh.Columns[1].Width = 2000;
			sh.Columns[2].Width = 1100;
			sh.Columns[3].Width = 1100;
			if (frmMonitor.use_one_column_for_excel) {
				sh.Columns[5].Width = 2000;
				col = 6;
			} else {
				col = 5;
				foreach (int key in _lu[line].DownColumns.Keys) {
					sh[1, col].Value = _lu[line].DownColumns[key];
					sh.Columns[col++].Width = 1100;
				}
			}

			foreach (string key in MU.marks_captions.Values) {
				sh[1, col].Value = key;
				sh.Columns[col++].Width = 1100;
			}
			sh.Columns[col].Width = 8000;

			sh[1, 1].Value = Res.strFrom;
			sh[1, 2].Value = Res.SelMins + ", " + (frmMonitor.TmStyle == frmMonitor.TimeStyle.MMM ? Res.strMins : Res.strHours);
			sh[1, 3].Value = Res.RealMins + ", " + (frmMonitor.TmStyle == frmMonitor.TimeStyle.MMM ? Res.strMins : Res.strHours);
			sh[1, 5].Value = Res.strCause;
			sh[1, col].Value = Res.strComment;

			dv = new DataView(_lu[line].DataSet.Tables["Stops"]);
			int mark;
			for (int i = 1; i <= dv.Count; i++) {
				row = i + 1;
				drv = dv[i - 1];
				sh.Rows[row].Style = _xlstyles["order"];
				sh[row, 0].Style = _xlstyles["none"];
				sh[row, 4].Style = _xlstyles[drv["RD"].ToString()];
				sh[row, 1].Value = drv["dt1"].ToString();
				sh[row, 2].Value = Utils.GetTimeSpanString(drv["TF"]);
				sh[row, 3].Value = Utils.GetTimeSpanString(drv["TD"]);
				//sh[row, 1].Style = xlstyles["order"];
				//sh[row, 2].Style = xlstyles["order"];
				sh[row, 3].Style = _xlstyles["style00"];
				if (frmMonitor.use_one_column_for_excel) {
					sh[row, 5].Value = drv["FullName"].ToString();
					col = 6;
				} else {
					col = 5;
					foreach (int key in _lu[line].DownColumns.Keys) {
						int stop = C.GetInt(drv[_lu[line].DownColumns[key]]);
						if (stop > 0)
							sh[row, col++].Value = _lu[line].Stops[C.GetInt(drv[_lu[line].DownColumns[key]])];
						else
							break;
					}
					col = 5 + _lu[line].DownColumns.Count;
				}
				foreach (string key in MU.marks.Keys) {
					mark = C.GetInt(drv[key]);
					if (mark > 0) sh[row, col].Value = MU.marks[key][mark];
					col++;
				}
				sh[row, col].Value = drv["Comment"].ToString();
				//sh[row, col].Style = xlstyles["order"];
			}
			//sh.Columns[0].Style = xlstyles["none"];
			//sh.Columns[0].Style = xlstyles["none"];
		}

		private void Analyse(int line, XLSheet sh)
		{
			Dictionary<string, float> kpiflt = _lu[line].GetFilteredKPI(_filter, _lu[line].DataSet.Tables["Gaps"]);

			// variables for columns & rows
			int cTr, cSm, r, cnt, totalwidth;
			r = 1;
			cTr = 0;			// tree column
			cSm = cTr + 1;	// sum column

			cnt = cSm;
			sh[r, cSm].Value = Res.strTime;
			sh.Columns[cTr].Width = 4000;// 4000;
			sh.Columns[cSm].Width = 1100; // 1100;
			totalwidth = sh.Columns[cTr].Width + sh.Columns[cSm].Width;
			foreach (string key in MU.Kpi.AnalyseColumns.Keys) {
				cnt++;
				sh.Columns[cnt].Width = 1000;// 1000;
				totalwidth += 1000;
				sh[r, cnt].Value = MU.Kpi.Names[key];
				sh[r, cnt].Style = _xlstyles["head"];
			}

			sh.ShowGridLines = false;

			float val;
			foreach (string key in MU.Kpi.AnalyseRows.Keys) {
				r++;

				// раскрашиваем колонку со временем
				if (_xlstyles.ContainsKey(key))
					sh[r, cSm].Style = _xlstyles[key];

				sh[r, cTr].Value = MU.Kpi.Names[key];
				sh[r, cSm].Value = Utils.GetTimeSpanString((int)kpiflt[key]);
				sh[r, cTr].Style = _xlstyles["order"];

				if (key.StartsWith("t")) {
					cnt = cSm;
					foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
						cnt++;
						if (MU.Kpi.NomParts[MU.Kpi.AnalyseColumns[k]].Contains(key) || ((key == "tSp" || key == "tNF" || key == "tt7") && k != "K12")) {
							val = -kpiflt[key] / kpiflt[MU.Kpi.AnalyseColumns[k]];
							if (!float.IsNaN(val) && !float.IsInfinity(val))
								sh[r, cnt].Value = C.GetFloat(val);
						}
						sh[r, cnt].Style = _xlstyles["perc"];
					}
					foreach (DataRow dr in _lu[line].DataSet.Tables["StopList"].Select("TopID=" + MU.Kpi.ID[key].ToString()))
						AddTreeGroup(line, (int)dr["DownID"], ref r, 1, key, _filter, kpiflt, sh);
				} else {
					// раскрашиваем светлозеленым строки с ключевыми временами
					for (int c = 0; c < MU.Kpi.AnalyseColumns.Count + 2; c++)
						sh[r, c].Style = _xlstyles["grperc"];

					cnt = cSm;
					foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
						cnt++;
						if (kpiflt[MU.Kpi.AnalyseColumns[k]] > 0) {
							val = kpiflt[key] / kpiflt[MU.Kpi.AnalyseColumns[k]];
							if (!float.IsNaN(val) && !float.IsInfinity(val))
								sh[r, cnt].Value = C.GetFloat(val);
						}
						if (key == MU.Kpi.AnalyseColumns[k])
							break;
					}
				}
			}

			#region | ChartAnalyse       |

			////InitChartAnalyse(chPrint);
			////LoadAnalysisChart(chPrint, line);

			////chPrint.Height = C.GetInt(chPrint.ChartArea.AxisX.Max) * 15 + 10;
			////chPrint.ChartArea.LocationDefault = new Point(-1, 5);
			////chPrint.ChartArea.SizeDefault = new Size(-1, chPrint.Height);

			////Image im = chPrint.GetImage(ImageFormat.Emf, new Size(TwipsToPixels(totalwidth) + 90, chPrint.Height));
			////XLPictureShape pic = new XLPictureShape(im);

			////sh[++r, 0].Value = pic;

			#endregion | ChartAnalyse       |

			#region | some results below |

			if (MU.Kpi.Names.ContainsKey("K1")) {
				r++;
				sh[r, cTr].Value = MU.Kpi.Names["K1"];
				sh[r, cSm].Value = kpiflt["K1"] / 100;
				sh[r, cTr].Style = _xlstyles["order"];
				sh[r, cSm].Style = _xlstyles["perc"];
			}
			if (MU.Kpi.Names.ContainsKey("K2")) {
				r++;
				sh[r, cTr].Value = MU.Kpi.Names["K2"];
				sh[r, cSm].Value = kpiflt["K2"] / 100;
				sh[r, cTr].Style = _xlstyles["order"];
				sh[r, cSm].Style = _xlstyles["perc"];
			}
			r++;
			sh[r, cTr].Value = String.Format("{0}, {1}", Res.strVolume, Res.strMeasure); //MonitorUnit.kpi.Names["Vlm"];
			sh[r, cSm].Value = kpiflt["Vlm"];
			sh[r, cTr].Style = _xlstyles["order"];
			sh[r, cSm].Style = _xlstyles["hl"];

			foreach (DataRow dr in _lu[line].DataSet.Tables["BrandName"].Select("Vlm>0 ", "")) {
				r++;
				sh[r, cTr].Value = String.Format("{0}, {1}", dr["Name"], Res.strMeasure); ;
				sh[r, cTr].Style = _xlstyles["order"];
				sh[r, cSm].Value = C.GetFloat(dr["Vlm"]);
				sh[r, cSm].Style = _xlstyles["hl"];
			}

			#endregion | some results below |
		}

		private void AddTreeGroup(int line, int downid, ref int r, int level, string key, string filter, Dictionary<string, float> kpiflt, XLSheet sh)
		{
			string computefilter = Utils.StrJoin(string.Format("D{0}={1}", level, downid), filter);
			float down = C.GetFloat(_lu[line].DataSet.Tables["Stops"].Compute("Sum(TD)", computefilter));
			if (down != 0) {
				r++;

				if (level >= 2)
					sh.Rows[r].Height = 0;

				sh[r, 0].Value = Utils.GetRowField(_lu[line].DataSet.Tables["StopList"], "DownID=" + downid.ToString(), "", "FullName");
				sh[r, 0].Style = _xlstyles["order"];
				sh[r, 1].Value = Utils.GetTimeSpanString((int)down);

				if (_xlstyles.ContainsKey(key))
					sh[r, 1].Style = _xlstyles[key];
				int cnt = 1;
				foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
					cnt++;
					if (MU.Kpi.NomParts[MU.Kpi.AnalyseColumns[k]].Contains(key) || ((key == "tt7") && k != "K12")) {
						sh[r, cnt].Value = -C.GetFloat(down / kpiflt[MU.Kpi.AnalyseColumns[k]]);
						sh[r, cnt].Style = _xlstyles["perc"];
					}
				}

				foreach (DataRow dr in _lu[line].DataSet.Tables["StopList"].Select("TopID=" + downid.ToString()))
					AddTreeGroup(line, (int)dr["DownID"], ref r, level + 1, key, filter, kpiflt, sh);
			}
		}

		private int TwipsToPixels(float twips)
		{
			return (int)(twips * _dpix / 1440);
		}

		private int PixelsToTwips(int pixels)
		{
			return (int)(pixels * 1440 / _dpix);
		}

	}


    class ExcelReport_Counters
    {
        public ExcelReport_Counters(DataTable Dat_set_tables, DataColumnCollection columns_)  //DataTable
		{

            Build(Dat_set_tables,columns_ );

		}

        private void Iterate_Counters(C1XLBook book, DataTable Dat_set_tables, DataColumnCollection columns_) // DataTable 
        {
            XLSheet sh;

            MU unit;
            book.Sheets.Clear();
            sh = book.Sheets.Add("Counters");


                  //  string sheetname = unit.GetLongUnitName_counters(false, "|"); //unit.LineName; //
                  //  string sheetheader = unit.GetLongUnitName_counters(false, ",  ");
                    string sheetdate = MU.DtStart.ToString("g") + "  -  " + MU.DtEnd.AddMinutes(-1).ToString("g");

                  //  sh.Rows[0].Style = _xlstyles["bheadu"];
                    sh[0, 0].Value = sheetdate;

                    int ii = 0;
                    int line = 1;
                    ii = 2;// line + 2;
                   // sh[ii, 1].Value = sheetheader;

                    int i_ = 2;

                    foreach (DataColumn column in columns_) // Dat_set_tables.Columns)
                    {
                        if (!(column.Caption == "error" || column.Caption == "dt"))
                        {
                            // Console.WriteLine(row[column]);
                            sh[1, i_].Value = column.Caption;
                            i_++;
                        }

                    }

                    i_ = 2;
                    ii = 2;
                    foreach (DataRow row in Dat_set_tables.Rows)
                    {
                         i_ = 2;
                        foreach (DataColumn column in columns_)// Dat_set_tables.Columns)
                        {
                            if (!(column.Caption == "error" || column.Caption == "dt"))
                            {
                                double lv_doubl;
                                string lv_str = row[column].ToString();
                                if (!(double.TryParse(lv_str, out lv_doubl)))
                                {
                                    sh[ii, i_].Value =  row[column]; 
                                }
                                else
                                {

                                    sh[ii, i_].Value = lv_doubl.ToString("F3");
                                }


                                i_++;
                            }
                        }
                        
                        ii++;
                    }


                    ii = 2;
  
        }



        private void Build(DataTable Dat_set_tables, DataColumnCollection columns_)  // DataTable 
		{

			try {
				using (SaveFileDialog sd = new SaveFileDialog()) {
					sd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					if (!frmMonitor.use_easy_counter)
                     sd.FileName = MU.DtEnd.Date.ToString("dd.MM.yyyy") + ".xls";
                    else
                     sd.FileName = "Counters" + MU.DtEnd.Date.ToString("dd.MM.yyyy") + ".xls";
					sd.Filter = "Excel|*.xls";
					if (sd.ShowDialog() == DialogResult.OK && sd.FileName != "") {
						////this.ResumeLayout();
						Cursor.Current = Cursors.WaitCursor;
						Application.DoEvents();
						using (C1XLBook book = new C1XLBook()) {


                             Iterate_Counters(book ,  Dat_set_tables ,  columns_ );
                             book.Save(sd.FileName);
                            }
						}
						System.Diagnostics.Process.Start(sd.FileName);
					}
				//}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			} finally {
				Cursor.Current = Cursors.Default;
			}




            //foreach (DataRow row in Dat_set_tables.Rows)
            //{
                
            //}

         }
    }

}
