﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.Drawing;
using System.Runtime.Serialization;
using System.Windows.Forms;
using C1.Win.C1FlexGrid;	
using C1.Win.C1Chart;
using System.Diagnostics;
using System.ComponentModel;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{

	#region | UndoRedo class          |
	public class UndoRedo
	{
		#region | fields |

		private DataSet current;
		private List<DataSet> DS;
		int idx;
		int lenth;
		private Dictionary<string, Dictionary<string, string>> tbls;
		private Dictionary<string, string> expr;
		private bool empty = true;

		public enum Action
		{
			UndoAll,
			Undo,
			Redo,
			Commit,
			Clear
		}
		#endregion

		public event EventHandler OnStackChanged;

		public UndoRedo(DataSet dataSet)
		{
			current = dataSet;

			tbls = new Dictionary<string, Dictionary<string, string>>(current.Tables.Count);
			foreach (DataTable dtb in current.Tables) {
				expr = new Dictionary<string, string>(dtb.Columns.Count);
				tbls.Add(dtb.TableName, expr);
				foreach (DataColumn dc in dtb.Columns)
					if (!(dc.Expression == null || dc.Expression == ""))
						expr.Add(dc.ColumnName, dc.Expression);
			}


			DS = new List<DataSet>();
			DS.Add(Copy(current));
			empty = false;
			idx = 0;
			lenth = 1;
		}

		private void Merge(DataSet ds1, DataSet ds2)
		{
			// Based on suggestion: http://www.thoughtshapes.com/node/61
			// Short description:
			// Prior to merging, store the string value of the Expression property for every DataColumn, 
			// set every DataColumn’s Expression property to null or string.Empty, and after merging add 
			// the stored expressions back to each corresponding DataColumn.
			// This workaround will reduce execution time up to ten times. 

			Stopwatch sw1 = Stopwatch.StartNew();

			ds1.BeginInit();

			foreach (DataTable tb in ds1.Tables) {
				tb.BeginLoadData();
				//tb.BeginInit();
			}

			// Удаляем все выражения
			foreach (string dtb in tbls.Keys)
				foreach (string dc in tbls[dtb].Keys)
					ds1.Tables[dtb].Columns[dc].Expression = "";

			ds1.Clear();
			ds1.Merge(ds2);

			// возвращаем все выражения на место
			foreach (string dtb in tbls.Keys)
				foreach (string dc in tbls[dtb].Keys)
					ds1.Tables[dtb].Columns[dc].Expression = tbls[dtb][dc];

			foreach (DataTable tb in ds1.Tables) {
				tb.EndLoadData();
				//tb.EndInit();
			}
			ds1.EndInit();

			Debug.WriteLine("UndoRedo_Merge: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
		}
		private DataSet Copy(DataSet ds)
		{
			// direct way is to use DataSet.Copy method, but in fact it's 5 times more slowly then next:
			DataSet newds = ds.Clone();
			Merge(newds, ds);
			return newds;
		}
		/// <summary>
		/// Убираем все Expression для всех колонок во всех таблицах
		/// </summary>
		public void RemoveExpressions()
		{
			// Удаляем все выражения
			foreach (string dtb in tbls.Keys)
				foreach (string dc in tbls[dtb].Keys) {
					current.Tables[dtb].Columns[dc].Expression = "";
					current.Tables[dtb].BeginLoadData();
				}

		}
		/// <summary>
		/// Восстанавливаем все ранее удаленные Expression
		/// </summary>
		public void RestoreExpessions()
		{
			foreach (string dtb in tbls.Keys) {
				foreach (string dc in tbls[dtb].Keys) 
					current.Tables[dtb].Columns[dc].Expression = tbls[dtb][dc];
				current.Tables[dtb].EndLoadData();
			}
		}

		public void ChangeState(Action act)
		{
			switch (act) {
				case Action.Commit:
					Commit();
					break;
				case Action.Redo:
					Redo();
					break;
				case Action.UndoAll:
					UndoAll();
					break;
				case Action.Undo:
					Undo();
					break;
				case Action.Clear:
					ClearStack();
					break;
			}
		}
		public void Commit()
		{
			CommitQuiet();
			OnStackChanged(this, EventArgs.Empty);
		}
		public void CommitQuiet()
		{
			idx++;

			DS.Insert(idx, Copy(current));
			lenth = idx + 1;
			DS.RemoveRange(lenth, DS.Count - lenth);
		}
		public void UndoAll()
		{
			if (ThereIsUndo) {
				idx = 0;
				Merge(current, DS[0]);
				OnStackChanged(this, EventArgs.Empty);
			}
		}
		public void Undo()
		{
			if (ThereIsUndo) {
				idx--;
				Merge(current, DS[idx]);
				OnStackChanged(this, EventArgs.Empty);
			}
		}
		public void Redo()
		{
			if (ThereIsRedo) {
				idx++;
				Merge(current, DS[idx]);
				OnStackChanged(this, EventArgs.Empty);
			}
		}
		public void Clear()
		{
			foreach (DataSet ds in DS)
				ds.Dispose();
			DS.Clear();
			idx = 0;
			lenth = 0;
			empty = true;
		}
		public void ClearStack()
		{
			current.AcceptChanges();
			DS.Clear();
			DS.Add(Copy(current));
			idx = 0;
			lenth = 0;
			OnStackChanged(this, EventArgs.Empty);
		}
		
		public DataSet Current
		{
			get { return current; }
		}
		public bool ThereIsRedo
		{
			get { return idx + 1 < lenth; }
		}
		public bool ThereIsUndo
		{
			get { return idx > 0; }
		}
		public bool Empty
		{
			get { return empty; }
		}
	}
	#endregion

	#region | TableFreeze class       |
	public class TableFreeze
	{
		private DataTable dtb;
		private Dictionary<string, string> expr;

		public TableFreeze(DataTable DataTable)
		{
			dtb = DataTable;
			expr = new Dictionary<string, string>(dtb.Columns.Count);
			foreach (DataColumn dc in dtb.Columns)
				if (!(dc.Expression == null || dc.Expression == "")) {
					expr.Add(dc.ColumnName, dc.Expression);
					dc.Expression = "";
				}
		}
		public void Worm()
		{
			foreach (string dc in expr.Keys)
				dtb.Columns[dc].Expression = expr[dc];
		}
	}

	public class DataSetFreeze
	{
		List<TableFreeze> tfList = new List<TableFreeze>();
		public DataSetFreeze(DataSet dts)
		{
			foreach (DataTable dtb in dts.Tables) {
				TableFreeze tf = new TableFreeze(dtb);
				tfList.Add(tf);
			}
		}
		public void Worm()
		{
			foreach (TableFreeze tf in tfList)
				tf.Worm();
		}

	}

	#endregion

	#region | SelectedZone class      |
	public sealed class SelectedZone : ChartDataSeries
	{
		public SelectedZone(C1Chart chart, int Group, DateTime timebase): base()
		{
			this._chart = chart;
			this._timebase = timebase;

			double[] x = new double[4];
			double[] y = new double[4];
			y[1] = 100000;
			y[2] = 100000;

			base.X.CopyDataIn(x);
			base.Y.CopyDataIn(y);
			base.FillStyle.HatchStyle = HatchStyleEnum.BackwardDiagonal;//WideUpwardDiagonal; //BackwardDiagonal; //LightUpwardDiagonal;
			base.FillStyle.FillType = FillTypeEnum.Hatch;
			base.FillStyle.Color1 = Color.Transparent;
			base.FillStyle.Color2 = Color.Blue;

			//base.FillStyle.HatchStyle = HatchStyleEnum.WideUpwardDiagonal;//WideUpwardDiagonal; //BackwardDiagonal; //LightUpwardDiagonal;
			//base.FillStyle.FillType = FillTypeEnum.Hatch;
			//base.FillStyle.Color1 = Color.Transparent;
			//base.FillStyle.Color2 = Color.FromArgb(64, Color.Black);

			
			UpperExtent = 0;
			LowerExtent = 0;

			chart.ChartGroups[Group].ChartData.SeriesList.Add(this);

		}
		public event EventHandler MouseEnter;

		private C1Chart _chart;
		public enum TimeZoneKind
		{
			Downtime,
			DowntimeHighlited,
			Selected,
			Production,
			Shift
		}
		private TimeZoneKind _kind;

		private DateTime _start;
		private DateTime _stop;
		private double _start_double;
		private double _stop_double; 
		private string _fullname = string.Empty;
		private string _comment = string.Empty;
		private string _strbrand = string.Empty;
		private string _tooltip = string.Empty;
		private int _downid = 0;
		private int _id = 0;
		private DateTime _timebase;
		private bool _mouseinside = false;
		private double _anchor = 0;
		private double _initialvalue = 0;
		private bool _growuponly = false;
		private bool _visible = false;

		public string FullName
		{
			get { return _fullname; }
		}
		public string Comment
		{
			get { return _comment; }
		}
		public string Brand
		{
			get { return _strbrand; }
		}
		public string ToolTip
		{
			get { return _tooltip; }
		}
		public int DownID
		{
			get { return _downid; }
			set { _downid = value;	}
		}
		public int ID
		{
			get { return _id; }
			set { _id = value; }
		}

		public DateTime Start
		{
			get { return _start; }
			set
			{
				_start = value;
				base.X[0] = _start.Subtract(_timebase).TotalDays;
				base.X[1] = base.X[0];

				_start_double = (double)X[0];

			}
		}
		public DateTime Stop
		{
			get { return _stop; }
			set
			{
				_stop = value;
				//base.UpperExtent = _stop.Subtract(_timebase).TotalDays;
				base.X[2] = _stop.Subtract(_timebase).TotalDays;
				base.X[3] = base.X[2];
				//_chart.Update();
				_stop_double = (double)X[2];
			}
		}
		public double UpperExtent
		{
			get { return _stop_double; }
			set
			{
				_stop_double = value;
				this.Stop = _timebase.AddDays(_stop_double);
			}
		}
		public double LowerExtent
		{
			get { return _start_double; }
			set
			{
				_start_double = value;
				this.Start = _timebase.AddDays(_start_double);
			}
		}
		public bool GrowUpOnly
		{
			get { return _growuponly; }
			set { _growuponly = value; }
		}
		public bool Visible
		{
			get { return _visible; }
			set { 
				_visible = value;
				if (_visible == false) {
					UpperExtent = 0;
					LowerExtent = 0;
					_anchor = 0;
					DownID = 0;
				}
			}
		}
		public bool MouseNearBorder(double mouseposition)
		{
			double ydfield = _chart.ChartArea.AxisX.ScrollBar.Scale * 6;
			
			double x1 = Math.Abs(mouseposition - _start_double) * 1440F;
			double x2 = Math.Abs(mouseposition - _stop_double) * 1440F;
			if (x1 < ydfield)
				_anchor = _stop_double;
			else if (x2 < ydfield)
				_anchor = _start_double;
			else
				return false;
			return true;
		}
		public void StartDragBorder(double y_position)
		{
			//if (_anchor != 0)
			//   return;

			Visible = true;

			Start = _timebase.AddDays(y_position);
			Stop = Start;
			_anchor = y_position;
			//_chart.Cursor = Cursors.VSplit;

		}
		public TimeZoneKind Kind
		{
			get { return _kind; }
			set { _kind = value; }
		}
		public bool DragBorder(double y_position)
		{
			if (_anchor < y_position) {
				if (!_growuponly || (y_position - _anchor) > _initialvalue) {
					this.Start = _timebase.AddDays(_anchor);
					this.Stop = _timebase.AddDays(y_position);
					return true;
				}
			} else {
				if (!_growuponly || (_anchor - y_position) > _initialvalue) {
					this.Start = _timebase.AddDays(y_position);
					this.Stop = _timebase.AddDays(_anchor);
					return true;
				}
			}
			return false;
		}
		public void FixInitialValue()
		{
			_initialvalue = Math.Abs(_stop_double - _start_double);
		}
		public bool MouseInside(DateTime dt)
		{
			bool res = _start <= dt && dt < _stop;
			if (_mouseinside == false && res) {
				_mouseinside = true;
				if (MouseEnter != null)
					MouseEnter(this, EventArgs.Empty);
			}
			if (!res)
				_mouseinside = false;
			return _mouseinside;
		}
		public bool NewValueIsValid()
		{
			return UpperExtent - LowerExtent > _initialvalue;
		}
		public void Reset()
		{
			Visible = false;
		}
		public bool IsEmptyDowntime
		{
			get { return DownID == 0; }
		}
		public C1Chart Chart
		{
			get { return _chart; }
		}
	}
	#endregion

	#region | NewZone class           |
	public sealed class NewZone : ChartDataSeries
	{
		public NewZone(C1Chart chart, int Group, DateTime timebase, DateTime dt1, DateTime dt2, Color col)
			: base()
		{
			this._chart = chart;
			this._timebase = timebase;

			double[] x = new double[4];
			double[] y = new double[4];
			y[1] = 100000;
			y[2] = 100000;

			base.X.CopyDataIn(x);
			base.Y.CopyDataIn(y);
			base.FillStyle.Color1 = col;

			this.Start = dt1;
			this.Stop = dt2;

			chart.ChartGroups[Group].ChartData.SeriesList.Add(this);

		}
		private C1Chart _chart;

		private DateTime _start;
		private DateTime _stop;
		private double _start_double;
		private double _stop_double;
		private DateTime _timebase;

		public DateTime Start
		{
			get { return _start; }
			set
			{
				_start = value;
				base.X[0] = _start.Subtract(_timebase).TotalDays;
				base.X[1] = base.X[0];

				_start_double = (double)X[0];
			}
		}
		public DateTime Stop
		{
			get { return _stop; }
			set
			{
				_stop = value;
				//base.UpperExtent = _stop.Subtract(_timebase).TotalDays;
				base.X[2] = _stop.Subtract(_timebase).TotalDays;
				base.X[3] = base.X[2];
				//_chart.Update();
				_stop_double = (double)X[2];
			}
		}
		public double UpperExtent
		{
			get { return _stop_double; }
			set
			{
				_stop_double = value;
				this.Stop = _timebase.AddDays(_stop_double);
			}
		}
		public double LowerExtent
		{
			get { return _start_double; }
			set
			{
				_start_double = value;
				this.Start = _timebase.AddDays(_start_double);
			}
		}
	}
	#endregion

	#region | Utils class             |
	/// <summary>
	/// Сериализует в формате xml и сохраняет в ApplicationSettings текущего пользователя
	/// </summary>
	public static class Utils
	{
		public static void SerializeDataObject(string name, object DataObject)
		{
			DataTable table;
			DataSet dataset;
			if (DataObject.GetType() == typeof(DataTable)) {
				table = DataObject as DataTable;
				table.RemotingFormat = SerializationFormat.Xml;
			} else if (DataObject.GetType() == typeof(DataSet)) {
				dataset = DataObject as DataSet;
				dataset.RemotingFormat = SerializationFormat.Xml;
			} else
				return;

			string fileName = name + ".xml";
			string fullName = Directory.GetParent(Application.UserAppDataPath).FullName + "\\" + fileName;

			System.Runtime.Serialization.IFormatter fmt = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

			try {
				using (Stream stm = new FileStream(fullName, System.IO.FileMode.Create)) {
					fmt.Serialize(stm, DataObject);
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}

		}
		/// <summary>
		/// Возращает объект который был сериализован процедурой SerializeDataObject()
		/// </summary>
		/// <param name="name">Имя сохраненного файла, к торому будет добавлено расширение .xml</param>
		/// <param name="AgeHours">Приемлемый возраст в часах
		/// если -1 то в годится любой, возраст не имеет значения</param>
		/// если 0 то в любом случае процедура должна вернуть null
		/// <returns></returns>
		public static Object DeserializeDataObject(string name, int AgeHours)
		{
			if (AgeHours == 0)
				return null;
			string Name = "";
			string fileName = name + ".xml";
			string fullName = Directory.GetParent(Application.UserAppDataPath).FullName + "\\" + fileName;

			if (File.Exists(fileName))
				Name = fileName;
			else if (File.Exists(fullName))
				Name = fullName;
			else
				return null;


			try {
				// если файл устарел (старше двух суток после перезаписи) и требуется более свежий то возвращаем null
				if (AgeHours > 0)
				   if (DateTime.Now.Subtract(File.GetLastWriteTime(Name)).TotalHours > AgeHours)
				      return null;

				System.Runtime.Serialization.IFormatter fmt = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
				using (Stream stm = new FileStream(Name, FileMode.Open)) {
					// readout early saved dataset or datatable from "Application settings"
					return fmt.Deserialize(stm);
				}
			} catch (Exception ex) {
				MessageBox.Show("LoadLists: " + ex.Message);
			}
			return null;
		}
		public static Object DeserializeDataObject(string name, DateTime Age)
		{
			Stopwatch sw1 = Stopwatch.StartNew();


			string Name = "";
			string fileName = name + ".xml";
			string fullName = Directory.GetParent(Application.UserAppDataPath).FullName + "\\" + fileName;

			if (File.Exists(fileName))
				Name = fileName;
			else if (File.Exists(fullName))
				Name = fullName;
			else
				return null;


			try {
				// если файл устарел (старше двух суток после перезаписи) и требуется более свежий то возвращаем null
				if (File.GetLastWriteTime(Name) < Age)
					return null;

				System.Runtime.Serialization.IFormatter fmt = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
				using (Stream stm = new FileStream(Name, FileMode.Open)) {
					// readout early saved dataset or datatable from "Application settings"
					////DataSet dts = fmt.Deserialize(stm) as DataSet;
					////Debug.WriteLine("DeserializeDataObject: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					////return dts;
					
					return fmt.Deserialize(stm);//fmt.Deserialize(stm) as DataSet;
				}
			} catch (Exception ex) {
				MessageBox.Show("LoadLists: " + ex.Message);
			}
			return null;
		}
		
		public static void CopyDatatableToClipboard(DataTable dtb)
		{
			if (dtb == null)
				return;
			StringBuilder sb = new StringBuilder();

			int nCol = dtb.Columns.Count;

			// get column captions
			List<string> cols = new List<string>();
			for (int c = 0; c < nCol; c++) {
				if (dtb.Columns[c].Caption.StartsWith("_"))
					continue;
				else if (dtb.Columns[c].Caption == null) {
					sb.Append(dtb.Columns[c].ColumnName);
				} else {
					sb.Append(dtb.Columns[c].Caption);
				}
				cols.Add(dtb.Columns[c].ColumnName);

				if (c < nCol - 1) sb.Append("\t");
			}

			// get table content
			foreach (DataRow dr in dtb.Rows) {
				sb.Append("\n");
				//for (int c = 0; c < nCol; c++) {
				//   sb.Append(dr[c]);
				//   if (c < nCol - 1) sb.Append("\t");
				//}
				int c = 0;
				foreach (string s in  cols ) {
					sb.Append(dr[s]);
					if (c < nCol - 1) sb.Append("\t");
					c++;
				}
			}

			//Set clipboard
			Clipboard.SetDataObject(sb.ToString());
		}
		public static void CopyDatatableToClipboard(C1FlexGrid gr)
		{
			//gr.SaveExcel("qwert.xls", FileFlags.AsDisplayed);

			StringBuilder sb = new StringBuilder();
			string val;

			//Clipboard.SetDataObject(gr.GetCellRange(0, 0, gr.Rows.Count - 1, gr.Cols.Count-1).Clip);			
			//return;

			string colname;
			for (int r = 0; r < gr.Rows.Count; r++) {
				for (int c = 0; c < gr.Cols.Count; c++) {
					colname = gr.Cols[c].Name;
					if (gr.Cols[c].Visible && gr.Rows[r].DataIndex > -1 && gr.Cols[c].DataIndex > -1) {
						val = gr[r, c].ToString();

						if (r == 0) {
							val = val.Replace("\r\n", " ");
							val = val.Replace("\n", " ");
						}
						sb.Append(val);
						if (c < gr.Cols.Count - 1) sb.Append("\t");
					}
				}
				if (r < gr.Rows.Count - 1) sb.Append("\n");
			}

			//Set clipboard
			Clipboard.SetDataObject(sb.ToString());
		}

		public static void AddColumns(DataTable table, Dictionary<string, string> dict)
		{
			DataColumn dc;
			foreach (string key in dict.Keys) {
				dc = table.Columns.Add(key, typeof(float), dict[key]);
				dc.DefaultValue = 0;
			}
		}
		public static void AddColumns(DataTable table, Dictionary<string, string> dict, string rel, string rel2)
		{
			////DataColumn dc;

			////string column0Name = "";
			////if (table.ChildRelations.Contains(rel))
			////   column0Name = table.ChildRelations[rel].ChildTable.Columns[0].ColumnName;
			////foreach (string key in dict.Keys) {
			////   if (dict[key].IndexOf("{0}") > 0)
			////      dc = table.Columns.Add(key, typeof(float), string.Format(dict[key], rel, column0Name));
			////   else if (dict[key].IndexOf("{2}") > 0 && rel2 != "")
			////      dc = table.Columns.Add(key, typeof(float), string.Format(dict[key], "", "", rel2));
			////   else if (key.StartsWith("m") && rel2 == "")
			////      continue;
			////   else
			////      dc = table.Columns.Add(key, typeof(float), dict[key]);

			////   dc.DefaultValue = 0;
			////}
			DataColumn dc;

			string column0Name = "";
			if (table.ChildRelations.Contains(rel))
				column0Name = table.ChildRelations[rel].ChildTable.Columns[0].ColumnName;
			string expr;
			foreach (string key in dict.Keys) {
				expr = dict[key];
				expr = expr.Replace("rel", rel);
				if (table.Columns.Contains(key))
					continue;
				if (dict[key].IndexOf("{0}") > 0)
					dc = table.Columns.Add(key, typeof(float), string.Format(expr, rel, column0Name));
				else if (expr.IndexOf("{2}") > 0 && rel2 != "")
					dc = table.Columns.Add(key, typeof(float), string.Format(expr, "", "", rel2));
				else if (key.StartsWith("m") && rel2 == "")
					continue;
				else
					dc = table.Columns.Add(key, typeof(float), expr);

				dc.DefaultValue = 0;
			}
		}

		public static void SetupColumns(C1FlexGrid fg, Dictionary<string, GridColumn> columns)
		{
			DataTable dtb;
			if (fg.DataSource.GetType() == typeof(DataTable))
				dtb = fg.DataSource as DataTable;
			else if (fg.DataSource.GetType() == typeof(DataView))
				dtb = (fg.DataSource as DataView).Table;
			else
				return;

			int position = fg.Cols.Fixed;
			foreach (string col in columns.Keys) {
				if (dtb.Columns.Contains(col)){
					fg.Cols[col].Visible = true;
					fg.Cols[col].Move(position);
					if (!String.IsNullOrEmpty(columns[col].Name))		fg.Cols[col].Caption = columns[col].Name;
					if (!String.IsNullOrEmpty(columns[col].Format))	fg.Cols[col].Format = columns[col].Format;
					if (columns[col].Width != 0)		fg.Cols[col].Width = columns[col].Width;
					//if (col.StartsWith("Mark")) {

					//}
					position++;
				}
			}
		
			foreach (DataColumn col in dtb.Columns)
				if (!columns.ContainsKey(col.ColumnName))
					fg.Cols[col.ColumnName].Visible = false;

		}
		public static DataGridViewTextBoxColumn AddColumnToGrid(string PropName, string Header, bool ReadOnly, int Width, bool Right, string Format)
		{
			DataGridViewTextBoxColumn c = new DataGridViewTextBoxColumn();
			if (PropName != "") c.DataPropertyName = PropName;
			c.HeaderText = Header;
			c.Name = PropName;
			c.ReadOnly = ReadOnly;
			if (Width == 0)
				c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			else
				c.Width = Width;
			if (Right)
				c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			if (Format != "")
				c.DefaultCellStyle.Format = Format;

			return c;
		}
		public static DataGridViewTextBoxColumn AddColumnToGrid(string Name, string PropName, string Header, bool ReadOnly, int Width, bool Right, string Format)
		{
			DataGridViewTextBoxColumn c = new DataGridViewTextBoxColumn();
			if (PropName != "") c.DataPropertyName = PropName;
			c.HeaderText = Header;
			c.Name = PropName;
			c.ReadOnly = ReadOnly;
			if (Width == 0)
				c.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			else
				c.Width = Width;
			if (Right)
				c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			if (Format != "")
				c.DefaultCellStyle.Format = Format;

			return c;
		}

		public static string GetTimeSpanString(int minutes)
		{
			switch (frmMonitor.TmStyle) {
				case frmMonitor.TimeStyle.MMM:
					return minutes.ToString();
					break;
				case frmMonitor.TimeStyle.HHxMM:
					if (minutes < 0) {
						minutes = 0 - minutes;
						return string.Format("-{0}:{1:00}", (int)(minutes / 60), minutes % 60);
					} else
						return string.Format("{0}:{1:00}", (int)(minutes / 60), minutes % 60);
					break;
				default:
				case frmMonitor.TimeStyle.HHxHH:
					return (minutes / 60F).ToString("0.00");
					break;
			}
		}
		public static string GetTimeSpanString(string minutes)
		{
			Single mins;
			Single.TryParse(minutes, out mins);
			return GetTimeSpanString((int)mins);
		}
		public static string GetTimeSpanString(object minutes)
		{
			return GetTimeSpanString(minutes.ToString());
		}
		public static string GetTimeSpanString(float minutes)
		{
			if (float.IsNaN(minutes) || float.IsInfinity(minutes)) minutes = 0;
			return GetTimeSpanString(C.GetInt(minutes));
		}

		public static string GetFloatString(float value)
		{
			if (Math.Abs(value) > 1)
				return value.ToString("0.#");
			else if (Math.Abs(value) > 0.1)
				return value.ToString("0.##");
			else if (Math.Abs(value) > 0.01)
				return value.ToString("0.###");
			return "0";
		}
		public static string GetFloatString(string value)
		{
			Single res;
            if (Single.TryParse(value, out res))
                return GetFloatString(res);
            else
                return "0";
		}

		public static DataTable GetDataTable(object obj)
		{
			if (obj.GetType() == typeof(DataGridView))
				return GetDataSourceTable((obj as DataGridView).DataSource);
			else if (obj.GetType() == typeof(C1FlexGrid))
				return GetDataSourceTable((obj as C1FlexGrid).DataSource);
			else if (obj.GetType() == typeof(BindingSource))
				return GetDataSourceTable((obj as BindingSource).DataSource);
			else
				return null;
		}
		public static DataTable GetDataSourceTable(object obj_datasource)
		{
			DataTable dtb = null;
			if (obj_datasource.GetType() == typeof(DataTable))
				dtb = obj_datasource as DataTable;
			else if (obj_datasource.GetType() == typeof(DataView))
				dtb = (obj_datasource as DataView).Table;
			return dtb;
		}

		public static T StrToEnum<T>(string name)
		{
			return (T)Enum.Parse(typeof(T), name, true);
			////////////////////////////////////// How to use:
			//////public enum PartOfTheDay
			//////{
			//////   Morning, Day, Evening, Night
			//////}
			//////PartOfTheDay d = StrToEnum<PartOfTheDay>("Evening");
			////////d is now PartOfTheDay.Evening
		}
		public static string StrJoin(string str1, string str2)
		{
			if (str1 == null) str1 = "";
			if (str2 == null) str2 = "";
			string strAnd = (str1 != string.Empty && str2 != string.Empty) ? " and " : "";
			return string.Concat(str1, strAnd, str2);
			//return string.Join(" and ", new string[] { str1, str2 });
		}
		public static string GetRowField(DataTable dtb, string filter, string sort, string field)
		{
			DataRow[] DR = dtb.Select(filter, sort);
			if (DR.Length > 0 && dtb.Columns.Contains(field))
				return DR[0][field].ToString();
			return "";
		}

	}
	

	#endregion

	#region | Custom ToolTip          |

	public class ToolTipMemo : ToolTip
	{
		private int _x;
		private int _y;
		private Control _control;
		private string _caption;
		bool _inuse = false;

		public ToolTipMemo(IContainer cnt)
			: base(cnt)
		{

			base.UseAnimation = false;
			//base.ShowAlways = true;
			// Это максимум, если попробовать больше, то сбросится на умолчание = 5000 ms
			base.AutoPopDelay = 32767;
		}

		public void SetToolTip(int X, int Y, Control control, string caption)
		{
			try {
				if (_inuse)
					return;

				_inuse = true;

				if (_control == null)
					_control = control;
				if (_control != control ||
						_x != X || _y != Y ||
						_caption != caption) {
					_control = control;
					_caption = caption;
					_x = X;
					_y = Y;
					base.SetToolTip(control, caption);
				}
			} catch { } finally { _inuse = false; }
		}
		public void SetToolTip()
		{
			if (_control != null) {
				_x = -1;
				_y = -1;
				base.SetToolTip(_control, "");
			}
		}
		//public new void SetToolTip(Control control, string caption)
		//{
		//   if (_control == null)
		//      _control = control; 
		//   if (_control != control ||
		//         _caption != caption) {
		//      _control = control;
		//      _caption = caption;
		//      base.SetToolTip(control, caption);
		//   }
		//}
	}

	#endregion

	#region | GridColumn              |
	public class GridColumn
	{
		public enum Measure
		{
			String = 0,
			Time = 1,
			Percents = 2,
			Pieces = 3,
			Volume = 4,
			Counter = 5
		}
		public GridColumn(string name, string format, int width)
		{
			Name = name;
			Format = format;
			Width = width;
		}
		public string Name { get; set; }
		public string Format { get; set; }
		public int Width { get; set; }
		public int Order { get; set; }
		public Measure MeasureUnit { get; set; }
	}
	#endregion

	#region | User session data       |

	internal class MonitorUser
	{
		private int id = -1;
		private string name = "";
		private int permission = 0; //			UserPermit Permission;
		//private bool admin;
		//private bool localadmin;
		private bool restriction_was_shown;
		private int session;
		internal MonitorUser(int Id, string Name, int Session)
		{
			id = Id;
			name = Name;
			session = Session;
		}
		public int Id
		{
			get { return id; }
		}
		public string Name
		{
			get { return name; }
		}
		public int Permission
		{
			get { return permission; }
			set { permission = value; } 
		}
		public bool Admin
		{
			get { return ((ProductionMonitor.UserPermit)permission & UserPermit.Admin) > 0; }
		}
		public bool LocalAdmin
		{
			get { return ((ProductionMonitor.UserPermit)permission & UserPermit.LocalAdmin) > 0; }
		}
		public UserPermit UserPermit
		{
			get { return (ProductionMonitor.UserPermit)permission; }
		}
		public int Session
		{
			get { return session; }
		}
		public bool RestrictedMessageWasShown
		{
			get { return restriction_was_shown; }
			set { restriction_was_shown = value; }
		}
	}
	[FlagsAttribute]
	internal enum UserPermit
	{
		None = 0,					// пользователь не прошел аутентификацию и не может ничего редактировать
		Disabled = 1,				// закрыт доступ к мастер данным
		LocalForeman = 2,			// доступ к редактированию текущих данных на доступных линиях
		Foreman = 4,				// доступ к редактированию текущих данных на всех линиях
		LocalAdmin = 8,			// разрешено
		Admin = 16
	}
	internal class SessData
	{
		public int SessionID;
		/// <summary>
		/// означает что юзеру можно редактировать данные
		/// </summary>
		public bool Valid;
		public UserPermit Permission;
		/// <summary>
		/// Чтобы не досаждать юзеру одним и тем же баном - показываем один раз
		/// и устанавливаем этот флаг что сообщение было показано
		/// </summary>
		public bool RestrictedMessageWasShown;
	}

	#endregion

	#region | Custom DataGridView ctr |
	public class DataGridViewR : DataGridView
	{
		protected override bool ProcessDialogKey(Keys keyData)
		{
			Keys key = (keyData & Keys.KeyCode);

			if (key == Keys.Enter && this.CurrentCell != null &&
					this.CurrentCell.ColumnIndex == this.Columns.Count - 1 &&
						this.CurrentCell.RowIndex == this.Rows.Count - 1) {

				//DataRowView drv = this.Rows[this.CurrentCell.RowIndex].DataBoundItem as DataRowView;
				//if (drv != null) drv.EndEdit();
				base.CommitEdit(DataGridViewDataErrorContexts.Commit);
				DataRowView drv = this.Rows[this.CurrentCell.RowIndex].DataBoundItem as DataRowView;
				if (drv != null) drv.EndEdit();


				this.CurrentCell = this[0, CurrentCell.RowIndex];
				//return this.ProcessTabKey(Keys.Tab);
				return true;	//this.ProcessTabKey(keyData);
			}

			if (key == Keys.Enter) {
				// "Enter" подменяем табом и посылаем таб, если же посылать keyData 
				// то будет срабатывать сохранение при каждой смене колонки
				return this.ProcessTabKey(Keys.Tab); //  this.ProcessTabKey(keyData);
			}

			return base.ProcessDialogKey(keyData);
		}
		protected override bool ProcessDataGridViewKey(KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter) {
				return this.ProcessTabKey(Keys.Tab);//this.ProcessTabKey(e.KeyCode);
			}
			return base.ProcessDataGridViewKey(e);
		}

		////protected override void OnCellEnter(DataGridViewCellEventArgs e)
		////{
		////   //can this cell be entered?
		////   if (this.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) {
		////      //this cell is readonly, find the next tabable cell
		////      if (!setNextTabableCell(e.ColumnIndex, e.RowIndex)) {
		////         //or tab to the next control
		////         setNextTabableControl();
		////      } 
		////   }
		////   base.OnCellEnter(e);
		////}
		////private delegate void SetColumnAndRowOnGrid(DataGridView grid, int i, int o);
		//////***********************************************
		//////Find the next cell that we want to be selectable
		//////***********************************************
		////private bool setNextTabableCell(int nextColumn, int nextRow)
		////{
		////   //keep selecting each next cell until one is found that isn't either readonly or invisable
		////   do {
		////      //at the last column, move down a row and go the the first column
		////      if (nextColumn == this.Columns.Count - 1) {
		////         nextColumn = 0;
		////         //at the last row and last column exit this method, no cell can be selected
		////         if (nextRow == this.Rows.Count - 1) {
		////            return false;
		////         } else {
		////            nextRow = Math.Min(this.Rows.Count - 1, nextRow + 1);
		////         }
		////      } else {
		////         nextColumn = Math.Min(this.Columns.Count - 1, nextColumn + 1);
		////      }
		////   }
		////   while (this.Rows[nextRow].Cells[nextColumn].ReadOnly == true ||
		////               this.Rows[nextRow].Cells[nextColumn].Visible == false);

		////   //a cell has been found that can be entered, use the delegate to select it
		////   SetColumnAndRowOnGrid method = new SetColumnAndRowOnGrid(setGridCell);
		////   this.BeginInvoke(method, this, nextColumn, nextRow);
		////   //that's all I have to say about that
		////   return true;
		////}
		//////***********************************************
		////// Method pointed to by the delegate
		//////***********************************************
		////private void setGridCell(DataGridView grid, int columnIndex, int rowIndex)
		////{
		////   grid.CurrentCell = grid.Rows[rowIndex].Cells[columnIndex];
		////   //grid.BeginEdit(true);
		////}
		//////***********************************************
		////// All the cells in the grid have been tried, none could be tabbed
		////// to so move onto the next control
		//////***********************************************
		////private void setNextTabableControl()
		////{
		////   this.SelectNextControl(this, true, true, true, true);
		////}

	}

	public class DataGridViewComboBoxEditingControlR : DataGridViewComboBoxEditingControl
	{
		protected override bool ProcessDialogKey(Keys keyData)
		{
			Keys key = (keyData & Keys.KeyCode);
			if (key == Keys.Space) {
				this.DroppedDown = true;
				return true; // base.ProcessDialogKey(keyData);
			}
			//if (key == Keys.Enter) {
			//   // "Enter" подменяем табом и посылаем таб, если же посылать keyData 
			//   // то будет срабатывать сохранение при каждой смене колонки
			//   return this.ProcessDialogKey(Keys.Tab); //  this.ProcessTabKey(keyData);
			//}
			return base.ProcessDialogKey(keyData);
		}
	}
	public class DataGridViewComboBoxCellR : DataGridViewComboBoxCell
	{
		public override Type EditType
		{
			get { return typeof(DataGridViewComboBoxEditingControlR); }
		}
	}
	public class DataGridViewComboBoxColumnR : DataGridViewComboBoxColumn
	{
		public DataGridViewComboBoxColumnR()
		{
			this.CellTemplate = new DataGridViewComboBoxCellR();
		}
	}

	#endregion

	#region | Custom C1Chart          |
	
	public class TimeLineChart : C1Chart
	{

		private DateTime _DateTime;
		private DateTime _BaseDateTime;
        public  DateTime _DateTime_LastRun; //  time last runing get data process , when day = today
		private double _maxy;
		private double _minx = 0;
		private MU _unit;
		private SelectedZone _sz;

		public void Init(DateTime dt, MU Unit)
		{
			_maxy = 1;
			_DateTime = dt;
			//_BaseDateTime = _DateTime.AddDays(-1).Date;
			//_BaseDateTime = dt.AddDays(-dt.DayOfYear - 1).Date;
			_BaseDateTime = new DateTime( 2000,1,1).AddDays(-1);
			_minx = _DateTime.Subtract(_BaseDateTime).TotalDays;
			_unit = Unit;
		}
		public int L
		{
			get { return _unit == null ? -1 : _unit.L; }
		}
		public DateTime BaseDateTime
		{
			get { return _BaseDateTime; }
		}
		public double MaxY
		{
			get { return _maxy; }
			set { _maxy = value; }
		}
		public DateTime GetDateTime(double d)
		{
			return _BaseDateTime.AddDays(d);
		}
		public double GetDouble(DateTime dt)
		{
			return dt.Subtract(_BaseDateTime).TotalDays;
		}
		public int GetInt(DateTime dt)
		{
			return (int)dt.Subtract(_BaseDateTime).TotalMinutes;
		}
		public double MinX
		{
			get { return _minx; }
		}
		public MU Unit
		{
			get {return _unit; }
			set { _unit = value;}
		}
		public SelectedZone SelectedZone
		{
			get { return _sz; }
			set { _sz = value; }
		}


	}
	public class LossesChart : C1Chart
	{
		private Dictionary<string, string> tags = new Dictionary<string, string>();
		private Dictionary<string, string> roots = new Dictionary<string, string>();
		private Stack<string> navigation = new Stack<string>();

		public string GetPointTag(int g, int s, int p)
		{
			string key = string.Format("g{0}s{1}p{2}", g, s, p);
			if (tags.ContainsKey(key))
				return tags[key];
			else
				return "";
		}
		public void SetPointTag(int g, int s, int p, string tag)
		{
			string key = string.Format("g{0}s{1}p{2}", g, s, p);
			if (tags.ContainsKey(key))
				tags[key] = tag;
			else
				tags.Add(key, tag);
		}
		public string GetPointRoot(int g, int s, int p)
		{
			string key = string.Format("g{0}s{1}p{2}", g, s, p);
			if (roots.ContainsKey(key))
				return roots[key];
			else
				return "";
		}
		public void SetPointRoot(int g, int s, int p, string tag)
		{
			string key = string.Format("g{0}s{1}p{2}", g, s, p);
			if (roots.ContainsKey(key))
				roots[key] = tag;
			else
				roots.Add(key, tag);
		}
		public Stack<string> NavigationStack
		{
			get { return navigation; }
		}
		public string CurrentTag
		{
			get { return navigation.Peek(); }
		}
	}

	#endregion

	#region | C1FlexGrid bottom panel |
	
	internal class FlexFreezeBottom : Panel
	{
		private C1.Win.C1FlexGrid.C1FlexGrid _frz;
		private C1.Win.C1FlexGrid.C1FlexGrid _main;
		private int _rows;
		SolidBrush backbrush = new SolidBrush(frmMonitor._colHalfControl);


		internal FlexFreezeBottom(C1.Win.C1FlexGrid.C1FlexGrid main, int rows)
		{
			// save info we'll need later
			_main = main;
			_rows = rows;

			// create child grid with frozen bottom rows
			_frz = new C1.Win.C1FlexGrid.C1FlexGrid();
			_frz.ScrollBars = ScrollBars.None;
			_frz.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.None;
			_frz.DrawMode = DrawModeEnum.OwnerDraw;
			_frz.OwnerDrawCell += new C1.Win.C1FlexGrid.OwnerDrawCellEventHandler(_frz_OwnerDrawCell);
			_frz.SetupEditor += new C1.Win.C1FlexGrid.RowColEventHandler(_frz_SetupEditor);
			_frz.AfterScroll += new C1.Win.C1FlexGrid.RangeEventHandler(_frz_AfterScroll);
			_frz.GridChanged += new C1.Win.C1FlexGrid.GridChangedEventHandler(_main_GridChanged);
			Controls.Add(_frz);

			// bind frozen grid to main grid (share data, layout, styles, selection)
			_frz.DataSource = main;

			// add this to the main grid
			main.Controls.Add(this);

			// hook up the events we need to synchronize with the main grid
			main.Resize += new System.EventHandler(_main_Resize);
			main.VisibleChanged += new System.EventHandler(_main_Resize);
			main.AfterScroll += new C1.Win.C1FlexGrid.RangeEventHandler(_main_AfterScroll);
			main.SelChange += new System.EventHandler(_main_SelChange);
			main.GridChanged += new C1.Win.C1FlexGrid.GridChangedEventHandler(_main_GridChanged);

			// initialize
			AdjustLayout();
		}

		internal void AdjustLayout()
		{
			// get index of the bottom (frozen) rows
			RowCollection rows = _main.Rows;
			int r2 = rows.Count - 1;
			int r1 = r2 - _rows + 1;
			if (r1 <= rows.Fixed) r1 = rows.Fixed;

			// hide control if the main grid is big enough
			Visible = (r1 <= r2);
			if (!Visible) return;

			// set control size and location
			Rectangle rc = _main.ClientRectangle;
			if (rows[r2].Bottom <= rc.Bottom) {
				Visible = false;
				return;
			}
			int hei = rows[r2].Bottom - rows[r1].Top + 1;
			rc.Y = rc.Bottom - hei;
			rc.Height = hei;
			Bounds = rc;

			// adjust child grid to hide fixed rows
			rc = ClientRectangle;
			hei = rows[rows.Fixed + rows.Frozen].Top;
			rc.Y -= hei;
			rc.Height += hei;
			_frz.Bounds = rc;

			// syncronize scroll position
			SynchScrollPosition(false);
		}
		internal void SynchScrollPosition(bool synchMain)
		{
			Point pos;

			// optionally synchronize X scroll position on the main grid
			if (synchMain) {
				pos = _frz.ScrollPosition;
				pos.Y = _main.ScrollPosition.Y;
				_main.ScrollPosition = pos;
			}

			// syncronize X scroll position of the frozen grid with main
			pos = _main.ScrollPosition;
			pos.Y = -64000;
			_frz.ScrollPosition = pos;

			// update everything right away
			_main.Update();
			_frz.Update();
		}

		// make normal and scrollable areas look different
		// (when painting and when editing)
		private void _frz_OwnerDrawCell(object sender, C1.Win.C1FlexGrid.OwnerDrawCellEventArgs e)
		{
			string colname = _frz.Cols[e.Col].Name;
			if (e.Style.BackColor == _frz.Styles.Normal.BackColor) {
				e.Graphics.FillRectangle(backbrush, e.Bounds);
				if (colname == "Time" || colname.StartsWith("MT"))
					e.DrawCell(DrawCellFlags.Border);
				else
					e.DrawCell(DrawCellFlags.Border | DrawCellFlags.Content);

			}

			float val = 0;
			if (float.TryParse(e.Text, out val)) {
				if (colname == "Time" || colname.StartsWith("MT")) {
					if (val == 0)
						e.Text = "";
					else
						e.Text = Utils.GetTimeSpanString(val);
					e.DrawCell(DrawCellFlags.Border | DrawCellFlags.Content | DrawCellFlags.Background);
					e.Handled = true;
				} else if (colname == "Ao" || colname == "Perc" || colname.StartsWith("M_"))
					e.Text = Utils.GetFloatString(val * 100);
			}
		}
		private void _frz_SetupEditor(object sender, C1.Win.C1FlexGrid.RowColEventArgs e)
		{
			_frz.BackColor = frmMonitor._colHalfControl;
		}

		// adjust layout after resizing the main grid (our container)
		private void _main_Resize(object sender, System.EventArgs e)
		{
			AdjustLayout();
		}

		// synchronize scrolling
		private void _main_AfterScroll(object sender, C1.Win.C1FlexGrid.RangeEventArgs e)
		{
			SynchScrollPosition(false);
		}
		private void _frz_AfterScroll(object sender, C1.Win.C1FlexGrid.RangeEventArgs e)
		{
			// if the frozen grid has the focus and the user selects non-frozen
			// cells, scroll main grid up.
			if (_frz.Focused) {
				int row = Math.Min(_frz.Row, _frz.RowSel);
				int bot = _main.Rows[row].Bottom + _main.ScrollPosition.Y;
				if (bot > Top) {
					// unless this is a mouse selection, give focus to main grid
					if (!_frz.Capture) _main.Focus();
					_main.TopRow = _main.Row;
				}
			}
			SynchScrollPosition(true);
		}

		// scroll while selecting
		// if the user extends the selection down on the main grid, scroll it down.
		private void _main_SelChange(object sender, System.EventArgs e)
		{
			// only when the main grid has the focus!
			if (!_main.Focused) return;

			// calculate selection bottom (accounting for scrolling)
			int row = Math.Max(_main.Row, _main.RowSel);
			if (row < 0)
				return;
			int bot = _main.Rows[row].Bottom + _main.ScrollPosition.Y;

			// try scrolling if past the bottom of the frozen area
			if (bot > Top) _main.TopRow++;
		}

		// adjust layout after massive changes (resize rows/cols, sort, etc)
		private void _main_GridChanged(object sender, C1.Win.C1FlexGrid.GridChangedEventArgs e)
		{
			if (_main.Redraw == false)
				return;
			switch (e.GridChangedType) {
				case GridChangedTypeEnum.AfterCollapse:
				case GridChangedTypeEnum.GridChanged:
				case GridChangedTypeEnum.LayoutChanged:
					AdjustLayout();
					break;
			}
		}
	}

	#endregion

}
