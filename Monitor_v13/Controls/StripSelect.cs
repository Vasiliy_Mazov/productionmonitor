using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using ProductionMonitor;

namespace ProductionMonitor
{
    public class StripSelect : ToolStripControlHost // System.Windows.Forms.ToolStripControlHost
	{
		#region | Form Designer     |

       

		public StripSelect(ArrayList Lns)
			: base(new UserControl()) 
        { 
		
			//This call is required by the Windows Form Designer.
			InitializeComponent();

			//Add any initialization after the InitializeComponent() call
            if (!frmMonitor.use_easy_counter)
			_unitsIntArr          = Lns;
            _unitsIntArr_Counters = Lns;

			Init();
		}

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
				components.Dispose();
			base.Dispose(disposing);
		}


         
		private System.ComponentModel.IContainer components;

		internal System.Windows.Forms.Button _btOK;
		internal System.Windows.Forms.Button _btCancel;
		internal System.Windows.Forms.TreeView _tree;
        internal System.Windows.Forms.TreeView _tree2;
		internal System.Windows.Forms.MonthCalendar _mc;
		internal System.Windows.Forms.ToolTip _tt;
		internal System.Windows.Forms.Panel _pn;
		private System.Windows.Forms.VScrollBar _sb;
		private System.Windows.Forms.TextBox _txtN;
		private System.Windows.Forms.DateTimePicker _dtPickerFrom;
		private System.Windows.Forms.DateTimePicker _dtPickerTo;
		internal System.Windows.Forms.RadioButton _rbDays;
		internal System.Windows.Forms.RadioButton _rbWeeks;
		internal System.Windows.Forms.RadioButton _rbMonths;
		internal System.Windows.Forms.RadioButton _rbTime;




		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._tree = new System.Windows.Forms.TreeView();
            this._tree2 = new System.Windows.Forms.TreeView();
			this._btOK = new System.Windows.Forms.Button();
			this._mc = new System.Windows.Forms.MonthCalendar();
			this._btCancel = new System.Windows.Forms.Button();
			this._pn = new System.Windows.Forms.Panel();
			this._rbDays = new System.Windows.Forms.RadioButton();
			this._dtPickerFrom = new System.Windows.Forms.DateTimePicker();
			this._txtN = new System.Windows.Forms.TextBox();
			this._sb = new System.Windows.Forms.VScrollBar();
			this._dtPickerTo = new System.Windows.Forms.DateTimePicker();
			this._rbWeeks = new System.Windows.Forms.RadioButton();
			this._rbMonths = new System.Windows.Forms.RadioButton();
			this._rbTime = new System.Windows.Forms.RadioButton();
			this._tt = new System.Windows.Forms.ToolTip(this.components);
			//
			// Tree
			//
			this._tree.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._tree.ForeColor = System.Drawing.SystemColors.WindowText;
			this._tree.FullRowSelect = true;
			this._tree.HideSelection = false;
            this._tree.Location = new System.Drawing.Point(6, 2);  // (6, 6);
			this._tree.Name = "Tree";
		//	this._tree.Size = new System.Drawing.Size(130, 344);
            this._tree.Size = new System.Drawing.Size(140, 2);  //(120, 100);
			this._tree.TabIndex = 3;
			this._tree.TabStop = false;
			this._tree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.Tree_AfterCheck);
			this._tree.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.Tree_BeforeCheck);
			this._tree.KeyDown += new KeyEventHandler(StripSelect_KeyDown);
            //
            // Tree2
            //
            this._tree2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._tree2.ForeColor = System.Drawing.SystemColors.WindowText;
            this._tree2.FullRowSelect = true;
            this._tree2.HideSelection = false;
            this._tree2.Location = new System.Drawing.Point(6, 10);//(6, 144);
            this._tree2.Name = "Tree2";
            this._tree2.Size = new System.Drawing.Size(140, 284);//(120, 164)
            this._tree2.TabIndex = 3;
            this._tree2.TabStop = false;
            this._tree2.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.Tree_AfterCheck);
            this._tree2.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.Tree_BeforeCheck);
            this._tree2.KeyDown += new KeyEventHandler(StripSelect_KeyDown);
            this._tree2.BackColor = System.Drawing.SystemColors.Info;


			//
			// mc
			//
			this._mc.Location = new System.Drawing.Point(1, 132);
			this._mc.MaxDate = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
			this._mc.MaxSelectionCount = 1;
			this._mc.MinDate = new System.DateTime(2005, 1, 1, 0, 0, 0, 0);
			this._mc.ShowWeekNumbers = true;
			this._mc.TabIndex = 4;
			this._mc.TitleBackColor = System.Drawing.SystemColors.GrayText;
			this._mc.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this._mc_DateChanged);
			this._mc.CreateControl();
			this._mc.KeyDown += new KeyEventHandler(StripSelect_KeyDown);
			//
			// pn
			//
			this._pn.BackColor = System.Drawing.SystemColors.Window;
			this._pn.Controls.Add(this._rbDays);
			this._pn.Controls.Add(this._dtPickerFrom);
			this._pn.Controls.Add(this._txtN);
			this._pn.Controls.Add(this._sb);
			this._pn.Controls.Add(this._dtPickerTo);
			this._pn.Controls.Add(this._mc);
			this._pn.Controls.Add(this._rbWeeks);
			this._pn.Controls.Add(this._rbMonths);
			this._pn.Controls.Add(this._rbTime);
			this._pn.Location = new System.Drawing.Point(_tree.Width + 11, _tree.Top);
			this._pn.Size = new System.Drawing.Size(_mc.Width + 2, 291);
			this._pn.TabIndex = 0;
			this._pn.TabStop = false;
			//
			// btOK
			//
			this._btOK.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._btOK.Size = new System.Drawing.Size(_mc.Width / 2 - 1, 22);
			this._btOK.Location = new System.Drawing.Point(_pn.Left + _btOK.Width + 5, _pn.Top + _pn.Height + 4);
			this._btOK.TabIndex = 0;
			this._btOK.TabStop = true;
			this._btOK.Text = "Ok";
			this._btOK.Click += new System.EventHandler(this.btOK_Click);
			this._btOK.Margin = new Padding(3, 3, 6, 3);
			this._btOK.KeyDown += new KeyEventHandler(btOK_KeyDown);
			//
			// btCancel
			//
			this._btCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._btCancel.Location = new System.Drawing.Point(_pn.Left, _btOK.Top);
			//this.btCancel.TabIndex = 2;
			this._btCancel.Size = new System.Drawing.Size(_btOK.Width, 22);
			this._btCancel.Text = "Cancel";
			this._btCancel.Click += new System.EventHandler(this.btCancel_Click);
			this._btCancel.TabStop = false;
			//
			// rbDays
			//
			this._rbDays.Checked = true;
			this._rbDays.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._rbDays.Location = new System.Drawing.Point(24, 60);
			this._rbDays.Size = new System.Drawing.Size(70, 15);
			this._rbDays.TabIndex = 2;  //2
			//this.rbDays.Text = Properties.Resources.strDays;
			this._rbDays.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
			//
			// dtFrom
			//
			this._dtPickerFrom.CustomFormat = "";
			this._dtPickerFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this._dtPickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this._dtPickerFrom.Location = new System.Drawing.Point(5, 4);
			this._dtPickerFrom.ShowUpDown = true;
			this._dtPickerFrom.Size = new System.Drawing.Size(_mc.Width - 8, 20);
			this._dtPickerFrom.ValueChanged += new System.EventHandler(this._dtFrom_ValueChanged);
			this._dtPickerFrom.TabStop = false;
			//
			// dtTo
			//
			this._dtPickerTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this._dtPickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this._dtPickerTo.Location = new System.Drawing.Point(5, 28);
			this._dtPickerTo.Name = "dtTo";
			this._dtPickerTo.ShowUpDown = true;
			this._dtPickerTo.Size = new System.Drawing.Size(_dtPickerFrom.Width, 20);
			this._dtPickerTo.ValueChanged += new System.EventHandler(this._dtTo_ValueChanged);
			this._dtPickerTo.TabStop = false;
			//
			// txtN
			//
			this._txtN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._txtN.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._txtN.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this._txtN.Location = new System.Drawing.Point(120, 77);
			this._txtN.MaxLength = 3;
			this._txtN.Size = new System.Drawing.Size(28, 26);
			//this.txtN.TabIndex = 7;
			this._txtN.Text = "0";
			this._txtN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this._txtN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._txtN_KeyPress);
			this._txtN.TextChanged += new System.EventHandler(this._txtN_TextChanged);
			this._txtN.TabStop = false;
			//
			// sb
			//
			this._sb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._sb.LargeChange = 1;
			this._sb.Location = new System.Drawing.Point(119, 61);
			this._sb.Maximum = -1;
			this._sb.Minimum = -12;
			this._sb.Name = "sb";
			this._sb.Size = new System.Drawing.Size(30, 60);
			this._sb.TabIndex = 1;
			this._sb.Value = -1;
			this._sb.ValueChanged += new System.EventHandler(this._sb_ValueChanged);
			this._sb.TabStop = true;
			//
			// rbWeeks
			//
			this._rbWeeks.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._rbWeeks.Location = new System.Drawing.Point(24, 76);
			this._rbWeeks.Size = new System.Drawing.Size(70, 15);
			this._rbWeeks.TabIndex = 2;
			//this.rbWeeks.Text = Properties.Resources.strWeeks;
			this._rbWeeks.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
			//
			// rbMonths
			//
			this._rbMonths.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._rbMonths.Location = new System.Drawing.Point(24, 92);
			this._rbMonths.Size = new System.Drawing.Size(70, 15);
			//this.rbMonths.Text = Properties.Resources.strMonths;
			this._rbMonths.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
			//
			// rbTime
			//
			this._rbTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this._rbTime.Location = new System.Drawing.Point(24, 108);
			this._rbTime.Size = new System.Drawing.Size(90, 15);
			//this.rbTime.Text = Properties.Resources.strAnyTime;
			this._rbTime.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
			//
			// tt
			//
			this._tt.AutoPopDelay = 1000;
			this._tt.InitialDelay = 500;
			this._tt.ReshowDelay = 100;
			//
			// this.Control
			//
			this.BackColor = System.Drawing.SystemColors.Control;
			this.Control.Controls.Add(this._tree);		// 0
            this.Control.Controls.Add(this._tree2);		// 0
			this.Control.Controls.Add(this._pn);		// 0
			this.Control.Controls.Add(this._btOK);		// 0
			this.Control.Controls.Add(this._btCancel);//
			this.KeyDown += new KeyEventHandler(StripSelect_KeyDown);

		//	_tree.Height = _btOK.Height + _pn.Height + 6;
			Control.Width = _tree.Width + _mc.Width - 6;
            Control.Height = _btOK.Height + _pn.Height + 6; //_tree.Height;
			//this.Control.TabIndex = 0;
		}

		#endregion | Form Designer     |

		#region | Declaration       |

		private DateTime _dtEnd;
		private DateTime _dtEndOld;
		private DateTime _dtStart;
		private DateTime _dtStartOld;
		private int _unitsCount = 1;  //2;
		private int _unitsOld;

		private MonitorConfig _config;
        private MonitorConfig_Counters _config_counters;


		private ArrayList _unitsIntArr;
        private ArrayList _unitsIntArr_Counters;

		private bool _inUse;

		public event EventHandler DataChanged;

		public class DataChangesArgs : EventArgs
		{
			public DateTime DateStart;
			public DateTime DateEnd;
			public IList<MU> Lines;
		}

		#endregion | Declaration       |

		private void Init()
		{
			if (MonitorConfig.Instance == null) 
				//return;

            if (MonitorConfig_Counters.Instance == null)
                return;

			SelectedUnits          = new List<MU>();
            SelectedUnits_Counters = new List<MU>();
			_config                = MonitorConfig.Instance;
            _config_counters       = MonitorConfig_Counters.Instance;

			BuildTree(_tree.Nodes, 0);


            BuildTreeCounters(_tree2.Nodes, 0);


			_sb.Minimum = -frmMonitor.MaxLines;
			CultureInfo ci = System.Threading.Thread.CurrentThread.CurrentCulture;
			_dtPickerFrom.CustomFormat = ci.DateTimeFormat.ShortDatePattern + "   HH:mm";		//"dd.MM.yyyy   HH:mm";
			_dtPickerTo.CustomFormat = ci.DateTimeFormat.ShortDatePattern + "   HH:mm";		//"dd.MM.yyyy   HH:mm";
			int h = SystemInformation.VerticalScrollBarArrowHeight;
			_txtN.Top = _sb.Top + h;
			_sb.Height = _txtN.Height + h * 2;

            if ( !frmMonitor.use_easy_counter)
            {
			// нужно для заполнения коллекции selectedunits
			SetTreeChecked(_tree.Nodes);
			GetFromTree(_tree.Nodes);
            }
            if (frmMonitor.use_easy_counter)
            {
                //_tree2.Nodes.ch
            SetTreeChecked_counters(_tree2.Nodes);
            GetFromTree_Counters(_tree2.Nodes);
            }

			#region | Подбираем ширину |
			
			// Ищем самое длинное название
			string ln = "";
			foreach (MU un in _config.MonitorUnits.Values) 
				if (ln.Length < un.LineName.Length)
					ln = un.LineName;
            foreach (MU un in _config_counters.MonitorUnits.Values)
                if (ln.Length < un.LineName.Length)
                    ln = un.LineName;

			// Вычисляем ширину самого длинного названия
			int width = 0;
			using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(new Bitmap(1, 1))) {
				width = (int)graphics.MeasureString(ln, _tree.Font).Width;
			}

			// Смещаем контролы под самое длинное название
			_pn.Left       += width;
			_btOK.Left     += width;
			_btCancel.Left += width;
			_tree.Width    += width;
            _tree2.Width   += width;
			Control.Width  += width;

			#endregion

		}

		#region | Properties        |

		internal MonitorConfig Config
		{
			get { return _config; }
		}

        internal MonitorConfig_Counters Config_Counters
        {
            get { return _config_counters; }
        }

		internal IList<MU> SelectedUnits
		{
			get;
			private set;
		}

        internal IList<MU> SelectedUnits_Counters
        {
            get;
            private set;
        }

		#endregion | Public Properties |

		public void ShowWindow(DateTime DateStart, DateTime DateEnd)
		{
			_inUse = true;
			_dtEnd = DateEnd;
			_dtEndOld = DateEnd;
			_dtStart = DateStart;
			_dtStartOld = DateStart;
			_mc.SetDate(_dtEnd.AddMinutes(-1));
			_unitsOld = _unitsCount;
			_sb.Value = -_unitsCount;
			_txtN.Text = _unitsCount.ToString();
			_inUse = false;

			_sb.Minimum = -frmMonitor.MaxTimes;
			SetItems();
            if (!frmMonitor.use_easy_counter)
			SetTreeChecked(_tree.Nodes);

            SetTreeChecked_counters(_tree2.Nodes);

		}

      
		#region | Buttons events    |

		private void btOK_Click(System.Object sender, System.EventArgs e)
		{


			this.PerformClick();

			foreach (MU mu in SelectedUnits)
				mu.Clear();
			SelectedUnits.Clear();
            SelectedUnits_Counters.Clear();
			if (_unitsIntArr != null)
				_unitsIntArr.Clear();
            if (_unitsIntArr_Counters != null)
                _unitsIntArr_Counters.Clear();

			GetFromTree(_tree.Nodes);
            GetFromTree_Counters(_tree2.Nodes);
			DataChangesArgs arg = new DataChangesArgs();
			arg.DateStart = _dtStart;
			arg.DateEnd = _dtEnd;
			arg.Lines = SelectedUnits;
			//if (unitsIntArr.Count > 0)
			DataChanged(this, arg);
		}

		private void StripSelect_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				btOK_Click(_btOK, null);
			else if (e.KeyCode == Keys.Right) {
				if (_rbDays.Checked)
					_rbWeeks.Checked = true;
				else if (_rbWeeks.Checked)
					_rbMonths.Checked = true;
				else if (_rbMonths.Checked)
					_rbTime.Checked = true;
				else if (_rbTime.Checked)
					_rbDays.Checked = true;
			} else if (e.KeyCode == Keys.Down) {
				if (_sb.Value < _sb.Maximum)
					_sb.Value++;
			} else if (e.KeyCode == Keys.Up) {
				if (_sb.Value > _sb.Minimum)
					_sb.Value--;
			} else
				return;
			e.Handled = true;
		}

		private void btOK_KeyDown(object sender, KeyEventArgs e)
		{
			btOK_Click(_btOK, null);
		}

		private void btCancel_Click(System.Object sender, System.EventArgs e)
		{
			this.PerformClick();
		}

		#endregion | Menus & Buttons   |

		#region | Tree              |

		private void Tree_BeforeCheck(object sender, System.Windows.Forms.TreeViewCancelEventArgs e)
		{ 
			TreeNode nd = (TreeNode)e.Node;
          //  nd.
			//int k = Tree_CountChecked(_tree.Nodes);
            int k = Tree_CountChecked(nd.Nodes);

			k += Convert.ToInt32(nd.Checked ? -1 : 1);
			if (k > frmMonitor.MaxLines && nd.Checked == false) {
				_tt.SetToolTip(_tree, String.Format("The selection more than {0} units are not allowed", frmMonitor.MaxLines));
				e.Cancel = true;
			} else
				_tt.RemoveAll();
		}

		private void Tree_AfterCheck(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if (_inUse)
				return;
			_inUse = true;
			TreeNode nn = e.Node;
			Tree_CheckChilds(nn);
			_inUse = false;
		}

		private void Tree_CheckChilds(TreeNode n)
		{
			// рекурсивная функция
			foreach (TreeNode nn in n.Nodes) {
				nn.Checked = n.Checked;
				if (nn.Checked != n.Checked)
					return;
				if (n.Checked)
					nn.EnsureVisible();
				else {
					n.Collapse();
					nn.Collapse();
				}
				Tree_CheckChilds(nn);
			}
		}

		private int Tree_CountChecked(TreeNodeCollection nn)
		{
			// рекурсивная функция
			int k = 0;
			foreach (TreeNode n in nn) {
				if (n.Nodes.Count > 0)
					k += Tree_CountChecked(n.Nodes);
				else
					if (n.Checked)
						k++;
			}
			return k;
		}
        private void SetTreeChecked_counters(TreeNodeCollection nn)
        {
            // рекурсивная функция
            foreach (TreeNode n in nn)
                if (n.Nodes.Count > 0)
                    SetTreeChecked_counters(n.Nodes);
                else
                    if (_unitsIntArr_Counters.Contains(((MU)n.Tag).ID))
                    {
                        n.Checked = true;
                        n.EnsureVisible();
                    }
        }
		private void SetTreeChecked(TreeNodeCollection nn)
		{
			// рекурсивная функция
			foreach (TreeNode n in nn)
				if (n.Nodes.Count > 0)
					SetTreeChecked(n.Nodes);
				else
					if (_unitsIntArr.Contains(((MU)n.Tag).ID)) {
						n.Checked = true;
						n.EnsureVisible();
					}

			////// procedure is used only for user convenience

			////InUse = true;
			////Tree.SuspendLayout();
			////Tree.CollapseAll();

			////// разворачиваем узлы с отмеченными линиями и отмечаем родителей
			//////...............

			////Tree.ResumeLayout();
			////InUse = false;
		}

		private void BuildTree(TreeNodeCollection tnc, int id)
		{
			if (tnc == null) return;
			TreeNode tn;
			foreach (MU un in _config.MonitorUnits.Values) {
				if (un.ParentID == id) {
					tn = new TreeNode(un.LineName);
					tn.Tag = un;
					tnc.Add(tn);
					BuildTree(tn.Nodes, un.ID);
				}
			}
		}

        private void BuildTreeCounters(TreeNodeCollection tnc, int id)
        {
            if (tnc == null) return;
            TreeNode tn;
            foreach (MU un in _config_counters.MonitorUnits.Values)
            {
                if (un.ParentID == id)
                {
                    tn = new TreeNode(un.LineName);
                    tn.Tag = un;
                    tnc.Add(tn);
                    BuildTreeCounters(tn.Nodes, un.ID);
                }
            }
        }

		private void GetFromTree(TreeNodeCollection tnc)
		{
			if (_config != null) {
				MU l;
				int cnt = 0;
				foreach (TreeNode tn in tnc) {
					if (tn.Checked) {
						l = tn.Tag as MU;
						if (l != null) {
							SelectedUnits.Add(l);
							_unitsIntArr.Add(l.ID);
						}
						cnt++;
					}
					if (tn.Nodes.Count > 0)
						GetFromTree(tn.Nodes);
				}
			}
		}

        private void GetFromTree_Counters(TreeNodeCollection tnc)
        {
            if (_config_counters != null)
            {
                MU l;
                int cnt = 0;
                foreach (TreeNode tn in tnc)
                {
                    if (tn.Checked)
                    {
                        l = tn.Tag as MU;
                        if (l != null)
                        {
                          //SelectedUnits_Counters.Add(l);
                            SelectedUnits_Counters.Insert(0, l);
                            //_unitsIntArr_Counters.Add(l.ID);
                            _unitsIntArr_Counters.Insert(0, l.ID);
                        }
                        cnt++;
                    }
                    if (tn.Nodes.Count > 0)
                        GetFromTree_Counters(tn.Nodes);
                }
            }
        }


		#endregion | Tree events       |

		#region | Control events    |

		private void _mc_DateChanged(System.Object sender, System.Windows.Forms.DateRangeEventArgs e)
		{
			if (_inUse)
				return;
			_dtEnd = _mc.SelectionEnd.Date;
			//	Debug.WriteLine("__mc_DateChanged");
			SetItems();
		}

		private void _sb_ValueChanged(object sender, System.EventArgs e)
		{
			if (_inUse)
				return;
			// Debug.WriteLine("__sb_ValueChanged");
			SetItems();
		}

		private void _rb_CheckedChanged(object sender, System.EventArgs e)
		{
			if (_inUse || (sender as RadioButton).Checked == false)
				return;
			// Debug.WriteLine("__rb_CheckedChanged");
			SetItems();
		}

		private void _dtFrom_ValueChanged(object sender, System.EventArgs e)
		{
			if (_inUse)
				return;
			if (_dtPickerFrom.Value >= _dtPickerTo.Value) {
				_dtPickerTo.Value = _dtPickerFrom.Value.AddMinutes(1);
				return;
			}
			SetItems();
		}

		private void _dtTo_ValueChanged(object sender, System.EventArgs e)
		{
			if (_inUse)
				return;
			if (_dtPickerFrom.Value >= _dtPickerTo.Value) {
				_dtPickerFrom.Value = _dtPickerTo.Value.AddMinutes(-1);
				return;
			}
			SetItems();
		}

		private void _txtN_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (Char.GetNumericValue(e.KeyChar) < 0)
				e.Handled = true;
		}

		private void _txtN_TextChanged(object sender, System.EventArgs e)
		{
			if (_inUse)
				return;
			if (_txtN.Text != "") {
				int intText = int.Parse(_txtN.Text);
				if (intText <= frmMonitor.MaxTimes && intText > 0)
					_sb.Value = -intText;
				else if (intText > frmMonitor.MaxTimes)
					_sb.Value = -frmMonitor.MaxTimes;
				else if (intText < 1)
					_sb.Value = -1;
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			//C.DrawRectangle(e.Graphics, new Rectangle(5, 5, Tree.Width + 1, Tree.Height + 1), C.BorderStyle.Solid);
			//C.DrawRectangle(pn.CreateGraphics(), new Rectangle(0, 0, pn.Width - 1, pn.Height - 1), C.BorderStyle.Solid);
			base.OnPaint(e);
		}


		#endregion


		private void SetItems()
		{
			if (_inUse)
				return;
			_inUse = true;

			_tree.CheckBoxes = true;
            _tree2.CheckBoxes = true;
			_sb.Enabled = !_rbTime.Checked;
			_txtN.Enabled = !_rbTime.Checked;

			bool TimeMode = _rbTime.Checked;
			_mc.Enabled = !TimeMode;
			_mc.ForeColor = TimeMode ? SystemColors.GrayText : SystemColors.WindowText;
			_dtPickerFrom.Enabled = TimeMode;
			_dtPickerTo.Enabled = TimeMode;

			_unitsCount = -_sb.Value;
			_txtN.Text = _unitsCount.ToString();

			MU.FixedDates = !_rbTime.Checked;
			if (TimeMode) {
				_dtStart = _dtPickerFrom.Value;
				_dtEnd = _dtPickerTo.Value.AddMinutes(1);
			} else {
				Calendar Cal = CultureInfo.InvariantCulture.Calendar;
				_dtEnd = _mc.SelectionEnd.Date;
				if (_rbDays.Checked) {
					_dtStart = _dtEnd.AddDays(-_unitsCount + 1);
					_dtEnd = _dtEnd.AddDays(1);
				} else if (_rbWeeks.Checked) {
					_dtEnd = _dtEnd.AddDays(7 - Convert.ToInt32(Cal.GetDayOfWeek(_dtEnd.AddDays(-1))));
					_dtStart = _dtEnd.AddDays(-7 * _unitsCount);
				} else if (_rbMonths.Checked) {
					_dtStart = _dtEnd.AddDays(-_dtEnd.Day + 1).AddMonths(-_unitsCount + 1);
					_dtEnd = _dtStart.AddMonths(_unitsCount);
				}

				_dtPickerFrom.Value = _dtStart;
				_dtPickerTo.Value = _dtEnd.AddMinutes(-1);
			}
			_inUse = false;
		}
        
	}
}