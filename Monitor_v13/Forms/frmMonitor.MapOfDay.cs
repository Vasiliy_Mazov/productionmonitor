﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using C1.Win.C1Chart;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{
	public sealed partial class frmMonitor
	{

        private bool is_mouse_move_process = false;
		private SelectedZone _sz;
		private int _iMouseDownX = 0;
		// нативное масштабирование работает очень неточно > отслеживаем сами:
		private double _dMouseDownX = 0;
		private double _dMouseUpX = 0;

		#region | Filling chart Map   |

		/// <summary>
		/// Заполнение графика "Карта суток"
		/// </summary>
		/// <param name="u">Line number</param>
		/// <param name="reload">True if need to reload completely.
		/// True устанавливать если только был переход на другой день или обновление всех данных (Refresh)
		/// </param>
		private void BuildChartMap(MU unit, bool reload)
		{
			Stopwatch sw = Stopwatch.StartNew();

			if (!_mapMode || unit == null || unit.IsEmpty())
				return;

			TimeLineChart chart = _ch[unit.L];
			if (reload) {
				chart.Header.Text = unit.GetLongUnitName(false);
				//DateTime dt = MU.use_timecorrection ? MU.dtStart.AddMinutes(unit.CorrectionTime) : MU.dtStart;
				chart.Init(unit.StartCorrected, unit);
				if (!loadfailed) pnMaps.Controls.SetChildIndex(chart, 0);
				unit.FillAutoComplete();
			}

			int i = 0, kr = 0;
			C1.Win.C1Chart.Label lab;
			double speed, maxspeed = 1;
			ChartDataSeries ChartDat;
			double speedtreshold = _lu[chart.L].LowSpeedTreshold * (TmStyle == TimeStyle.MMM ? 1 : 60);

			chart.BeginUpdate();
			chart.UseGrayscale = false;

#if DEBUG
			//if (chart.L == 0)
			//   throw new Exception("From LoadChartMap Exception");
#endif

			chart.ChartGroups[0].ChartData.SeriesList.Clear();
			chart.ChartGroups[1].ChartData.SeriesList.Clear();

			chart.ChartLabels.LabelsCollection.Clear();

			chart.Header.Style.Border.Color = SystemColors.ControlDark;
			chart.Header.Style.BackColor = Color.White;
			chart.Header.Style.ForeColor = SystemColors.ControlText;
			chart.Header.Style.Border.Thickness = 2;
			chart.Header.Style.Border.Rounding.All = 0;
			chart.Header.Style.Opaque = true;

			if (unit.DataSet.Tables["Gaps"].DefaultView.Count > 0) {
				chart.ChartArea.PlotArea.Boxed = false;
				chart.ChartArea.AxisY.AutoMajor = true;
				chart.Enabled = true;

				double[] xr;
				double[] yr;
				List<MU.GapRecord> StopRecs = GetGapList(unit.L, "Stops");
				//Debug.WriteLine(string.Format("LoadChartMap_1() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
				List<MU.GapRecord> GapRecs = GetGapList(unit.L, "Gaps");
				//Debug.WriteLine(string.Format("LoadChartMap_2() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
				List<MU.GapRecord> BrandRecs = GetGapList(unit.L, "Brands");
				//Debug.WriteLine(string.Format("LoadChartMap_3() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
				List<MU.GapRecord> ShiftRecs = GetGapList(unit.L, "Shifts");
				//Debug.WriteLine(string.Format("LoadChartMap_4() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

				#region | Colored downtimes  |

				foreach (string key in MU.Kpi.Colors.Keys) {
					// Array for Root downtimes
					xr = new double[StopRecs.Count * 4 + 8];
					yr = new double[StopRecs.Count * 4 + 8];

					i = 0;
					foreach (MU.GapRecord rec in StopRecs) {
						if (rec.RD != key)
							continue;

						kr = i * 4 + 1;
						xr[kr] = rec.dt1;
						xr[kr + 1] = rec.dt1;
						xr[kr + 2] = rec.dt2 + _oneMin;
						xr[kr + 3] = rec.dt2 + _oneMin;
						yr[kr] = 0;
						yr[kr + 1] = 100000;
						yr[kr + 2] = 100000;
						yr[kr + 3] = 0;

						i++;
					}

					if (i == 0)
						continue;

					xr[0] = 0;				// поднимаем начало
					yr[0] = yr[1];
					xr[kr + 4] = 0;			// опускаем конец
					yr[kr + 4] = yr[kr + 3];

					ChartDat = new ChartDataSeries();
					ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
					ChartDat.LineStyle.Color = MU.Kpi.Colors[key];
					ChartDat.X.CopyDataIn(xr);
					ChartDat.Y.CopyDataIn(yr);
					ChartDat.Tag = key;
					chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
				}

				#endregion | Colored downtimes  |

				#region | Black (Not Filled) |

				xr = new double[GapRecs.Count * 4 + 8];
				yr = new double[GapRecs.Count * 4 + 8];

				i = 0;
				foreach (MU.GapRecord rec in GapRecs) {
					speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;

					if (rec.RD != "tNF" || speed > speedtreshold)
						continue;
					kr = i * 4 + 1;
					xr[kr] = rec.dt1;
					xr[kr + 1] = rec.dt1;
					xr[kr + 2] = rec.dt2 + _oneMin;
					xr[kr + 3] = rec.dt2 + _oneMin;

					yr[kr] = 0;
					yr[kr + 1] = 100000;
					yr[kr + 2] = 100000;
					yr[kr + 3] = 0;
					i++;
				}

				xr[0] = 0;					// поднимаем начало
				yr[0] = yr[1];
				xr[kr + 4] = 0;			// опускаем конец
				yr[kr + 4] = yr[kr + 3];

				ChartDat = new ChartDataSeries();
				ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
				ChartDat.LineStyle.Color = MU.Kpi.Colors["tNF"];
				ChartDat.X.CopyDataIn(xr);
				ChartDat.Y.CopyDataIn(yr);
				ChartDat.Tag = "tNF";
				chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);

				#endregion | Black (Not Filled) |

				#region | Green chart (new)  |

				if (!unit.DataSet.Tables.Contains("Minutes") || _filter != "") {
					xr = new double[GapRecs.Count * 4 + 8];
					yr = new double[GapRecs.Count * 4 + 8];

					i = 0;
					foreach (MU.GapRecord rec in GapRecs) {
						kr = i * 4 + 1;
						xr[kr] = rec.dt1;
						xr[kr + 1] = rec.dt1;
						xr[kr + 2] = rec.dt2 + _oneMin;
						xr[kr + 3] = rec.dt2 + _oneMin;

						speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;
						yr[kr] = 0;
						yr[kr + 1] = speed;
						yr[kr + 2] = speed;
						yr[kr + 3] = 0;
						if (maxspeed < speed) maxspeed = speed;
						i++;
					}

					xr[0] = 0;				// поднимаем начало
					yr[0] = yr[1];
					xr[kr + 4] = 0;			// опускаем конец
					yr[kr + 4] = yr[kr + 3];

					ChartDat = new ChartDataSeries();
					ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
					ChartDat.LineStyle.Color = MU.Kpi.Colors["TR"];
					ChartDat.X.CopyDataIn(xr);
					ChartDat.Y.CopyDataIn(yr);
					ChartDat.Tag = "TR";
					chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
				} else {
					maxspeed = chart.MaxY;
					unit.LoadDataMinutes();
					BuildChartMapMinutes(chart);
				}

				#endregion | Green chart (new)  |

				#region | Nom speed & labels |

				xr = new double[BrandRecs.Count * 4 + 8];
				yr = new double[BrandRecs.Count * 4 + 8];

				i = 0; kr = 0;
				double prevspeed = 0;
				foreach (MU.GapRecord rec in BrandRecs) {
					kr = i * 4 + 1;
					xr[kr] = rec.dt1;
					xr[kr + 1] = rec.dt1;
					xr[kr + 2] = rec.dt2 + _oneMin;
					xr[kr + 3] = rec.dt2 + _oneMin;

					speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;
					yr[kr] = 0;
					yr[kr + 1] = speed;
					yr[kr + 2] = speed;
					yr[kr + 3] = 0;
					if (maxspeed < speed) maxspeed = speed;

					lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
					ch0_SetLabel(lab, "", "B", 0, rec.dt1, true);
					if (prevspeed != speed) {
						lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
						ch0_SetLabel(lab, "just a format", speed.ToString("0.#"), speed, rec.dt1, false);
						prevspeed = speed;
					}
					i++;
				}

				xr[0] = 0;				// поднимаем начало
				yr[0] = yr[1];
				xr[kr + 4] = 0;			// опускаем конец
				yr[kr + 4] = yr[kr + 3];

				ChartDat = new ChartDataSeries();
				ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
				ChartDat.LineStyle.Color = Color.DarkGreen;
				ChartDat.X.CopyDataIn(xr);
				ChartDat.Y.CopyDataIn(yr);

				chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
				ChartDat.SymbolStyle.Shape = SymbolShapeEnum.None;
				ChartDat.LineStyle.Thickness = 1;
				ChartDat.Tag = "nominal";

				#endregion | Nom speed & labels |

				#region | Shift labels       |

				foreach (MU.GapRecord rec in ShiftRecs) {
					lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
					ch0_SetLabel(lab, "just a shift", "S", 0, rec.dt1, true);
				}

				#endregion | Shift labels       |

				#region | Date labels        |

				DateTime datelabel = MU.DtStart.Date.AddDays(1);
				i = 0;
				while (datelabel < MU.DtEnd) {
					lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
					ch0_SetLabel(lab, "D_" + i.ToString(), datelabel.ToShortDateString(), maxspeed, chart.GetDouble(datelabel), true);
					datelabel = datelabel.Date.AddDays(1);
					i++;
					lab.Connected = true;
				}

				#endregion | Date labels        |

				SelectedZone sz = new SelectedZone(chart, 1, chart.BaseDateTime);
				chart.SelectedZone = sz;

				//maxspeed = maxspeed * 1.04;
				chart.ChartArea.AxisY.Max = maxspeed;
				chart.ChartArea.AxisY2.Max = maxspeed;
				chart.MaxY = maxspeed;
			} else {
				#region | there is no data   |

				// when there is no any data should to add empty
				ChartDataSeries dummy = new ChartDataSeries();
				dummy.X.CopyDataIn(new double[1]);
				dummy.Y.CopyDataIn(new double[1]);
				chart.ChartGroups[0].ChartData.SeriesList.Add(dummy);

				chart.ChartArea.PlotArea.Boxed = true;
				chart.Enabled = false;
				chart.ChartArea.Style.ForeColor = SystemColors.ControlDark;
				lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
				if (_lu[chart.L].Error != "")
					lab.Text = _lu[chart.L].Error;					// in case error
				else
					lab.Text = Res.mapNotAvalb;
				lab.AttachMethod = AttachMethodEnum.Coordinate;
				lab.Visible = true;
				lab.Style.Font = new Font("Verdana", 12, FontStyle.Bold, GraphicsUnit.Pixel);
				lab.Style.Autowrap = true;
				lab.Style.ForeColor = SystemColors.ControlDark;
				lab.AttachMethodData.Y = 30;
				lab.AttachMethodData.X = 40;
				//chart.ChartArea.AxisY.UnitMajor = chart.ChartArea.AxisY.Max;
				chart.ChartArea.AxisY.Visible = false;

				#endregion | there is no data   |
			}
			//Debug.WriteLine(string.Format("LoadChartMap_5() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			chart.EndUpdate();

			if (_state == C.State.DataLoading || !unit.IsBinded) ch0_ResetScale(chart , true);
			chart.Visible = true;

			sw.Stop();
			Debug.WriteLine(string.Format("LoadChartMap_N() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}


        public static void DecreaseLength(ref double[] arr, int delta)
        {
          /*  double[] tmp = new double[arr.Length + delta];
            Array.Copy(arr, 0, tmp, 0, arr.Length);
            arr = tmp;
            */
            double[] tmp = new double[ delta];
            Array.Copy(arr, 0, tmp, 0, tmp.Length);
            arr = tmp;

        }

        public void CreateDeviationMap(ref TimeLineChart chart ,MU unit , int id , Color  color_)  //ref double[] arr, int delta
        {

            DateTime _BaseDateTime;
            _BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
            //TimeLineChart chart = _ch_[unit.L];
            int lv_length;
            double[] xr_deviation;
            double[] yr_deviation;

            DataRow[] DR_deviation = _lu_[unit.L].DataSet.Tables["Counters_deviation"].Select();// ("id_deviation = " + id.ToString());

            //int lv_length;
            if (DR_deviation.Length > 0) lv_length = DR_deviation.Length;
            else return;
                //lv_length = 1;


            xr_deviation = new double[lv_length * 4 + 8];
            yr_deviation = new double[lv_length * 4 + 8];


             int 
            ind = 0;
            int lv_count_dev = 0;
            int i;
            i = 0;
            foreach (DataRow dr in DR_deviation)
            {

                if ((int)dr["id_deviation"] != id)
                {
                    lv_count_dev++;
                    continue;
                }
                ind = i * 4 + 1;

                xr_deviation[ind] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                xr_deviation[ind + 1] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                if (DR_deviation.Length > lv_count_dev + 1)
                {
                    xr_deviation[ind + 2] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                }
                else
                {

                    xr_deviation[ind + 2] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;

                 }

                yr_deviation[ind] = 0;
                yr_deviation[ind + 1] = 300;// 0000;
                yr_deviation[ind + 2] = 300;// 0000;
                yr_deviation[ind + 3] = 0;

                lv_count_dev++;
                i++;
                // ind = ind + 4;
            }

            //if (i == 0)
            //   continue;

            xr_deviation[0] = 0;				// поднимаем начало
            yr_deviation[0] = yr_deviation[1];
            //xr_deviation[ind + 4] = 0;			// опускаем конец
            //yr_deviation[ind + 4] = yr_deviation[ind + 3];
            ChartDataSeries ChartDat;

            ChartDat = new ChartDataSeries();
            // ChartDat.LineStyle.  Pattern = LinePatternEnum.DashDotDot;// None;// Solid;
            ChartDat.LineStyle.Color = color_;// Color.DarkRed;
            //  ChartDat.


            //ChartDat.SymbolStyle.Shape = SymbolShapeEnum.VerticalLine;
            /// ChartDat.LineStyle.Color = MU.Kpi.Colors[key];
            /// 
            ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
            ChartDat.X.CopyDataIn(xr_deviation);
            ChartDat.Y.CopyDataIn(yr_deviation);
            ChartDat.Tag = "ZZ_DEVIATION";// key;
            // chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
            ChartDat.LineStyle.Thickness = 3;
            chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
        }


        public void CreateBrandsMap(ref TimeLineChart chart, MU unit, int id, Color color_)  //ref double[] arr, int delta
        {

            C1.Win.C1Chart.Label lab;
            DateTime _BaseDateTime;
            _BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
            //TimeLineChart chart = _ch_[unit.L];
            int lv_length;
            double[] xr_deviation;
            double[] yr_deviation;

            DataRow[] DR_deviation = _lu_[unit.L].DataSet.Tables["Counters_brands"].Select();// ("id_deviation = " + id.ToString());

            //int lv_length;
            if (DR_deviation.Length > 0) lv_length = DR_deviation.Length;
            else
                lv_length = 1;


            xr_deviation = new double[lv_length * 4 + 2];
            yr_deviation = new double[lv_length * 4 + 2];


            int
           ind = 0;
            int lv_count_dev = 0;
            int i;
            i = 0;
            foreach (DataRow dr in DR_deviation)
            {

                ////if ((int)dr["id_deviation"] != id)
                ////{
                ////    lv_count_dev++;
                ////    continue;
                ////}
                ind = i * 4 + 1;

                xr_deviation[ind] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                ch0_SetLabel(lab, "", "B", 0, xr_deviation[ind], true);   
                xr_deviation[ind + 1] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;

                xr_deviation[ind + 2] = xr_deviation[ind + 1];
                xr_deviation[ind + 3] = xr_deviation[ind];
                //if (DR_deviation.Length > lv_count_dev + 1)
                //{
                //    xr_deviation[ind + 2] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                //    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                //}
                //else
                //{

                //    xr_deviation[ind + 2] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                //    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;

                //}

                yr_deviation[ind] = 0;
                yr_deviation[ind + 1] = 800;// 0000;
                yr_deviation[ind + 2] = 800;// 0000;
                yr_deviation[ind + 3] = 0;

                lv_count_dev++;
                i++;
                // ind = ind + 4;
            }

            //if (i == 0)
            //   continue;

           // xr_deviation[0] = 0;				// поднимаем начало
            //yr_deviation[0] = yr_deviation[1];

            xr_deviation[0] = xr_deviation[1];				// поднимаем начало
            yr_deviation[0] = yr_deviation[1];


            //xr_deviation[ind + 4] = 0;			// опускаем конец
            //yr_deviation[ind + 4] = yr_deviation[ind + 3];

            xr_deviation[ind + 4] = xr_deviation[ind + 3];			// опускаем конец
            yr_deviation[ind + 4] = 0;

            ChartDataSeries ChartDat;

            ChartDat = new ChartDataSeries();
            // ChartDat.LineStyle.  Pattern = LinePatternEnum.DashDotDot;// None;// Solid;
            ChartDat.LineStyle.Color = color_;// Color.DarkRed;
            //  ChartDat.


            //ChartDat.SymbolStyle.Shape = SymbolShapeEnum.VerticalLine;
            /// ChartDat.LineStyle.Color = MU.Kpi.Colors[key];
            /// 
            ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
            ChartDat.X.CopyDataIn(xr_deviation);
            ChartDat.Y.CopyDataIn(yr_deviation);
            ChartDat.Tag = "ZZ_BRANDS";// key;
            // chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
            ChartDat.LineStyle.Thickness = 1;
            chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
        }
          
        private void BuildChartMapCounter(MU unit, bool reload)
        {
            Stopwatch sw = Stopwatch.StartNew();

           // if (!_mapMode || unit == null || unit.IsEmpty())
           //     return;

            TimeLineChart chart = _ch_[unit.L];
            if (reload)
            {
               // chart.Header.Text = unit.GetLongUnitName_counters(false, "-");  //false
                chart.Header.Text = unit.GetLongUnitName_counters2(true, " ", unit.Parent_Counters);
                //DateTime dt = MU.use_timecorrection ? MU.dtStart.AddMinutes(unit.CorrectionTime) : MU.dtStart;
                chart.Init(unit.StartCorrected, unit);
               // if (!loadfailed) pnMaps.Controls.SetChildIndex(chart, 0);
                ////unit.FillAutoComplete();  avg
            }

            int i = 0, kr = 0;
            C1.Win.C1Chart.Label lab;
            double speed, maxspeed = 100;
            ChartDataSeries ChartDat;
            double speedtreshold = _lu_[chart.L].LowSpeedTreshold * (TmStyle == TimeStyle.MMM ? 1 : 60);

            chart.BeginUpdate();
            chart.UseGrayscale = false;

#if DEBUG
            //if (chart.L == 0)
            //   throw new Exception("From LoadChartMap Exception");
#endif

            chart.ChartGroups[0].ChartData.SeriesList.Clear();
            chart.ChartGroups[1].ChartData.SeriesList.Clear();
          //  chart.ChartGroups[2].ChartData.SeriesList.Clear();

            chart.ChartLabels.LabelsCollection.Clear();

            chart.Header.Style.Border.Color        = SystemColors.ControlDark;
            chart.Header.Style.BackColor           = Color.White;
            chart.Header.Style.ForeColor           = SystemColors.ControlText;
            chart.Header.Style.Border.Thickness    = 2;
            chart.Header.Style.Border.Rounding.All = 0;
            chart.Header.Style.Opaque = true;

            //if (unit.DataSet.Tables["Gaps"].DefaultView.Count > 0)
                if (unit.DataSet.Tables["Counters_value"].DefaultView.Count > 0)  //(1 == 1)
            {
            //    chart.ChartArea.PlotArea.Boxed = false;
            //    chart.ChartArea.AxisY.AutoMajor = true;
                chart.Enabled = true;

                double[] xr;
                double[] yr;

//#region |  downtimes  |

                #region | Colored downtimes  |


                // Array for Root downtimes
             //   xr = new double[1000];
              //  yr = new double[1000];
		 // Random rnd = new Random(DateTime.Now.Second * DateTime.Now.DayOfYear);
         //           	DateTime dt1 = DateTime.Now.AddDays(-260) ;
         //               DateTime dt2; 

              /*  for (int ii = 0; ii < 1; ii++)   //1000
                {
                    kr = ii;// *4 + 1;
                    dt2 = dt1.AddMinutes(ii);
                    xr[kr] = _ch_[0].GetInt(dt2) / 1440D;   ///(DateTime)
                    yr[kr] = rnd.Next(100);
                }
                    */
            //    foreach (MU.GapRecord rec in StopRecs)
                DataRow[] DR = _lu_[unit.L].DataSet.Tables["Counters_value"].Select();// Select(_filter, "dt1");
               // c1FlexGrid1.DataSource = _lu_[unit.L].DataSet.Tables["Counters_kpi"];
             int  iii = 0;

                xr = new double[DR.Length];
                yr = new double[DR.Length ];
   int lv_length_DR;
   lv_length_DR = DR.Length;
   double dr_befo     = 0;
   double dr_befo_d   = 0;
   bool lv_is_add     = false;
   int lv_count_empty = 0;
   int lv_count_foreach = 0;

               double m_MaxY = chart.MaxY;
               double m_MinY = 0;
               DateTime _BaseDateTime;
               _BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
                foreach (DataRow dr in DR)
                {
                   // xr[iii] = _ch_[unit.L].GetInt((DateTime)dr["dt"]) / 1440D;
                  //  dt.Subtract(_BaseDateTime).TotalMinutes;
                    //xr[iii] =            C.GetInt((DateTime)dr["dt"]) / 1440D;

                    if ((iii != 0) && (lv_count_foreach < lv_length_DR - 2))
                    {
                        if (dr_befo != (double)dr["value"]) //|| yr[iii] != yr[iii+1])
                        {
                           if (lv_count_empty > 0 && dr_befo_d > 0 )
                            {
                                xr[iii]  = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D; //dr_befo_d;
                                yr[iii] = dr_befo;
                                iii++;
                                lv_count_empty = 0;
                            }
                            
                            xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                            yr[iii] = (double)dr["value"];// rnd.Next(100);   
                            lv_is_add = true;

                        }
                        else
                        {
                            lv_count_empty++;
                        }
                        
                    }
                    else
                    {
                        xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                        yr[iii] = (double)dr["value"];
                        lv_is_add = true;

                    }



                    if (m_MaxY < yr[iii])
                        m_MaxY = (int)(yr[iii]);
                    if (m_MinY > yr[iii])
                        m_MinY = (int)(yr[iii]);
                    if (  lv_is_add )
                    {
                        iii++;
                        lv_is_add = false;
                    }

                    lv_count_foreach++;
                    dr_befo   = (double)dr["value"];
                    dr_befo_d = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                }

                if ((iii - 1) < lv_length_DR && lv_length_DR > 10)
                {
                    DecreaseLength(ref yr, iii - 1);
                    DecreaseLength(ref xr, iii - 1);
                }



                double[] xr_top;
                double[] yr_top;

                double[] xr_bottom;
                double[] yr_bottom;

                DataRow[] DR_lim = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select();

                int lv_length;
                if (DR_lim.Length > 0) lv_length = DR_lim.Length;
                else
                    lv_length = 1;

                xr_bottom = new double[lv_length * 2];
                yr_bottom = new double[lv_length * 2];
                xr_top = new double[lv_length* 2];
                yr_top = new double[lv_length * 2];


                int ind = 0;
                foreach (DataRow dr in DR_lim)
                {
                    xr_bottom[ind]     = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    xr_bottom[ind + 1] =  (double)((DateTime)dr["dt_to"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    yr_bottom[ind] = (float)dr["LL_value"];
                    yr_bottom[ind + 1] = (float)dr["LL_value"];

                    xr_top[ind] =      (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    xr_top[ind + 1] =  (double)((DateTime)dr["dt_to"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    yr_top[ind] =      (float)dr["HH_value"];
                    yr_top[ind + 1] =  (float)dr["HH_value"];

                    if (m_MaxY < yr_top[ind])
                        m_MaxY = (float)dr["HH_value"];
                    if (m_MinY > yr_bottom[ind])
                        m_MinY = (float)dr["LL_value"];

                    ind = ind + 2;
                }
                //xr_bottom = new double[2];
                //yr_bottom = new double[2];
                //xr_bottom[0] = xr[0];
                //xr_bottom[1] = xr[iii - 1];

                    ChartDat = new ChartDataSeries();
                   // ChartDat.LineStyle.  Pattern = LinePatternEnum.DashDotDot;// None;// Solid;
                    ChartDat.LineStyle.Color = Color.DarkRed;

                    DataRow[] DR1 = _lu_[unit.L].DataSet.Tables["Counters"].Select();
                    foreach (DataRow dr in DR1)
                    {
                        ChartDat.LineStyle.Color = Color.FromArgb((int)dr["color"]); // (Color)dr["color"];
                        chart.Header.Text = chart.Header.Text.Trim() + "," + ((string)dr["name"]).Trim() + "," + dr["ISO"];
                    }



                    ChartDat.SymbolStyle.Shape = SymbolShapeEnum.VerticalLine;
                    ChartDat.SymbolStyle.Size = 1;
                   /// ChartDat.LineStyle.Color = MU.Kpi.Colors[key];
                    ChartDat.X.CopyDataIn(xr);
                    ChartDat.Y.CopyDataIn(yr);
                    ChartDat.Tag = "ZZ" ;// key;
                   // chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
                    ChartDat.LineStyle.Thickness = 3;
                    chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

//////////////////////////////////////
                   
              //   DR = _lu_[unit.L].DataSet.Tables["Counters_value"].Select(string.Format("value > '{0}'" ,yr_top[0] ));
                xr = new double[DR.Length];
                yr = new double[DR.Length];

                double[] xr_;
                double[] yr_;
                xr_ = new double[DR.Length];
                yr_ = new double[DR.Length]; 
                    iii = 0;
                    int iiii = 0;
                    try
                    {
//лимиты верхний и нижний
                        DateTime dtime_to = new DateTime(1, 1, 1); ;
                        DataRow[] DR_lim_ = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select();
               foreach (DataRow dr in DR)
               {

                   if (dtime_to < (DateTime)dr["dt"] && (tsbShowLimitsCounter.Checked))
                   {
                   string ss = string.Format(" dt_from <= #{0:M/d/yy H:m}# and dt_to > #{0:M/d/yy H:m}# ", (DateTime)dr["dt"]);
                   DR_lim_ = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select(ss);

                   
                  
                   }
                   if (DR_lim_.Length > 0)
                   {
                       DataRow dr_ = DR_lim_[0];
                       dtime_to = (DateTime)dr_["dt_to"];

                   if ((double)dr["value"] > (float)dr_["HH_value"])  // yr_top[0]
	{
       
	                xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                    yr[iii] = (double)dr["value"];
                    iii++;	 
	}

                   if ((double)dr["value"] < (float)dr_["LL_value"] )  //yr_bottom[0])
                   {
                       xr_[iiii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                       yr_[iiii] = (double)dr["value"];
                       iiii++;
                   }
                   }
               }
                    }
                    catch (Exception)
                    {                       // throw;
                    }

                    if ( (iii - 1 ) < DR.Length && iii > 1)
                    {
                        DecreaseLength(ref xr, iii - 1);
                        DecreaseLength(ref yr, iii - 1);
                    }
                    if ((iiii - 1) < DR.Length && iiii  > 1)
                    {
                        DecreaseLength(ref xr_, iiii - 1);
                        DecreaseLength(ref yr_, iiii - 1);
                    }


                    if (iii > 0 && tsbShowLimitsCounter.Checked )
                    {
                        ChartDataSeries ChartDat_err;
                        ChartDat_err = new ChartDataSeries();
                        //  ChartDat_err.AddNewSeries();
                        ChartDat_err.SymbolStyle.Shape = SymbolShapeEnum.Tri;// VerticalLine;
                        ChartDat_err.SymbolStyle.Color = Color.Red;
                        ChartDat_err.SymbolStyle.Size = 3; //5
                        ChartDat_err.LineStyle.Pattern = LinePatternEnum.None;// Dot;// Solid;
                        ChartDat_err.X.CopyDataIn(xr);
                        ChartDat_err.Y.CopyDataIn(yr);
                        ChartDat_err.Tag = "ZZ_ERROR_TOP";// key;
                        chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat_err);
                    }

                    if (iiii > 0 && tsbShowLimitsCounter.Checked)
                    {
                        ChartDataSeries ChartDat_err_b;
                        ChartDat_err_b = new ChartDataSeries();
                        //  ChartDat_err.AddNewSeries();
                        ChartDat_err_b.SymbolStyle.Shape = SymbolShapeEnum.InvertedTri;// VerticalLine;
                        ChartDat_err_b.SymbolStyle.Color = Color.Blue;
                        ChartDat_err_b.SymbolStyle.Size = 3;  //5
                        ChartDat_err_b.LineStyle.Pattern = LinePatternEnum.None;// Dot;// Solid;
                        ChartDat_err_b.X.CopyDataIn(xr_);
                        ChartDat_err_b.Y.CopyDataIn(yr_);
                        ChartDat_err_b.Tag = "ZZ_ERROR_BUTTON";// key;
                        chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat_err_b);  
                    }

//////////////////////////////////


///--------------------------------  граничные линии
///
                     if (xr_top.Length > 0)
                     {

                         ChartDataSeries ChartDat1;
                         ChartDat1 = new ChartDataSeries();
                         //ChartDat1.LineStyle.Pattern = LinePatternEnum.DashDot;
                         ChartDat1.SymbolStyle.Shape = SymbolShapeEnum.Star;// VerticalLine;
                         ChartDat1.SymbolStyle.Color = Color.GreenYellow;
                         ChartDat1.LineStyle.Color = Color.Green; // Color.Blue;
                         ChartDat1.LineStyle.Pattern = LinePatternEnum.Dash; //LinePatternEnum.Dot;// Solid;
                         ChartDat1.X.CopyDataIn(xr_top);
                         ChartDat1.Y.CopyDataIn(yr_top);
                         ChartDat1.Tag = "ZZ_TOP";// key;
                         ChartDat1.LineStyle.Thickness = 2;// 3;
                         chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat1);
                     };
                     if (xr_bottom.Length > 0)
                     {
                         ChartDataSeries ChartDat2;
                         ChartDat2 = new ChartDataSeries();
                         //ChartDat2.LineStyle.Pattern = LinePatternEnum.DashDot;
                         ChartDat2.SymbolStyle.Shape = SymbolShapeEnum.Star;//
                         ChartDat2.SymbolStyle.Color = Color.GreenYellow;

                         ChartDat2.LineStyle.Color = Color.Green; // Color.Blue;
                         ChartDat2.LineStyle.Pattern = LinePatternEnum.Dash;
                         ChartDat2.LineStyle.Thickness = 2;// 3;
                         ChartDat2.X.CopyDataIn(xr_bottom);
                         ChartDat2.Y.CopyDataIn(yr_bottom);
                         ChartDat2.Tag = "ZZ_BOTTOM";// key;
                         //ChartDat1.Group.ShowOutline = 
                         chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat2);
                     }
               // }

                #endregion | Colored downtimes  |

                     #region | Deviation |

                     if ( 1 == 2)   //(tsbShowLimitsCounter.Checked)
                     {
                         CreateDeviationMap(ref chart, unit, 2, Color.Red);
                         CreateDeviationMap(ref chart, unit, 0, Color.Black);
                         CreateDeviationMap(ref chart, unit, 4, Color.Blue);
                         CreateDeviationMap(ref chart, unit, 3, Color.Green);
                         CreateDeviationMap(ref chart, unit, 1, Color.Yellow);
                     }



                     #endregion | Deviation |

                     #region | Brands |
                     if (1 == 1)   //
                      CreateBrandsMap(ref chart, unit, 1, Color.Blue);
                     #endregion | Brands |



                     ///*
                #region | Black (Not Filled) |
                    /*
                xr = new double[GapRecs.Count * 4 + 8];
                yr = new double[GapRecs.Count * 4 + 8];

                i = 0;
                foreach (MU.GapRecord rec in GapRecs)
                {
                    speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;

                    if (rec.RD != "tNF" || speed > speedtreshold)
                        continue;
                    kr = i * 4 + 1;
                    xr[kr] = rec.dt1;
                    xr[kr + 1] = rec.dt1;
                    xr[kr + 2] = rec.dt2 + _oneMin;
                    xr[kr + 3] = rec.dt2 + _oneMin;

                    yr[kr] = 0;
                    yr[kr + 1] = 100000;
                    yr[kr + 2] = 100000;
                    yr[kr + 3] = 0;
                    i++;
                }
               
                xr[0] = 0;					// поднимаем начало
                yr[0] = yr[1];
                xr[kr + 4] = 0;			// опускаем конец
                yr[kr + 4] = yr[kr + 3];
              
                ChartDat = new ChartDataSeries();
                ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
                ChartDat.LineStyle.Color = MU.Kpi.Colors["tNF"];
                ChartDat.X.CopyDataIn(xr);
                ChartDat.Y.CopyDataIn(yr);
                ChartDat.Tag = "tNF";
                chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
  */
                #endregion | Black (Not Filled) |

                    #region | Green chart (new)  |
                    /*
                if (!unit.DataSet.Tables.Contains("Minutes") || _filter != "")
                {
                    xr = new double[GapRecs.Count * 4 + 8];
                    yr = new double[GapRecs.Count * 4 + 8];

                    i = 0;
                    foreach (MU.GapRecord rec in GapRecs)
                    {
                        kr = i * 4 + 1;
                        xr[kr] = rec.dt1;
                        xr[kr + 1] = rec.dt1;
                        xr[kr + 2] = rec.dt2 + _oneMin;
                        xr[kr + 3] = rec.dt2 + _oneMin;

                        speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;
                        yr[kr] = 0;
                        yr[kr + 1] = speed;
                        yr[kr + 2] = speed;
                        yr[kr + 3] = 0;
                        if (maxspeed < speed) maxspeed = speed;
                        i++;
                    }

                    xr[0] = 0;				// поднимаем начало
                    yr[0] = yr[1];
                    xr[kr + 4] = 0;			// опускаем конец
                    yr[kr + 4] = yr[kr + 3];

                    ChartDat = new ChartDataSeries();
                    ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
                    ChartDat.LineStyle.Color = MU.Kpi.Colors["TR"];
                    ChartDat.X.CopyDataIn(xr);
                    ChartDat.Y.CopyDataIn(yr);
                    ChartDat.Tag = "TR";
                    chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
                }
                else
                {
                    maxspeed = chart.MaxY;
                    unit.LoadDataMinutes();
                    BuildChartMapMinutes(chart);
                }
                */
                #endregion | Green chart (new)  |

                #region | Nom speed & labels |
/*
                xr = new double[BrandRecs.Count * 4 + 8];
                yr = new double[BrandRecs.Count * 4 + 8];

                i = 0; kr = 0;
                double prevspeed = 0;
                foreach (MU.GapRecord rec in BrandRecs)
                {
                    kr = i * 4 + 1;
                    xr[kr] = rec.dt1;
                    xr[kr + 1] = rec.dt1;
                    xr[kr + 2] = rec.dt2 + _oneMin;
                    xr[kr + 3] = rec.dt2 + _oneMin;

                    speed = TmStyle == TimeStyle.MMM ? rec.speed : rec.speed * 60;
                    yr[kr] = 0;
                    yr[kr + 1] = speed;
                    yr[kr + 2] = speed;
                    yr[kr + 3] = 0;
                    if (maxspeed < speed) maxspeed = speed;

                    lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                    ch0_SetLabel(lab, "", "B", 0, rec.dt1, true);
                    if (prevspeed != speed)
                    {
                        lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                        ch0_SetLabel(lab, "just a format", speed.ToString("0.#"), speed, rec.dt1, false);
                        prevspeed = speed;
                    }
                    i++;
                }

                xr[0] = 0;				// поднимаем начало
                yr[0] = yr[1];
                xr[kr + 4] = 0;			// опускаем конец
                yr[kr + 4] = yr[kr + 3];

                ChartDat = new ChartDataSeries();
                ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
                ChartDat.LineStyle.Color = Color.DarkGreen;
                ChartDat.X.CopyDataIn(xr);
                ChartDat.Y.CopyDataIn(yr);

                chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
                ChartDat.SymbolStyle.Shape = SymbolShapeEnum.None;
                ChartDat.LineStyle.Thickness = 1;
                ChartDat.Tag = "nominal";
*/
                #endregion | Nom speed & labels |

                #region | Shift labels       |
/*
                foreach (MU.GapRecord rec in ShiftRecs)
                {
                    lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                    ch0_SetLabel(lab, "just a shift", "S", 0, rec.dt1, true);
                }

                    */
                #endregion | Shift labels       |

                #region | Date labels        |

                DateTime datelabel = MU.DtStart.Date.AddDays(1);
                i = 0;
                while (datelabel < MU.DtEnd)
                {
                    lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                    ch0_SetLabel(lab, "D_" + i.ToString(), datelabel.ToShortDateString(), maxspeed, chart.GetDouble(datelabel), true);
                    datelabel = datelabel.Date.AddDays(1);
                    i++;
                    lab.Connected = true;
                }

                #endregion | Date labels        |


        //     #endregion  |  downtimes  |
                SelectedZone sz = new SelectedZone(chart, 1, chart.BaseDateTime);
                chart.SelectedZone = sz;

                //maxspeed = maxspeed * 1.04;
               // chart.ChartArea.AxisY.Max = maxspeed;
               // chart.ChartArea.AxisY2.Max = maxspeed;
               // chart.MaxY = maxspeed;

                m_MaxY = m_MaxY * 1.1;// 1.04; 
                chart.ChartArea.AxisY.Max  = m_MaxY;
                chart.ChartArea.AxisY2.Max = m_MaxY;
                chart.MaxY = m_MaxY;
               // chart.Mi = 0;
                m_MinY = m_MinY * 0.95;
                chart.ChartArea.AxisY.Min = m_MinY;
                chart.ChartArea.AxisY2.Min = m_MinY;

            }
            else
            {
                #region | there is no data   |

                // when there is no any data should to add empty
                ChartDataSeries dummy = new ChartDataSeries();
                dummy.X.CopyDataIn(new double[1]);
                dummy.Y.CopyDataIn(new double[1]);
                chart.ChartGroups[0].ChartData.SeriesList.Add(dummy);

                chart.ChartArea.PlotArea.Boxed = true;
                chart.Enabled = false;
                chart.ChartArea.Style.ForeColor = SystemColors.ControlDark;
                lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
                if (_lu_[chart.L].Error != "")
                    lab.Text = _lu_[chart.L].Error;					// in case error
                else
                    lab.Text = Res.mapNotAvalb;
                lab.AttachMethod = AttachMethodEnum.Coordinate;
                lab.Visible = true;
                lab.Style.Font = new Font("Verdana", 12, FontStyle.Bold, GraphicsUnit.Pixel);
                lab.Style.Autowrap = true;
                lab.Style.ForeColor = SystemColors.ControlDark;
                lab.AttachMethodData.Y = 30;
                lab.AttachMethodData.X = 40;
                //chart.ChartArea.AxisY.UnitMajor = chart.ChartArea.AxisY.Max;
                chart.ChartArea.AxisY.Visible = false;

                #endregion | there is no data   |
 //*/
            }
            //Debug.WriteLine(string.Format("LoadChartMap_5() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));



            chart.EndUpdate();

            //DateTime tm1 = unit.StartCorrected.Date;
            //DateTime tm2 = unit.EndCorrected.Date;

            //if (tm2 != tm1)
            //{
                
            //}

            if (_state == C.State.DataLoading || !unit.IsBinded )
            {
                //SetLabelsVisible(chart, MU.Mins / 1440D);
                if (MU.DtStart != DateTime.Now.Date)
                  ch0_ResetScale(chart , true);
                else
                  ch0_ResetScale(chart , false);
            }
            chart.Visible = true;
            chart.Enabled = true;

            sw.Stop();
            Debug.WriteLine(string.Format("LoadChartMap_N() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
        }

		private void BuildChartMapMinutes(TimeLineChart chart)
		{
			if (!_lu[chart.L].DataSet.Tables.Contains("Minutes"))
				return;

			// Ищем зеленый график который был нарисован по гепам чтобы его удалить
			ChartDataSeries toremove = null;
			int order = 0;
			foreach (ChartDataSeries ser in chart.ChartGroups[1].ChartData.SeriesList) {
				if (ser.Tag != null && ser.Tag.ToString() == "TR") {
					toremove = ser;
					break;
				} else if (ser.Tag != null && ser.Tag.ToString() == "TRM")
					return;	// Уже есть минутный график, поэтому выходим
				order++;
			}

			Stopwatch sw = Stopwatch.StartNew();

			#region | Заполнение массива для графика |

			double[] y = new double[MU.Mins * 2 + 10];
			double[] x = new double[MU.Mins * 2 + 10];
			double m_MaxY = chart.MaxY;

			int n = 0, count = 0; //xprev = 0, yprev = 0;
			DateTime dtcurr, dtnext = new DateTime(1, 1, 1);
			foreach (DataRow dr in _lu[chart.L].DataSet.Tables["Minutes"].Rows) {
				n = count++ * 2 + 1;
				dtcurr = (DateTime)dr[0];
				x[n] = chart.GetDouble(dtcurr);
				if (n != 1 && dtcurr != dtnext) {
					// если данные отсутствуют (ридер не работал) то добавляем две новых точки
					x[n] = x[n - 1];
					y[n] = 0;
					x[n + 1] = chart.GetDouble((DateTime)dr[0]);
					y[n + 1] = 0;
					n = count++ * 2 + 1;
					x[n] = x[n - 1];
				}
				x[n + 1] = x[n] + _oneMin;
				y[n] = Convert.ToDouble(dr[1].ToString());
				if (TmStyle != TimeStyle.MMM)
					y[n] *= 60;
				y[n + 1] = y[n];
				if (m_MaxY < y[n])
					m_MaxY = (int)(y[n]);
				dtnext = dtcurr.AddMinutes(1);
			}

			y[0] = 0;
			x[0] = x[1];
			y[n + 2] = 0;
			x[n + 2] = x[n + 1];

			ChartDataSeries ChartDat;
			ChartDat = new ChartDataSeries();
			ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
			ChartDat.LineStyle.Color = MU.Kpi.Colors["TR"];
			ChartDat.Y.CopyDataIn(y);
			ChartDat.X.CopyDataIn(x);
			ChartDat.Tag = "TRM";

			#endregion | Заполнение массива для графика |

			// удаляем предыдущий вариант и вставляем новый
			if (toremove != null)
				chart.ChartGroups[1].ChartData.SeriesList.Remove(toremove);
			chart.ChartGroups[1].ChartData.SeriesList.Insert(order, ChartDat);

			if (m_MaxY > chart.MaxY) {
				//m_MaxY = m_MaxY * 1.04;
				chart.ChartArea.AxisY.Max = m_MaxY;
				chart.ChartArea.AxisY2.Max = m_MaxY;
				chart.MaxY = m_MaxY;
			}

			Cursor = Cursors.Default;
		}

		#region | Gaps normalizer     |

		private MU.StartComparer sc = new MU.StartComparer();

		private List<MU.GapRecord> GetGapList(int line, string tablename)
		{
			List<MU.GapRecord> recs = new List<MU.GapRecord>();
            if (tablename == "Brands" && (_filter == string.Empty || (_filter.Contains("Shift"))))  // !_filter.  || 
				return recs;
			if (tablename == "Shifts" && (_filter == string.Empty ||(_filter.Contains("Brand") || _filter.Contains("Format"))))
				return recs;

			DataRow[] DR = _lu[line].DataSet.Tables[tablename].Select(_filter, "dt1");
			bool thereisgp = _lu[line].DataSet.Tables[tablename].Columns.Contains("GP");
			bool thereisrd = _lu[line].DataSet.Tables[tablename].Columns.Contains("RD");
			bool isbrandtb = tablename == "Brands";
			DataRow par;
			foreach (DataRow dr in DR) {
				if (dr["dt1"] == DBNull.Value || dr["dt2"] == DBNull.Value)
					continue;
				MU.GapRecord newrec = new MU.GapRecord();
				// переводим в интегер чтобы правильно сравнивались
				newrec.dt1 = _ch[line].GetInt((DateTime)dr["dt1"]);
				newrec.dt2 = _ch[line].GetInt((DateTime)dr["dt2"]);
				if (newrec.dt1 > newrec.dt2)
					continue; // Debug.WriteLine("newrec.dt1 > newrec.dt2 !!!!!!!!!!");
				if (thereisrd) newrec.RD = dr["RD"] == DBNull.Value ? "tNF" : dr["RD"].ToString();
				if (thereisgp) newrec.speed = C.GetFloat(dr["GP"]) / (C.GetFloat(dr["TF"]));
				if (isbrandtb) {
					par = dr.GetParentRow("FormatListBrands");
					newrec.speed = C.GetFloat(par["Speed"]) / 60;
				}
				recs.Add(newrec);
			}
			recs.Sort(sc);

			if (tablename != "Gaps")
				NormalizeGaps(recs, 1);
			foreach (MU.GapRecord rec in recs) {
				rec.dt1 = rec.dt1 / 1440D;
				rec.dt2 = rec.dt2 / 1440D;
			}
			return recs;
		}

//
        private List<MU.GapRecord> GetGapListCounters(int line, string tablename)
        {
            List<MU.GapRecord> recs = new List<MU.GapRecord>();

             return recs;
            if ((tablename == "Brands" )  && _filter.Contains("Shift"))
                return recs;
            if ((tablename == "Shifts" ) && (_filter.Contains("Brand") || _filter.Contains("Format")))
                return recs;



            DataRow[] DR = _lu_[line].DataSet.Tables[tablename].Select(_filter, "dt1");
            bool thereisgp = _lu_[line].DataSet.Tables[tablename].Columns.Contains("GP");
            bool thereisrd = _lu_[line].DataSet.Tables[tablename].Columns.Contains("RD");
            bool isbrandtb = tablename == "Brands";
            DataRow par;
            foreach (DataRow dr in DR)
            {
                if (dr["dt1"] == DBNull.Value || dr["dt2"] == DBNull.Value)
                    continue;
                MU.GapRecord newrec = new MU.GapRecord();
                // переводим в интегер чтобы правильно сравнивались
                newrec.dt1 = _ch_[line].GetInt((DateTime)dr["dt1"]);
                newrec.dt2 = _ch_[line].GetInt((DateTime)dr["dt2"]);
                if (newrec.dt1 > newrec.dt2)
                    continue; // Debug.WriteLine("newrec.dt1 > newrec.dt2 !!!!!!!!!!");
                if (thereisrd) newrec.RD = dr["RD"] == DBNull.Value ? "tNF" : dr["RD"].ToString();
                if (thereisgp) newrec.speed = C.GetFloat(dr["GP"]) / (C.GetFloat(dr["TF"]));
                if (isbrandtb)
                {
                    par = dr.GetParentRow("FormatListBrands");
                    newrec.speed = C.GetFloat(par["Speed"]) / 60;
                }
                recs.Add(newrec);
            }
            recs.Sort(sc);

            if (tablename != "Gaps")
                NormalizeGaps(recs, 1);
            foreach (MU.GapRecord rec in recs)
            {
                rec.dt1 = rec.dt1 / 1440D;
                rec.dt2 = rec.dt2 / 1440D;
            }
            return recs;
        }

//
		private void NormalizeGaps(List<MU.GapRecord> recs, int position)
		{
			if (recs.Count < 2)
				return;
			//Debug.WriteLine("++++++++++++++++++++++++++");
			for (int n = position; n < recs.Count; n++) {
				if (recs[n - 1].dt1 == recs[n].dt1) {
					if (recs[n - 1].dt2 > recs[n].dt2)
						recs[n - 1].dt1 = recs[n].dt2 + 1;
					else
						recs[n].dt1 = recs[n - 1].dt2 + 1;
					int l = recs.Count - n + 1 > 20 ? 20 : recs.Count - n + 1;
					recs.Sort(n - 1, l, sc);
					//NormalizeGaps(recs, n);
					//break;
				}
				if (recs[n - 1].dt2 > recs[n].dt2) {
					////Debug.WriteLine("  --");
					////Debug.WriteLine(recs[n - 1].dt1.ToString());
					////Debug.WriteLine(recs[n - 1].dt2.ToString());
					////Debug.WriteLine(recs[n].dt1.ToString());
					////Debug.WriteLine(recs[n].dt2.ToString());
					////if (recs[n].dt1 >= recs[n].dt2)
					////   Debug.WriteLine("recs[n].dt1 >= recs[n].dt2 !!!!!!!!!!");

					MU.GapRecord newrec = new MU.GapRecord();
					newrec.dt1 = recs[n].dt2 + 1;
					newrec.dt2 = recs[n - 1].dt2;
					newrec.RD = recs[n - 1].RD;
					newrec.speed = recs[n - 1].speed;
					recs[n - 1].dt2 = recs[n].dt1 - 1;
					recs.Insert(n + 1, newrec);
					////Debug.WriteLine("  --");
					////Debug.WriteLine(recs[n - 1].dt1.ToString());
					////Debug.WriteLine(recs[n - 1].dt2.ToString());
					////Debug.WriteLine(recs[n].dt1.ToString());
					////Debug.WriteLine(recs[n].dt2.ToString());
					////Debug.WriteLine(newrec.dt1.ToString());
					////Debug.WriteLine(newrec.dt2.ToString());
					int l = recs.Count - n + 1;
					l = l > 20 ? 20 : l;		// проверял экспериментально, при значении меньше 9 возникали исключения переполнения стека, а при больших значениях занимает много времени
					recs.Sort(n - 1, l, sc);
					NormalizeGaps(recs, n);
					break;
				}
			}
		}

		#endregion | Gaps normalizer     |

		#endregion | Filling chart Map   |

		#region | Fields              |

		//private TimeZone selzone;
		//private TimeZone highzone;
		private DateTime mouse_location;
		private double mouse_y_discrete;

		DataGridViewTreeViewComboBoxColumn dgvcol_tree;
		DataGridViewListViewComboBoxColumn dgvcol_brand;
		DataGridViewListViewComboBoxColumn dgvcol_format;
		DataGridViewComboBoxColumn dgvcol_shift;
		DataGridViewComboBoxColumn dgvcol_mark1;
		DataGridViewComboBoxColumn dgvcol_mark2;

		private bool delete_in_use = false; // используется только процедуре удаления строк из грида
		private Image im_save_blue = Res.save_tiny_blue;
		private Image im_save_red = Res.save_tiny_red;

		#endregion | Fields              |

		private void SelectTimeLine(int line)
		{
			TimeLineChart chart = _ch[line];
			if (chart == _chs)
				return;
			if (_chs != null) {
				_chs.Header.Style.Border.Color = SystemColors.ControlDark;
				_chs.Header.Style.ForeColor    = SystemColors.ControlText;
				//ch0_ResetScale(_chs);

				if (_filter != string.Empty) {
					_filter = string.Empty;
					// на случай если был наложен фильтр рисуем заново
					BuildChartMap(_ch[line].Unit, false);
                  //  BuildChartMapCounter(_ch_[line].Unit, false);
				}
			}
			_chs = chart;
			_chs.Header.Style.Border.Color = Color.Blue;
			_chs.Header.Style.ForeColor = Color.Blue;
			Debug.WriteLine("SelectLine");
		}



        private void ch0_MouseMove_Counter(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            if (is_mouse_move_process )
            {
                return;
            }
            is_mouse_move_process = true;

            TimeLineChart chart = (TimeLineChart)sender;
            if (_lu_.Count < 1 || chart.ChartGroups[0].ChartData.SeriesList.Count == 0)
            {
                is_mouse_move_process = false;
                return;	// chart is empty

            }

            if (e.Button == MouseButtons.Right)
            {
                is_mouse_move_process = false;
                return;	// context menu
            }

            if (e.Button == MouseButtons.Left && chart.Cursor != Cursors.VSplit && Control.ModifierKeys != Keys.Alt)
            {
                is_mouse_move_process = false;
                return;	// scaling
            }


            if (e.Button == MouseButtons.Left &&  ( Control.ModifierKeys != Keys.Alt  ||  Control.ModifierKeys != Keys.Shift  ||  Control.ModifierKeys != Keys.Control  ))
            {
                is_mouse_move_process = false;
                return;	// scaling
            }

            // Устанавливаем фокус на график под мышкой (с целью чтобы ранее выбранный по клику не лез сам на глаза) 
         
            
          //  if (!_isEdit)
           
              chart.Focus();

            double yd = 0, xd = 0;
            chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref xd, ref yd);
            _sz = chart.SelectedZone;

            L = chart.L;
            if (IsEmpty_Counter())
            {
                is_mouse_move_process = false;
                return;
            }

            int currline = chart.L;
            //if (!_lu_[currline].IsCompleted || (_lu_[currline].Frozen && _isEdit))
            //    return;
            if (!_lu_[currline].IsCompleted)
            {
                is_mouse_move_process = false;
                return;
            }


            //if (isedit)
            //   chart.Focus();
            try
            {
#if DEBUG
                //_mouseMoveCnt++;
                //if (_mouseMoveCnt > 100) {
                //   _mouseMoveCnt = 0;
                //   Debug.WriteLine("test mouse exception");
                //   throw new Exception("test mouse exception");
                //}
#endif
                if (xd < 0) xd = -1;
                mouse_y_discrete = Math.Round(xd * 1440) / 1440;
                mouse_location = chart.GetDateTime(Math.Floor(xd * 1440) / 1440);

                if (_lu_[currline].MouseData.LastMouseLocation == mouse_location)
                {
                    is_mouse_move_process = false;
                    return;
                }

                if (!_lu_[currline].BuildMouseData_Counter(mouse_location, TmStyle == TimeStyle.MMM))
                {
                    Tt.SetToolTip();
               //     return;
                }

                // to reduce CPU workload
                Thread.Sleep(2);

                ChartRegionEnum region = chart.ChartRegionFromCoord(e.X, e.Y);

                //highzone = chart.ChartArea.PlotArea.AlarmZones.AlarmZoneAtCoord(e.X, e.Y) as TimeZone;
                //TimeZone tz = highzone ?? null;
                string strTip = string.Empty;
                if (_isEdit && e.Button == MouseButtons.None && region != ChartRegionEnum.ChartLabel)
                {
                    if (_sz.MouseNearBorder(xd) &&
                            (_sz.DownID == 0 || // либо у границы новой зоны либо у границы заполненной зоны по Alt
                            (_sz.DownID != 0 && Control.ModifierKeys == Keys.Alt)))
                    {
                        chart.Cursor = Cursors.VSplit;
                        chart.Interaction.Enabled = false;
                        is_mouse_move_process = false;
                        return;
                    }
                    else if (_sz.MouseInside(mouse_location))
                    {
                        #region | тултип к выделенной зоне |

                        //if (tabEdit.SelectedTab == tpEditDnTms &&
                        //    (_sz.DownID == 0 || // либо у границы новой зоны либо у границы заполненной зоны по Alt
                        //    (_sz.DownID != 0 && Control.ModifierKeys == Keys.Alt)))
                        //{
                        //    Tt.SetToolTip(chart, GetSelectionTip(_sz.LowerExtent, _sz.UpperExtent));
                        //    chart.Cursor = Cursors.Default;
                        //    return;
                        //}
                        //else if (tabEdit.SelectedTab == tpEditProds)
                        //{
                        //    AddString(ref strTip, _lu_[currline].MouseData.LastMouseLocation.ToString("g"));
                        //    if (_lu_[L].DataSet.Tables.Contains("Minutes"))
                        //    {
                        //        string dates = C.GetCDateFilter("dt", (DateTime)_lu[currline].MouseData.current_production["dt1"], mouse_location);
                        //        int b1 = C.GetInt(_lu_[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
                        //        dates = C.GetCDateFilter("dt", mouse_location, (DateTime)_lu[currline].MouseData.current_production["dt2"]);
                        //        int b2 = C.GetInt(_lu_[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
                        //        AddString(ref strTip, string.Format("GP: {0} + {1} = {2}", b1, b2, b1 + b2));
                        //    }
                        //    AddString(ref strTip, _lu_[currline].MouseData.ProductionInterval);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ProductionName);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ProductionGP);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ProductionNP);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ProductionVolume);
                        //    AddString(ref strTip, _lu_[currline].MouseData.DowntimeFullName);
                        //    Tt.SetToolTip(chart, strTip);
                        //    chart.Cursor = Cursors.Default;
                        //}
                        //else if (tabEdit.SelectedTab == tpEditShifts)
                        //{
                        //    AddString(ref strTip, _lu_[currline].MouseData.LastMouseLocation.ToString("g"));
                        //    AddString(ref strTip, _lu_[currline].MouseData.ShiftInterval);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ShiftName);
                        //    AddString(ref strTip, _lu_[currline].MouseData.ShiftVolume);
                        //    Tt.SetToolTip(chart, strTip);
                        //    chart.Cursor = Cursors.Default;
                        //}

                        #endregion | тултип к выделенной зоне |
                    }
                    else
                    {
                        chart.Cursor = Cursors.Default;
                        chart.Interaction.Enabled = true;
                    }
                }

                if (region == ChartRegionEnum.ChartLabel)
                {
                    #region | попали на лейбл B или S   |

                    //int lb = -1;
                    //if (chart.ChartLabels.LabelFromCoord(e.X, e.Y, ref lb))
                    //{
                    //    C1.Win.C1Chart.Label label = chart.ChartLabels[lb];
                    //    //SetToolTip(chart, label.Name);
                    //    if (label.Text == "S")
                    //    {
                    //        AddString(ref strTip, _lu_[currline].MouseData.ShiftName);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ShiftInterval);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ShiftVolume);
                    //        Tt.SetToolTip(chart, strTip);
                    //    }
                    //    else if (label.Text == "B")
                    //    {
                    //        AddString(ref strTip, _lu_[currline].MouseData.ProductionName);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ProductionInterval);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ProductionGP);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ProductionNP);
                    //        AddString(ref strTip, _lu_[currline].MouseData.ProductionVolume);
                    //        Tt.SetToolTip(chart, strTip);
                    //    }
                    //    _sz.Visible = false;
                    //    //throw new Exception("TEST Exception!!!!");
                    //}

                    #endregion | попали на лейбл B или S   |
                }

                else if (_isEdit && tabEdit.SelectedTab == tpEditShifts)
                {
                    #region | отображение смены         |

                    //_sz.FixInitialValue();
                    //if (!_sz.Visible) bsProdShift_PositionChanged(_bsShifts, EventArgs.Empty);
                    //_bsShifts.Position = _bsShifts.Find("ID", _lu_[currline].MouseData.ShiftID);
                    //chart.Update();

                    #endregion | отображение смены         |
                }
                else
                {
                    #region | рисуем default тултип     |

                    // выделяем соответствующую зону на графике
                    ChangeSelectedZone_Counter(_lu_[currline].MouseData.current_downtime);

                    ////// выделяем соответствующую зону на графике
                    ////if (isedit && bsDnTms != null && !sz.Visible)
                    ////   bsDnTms_PositionChanged(bsDnTms, EventArgs.Empty);

                    ////// выделяем соответствующую запись в гриде
                    ////if (isedit && bsDnTms != null && lu[currline].MouseData.DowntimeID != -1 && lu[currline].MouseData.DowntimeID != sz.DownID) //&& highzone != null && highzone.DowntimeOrder != 0)
                    ////   bsDnTms.Position = bsDnTms.Find("ID", lu[currline].MouseData.DowntimeID);

                    // при загрузке данных статусбар отображает процесс загрузки
                    sbpDesc.TextAlign = ContentAlignment.MiddleLeft;
                    if (_state != C.State.DataLoading)
                    {
                        sbpMins.Text  = _lu_[currline].MouseData.DowntimeInterval;
                        sbpBrand.Text = "";
                     //  if ( !String.IsNullOrEmpty(_lu_[currline].MouseData.ProductionName ))    
                     //   sbpBrand.Text = _lu_[currline].MouseData.ProductionName;

                        sbpDesc.Text = "";
                        if (( !String.IsNullOrEmpty(_lu_[currline].MouseData.DowntimeFullName ) && !String.IsNullOrEmpty(_lu_[currline].MouseData.DowntimeComment) ))
                        sbpDesc.Text =  string.Format("{0} \\ {1}", (_lu_[currline].MouseData.DowntimeFullName).Trim(), (_lu_[currline].MouseData.DowntimeComment).Trim());
                        if (!String.IsNullOrEmpty(_lu_[currline].MouseData.CurrentSpeed))  
                        sbpDesc.Text = "|" + sbpDesc.Text + _lu_[currline].MouseData.CurrentSpeed.Trim();
                        sbpDesc.Text = "|" + sbpDesc.Text + _lu_[currline].MouseData.ProductionName.Trim();
                    }

                    StringBuilder sb = new StringBuilder();
                    if (cbShowTipsCounter.Checked && yd >= 0)
                    {
                        sb.AppendLine(_lu_[currline].MouseData.LastMouseLocation.ToString("g"));
                        // sb.AppendLine((String)_lu_[currline].MouseData.GapSpeed );
                   //     if (_lu_[currline].MouseData.DowntimeInterval != string.Empty) sb.AppendLine(_lu_[currline].MouseData.DowntimeInterval);
                        if (_lu_[currline].MouseData.CurrentSpeed     != string.Empty) sb.AppendLine(_lu_[currline].MouseData.CurrentSpeed);
                      //  if (_lu_[currline].MouseData.AverageSpeed     != string.Empty) sb.AppendLine(_lu_[currline].MouseData.AverageSpeed);
                      //  if (_lu_[currline].MouseData.ProductionName   != string.Empty) sb.AppendLine(_lu_[currline].MouseData.ProductionName);
                      //  if (_lu_[currline].MouseData.DowntimeFullName != string.Empty) sb.AppendLine(_lu_[currline].MouseData.DowntimeFullName);
                      //  if (_lu_[currline].MouseData.DowntimeMark     != string.Empty) sb.AppendLine(_lu_[currline].MouseData.DowntimeMark);
                      //  if (_lu_[currline].MouseData.DowntimeComment  != string.Empty) sb.AppendLine(_lu_[currline].MouseData.DowntimeComment);
                      //  if (_lu_[currline].MouseData.ShiftName        != string.Empty) sb.Append(_lu_[currline].MouseData.ShiftName);
                        if (_lu_[currline].MouseData.ProductionName != string.Empty) sb.AppendLine(_lu_[currline].MouseData.ProductionName.Trim());
                        Tt.SetToolTip(chart, sb.ToString());
                    }
                    else
                        Tt.SetToolTip(chart, string.Empty);
                    //if (e.Button != MouseButtons.Left)
                    //   chart.Interaction.Enabled = true;

                    #endregion | рисуем default тултип     |
                }
            }
            catch (Exception ex)
            {
                is_mouse_move_process = false;
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
            is_mouse_move_process = false;
        }

		private void ch0_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			TimeLineChart chart = (TimeLineChart)sender;
			if (_lu.Count < 1 || chart.ChartGroups[0].ChartData.SeriesList.Count == 0)
				return;	// chart is empty

			if (e.Button == MouseButtons.Right)
				return;	// context menu

			if (e.Button == MouseButtons.Left && chart.Cursor != Cursors.VSplit && Control.ModifierKeys != Keys.Alt)
				return;	// scaling

			// Устанавливаем фокус на график под мышкой (с целью чтобы ранее выбранный по клику не лез сам на глаза) 
			if (!_isEdit)
				chart.Focus();

			double yd = 0, xd = 0;
			chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref xd, ref yd);
			_sz = chart.SelectedZone;

            L = chart.L; 
			if (IsEmpty())
				return;

			int currline = chart.L;
			if (!_lu[currline].IsCompleted || (_lu[currline].Frozen && _isEdit))
				return;

			//if (isedit)
			//   chart.Focus();
			try {
#if DEBUG
				//_mouseMoveCnt++;
				//if (_mouseMoveCnt > 100) {
				//   _mouseMoveCnt = 0;
				//   Debug.WriteLine("test mouse exception");
				//   throw new Exception("test mouse exception");
				//}
#endif
				if (xd < 0) xd = -1;
				mouse_y_discrete = Math.Round(xd * 1440) / 1440;
				mouse_location   = chart.GetDateTime(Math.Floor(xd * 1440) / 1440);

				if (_lu[currline].MouseData.LastMouseLocation == mouse_location)
					return;

				if (!_lu[currline].BuildMouseData(mouse_location, TmStyle == TimeStyle.MMM)) {
					Tt.SetToolTip();
					return;
				}

				// to reduce CPU workload
				Thread.Sleep(2);

				ChartRegionEnum region = chart.ChartRegionFromCoord(e.X, e.Y);

				//highzone = chart.ChartArea.PlotArea.AlarmZones.AlarmZoneAtCoord(e.X, e.Y) as TimeZone;
				//TimeZone tz = highzone ?? null;
				string strTip = string.Empty;
				if (_isEdit && e.Button == MouseButtons.None && region != ChartRegionEnum.ChartLabel) {
					if (_sz.MouseNearBorder(xd) &&
							(_sz.DownID == 0 || // либо у границы новой зоны либо у границы заполненной зоны по Alt
							(_sz.DownID != 0 && Control.ModifierKeys == Keys.Alt))) {
						chart.Cursor = Cursors.VSplit;
						chart.Interaction.Enabled = false;
						return;
					} else if (_sz.MouseInside(mouse_location)) {
						#region | тултип к выделенной зоне |

						if (tabEdit.SelectedTab == tpEditDnTms &&
							(_sz.DownID == 0 || // либо у границы новой зоны либо у границы заполненной зоны по Alt
							(_sz.DownID != 0 && Control.ModifierKeys == Keys.Alt))) {
							Tt.SetToolTip(chart, GetSelectionTip(_sz.LowerExtent, _sz.UpperExtent));
							chart.Cursor = Cursors.Default;
							return;
						} else if (tabEdit.SelectedTab == tpEditProds) {
							AddString(ref strTip, _lu[currline].MouseData.LastMouseLocation.ToString("g"));
							if (_lu[L].DataSet.Tables.Contains("Minutes")) {
								string dates = C.GetCDateFilter("dt", (DateTime)_lu[currline].MouseData.current_production["dt1"], mouse_location);
								int b1 = C.GetInt(_lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
								dates = C.GetCDateFilter("dt", mouse_location, (DateTime)_lu[currline].MouseData.current_production["dt2"]);
								int b2 = C.GetInt(_lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
								AddString(ref strTip, string.Format("GP: {0} + {1} = {2}", b1, b2, b1 + b2));
							}
							AddString(ref strTip, _lu[currline].MouseData.ProductionInterval);
							AddString(ref strTip, _lu[currline].MouseData.ProductionName);
							AddString(ref strTip, _lu[currline].MouseData.ProductionGP);
							AddString(ref strTip, _lu[currline].MouseData.ProductionNP);
							AddString(ref strTip, _lu[currline].MouseData.ProductionVolume);
							AddString(ref strTip, _lu[currline].MouseData.DowntimeFullName);
							Tt.SetToolTip(chart, strTip);
							chart.Cursor = Cursors.Default;
						} else if (tabEdit.SelectedTab == tpEditShifts) {
							AddString(ref strTip, _lu[currline].MouseData.LastMouseLocation.ToString("g"));
							AddString(ref strTip, _lu[currline].MouseData.ShiftInterval);
							AddString(ref strTip, _lu[currline].MouseData.ShiftName);
							AddString(ref strTip, _lu[currline].MouseData.ShiftVolume);
							Tt.SetToolTip(chart, strTip);
							chart.Cursor = Cursors.Default;
						}

						#endregion | тултип к выделенной зоне |
					} else {
						chart.Cursor = Cursors.Default;
						chart.Interaction.Enabled = true;
					}
				}

				if (region == ChartRegionEnum.ChartLabel) {
					#region | попали на лейбл B или S   |

					int lb = -1;
					if (chart.ChartLabels.LabelFromCoord(e.X, e.Y, ref lb)) {
						C1.Win.C1Chart.Label label = chart.ChartLabels[lb];
						//SetToolTip(chart, label.Name);
						if (label.Text == "S") {
							AddString(ref strTip, _lu[currline].MouseData.ShiftName);
							AddString(ref strTip, _lu[currline].MouseData.ShiftInterval);
							AddString(ref strTip, _lu[currline].MouseData.ShiftVolume);
							Tt.SetToolTip(chart, strTip);
						} else if (label.Text == "B") {
							AddString(ref strTip, _lu[currline].MouseData.ProductionName);
							AddString(ref strTip, _lu[currline].MouseData.ProductionInterval);
							AddString(ref strTip, _lu[currline].MouseData.ProductionGP);
							AddString(ref strTip, _lu[currline].MouseData.ProductionNP);
							AddString(ref strTip, _lu[currline].MouseData.ProductionVolume);
							Tt.SetToolTip(chart, strTip);
						}
						_sz.Visible = false;
						//throw new Exception("TEST Exception!!!!");
					}

					#endregion | попали на лейбл B или S   |
				} else if (_isEdit && e.Button == MouseButtons.Left && (Control.ModifierKeys == Keys.Alt || chart.Cursor == Cursors.VSplit)) {
					#region | border moving is active   |

					// если выделяем в произвольном месте по альту то сбрасываем принадлежность зоны к известному простою
					_sz.DragBorder(mouse_y_discrete);
					chart.Update();
					Tt.SetToolTip(chart, GetSelectionTip(_sz.LowerExtent, _sz.UpperExtent));

					#endregion | border moving is active   |
				} else if (_isEdit && tabEdit.SelectedTab == tpEditProds) {
					#region | отображение налива        |

					_sz.FixInitialValue();
					if (!_sz.Visible) bsProdShift_PositionChanged(_bsProds, EventArgs.Empty);
					_bsProds.Position = _bsProds.Find("ID", _lu[currline].MouseData.ProductionID);
					chart.Update();

					#endregion | отображение налива        |
				} else if (_isEdit && tabEdit.SelectedTab == tpEditShifts) {
					#region | отображение смены         |

					_sz.FixInitialValue();
					if (!_sz.Visible) bsProdShift_PositionChanged(_bsShifts, EventArgs.Empty);
					_bsShifts.Position = _bsShifts.Find("ID", _lu[currline].MouseData.ShiftID);
					chart.Update();

					#endregion | отображение смены         |
				} else {
					#region | рисуем default тултип     |

					// выделяем соответствующую зону на графике
					ChangeSelectedZone(_lu[currline].MouseData.current_downtime);

					////// выделяем соответствующую зону на графике
					////if (isedit && bsDnTms != null && !sz.Visible)
					////   bsDnTms_PositionChanged(bsDnTms, EventArgs.Empty);

					////// выделяем соответствующую запись в гриде
					////if (isedit && bsDnTms != null && lu[currline].MouseData.DowntimeID != -1 && lu[currline].MouseData.DowntimeID != sz.DownID) //&& highzone != null && highzone.DowntimeOrder != 0)
					////   bsDnTms.Position = bsDnTms.Find("ID", lu[currline].MouseData.DowntimeID);

					// при загрузке данных статусбар отображает процесс загрузки
					if (_state != C.State.DataLoading) {
						sbpMins.Text = _lu[currline].MouseData.DowntimeInterval;
						sbpBrand.Text = _lu[currline].MouseData.ProductionName;
						sbpDesc.Text = string.Format("{0} \\ {1}", _lu[currline].MouseData.DowntimeFullName, _lu[currline].MouseData.DowntimeComment);
					}

					StringBuilder sb = new StringBuilder();
					if (_chbShowTips.Checked && yd >= 0) {
						sb.AppendLine(_lu[currline].MouseData.LastMouseLocation.ToString("g"));
						if (_lu[currline].MouseData.DowntimeInterval != string.Empty) 
							sb.AppendLine(_lu[currline].MouseData.DowntimeInterval);
						if (_lu[currline].MouseData.CurrentSpeed != string.Empty) sb.AppendLine(_lu[currline].MouseData.CurrentSpeed);
						if (_lu[currline].MouseData.AverageSpeed != string.Empty) sb.AppendLine(_lu[currline].MouseData.AverageSpeed);
						if (_lu[currline].MouseData.ProductionName != string.Empty) sb.AppendLine(_lu[currline].MouseData.ProductionName);
						if (_lu[currline].MouseData.DowntimeFullName != string.Empty) sb.AppendLine(_lu[currline].MouseData.DowntimeFullName);
						if (_lu[currline].MouseData.DowntimeMark != string.Empty) sb.AppendLine(_lu[currline].MouseData.DowntimeMark);
						if (_lu[currline].MouseData.DowntimeComment != string.Empty) sb.AppendLine(_lu[currline].MouseData.DowntimeComment);
						if (_lu[currline].MouseData.ShiftName != string.Empty) sb.Append(_lu[currline].MouseData.ShiftName);
						Tt.SetToolTip(chart, sb.ToString());
					} else
						Tt.SetToolTip(chart, string.Empty);
					//if (e.Button != MouseButtons.Left)
					//   chart.Interaction.Enabled = true;

					#endregion | рисуем default тултип     |
				}
			} catch (Exception ex) {
#if DEBUG
				MessageBox.Show(ex.ToString());
#endif
			}
		}

		private void AddString(ref string original, string newrow)
		{
			string newline = original.Length == 0 ? "" : Environment.NewLine;
			if (newrow != "") original = string.Format("{0}{1}{2}", original, newline, newrow);
		}

		#region | Chart events        |

		private void ch0_Enter(object sender, EventArgs e)
		{
			TimeLineChart chart = sender as TimeLineChart;
			if (chart != null && chart.L >= 0 && chart.Unit != null)
				SelectTimeLine(chart.L);
		}


        private void ch0_Enter_counters(object sender, EventArgs e)
        {
            TimeLineChart chart = sender as TimeLineChart;

            if (chart != null && chart.L >= 0 && chart.Unit != null)
            {
                SelectTimeLine_counters(chart.L);
            //if (chart.Enabled == true )
             //  chart.Focus();

            }
        }

        private void SelectTimeLine_counters(int line)
        {
            TimeLineChart chart = _ch_[line];
            if (chart == _chs)
                return;

            if (_chs != null)
            {
                
                _chs.Header.Style.Border.Color = SystemColors.ControlDark;
             //   _chs.Header.Style.Border.Rounding.LeftTop = 5;
              // _chs.Header.Style.Border.Rounding.RightBottom = 5;

                _chs.Header.Style.ForeColor    = SystemColors.ControlText;
                //ch0_ResetScale(_chs);

                if (_filter != string.Empty)
                {
                    _filter = string.Empty;
                    // на случай если был наложен фильтр рисуем заново
                    //BuildChartMap(_ch[line].Unit, false);
                //    BuildChartMapCounter(_ch_[line].Unit, false);
                }
            }
            _chs = chart;
            _chs.Header.Style.Border.Color = Color.Blue;
            _chs.Header.Style.ForeColor    = Color.Blue;

            Debug.WriteLine("SelectLine_counters");
        }


		private void ch0_Transform(object sender, C1.Win.C1Chart.TransformEventArgs e)
		{
			//if (chs == null)
			//   return;
			AxisScrollBar xsb = _chs.ChartArea.AxisX.ScrollBar;
			double actual = (xsb.Max - xsb.Min) * xsb.Scale;
			//double proposed = e.MaxX - e.MinX;
			double proposed = _dMouseUpX - _dMouseDownX;

			if (actual / proposed > 100 || proposed < 0.005)
				e.Cancel = true;
			else {
				//SetLabelsVisible(L, e.MaxX - e.MinX);
				e.MinX = _dMouseDownX;
				e.MaxX = _dMouseUpX;
				SetLabelsVisible(_chs, e.MaxX - e.MinX);
				xsb.Step = (_dMouseUpX - _dMouseDownX) / 3;
			}
		}

		private void SetLabelsVisible(TimeLineChart chart, double val)
		{
			bool vis = val <= 3;
			foreach (C1.Win.C1Chart.Label l in chart.ChartLabels.LabelsCollection) {
				if (l.Name.StartsWith("D"))
					l.Visible = val <= 4;
				else
					l.Visible = vis;
			}
			if (val <= 4)
				chart.ChartArea.AxisX.AnnoFormatString = " H:mm";
			else if (val <= 8)
				chart.ChartArea.AxisX.AnnoFormatString = " dd/MM H:mm";
			else
				chart.ChartArea.AxisX.AnnoFormatString = " dd/MM";
		}

		private void ch0_MouseLeave(object sender, System.EventArgs e)
		{
            is_mouse_move_process = false;
			sbpMins.Text = string.Empty;
			sbpBrand.Text = string.Empty;
			sbpDesc.Text = string.Empty;
			Tt.SetToolTip(sender as Control, string.Empty);
			if (!_isEdit && _sz != null) _sz.Visible = false;
			//Debug.WriteLine("__!___!___ch0_MouseLeave");
		}

        private void ch0_Click_Counter(object sender, System.EventArgs e)
        {
          /*  if (use_map_click_to_select_line)
            {
                int r = _chs.L + _grLines.Rows.Fixed;
                (sender as Control).Focus();
                if (_grLines.Row != r) _grLines.Row = r;
            }
            */
            L = _chs.L;
        }

		private void ch0_Click(object sender, System.EventArgs e)
		{
			if (use_map_click_to_select_line) {
				int r = _chs.L + _grLines.Rows.Fixed;
				(sender as Control).Focus();
				if (_grLines.Row != r) _grLines.Row = r;
			}
		}

		private void ch0_DoubleClick(object sender, System.EventArgs e)
		{
			if (_state != C.State.Idle) return;
			//C1Chart chart = sender as C1Chart;

			_lu[_chs.L].LoadDataMinutes();
			BuildChartMapMinutes(_chs);

			if (!_isEdit)
				return;
			if (tabEdit.SelectedTab == tpEditDnTms && _sz.MouseInside(mouse_location) && _sz.DownID == 0) {

				DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
				if (validDate >= _sz.Stop.AddMinutes(-1)) {
					MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
									"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				} else if (_sz.Start < validDate && validDate < _sz.Stop.AddMinutes(-1))
					_sz.Start = validDate;
				
				AddDownTime(_sz.Start, _sz.Stop.AddMinutes(-1), 0, 0, 0, "");
			} else if (GetCurrentBlackZone()) {

				DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
				if (validDate >= _sz.Stop.AddMinutes(-1)) {
					MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
									"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				} else if (_sz.Start < validDate && validDate < _sz.Stop.AddMinutes(-1))
					_sz.Start = validDate;

				AddDownTime(_sz.Start, _sz.Stop.AddMinutes(-1), 0, 0, 0, "");

			}
		}


        private void ch0_MouseDown_Counter(object sender, MouseEventArgs e)
        {
            _iMouseDownX = e.X;

            #region | Нативное масштабирование неточно работает > вычисляем маcштаб сами |

            // Проолжение процедуры в ch0_MouseUp, а завершение в ch0_Transform

            C1Chart chart = (C1Chart)sender;
            double yd = 0;
            chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseDownX, ref yd);

            #endregion | Нативное масштабирование неточно работает > вычисляем маcштаб сами |

            if (!_isEdit)
                return;
            C1Chart ch = sender as C1Chart;
            if (tabEdit.SelectedTab == tpEditDnTms && Control.ModifierKeys == Keys.Alt && ch.Cursor != Cursors.VSplit)
            {
                _sz.DownID = 0;
                _sz.Kind = SelectedZone.TimeZoneKind.Downtime;
                _sz.StartDragBorder(mouse_y_discrete);
                _sz.GrowUpOnly = false;
            }
            else if (tabEdit.SelectedTab == tpEditDnTms)
            {
                //if (!sz.MouseInside(mouse_location)) {
                //   // сбрасываем ручное выделение на графике
                //   sz.Visible = false;
                //   sz.DownID = 0;
                //   // выделяем запись в гриде редактирования простоев
                if (_bsDnTms != null && _lu_[L].MouseData.DowntimeID != -1 && _sz != null && _sz.DownID != 0) //&& highzone != null && highzone.DowntimeOrder != 0)
                    _bsDnTms.Position = _bsDnTms.Find("ID", _lu_[L].MouseData.DowntimeID);
                //}
            }
            else if (tabEdit.SelectedTab == tpEditProds)
            {
                _sz.Kind = SelectedZone.TimeZoneKind.Production;
                _sz.FixInitialValue();
                _sz.GrowUpOnly = true;
            }
            else if (tabEdit.SelectedTab == tpEditShifts)
            {
                _sz.Kind = SelectedZone.TimeZoneKind.Shift;
                _sz.GrowUpOnly = true;
            }
            //Debug.WriteLine("___!!___!!___!!___!!___ch0_MouseDown");
        }

        private void ch0_MouseUp_Counter(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            #region | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

            // Переводим положение на экране в координаты графика
            double yd = 0;
            C1Chart chart = (C1Chart)sender;
            chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseUpX, ref yd);

            // Если выделяли в обратную сторону то меняем значения местами
            double buffX;
            if (_dMouseUpX < _dMouseDownX)
            {
                buffX = _dMouseUpX;
                _dMouseUpX = _dMouseDownX;
                _dMouseDownX = buffX;
            }

            // Если вылезли за пределы графика то корректируем по границе
            AxisScrollBar xsb = chart.ChartArea.AxisX.ScrollBar;
            if (xsb.Min > _dMouseDownX) _dMouseDownX = xsb.Min;
            if (xsb.Max < _dMouseUpX) _dMouseUpX = xsb.Max;

            #endregion | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

            // деление записи после перетаскивания границы мышкой
/*
            if (_ch_[L].Cursor == Cursors.VSplit && _sz.NewValueIsValid())
            {
                _ch_[L].Cursor = Cursors.Default;

                if (tabEdit.SelectedTab == tpEditProds)
                    SplitProduction(_sz.Start, _sz.Stop.AddMinutes(-1), 0, 0, false);
                else if (tabEdit.SelectedTab == tpEditShifts)
                  SplitShift(_sz.Start, _sz.Stop.AddMinutes(-1), 0, false);
                else if (tabEdit.SelectedTab == tpEditDnTms)
                {
                    if (_sz.ID == 0) return;
                    Application.DoEvents();
                    DataRow dr = C.GetRow(_lu_[L].DataSet.Tables["Stops"], "ID=" + _sz.DownID);
                    //if (dr == null) dr = C.GetRow(lu[L].DataSet.Tables["Stops"], "dt2=" + C.GetCDateString(selzone.Stop.AddMinutes(-1)));
                    if (dr == null) return;

                    // можно только увеличивать зону
                    if (_sz.Stop.Subtract(_sz.Start).TotalMinutes <= C.GetInt(dr["TF"])) return;
                    int mark1 = C.GetInt(dr["mark1"]);
                    int mark2 = C.GetInt(dr["mark2"]);
                    int downid = C.GetInt(dr["DownID"]);
                    AddDownTime(_sz.Start, _sz.Stop.AddMinutes(-1), mark1, mark2, downid, dr["Comment"].ToString());
                }
            }
                */
            else if (_sz != null && !_sz.MouseInside(mouse_location) &&
                        tabEdit.SelectedTab == tpEditDnTms &&
                        Math.Abs(_iMouseDownX - e.X) < 5)
            {
                // сбрасываем ручное выделение на графике
                _sz.Visible = false;
                _sz.DownID = 0;
                // выделяем запись в гриде редактирования простоев
                if (_bsDnTms != null && _lu_[L].MouseData.DowntimeID != -1) //&& highzone != null && highzone.DowntimeOrder != 0)
                    _bsDnTms.Position = _bsDnTms.Find("ID", _lu_[L].MouseData.DowntimeID);
            }
        }



		private void ch0_MouseDown(object sender, MouseEventArgs e)
		{
			_iMouseDownX = e.X;

			#region | Нативное масштабирование неточно работает > вычисляем маcштаб сами |

			// Проолжение процедуры в ch0_MouseUp, а завершение в ch0_Transform

			C1Chart chart = (C1Chart)sender;
			double yd = 0;
			chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseDownX, ref yd);

			#endregion | Нативное масштабирование неточно работает > вычисляем маcштаб сами |

			if (!_isEdit)
				return;
			C1Chart ch = sender as C1Chart;
			if (tabEdit.SelectedTab == tpEditDnTms && Control.ModifierKeys == Keys.Alt && ch.Cursor != Cursors.VSplit) {
				_sz.DownID = 0;
				_sz.Kind = SelectedZone.TimeZoneKind.Downtime;
				_sz.StartDragBorder(mouse_y_discrete);
				_sz.GrowUpOnly = false;
			} else if (tabEdit.SelectedTab == tpEditDnTms) {
				//if (!sz.MouseInside(mouse_location)) {
				//   // сбрасываем ручное выделение на графике
				//   sz.Visible = false;
				//   sz.DownID = 0;
				//   // выделяем запись в гриде редактирования простоев
				if (_bsDnTms != null && _lu[L].MouseData.DowntimeID != -1 && _sz != null && _sz.DownID != 0) //&& highzone != null && highzone.DowntimeOrder != 0)
					_bsDnTms.Position = _bsDnTms.Find("ID", _lu[L].MouseData.DowntimeID);
				//}
			} else if (tabEdit.SelectedTab == tpEditProds) {
				_sz.Kind = SelectedZone.TimeZoneKind.Production;
				_sz.FixInitialValue();
				_sz.GrowUpOnly = true;
			} else if (tabEdit.SelectedTab == tpEditShifts) {
				_sz.Kind = SelectedZone.TimeZoneKind.Shift;
				_sz.GrowUpOnly = true;
			}
			//Debug.WriteLine("___!!___!!___!!___!!___ch0_MouseDown");
		}

		private void ch0_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			#region | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

			// Переводим положение на экране в координаты графика
			double yd = 0;
			C1Chart chart = (C1Chart)sender;
			chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseUpX, ref yd);

			// Если выделяли в обратную сторону то меняем значения местами
			double buffX;
			if (_dMouseUpX < _dMouseDownX) {
				buffX = _dMouseUpX;
				_dMouseUpX = _dMouseDownX;
				_dMouseDownX = buffX;
			}

			// Если вылезли за пределы графика то корректируем по границе
			AxisScrollBar xsb = chart.ChartArea.AxisX.ScrollBar;
			if (xsb.Min > _dMouseDownX) _dMouseDownX = xsb.Min;
			if (xsb.Max < _dMouseUpX) _dMouseUpX = xsb.Max;

			#endregion | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

			// деление записи после перетаскивания границы мышкой
			if (_ch[L].Cursor == Cursors.VSplit && _sz.NewValueIsValid()) {
				_ch[L].Cursor = Cursors.Default;
				if (tabEdit.SelectedTab == tpEditProds)
					SplitProduction(_sz.Start, _sz.Stop.AddMinutes(-1), 0, 0, false);
				else if (tabEdit.SelectedTab == tpEditShifts)
					SplitShift(_sz.Start, _sz.Stop.AddMinutes(-1), 0, false);
				else if (tabEdit.SelectedTab == tpEditDnTms) {
					if (_sz.ID == 0) return;
					Application.DoEvents();
					DataRow dr = C.GetRow(_lu[L].DataSet.Tables["Stops"], "ID=" + _sz.DownID);
					//if (dr == null) dr = C.GetRow(lu[L].DataSet.Tables["Stops"], "dt2=" + C.GetCDateString(selzone.Stop.AddMinutes(-1)));
					if (dr == null) return;

					// можно только увеличивать зону
					if (_sz.Stop.Subtract(_sz.Start).TotalMinutes <= C.GetInt(dr["TF"])) return;
					int mark1 = C.GetInt(dr["mark1"]);
					int mark2 = C.GetInt(dr["mark2"]);
					int downid = C.GetInt(dr["DownID"]);
					AddDownTime(_sz.Start, _sz.Stop.AddMinutes(-1), mark1, mark2, downid, dr["Comment"].ToString());
				}
			} else if (_sz != null && !_sz.MouseInside(mouse_location) &&
						tabEdit.SelectedTab == tpEditDnTms &&
						Math.Abs(_iMouseDownX - e.X) < 5) {
				// сбрасываем ручное выделение на графике
				_sz.Visible = false;
				_sz.DownID = 0;
				// выделяем запись в гриде редактирования простоев
				if (_bsDnTms != null && _lu[L].MouseData.DowntimeID != -1) //&& highzone != null && highzone.DowntimeOrder != 0)
					_bsDnTms.Position = _bsDnTms.Find("ID", _lu[L].MouseData.DowntimeID);
			}
		}

		private string GetSelectionTip(Double db1, Double db2)
		{
			DateTime dt1 = _chs.GetDateTime(db1);
			DateTime dt2 = _chs.GetDateTime(db2);
			if (!_lu[L].DataSet.Tables.Contains("Minutes"))
				return "";
			//object obj = lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", C.GetCDateFilter("dt", dt1, dt2));
			//int bot = obj != null && obj != DBNull.Value ? Convert.ToInt32(obj) : 0;
			int bot = C.GetInt(_lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", C.GetCDateFilter("dt", dt1, dt2)));
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(string.Format("{0:t} - {1:t}", dt1, dt2.AddMinutes(-1)));
			sb.AppendLine(string.Format("Minutes: {0}", dt2.Subtract(dt1).TotalMinutes));
			sb.Append(string.Format("{1}: {0}", bot, Res.strGP));
			return sb.ToString();
		}

		private void ch0_ResetScale(TimeLineChart chart , bool b )
		{
        if (b)
	{
        chart.ChartArea.AxisX.ScrollBar.Scale = 1;
	}

			double minx = chart.MinX;
			double maxx = minx + MU.Mins / 1440F;
			chart.ChartArea.AxisX.SetMinMax(minx, maxx);
			chart.ChartArea.AxisX.ScrollBar.Min = minx;
			chart.ChartArea.AxisX.ScrollBar.Max = maxx;

			chart.Cursor = Cursors.Default;

			SetLabelsVisible(chart, MU.Mins / 1440D);
		}

		private void ch0_SetLabel(C1.Win.C1Chart.Label lab, string name, string txt, double valY, double valX, bool top)
		{
			lab.Name = name;
			lab.Text = txt;
			lab.AttachMethod = AttachMethodEnum.DataCoordinate;
			lab.Visible = true;
			lab.Style.BackColor = txt == "S" ? Color.LightSteelBlue :
				name.StartsWith("D_") ? Color.White : Color.FromArgb(100, Color.White);
			lab.Style.Font = name.StartsWith("D") ? _font : _fontBold;
			lab.Offset = name.StartsWith("D_") ? 1 : 2;
			lab.Compass = top && txt != "S" ? LabelCompassEnum.NorthEast : LabelCompassEnum.SouthEast;
			lab.Style.Border.BorderStyle = txt == "S" ? C1.Win.C1Chart.BorderStyleEnum.Solid : C1.Win.C1Chart.BorderStyleEnum.None;
			lab.AttachMethodData.X = valX;
			lab.AttachMethodData.Y = valY;
			lab.AttachMethodData.GroupIndex = 0;
		}

		#endregion | Chart events        |

		private void SetEditState(bool edit)
		{

			if (!_mapMode || MU.Mins > MU.maxEditMins || L < 0 || !GetCredentials(_lu[L])) {
				// если не в режиме одного дня значит выходим
				tsbEdit.Checked = false;
				edit = false;
			}
			if (_user != null) {
				DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
				if (validDate > _lu[L].EndCorrected) {
					MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
									"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					tsbEdit.Checked = false;
					edit = false;
				}
			}
			if (IsEmpty() || _lu[L].DataSet.Tables["Gaps"].Rows.Count == 0) {
				// если пустые то тоже выходим
				tsbEdit.Checked = false;
				edit = false;
			}

			Stopwatch sw = Stopwatch.StartNew();
			SetCursWait();

			if (edit) {
				if (_isEdit && pnEdit.Visible && _ch[L].Visible)
					return;

				#region | perfom layout |

				// замораживаем
				//tabEdit.SuspendLayout();
				//tsEdit.SuspendLayout();
				//pnEdit.SuspendLayout();
				pnMaps.SuspendLayout();

				// расставляем контролы
				_ch[L].Visible = true;
				chartmap_VisibleChanged(_ch[L], null);		// set control edit state
				for (int n = 0; n < _lu.Count; n++)
					if (n != L)
						_ch[n].Visible = false;					//	hide 'Timeline' charts
				pnMaps.Controls.SetChildIndex(pnEdit, 0);
				pnEdit.Visible = true;

				//размораживаем
				//tabEdit.ResumeLayout(true);
				//tsEdit.ResumeLayout(true);
				//pnEdit.ResumeLayout(true);
				pnMaps.ResumeLayout(true);
				pnMaps.PerformLayout();
				Application.DoEvents();

				#endregion | perfom layout |

				Debug.WriteLine(string.Format("SetEditState(0) Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F, edit));

				////SetEditGrids();						// RefreshLine() will arise there
				////Debug.WriteLine(string.Format("SetEditState(1) Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F, edit));

				// set references
				_chs = _ch[L];
				_sz = _chs.ChartGroups[0].ChartData.SeriesList[_chs.ChartGroups[0].ChartData.SeriesList.Count - 1] as SelectedZone;

				_lu[_chs.L].LoadDataMinutes();
				BuildChartMapMinutes(_chs);

				SetEditGrids();						// RefreshLine() will arise there
				Debug.WriteLine(string.Format("SetEditState(1) Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F, edit));

				_isEdit = true;
			} else {
				// переходим в обычный режим (скидываем редактирование)
				_isEdit = false;

				if (pnEdit.Visible)
					if (_state == C.State.Idle) {
						for (int n = 0; n < _lu.Count; n++)
							_ch[n].Visible = true;
						//int cnt = 0;
						//for (int n = 0; n < _lu.Count; n++)
						//   if (_lu[n].IsLine)
						//      cnt++;
						//for (int n = 0; n < cnt; n++)
						//   _ch[n].Visible = true;
					}

				pnEdit.Visible = false;
				if (_sz != null) _sz.Visible = false;
				ResetChartMap();
				SetChartMapHeight(_monitorSettings.BigMap);
			}

			SetCursDefault();
			sw.Stop();
			Debug.WriteLine(string.Format("SetEditState({1}) Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F, edit));
		}

		private void InitEditGrids()
		{
			_newRowStyle = new DataGridViewCellStyle(dgvDnTms.DefaultCellStyle) { ForeColor = Color.Red, SelectionForeColor = Color.DarkRed };
			_editRowStyle = new DataGridViewCellStyle(dgvDnTms.DefaultCellStyle) { ForeColor = Color.Blue, SelectionForeColor = Color.DarkBlue };
			_outdatedRowStyle = new DataGridViewCellStyle(dgvDnTms.DefaultCellStyle) { ForeColor = Color.Gray };

			// привязка событий

			#region | Downtimes events         |

			dgvDnTms.EditMode = DataGridViewEditMode.EditOnEnter;

			dgvDnTms.AutoGenerateColumns = false;
			dgvDnTms.AllowUserToDeleteRows = false;
			dgvDnTms.VirtualMode = true;
			dgvDnTms.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			dgvDnTms.BackgroundColor = _colWhite;
			dgvDnTms.GridColor = SystemColors.ControlLight;
			dgvDnTms.DefaultCellStyle.SelectionBackColor = dgvDnTms.DefaultCellStyle.BackColor;
			dgvDnTms.DefaultCellStyle.SelectionForeColor = dgvDnTms.DefaultCellStyle.ForeColor;

			dgvDnTms.CellValuePushed += new DataGridViewCellValueEventHandler(dgv_CellValuePushed);
			dgvDnTms.CellValidating += new DataGridViewCellValidatingEventHandler(dgvDnTms_CellValidating);
			dgvDnTms.CellFormatting += new DataGridViewCellFormattingEventHandler(dgv_CellFormatting);

			dgvDnTms.RowPrePaint += new DataGridViewRowPrePaintEventHandler(dgv_RowPrePaint);
			dgvDnTms.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgv_RowPostPaint);
			dgvDnTms.RowHeaderMouseDoubleClick += new DataGridViewCellMouseEventHandler(dgv_RowHeaderMouseDoubleClick);

			dgvDnTms.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvDnTms.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dgvDnTms_EditingControlShowing);
			dgvDnTms.CellValueChanged += new DataGridViewCellEventHandler(dgvDnTms_CellValueChanged);
			dgvDnTms.CellEndEdit += new DataGridViewCellEventHandler(dgvDnTms_CellEndEdit);
			dgvDnTms.CellEnter += new DataGridViewCellEventHandler(dgv_CellEnter);
			dgvDnTms.Enter += new EventHandler(dgv_Enter);

			dgvDnTms.CellBeginEdit += new DataGridViewCellCancelEventHandler(dgvDnTms_CellBeginEdit);

			#endregion | Downtimes events         |

			#region | Production events        |

			dgvProds.EditMode = DataGridViewEditMode.EditOnEnter;
			dgvProds.AutoGenerateColumns = false;
			dgvProds.AllowUserToDeleteRows = false;
			dgvProds.VirtualMode = true;
			dgvProds.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			dgvProds.BackgroundColor = _colWhite;
			dgvProds.GridColor = SystemColors.ControlLight;

			dgvProds.CellValueNeeded += new DataGridViewCellValueEventHandler(dgv_CellValueNeeded);
			dgvProds.CellValuePushed += new DataGridViewCellValueEventHandler(dgv_CellValuePushed);
			dgvProds.CellFormatting += new DataGridViewCellFormattingEventHandler(dgv_CellFormatting);
			//dgvProds.CellValidating += new DataGridViewCellValidatingEventHandler(dgvDnTms_CellValidating);
			dgvProds.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvProds.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgv_RowPostPaint);
			dgvProds.RowHeaderMouseDoubleClick += new DataGridViewCellMouseEventHandler(dgv_RowHeaderMouseDoubleClick);
			dgvProds.CellEnter += new DataGridViewCellEventHandler(dgv_CellEnter);
			dgvProds.Enter += new EventHandler(dgv_Enter);

			#endregion | Production events        |

			#region | Shifts events            |

			dgvShifts.EditMode = DataGridViewEditMode.EditOnEnter;
			dgvShifts.AutoGenerateColumns = false;
			dgvShifts.AllowUserToDeleteRows = false;
			dgvShifts.VirtualMode = true;
			//dgvShift.RowTemplate.Height = 18;
			dgvShifts.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
			dgvShifts.BackgroundColor = _colWhite;
			dgvShifts.GridColor = SystemColors.ControlLight;

			dgvShifts.CellValuePushed += new DataGridViewCellValueEventHandler(dgv_CellValuePushed);
			dgvShifts.CellFormatting += new DataGridViewCellFormattingEventHandler(dgv_CellFormatting);
			//dgvShifts.CellValidating += new DataGridViewCellValidatingEventHandler(dgv_CellValidating);
			//dgvShifts.RowValidated += new DataGridViewCellEventHandler(dgvShifts_RowValidated);
			dgvShifts.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgv_RowPostPaint);
			dgvShifts.RowHeaderMouseDoubleClick += new DataGridViewCellMouseEventHandler(dgv_RowHeaderMouseDoubleClick);
			dgvShifts.CellEnter += new DataGridViewCellEventHandler(dgv_CellEnter);
			dgvShifts.Enter += new EventHandler(dgv_Enter);

			#endregion | Shifts events            |
		}

		private void SetEditGrids()
		{
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			DataGridViewTextBoxColumn c;

			#region | Downtimes       |

			if (_bsDnTms != null) _bsDnTms.Dispose();
			_bsDnTms = new BindingSource(_lu[L].DataSet, "Stops");
			_bsDnTms.Sort = "dt1";
			_bsDnTms.Filter = "dt1 IS NOT NULL"; //"TF>0 AND ID<>0";
			//bsDnTms.RaiseListChangedEvents = false;
			_bsDnTms.PositionChanged += new EventHandler(bsDnTms_PositionChanged);

			// сначала надо очистить, а потом назначать датасорс, иначе возникает ошибка в событиях грида
			dgvDnTms.Columns.Clear();
			dgvDnTms.DataSource = _bsDnTms;
			dgvDnTms.SuspendLayout();

			#region | DownTime columns   |

			//if (dgvDnTms.Columns.Count == 0) {
			dgvDnTms.Columns.Add(Utils.AddColumnToGrid("dt1", Res.strFrom, false, 100, false, ""));
			dgvDnTms.Columns.Add(Utils.AddColumnToGrid("dt2", Res.strTo, false, 100, false, ""));
			dgvDnTms.Columns.Add(Utils.AddColumnToGrid("TF", "Minutes", false, 40, true, ""));

			#region | simple column case |

			//col = new DataGridViewComboBoxColumn();
			////col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			//col.DataPropertyName = "DownID";
			//col.DataSource = lu[L].DataSet.Tables["StopList"];
			//col.ValueMember = "DownID";
			//col.DisplayMember = "FullName";
			//col.HeaderText = "Detail";
			//col.Name = "DownID";
			//col.ReadOnly = true;
			//col.Width = 150;
			//col.MaxDropDownItems = 15;
			//col.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			//dgvDowntimes.Columns.Add(col);

			#endregion | simple column case |

			if (dgvcol_tree != null) dgvcol_tree.Dispose();
			dgvcol_tree = new DataGridViewTreeViewComboBoxColumn();
			//lu[L].DataSet.Tables["StopList"].DefaultView.Sort = "KpiID DESC";
			dgvcol_tree.DataSource = _lu[L].DataSet.Tables["StopList"];

			dgvcol_tree.DataPropertyName = "DownID";
			dgvcol_tree.ValueMember = "DownID";
			dgvcol_tree.DisplayMember = "FullName";
			dgvcol_tree.HeaderText = Res.strCause;
			dgvcol_tree.Name = "DownID";
			dgvcol_tree.ReadOnly = false;
			dgvcol_tree.Width = 200;
			dgvcol_tree.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			dgvcol_tree.TreeDisplayMember = "Name";
			dgvcol_tree.TreeParentMember = "TopID";
			dgvcol_tree.TreeMember = "DownID";
			dgvcol_tree.TreeTagMember = "KpiID";
			dgvDnTms.Columns.Add(dgvcol_tree);
			dgvcol_tree.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			dgvcol_tree.TreeImageList = MU.Kpi.ImageList;
			//DataGridViewTreeViewComboBoxCell treecell = dgvcol_tree.CellTemplate as DataGridViewTreeViewComboBoxCell;
			//treecell.FillTree();

			if (MU.marks.ContainsKey("Mark1")) {
				dgvcol_mark1 = new DataGridViewComboBoxColumnR();
				dgvcol_mark1.DataSource = new DataView(_lu[L].DataSet.Tables["MarkList"], "BandID=1", "", DataViewRowState.OriginalRows);

				dgvcol_mark1.DisplayStyleForCurrentCellOnly = true;
				dgvcol_mark1.DataPropertyName = "Mark1";
				dgvcol_mark1.ValueMember = "val";
				dgvcol_mark1.DisplayMember = "Name";
				//dgvcol_mark1.HeaderText = "Кто устранил";
				dgvcol_mark1.HeaderText = Utils.GetRowField(_lu[L].DataSet.Tables["MarkBandList"], "ID=1", "", "Name");
				dgvcol_mark1.Name = "Mark1";
				dgvcol_mark1.ReadOnly = false;
				dgvcol_mark1.Width = 70;
				dgvcol_mark1.MaxDropDownItems = 10;
				dgvcol_mark1.DropDownWidth = 100;
				dgvcol_mark1.DefaultCellStyle.DataSourceNullValue = DBNull.Value;
				dgvDnTms.Columns.Add(dgvcol_mark1);
			}

			if (MU.marks.ContainsKey("Mark2")) {
				dgvcol_mark2 = new DataGridViewComboBoxColumnR();
				dgvcol_mark2.DataSource = new DataView(_lu[L].DataSet.Tables["MarkList"], "BandID=2", "", DataViewRowState.OriginalRows);

				dgvcol_mark2.DisplayStyleForCurrentCellOnly = true;
				dgvcol_mark2.DataPropertyName = "Mark2";
				//dgvcol_mark1.DataSource = lu[L].DataSet.Tables["MarkList"];
				dgvcol_mark2.ValueMember = "val";
				dgvcol_mark2.DisplayMember = "Name";
				//dgvcol_mark2.HeaderText = "Что сделано";
				dgvcol_mark2.HeaderText = Utils.GetRowField(_lu[L].DataSet.Tables["MarkBandList"], "ID=2", "", "Name");
				dgvcol_mark2.Name = "Mark2";
				dgvcol_mark2.ReadOnly = false;
				dgvcol_mark2.Width = 70;
				dgvcol_mark2.MaxDropDownItems = 10;
				dgvcol_mark2.DropDownWidth = 100;
				dgvDnTms.Columns.Add(dgvcol_mark2);
			}
			int wdth = dgvDnTms.Width - dgvDnTms.Columns["dt1"].Width * 2 - dgvDnTms.Columns["TF"].Width -
				dgvDnTms.Columns["DownID"].Width - dgvDnTms.RowHeadersWidth - 6 - SystemInformation.VerticalScrollBarWidth;

			if (MU.marks.ContainsKey("Mark1")) wdth -= dgvDnTms.Columns["Mark1"].Width;
			if (MU.marks.ContainsKey("Mark2")) wdth -= dgvDnTms.Columns["Mark2"].Width;

			DataGridViewTextBoxColumn tb = Utils.AddColumnToGrid("Comment", Res.strComment, false, 600, false, "");
			dgvDnTms.Columns.Add(tb);
			//bsDnTms.RaiseListChangedEvents = true;

			//dgvDnTms.Columns["Comment"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
			//scAutoComplete.AddRange(new string[] { "Выходной день", "Выходная ночть", "ghi" });
			//}

			#endregion | DownTime columns   |

			dgvDnTms.ResumeLayout();

			#endregion | Downtimes       |

			#region | Production      |

			if (_bsProds != null) _bsProds.Dispose();
			_bsProds = new BindingSource(_lu[L].DataSet, "Brands");
			_bsProds.Sort = "dt1";
			_bsProds.Filter = "dt1 IS NOT NULL";
			_bsProds.Position = -1;
			_bsProds.PositionChanged += new EventHandler(bsProdShift_PositionChanged);

			dgvProds.Columns.Clear();
			//todo: возникает ошибка!!! в dgv_CellValueNeeded() или dgv_CellValidating()
			dgvProds.DataSource = _bsProds;
			//dgvcol_brand.DataSource = lu[L].DataSet.Tables["BrandName"];
			//dgvcol_format.DataSource = lu[L].DataSet.Tables["FormatName"];

			#region | Production columns |

			////dgvProds.Columns.Clear();
			//if (dgvProds.Columns.Count == 0) {
			//// add ID column for debug mode only
			//dgvProd.Columns.Add(AddColumnToGrid("ID", "ID", false, 110, false));

			dgvProds.Columns.Add(Utils.AddColumnToGrid("dt1", Res.strFrom, false, 110, false, "g"));
			dgvProds.Columns.Add(Utils.AddColumnToGrid("dt2", Res.strTo, false, 110, false, "g"));
			dgvProds.Columns.Add(Utils.AddColumnToGrid("TF", Res.strMins, false, 50, true, ""));
			dgvProds.Columns.Add(Utils.AddColumnToGrid("GP", "GP", false, 70, true, ""));
			c = Utils.AddColumnToGrid("", "NP", false, 70, true, "");
			c.Name = "NP";
			dgvProds.Columns.Add(c);

			#region | simple combobox version |

			////colcomb = new DataGridViewComboBoxColumn();
			//////col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			////colcomb.DataPropertyName = "Brand";
			////colcomb.DataSource = lu[L].DataSet.Tables["BrandName"];
			////colcomb.ValueMember = "Id";
			////colcomb.DisplayMember = "EName";
			////colcomb.HeaderText = "Brand";
			////colcomb.Name = "Brand";
			////colcomb.ReadOnly = false;
			////colcomb.Width = 200;
			////colcomb.MaxDropDownItems = 15;
			////colcomb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			////dgvProd.Columns.Add(colcomb);

			////colcomb = new DataGridViewComboBoxColumn();
			////colcomb.DataPropertyName = "Format";
			////colcomb.DataSource = lu[L].DataSet.Tables["FormatName"];
			////colcomb.ValueMember = "Id";
			////colcomb.DisplayMember = "EName";
			////colcomb.HeaderText = "Format";
			////colcomb.Name = "Format";
			////colcomb.ReadOnly = false;
			////colcomb.Width = 200;
			////colcomb.MaxDropDownItems = 15;
			////colcomb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			////dgvProd.Columns.Add(colcomb);

			#endregion | simple combobox version |

			dgvcol_brand = new DataGridViewListViewComboBoxColumn();
			_lu[L].DataSet.Tables["BrandName"].DefaultView.RowFilter = "rvalid=1 OR chl>0";
			dgvcol_brand.DataSource = _lu[L].DataSet.Tables["BrandName"].DefaultView;

			dgvcol_brand.DataPropertyName = "Brand";
			//dgvcol_brand.DataSource = lu[L].DataSet.Tables["BrandName"];
			dgvcol_brand.ValueMember = "Id";
			dgvcol_brand.DisplayMember = "Name";
			dgvcol_brand.HeaderText = Res.strBrands;
			dgvcol_brand.Name = "Brand";
			dgvcol_brand.ReadOnly = false;
			dgvcol_brand.Width = 200;
			dgvcol_brand.MaxDropDownItems = 15;
			dgvcol_brand.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			dgvcol_brand.ColumnsMember.Add("Name", Res.strBrands);
			dgvProds.Columns.Add(dgvcol_brand);

			dgvcol_format = new DataGridViewListViewComboBoxColumn();
			// фильтр нужен для того чтобы убрать из списка пукты которые есть в списке но их valid равен false
			_lu[L].DataSet.Tables["FormatName"].DefaultView.RowFilter = "rvalid=1 OR chl>0";
			dgvcol_format.DataSource = _lu[L].DataSet.Tables["FormatName"].DefaultView;

			dgvcol_format.DataPropertyName = "Format";
			//dgvcol_format.DataSource = lu[L].DataSet.Tables["FormatName"];
			dgvcol_format.ValueMember = "Id";
			dgvcol_format.DisplayMember = "Name";
			dgvcol_format.HeaderText = Res.strFormats;
			dgvcol_format.Name = "Format";
			dgvcol_format.ReadOnly = false;
			dgvcol_format.Width = 200;
			dgvcol_format.MaxDropDownItems = 15;
			dgvcol_format.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
			dgvcol_format.ColumnsMember.Add("Name", Res.strFormats);
			dgvcol_format.ColumnsMember.Add("Volume", Res.strMeasure);
			dgvcol_format.ColumnsMember.Add("SpeedM", "Speed");
			dgvcol_format.ColumnsMember.Add("PerCase", "Units in pack");
			//collist.ColumnsMember = new string[] { "EName", "Volume", "SpeedM", "PerCase" };
			//collist.ColumnsCaptions = new string[] { "Format", "Volume", "Speed", "Units in pack" };
			dgvProds.Columns.Add(dgvcol_format);
			//}

			#endregion | Production columns |

			#endregion | Production      |

			#region | Shifts          |

			if (_bsShifts != null) _bsShifts.Dispose();
			_bsShifts = new BindingSource(_lu[L].DataSet, "Shifts");
			_bsShifts.Sort = "dt1";
			_bsShifts.Filter = "dt1 IS NOT NULL"; // "TF>0 AND ID<>0";
			_bsShifts.PositionChanged += new EventHandler(bsProdShift_PositionChanged);

			dgvShifts.Columns.Clear();
			dgvShifts.DataSource = _bsShifts;
			//dgvcol_shift.DataSource = lu[L].DataSet.Tables["ShiftName"];

			#region | Shifts columns     |

			////dgvShifts.Columns.Clear();
			//dgvShifts.EditMode = DataGridViewEditMode.EditOnEnter;
			//if (dgvShifts.Columns.Count == 0) {
			dgvShifts.Columns.Add(Utils.AddColumnToGrid("dt1", Res.strFrom, false, 110, false, "g"));
			dgvShifts.Columns.Add(Utils.AddColumnToGrid("dt2", Res.strTo, false, 110, false, "g"));
			dgvShifts.Columns.Add(Utils.AddColumnToGrid("TF", Res.strMins, false, 50, true, ""));

			dgvcol_shift = new DataGridViewComboBoxColumnR();
			_lu[L].DataSet.Tables["ShiftName"].DefaultView.RowFilter = "rvalid=1 OR chl>0";

			dgvcol_shift.DataSource = _lu[L].DataSet.Tables["ShiftName"].DefaultView;
			//dgvcol_shift.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			dgvcol_shift.DataPropertyName = "Shift";
			//dgvcol_shift.DataSource = lu[L].DataSet.Tables["ShiftName"];
			dgvcol_shift.ValueMember = "Id";
			dgvcol_shift.DisplayMember = "Name";
			dgvcol_shift.HeaderText = Res.strShifts;
			dgvcol_shift.Name = "Shift";
			dgvcol_shift.ReadOnly = false;
			dgvcol_shift.Width = 200;
			dgvcol_shift.MaxDropDownItems = 15;
			dgvShifts.Columns.Add(dgvcol_shift);

			dgvcol_shift.AutoComplete = true;
			//}

			#endregion | Shifts columns     |

			#endregion | Shifts          |

			if (_lu[L].UndoRedo == null || _lu[L].UndoRedo.Empty) {
				_lu[L].UndoRedo = new UndoRedo(_lu[L].DataSet);
				_lu[L].UndoRedo.OnStackChanged += new EventHandler(UndoRedo_OnStackChanged);
				Warm();
			}
			SetUndoRedoButtons();
			ResizeDowntimesGrid();

#if DEBUG
			Debug.WriteLine("EditGridsCustomization(): " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private void RefreshLine()
		{
			// is used only in edit mode
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif

			_lu[L].CalcTotalRow(_filter);
			//Debug.WriteLine("RefreshLine_0: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			BuildAnalysis();
			//Debug.WriteLine("RefreshLine_1: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			BuildChartMap(_lu[L], false);
            //BuildChartMapCounter(_lu_[L], false);

#if DEBUG
			Debug.WriteLine("RefreshLine_N: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		#region | Buttons             |

		private void btEdit_Click(object sender, EventArgs e)
		{
			SetEditState(tsbEdit.Checked);
		}

		private void btUndoAll_Click(object sender, EventArgs e)
		{
			ChangeState(UndoRedo.Action.UndoAll);
		}

		private void btUndo_Click(object sender, EventArgs e)
		{
			ChangeState(UndoRedo.Action.Undo);
		}

		private void btRedo_Click(object sender, EventArgs e)
		{
			ChangeState(UndoRedo.Action.Redo);
		}

		private void btSave_Click(object sender, EventArgs e)
		{
			// если юзер находится в гриде и строка в режиме редактирования то надо завершить эту строку
			this.Validate();

			// сохраняем
			Save();

			// перезагружаем вручную поминутные данные поскольку в автомате (по событию из UndoRedo) не обновится.
			BuildChartMap(_lu[L], false);
            //BuildChartMapCounter(_lu_[L], false);
		}

		private void btDelete_Click(object sender, EventArgs e)
		{
			// если юзер находится в гриде и строка в режиме редактирования то надо завершить эту строку
			this.Validate();

			//// сохраняем
			if (tabEdit.SelectedTab == tpEditDnTms)
				DeleteEntry(_bsDnTms);
			if (tabEdit.SelectedTab == tpEditProds)
				DeleteEntry(_bsProds);
			if (tabEdit.SelectedTab == tpEditShifts)
				DeleteEntry(_bsShifts);

			// перезагружаем вручную поминутные данные поскольку в автомате (по событию из UndoRedo) не обновится.
			BuildChartMap(_lu[L], false);
           // BuildChartMapCounter(_lu_[L], false);
		}

		#endregion | Buttons             |

		private void Save()
		{
			if (!_isEdit)
				return;
			if (CheckForNullsBeforeToSave())
				return;

			// Обязательно вызвать, в противном случае будет возникать куча событий RowChanged
			Freeze();
			SetCursWait();

			try {
				_lu[L].Save();
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}

			_lu[L].UndoRedo.ClearStack();

			SetCursDefault();
		}

		private void edit_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			SetCursWait();

			Debug.WriteLine("frmMonitor_RowChanged!!!!!!!!!!!!!!");

			CheckRowForNulls(e.Row);
			_lu[L].UndoRedo.CommitQuiet();
			// из CommitQuiet обратного вызова ur_OnStackChanged не будет, поэтому вызываем вручную
			UndoRedo_OnStackChanged("foo", EventArgs.Empty);

			SetCursDefault();
		}

		private void ChangeState(UndoRedo.Action act)
		{
			SetCursWait();

			Freeze();
			_lu[L].UndoRedo.ChangeState(act);

			SetCursDefault();
		}

		private void Freeze()
		{
			// если уже заморожен то выходим
			if (_lu[L].Frozen) return;

			_bsDnTms.RaiseListChangedEvents = false;
			_bsProds.RaiseListChangedEvents = false;
			_bsShifts.RaiseListChangedEvents = false;

			((System.ComponentModel.ISupportInitialize)(this.dgvDnTms)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvShifts)).BeginInit();

			_lu[L].DataSet.Tables["Stops"].RowChanged -= new DataRowChangeEventHandler(edit_RowChanged);
			_lu[L].DataSet.Tables["Brands"].RowChanged -= new DataRowChangeEventHandler(edit_RowChanged);
			_lu[L].DataSet.Tables["Shifts"].RowChanged -= new DataRowChangeEventHandler(edit_RowChanged);

			_lu[L].Frozen = true;
			Debug.WriteLine("__F__R__E__E__Z__Z__Z__Z__Z__E__");
		}

		private void Warm()
		{
			// если уже нагретый то выходим
			if (!_lu[L].Frozen) return;

			_lu[L].DataSet.Tables["Stops"].RowChanged += new DataRowChangeEventHandler(edit_RowChanged);
			_lu[L].DataSet.Tables["Brands"].RowChanged += new DataRowChangeEventHandler(edit_RowChanged);
			_lu[L].DataSet.Tables["Shifts"].RowChanged += new DataRowChangeEventHandler(edit_RowChanged);

			_bsDnTms.RaiseListChangedEvents = true;
			_bsProds.RaiseListChangedEvents = true;
			_bsShifts.RaiseListChangedEvents = true;

			_bsDnTms.ResetBindings(false);
			_bsProds.ResetBindings(false);
			_bsShifts.ResetBindings(false);

			((System.ComponentModel.ISupportInitialize)(this.dgvDnTms)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvProds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvShifts)).EndInit();

			_lu[L].Frozen = false;
			Debug.WriteLine("__W__A__R__M__M__M__M__M__M__");
		}

		private void UndoRedo_OnStackChanged(object sender, EventArgs e)
		{
			//SetCursWait();
			Stopwatch sw = Stopwatch.StartNew();

			// реагируем только в случае если это реальное событие из UndoRedo, а не ручной вызов
			if (sender != null && sender.GetType() == typeof(UndoRedo))
				Warm();

			RefreshLine();
			SetUndoRedoButtons();

			Debug.WriteLine(string.Format("ur_OnStackChanged() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
			//SetCursDefault();
		}

		private void SetUndoRedoButtons()
		{
			tsbEditUndoAll.Enabled = _lu[L].UndoRedo.ThereIsUndo;
			tsbEditUndo.Enabled = _lu[L].UndoRedo.ThereIsUndo;
			tsbEditRedo.Enabled = _lu[L].UndoRedo.ThereIsRedo;
			tsbEditSave.Enabled = _lu[L].UndoRedo.ThereIsUndo;
			if (tabEdit.SelectedTab == tpEditDnTms)
				tsbEditDelete.Enabled = _bsDnTms.Count > 0;
			if (tabEdit.SelectedTab == tpEditProds)
				tsbEditDelete.Enabled = _bsProds.Count > 1;
			if (tabEdit.SelectedTab == tpEditShifts)
				tsbEditDelete.Enabled = _bsShifts.Count > 1;
		}

		#region | Edit grid events    |

		private void dgv_Enter(object sender, EventArgs e)
		{
			// Иногда по непонятным причинам курсор остается в состоянии ожидания (часики)
			// поэтому на всякий случай сбрасываем в нормальное состояние
			DataGridView dgv = sender as DataGridView;
			dgv.Cursor = Cursors.Default;

			//Debug.WriteLine("_________dgv_Enter_________");
		}

		private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (e.ColumnIndex < 0) return;
			DataGridView dgv = sender as DataGridView;
			//if (!dgv.Columns[e.ColumnIndex].Name.Equals("TF")) 
			//   return;
			if (dgv[e.ColumnIndex, e.RowIndex].Value == null || L < 0) 
				return;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			// отрисовка цветного прямоугольника в колонке TF (выбрано минут)
			if (drv != null && dgv == dgvDnTms && dgv.Columns[e.ColumnIndex].Name.Equals("TF")) {
				string rootdown = drv["RD"] == DBNull.Value ? "tNF" : drv["RD"].ToString();
				e.CellStyle = MU.Kpi.DataGridViewCellStyle[rootdown];
			} else if (dgv == dgvDnTms) {
				DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
				if ((DateTime)drv["dt1"] < validDate) {
					e.CellStyle = _outdatedRowStyle;
				}
			}
		}

		private void dgv_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;

			if (e.ColumnIndex < 0 || e.RowIndex < 0 || dgv[0, e.RowIndex].Value == null)
				return;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
			string colnane = dgv.Columns[e.ColumnIndex].Name;

			if (drv != null) {
				if (colnane == "NP") {
					//if (drv["GP"] != null) {
					//   int gp = Convert.ToInt32(drv["GP"]);
					//   float factor = Convert.ToSingle(drv["FactorNP"]);
					//   e.Value = Convert.ToInt32(gp * factor);
					//}
					int gp;
					if (int.TryParse(drv["GP"].ToString(), out gp)) {
						float factor = Convert.ToSingle(drv["FactorNP"]);
						if (Single.IsInfinity(factor) || Single.IsNaN(factor)) factor = 1;
						e.Value = Convert.ToInt32(gp * factor);
					}
				}
			}
		}

		private void dgv_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{
			if (dgvProds.Columns[e.ColumnIndex].Name == "NP") {
				DataRowView dr = dgvProds.Rows[e.RowIndex].DataBoundItem as DataRowView;
				if (e.Value == null) e.Value = 0;
				int val;
				float gp = Convert.ToSingle(dr["GP"]);

				int.TryParse(e.Value.ToString(), out val);
				//if (val == 0 || Single.IsInfinity(val))
				//   e.Value = 1;
				if (val == 0 || Single.IsInfinity(val) || gp == 0)
					dr["FactorNP"] = 1;
				else
					dr["FactorNP"] = val / Convert.ToSingle(dr["GP"]);
			}
		}

		private void dgv_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			////// as it was in original sample:
			////e.PaintParts = DataGridViewPaintParts.ContentForeground;
			////Rectangle borderRect = new Rectangle(e.RowBounds.X, e.RowBounds.Y, e.RowBounds.Width - 1, e.RowBounds.Height - 1);
			////if ((e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected) {
			////   using (LinearGradientBrush lgb = new LinearGradientBrush(e.RowBounds,
			////      ProfessionalColors.ToolStripGradientMiddle, ProfessionalColors.ToolStripContentPanelGradientBegin, 90F)) {
			////      e.Graphics.FillRectangle(lgb, borderRect);
			////      using (Pen p = new Pen(ProfessionalColors.SeparatorDark)) {
			////         e.Graphics.DrawRectangle(p, borderRect);
			////      }
			////   }
			////} else {
			////   using (LinearGradientBrush lgb = new LinearGradientBrush(e.RowBounds,
			////      ProfessionalColors.ToolStripContentPanelGradientEnd, ProfessionalColors.ToolStripContentPanelGradientBegin, 0F)) {
			////      e.Graphics.FillRectangle(lgb, e.RowBounds);
			////      using (Pen p = new Pen(ProfessionalColors.ButtonPressedBorder)) {
			////         e.Graphics.DrawRectangle(p, borderRect);
			////      }
			////   }
			////}
		}

		private void dgv_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			Rectangle r = e.RowBounds;
			DataGridView dgv = sender as DataGridView;
			//if (dgv == null || e.RowIndex == 0)
			//   return;
			if (dgv[0, e.RowIndex].Value == null) return;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			switch (drv.Row.RowState) {
				case DataRowState.Added:
					// добавляем красную иконку 'Save' в селекторе строке
					e.Graphics.DrawImage(im_save_red, r.Left + 15, r.Top);
					break;
				case DataRowState.Modified:
					// добавляем синюю иконку 'Save' в селекторе строке
					e.Graphics.DrawImage(im_save_blue, r.Left + 15, r.Top);
					break;
				default:
					// for row numbers in row selector:
					string strRowNumber = (e.RowIndex + 1).ToString();
					while (strRowNumber.Length < dgvDnTms.RowCount.ToString().Length) strRowNumber = "0" + strRowNumber;
					SizeF size = e.Graphics.MeasureString(strRowNumber, this.Font);
					if (dgvDnTms.RowHeadersWidth < (int)(size.Width + 20)) dgvDnTms.RowHeadersWidth = (int)(size.Width + 20);
					e.Graphics.DrawString(strRowNumber, this.Font, SystemBrushes.ControlText, r.Left + 15, r.Top + (r.Height - size.Height) / 2);
					break;
			}

			// отрисовка цветной рамки выбраной строки
			if (dgv == dgvDnTms && (e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected) {
				string rootdown = drv["RD"] == DBNull.Value ? "tNF" : drv["RD"].ToString();
				Pen p = MU.Kpi.Pens[rootdown];
				int hw = dgv.RowHeadersWidth;
				int totalwidth = 0;
				//e.Graphics.DrawLine(p, e.RowBounds.X + hw, e.RowBounds.Y, e.RowBounds.X + e.RowBounds.Width, e.RowBounds.Y);
				//e.Graphics.DrawLine(p, e.RowBounds.X + hw, e.RowBounds.Y + e.RowBounds.Height - 2, e.RowBounds.X + e.RowBounds.Width, e.RowBounds.Y + e.RowBounds.Height - 2);
				foreach (DataGridViewColumn dgvc in dgv.Columns)
					totalwidth += dgvc.Width;
				Rectangle rct = new Rectangle(e.RowBounds.X + hw, e.RowBounds.Y, totalwidth - 1, e.RowBounds.Height - 2);
				e.Graphics.DrawRectangle(p, rct);
				rct.Inflate(-1, -1);
				e.Graphics.DrawRectangle(p, rct);
			}
		}

		private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			//throw new NotImplementedException();
			DataGridView dgv = sender as DataGridView;
			BindingSource bs = dgv.DataSource as BindingSource;
			bs.ResetBindings(false);
			Debug.WriteLine("dgv_DataError___________________________________________dgv_DataError");
		}

		private void dgv_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			if (drv == null)
				return;

			string tb_name = "";
			switch (drv.Row.Table.TableName) {
				case "Stops": tb_name = "l_stops"; break;
				case "Brands": tb_name = "l_brands"; break;
				case "Shifts": tb_name = "l_shifts"; break;
			}
			string sel;
			sel = string.Format("SELECT ss.dt1, ss.name, ss.hash FROM {0} st LEFT OUTER JOIN " +
				" l_sessions ss ON st.Session = ss.id WHERE st.ID = {1}", tb_name, drv["ID"]);

			string res = "";
			//todo: добавить проверку хеша
			try {
				using (SqlConnection conn = new SqlConnection(SetConn(_lu[L].Connection))) {
					conn.Open();
					SqlCommand cmd = new SqlCommand(sel, conn);
					cmd.CommandTimeout = 100;
					using (SqlDataReader dr = cmd.ExecuteReader()) {
						if (dr.Read()) {
							res = "The entry was created or modified by the user: ";
							res += Environment.NewLine;
							res += dr.GetString(1).ToString();
							res += Environment.NewLine;
							res += "Session was started at: ";
							res += Environment.NewLine;
							res += dr.GetDateTime(0).ToString();
							MessageBox.Show(res, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}
		}

		private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			if (e.ColumnIndex < 3) {
				this.BeginInvoke(new MethodInvoker(delegate()
					{ dgv.CurrentCell = dgv[3, e.RowIndex]; }));
			}
		}

		private string CleanInputNumber(string str)
		{
			return System.Text.RegularExpressions.Regex.Replace(str, "[a-zA-Z\b-]", "");
		}

		private void dgvDnTms_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control.GetType() == typeof(DataGridViewTreeViewComboBoxEditingControl)) {
				////DataGridViewTreeViewComboBoxEditingControl c = e.Control as DataGridViewTreeViewComboBoxEditingControl;
				////if (c == null) return;
				////c.TreeView.ImageList = MonitorUnit.kpi.ImageList;
				//////c.SelectedIndex = -1;
				//////foreach (TreeNode n in c.TreeView.Nodes)
				//////   n.Expand();
			} else if (e.Control is DataGridViewTextBoxEditingControl) {
				DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
				//te.AutoCompleteMode = AutoCompleteMode.Suggest;
				//te.AutoCompleteSource = AutoCompleteSource.CustomSource;
				//te.AutoCompleteCustomSource.AddRange(new string[] { "Выходной день", "Выходная ночь", "Выходной идти некуда" });

				TextBox txt = e.Control as TextBox;
				txt.AutoCompleteCustomSource = _lu[L].AutoCompletes;
				txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
				txt.AutoCompleteSource = AutoCompleteSource.CustomSource;
			} else if (e.Control is DataGridViewComboBoxEditingControl) {
				DataGridViewComboBoxEditingControl cbx = (DataGridViewComboBoxEditingControl)e.Control;
				if (dgvDnTms.CurrentCell.Value == DBNull.Value)
					cbx.SelectedIndex = -1;
			}
		}

		private void dgvDnTms_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			if (dgv == null || dgv[e.ColumnIndex, e.RowIndex].Value == null)
				return;

			// получаем связанную с текущей ячейкой строку
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			// получаем связанную с текущей ячейкой колонку
			string colname = dgv.Columns[e.ColumnIndex].Name;

			if (drv == null || L < 0)
				return;

			DataGridViewCell cell = dgv[e.ColumnIndex, e.RowIndex];
			if (!cell.IsInEditMode)
				return;
			Control ctrl = dgv.EditingControl;

			if (dgv == dgvProds && colname == "NP") {
				//int val = 0;
				//if (!int.TryParse(e.FormattedValue.ToString(), out val) || val < 1) {
				//   if (Convert.ToInt32(drv["GP"]) != 0)
				//      e.Cancel = true;
				//} else if (Convert.ToInt32(drv["GP"]) == 0 && val != 0)
				//   e.Cancel = true;

				////string newval = CleanInputNumber(ctrl.Text);
				////if (newval != ctrl.Text)
				////   ctrl.Text = CleanInputNumber(newval);
			} else if (dgv == dgvDnTms && colname == "Comment") {
				string val = e.FormattedValue.ToString();
				if (!_lu[L].AutoCompletes.Contains(val))
					_lu[L].AutoCompletes.Add(val);
			}
		}

		private void dgvDnTms_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			////if (e.ColumnIndex < 0) return;
			////DataGridView dgv = sender as DataGridView;
			////if (dgv[e.ColumnIndex, e.RowIndex].Value == null)
			////   return;
			////DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			//////if (dgv.CurrentCell.OwningColumn.Name == "Mark1" || dgv.CurrentCell.OwningColumn.Name == "Mark2")
			//////   return;

			////if (dgv.CurrentCell.OwningColumn.Name.ToLower() == "downid") {
			////   //drv.Row.EndEdit();
			////   drv.EndEdit();
			////   dgv.EndEdit();
			////}

			//////if (drv != null && (drv.Row.RowState == DataRowState.Modified || drv.Row.RowState == DataRowState.Added))
			//////   if (dgv.Columns[e.ColumnIndex].Name.ToLower() == "downid")
			//////      drv.Row.EndEdit();
			////Debug.WriteLine("{{{{{{{{{{{{{dgvDnTms_CellEndEdit}}}}}}}}}}}}__" + dgv.CurrentCell.OwningColumn.Name);
		}

		private void dgvDnTms_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			////if (e.ColumnIndex < 0) return;
			////DataGridView dgv = sender as DataGridView;
			////if (dgv[e.ColumnIndex, e.RowIndex].Value == null)
			////   return;
			////DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			DataRowView drv = dgvDnTms.CurrentCell.OwningRow.DataBoundItem as DataRowView;
			if (drv != null && dgvDnTms.CurrentCell.OwningColumn.Name == "DownID") {
				// фиксируем изменение строки чтобы сразу перерисовывался чарт суток
				drv.EndEdit();
				// чтобы в дальнейшем не срабатывало событие изменения строки при изменении каждой колонки
				dgvDnTms.CancelEdit();
			}
		}

		private void dgvDnTms_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			if (dgv == null || dgv[e.ColumnIndex, e.RowIndex].Value == null)
				return;

			// получаем связанную с текущей ячейкой строку
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;

			if (drv == null || L < 0)
				return;
			DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
			if ((DateTime)drv["dt1"] < validDate) {
				//MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
				//   "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}


		#endregion | Edit grid events    |

		private void cmMap_Opening(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// прячем тултип
			Tt.RemoveAll();

			//todone: сделать пункт меню "Удалить" доступным только для админа и локального админа
			cmMap.Items["mnuApply"].Visible     = false;
			cmMap.Items["mnuDelProd"].Visible   = false;
			cmMap.Items["mnuChange"].Visible    = false;
			cmMap.Items["mnuAutoShift"].Visible = false;
			cmMap.Items["mnuShowMins"].Visible  = _chs != null && _lu[_chs.L].DataSet.Tables.Contains("Minutes");

			if (!_isEdit)
				return;	// нет режима редактирования

			////if (!selzone.Visible)
			////   return;	// зона редактирования не видна

			if (!_sz.Visible) GetCurrentBlackZone();
			if (!_sz.Visible || !_sz.MouseInside(mouse_location) ||
				(_sz.DownID != 0 && tabEdit.SelectedTab == tpEditDnTms))
				return;

			DateTime dtFrom = _sz.Start;
			DateTime dtTo = _sz.Stop;
			bool mouse_is_inside_seln = _sz.MouseInside(mouse_location);

			if (dtFrom == dtTo)
				return;
			if (tabEdit.SelectedTab == tpEditDnTms) {
				if (mouse_is_inside_seln) {
					cmMap.Items["mnuApply"].Visible = true;
					cmMap.Items["mnuApply"].Text = Res.mnuMapAddDwnTm;										// "Add downtime entry"
					cmMap.Items["mnuDelProd"].Visible = _lu[L].SessionData.Permission == UserPermit.LocalAdmin || _lu[L].SessionData.Permission == UserPermit.Admin;
				} else {
					if (_sz.IsEmptyDowntime && _sz.MouseInside(mouse_location)) {
						//todone: определить нет ли рядом еще черных зон, и если есть, добавить их к интервалу
						cmMap.Items["mnuApply"].Visible = true;
						DateTime dt1 = _dtEmpty;
						DateTime dt2 = _dtEmpty;
						GetAroundBlacks(ref dt1, ref dt2);
						_sz.Start = dt1;
						_sz.Stop = dt2.AddMinutes(1);
						cmMap.Items["mnuApply"].Text = Res.mnuMapAddDwnTm;									// "Add the downtime entry instead of the black zone";
					}
				}
			} else if (mouse_is_inside_seln && tabEdit.SelectedTab == tpEditProds) {
				cmMap.Items["mnuApply"].Visible = true;
				cmMap.Items["mnuApply"].Text = string.Format(Res.mnuMapSplit, mouse_location);	// "Split the production entry";
			} else if (mouse_is_inside_seln && tabEdit.SelectedTab == tpEditShifts) {
				cmMap.Items["mnuApply"].Visible = true;
				cmMap.Items["mnuApply"].Text = string.Format(Res.mnuMapSplit, mouse_location);	// "Split the shift entry";
				DateTime dt1, dt2;
                if (GetShiftTimeByTemplate(out dt1, out dt2))
                {
                    if (!use_easy_counter )
                    {
					cmMap.Items["mnuAutoShift"].Visible = true;
					cmMap.Items["mnuAutoShift"].Text = string.Format(Res.mnuMapAutoShift, dt1, dt2.AddMinutes(-1));
                    }
                }
			}
		}

		private bool GetCurrentBlackZone()
		{
			if (tabEdit.SelectedTab != tpEditDnTms)
				return false;
			if (_lu[L].MouseData.DowntimeID == -1 &&
				_lu[L].MouseData.GapSpeed <= _lu[L].LowSpeedTreshold) {
				DateTime dtFromSuggested = _lu[L].MouseData.DowntimeStart;
				DateTime dtToSuggested = _lu[L].MouseData.DowntimeStop;

				GetAroundBlacks(ref dtFromSuggested, ref dtToSuggested);
				if (_lu[L].MouseData.DowntimeStop.Subtract(_lu[L].MouseData.DowntimeStart) >
					dtToSuggested.Subtract(dtFromSuggested)) {
					_sz.Start = _lu[L].MouseData.DowntimeStart;
					_sz.Stop = _lu[L].MouseData.DowntimeStop.AddMinutes(1);
				} else {
					_sz.Start = dtFromSuggested;
					_sz.Stop = dtToSuggested.AddMinutes(1);
				}

				_sz.Visible = true;
				return true;
			}
			return false;
		}

		private void GetAroundBlacks(ref DateTime dt1, ref DateTime dt2)
		{
			int GP;
			// выбираем то, что лежит после мыши
			DataRow[] DR = _lu[L].DataSet.Tables["Gaps"].Select(string.Format("dt2>=#{0:M/d/yy H:m}#", mouse_location), "dt1");
			foreach (DataRow dr in DR) {
				GP = (int)dr["GP"];
				if (GP == 0 && dr["Stop"] == DBNull.Value)
					dt2 = (DateTime)dr["dt2"];
				else {
					if (use_smart_autofill && dr["Stop"] == DBNull.Value)
						GetNextBlack(true, ref dt2);
					break;
				}
			}

			// выбираем то, что лежит до мыши
			DR = _lu[L].DataSet.Tables["Gaps"].Select(string.Format("dt1<#{0:M/d/yy H:m}#", mouse_location), "dt1 DESC");
			foreach (DataRow dr in DR) {
				GP = (int)dr["GP"];
				if (GP == 0 && dr["Stop"] == DBNull.Value)
					dt1 = (DateTime)dr["dt1"];
				else {
					if (use_smart_autofill && dr["Stop"] == DBNull.Value)
						GetNextBlack(false, ref dt1);
					break;
				}
			}
		}

		private void GetNextBlack(bool ahead, ref DateTime dt)
		{
			string flt = ahead ? "dt1<=#{0:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#" : "dt1<=#{0:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#";
			DateTime dtnew = ahead ? dt.AddMinutes(1) : dt.AddMinutes(-1);
			DataRow dr = C.GetRow(_lu[L].DataSet.Tables["Gaps"], string.Format(flt, dtnew));

			if (dr != null && use_smart_autofill && dr["Stop"] == DBNull.Value) { // && C.GetInt(dr["Seconds"]) > 30
				string val = C.GetRowField(_lu[L].DataSet.Tables["Minutes"], string.Format("dt=#{0:M/d/yy H:m}#", dtnew), "Seconds");
				if (val == "")
					return;
				int secs = C.GetInt(val);
				if (secs > 20)
					dt = dtnew;
				if (secs > 40)
					GetNextBlack(ahead, ref dt);
			}
		}

		private void FindEmptyGap(bool minor)
		{
			if (!_isEdit)
				return;

			int TF;
			DateTime dt1, dt2;
			//DataRow[] DR = lu[L].DataSet.Tables["Gaps"].Select(string.Format("dt2>=#{0:M/d/yy H:m}#", mouse_location), "dt1");
			////foreach (DataRow dr in lu[L].DataSet.Tables["Gaps"].Select("GP=0 AND Stop IS NULL", "dt1")) {
			foreach (DataRow dr in _lu[L].DataSet.Tables["Gaps"].Select("TF/(TF-Seconds)>=4 AND Stop IS NULL", "dt1")) {
				//GP = (int)dr["GP"];
				TF = (int)dr["TF"];
				if ((!minor && TF >= _lu[L].MinIdleTime) || (minor && TF < _lu[L].MinIdleTime)) {
					dt1 = (DateTime)dr["dt1"];
					dt2 = (DateTime)dr["dt2"];
					mouse_location = dt1;
					GetAroundBlacks(ref dt1, ref dt2);
					if (minor && dt2.Subtract(dt1).TotalMinutes >= _lu[L].MinIdleTime)
						continue;
					if (_chs.ChartArea.AxisX.ScrollBar.Scale != 1) {
						double min = _chs.ChartArea.AxisX.ScrollBar.Min;
						double max = _chs.ChartArea.AxisX.ScrollBar.Max;
						//double scale = chs.ChartArea.AxisY.ScrollBar.Scale;
						double val = (_chs.GetDouble(dt1) - min) / (max - min);
						if (val > 0.5) val = (_chs.GetDouble(dt2) - min) / (max - min);
						_chs.ChartArea.AxisX.ScrollBar.Value = val;
					}

					DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
					if (validDate >= dt2) {
						continue;
					} else if (dt1 < validDate && validDate < dt2)
						dt1 = validDate;

					AddDownTime(dt1, dt2, 0, 0, 0, "");
					//chs.ChartArea.AxisY.ScrollBar.Max = 0.5;
					//chs.ChartArea.AxisY.ScrollBar.Scroll(ScrollEventType.SmallIncrement);
					Cursor.Position = dgvDnTms.EditingControl.PointToScreen(new Point(100, 100));
					//Cursor.Position = dgvDnTms.PointToScreen(new Point(100, 100));
					break;
				}
			}
		}

		private void AddNewEntry()
		{
			if ((_state & C.State.Idle) == 0)
				return;

			if (!_sz.Visible || !_sz.MouseInside(mouse_location))
				return;

#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			DateTime dtFrom = _sz.Start;
			DateTime dtTo = _sz.Stop.AddMinutes(-1);

			DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
			if (validDate >= dtTo) {
				MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
				"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			} else if (dtFrom < validDate && validDate < dtTo)
				dtFrom = validDate;

			if (tabEdit.SelectedTab == tpEditDnTms)
				AddDownTime(dtFrom, dtTo, 0, 0, 0, "");
			else if (tabEdit.SelectedTab == tpEditProds)
				SplitProduction(mouse_location, dtTo, 0, 0, true);
			else if (tabEdit.SelectedTab == tpEditShifts)
				SplitShift(mouse_location, dtTo, 0, false);

#if DEBUG
			Debug.WriteLine("AddNewEntry(): " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private void AddDownTime(DateTime dt1, DateTime dt2, int mark1, int mark2, int downid, string comment)
		{

			if (_lu[L].Frozen) return;
			Freeze();

			SetCursWait();

			DataRow drNew = _lu[L].AddDownTime(dt1, dt2, mark1, mark2, downid, comment);

			//bsDnTms.Sort = bsDnTms.Sort == "dt2" ? "dt1" : "dt2";
			SetCursDefault();

			// select added row in grid
			_bsDnTms.Position = _bsDnTms.Find("ID", drNew["ID"]);
			CheckRowForNulls(drNew);

			//Debug.WriteLine(string.Format("AddDownTime_5 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			if (dgvDnTms.CurrentRow != null && downid == 0) {
				dgvDnTms.CurrentCell = dgvDnTms["DownID", dgvDnTms.CurrentRow.Index];

				// a trick to show drop down treeview instantly (without any click)
				//dgvDnTms.Select();
				dgvDnTms.BeginEdit(true);
				if (dgvDnTms.EditingControl != null
					  && dgvDnTms.EditingControl is DataGridViewTreeViewComboBoxEditingControl) {
					DataGridViewTreeViewComboBoxEditingControl cmb = this.dgvDnTms.EditingControl as DataGridViewTreeViewComboBoxEditingControl;
					cmb.DroppedDown = true;
					cmb.Focus();
					cmb.TreeView.Focus();
				}
			}
			//Debug.WriteLine(string.Format("AddDownTime() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}

		private void SplitProduction(DateTime dt1, DateTime dt2, int BrandID, int FormatID, bool showdropdown)
		{
			Stopwatch sw = Stopwatch.StartNew();

			// при изменении таблицы предотвращаем вызов Commit
			Freeze();
			Debug.WriteLine(string.Format("AddProduction_0 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			SetCursWait();

			DataRow drNew = _lu[L].SplitProduction(dt1, dt2, BrandID, FormatID);

			if (dt2 == _dtEmpty)
				return;

			// select added row in grid
			_bsProds.Position = _bsProds.Find("ID", drNew["ID"]);

			//bsProds.Sort = bsProds.Sort == "dt2" ? "dt1" : "dt2";
			SetCursDefault();

			if (show_dropdown_for_new_prod_or_shift && showdropdown && dgvProds.CurrentRow != null) {
				dgvProds.CurrentCell = dgvProds["Brand", dgvProds.CurrentRow.Index];

				// a trick to show drop down treeview instantly (without any click)
				dgvProds.BeginEdit(false);
				if (dgvProds.EditingControl != null
					  && dgvProds.EditingControl is DataGridViewListViewComboBoxEditingControl) {
					DataGridViewListViewComboBoxEditingControl cmb = this.dgvProds.EditingControl as DataGridViewListViewComboBoxEditingControl;
					cmb.DroppedDown = true;
					cmb.Focus();
				}
			}
			sw.Stop();
			Debug.WriteLine(string.Format("AddProduction() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}

		private void SplitShift(DateTime dt1, DateTime dt2, int ShiftID, bool TwoSteps)
		{
			Stopwatch sw = Stopwatch.StartNew();

			// при изменении таблицы предотвращаем вызов Commit
			Freeze();
			Debug.WriteLine(string.Format("AddShift_0 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			SetCursWait();

			DataRow drNew = _lu[L].SplitShift(dt1, dt2, ShiftID, TwoSteps);

			if (TwoSteps)
				return;

			// select added row in grid
			_bsShifts.Position = _bsShifts.Find("ID", drNew["ID"]);

			//bsShifts.Sort = bsShifts.Sort == "dt2" ? "dt1" : "dt2";
			SetCursDefault();

			if (show_dropdown_for_new_prod_or_shift && dgvShifts.CurrentRow != null) {
				dgvShifts.CurrentCell = dgvShifts["Shift", dgvShifts.CurrentRow.Index];

				// a trick to show drop down treeview instantly (without any click)
				dgvShifts.BeginEdit(false);
				if (dgvShifts.EditingControl != null
					  && dgvShifts.EditingControl is DataGridViewComboBoxEditingControl) {
					DataGridViewComboBoxEditingControl cmb = this.dgvShifts.EditingControl as DataGridViewComboBoxEditingControl;
					cmb.DroppedDown = true;
					cmb.Focus();
				}
			}
			sw.Stop();
			Debug.WriteLine(string.Format("AddShift() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}

		private void DeleteEntry(BindingSource bs)
		{
			if (delete_in_use)
				return;

			int position = bs.Position;
			if (position < 0 || bs.Count == 0)
				return;

			// устанавливаем флаг для предотвращения повторного вызова
			delete_in_use = true;

			DataRowView drvcurr = null;
			if (bs.Current != null) drvcurr = bs.Current as DataRowView;
			if (drvcurr == null)
				return;
			DataRowView drvinstead;

			DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
			if ((DateTime)drvcurr.Row["dt1"] < validDate) {
				MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
					"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				delete_in_use = false;
				return;
			}


			// Для простоев удаляются строки из DataTable, но при сохранении в базе данных
			// строки не удаляются а устанавливаются в NULL поле простоя в таблице гепов
			if (bs == _bsDnTms) {
				SetCursWait();
				// Удаляем без фриза, будто это событие пришло напрямую от контрола
				// тем самым избегаем вызов ResetBindings(false) при котором в любом случае передергивается грид
				drvcurr.Row.Delete();
				_lu[L].UndoRedo.CommitQuiet();
				UndoRedo_OnStackChanged("foo", EventArgs.Empty);
				SetCursDefault();
			}

			// Для брендов и смен меняется значение l_gaps.Brand на соседнее (предыдущее или последующее)
			// строка, к которой добавились гепы от удаленного бренда устанавливается в состояние Added
			if (bs == _bsProds || bs == _bsShifts) {
				if (bs.Count == 1) {
					MessageBox.Show(Res.msgLastRecord, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				} else {
					SetCursWait();

					Freeze();
					// ищем соседнюю (по времени) строку
					if (position == bs.Count - 1) // если текущая позиция последняя то берем предыдущую
						drvinstead = bs[position - 1] as DataRowView;
					else									// во всех других случаях берем следующую
						drvinstead = bs[position + 1] as DataRowView;

					// от 'удаляемой' строки переподключаем гепы на соседнюю строку
					if (bs == _bsProds) {
						foreach (DataRow dr in drvcurr.Row.GetChildRows("rel"))
							dr["Brands"] = drvinstead["ID"];
						drvinstead["FactorNP"] = 1;
					} else {
						foreach (DataRow dr in drvcurr.Row.GetChildRows("ShGaps"))
							dr["Shifts"] = drvinstead["ID"];
					}
					drvinstead.Row.AcceptChanges();
					drvinstead.Row.SetAdded();

					// фиксируем изменения
					_lu[L].UndoRedo.Commit();

					SetCursDefault();
				}
			}

			delete_in_use = false;
		}

		private void DeleteProduction()
		{
			if ((_state & C.State.Idle) == 0)
				return;

			if (CheckToSave())
				return;
			string msg = string.Format(Res.msgWillBeDeleted, Environment.NewLine);
			if (MessageBox.Show(msg, "Warning!", MessageBoxButtons.YesNo,
				MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2)
				!= DialogResult.Yes)
				return;

			DateTime dt1 = _sz.Start;
			DateTime dt2 = _sz.Stop.AddMinutes(-1);

			// без этого лишнего DoEvents() не появляется Wait курсор
			Application.DoEvents();
			SetCursWait();
			Freeze();

			_lu[L].DeleteProduction(dt1, dt2);

			SetCursDefault();
		}

		private void AddShiftByTemplate()
		{
			DateTime dt1, dt2, dtprev = _dtEmpty;

			if (GetShiftTimeByTemplate(out dt1, out dt2)) {
				// если это зона внутни другой большой зоны, тогда делим её на две части
				DataRow[] DR = _lu[L].DataSet.Tables["Shifts"].Select(string.Format("dt1<#{0:M/d/yy H:m}#", dt1), "dt1 DESC", DataViewRowState.CurrentRows);
				if (DR.Length > 0) {
					DataRow dr = DR[0];
					if ((DateTime)dr["dt2"] > dt2)
						SplitShift((DateTime)dr["dt1"], dt1.AddMinutes(-1), 0, true);
				}

				SplitShift(dt1, dt2.AddMinutes(-1), 0, false);
			}
		}

		private bool GetShiftTimeByTemplate(out DateTime dt1, out DateTime dt2)
		{
			DateTime base_dt;
			base_dt = mouse_location.Date;
			dt1 = base_dt.AddMinutes(_lu[L].ShiftMins[0]);
			dt2 = base_dt.AddMinutes(_lu[L].ShiftMins[0]);
			if (dt1 > mouse_location)
				base_dt = base_dt.AddDays(-1);

			// если в сутках две смены
			if (_lu[L].ShiftMins.Count == 2) {
				dt1 = base_dt.AddMinutes(_lu[L].ShiftMins[0]);
				dt2 = base_dt.AddMinutes(_lu[L].ShiftMins[1]);
				if (dt1 < mouse_location && mouse_location < dt2) return true;
				else {
					dt1 = dt2;
					dt2 = base_dt.AddDays(1).AddMinutes(_lu[L].ShiftMins[0]);
				}
				if (dt1 < mouse_location && mouse_location < dt2) return true;
			}
			// если в сутках 3 смены
			if (_lu[L].ShiftMins.Count == 3) {
				dt1 = base_dt.AddMinutes(_lu[L].ShiftMins[0]);
				dt2 = base_dt.AddMinutes(_lu[L].ShiftMins[1]);
				if (dt1 < mouse_location && mouse_location < dt2) return true;
				else {
					dt1 = dt2;
					dt2 = base_dt.AddMinutes(_lu[L].ShiftMins[2]);
				}
				if (dt1 < mouse_location && mouse_location < dt2) return true;
				else {
					dt1 = dt2;
					dt2 = base_dt.AddDays(1).AddMinutes(_lu[L].ShiftMins[0]);
				}
				if (dt1 < mouse_location && mouse_location < dt2) return true;
			}
			return false;
		}

		private void bsDnTms_PositionChanged(object sender, EventArgs e)
		{
			if (!_isEdit || IsEmpty())
				return;

			BindingSource bs = sender as BindingSource;
			if (bs == null || bs.IsBindingSuspended)
				return;
			DataRowView drv = bs.Current as DataRowView;
			if (drv == null)
				return;
			try {
				DateTime dt;
				if (drv != null && DateTime.TryParse(drv["dt1"].ToString(), out dt)) {
					ChangeSelectedZone(drv.Row);
					//////sz.Start = dt;
					//////sz.Stop = ((DateTime)drv["dt2"]).AddMinutes(1); // = dt.AddMinutes((int)drv["TF"]);
					//////sz.Visible = true;
					//////sz.DownID = C.GetInt(drv["ID"]);
					//////sz.Kind = SelectedZone.TimeZoneKind.DowntimeHighlited;

					//////sz.ID = C.GetInt(drv["ID"]);
					//////ch[L].Update();

					//ch[L].Update();
					//bs.Position = bs.Position;
					//dgvDnTms.Update();
				}
			} catch { }
			Debug.WriteLine("bsDowntimes_PositionChanged");
		}

		private void bsProdShift_PositionChanged(object sender, EventArgs e)
		{
			if (!_isEdit || IsEmpty())
				return;

			BindingSource bs = sender as BindingSource;
			if (bs == null || bs.IsBindingSuspended)
				return;
			DataRowView drv = bs.Current as DataRowView;
			if (drv == null)
				return;

			DateTime dt;
			if (_sz != null && drv != null && DateTime.TryParse(drv["dt1"].ToString(), out dt)) {
				_sz.Visible = true;

				_sz.Start = dt;
				//dt.AddMinutes(C.GetInt( drv["TF"]));
				_sz.Stop = ((DateTime)drv["dt2"]).AddMinutes(1);

				//selzone.Kind = TimeZone.TimeZoneKind.DowntimeHighlited;
				_ch[L].Update();
				//dgvDowntimes.Update();
			}
		}

        private void ChangeSelectedZone_Counter(DataRow drv)
        {
            if (_sz.DownID == 0 && _sz.Visible)
            {
                return;
            }
            else if (drv != null ) //&& drv.RowState != DataRowState.Deleted && drv.RowState != DataRowState.Detached)
            {
                if (_sz.DownID != C.GetInt(drv["ID"]))
                {
                    _sz.Start   = (DateTime)drv["dt"];
                    _sz.Stop    = ((DateTime)drv["dt"]).AddMinutes(1); // = dt.AddMinutes((int)drv["TF"]);
                    _sz.Visible = true;
                    _sz.DownID  = C.GetInt(drv["ID"]);
                    _sz.Kind    = SelectedZone.TimeZoneKind.DowntimeHighlited;

                    _sz.ID = C.GetInt(drv["ID"]);
                    _sz.Chart.Update();
                }
            }
            else if (_sz.Visible)
            {
                _sz.Visible = false;
                _sz.Chart.Update();
            }
        }
		private void ChangeSelectedZone(DataRow drv)
		{
			if (_sz.DownID == 0 && _sz.Visible) {
				return;
			} else if (drv != null && drv.RowState != DataRowState.Deleted && drv.RowState != DataRowState.Detached) {
				if (_sz.DownID != C.GetInt(drv["ID"])) {
					_sz.Start = (DateTime)drv["dt1"];
					_sz.Stop = ((DateTime)drv["dt2"]).AddMinutes(1); // = dt.AddMinutes((int)drv["TF"]);
					_sz.Visible = true;
					_sz.DownID = C.GetInt(drv["ID"]);
					_sz.Kind = SelectedZone.TimeZoneKind.DowntimeHighlited;

					_sz.ID = C.GetInt(drv["ID"]);
					_sz.Chart.Update();
				}
			} else if (_sz.Visible) {
				_sz.Visible = false;
				_sz.Chart.Update();
			}
		}

		#region | ChartMap Size&Scale |

		private void ResetChartMap()
		{
			//editmode = EditMode.None;
			if (_sz != null) _sz.Reset();
			Cursor = Cursors.Default;
        }
        private void chbShowBigMap_CheckStateChanged(object sender, EventArgs e)
        {
            if (L < 0 || _lu == null)
                return;
            _monitorSettings.BigMap = _chbShowBigMap.Checked;
            SetChartMapHeight(_chbShowBigMap.Checked);
            //foreach (C1ChartR c in ch)
            //   chartmap_VisibleChanged(c, null);
        }
		private void SetChartMapHeight(bool IsHight)
		{
			pnMaps.SuspendLayout();
			if (IsHight) {
				foreach (C1Chart chart in _ch) {
					if (Visible) {
						chart.Height = _chMapHeiBig;
						chart.ChartArea.AxisY.Visible = true;
						chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
					}
				}
			} else {
				foreach (C1Chart chart in _ch) {
					if (Visible) {
						chart.ChartArea.AxisY.Visible = false;
						chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei); //new Size(-1, chMapAreaHei);
						chart.Height = _chMapHei;
					}
				}
			}
			Debug.WriteLine("SetChartMapHeight");
			pnMaps.ResumeLayout();
		}

        private void chartmap_VisibleChangedCounter(object sender, EventArgs e)
        {
            C1Chart chart = sender as C1Chart;
            if (!Visible)
                return;
            if ( cbBigChartCounter.Checked )
            {
                chart.Height = _chMapHeiBig;
                chart.ChartArea.AxisY.Visible = true;
                chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
            }
            else
            {
                chart.Height = _chMapHei;
                chart.ChartArea.AxisY.Visible = false;
                chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei);
            }
        }

		private void chartmap_VisibleChanged(object sender, EventArgs e)
		{
			C1Chart chart = sender as C1Chart;
			if (!Visible)
				return;
			if (tsbEdit.Checked || _monitorSettings.BigMap) {
				chart.Height = _chMapHeiBig;
				chart.ChartArea.AxisY.Visible = true;
				chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
			} else {
				chart.Height = _chMapHei;
				chart.ChartArea.AxisY.Visible = false;
				chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei);
			}
		}

		#endregion | ChartMap Size&Scale |

		private void tabEdit_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_sz != null) _sz.Reset();

			if (tabEdit.SelectedTab == tpEditDnTms)
				tsbEditDelete.Enabled = _bsDnTms.Count > 0;
			if (tabEdit.SelectedTab == tpEditProds)
				tsbEditDelete.Enabled = _bsProds.Count > 1;
			if (tabEdit.SelectedTab == tpEditShifts)
				tsbEditDelete.Enabled = _bsShifts.Count > 1;
		}

		private bool CheckToSave()
		{
			if (L == -1)
				return false;
			if (_lu[L].UndoRedo.ThereIsUndo) {
				MessageBox.Show(Res.msgShouldToSave, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return true;
			} else
				return false;
		}

		private bool CheckForNullsBeforeToSave()
		{
			//todone: перед сохранением надо проверять все ли поля заполнены.
			foreach (DataRow r in _lu[L].DataSet.Tables["Stops"].Rows) {
				if ((r.RowState == DataRowState.Added || r.RowState == DataRowState.Modified)
					&& r["TF"] != DBNull.Value && r.GetColumnsInError().Length > 0) {
					MessageBox.Show(Res.msgCheckForNulls,
						"Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return true;
				}
			}
			return false;
		}

		private int GetGP(DateTime dt1, DateTime dt2)
		{
			//int res = 0;
			string dates = C.GetCDateFilter("dt", dt1, dt2);
			//Int32.TryParse(lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates).ToString(), out res);
			//return res;
			return C.GetInt(_lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
		}

		private float GetSeconds(DateTime dt1, DateTime dt2)
		{
			//int res = 0;
			string dates = C.GetCDateFilter("dt", dt1, dt2);
			//Int32.TryParse(lu[L].DataSet.Tables["Minutes"].Compute("SUM(Seconds)", dates).ToString(), out res);
			//return res/60F;
			return C.GetInt(_lu[L].DataSet.Tables["Minutes"].Compute("SUM(Seconds)", dates)) / 60F;
		}

		private void ReloadDataInEditMode()
		{
			_lu[L].Error = "Should to reload";
			SetCursWait();

			// Необязательные визуальные примочки: окрашиваем все в серый, т.е. в стиле disabled
			_ch[L].UseGrayscale = true;
			_ch[L].Refresh();
			//Thread.Sleep(3000);

			// После этого вызова будет завершено через другие потоки в Finish()
			LoadFailedStart();
		}

		private void CheckRowForNulls(DataRow dr)
		{
			string err = "Empty value not allowed for this column";
			string col;
			if (dr.Table.TableName == "Stops") {
				int downid = dr["DownID"] == DBNull.Value ? -1 : (int)dr["DownID"];

				dr.SetColumnError("DownID", downid == -1 ? err : "");
				dr.SetColumnError("Comment", !allow_empty_comments && dr["Comment"] == DBNull.Value ? err : "");
				if (downid >= 0 && dr["D0"] != DBNull.Value) {
					DataRow[] DR = _lu[L].DataSet.Tables["MarkBandList"].Select("KpiID=" + dr["D0"].ToString());
					foreach (DataRow drm in DR) {
						err = string.Format("Для значения '{0}' {1}эта колонка должна быть заполнена", dr["FullName"], Environment.NewLine);
						col = "Mark" + drm["ID"].ToString();
						dr.SetColumnError(col, dr[col] == DBNull.Value ? err : "");
						dr.SetColumnError("Comment", !allow_empty_red_comments && dr["Comment"] == DBNull.Value ? err : "");
					}
					if (DR.Length == 0) {
						if (dr.Table.Columns.Contains("Mark1")) dr.SetColumnError("Mark1", "");
						if (dr.Table.Columns.Contains("Mark2")) dr.SetColumnError("Mark2", "");
						dr.SetColumnError("Comment", "");
					}
				}
			}
		}

		private void ResizeDowntimesGrid()
		{
			try {
				int width = dgvDnTms.Width;
				foreach (DataGridViewColumn dc in dgvDnTms.Columns)
					if (dc.Name != "Comment" && dc.Visible)
						width -= dc.Width;
				width = width - dgvDnTms.RowHeadersWidth - SystemInformation.VerticalScrollBarWidth;
				if (width < 200) width = 200;
				dgvDnTms.Columns["Comment"].Width = width;
				//dgvDnTms.AutoResizeRows();
			} catch { }
		}



		#region | credentials      |

		/// <summary>
		/// Возвращает true только в том случае, если проверка для конечного юнита (не узла!!!) пройдена успешно,
		/// т.е. есть разрешение на редактирование этого юнита текущим юзером
		/// </summary>
		/// <param name="mu"></param>
		/// <returns></returns>
		private bool GetCredentials(MU mu)
		{
			bool res = false;
			SessData sd;

			#region | проходим один раз, но до первой успешной попытки |

			if (_user == null) {
				if (MU.UseWinAuthentication) {
					string username = Environment.UserDomainName + "\\" + Environment.UserName;
					DataTable dtb = frmLogin.LookupUser(username, MU.Config.Units[mu.ParentIdKpi].Connection);
					DataRow r;
					if (dtb != null && dtb.Rows.Count > 0) {
						r = dtb.Rows[0];
						_user = new MonitorUser(C.GetInt(r["ID"]), username, C.GetInt(r["session"].ToString()));
						_user.Permission = frmLogin.GetRole(r["Role"].ToString(), (int)r["Salt"]);
					} else if (dtb != null) {
						// таблица есть но пустая т.е. юзера можно больше не искать, его нет вообще с следовательно прав у него нет
						// и мы сюда больше не попадем, и поэтому говорим юзеру о случившемся
						MessageBox.Show(Res.msgHaveNoPerm, "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						_user = new MonitorUser(0, username, 0);
						_user.Permission = 0;
						_user.RestrictedMessageWasShown = true;
					}
					if (_user == null) {
						// произошла ошибка при соединении с базой данных поэтому пользователя не создаем
						MessageBox.Show(Res.msgPermError, "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				} else {
					using (frmLogin f = new frmLogin(MU.Config.Units[mu.ParentIdKpi].Connection)) {
						if (f.ShowDialog() == DialogResult.OK)
							_user = f.User;
					}
					// если юзер найден, но прав у него нет то мы сюда больше не попадем, и поэтому говорим юзеру о случившемся
					if (_user != null && (_user.UserPermit & (UserPermit.Admin | UserPermit.LocalAdmin | UserPermit.LocalForeman)) == 0) {
						if (!_user.RestrictedMessageWasShown) {
							MessageBox.Show(Res.msgPermVerButNo, "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							_user.RestrictedMessageWasShown = true;
						}
					}
				}
				////if (user != null) {
				////   //slUser.Text = user.Name;
				////   //slPermission.Text = user.UserPermit.ToString();
				////   if (user.UserPermit == UserPermit.Disabled)
				////      Close();
				////}
			}

			#endregion | проходим один раз, но до первой успешной попытки |

			if (_user == null || mu.Connection == "" || (_user.UserPermit & (UserPermit.Admin | UserPermit.LocalAdmin | UserPermit.LocalForeman)) == 0)
				return false;

			// если у юнита есть свое соединение и нет в кэше то делаем запрос сессии
			// запрос делать надо если даже аdmin, чтобы был получени номер сессии
			//if (!sessions.ContainsKey(mu.ID)) {	lu[L].SessionData
			if (_lu[L].SessionData == null) {
				SetCursWait();

				int sess = 0;
				sd = new SessData();
				sess = GetSession(_user, mu.Connection);
				if (sess < 0) {
					SetCursDefault();
					return false; // произошла какая-то ошибка, надо пробовать еще раз, т.е. выходим без добавления сессии
				}
				sd.SessionID = sess;
				sd.Valid = sess > 0;
				if (sd.Valid) {
					sd.Permission = (UserPermit)_user.Permission;
					res = true;
				} else {
					sd.Permission = UserPermit.None;
					sd.RestrictedMessageWasShown = true;
					MessageBox.Show(Res.msgHaveNoPermLine, "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				//sessions.Add(mu.ID, sd);
				_lu[L].SessionData = sd;

				SetCursDefault();
			} else
				res = _lu[L].SessionData.Valid;

			return res;
		}

		/// <summary>
		/// Если соединение прошло с ошибкой то вернет -1
		/// Если соединение прошло но либо юзера нет в таблице r_users либо есть но valid = false то возвращает ноль
		/// Если r_users.valid  = true и юзер является локальным админом то получаем номер сессии
		/// или если глобальный админ то получаем номер сессии без всяких других условий
		/// </summary>
		/// <param name="User"></param>
		/// <param name="connStr"></param>
		/// <returns></returns>
		private int GetSession(MonitorUser User, string connStr)
		{
			//
			DateTime dt1 = DateTime.Now;
			int sess = -1;
			string hash = C.GetMd5Hash(dt1.ToString("yyyyMMddHHmmss") + User.Name, dt1.Minute);
			DataTable result = new DataTable();

			const string query = "e_getsession";
			using (SqlConnection conn = new SqlConnection(connStr)) {
				conn.Open();
				using (SqlCommand cmd = new SqlCommand(query, conn)) {
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@id", SqlDbType.Int).Value = User.Id;
					cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = User.Name;
					cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime).Value = dt1;
					cmd.Parameters.Add("@hash", SqlDbType.Char, 32).Value = hash;
					using (SqlDataReader dr = cmd.ExecuteReader()) {
						result.Load(dr);
						if (result.Rows.Count > 0) {
							sess = C.GetInt(result.Rows[0]["session"]);
							if ((C.GetBool(result.Rows[0]["valid"]) && (User.UserPermit == UserPermit.LocalAdmin || User.UserPermit == UserPermit.LocalForeman) || User.UserPermit == UserPermit.Admin))
								return sess;
							// если соединение прошло и получен номер сессии и valid то возвращаем номер сессии или если глобальный админ
							else
								return 0;
							// соединение прошло но valid = false, поэтому возвращает ноль
						}
					}
				}
			}
			return sess;
		}

		#endregion | credentials      |

		private bool HasRight()
		{
			return true;
		}

		//private bool CheckDateWithMessage()
		//{
		//   DateTime validDate = _lu[L].GetEditValidStartDate(_user.UserPermit);
		//   if (validDate > _lu[L].EndCorrected) {
		//      MessageBox.Show(string.Format("Даннные за период до {0:g} закрыты для редактирования", validDate),
		//                  "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//      return false;
		//   }
		//   return true;
		//}
	}
}

/*
-- заполнение таблицы простоев короткими оставами из дерева простоев

DECLARE @dt1 smalldatetime, @dt2 smalldatetime

SET @dt1='20100101'
SET @dt2='20110101'

SELECT MIN(DT) [dt1], COUNT(DT) TF, l_gaps.Stop
INTO #t1
FROM l_rozliv JOIN l_gaps ON l_rozliv.gap=l_gaps.gap
WHERE DT>=@dt1 AND DT<@dt2
GROUP BY l_gaps.Stop
ORDER BY MIN(DT)

UPDATE l_Stops
SET DownID=10002
WHERE id IN (SELECT Stop FROM #t1 WHERE TF=4)

DROP TABLE #t1
 *
 *
 *

ALTER PROCEDURE dbo.v_addstopsimple
	@dt1 smalldatetime,
	@dt2 smalldatetime, -- dt2 не входит в обновляемый интервал
	@DownID int,
	@Mark1 int,
	@Mark2 int,
	@Comment nvarchar(200),
	@session int,
	@ID int out
AS

DECLARE @GapNew int, @Brand int, @Shift int

-- определяем @Brand и @Shift в выбранном интервале времени
SELECT TOP 1 @Brand=l_gaps.brand, @Shift=l_gaps.shift
FROM l_rozliv JOIN l_gaps ON l_rozliv.gap=l_gaps.gap
WHERE  dt>=@dt1 and dt<=@dt2
ORDER BY dt

-- вставляем новую строку в таблицу простоев
INSERT INTO l_stops (DownID, Mark1, Mark2, Comment, session)
			VALUES (@DownID, @Mark1, @Mark2, @Comment, @session)
SET @ID=SCOPE_IDENTITY()

-- вставляем новый геп
INSERT INTO l_gaps (Stop, Brand, Shift)
			VALUES(@ID, @Brand, @Shift)
SET @GapNew = SCOPE_IDENTITY()

-- обновляем гепы в таблице розлива
UPDATE l_rozliv SET gap=@GapNew WHERE dt>=@dt1 and dt<@dt2

*/