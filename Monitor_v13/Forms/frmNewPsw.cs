using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace ProductionMonitor
{
	/// <summary>
	/// Summary description for password.
	/// </summary>
	public class frmNewPsw : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label lb2;
		private System.Windows.Forms.Label lb3;
		private System.Windows.Forms.TextBox passField;
		private System.Windows.Forms.TextBox confirmField;
		private System.Windows.Forms.Label lb4;
		private System.Windows.Forms.Label entropyStatus;
		private System.Windows.Forms.Button btOk;
		private System.Windows.Forms.Button btCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmNewPsw()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
				components.Dispose();
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lb2 = new System.Windows.Forms.Label();
			this.lb3 = new System.Windows.Forms.Label();
			this.passField = new System.Windows.Forms.TextBox();
			this.confirmField = new System.Windows.Forms.TextBox();
			this.lb4 = new System.Windows.Forms.Label();
			this.entropyStatus = new System.Windows.Forms.Label();
			this.btOk = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lb2
			// 
			this.lb2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
							| System.Windows.Forms.AnchorStyles.Right)));
			this.lb2.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lb2.Location = new System.Drawing.Point(450, 9);
			this.lb2.Name = "lb2";
			this.lb2.Size = new System.Drawing.Size(270, 42);
			this.lb2.TabIndex = 1;
			this.lb2.Text = "It is recommended that you enter a strong password by using a combination of A-Z," +
				 " a-z, 0-9, and punctuation marks.";
			// 
			// lb3
			// 
			this.lb3.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lb3.Location = new System.Drawing.Point(12, 20);
			this.lb3.Name = "lb3";
			this.lb3.Size = new System.Drawing.Size(88, 16);
			this.lb3.TabIndex = 2;
			this.lb3.Text = "Password:";
			this.lb3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// passField
			// 
			this.passField.Location = new System.Drawing.Point(106, 19);
			this.passField.MaxLength = 64;
			this.passField.Name = "passField";
			this.passField.Size = new System.Drawing.Size(134, 20);
			this.passField.TabIndex = 0;
			this.passField.UseSystemPasswordChar = true;
			this.passField.TextChanged += new System.EventHandler(this.passField_TextChanged);
			// 
			// confirmField
			// 
			this.confirmField.Location = new System.Drawing.Point(106, 45);
			this.confirmField.MaxLength = 64;
			this.confirmField.Name = "confirmField";
			this.confirmField.Size = new System.Drawing.Size(134, 20);
			this.confirmField.TabIndex = 1;
			this.confirmField.UseSystemPasswordChar = true;
			// 
			// lb4
			// 
			this.lb4.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.lb4.Location = new System.Drawing.Point(12, 45);
			this.lb4.Name = "lb4";
			this.lb4.Size = new System.Drawing.Size(88, 18);
			this.lb4.TabIndex = 4;
			this.lb4.Text = "Confirm:";
			this.lb4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// entropyStatus
			// 
			this.entropyStatus.Location = new System.Drawing.Point(450, 106);
			this.entropyStatus.Name = "entropyStatus";
			this.entropyStatus.Size = new System.Drawing.Size(270, 32);
			this.entropyStatus.TabIndex = 6;
			this.entropyStatus.Text = "Waiting for password entry...";
			// 
			// btOk
			// 
			this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btOk.Location = new System.Drawing.Point(207, 83);
			this.btOk.Name = "btOk";
			this.btOk.Size = new System.Drawing.Size(75, 23);
			this.btOk.TabIndex = 2;
			this.btOk.Text = "Ok";
			this.btOk.Click += new System.EventHandler(this.ok_Click);
			// 
			// btCancel
			// 
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btCancel.Location = new System.Drawing.Point(126, 83);
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size(75, 23);
			this.btCancel.TabIndex = 3;
			this.btCancel.Text = "Cancel";
			// 
			// frmNewPsw
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size(294, 118);
			this.Controls.Add(this.btCancel);
			this.Controls.Add(this.btOk);
			this.Controls.Add(this.entropyStatus);
			this.Controls.Add(this.confirmField);
			this.Controls.Add(this.lb4);
			this.Controls.Add(this.passField);
			this.Controls.Add(this.lb3);
			this.Controls.Add(this.lb2);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmNewPsw";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Please Enter a Password";
			this.Load += new System.EventHandler(this.frmNewPsw_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmNewPsw_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		private void ok_Click(object sender, System.EventArgs e)
		{
			if (passField.TextLength < 5) {
				passField.Focus();
				MessageBox.Show(this,
					Properties.Resources.strNewPswPleaseValid, "Input Error",
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				passField.Focus();
				return;
			}

			if (confirmField.TextLength < 5) {
				confirmField.Focus();
				MessageBox.Show(this,
					Properties.Resources.strNewPswReEnter, "Input Error",
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				confirmField.Focus();
				return;
			}

			if (!passField.Text.Equals(confirmField.Text)) {
				// Fields don't match
				confirmField.Focus();
				MessageBox.Show(this,
					Properties.Resources.strNewPswDoNotMatch, "Passwords don't match!",
					MessageBoxButtons.OK, MessageBoxIcon.Error);
				this.AcceptButton = null;
				confirmField.Focus();
				return;
			}

			// If we get here, then its ok to dispose of the dialog and set DialogResult to OK
			DialogResult = DialogResult.OK;
			Close();

		}

		public System.String getPass()
		{
			return passField.Text;
		}

		private void passField_TextChanged(object sender, System.EventArgs e)
		{
			checkEffectiveBitSize(passField.TextLength);
		}

		private void checkEffectiveBitSize(int passSize)
		{
			int charSet = 0;
			string passStrength = "";

			charSet = getCharSetUsed(passField.Text);

			double result = Math.Log(Math.Pow(charSet, passSize)) / Math.Log(2);

			if (result <= 32) {
				passStrength = "weak;";
			} else if (result <= 64) {
				passStrength = "mediocre;";
			} else if (result <= 128) {
				passStrength = "OK;";
			} else if (result > 128) {
				passStrength = "great;";
			}

			entropyStatus.Text = "Your password is " + passStrength +
				" it is equivalent to a " + Math.Round(result, 0) + "-bit key.";

		}

		private int getCharSetUsed(string pass)
		{
			int ret = 0;

			if (containsNumbers(pass)) 
				ret += 10;
			if (containsLowerCaseChars(pass)) 
				ret += 26;
			if (containsUpperCaseChars(pass)) 
				ret += 26;
			if (containsPunctuation(pass)) 
				ret += 31;
			return ret;
		}

		private bool containsNumbers(string str)
		{
			Regex pattern = new Regex(@"[\d]");
			return pattern.IsMatch(str);
		}

		private bool containsLowerCaseChars(string str)
		{
			Regex pattern = new Regex("[a-z]");
			return pattern.IsMatch(str);
		}

		private bool containsUpperCaseChars(string str)
		{
			Regex pattern = new Regex("[A-Z]");
			return pattern.IsMatch(str);
		}

		private bool containsPunctuation(string str)
		{
			// regular expression include _ as a valid char for alphanumeric.. 
			// so we need to explicity state that its considered punctuation.
			Regex pattern = new Regex(@"[\W|_]");
			return pattern.IsMatch(str);
		}

		private void frmNewPsw_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				ok_Click(null, null);
		}

		private void frmNewPsw_Load(object sender, EventArgs e)
		{
			this.Text = Properties.Resources.strNewPswFormText;
			lb3.Text = Properties.Resources.strNewPassword;
			lb4.Text = Properties.Resources.strConfirm;
			btCancel.Text = Properties.Resources.btCancel;
			btOk.Text = Properties.Resources.btApply;
		}
	}

}
