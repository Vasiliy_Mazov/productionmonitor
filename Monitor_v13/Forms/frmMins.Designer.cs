﻿namespace ProductionMonitor
{
	partial class frmMins
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this._btOk = new System.Windows.Forms.Button();
			this._btCopy = new System.Windows.Forms.Button();
			this._btCancel = new System.Windows.Forms.Button();
			this._dgv = new System.Windows.Forms.DataGridView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._dgv)).BeginInit();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this._btOk);
			this.panel1.Controls.Add(this._btCopy);
			this.panel1.Controls.Add(this._btCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(5, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(334, 34);
			this.panel1.TabIndex = 0;
			// 
			// _btOk
			// 
			this._btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._btOk.Location = new System.Drawing.Point(259, 10);
			this._btOk.Name = "_btOk";
			this._btOk.Size = new System.Drawing.Size(75, 23);
			this._btOk.TabIndex = 0;
			this._btOk.Text = "Ok";
			this._btOk.UseVisualStyleBackColor = true;
			// 
			// _btCopy
			// 
			this._btCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btCopy.Location = new System.Drawing.Point(0, 10);
			this._btCopy.Name = "_btCopy";
			this._btCopy.Size = new System.Drawing.Size(146, 23);
			this._btCopy.TabIndex = 0;
			this._btCopy.Text = "Cancel";
			this._btCopy.UseVisualStyleBackColor = true;
			this._btCopy.Click += new System.EventHandler(this._btCopy_Click);
			// 
			// _btCancel
			// 
			this._btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btCancel.Location = new System.Drawing.Point(178, 10);
			this._btCancel.Name = "_btCancel";
			this._btCancel.Size = new System.Drawing.Size(75, 23);
			this._btCancel.TabIndex = 0;
			this._btCancel.Text = "Cancel";
			this._btCancel.UseVisualStyleBackColor = true;
			this._btCancel.Visible = false;
			// 
			// _dgv
			// 
			this._dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this._dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dgv.Dock = System.Windows.Forms.DockStyle.Fill;
			this._dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this._dgv.EnableHeadersVisualStyles = false;
			this._dgv.Location = new System.Drawing.Point(1, 1);
			this._dgv.Name = "_dgv";
			this._dgv.RowTemplate.Height = 18;
			this._dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this._dgv.Size = new System.Drawing.Size(332, 448);
			this._dgv.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
			this.panel2.Controls.Add(this._dgv);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(5, 5);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(1);
			this.panel2.Size = new System.Drawing.Size(334, 450);
			this.panel2.TabIndex = 2;
			// 
			// frmMins
			// 
			this.AcceptButton = this._btOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._btCancel;
			this.ClientSize = new System.Drawing.Size(344, 494);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "frmMins";
			this.Padding = new System.Windows.Forms.Padding(5);
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Detail view";
			this.Load += new System.EventHandler(this.frmMins_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this._dgv)).EndInit();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.DataGridView _dgv;
		private System.Windows.Forms.Button _btOk;
		private System.Windows.Forms.Button _btCancel;
		private System.Windows.Forms.Button _btCopy;
	}
}