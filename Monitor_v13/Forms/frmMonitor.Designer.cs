﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProductionMonitor
{
	public partial class frmMonitor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMonitor));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tpCounters = new System.Windows.Forms.TabPage();
            this.pnMaps_counts = new System.Windows.Forms.Panel();
            this.cbBigChartCounter = new System.Windows.Forms.CheckBox();
            this.cbShowTipsCounter = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tpDetail = new System.Windows.Forms.TabPage();
            this.tsDet = new System.Windows.Forms.ToolStrip();
            this.tsbDetDwDate = new System.Windows.Forms.ToolStripButton();
            this.tsbDetDw = new System.Windows.Forms.ToolStripButton();
            this.tsbDetProdExt = new System.Windows.Forms.ToolStripButton();
            this.tsbDetProdShort = new System.Windows.Forms.ToolStripButton();
            this.tsbDetFromTo = new System.Windows.Forms.ToolStripButton();
            this.tsbDetFromToPvt = new System.Windows.Forms.ToolStripButton();
            this.tpMap = new System.Windows.Forms.TabPage();
            this.pnMaps = new System.Windows.Forms.Panel();
            this.pnEdit = new System.Windows.Forms.Panel();
            this.tabEdit = new System.Windows.Forms.TabControl();
            this.tpEditDnTms = new System.Windows.Forms.TabPage();
            this.dgvDnTms = new ProductionMonitor.DataGridViewR();
            this.tpEditProds = new System.Windows.Forms.TabPage();
            this.dgvProds = new ProductionMonitor.DataGridViewR();
            this.tpEditShifts = new System.Windows.Forms.TabPage();
            this.dgvShifts = new ProductionMonitor.DataGridViewR();
            this.tsEdit = new System.Windows.Forms.ToolStrip();
            this.tsbEditUndoAll = new System.Windows.Forms.ToolStripButton();
            this.tsbEditUndo = new System.Windows.Forms.ToolStripButton();
            this.tsbEditRedo = new System.Windows.Forms.ToolStripButton();
            this.tsbEditDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbEditSave = new System.Windows.Forms.ToolStripButton();
            this.tpTrends = new System.Windows.Forms.TabPage();
            this.pnTrends = new System.Windows.Forms.Panel();
            this.tsTrends = new System.Windows.Forms.ToolStrip();
            this.tsbTrendsDays = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsWeek = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsMonths = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsQuarters = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsYears = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsBrands = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsFormats = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsShifts = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsPrint = new System.Windows.Forms.ToolStripButton();
            this.tsbTrendsAddToPrint = new System.Windows.Forms.ToolStripButton();
            this.tpLosses = new System.Windows.Forms.TabPage();
            this.pnLosses = new System.Windows.Forms.Panel();
            this.tsLosses = new System.Windows.Forms.ToolStrip();
            this.tscbLossesBaseTime = new System.Windows.Forms.ToolStripComboBox();
            this.tsbLossesPrint = new System.Windows.Forms.ToolStripButton();
            this.tsbLossesAddToPrint = new System.Windows.Forms.ToolStripButton();
            this.tpAnalyse = new System.Windows.Forms.TabPage();
            this.tpCounters_kpi = new System.Windows.Forms.TabPage();
            this.tC_Reports_counter = new System.Windows.Forms.TabControl();
            this.tP_CounterRep1 = new System.Windows.Forms.TabPage();
            this.c1FlexGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.ds_counters = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tP_CounterRep2 = new System.Windows.Forms.TabPage();
            this.c1FlexGrid2 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cmDetailed = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmLinesGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmTrendsChart = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmMap = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tmActivity = new System.Windows.Forms.Timer(this.components);
            this._tmLoading = new System.Windows.Forms.Timer(this.components);
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tssbMainDateLineSelector = new System.Windows.Forms.ToolStripSplitButton();
            this.tsbMainPrev = new System.Windows.Forms.ToolStripButton();
            this.tsbMainNext = new System.Windows.Forms.ToolStripButton();
            this.tsbMainNow = new System.Windows.Forms.ToolStripButton();
            this.tsbMainRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbMainHideFilters = new System.Windows.Forms.ToolStripButton();
            this.tscbMainShifts = new System.Windows.Forms.ToolStripComboBox();
            this.tscbMainFormats = new System.Windows.Forms.ToolStripComboBox();
            this.tscbMainBrands = new System.Windows.Forms.ToolStripComboBox();
            this.tsbMainFiltReset = new System.Windows.Forms.ToolStripButton();
            this.tsbMainRefreshFailed = new System.Windows.Forms.ToolStripButton();
            this.tsMainSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.tsbEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.tsbBigChartCounter = new System.Windows.Forms.ToolStripButton();
            this.tsbShowTipsCounter = new System.Windows.Forms.ToolStripButton();
            this.tsbShowLimitsCounter = new System.Windows.Forms.ToolStripButton();
            this.pnLegend = new System.Windows.Forms.FlowLayoutPanel();
            this.btHideLegend = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.cmTrendsGrig = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pnLines = new System.Windows.Forms.Panel();
            this.splitter1 = new System.Windows.Forms.Button();
            this.sb = new System.Windows.Forms.StatusStrip();
            this.sbpLine = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpMins = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpBrand = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpDesc = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.cmMapCounter = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tm_counter = new System.Windows.Forms.Timer(this.components);
            this.Tt = new ProductionMonitor.ToolTipMemo(this.components);
            this.tabMain.SuspendLayout();
            this.tpCounters.SuspendLayout();
            this.pnMaps_counts.SuspendLayout();
            this.tpDetail.SuspendLayout();
            this.tsDet.SuspendLayout();
            this.tpMap.SuspendLayout();
            this.pnMaps.SuspendLayout();
            this.pnEdit.SuspendLayout();
            this.tabEdit.SuspendLayout();
            this.tpEditDnTms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDnTms)).BeginInit();
            this.tpEditProds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProds)).BeginInit();
            this.tpEditShifts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShifts)).BeginInit();
            this.tsEdit.SuspendLayout();
            this.tpTrends.SuspendLayout();
            this.tsTrends.SuspendLayout();
            this.tpLosses.SuspendLayout();
            this.tsLosses.SuspendLayout();
            this.tpCounters_kpi.SuspendLayout();
            this.tC_Reports_counter.SuspendLayout();
            this.tP_CounterRep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_counters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            this.panel3.SuspendLayout();
            this.tP_CounterRep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).BeginInit();
            this.panel1.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.pnLegend.SuspendLayout();
            this.sb.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tpCounters);
            this.tabMain.Controls.Add(this.tpDetail);
            this.tabMain.Controls.Add(this.tpMap);
            this.tabMain.Controls.Add(this.tpTrends);
            this.tabMain.Controls.Add(this.tpLosses);
            this.tabMain.Controls.Add(this.tpAnalyse);
            this.tabMain.Controls.Add(this.tpCounters_kpi);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.tabMain.ItemSize = new System.Drawing.Size(120, 18);
            this.tabMain.Location = new System.Drawing.Point(0, 213);
            this.tabMain.Multiline = true;
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Drawing.Point(15, 3);
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(846, 216);
            this.tabMain.TabIndex = 1;
            this.tabMain.Visible = false;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabs_SelectedIndexChanged);
            // 
            // tpCounters
            // 
            this.tpCounters.BackColor = System.Drawing.Color.White;
            this.tpCounters.Controls.Add(this.pnMaps_counts);
            this.tpCounters.Location = new System.Drawing.Point(4, 22);
            this.tpCounters.Name = "tpCounters";
            this.tpCounters.Padding = new System.Windows.Forms.Padding(3);
            this.tpCounters.Size = new System.Drawing.Size(838, 190);
            this.tpCounters.TabIndex = 6;
            this.tpCounters.Text = "Counters";
            // 
            // pnMaps_counts
            // 
            this.pnMaps_counts.AutoScroll = true;
            this.pnMaps_counts.BackColor = System.Drawing.Color.Transparent;
            this.pnMaps_counts.Controls.Add(this.cbBigChartCounter);
            this.pnMaps_counts.Controls.Add(this.cbShowTipsCounter);
            this.pnMaps_counts.Controls.Add(this.panel2);
            this.pnMaps_counts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMaps_counts.Location = new System.Drawing.Point(3, 3);
            this.pnMaps_counts.Name = "pnMaps_counts";
            this.pnMaps_counts.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.pnMaps_counts.Size = new System.Drawing.Size(832, 184);
            this.pnMaps_counts.TabIndex = 3;
            // 
            // cbBigChartCounter
            // 
            this.cbBigChartCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbBigChartCounter.AutoSize = true;
            this.cbBigChartCounter.Checked = true;
            this.cbBigChartCounter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBigChartCounter.Location = new System.Drawing.Point(611, 3);
            this.cbBigChartCounter.Name = "cbBigChartCounter";
            this.cbBigChartCounter.Size = new System.Drawing.Size(70, 17);
            this.cbBigChartCounter.TabIndex = 1;
            this.cbBigChartCounter.Text = "Big Chart";
            this.cbBigChartCounter.UseVisualStyleBackColor = true;
            this.cbBigChartCounter.Visible = false;
            this.cbBigChartCounter.CheckStateChanged += new System.EventHandler(this.cbBigChartCounter_CheckStateChanged);
            // 
            // cbShowTipsCounter
            // 
            this.cbShowTipsCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbShowTipsCounter.AutoSize = true;
            this.cbShowTipsCounter.Checked = true;
            this.cbShowTipsCounter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowTipsCounter.Location = new System.Drawing.Point(775, 3);
            this.cbShowTipsCounter.Name = "cbShowTipsCounter";
            this.cbShowTipsCounter.Size = new System.Drawing.Size(52, 17);
            this.cbShowTipsCounter.TabIndex = 2;
            this.cbShowTipsCounter.Text = "Show";
            this.cbShowTipsCounter.UseVisualStyleBackColor = true;
            this.cbShowTipsCounter.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(832, 164);
            this.panel2.TabIndex = 0;
            this.panel2.Visible = false;
            // 
            // tpDetail
            // 
            this.tpDetail.Controls.Add(this.tsDet);
            this.tpDetail.Location = new System.Drawing.Point(4, 22);
            this.tpDetail.Name = "tpDetail";
            this.tpDetail.Padding = new System.Windows.Forms.Padding(1);
            this.tpDetail.Size = new System.Drawing.Size(838, 190);
            this.tpDetail.TabIndex = 3;
            this.tpDetail.Text = "Detailed List";
            this.tpDetail.UseVisualStyleBackColor = true;
            // 
            // tsDet
            // 
            this.tsDet.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsDet.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDetDwDate,
            this.tsbDetDw,
            this.tsbDetProdExt,
            this.tsbDetProdShort,
            this.tsbDetFromTo,
            this.tsbDetFromToPvt});
            this.tsDet.Location = new System.Drawing.Point(1, 1);
            this.tsDet.Name = "tsDet";
            this.tsDet.Size = new System.Drawing.Size(836, 25);
            this.tsDet.TabIndex = 0;
            this.tsDet.Text = "tsDet";
            // 
            // tsbDetDwDate
            // 
            this.tsbDetDwDate.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetDwDate.Checked = true;
            this.tsbDetDwDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbDetDwDate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetDwDate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetDwDate.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetDwDate.Name = "tsbDetDwDate";
            this.tsbDetDwDate.Size = new System.Drawing.Size(81, 22);
            this.tsbDetDwDate.Text = "tsbDetDwDate";
            this.tsbDetDwDate.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tsbDetDw
            // 
            this.tsbDetDw.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetDw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetDw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetDw.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetDw.Name = "tsbDetDw";
            this.tsbDetDw.Size = new System.Drawing.Size(58, 22);
            this.tsbDetDw.Text = "tsbDetDw";
            this.tsbDetDw.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tsbDetProdExt
            // 
            this.tsbDetProdExt.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetProdExt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetProdExt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetProdExt.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetProdExt.Name = "tsbDetProdExt";
            this.tsbDetProdExt.Size = new System.Drawing.Size(81, 22);
            this.tsbDetProdExt.Text = "tsbDetProdExt";
            this.tsbDetProdExt.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tsbDetProdShort
            // 
            this.tsbDetProdShort.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetProdShort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetProdShort.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetProdShort.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetProdShort.Name = "tsbDetProdShort";
            this.tsbDetProdShort.Size = new System.Drawing.Size(91, 22);
            this.tsbDetProdShort.Text = "tsbDetProdShort";
            this.tsbDetProdShort.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tsbDetFromTo
            // 
            this.tsbDetFromTo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetFromTo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetFromTo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetFromTo.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetFromTo.Name = "tsbDetFromTo";
            this.tsbDetFromTo.Size = new System.Drawing.Size(79, 22);
            this.tsbDetFromTo.Text = "tsbDetFromTo";
            this.tsbDetFromTo.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tsbDetFromToPvt
            // 
            this.tsbDetFromToPvt.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbDetFromToPvt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbDetFromToPvt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetFromToPvt.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbDetFromToPvt.Name = "tsbDetFromToPvt";
            this.tsbDetFromToPvt.Size = new System.Drawing.Size(95, 22);
            this.tsbDetFromToPvt.Text = "tsbDetFromToPvt";
            this.tsbDetFromToPvt.Click += new System.EventHandler(this.tsbDetDw_Click);
            // 
            // tpMap
            // 
            this.tpMap.BackColor = System.Drawing.Color.White;
            this.tpMap.Controls.Add(this.pnMaps);
            this.tpMap.Location = new System.Drawing.Point(4, 22);
            this.tpMap.Name = "tpMap";
            this.tpMap.Size = new System.Drawing.Size(838, 190);
            this.tpMap.TabIndex = 0;
            this.tpMap.Text = "Timeline";
            this.tpMap.UseVisualStyleBackColor = true;
            // 
            // pnMaps
            // 
            this.pnMaps.AutoScroll = true;
            this.pnMaps.Controls.Add(this.pnEdit);
            this.pnMaps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMaps.Location = new System.Drawing.Point(0, 0);
            this.pnMaps.Name = "pnMaps";
            this.pnMaps.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.pnMaps.Size = new System.Drawing.Size(838, 190);
            this.pnMaps.TabIndex = 1;
            // 
            // pnEdit
            // 
            this.pnEdit.BackColor = System.Drawing.SystemColors.Control;
            this.pnEdit.Controls.Add(this.tabEdit);
            this.pnEdit.Controls.Add(this.tsEdit);
            this.pnEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnEdit.Location = new System.Drawing.Point(0, 20);
            this.pnEdit.Name = "pnEdit";
            this.pnEdit.Size = new System.Drawing.Size(838, 170);
            this.pnEdit.TabIndex = 0;
            this.pnEdit.Visible = false;
            // 
            // tabEdit
            // 
            this.tabEdit.Controls.Add(this.tpEditDnTms);
            this.tabEdit.Controls.Add(this.tpEditProds);
            this.tabEdit.Controls.Add(this.tpEditShifts);
            this.tabEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabEdit.Location = new System.Drawing.Point(0, 25);
            this.tabEdit.Name = "tabEdit";
            this.tabEdit.SelectedIndex = 0;
            this.tabEdit.Size = new System.Drawing.Size(838, 145);
            this.tabEdit.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabEdit.TabIndex = 3;
            this.tabEdit.SelectedIndexChanged += new System.EventHandler(this.tabEdit_SelectedIndexChanged);
            // 
            // tpEditDnTms
            // 
            this.tpEditDnTms.Controls.Add(this.dgvDnTms);
            this.tpEditDnTms.Location = new System.Drawing.Point(4, 22);
            this.tpEditDnTms.Name = "tpEditDnTms";
            this.tpEditDnTms.Padding = new System.Windows.Forms.Padding(1);
            this.tpEditDnTms.Size = new System.Drawing.Size(830, 119);
            this.tpEditDnTms.TabIndex = 0;
            this.tpEditDnTms.Text = "Downtimes";
            this.tpEditDnTms.UseVisualStyleBackColor = true;
            // 
            // dgvDnTms
            // 
            this.dgvDnTms.AllowUserToAddRows = false;
            this.dgvDnTms.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDnTms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDnTms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDnTms.Location = new System.Drawing.Point(1, 1);
            this.dgvDnTms.MultiSelect = false;
            this.dgvDnTms.Name = "dgvDnTms";
            this.dgvDnTms.ShowCellToolTips = false;
            this.dgvDnTms.Size = new System.Drawing.Size(828, 117);
            this.dgvDnTms.TabIndex = 2;
            // 
            // tpEditProds
            // 
            this.tpEditProds.Controls.Add(this.dgvProds);
            this.tpEditProds.Location = new System.Drawing.Point(4, 22);
            this.tpEditProds.Name = "tpEditProds";
            this.tpEditProds.Padding = new System.Windows.Forms.Padding(1);
            this.tpEditProds.Size = new System.Drawing.Size(830, 119);
            this.tpEditProds.TabIndex = 1;
            this.tpEditProds.Text = "Production";
            this.tpEditProds.UseVisualStyleBackColor = true;
            // 
            // dgvProds
            // 
            this.dgvProds.AllowUserToAddRows = false;
            this.dgvProds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProds.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProds.Location = new System.Drawing.Point(1, 1);
            this.dgvProds.MultiSelect = false;
            this.dgvProds.Name = "dgvProds";
            this.dgvProds.Size = new System.Drawing.Size(828, 117);
            this.dgvProds.TabIndex = 1;
            // 
            // tpEditShifts
            // 
            this.tpEditShifts.Controls.Add(this.dgvShifts);
            this.tpEditShifts.Location = new System.Drawing.Point(4, 22);
            this.tpEditShifts.Name = "tpEditShifts";
            this.tpEditShifts.Padding = new System.Windows.Forms.Padding(1);
            this.tpEditShifts.Size = new System.Drawing.Size(830, 119);
            this.tpEditShifts.TabIndex = 2;
            this.tpEditShifts.Text = "Shifts";
            this.tpEditShifts.UseVisualStyleBackColor = true;
            // 
            // dgvShifts
            // 
            this.dgvShifts.AllowUserToAddRows = false;
            this.dgvShifts.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvShifts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShifts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvShifts.Location = new System.Drawing.Point(1, 1);
            this.dgvShifts.MultiSelect = false;
            this.dgvShifts.Name = "dgvShifts";
            this.dgvShifts.Size = new System.Drawing.Size(828, 117);
            this.dgvShifts.TabIndex = 2;
            // 
            // tsEdit
            // 
            this.tsEdit.CausesValidation = true;
            this.tsEdit.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbEditUndoAll,
            this.tsbEditUndo,
            this.tsbEditRedo,
            this.tsbEditDelete,
            this.tsbEditSave});
            this.tsEdit.Location = new System.Drawing.Point(0, 0);
            this.tsEdit.Name = "tsEdit";
            this.tsEdit.Size = new System.Drawing.Size(838, 25);
            this.tsEdit.TabIndex = 0;
            this.tsEdit.Text = "toolStrip1";
            // 
            // tsbEditUndoAll
            // 
            this.tsbEditUndoAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditUndoAll.Enabled = false;
            this.tsbEditUndoAll.Image = global::ProductionMonitor.Properties.Resources.undo_all;
            this.tsbEditUndoAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditUndoAll.Name = "tsbEditUndoAll";
            this.tsbEditUndoAll.Size = new System.Drawing.Size(23, 22);
            this.tsbEditUndoAll.Text = "Undo all";
            this.tsbEditUndoAll.Click += new System.EventHandler(this.btUndoAll_Click);
            // 
            // tsbEditUndo
            // 
            this.tsbEditUndo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditUndo.Enabled = false;
            this.tsbEditUndo.Image = global::ProductionMonitor.Properties.Resources.undo;
            this.tsbEditUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditUndo.Name = "tsbEditUndo";
            this.tsbEditUndo.Size = new System.Drawing.Size(23, 22);
            this.tsbEditUndo.Text = "Undo";
            this.tsbEditUndo.Click += new System.EventHandler(this.btUndo_Click);
            // 
            // tsbEditRedo
            // 
            this.tsbEditRedo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditRedo.Enabled = false;
            this.tsbEditRedo.Image = global::ProductionMonitor.Properties.Resources.redo;
            this.tsbEditRedo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditRedo.Name = "tsbEditRedo";
            this.tsbEditRedo.Size = new System.Drawing.Size(23, 22);
            this.tsbEditRedo.Text = "Redo";
            this.tsbEditRedo.Click += new System.EventHandler(this.btRedo_Click);
            // 
            // tsbEditDelete
            // 
            this.tsbEditDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditDelete.Enabled = false;
            this.tsbEditDelete.Image = global::ProductionMonitor.Properties.Resources.delete;
            this.tsbEditDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditDelete.Name = "tsbEditDelete";
            this.tsbEditDelete.Size = new System.Drawing.Size(23, 22);
            this.tsbEditDelete.Text = "Delete";
            this.tsbEditDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // tsbEditSave
            // 
            this.tsbEditSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEditSave.Enabled = false;
            this.tsbEditSave.Image = global::ProductionMonitor.Properties.Resources.save;
            this.tsbEditSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditSave.Name = "tsbEditSave";
            this.tsbEditSave.Size = new System.Drawing.Size(23, 22);
            this.tsbEditSave.Text = "Save";
            this.tsbEditSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // tpTrends
            // 
            this.tpTrends.AutoScroll = true;
            this.tpTrends.Controls.Add(this.pnTrends);
            this.tpTrends.Controls.Add(this.tsTrends);
            this.tpTrends.Location = new System.Drawing.Point(4, 22);
            this.tpTrends.Name = "tpTrends";
            this.tpTrends.Size = new System.Drawing.Size(838, 190);
            this.tpTrends.TabIndex = 1;
            this.tpTrends.Text = "Trends";
            this.tpTrends.UseVisualStyleBackColor = true;
            // 
            // pnTrends
            // 
            this.pnTrends.AutoScroll = true;
            this.pnTrends.AutoScrollMinSize = new System.Drawing.Size(400, 400);
            this.pnTrends.BackColor = System.Drawing.Color.White;
            this.pnTrends.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnTrends.Location = new System.Drawing.Point(0, 25);
            this.pnTrends.Name = "pnTrends";
            this.pnTrends.Size = new System.Drawing.Size(838, 165);
            this.pnTrends.TabIndex = 0;
            // 
            // tsTrends
            // 
            this.tsTrends.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsTrends.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbTrendsDays,
            this.tsbTrendsWeek,
            this.tsbTrendsMonths,
            this.tsbTrendsQuarters,
            this.tsbTrendsYears,
            this.tsbTrendsBrands,
            this.tsbTrendsFormats,
            this.tsbTrendsShifts,
            this.tsbTrendsPrint,
            this.tsbTrendsAddToPrint});
            this.tsTrends.Location = new System.Drawing.Point(0, 0);
            this.tsTrends.Name = "tsTrends";
            this.tsTrends.Size = new System.Drawing.Size(838, 25);
            this.tsTrends.TabIndex = 2;
            this.tsTrends.Text = "Trends";
            // 
            // tsbTrendsDays
            // 
            this.tsbTrendsDays.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsDays.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsDays.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsDays.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsDays.Name = "tsbTrendsDays";
            this.tsbTrendsDays.Size = new System.Drawing.Size(83, 22);
            this.tsbTrendsDays.Text = "tsbTrendsDays";
            this.tsbTrendsDays.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsWeek
            // 
            this.tsbTrendsWeek.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsWeek.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsWeek.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsWeek.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsWeek.Name = "tsbTrendsWeek";
            this.tsbTrendsWeek.Size = new System.Drawing.Size(43, 22);
            this.tsbTrendsWeek.Text = "Weeks";
            this.tsbTrendsWeek.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsMonths
            // 
            this.tsbTrendsMonths.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsMonths.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsMonths.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsMonths.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsMonths.Name = "tsbTrendsMonths";
            this.tsbTrendsMonths.Size = new System.Drawing.Size(46, 22);
            this.tsbTrendsMonths.Text = "Months";
            this.tsbTrendsMonths.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsQuarters
            // 
            this.tsbTrendsQuarters.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsQuarters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsQuarters.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsQuarters.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsQuarters.Name = "tsbTrendsQuarters";
            this.tsbTrendsQuarters.Size = new System.Drawing.Size(54, 22);
            this.tsbTrendsQuarters.Text = "Quarters";
            this.tsbTrendsQuarters.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsYears
            // 
            this.tsbTrendsYears.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsYears.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsYears.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsYears.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsYears.Name = "tsbTrendsYears";
            this.tsbTrendsYears.Size = new System.Drawing.Size(38, 22);
            this.tsbTrendsYears.Text = "Years";
            this.tsbTrendsYears.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsBrands
            // 
            this.tsbTrendsBrands.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsBrands.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsBrands.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsBrands.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsBrands.Name = "tsbTrendsBrands";
            this.tsbTrendsBrands.Size = new System.Drawing.Size(44, 22);
            this.tsbTrendsBrands.Text = "Brands";
            this.tsbTrendsBrands.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsFormats
            // 
            this.tsbTrendsFormats.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsFormats.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsFormats.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tsbTrendsFormats.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsFormats.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsFormats.Name = "tsbTrendsFormats";
            this.tsbTrendsFormats.Size = new System.Drawing.Size(50, 22);
            this.tsbTrendsFormats.Text = "Formats";
            this.tsbTrendsFormats.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsShifts
            // 
            this.tsbTrendsShifts.BackColor = System.Drawing.SystemColors.ControlDark;
            this.tsbTrendsShifts.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbTrendsShifts.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsShifts.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbTrendsShifts.Name = "tsbTrendsShifts";
            this.tsbTrendsShifts.Size = new System.Drawing.Size(38, 22);
            this.tsbTrendsShifts.Text = "Shifts";
            this.tsbTrendsShifts.Click += new System.EventHandler(this.tsbTrendsMode_Click);
            // 
            // tsbTrendsPrint
            // 
            this.tsbTrendsPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbTrendsPrint.Image = global::ProductionMonitor.Properties.Resources.report;
            this.tsbTrendsPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsPrint.Name = "tsbTrendsPrint";
            this.tsbTrendsPrint.Size = new System.Drawing.Size(60, 22);
            this.tsbTrendsPrint.Text = "Report";
            this.tsbTrendsPrint.Click += new System.EventHandler(this.tsbTrendsPrint_Click);
            // 
            // tsbTrendsAddToPrint
            // 
            this.tsbTrendsAddToPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbTrendsAddToPrint.Image = global::ProductionMonitor.Properties.Resources.report_add;
            this.tsbTrendsAddToPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTrendsAddToPrint.Name = "tsbTrendsAddToPrint";
            this.tsbTrendsAddToPrint.Size = new System.Drawing.Size(46, 22);
            this.tsbTrendsAddToPrint.Text = "Add";
            this.tsbTrendsAddToPrint.Click += new System.EventHandler(this.tsbTrendsAddToPrint_Click);
            // 
            // tpLosses
            // 
            this.tpLosses.AutoScroll = true;
            this.tpLosses.Controls.Add(this.pnLosses);
            this.tpLosses.Controls.Add(this.tsLosses);
            this.tpLosses.Location = new System.Drawing.Point(4, 22);
            this.tpLosses.Name = "tpLosses";
            this.tpLosses.Size = new System.Drawing.Size(838, 190);
            this.tpLosses.TabIndex = 1;
            this.tpLosses.Text = "Losses Deployment";
            this.tpLosses.UseVisualStyleBackColor = true;
            // 
            // pnLosses
            // 
            this.pnLosses.AutoScroll = true;
            this.pnLosses.AutoScrollMinSize = new System.Drawing.Size(400, 400);
            this.pnLosses.BackColor = System.Drawing.Color.White;
            this.pnLosses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnLosses.Location = new System.Drawing.Point(0, 25);
            this.pnLosses.Name = "pnLosses";
            this.pnLosses.Size = new System.Drawing.Size(838, 165);
            this.pnLosses.TabIndex = 0;
            // 
            // tsLosses
            // 
            this.tsLosses.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsLosses.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tscbLossesBaseTime,
            this.tsbLossesPrint,
            this.tsbLossesAddToPrint});
            this.tsLosses.Location = new System.Drawing.Point(0, 0);
            this.tsLosses.Name = "tsLosses";
            this.tsLosses.Size = new System.Drawing.Size(838, 25);
            this.tsLosses.TabIndex = 0;
            this.tsLosses.Text = "Reports";
            // 
            // tscbLossesBaseTime
            // 
            this.tscbLossesBaseTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tscbLossesBaseTime.Name = "tscbLossesBaseTime";
            this.tscbLossesBaseTime.Size = new System.Drawing.Size(200, 25);
            // 
            // tsbLossesPrint
            // 
            this.tsbLossesPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbLossesPrint.Image = global::ProductionMonitor.Properties.Resources.report;
            this.tsbLossesPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLossesPrint.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbLossesPrint.Name = "tsbLossesPrint";
            this.tsbLossesPrint.Size = new System.Drawing.Size(82, 22);
            this.tsbLossesPrint.Text = "Short stops";
            this.tsbLossesPrint.Click += new System.EventHandler(this.tsbLossesPrint_Click);
            // 
            // tsbLossesAddToPrint
            // 
            this.tsbLossesAddToPrint.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbLossesAddToPrint.CheckOnClick = true;
            this.tsbLossesAddToPrint.Image = global::ProductionMonitor.Properties.Resources.report_add;
            this.tsbLossesAddToPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLossesAddToPrint.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.tsbLossesAddToPrint.Name = "tsbLossesAddToPrint";
            this.tsbLossesAddToPrint.Size = new System.Drawing.Size(92, 22);
            this.tsbLossesAddToPrint.Text = "Add to report";
            this.tsbLossesAddToPrint.Click += new System.EventHandler(this.tsbLossesAddToPrint_Click);
            // 
            // tpAnalyse
            // 
            this.tpAnalyse.Location = new System.Drawing.Point(4, 22);
            this.tpAnalyse.Name = "tpAnalyse";
            this.tpAnalyse.Size = new System.Drawing.Size(838, 190);
            this.tpAnalyse.TabIndex = 5;
            this.tpAnalyse.Text = "KPI Analysis";
            this.tpAnalyse.UseVisualStyleBackColor = true;
            // 
            // tpCounters_kpi
            // 
            this.tpCounters_kpi.Controls.Add(this.tC_Reports_counter);
            this.tpCounters_kpi.Location = new System.Drawing.Point(4, 22);
            this.tpCounters_kpi.Name = "tpCounters_kpi";
            this.tpCounters_kpi.Padding = new System.Windows.Forms.Padding(3);
            this.tpCounters_kpi.Size = new System.Drawing.Size(838, 190);
            this.tpCounters_kpi.TabIndex = 7;
            this.tpCounters_kpi.Text = "Counters KPI";
            this.tpCounters_kpi.UseVisualStyleBackColor = true;
            // 
            // tC_Reports_counter
            // 
            this.tC_Reports_counter.Controls.Add(this.tP_CounterRep1);
            this.tC_Reports_counter.Controls.Add(this.tP_CounterRep2);
            this.tC_Reports_counter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tC_Reports_counter.Location = new System.Drawing.Point(3, 3);
            this.tC_Reports_counter.Name = "tC_Reports_counter";
            this.tC_Reports_counter.SelectedIndex = 0;
            this.tC_Reports_counter.Size = new System.Drawing.Size(832, 184);
            this.tC_Reports_counter.TabIndex = 4;
            // 
            // tP_CounterRep1
            // 
            this.tP_CounterRep1.Controls.Add(this.c1FlexGrid1);
            this.tP_CounterRep1.Controls.Add(this.panel3);
            this.tP_CounterRep1.Location = new System.Drawing.Point(4, 22);
            this.tP_CounterRep1.Name = "tP_CounterRep1";
            this.tP_CounterRep1.Padding = new System.Windows.Forms.Padding(3);
            this.tP_CounterRep1.Size = new System.Drawing.Size(824, 158);
            this.tP_CounterRep1.TabIndex = 0;
            this.tP_CounterRep1.Text = "Report1";
            this.tP_CounterRep1.UseVisualStyleBackColor = true;
            // 
            // c1FlexGrid1
            // 
            this.c1FlexGrid1.AllowEditing = false;
            this.c1FlexGrid1.AutoResize = false;
            this.c1FlexGrid1.ColumnInfo = resources.GetString("c1FlexGrid1.ColumnInfo");
            this.c1FlexGrid1.DataMember = "Counters_kpi";
            this.c1FlexGrid1.DataSource = this.ds_counters;
            this.c1FlexGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1FlexGrid1.Location = new System.Drawing.Point(3, 25);
            this.c1FlexGrid1.Name = "c1FlexGrid1";
            this.c1FlexGrid1.Rows.Count = 1;
            this.c1FlexGrid1.Rows.DefaultSize = 18;
            this.c1FlexGrid1.Size = new System.Drawing.Size(818, 130);
            this.c1FlexGrid1.StyleInfo = resources.GetString("c1FlexGrid1.StyleInfo");
            this.c1FlexGrid1.TabIndex = 2;
            this.c1FlexGrid1.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            this.c1FlexGrid1.Click += new System.EventHandler(this.c1FlexGrid1_Click);
            // 
            // ds_counters
            // 
            this.ds_counters.DataSetName = "ds_counters";
            this.ds_counters.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dataTable1.TableName = "Counters_kpi";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Название";
            this.dataColumn1.ColumnName = "Name";
            this.dataColumn1.ReadOnly = true;
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Max";
            this.dataColumn2.ColumnName = "Max_";
            this.dataColumn2.DataType = typeof(decimal);
            this.dataColumn2.ReadOnly = true;
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Min";
            this.dataColumn3.ColumnName = "Min_";
            this.dataColumn3.DataType = typeof(decimal);
            this.dataColumn3.ReadOnly = true;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Average";
            this.dataColumn4.ColumnName = "Avg_";
            this.dataColumn4.DataType = typeof(decimal);
            this.dataColumn4.ReadOnly = true;
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "id";
            this.dataColumn5.ReadOnly = true;
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "error";
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "dt",
                        "Counters"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn15,
        this.dataColumn14};
            this.dataTable2.TableName = "Counters_rep2";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Дата";
            this.dataColumn7.ColumnName = "Date";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Время";
            this.dataColumn8.ColumnName = "Time";
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Brand";
            this.dataColumn9.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn9.ColumnName = "Brand";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn13.ColumnName = "error";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.Caption = "Наименование";
            this.dataColumn14.ColumnName = "Counters";
            // 
            // dataColumn15
            // 
            this.dataColumn15.AllowDBNull = false;
            this.dataColumn15.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn15.ColumnName = "dt";
            this.dataColumn15.DataType = typeof(System.DateTime);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(818, 22);
            this.panel3.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Excel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tP_CounterRep2
            // 
            this.tP_CounterRep2.Controls.Add(this.c1FlexGrid2);
            this.tP_CounterRep2.Controls.Add(this.panel1);
            this.tP_CounterRep2.Location = new System.Drawing.Point(4, 22);
            this.tP_CounterRep2.Name = "tP_CounterRep2";
            this.tP_CounterRep2.Padding = new System.Windows.Forms.Padding(3);
            this.tP_CounterRep2.Size = new System.Drawing.Size(824, 158);
            this.tP_CounterRep2.TabIndex = 1;
            this.tP_CounterRep2.Text = "Report2";
            this.tP_CounterRep2.UseVisualStyleBackColor = true;
            // 
            // c1FlexGrid2
            // 
            this.c1FlexGrid2.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.c1FlexGrid2.AllowEditing = false;
            this.c1FlexGrid2.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.c1FlexGrid2.AutoResize = false;
            this.c1FlexGrid2.ColumnInfo = resources.GetString("c1FlexGrid2.ColumnInfo");
            this.c1FlexGrid2.DataSource = this.ds_counters;
            this.c1FlexGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1FlexGrid2.Location = new System.Drawing.Point(3, 30);
            this.c1FlexGrid2.Name = "c1FlexGrid2";
            this.c1FlexGrid2.Rows.Count = 2;
            this.c1FlexGrid2.Rows.DefaultSize = 18;
            this.c1FlexGrid2.Size = new System.Drawing.Size(818, 125);
            this.c1FlexGrid2.StyleInfo = resources.GetString("c1FlexGrid2.StyleInfo");
            this.c1FlexGrid2.TabIndex = 4;
            this.c1FlexGrid2.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(818, 27);
            this.panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(180, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(84, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Excel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Собрать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmDetailed
            // 
            this.cmDetailed.Name = "cmGrid";
            this.cmDetailed.Size = new System.Drawing.Size(61, 4);
            // 
            // cmLinesGrid
            // 
            this.cmLinesGrid.Name = "cmLines";
            this.cmLinesGrid.Size = new System.Drawing.Size(61, 4);
            // 
            // cmTrendsChart
            // 
            this.cmTrendsChart.Name = "cmTrendsChart";
            this.cmTrendsChart.Size = new System.Drawing.Size(61, 4);
            // 
            // cmMap
            // 
            this.cmMap.Name = "cmMap";
            this.cmMap.Size = new System.Drawing.Size(61, 4);
            this.cmMap.Opening += new System.ComponentModel.CancelEventHandler(this.cmMap_Opening);
            this.cmMap.MouseLeave += new System.EventHandler(this.cmMap_MouseLeave);
            // 
            // _tmActivity
            // 
            this._tmActivity.Interval = 60000;
            this._tmActivity.Tick += new System.EventHandler(this.tmActivity_Tick);
            // 
            // _tmLoading
            // 
            this._tmLoading.Tick += new System.EventHandler(this.tmLoading_Tick);
            // 
            // tsMain
            // 
            this.tsMain.CanOverflow = false;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssbMainDateLineSelector,
            this.tsbMainPrev,
            this.tsbMainNext,
            this.tsbMainNow,
            this.tsbMainRefresh,
            this.tsbMainHideFilters,
            this.tscbMainShifts,
            this.tscbMainFormats,
            this.tscbMainBrands,
            this.tsbMainFiltReset,
            this.tsbMainRefreshFailed,
            this.tsMainSeparator,
            this.tsbEdit,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.tsbBigChartCounter,
            this.tsbShowTipsCounter,
            this.tsbShowLimitsCounter});
            this.tsMain.Location = new System.Drawing.Point(0, 24);
            this.tsMain.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsMain.Name = "tsMain";
            this.tsMain.Padding = new System.Windows.Forms.Padding(10, 0, 1, 0);
            this.tsMain.Size = new System.Drawing.Size(846, 29);
            this.tsMain.TabIndex = 4;
            this.tsMain.Text = "tools";
            // 
            // tssbMainDateLineSelector
            // 
            this.tssbMainDateLineSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssbMainDateLineSelector.DropDownButtonWidth = 14;
            this.tssbMainDateLineSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tssbMainDateLineSelector.Margin = new System.Windows.Forms.Padding(0, 1, 10, 2);
            this.tssbMainDateLineSelector.Name = "tssbMainDateLineSelector";
            this.tssbMainDateLineSelector.Size = new System.Drawing.Size(19, 26);
            this.tssbMainDateLineSelector.DropDownOpened += new System.EventHandler(this.btDateLineSelector_DropDownOpened);
            this.tssbMainDateLineSelector.DropDownOpening += new System.EventHandler(this.btDateLineSelector_DropDownOpening);
            // 
            // tsbMainPrev
            // 
            this.tsbMainPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainPrev.Image = global::ProductionMonitor.Properties.Resources.prev;
            this.tsbMainPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainPrev.Name = "tsbMainPrev";
            this.tsbMainPrev.Size = new System.Drawing.Size(26, 26);
            this.tsbMainPrev.Text = "Previous";
            this.tsbMainPrev.ToolTipText = "to";
            // 
            // tsbMainNext
            // 
            this.tsbMainNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainNext.Image = global::ProductionMonitor.Properties.Resources.next;
            this.tsbMainNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainNext.Name = "tsbMainNext";
            this.tsbMainNext.Size = new System.Drawing.Size(26, 26);
            this.tsbMainNext.Text = "Next";
            // 
            // tsbMainNow
            // 
            this.tsbMainNow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainNow.Image = global::ProductionMonitor.Properties.Resources.first;
            this.tsbMainNow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainNow.Name = "tsbMainNow";
            this.tsbMainNow.Size = new System.Drawing.Size(26, 26);
            this.tsbMainNow.Text = "Now";
            this.tsbMainNow.Click += new System.EventHandler(this.btNow_Click);
            // 
            // tsbMainRefresh
            // 
            this.tsbMainRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainRefresh.Image = global::ProductionMonitor.Properties.Resources.refr;
            this.tsbMainRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainRefresh.Name = "tsbMainRefresh";
            this.tsbMainRefresh.Size = new System.Drawing.Size(26, 26);
            this.tsbMainRefresh.Text = "Refresh";
            this.tsbMainRefresh.ToolTipText = "Refresh";
            // 
            // tsbMainHideFilters
            // 
            this.tsbMainHideFilters.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMainHideFilters.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainHideFilters.Image = global::ProductionMonitor.Properties.Resources.hide_left;
            this.tsbMainHideFilters.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainHideFilters.Name = "tsbMainHideFilters";
            this.tsbMainHideFilters.Size = new System.Drawing.Size(26, 26);
            this.tsbMainHideFilters.Text = "‹";
            this.tsbMainHideFilters.Click += new System.EventHandler(this.btHide_Click);
            // 
            // tscbMainShifts
            // 
            this.tscbMainShifts.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tscbMainShifts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tscbMainShifts.Name = "tscbMainShifts";
            this.tscbMainShifts.Size = new System.Drawing.Size(121, 29);
            this.tscbMainShifts.Visible = false;
            // 
            // tscbMainFormats
            // 
            this.tscbMainFormats.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tscbMainFormats.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tscbMainFormats.Name = "tscbMainFormats";
            this.tscbMainFormats.Size = new System.Drawing.Size(121, 29);
            this.tscbMainFormats.Visible = false;
            // 
            // tscbMainBrands
            // 
            this.tscbMainBrands.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tscbMainBrands.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tscbMainBrands.Name = "tscbMainBrands";
            this.tscbMainBrands.Size = new System.Drawing.Size(121, 29);
            this.tscbMainBrands.Visible = false;
            // 
            // tsbMainFiltReset
            // 
            this.tsbMainFiltReset.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMainFiltReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbMainFiltReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainFiltReset.Name = "tsbMainFiltReset";
            this.tsbMainFiltReset.Size = new System.Drawing.Size(39, 26);
            this.tsbMainFiltReset.Text = "Reset";
            this.tsbMainFiltReset.Visible = false;
            this.tsbMainFiltReset.Click += new System.EventHandler(this.btFiltReset_Click);
            // 
            // tsbMainRefreshFailed
            // 
            this.tsbMainRefreshFailed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainRefreshFailed.Image = global::ProductionMonitor.Properties.Resources.refr_pink;
            this.tsbMainRefreshFailed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainRefreshFailed.Name = "tsbMainRefreshFailed";
            this.tsbMainRefreshFailed.Size = new System.Drawing.Size(26, 26);
            this.tsbMainRefreshFailed.Text = "Refresh failed units";
            this.tsbMainRefreshFailed.ToolTipText = "Refresh failed units";
            this.tsbMainRefreshFailed.Visible = false;
            // 
            // tsMainSeparator
            // 
            this.tsMainSeparator.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsMainSeparator.Name = "tsMainSeparator";
            this.tsMainSeparator.Size = new System.Drawing.Size(6, 29);
            // 
            // tsbEdit
            // 
            this.tsbEdit.CheckOnClick = true;
            this.tsbEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbEdit.Image = global::ProductionMonitor.Properties.Resources.edit;
            this.tsbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEdit.Name = "tsbEdit";
            this.tsbEdit.Size = new System.Drawing.Size(26, 26);
            this.tsbEdit.Text = "Edit";
            this.tsbEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(82, 26);
            this.toolStripLabel1.Text = "                         ";
            // 
            // tsbBigChartCounter
            // 
            this.tsbBigChartCounter.Checked = true;
            this.tsbBigChartCounter.CheckOnClick = true;
            this.tsbBigChartCounter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsbBigChartCounter.DoubleClickEnabled = true;
            this.tsbBigChartCounter.Image = global::ProductionMonitor.Properties.Resources.checkbox_empty;
            this.tsbBigChartCounter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBigChartCounter.Name = "tsbBigChartCounter";
            this.tsbBigChartCounter.Size = new System.Drawing.Size(75, 26);
            this.tsbBigChartCounter.Text = "Big chart";
            this.tsbBigChartCounter.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tsbShowTipsCounter
            // 
            this.tsbShowTipsCounter.CheckOnClick = true;
            this.tsbShowTipsCounter.DoubleClickEnabled = true;
            this.tsbShowTipsCounter.Image = global::ProductionMonitor.Properties.Resources.checkbox_empty;
            this.tsbShowTipsCounter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowTipsCounter.Name = "tsbShowTipsCounter";
            this.tsbShowTipsCounter.Size = new System.Drawing.Size(79, 26);
            this.tsbShowTipsCounter.Text = "Show tips";
            this.tsbShowTipsCounter.Click += new System.EventHandler(this.tsbShowTipsCounter_Click);
            // 
            // tsbShowLimitsCounter
            // 
            this.tsbShowLimitsCounter.Checked = global::ProductionMonitor.Properties.Settings.Default.ShowOverLimits;
            this.tsbShowLimitsCounter.CheckOnClick = true;
            this.tsbShowLimitsCounter.DoubleClickEnabled = true;
            this.tsbShowLimitsCounter.Image = global::ProductionMonitor.Properties.Resources.checkbox_empty;
            this.tsbShowLimitsCounter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShowLimitsCounter.Name = "tsbShowLimitsCounter";
            this.tsbShowLimitsCounter.Size = new System.Drawing.Size(110, 26);
            this.tsbShowLimitsCounter.Text = "Show over limits";
            this.tsbShowLimitsCounter.Click += new System.EventHandler(this.tsbShowLimitsCounter_Click);
            // 
            // pnLegend
            // 
            this.pnLegend.AutoSize = true;
            this.pnLegend.Controls.Add(this.btHideLegend);
            this.pnLegend.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLegend.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.pnLegend.Location = new System.Drawing.Point(0, 53);
            this.pnLegend.Name = "pnLegend";
            this.pnLegend.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.pnLegend.Size = new System.Drawing.Size(846, 21);
            this.pnLegend.TabIndex = 5;
            // 
            // btHideLegend
            // 
            this.btHideLegend.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btHideLegend.FlatAppearance.BorderSize = 0;
            this.btHideLegend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btHideLegend.Image = global::ProductionMonitor.Properties.Resources.hide_right;
            this.btHideLegend.Location = new System.Drawing.Point(824, 2);
            this.btHideLegend.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.btHideLegend.Name = "btHideLegend";
            this.btHideLegend.Size = new System.Drawing.Size(17, 17);
            this.btHideLegend.TabIndex = 0;
            this.btHideLegend.TabStop = false;
            this.btHideLegend.UseVisualStyleBackColor = true;
            this.btHideLegend.Click += new System.EventHandler(this.btHideLegend_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(846, 24);
            this.MainMenu.TabIndex = 6;
            this.MainMenu.Text = "menuStrip1";
            this.MainMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MainMenu_ItemClicked);
            // 
            // cmTrendsGrig
            // 
            this.cmTrendsGrig.Name = "cmTrendsGrig";
            this.cmTrendsGrig.Size = new System.Drawing.Size(61, 4);
            // 
            // pnLines
            // 
            this.pnLines.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLines.Location = new System.Drawing.Point(0, 74);
            this.pnLines.Name = "pnLines";
            this.pnLines.Padding = new System.Windows.Forms.Padding(3);
            this.pnLines.Size = new System.Drawing.Size(846, 134);
            this.pnLines.TabIndex = 7;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 208);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(846, 5);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            this.splitter1.Visible = false;
            // 
            // sb
            // 
            this.sb.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbpLine,
            this.sbpMins,
            this.sbpBrand,
            this.sbpDesc,
            this.toolStripProgressBar1});
            this.sb.Location = new System.Drawing.Point(0, 429);
            this.sb.Name = "sb";
            this.sb.Size = new System.Drawing.Size(846, 22);
            this.sb.SizingGrip = false;
            this.sb.TabIndex = 5;
            // 
            // sbpLine
            // 
            this.sbpLine.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.sbpLine.Name = "sbpLine";
            this.sbpLine.Size = new System.Drawing.Size(4, 17);
            // 
            // sbpMins
            // 
            this.sbpMins.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.sbpMins.Name = "sbpMins";
            this.sbpMins.Size = new System.Drawing.Size(4, 17);
            // 
            // sbpBrand
            // 
            this.sbpBrand.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.sbpBrand.Name = "sbpBrand";
            this.sbpBrand.Size = new System.Drawing.Size(4, 17);
            // 
            // sbpDesc
            // 
            this.sbpDesc.Name = "sbpDesc";
            this.sbpDesc.Size = new System.Drawing.Size(819, 17);
            this.sbpDesc.Spring = true;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
            this.toolStripProgressBar1.Step = 1;
            this.toolStripProgressBar1.Value = 1;
            this.toolStripProgressBar1.Visible = false;
            // 
            // cmMapCounter
            // 
            this.cmMapCounter.Name = "cmMap";
            this.cmMapCounter.Size = new System.Drawing.Size(61, 4);
            this.cmMapCounter.Opening += new System.ComponentModel.CancelEventHandler(this.cmMapCounter_Opening);
            // 
            // _tm_counter
            // 
            this._tm_counter.Tick += new System.EventHandler(this._tm_counter_Tick);
            // 
            // Tt
            // 
            this.Tt.AutoPopDelay = 32767;
            this.Tt.InitialDelay = 500;
            this.Tt.ReshowDelay = 100;
            this.Tt.UseAnimation = false;
            this.Tt.Popup += new System.Windows.Forms.PopupEventHandler(this.Tt_Popup);
            // 
            // frmMonitor
            // 
            this.ClientSize = new System.Drawing.Size(846, 451);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.sb);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnLines);
            this.Controls.Add(this.pnLegend);
            this.Controls.Add(this.tsMain);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "frmMonitor";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.frmMonitor_Load);
            this.SizeChanged += new System.EventHandler(this.frmMonitor_SizeChanged);
            this.Closed += new System.EventHandler(this.frmMonitor_Closed);
            this.Shown += new System.EventHandler(this.frmMonitor_Shown);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMonitor_Closing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMonitor_KeyDown);
            this.tabMain.ResumeLayout(false);
            this.tpCounters.ResumeLayout(false);
            this.pnMaps_counts.ResumeLayout(false);
            this.pnMaps_counts.PerformLayout();
            this.tpDetail.ResumeLayout(false);
            this.tpDetail.PerformLayout();
            this.tsDet.ResumeLayout(false);
            this.tsDet.PerformLayout();
            this.tpMap.ResumeLayout(false);
            this.pnMaps.ResumeLayout(false);
            this.pnEdit.ResumeLayout(false);
            this.pnEdit.PerformLayout();
            this.tabEdit.ResumeLayout(false);
            this.tpEditDnTms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDnTms)).EndInit();
            this.tpEditProds.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProds)).EndInit();
            this.tpEditShifts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShifts)).EndInit();
            this.tsEdit.ResumeLayout(false);
            this.tsEdit.PerformLayout();
            this.tpTrends.ResumeLayout(false);
            this.tpTrends.PerformLayout();
            this.tsTrends.ResumeLayout(false);
            this.tsTrends.PerformLayout();
            this.tpLosses.ResumeLayout(false);
            this.tpLosses.PerformLayout();
            this.tsLosses.ResumeLayout(false);
            this.tsLosses.PerformLayout();
            this.tpCounters_kpi.ResumeLayout(false);
            this.tC_Reports_counter.ResumeLayout(false);
            this.tP_CounterRep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds_counters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tP_CounterRep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.pnLegend.ResumeLayout(false);
            this.sb.ResumeLayout(false);
            this.sb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}


		[System.Diagnostics.DebuggerStepThrough()]
		//private void InitializeToolStrips(System.ComponentModel.ComponentResourceManager resources)
		private void InitializeToolStrips()
		{

		}


		#endregion

		private TabControl tabMain;
		private TabPage tpDetail;
		private TabPage tpMap;
		private TabPage tpTrends;
		private TabPage tpLosses;
		private TabPage tpAnalyse;
		private TabControl tabEdit;
		private TabPage tpEditDnTms;
		private TabPage tpEditProds;
		private TabPage tpEditShifts;
		private ToolStrip tsMain;
		private ToolStripSplitButton tssbMainDateLineSelector;
		private ToolStripButton tsbMainPrev;
		private ToolStripButton tsbMainNext;
		private ToolStripButton tsbMainNow;
		private ToolStripButton tsbMainRefresh;
		private ToolStripButton tsbMainRefreshFailed;
		private ToolStripButton tsbMainFiltReset;
		private ToolStripComboBox tscbMainBrands;
		private ToolStripComboBox tscbMainFormats;
		private ToolStripComboBox tscbMainShifts;
		private ToolStripButton tsbMainHideFilters;
		private ToolStripSeparator tsMainSeparator;
		private FlowLayoutPanel pnLegend;
		private Button btHideLegend;
		private MenuStrip MainMenu;
		private Panel pnLines;
		private Panel pnMaps;
		private Button splitter1;
		private StatusStrip sb;
		private ToolStripStatusLabel sbpMins;
		private ToolStripStatusLabel sbpBrand;
		private ToolStripStatusLabel sbpDesc;
		private ToolStripStatusLabel sbpLine;
		private ToolStrip tsDet;
		private ToolStripButton tsbDetDwDate;
		private ToolStripButton tsbDetDw;
		private ToolStripButton tsbDetProdExt;
		private ToolStripButton tsbDetProdShort;
		private ToolStripButton tsbDetFromTo;
		private ToolStripButton tsbDetFromToPvt;
		private ToolStrip tsEdit;
		private ToolStripButton tsbEditSave;
		private ToolStripButton tsbEditUndoAll; 
		private ToolStripButton tsbEditUndo;
		private ToolStripButton tsbEditRedo;
		private ToolStripButton tsbEditDelete;
		private ToolStripButton tsbEdit;
		private DataGridViewR dgvDnTms;
		private DataGridViewR dgvProds;
		private DataGridViewR dgvShifts;
		private ToolStrip tsTrends;
		private ToolStripButton tsbTrendsShifts;
		private ToolStripButton tsbTrendsFormats;
		private ToolStripButton tsbTrendsBrands;
		private ToolStripButton tsbTrendsWeek;
		private ToolStripButton tsbTrendsMonths;
		private ToolStripButton tsbTrendsYears;
		private ToolStripButton tsbTrendsDays;
		private ToolStripButton tsbTrendsQuarters;
		private ToolStripButton tsbTrendsPrint;
		private ToolStripButton tsbTrendsAddToPrint;
		private ToolStrip tsLosses;
		private ToolStripButton tsbLossesPrint;
		private ToolStripButton tsbLossesAddToPrint;
		private ToolStripComboBox tscbLossesBaseTime;
		private ContextMenuStrip cmDetailed, cmLinesGrid, cmTrendsGrig, cmTrendsChart, cmMap;
		private ToolTipMemo Tt;
		private System.Windows.Forms.Timer _tmActivity;
		internal System.Windows.Forms.Timer _tmLoading;

		private Panel pnEdit;
		private Panel pnLosses;
		private Panel pnTrends;
        private TabPage tpCounters;
        private Panel pnMaps_counts;
        private Panel panel2;
        private CheckBox cbShowTipsCounter;
        private CheckBox cbBigChartCounter;
        private ToolStripProgressBar toolStripProgressBar1;
        private ContextMenuStrip cmMapCounter;
        private Timer _tm_counter;
        private TabPage tpCounters_kpi;
        private Button button1;
        public DataSet ds_counters;
        private DataTable dataTable1;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton tsbBigChartCounter;
        private ToolStripButton tsbShowTipsCounter;
        private ToolStripLabel toolStripLabel1;
        private ToolStripButton tsbShowLimitsCounter;
        private Button button2;
        private TabControl tC_Reports_counter;
        private TabPage tP_CounterRep1;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid1;
        private TabPage tP_CounterRep2;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid2;
        private DataTable dataTable2;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataColumn dataColumn13;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private Panel panel1;
        private Button button3;
        private Label label1;
        private Panel panel3;
	}
}