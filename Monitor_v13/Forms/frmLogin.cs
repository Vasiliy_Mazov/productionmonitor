﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;

namespace ProductionMonitor
{
	internal class frmLogin : Form
	{

		internal frmLogin()
		{
			InitializeComponent();
		}
		internal frmLogin(string ConnectionString)
			: this()
		{
			conn = ConnectionString;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region | designer      |

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtUsr = new System.Windows.Forms.TextBox();
			this.txtPsw = new System.Windows.Forms.TextBox();
			this.btLogin = new System.Windows.Forms.Button();
			this.btChange = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btCancel = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtUsr
			// 
			this.txtUsr.Location = new System.Drawing.Point(160, 19);
			this.txtUsr.Name = "txtUsr";
			this.txtUsr.Size = new System.Drawing.Size(138, 20);
			this.txtUsr.TabIndex = 0;
			// 
			// txtPsw
			// 
			this.txtPsw.Location = new System.Drawing.Point(160, 45);
			this.txtPsw.Name = "txtPsw";
			this.txtPsw.Size = new System.Drawing.Size(138, 20);
			this.txtPsw.TabIndex = 1;
			this.txtPsw.UseSystemPasswordChar = true;
			// 
			// btLogin
			// 
			this.btLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btLogin.Location = new System.Drawing.Point(261, 83);
			this.btLogin.Name = "btLogin";
			this.btLogin.Size = new System.Drawing.Size(65, 23);
			this.btLogin.TabIndex = 2;
			this.btLogin.Text = "Login";
			this.btLogin.UseVisualStyleBackColor = true;
			this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
			// 
			// btChange
			// 
			this.btChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btChange.Location = new System.Drawing.Point(56, 83);
			this.btChange.Name = "btChange";
			this.btChange.Size = new System.Drawing.Size(113, 23);
			this.btChange.TabIndex = 3;
			this.btChange.Text = "Change password";
			this.btChange.UseVisualStyleBackColor = true;
			this.btChange.Click += new System.EventHandler(this.btLogin_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(68, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(90, 17);
			this.label1.TabIndex = 4;
			this.label1.Text = "Name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(68, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(90, 17);
			this.label2.TabIndex = 4;
			this.label2.Text = "Password:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// btCancel
			// 
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Location = new System.Drawing.Point(190, 83);
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size(65, 23);
			this.btCancel.TabIndex = 4;
			this.btCancel.Text = "Cancel";
			this.btCancel.UseVisualStyleBackColor = true;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::ProductionMonitor.Properties.Resources.Config;
			this.pictureBox1.Location = new System.Drawing.Point(30, 29);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(32, 32);
			this.pictureBox1.TabIndex = 5;
			this.pictureBox1.TabStop = false;
			// 
			// frmLogin
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size(338, 118);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btChange);
			this.Controls.Add(this.btCancel);
			this.Controls.Add(this.btLogin);
			this.Controls.Add(this.txtPsw);
			this.Controls.Add(this.txtUsr);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmLogin";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "User Validation";
			this.Load += new System.EventHandler(this.frmLogin_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmLogin_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		#region | declare       |

		private TextBox txtUsr;
		private TextBox txtPsw;
		private Button btLogin;
		private Button btChange;
		private Label label1;
		private Label label2;
		private System.ComponentModel.Container components;

		private bool waschanged;
		private DataTable dtbcash;
		private DataTable dtbuser;
		private MonitorUser user;
		private readonly string conn;
		private Button btCancel;
		private int session;
		private PictureBox pictureBox1;
		internal static string user_first_login_psw = "qwerTy";

		#endregion

		#region | backend       |
		/// <summary>
		/// Looks up the users password crypto string in the database
		/// </summary>
		/// <param name="Username"></param>
		/// <returns></returns>	
		internal static DataTable LookupUser(string Username, string connStr)
		{
			DataTable result = new DataTable();
			try {
				DateTime dt1 = DateTime.Now;
				string hash = C.GetMd5Hash((dt1.ToString("yyyyMMddHHmmss") + Username), dt1.Minute);
				const string query = "e_getuser";
				using (SqlConnection conn = new SqlConnection(connStr)) {
					conn.Open();
					using (SqlCommand cmd = new SqlCommand(query, conn) { CommandType = CommandType.StoredProcedure }) {
						cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = Username;
						cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime).Value = dt1;
						cmd.Parameters.Add("@hash", SqlDbType.Char, 32).Value = hash;
						using (SqlDataReader dr = cmd.ExecuteReader()) {
							////if (testcountrer++ < 2)
							////   throw new Exception("first time test exception");
							result.Load(dr);
						}
					}
				}
			} catch (Exception ex) {
				result = null;
				MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
			} 
			return result;
		}
		////private static int testcountrer = 0;

		internal static bool SaveUserPsw(string Username, string Psw, int Salt, string Conn)
		{
			int res = 0;
			try {
				string query = string.Format("UPDATE g_users SET Hash = '{0}', Salt= {1} WHERE Name = '{2}'", Psw, Salt, Username);
				using (SqlConnection conn = new SqlConnection(Conn)) {
					conn.Open();
					using (SqlCommand cmd = new SqlCommand(query, conn)) {
						res = cmd.ExecuteNonQuery();
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			return res > 0;
		}
		internal static bool SaveUserPsw(string Username, string Psw, int Salt, string Role, string Conn)
		{
			int res = 0;
			try {
				string query = string.Format("UPDATE g_users SET Hash = '{0}', Salt= {1}, Role = '{3}' WHERE Name = '{2}'", Psw, Salt, Username, Role);
				using (SqlConnection conn = new SqlConnection(Conn)) {
					conn.Open();
					using (SqlCommand cmd = new SqlCommand(query, conn)) {
						//throw new Exception("justfortest!!!");
						res = cmd.ExecuteNonQuery();
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			return res > 0;
		}
		#endregion

		private void btLogin_Click(object sender, EventArgs e)
		{
			// Если пользователь не ввел логин
			if (string.IsNullOrEmpty(txtUsr.Text)) {
				//Focus box before showing a message
				txtUsr.Focus();
				MessageBox.Show(Properties.Resources.msgLoginEnterName, Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Focus again afterwards, sometimes people double click message boxes and select another control accidentally
				txtUsr.Focus();
				return;
			}
			
			//OK they enter a user and pass, lets see if they can authenticate
			if (dtbuser == null || dtbuser.TableName != txtUsr.Text) {
				dtbuser = LookupUser(txtUsr.Text, conn);
				if (dtbuser == null) {
					txtUsr.Focus();
					MessageBox.Show(Properties.Resources.msgLoginFailedAttemptToRetrieve, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
					txtUsr.Focus();
					return;
				}
				// запоминаем имя пользователя чтобы повторно за ним в базу данных не лезть
				dtbuser.TableName = txtUsr.Text;
			}

			// до базы достучались, но пользователя не нашли
			if (dtbuser.Rows.Count == 0) {
				txtUsr.Focus();
				MessageBox.Show(Properties.Resources.msgLoginInvalidName, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
				txtUsr.Focus();
				return;
			}

			string dbHash = Convert.ToString(dtbuser.Rows[0]["Hash"]);
			int dbSalt = Convert.ToInt32(dtbuser.Rows[0]["Salt"]);
			int dbRole = GetRole(dtbuser.Rows[0]["Role"].ToString(), dbSalt);
			bool verified = C.VerifyMd5Hash(txtPsw.Text, dbHash, dbSalt);

			// Или это самый первый заход пользователя в систему или пользователь хочет сменить пароль
			if (C.VerifyMd5Hash(user_first_login_psw, dbHash, dbSalt) || (sender == btChange && verified)) {
				//todone: заменить проверку типа dbHash == "" на контрольную фразу с солью, чтобы не было возможности ручками в базе сбрасывать пароль
				frmNewPsw passForm = new frmNewPsw();
				if (passForm.ShowDialog(this) == DialogResult.OK) {
					// OK, do something with the password. 
					int salt = Guid.NewGuid().GetHashCode();
					dbHash = C.GetMd5Hash(passForm.getPass(), salt);

					string roleHash = C.GetMd5Hash(dbRole.ToString(), salt);
					//string roleHash = C.GetMd5Hash(100.ToString(), salt);
					if (!SaveUserPsw(txtUsr.Text, dbHash, salt, roleHash, conn))
						MessageBox.Show(Properties.Resources.msgLoginTryAnotherOneTime, Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
					waschanged = true;
				} else
					return;
				passForm.Dispose();
			}

			if (verified || waschanged) {
				if (dtbcash == null || string.Compare(dtbcash.Rows[0]["Name"].ToString(), dtbuser.Rows[0]["Name"].ToString()) != 0)
					Utils.SerializeDataObject("lastuser", dtbuser);
				//MessageBox.Show("You are secured!!!!\r\nYour session id = " + dtbuser.Rows[0]["session"]);
				session = Convert.ToInt32(dtbuser.Rows[0]["Session"]);
				user = new MonitorUser((int)dtbuser.Rows[0]["ID"], txtUsr.Text, session) { Permission = dbRole };
				DialogResult = DialogResult.OK;
				Close();
			} else {
				//You may want to use the same error message so they can't tell which field they got wrong
				txtPsw.Focus();
				MessageBox.Show(Properties.Resources.msgLoginInvalidPassword, Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtPsw.Focus();
				return;
			}
		}
		
		internal static int GetRole(string hash, int salt)
		{
			Stopwatch sw1 = Stopwatch.StartNew();
			for (int n = 0; n < 100; n++ ) {
				if (C.GetMd5Hash(n.ToString(), salt) == hash) {
					sw1.Stop();
					Debug.WriteLine("GetRole: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					return n;
				}
			}
			sw1.Stop();
			Debug.WriteLine("GetRole: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

			return 0; // -1 нельзя возвращать
		}
		private void frmLogin_Load(object sender, EventArgs e)
		{
			Text = Properties.Resources.strPswFormText;
			label1.Text = Properties.Resources.strPswName;
			label2.Text = Properties.Resources.strPassword;
			btChange.Text = Properties.Resources.strPswChange;
			btCancel.Text = Properties.Resources.btCancel;
			btLogin.Text = Properties.Resources.btApply;

			dtbcash = Utils.DeserializeDataObject("lastuser", -1) as DataTable;
			if (dtbcash == null || dtbcash.Rows.Count == 0 || !dtbcash.Columns.Contains("Name"))
				return;
			txtUsr.Text = dtbcash.Rows[0]["Name"].ToString();
			txtPsw.Select();
			Refresh();
		}
		private void frmLogin_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
				btLogin_Click(null, null);
		}

		public MonitorUser User
		{
			get { return user; }
		}
		public int Session
		{
			get { return session; }
		}

	}
}

