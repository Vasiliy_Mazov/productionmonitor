﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Res = ProductionMonitor.Properties.Resources;
using System.ComponentModel;

namespace ProductionMonitor
{



    #region | MonitorConfig_Counters class     |
    public sealed class MonitorConfig_Counters : ConfigCounters
    {
        private static MonitorConfig_Counters _instance;

        private MonitorConfig_Counters()
            : base()
        {
            MonitorUnits = new Dictionary<int, MU>();

            foreach (UnitCounters u in base.Units.Values)
            {
                if ((u.ParentID != 0 && u.Parent == null) || (u.ID < 0 && !C.TestEnvironment))
                    continue;  // сирота
                MU mu = new MU(u.ID, u.ParentID, u.LineName, u.Connection);
                mu.ParentIdKpi = u.ParentIdKpi;
                mu.ParentIdConfig = u.ParentIdConfig;
                mu.ParentIdSku = u.ParentIdSku;
                mu.ParentIdShifts = u.ParentIdShifts;
                mu.ParentIdStops = u.ParentIdStops;
                mu.Parent_Counters = u.Parent;
                mu.Level = u.Level;
                MonitorUnits.Add(u.ID, mu);
            }

            // Заполняем детей
            foreach (MU mu in MonitorUnits.Values)
                if (base.Units.ContainsKey(mu.ID))
                    foreach (UnitCounters u in base.Units[mu.ID].Childs)
                        if (MonitorUnits.ContainsKey(u.ID))
                            mu.Childs.Add(MonitorUnits[u.ID]);

            // Определяем спаренные линии
            foreach (MU mu in MonitorUnits.Values)
                if (mu.Childs.Count > 0)
                    for (int n = 1; n < mu.Childs.Count; n++)
                    {
                        if (mu.Childs[0].Kind != UnitKind.Line ||
                        mu.Childs[0].ParentIdKpi != mu.Childs[n].ParentIdKpi ||
                        mu.Childs[0].ParentIdConfig != mu.Childs[n].ParentIdConfig ||
                        mu.Childs[0].ParentIdSku != mu.Childs[n].ParentIdSku ||
                        mu.Childs[0].ParentIdShifts != mu.Childs[n].ParentIdShifts ||
                        mu.Childs[0].ParentIdStops != mu.Childs[n].ParentIdStops)
                            continue;
                        mu.IsCoupledLine = true;
                    }

            InitializeEventCounters();
        }

        private void InitializeEventCounters()
        {
            SqlConnection cn = null;
            SqlCommand cmd = null;
            try
            {
                cn = new SqlConnection(base.BaseUnitConnection + ";Connect Timeout=5");
                cmd = new SqlCommand("SELECT * FROM e_cfg ORDER BY LineId", cn);
                cn.Open();
                if (cn.State == ConnectionState.Open)
                {
                    Debug.WriteLine(cn.State.ToString());
                    using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        DataSet dts = new DataSet();
                        dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "t1" });
                        DataTable tb1 = dts.Tables["t1"];

                        DataTable tbLinesId = tb1.DefaultView.ToTable("ids", true, "LineId");
                        foreach (DataRow row in tbLinesId.Rows)
                        {
                            int id = (int)row[0];
                            MU mu = new MU(id);

                            mu.Initialize(tb1);
                            //if (mu.DbState == PlcUnit.DbRel.OwnDB)
                            //   has_unit_sql_connection = true;

                            MonitorUnits.Add(id, mu);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MakeLog(string.Format("InitializeEventCounters()  exception: {0}, DB: {1}", cn.DataSource, cn.Database));
                //MakeLog(string.Format("Exception details: {0}", ex.Message));
                //needtoinit = true;
            }
            finally
            {
                if (cn != null) cn.Dispose();
                if (cmd != null) cmd.Dispose();
            }
        }

        public static MonitorConfig_Counters Instance
        {
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        _instance = new MonitorConfig_Counters();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Configuration error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                return _instance;
            }
        }

        public IDictionary<int, MU> MonitorUnits { get; private set; }

    }
    #endregion

	


	#region | MonitorConfig class     |
	public sealed class MonitorConfig : Config
	{
		private static MonitorConfig _instance;

		private MonitorConfig()
			: base()
		{
			MonitorUnits = new Dictionary<int, MU>();

			foreach (Unit u in base.Units.Values) {
				if ((u.ParentID != 0 && u.Parent == null) || (u.ID < 0 && !C.TestEnvironment))
					continue;  // сирота
				MU mu = new MU(u.ID, u.ParentID, u.LineName, u.Connection);
				mu.ParentIdKpi = u.ParentIdKpi;
				mu.ParentIdConfig = u.ParentIdConfig;
				mu.ParentIdSku = u.ParentIdSku;
				mu.ParentIdShifts = u.ParentIdShifts;
				mu.ParentIdStops = u.ParentIdStops;
				mu.Parent = u.Parent;
				mu.Level = u.Level;
				MonitorUnits.Add(u.ID, mu);
			}
			
			// Заполняем детей
			foreach (MU mu in MonitorUnits.Values)
				if (base.Units.ContainsKey(mu.ID))
					foreach (Unit u in base.Units[mu.ID].Childs)
						if (MonitorUnits.ContainsKey(u.ID))
							mu.Childs.Add(MonitorUnits[u.ID]);

			// Определяем спаренные линии
			foreach (MU mu in MonitorUnits.Values)
				if (mu.Childs.Count > 0) 
					for (int n = 1; n < mu.Childs.Count; n++) {
						if (mu.Childs[0].Kind       != UnitKind.Line ||
						mu.Childs[0].ParentIdKpi    != mu.Childs[n].ParentIdKpi ||
						mu.Childs[0].ParentIdConfig != mu.Childs[n].ParentIdConfig ||
						mu.Childs[0].ParentIdSku    != mu.Childs[n].ParentIdSku ||
						mu.Childs[0].ParentIdShifts != mu.Childs[n].ParentIdShifts ||
						mu.Childs[0].ParentIdStops  != mu.Childs[n].ParentIdStops)
							continue;
						mu.IsCoupledLine = true;
					}

			InitializeEventCounters();
		}

		private void InitializeEventCounters()
		{
			SqlConnection cn = null;
			SqlCommand cmd = null;
			try {
				cn = new SqlConnection(base.BaseUnitConnection + ";Connect Timeout=5");
				cmd = new SqlCommand("SELECT * FROM e_cfg ORDER BY LineId", cn);
				cn.Open();
				if (cn.State == ConnectionState.Open) {
					Debug.WriteLine(cn.State.ToString());
					using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)) {
						DataSet dts = new DataSet();
						dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "t1" });
						DataTable tb1 = dts.Tables["t1"];

						DataTable tbLinesId = tb1.DefaultView.ToTable("ids", true, "LineId");
						foreach (DataRow row in tbLinesId.Rows) {
							int id = (int)row[0];
							MU mu = new MU(id);

							mu.Initialize(tb1);
							//if (mu.DbState == PlcUnit.DbRel.OwnDB)
							//   has_unit_sql_connection = true;

							MonitorUnits.Add(id, mu);
						}
					}
				}
			} catch (Exception ex) {
				//MakeLog(string.Format("InitializeEventCounters()  exception: {0}, DB: {1}", cn.DataSource, cn.Database));
				//MakeLog(string.Format("Exception details: {0}", ex.Message));
				//needtoinit = true;
			} finally {
				if (cn != null) cn.Dispose();
				if (cmd != null) cmd.Dispose();
			}
		}

		public static MonitorConfig	Instance
		{
			get
			{
				if (_instance == null) {
					try {
						_instance = new MonitorConfig();
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Configuration error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
					}
				}
				return _instance;
			}
		}

		public IDictionary<int, MU> MonitorUnits { get;	private set; }

	}
	#endregion

	

	public sealed class MU : Unit
	{
		public MU(int unit, int parentunit, string name, string conn) :
			base(unit, parentunit, name, conn) {
				Init();
			}

		public MU(int id)
			: base(id) {
				Init();
		}

		private void Init()
		{
			DownColumns = new Dictionary<int, string>();
			Stops       = new Dictionary<int, string>();
			YearKpi     = new Dictionary<string, float>();
			TgtsYears   = new Dictionary<int, Dictionary<int, double>>();
			YearsTgts   = new Dictionary<int, Dictionary<string, double>>();
			ShiftMins   = new List<int>();
		}

		public void Initialize(DataTable tb1)
		{
			try {

				if (tb1 != null) {
					LineNameFromDB = C.GetRowField(tb1, string.Format("LineId={0} AND Name='Line Name'", ID), "Value");

					int res;
					if (int.TryParse(C.GetRowField(tb1, string.Format("LineId={0} AND Name='Parent Id'", ID), "Value"), out res))
						ParentID = res;

					Kind = UnitKind.EventCounter;

				}


			} catch (Exception ex) {
				

			}
		}


		#region | declare           |

		private Dictionary<int, List<DataRow>> branchs = new Dictionary<int, List<DataRow>>();  // вспомогательная коллекция, нужна только на момент заполнения колонок D0,D1,D2...
		private int _downLevels = 1;
		private int _order = -1;
		private int _timeCorrection = 0;
		private int _minIdleTime = 5;
		private int _daysToLive = 7;
		private int _lineWeight = 1000;
		private string _error = ""; private float _lowSpeedTreshold = 0;
		private DateTime _cashValidDate = new DateTime(1, 1, 1);
		private DateTime _utcDate = _dtEmpty;
		private DateTime _yearValidDate;
		private AutoCompleteStringCollection _autoCompletes;
		private int _newId = -1000;
		public struct CurrentData
		{
			public DataRow current_gap;
			public DataRow current_downtime;
			public DataRow current_production;
			public DataRow current_shift;

			public DateTime LastMouseLocation;
			public DateTime DowntimeStart;
			public DateTime DowntimeStop;
			public DateTime ProductionStart;
			public DateTime ProductionStop;
			public DateTime ShiftStart;
			public DateTime ShiftStop;

			public double DowntimeMins;
			public double ProductionMins;
			public double ShiftMins;

			public int DowntimeID;
			public int ProductionID;
			public int ShiftID;

			public string DowntimeFullName;
			public string DowntimeComment;
			public string DowntimeMark;
			public string DowntimeInterval;
			public string DowntimeFullTime;

			public string ProductionName;
			public string ProductionInterval;
			public string ProductionGP;
			public string ProductionNP;
			public string ProductionVolume;
			public string ProductionFullTime;

			public string ShiftName;
			public string ShiftInterval;
			public string ShiftVolume;
			public string ShiftFullTime;

			public string CurrentSpeed;
			public string AverageSpeed;
			public double GapSpeed;
		}
		private CurrentData _mouseData = new CurrentData();
		private List<MU> _childs = new List<MU>();
		private bool _isCoupledLine;
		private BackgroundWorker _dbWorker;

		#endregion | declare           |


		#region | static            |

		public static int minTrendMins = 4320;
		public static int maxEditMins = minTrendMins;
		public static int maxDetailedMins = 14400;
		
		public static DataTable tbKPI;
		public static Dictionary<string, Dictionary<int, string>> marks = new Dictionary<string, Dictionary<int, string>>();
		public static Dictionary<string, string> marks_captions = new Dictionary<string, string>();
		public static int topunitlevel = 100;

		private static Dictionary<int, DataSet> _listsCash = new Dictionary<int, DataSet>();
		private static Dictionary<int, Color> _colors = new Dictionary<int, Color>();
		private static bool _useDefaultLanguage = true; 
		private readonly static DateTime _dtEmpty = new DateTime(1, 1, 1);
		private static DataTable _tbTotal;
		private static DateTime _dtStart, _dtEnd;
		private static int _mins = 1440;
		private static int _currYear;
		private bool _yearDataSetIsLoaded;

		public static void SetTopUnitLevel(MU mu)
		{
			if (topunitlevel > mu.Level)
				topunitlevel = mu.Level;
		}
		public static void ResetTopUnitLevel()
		{
			topunitlevel = 100;
		}
		public static void SetTimePeriod(int days, DateTime end)
		{
			_dtEnd = end;
			_dtStart = end.AddDays(-days);
			_mins = days * 1440;
			SetTimePeriodMode();
		}
		public static void SetTimePeriod(DateTime start, DateTime end)
		{
			_dtEnd = end;
			_dtStart = start;
			_mins = Convert.ToInt32((DtEnd.Subtract(DtStart)).TotalMinutes);
			SetTimePeriodMode();
		}
		public static void SetTimePeriod(DateTime end)
		{
			_dtEnd = end;
			_dtStart = end.AddMinutes(-Mins);
			SetTimePeriodMode();
		}
		private static void SetTimePeriodMode()
		{
			int days = (int)DtEnd.Subtract(DtStart).TotalDays;
			if (days >= 500) {
				Mode = PeriodMode.Long;
			} else if (days > 90) {
				Mode = PeriodMode.Mean;
			} else {
				Mode = PeriodMode.Short;
			}
			_currYear = _dtEnd.AddMinutes(-1).Year;
		}

		public static KPI Kpi { get; set; }
		public static DateTime DtEnd { get { return _dtEnd; } }
		public static DateTime DtStart { get { return _dtStart; } }
		public static int Mins { get { return _mins; } }
		public static MonitorConfig Config { get; set; }
        public static MonitorConfig_Counters Config_counter { get; set; }
		public static Exception Exception { get; set; }
		public static string StackTrace
		{
			get
			{
				if (Exception != null)
					return Exception.StackTrace;
				else
					return "";
			}
		}
		public static DataTable Total
		{
			get
			{
				if (_tbTotal == null) {
					_tbTotal = new DataTable("Lines");
					_tbTotal.Columns.Add("ID", typeof(int));
					_tbTotal.Columns.Add("Line", typeof(string));
				}
				return _tbTotal; 
			}
      }
		public static DataTable TotalFilt { get; set; }
		public static string TrendMode { get; set; }
		public static PeriodMode Mode { get; set; }
		public static string LoadingText { get; set; }
		public static bool UseDefaultLanguage
		{
			get { return _useDefaultLanguage; }
			set { _useDefaultLanguage = value; }
		}
		public static bool FixedDates { get; set; }
		public static bool UseMark2 { get; set; }
		public static bool UseMark1 { get; set; }
		public static bool UseTimecorrection { get; set; }
		public static bool UseWinAuthentication { get; set; }
		public static bool CashNeedToUpdate { get; set; }
		public static Dictionary<int, DataSet> ListsCash
		{
			get { return _listsCash; }
			set { _listsCash = value; }
		}
		public static Dictionary<int, Color> Colors
		{
			get {	return _colors;	}
			set
			{
				if (_colors == value)
					return;
				_colors = value;
			}
		}
		public static int CurrYear { get { return _currYear; } }
		public static int PrevYear { get { return _currYear - 1; } }

		[Flags]
		public enum PeriodMode
		{
			Days = 1,
			Weeks = 2,
			Months = 4,
			Quarters = 8,
			Years = 16,
			Short = 7,
			Mean = 14,
			Long = 28
		}
		
		#endregion | public static     |


		#region | properties        |

		/// <summary>
		/// Количество колонок DownColumns, зависит от глубины дерева простоев
		/// </summary>
		public int DownLevels
		{
			get { return DownColumns.Count; }
			set
			{
				_downLevels = value;
				if (_downLevels == 0)
					DownColumns.Clear();
			}
		}

		/// <summary>
		/// Словарь с именами колонок типа D0, D1, D2... в которых хранятся
		/// номера DownID от самого верхнего (D0) до текущего. Эти колонки
		/// добавляются на стадии связывания датасета и их количесто определяется
		/// развесистостью дерева простоев
		/// </summary>
		public IDictionary<int, string> DownColumns { get; private set; }
		/// <summary>
		/// Список простоев из таблицы StopList
		/// </summary>
		public IDictionary<int, string> Stops { get; private set; }
		/// <summary>
		/// Словарь kpi - с подузлами значений в виде словаря год - значение
		/// </summary>
		public IDictionary<int, Dictionary<int, double>> TgtsYears { get; private set; }
		/// <summary>
		/// Словарь по годам - с подузлами значений в виде словаря kpi - значение
		/// </summary>
		public IDictionary<int, Dictionary<string, double>> YearsTgts { get; private set; }
		
		public IDictionary<string, float> YearKpi	{ get; private set; }

		/// <summary>
		/// Сводные KPI по годам
		/// </summary>
		public DataSet YearsDS { get; set; }

		/// <summary>
		/// Кэш для родительского узла
		/// </summary>
		public DataSet YearsDsCash { get; set; }

		/// <summary>
		/// Основные данные
		/// </summary>
		public DataSet DataSet { get; set; }

		/// <summary>
		/// Дополнительный датасет для хранения данных о перестройках с формата на формат
		/// и с бренда на бренд - "From To Matrix"
		/// </summary>
		public DataSet FromToDS { get; set; }

		public DataSet DataCash { get; set; }

		/// <summary>
		/// Порядок в гриде
		/// </summary>
		public int L
		{
			get { return _order; }
			set { _order = value; }
		}

		/// <summary>
		/// Запрос к базе данных выполнен, результат запроса  (успешно или нет) не учитывается
		/// </summary>
		public bool IsLoaded { get; set; }

		/// <summary>
		/// Процедура связывания датасета выполнена, результат (успешно или нет) не учитывается
		/// </summary>
		public bool IsBinded { get; set; }

		/// <summary>
		/// Запрос к базе данных и процедура связывания датасета выполнены, результат (успешно или нет) не учитывается
		/// </summary>
		public bool IsCompleted
		{
			get { return IsLoaded && IsBinded; }
		}

		/// <summary>
		/// Запрос к базе данных и связывание датасета выполнены успешно
		/// либо если это корневой узел то значения детей уже посчитали
		/// </summary>
		public bool IsOk
		{
			get
			{
				if (Kind == UnitKind.Line || IsCoupledLine )
					return IsLoaded && IsBinded && _error == "";
				else if (Kind == UnitKind.Node)
					return LineRow != null && LineRow["TF"] != DBNull.Value;
				return false;
			}
		}

		/// <summary>
		/// Истина если нет своей базы данных и KPI надо вычислять по подчиненным узлам или линиям
		/// </summary>
		public bool IsNode
		{
			get { return Kind == UnitKind.Node; }
		}

		/// <summary>
		/// Истина если нет подузлов и задан Connection String
		/// </summary>
		public bool IsLine
		{
			get { return Kind == UnitKind.Line; }
		}

		public bool IsCoupledLine { 
			get { return _isCoupledLine; }
			set {
				if (value && _dbWorker == null) {
					_dbWorker = new BackgroundWorker { WorkerReportsProgress = true };
					_dbWorker.DoWork += new DoWorkEventHandler(_dbWorker_DoWork);
					_dbWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_dbWorker_RunWorkerCompleted);
				}
					_isCoupledLine = value;
				}
		}

		void _dbWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{

		}

		void _dbWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			BuildDataFromChilds();
		}

		/// <summary>
		/// Содежит последнюю ошибку возникшую в процессе загрузки данных или при последующем связывании датасета
		/// </summary>
		public string Error
		{
			get { return _error; }
			set { _error = value; }
		}

		/// <summary>
		/// UndoRedo уже привязан
		/// </summary>
		public bool InEditState { get; set; }

		/// <summary>
		/// Используется в режиме редактирования как стек изменений датасета
		/// </summary>
		public UndoRedo UndoRedo { get; set; }

		/// <summary>
		/// Означает что все BindingSource имеют свойство RaiseListChangedEvents = false
		/// и также в таблицах не возникает события RowChanged
		/// </summary>
		public bool Frozen { get; set; }

		/// <summary>
		/// Obsolete property
		/// </summary>
		public int DefaultBrand { get; set; }

		/// <summary>
		/// Obsolete property
		/// </summary>
		public int DefaultFormat { get; set; }

		/// <summary>
		/// Obsolete property
		/// </summary>
		public int DefaultShift { get; set; }

		/// <summary>
		/// Коррекция времени суток, используется в случае если учет
		/// суток начинается не в 00:00 а по времени начала смены
		/// </summary>
		public int CorrectionTime
		{
			get { return _timeCorrection; }
			set { _timeCorrection = value; }
		}

		/// <summary>
		/// Время в минутах (меньше либо равно) которое должно рассматриваться как короткий downtime
		/// </summary>
		public int MinIdleTime
		{
			get { return _minIdleTime; }
			set { _minIdleTime = value; }
		}

		/// <summary>
		/// Количество дней от текущего когда можно редактировать данные
		/// за исключением черных зон
		/// </summary>
		public int DaysToLive
		{
			get { return _daysToLive; }
			set { _daysToLive = value; }
		}

		/// <summary>
		/// Минуты от начала суток для перехода одной смены на другую.
		/// </summary>
		public IList<int> ShiftMins { get; private set;	}

		/// <summary>
		/// Время последнего редактирования списков, берется из базы данных
		/// Если присвоить значение 1,1,1  то будет запрошена валидная дата из базы, и если надо то загружены списки заново
		/// Если присвоить значение Now то будут загружены списки заново принудительно
		/// </summary>
		public DateTime CashValidDate
		{
			get
			{
#if DEBUG
				Stopwatch sw1 = Stopwatch.StartNew();
#endif

				if (_cashValidDate == _dtEmpty) {
					string strconn = Connection;  //mu.Connection.Replace("true", "SSPI");
					using (SqlConnection conn = new SqlConnection(SetConn(strconn))) {
						try {
							//Debug.WriteLine("GetCashDate Before Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
							conn.Open();
							//Debug.WriteLine("GetCashDate After Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

							SqlCommand cmd = new SqlCommand("SELECT [id], [Value] FROM c_cfg", conn)
							{
								CommandType = CommandType.Text,
								CommandTimeout = 10
							};
#if DEBUG
							//throw new Exception("Just a test ;)");
#endif
							DateTime dt;
							int lineweight;
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								using (DataTable dtb = new DataTable()) {
									dtb.Load(dr);
									if (DateTime.TryParseExact(C.GetRowField(dtb, "id=9", "Value"), "yyyyMMdd HH:mm", null, System.Globalization.DateTimeStyles.None, out dt))
										CashValidDate = dt;
									else
										CashValidDate = DateTime.Now;

									if (int.TryParse(C.GetRowField(dtb, "id=11", "Value"), out lineweight))
										LineWeight = lineweight;
									else
										LineWeight = 1000;
								}
							}
						} catch {
							_cashValidDate = DateTime.Now;
							LineWeight = 1000;
						}
					}
				}
#if (DEBUG)
				Debug.WriteLine("GetCashDate: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
				return _cashValidDate;
			}
			set { _cashValidDate = value; }
		}

		/// <summary>
		/// For consolidated KPI
		/// </summary>
		public int LineWeight
		{
			get { return _lineWeight; }
			set { _lineWeight = value; }
		}

		public AutoCompleteStringCollection AutoCompletes
		{
			get { return _autoCompletes; }
		}

		/// <summary>
		/// Скорость, ниже которой, считается за простой, т.е. на графике закрашено черным
		/// </summary>
		public float LowSpeedTreshold
		{
			get { return _lowSpeedTreshold; }
			set { _lowSpeedTreshold = value; }
		}

		/// <summary>
		///
		/// </summary>
		internal SessData SessionData { get; set; }

		/// <summary>
		/// Возвращает набор описаний по простою, производству, сменам
		/// для текущего положения указателя мыши на графике 'Timeline'
		/// </summary>
		public CurrentData MouseData
		{
			get { return _mouseData; }
		}

		/// <summary>
		/// Возвращает строку из основной таблицы 
		/// </summary>
		public DataRow LineRow
		{
			get { return C.GetRow(Total, "ID=" + L); }
		}

		public override UnitKind Kind
		{
			get
			{
				if (Connection != "")
					return UnitKind.Line;
				if (Childs.Count == 0 && Connection == "")
					return UnitKind.Counter;
				return UnitKind.Node;
			}
		}

		public int ReducedLevel
		{
			get
			{
				if (topunitlevel == 100)
					return Level;
				else
					return Level - topunitlevel;
			}
		}

		/// <summary>
		/// Время 'От' со сдвигом согласно настроек линии и программы
		/// </summary>
		public DateTime StartCorrected
		{
			get
			{
				if (FixedDates && UseTimecorrection)
					return DtStart.AddMinutes(CorrectionTime);
				else
					return DtStart;
			}
		}

		/// <summary>
		/// Время 'До' со сдвигом согласно настроек линии и программы
		/// </summary>
		public DateTime EndCorrected
		{
			get
			{
				if (FixedDates && UseTimecorrection)
					return DtEnd.AddMinutes(CorrectionTime);
				else
					return DtEnd;
			}
		}

		/// <summary>
		/// Коллекция подузлов
		/// </summary>
		public new List<MU> Childs
		{
			get { return _childs; }
		}

		public bool YearDataSetIsLoaded
		{
			get
			{
				return _yearDataSetIsLoaded;
			}
		}

		public override string Connection
		{
			get
			{
				return base.Connection;
			}
		}


		#endregion | properties        |


		#region | public functions  |

		/// <summary>
		/// Очищает флаги и ресурсы
		/// </summary>
		public void Clear()
		{
			_order = -1;
			IsLoaded = false;
			IsBinded = false;
			_error = "";
			Frozen = true;
			DataSet = null;
			FromToDS = null;
			if (UndoRedo != null) UndoRedo.Clear();
		}

		/// <summary>
		/// Загрузка текущих данных, и, при необходимости, загрузка справочников
		/// </summary>
		public void LoadData()
		{
			try {
#if DEBUG
				//Thread.Sleep(1000);
				//Thread.Sleep(250 * (this.Order % 4));
				//if (L == 1)
				//   throw new Exception("Test exception to examine program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");
#endif
				Stopwatch sw1 = Stopwatch.StartNew();
				// не знаю имеет ли это смысл:
				if (DataSet != null) DataSet.Dispose();


				LoadingText = string.Format("{0},    {1}", GetLongUnitName(false), Res.splLoadMasterData);
				LoadLists(CashNeedToUpdate);

				//Thread.Sleep(timesleep);

				LoadingText = string.Format("{0},    {1}", GetLongUnitName(false), Res.splLoadData);
				string sp = Mins <= maxDetailedMins ? "v_detailed" : "v_brief"; // mapmode // mins <= 14400
				DateTime tm1 = StartCorrected;
				DateTime tm2 = EndCorrected;

				using (SqlConnection conn = new SqlConnection(Connection)) {
					//Debug.WriteLine("LoadData Before Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					conn.Open();
					//Debug.WriteLine("LoadData After Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					SqlCommand cmd = new SqlCommand(sp, conn) { CommandTimeout = 30, CommandType = CommandType.StoredProcedure };
					cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4).Value = tm1;
					cmd.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4).Value = tm2;

					using (SqlDataReader dr = cmd.ExecuteReader()) {
						DataSet.Load(dr, LoadOption.OverwriteChanges, new string[] { "Gaps", "Stops", "Brands", "Shifts" });
					}

					#region | DataSet cash |

					if (Config.MonitorUnits[Parent.ID].IsCoupledLine) {
						Debug.WriteLine("LoadData Before DataCash: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
						if (DataCash == null) DataCash = new DataSet();
						DataCash.Clear();
						DataCash.Tables.Clear();
						DataCash.Tables.Add(DataSet.Tables["Gaps"].Copy());
						DataCash.Tables.Add(DataSet.Tables["Stops"].Copy());
						DataCash.Tables.Add(DataSet.Tables["Brands"].Copy());
						DataCash.Tables.Add(DataSet.Tables["Shifts"].Copy());
						Debug.WriteLine("LoadData After DataCash: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					}

					#endregion
				}
#if (DEBUG)
				Debug.WriteLine("LoadData: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
			} catch (SqlException ex) {
				Error = String.Format("{0}:\t{1}", ex.Number, ex.Message);
				Exception = ex;
			} catch (Exception ex) {
				Error = ex.Message;
				Exception = ex;
			} finally {
				IsLoaded = true;
				LoadingText = "";
			}
		}
        /// <summary>
        /// Загрузка текущих данных, и, при необходимости, загрузка справочников
        /// </summary>
        public void LoadData_Counters()
        {
            try
            {
#if DEBUG
                //Thread.Sleep(250 * (this.Order % 4));
                //if (L == 1)
                //   throw new Exception("Test exception to examine program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");
#endif
                Stopwatch sw1 = Stopwatch.StartNew();
                // не знаю имеет ли это смысл:
                if (DataSet != null) DataSet.Dispose();


                LoadingText = string.Format("{0},    {1}", GetLongUnitName(false), "Загр данных счетчиков");//Res.splLoadMasterData
                LoadLists_Counter(CashNeedToUpdate);

                //Thread.Sleep(timesleep);

               // LoadingText = string.Format("{0},    {1}", GetLongUnitName(false), Res.splLoadData);
                string sp = Mins <= maxDetailedMins ? "SelectCountersValueCommand" : "SelectCountersValueCommand"; // mapmode // mins <= 14400
                DateTime tm1 = StartCorrected;
                DateTime tm2 = EndCorrected;

                using (SqlConnection conn = new SqlConnection(Connection))
                {

                    //Debug.WriteLine("LoadData Before Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
                    conn.Open();
                    //Debug.WriteLine("LoadData After Open: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
                    SqlCommand cmd = new SqlCommand(sp, conn) { CommandTimeout = 30, CommandType = CommandType.StoredProcedure };
                    cmd.Parameters.Add("@id_counter", SqlDbType.Int).Value = ID;
                    cmd.Parameters.Add("@dtfrom",     SqlDbType.SmallDateTime, 4).Value = tm1;
                    cmd.Parameters.Add("@dtto",       SqlDbType.SmallDateTime, 4).Value = tm2;

                    DataTable result = new DataTable();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                       // result.TableName = "Counters_value";
                      //  result.Load(dr);
                     //   DataSet.Tables.Add(result);
                 //       DataSet.Load(dr, LoadOption.OverwriteChanges, new string[] { "Gaps", "Stops", "Brands", "Shifts" });
                        DataSet.Load(dr, LoadOption.OverwriteChanges, new string[] { "Counters_value", "Counters_kpi", "Counters_Limits", "Counters_Deviation" , "Counters_brands"});
                    }

                }
           ////////////////////////

                using (SqlConnection conn1 = new SqlConnection(Connection))
                {
                    sp = "SelectCounters";
                      conn1.Open();
                    SqlCommand cmd = new SqlCommand(sp, conn1) { CommandTimeout = 30, CommandType = CommandType.StoredProcedure };
                    cmd.Parameters.Add("@id_counter", SqlDbType.Int).Value = ID;
                    DataTable result = new DataTable();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                         DataSet.Load(dr, LoadOption.OverwriteChanges, new string[] { "Counters" });
                    }

                }

#if (DEBUG)
                Debug.WriteLine("LoadData: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
            }
            catch (SqlException ex)
            {
                Error = String.Format("{0}:\t{1}", ex.Number, ex.Message);
                Exception = ex;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                Exception = ex;
            }
            finally
            {
                IsLoaded = true;
                LoadingText = "";
            }
        }

		/// <summary>
		/// Заполнение поминутными данными с запросом к базе данных
		/// </summary>
		/// <param name="chart"></param>
		public void LoadDataMinutes()
		{
			if (DataSet.Tables.Contains("Minutes"))
				return;

			Stopwatch sw = Stopwatch.StartNew();

			string dates = string.Format("dt>=\'{0:yyyyMMdd H:mm}\' AND dt<\'{1:yyyyMMdd H:mm}\'", StartCorrected, EndCorrected);
			string strCom = string.Format("SELECT dt, bottles, seconds, gap FROM l_rozliv WHERE {0} ORDER BY dt;", dates);
			if (AutoCompletes.Count < 100)
				strCom += " SELECT DISTINCT Comment FROM l_stops WHERE (ID IN " +
				" (SELECT TOP (100) l_stops.ID ORDER BY l_stops.ID DESC));";

			using (DataSet dtsparts = new DataSet()) {
				using (SqlDataAdapter da = new SqlDataAdapter(strCom, SetConn(Connection))) {
					try {
						Cursor.Current = Cursors.WaitCursor;
						Application.DoEvents();
						da.Fill(dtsparts, "Minutes");
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception", MessageBoxButtons.OK, MessageBoxIcon.Stop);
						return;
					} finally {
						Cursor.Current = Cursors.Default;
					}
				}
				DataTable dtb = dtsparts.Tables["Minutes"];
				dtsparts.Tables.Remove(dtb);
				if (!DataSet.Tables.Contains("Minutes"))
					DataSet.Tables.Add(dtb);
				DataSet.Tables["Minutes"].AcceptChanges();
				Debug.WriteLine(string.Format("LoadChartMapMinutes_1() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
				////GetLightShorts();
				Debug.WriteLine(string.Format("LoadChartMapMinutes_2() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
				if (dtsparts.Tables.Count > 1) {
					string[] str = new string[dtsparts.Tables[1].Rows.Count];
					int cnt = 0;
					foreach (DataRow dr in dtsparts.Tables[1].Rows)
						str[cnt++] = dr[0].ToString();
					AutoCompletes.AddRange(str);
				}
			}
			Debug.WriteLine(string.Format("LoadChartMapMinutes_N() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			//Cursor = Cursors.Default;
		}

		/// <summary>
		/// Загружает все данные по годам из таблицы l_bw
		/// </summary>
		public void LoadYearData()
		{
			try {
#if (DEBUG)
				//Thread.Sleep(250 * (this.Order % 4));
				//if (L == 1)
				//   throw new Exception("Test exception from 'LoadYearData' to examine the program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");

				Stopwatch sw1 = Stopwatch.StartNew();
#endif
				LoadingText = string.Format("{0},    {1}", GetLongUnitName(false), "Loading year data");

				DateTime tm1, tm2;

				// Этот вариант грузит 10 лет
				tm1 = DateTime.Now.Date.AddDays(-DateTime.Now.Date.DayOfYear + 1).AddYears(-10);
				tm2 = tm1.AddYears(11);

				using (SqlConnection conn = new SqlConnection(Connection)) {//(SetConn(this.Connection))) {
					conn.Open();
					using (SqlCommand cmd = new SqlCommand("v_bw", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
						cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4).Value = tm1;
						cmd.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4).Value = tm2;
						using (SqlDataReader dr = cmd.ExecuteReader()) {
							YearsDS.Load(dr, LoadOption.OverwriteChanges, new string[] { "year" });
						}
						if (Config.MonitorUnits[Parent.ID].IsCoupledLine) {
							YearsDsCash = YearsDS.Copy();
						}
					}
				}
#if (DEBUG)
				Debug.WriteLine("LoadYearData: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
			} catch {
				//this.Error = ex.Message;
				//this.ex = ex;
			}
		}

		/// <summary>
		/// Генератор цветов для графиков 'Кто устранил', 'Что сделано' по номеру ID
		/// </summary>
		/// <param name="dr"></param>
		/// <returns></returns>
		public static Color GetColorMarks(DataRow dr)
		{
			int id = (int)dr["ID"];

			if (Colors.ContainsKey(id))
				return Colors[id];
			else {
				int pos;
				int[] seeds = new int[] {
				102, 102, 153,	102, 0, 102,
				205, 212, 197,	181, 95, 181,
				102, 102, 51,	51, 102, 153,
				156, 146, 115,	131, 177, 193,
				169, 182, 157,	225, 161, 121,
				153, 51,	102, 153, 102, 0,
				198, 101, 55,	102,	153, 205,
				186, 178, 157,	214, 167, 214,
				196, 99, 162,	160, 158, 181 };
				pos = Colors.Count * 3;
				if (pos >= seeds.Length) {
					pos = (Colors.Count - 18) * 3 + 1;
					if (pos + 2 >= seeds.Length) {
						pos = (Colors.Count - 35) * 3 + 2;
						if (pos + 2 >= seeds.Length)
							pos = (Colors.Count % 18) * 3;
					}
				}
				int r = seeds[pos];
				int g = seeds[pos + 1];
				int b = seeds[pos + 2];
				Color c = Color.FromArgb(r, g, b);
				Colors.Add(id, c);
				return c;
			}
		}

		/// <summary>
		/// Заполняет основной датасет недостающими таблицами, данными, связями, ограничениями
		/// </summary>
		public void BindDataset()
		{
			if (IsEmpty())
				return;
			//DataSet dataset = null;
			Stopwatch sw1 = Stopwatch.StartNew();

#if (DEBUG)
			//Thread.Sleep(1000);
			//if (this.Order % 2 > 0)	Thread.Sleep(500);
#endif
			int debugcnt = 0;
			try {
				//if (this.L == 1) throw new Exception("Test Exception #1 from 'BindDataset'!!!!");

				//NormalizeGapTb(dataset.Tables["Gaps"], "");
				if (IsLine)
					NormalizeGaps(DataSet.Tables["Gaps"]);

				Debug.WriteLine(string.Format("{0}. BindDataset_(NormalizeGapTb): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataTable tbRoot = Kpi.TableRoot.Copy();
				DataSet.Tables.Add(tbRoot);

				foreach (DataTable dtb in DataSet.Tables)
					dtb.Locale = Thread.CurrentThread.CurrentCulture;
				Debug.WriteLine(string.Format("{0}. BindDataset_(Tables locale): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				if (!YearsCheckTime() && !IsCoupledLine) {
					YearsDS = new DataSet();
					_yearDataSetIsLoaded = false;
					YearsDS.Tables.Add(DataSet.Tables["Root"].Copy());
					YearsDS.Tables.Add(DataSet.Tables["StopList"].Copy());
					YearsDS.Tables.Add(DataSet.Tables["BrandName"].Copy());
					YearsDS.Tables.Add(DataSet.Tables["FormatName"].Copy());
				}
				//dataset.Tables["Gaps"].PrimaryKey = new DataColumn[] { dataset.Tables["Gaps"].Columns["dt1"] };

				DataSet.Relations.Add("StopGap", DataSet.Tables["Stops"].Columns["ID"], DataSet.Tables["Gaps"].Columns["Stop"]);
				DataSet.Relations["StopGap"].ChildKeyConstraint.DeleteRule = Rule.SetNull;
				DataSet.Relations["StopGap"].ChildKeyConstraint.UpdateRule = Rule.Cascade;
				DataSet.Tables["Stops"].Columns.Add("dt1", typeof(DateTime), "min(child(StopGap).dt1)");
				if (DataSet.Tables["Gaps"].Columns.Contains("dt2")) DataSet.Tables["Stops"].Columns.Add("dt2", typeof(DateTime), "max(child(StopGap).dt2)");
				Debug.WriteLine(string.Format("{0}. BindDataset_(StopGap relation): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				// на самом деле NULL не разрешаются, но чтобы не возникало исключения в процессе
				// добавления новой строки уберем это ограничение
				DataSet.Tables["Stops"].Columns["DownID"].AllowDBNull = true;

				PrepareForCompareMode();
				Debug.WriteLine(string.Format("{0}. BindDataset_(PrepareForCompareMode): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				#region | Root DownID adding to StopList |

				////foreach (DataRow dr in dataset.Tables["StopList"].Rows)
				////   if (dr["TopID"] == DBNull.Value)
				////      dr["TopID"] = dr["KpiID"];

				////DataRow drn;
				////foreach (DataRow dr in dataset.Tables["Root"].Rows) {
				////   drn = dataset.Tables["StopList"].NewRow();
				////   drn["DownID"] = dr["KpiID"];
				////   drn["TopID"] = DBNull.Value;
				////   drn["KpiID"] = dr["KpiID"];
				////   drn["EName"] = dr["Ename"];
				////   drn["NameID"] = dr["NameID"];
				////   dataset.Tables["StopList"].Rows.Add(drn);
				////}

				#endregion | Root DownID adding to StopList |

				DataSet.Relations.Add("RootStopList", tbRoot.Columns["KpiID"], DataSet.Tables["StopList"].Columns["KpiID"]);
				DataSet.Relations.Add("StopListStops", DataSet.Tables["StopList"].Columns["DownID"], DataSet.Tables["Stops"].Columns["DownID"]);
				DataSet.Tables["StopList"].Columns.Add("RD", typeof(string), "Parent(RootStopList).RD");
				DataSet.Tables["StopList"].Columns.Add("FullName", typeof(string));
				DataSet.Tables["StopList"].Columns.Add("chl", typeof(int), "Count(child(StopListStops).ID)");

				// Добавляем колонку с именем которое будет равным либо полю EName либо Value
				foreach (DataTable dtb in DataSet.Tables)
					if (dtb.Columns.Contains("NameID") && dtb.Columns.Contains("Value") && !dtb.Columns.Contains("Name"))
						dtb.Columns.Add("Name", typeof(string));

				Debug.WriteLine(string.Format("{0}. BindDataset_(StopList filling): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				ChangeNameColumn();

				Debug.WriteLine(string.Format("{0}. BindDataset_(StopList filling+ChangeNameColumn): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));
				FillFullNameColumn(DataSet.Tables["StopList"]);
				Debug.WriteLine(string.Format("{0}. BindDataset_(StopList filling+FillFullNameColumn): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				if (Stops.Count == 0)
					foreach (DataRow dr in DataSet.Tables["StopList"].Rows)
						Stops.Add((int)dr["DownID"], dr["Name"].ToString());

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(StopList filling): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["Stops"].Columns.Add("RD", typeof(string), "Parent(StopListStops).RD");
				foreach (string col in DownColumns.Values)
					DataSet.Tables["Stops"].Columns.Add(col, typeof(int), "Parent(StopListStops)." + col);

				DataSet.Tables["Stops"].Columns.Add("FullName", typeof(string), "Parent(StopListStops).FullName");

				#region | PlanSpeed & SceduledSpeed |

				//////if (dataset.Tables["StopList"].Columns.Contains("PlanSpeed")) {
				//////   dataset.Tables["Stops"].Columns.Add("PlanSpeed", typeof(bool), "Parent(StopListStops).PlanSpeed");
				//////   dataset.Tables["Gaps"].Columns.Add("PlanSpeed", typeof(bool), "Parent(StopGap).PlanSpeed");
				//////}
				//////if (dataset.Tables["StopList"].Columns.Contains("ScheduleSpeed")) {
				//////   dataset.Tables["Stops"].Columns.Add("ScheduleSpeed", typeof(bool), "Parent(StopListStops).ScheduleSpeed");
				//////   dataset.Tables["Gaps"].Columns.Add("ScheduleSpeed", typeof(bool), "Parent(StopGap).ScheduleSpeed");
				//////}

				#endregion | PlanSpeed & SceduledSpeed |

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(Stops filling): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["FormatName"].Columns.Add("SpeedM", typeof(double), "Speed/60");
				DataSet.Relations.Add("FormatListBrands", DataSet.Tables["FormatName"].Columns["ID"], DataSet.Tables["Brands"].Columns["Format"]);
				DataSet.Relations.Add("BrandListBrands", DataSet.Tables["BrandName"].Columns["ID"], DataSet.Tables["Brands"].Columns["Brand"]);
				DataSet.Relations.Add("ShiftListShifts", DataSet.Tables["ShiftName"].Columns["ID"], DataSet.Tables["Shifts"].Columns["Shift"]);
				DataSet.Tables["Brands"].Columns.Add("SpeedM", typeof(double), "Parent(FormatListBrands).SpeedM");
				DataSet.Tables["Brands"].Columns.Add("Volume", typeof(float), "Parent(FormatListBrands).Volume");

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(Brands & Shifts): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["Gaps"].Columns.Add("RD", typeof(string), "Parent(StopGap).RD");
				//dataset.Tables["Gaps"].Columns.Add("Mark1", typeof(int), "Parent(StopGap).Mark1");
				//dataset.Tables["Gaps"].Columns.Add("Mark2", typeof(int), "Parent(StopGap).Mark2");
				foreach (string col in DownColumns.Values)
					DataSet.Tables["Gaps"].Columns.Add(col, typeof(int), "Parent(StopGap)." + col);
				//dataset.Tables["Gaps"].Columns.Add("Mark1", typeof(int), "Parent(StopGap).Mark1");
				DataSet.Tables["Gaps"].Columns.Add("DownID", typeof(int), "Parent(StopGap).DownID");
				DataSet.Tables["Gaps"].Columns.Add("Comment", typeof(string), "Parent(StopGap).Comment");
				DataSet.Tables["Gaps"].DefaultView.Sort = "dt1";

				#region | default values for Lists |

				DataRow[] DR;
				DR = DataSet.Tables["BrandName"].Select("", "ID");
				if (DR.Length > 0)
					DefaultBrand = (int)DR[0]["ID"];
				DR = DataSet.Tables["FormatName"].Select("", "ID");
				if (DR.Length > 0) {
					DefaultFormat = (int)DR[0]["ID"];
					LowSpeedTreshold = C.GetFloat(DR[0]["Speed"]) / (4 * 60);
				}
				DR = DataSet.Tables["ShiftName"].Select("", "ID");
				if (DR.Length > 0)
					DefaultShift = (int)DR[0]["ID"];

				#endregion | default values for Lists |


				DataSet.Relations.Add("rel", DataSet.Tables["Brands"].Columns["ID"], DataSet.Tables["Gaps"].Columns["Brands"]);
				DataSet.Relations["rel"].ChildKeyConstraint.DeleteRule = Rule.SetNull;
				DataSet.Relations["rel"].ChildKeyConstraint.UpdateRule = Rule.Cascade;
				DataSet.Relations["rel"].ChildKeyConstraint.AcceptRejectRule = AcceptRejectRule.Cascade;
				DataSet.Relations.Add("ShGaps", DataSet.Tables["Shifts"].Columns["ID"], DataSet.Tables["Gaps"].Columns["Shifts"]);
				DataSet.Relations["ShGaps"].ChildKeyConstraint.DeleteRule = Rule.SetNull;
				DataSet.Relations["ShGaps"].ChildKeyConstraint.UpdateRule = Rule.Cascade;

				DataSet.Tables["Gaps"].Columns.Add("Format", typeof(int), "Parent(rel).Format");
				DataSet.Tables["Gaps"].Columns.Add("Brand", typeof(int), "Parent(rel).Brand");
				DataSet.Tables["Gaps"].Columns.Add("Shift", typeof(int), "Parent(ShGaps).Shift");
				////DataSet.Tables["Gaps"].Columns["Format"].DefaultValue = DefaultFormat;		
				////DataSet.Tables["Gaps"].Columns["Brand"].DefaultValue = DefaultBrand;		
				////DataSet.Tables["Gaps"].Columns["Shift"].DefaultValue = DefaultShift;

				Utils.AddColumns(DataSet.Tables["Gaps"], Kpi.Expr1, "rel", "");


				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(Gaps!!!!!): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["Stops"].Columns.Add("Format", typeof(int), "max(child(StopGap).Format)");
				DataSet.Tables["Stops"].Columns.Add("Brand", typeof(int), "max(child(StopGap).Brand)");
				DataSet.Tables["Stops"].Columns.Add("Shift", typeof(int), "max(child(StopGap).Shift)");

				#region | неудачная попытка, медленно работает |

				////dataset.Tables["Stops"].Columns.Add("Format2", typeof(int));
				////dataset.Tables["Stops"].Columns.Add("Brand2", typeof(int));
				////dataset.Tables["Stops"].Columns.Add("Shift2", typeof(int));
				////
				////// вариант с перебором строк, очень медленно
				////foreach (DataRow r in dataset.Tables["Stops"].Select()) {
				////   r["Format2"] = r["Format"];
				////   r["Brand2"] = r["Brand"];
				////   r["Shift2"] = r["Shift"];
				////}
				////// тоже самое, но с использованием Expression, тоже не работает
				////dataset.Relations.Add("BrandNameStops", dataset.Tables["BrandName"].Columns["ID"], dataset.Tables["Stops"].Columns["Brand2"]);
				////dataset.Relations.Add("FormatNameStops", dataset.Tables["FormatName"].Columns["ID"], dataset.Tables["Stops"].Columns["Format2"]);
				////dataset.Relations.Add("ShiftNameStops", dataset.Tables["ShiftName"].Columns["ID"], dataset.Tables["Stops"].Columns["Shift2"]);

				#endregion | неудачная попытка, медленно работает |

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(Relations for Format & Brand & Shift): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				if (IsLine) {
					DataSet.Tables["Stops"].Columns.Add("TF", typeof(int), "sum(child(StopGap).TF)");
					DataSet.Tables["Stops"].Columns.Add("TD", typeof(float), "sum(child(StopGap).TD)");
					DataSet.Tables["Stops"].Columns.Add("Seconds", typeof(float), "sum(child(StopGap).Seconds)");
				} else {
					DataSet.Tables["Stops"].Columns.Add("TF", typeof(int), "sum(child(StopGap).TF)/2");
					DataSet.Tables["Stops"].Columns.Add("TD", typeof(float), "sum(child(StopGap).TD)/2");
					DataSet.Tables["Stops"].Columns.Add("Seconds", typeof(float), "sum(child(StopGap).Seconds)/2");
				}
				DataSet.Tables["Stops"].Columns.Add("Day", typeof(DateTime), "dt1");				// only for grDetail visualisation

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(TF & TD Columns for 'Stops'): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				// For trends tab only: MTTR, MTBF etc
				foreach (string key in Kpi.Names.Keys)
					if (key.StartsWith("m") && Kpi.NomParts.ContainsKey(key) && Kpi.Roots.ContainsKey(Kpi.NomParts[key])
							&& !DataSet.Tables["Stops"].Columns.Contains(Kpi.NomParts[key]))
						DataSet.Tables["Stops"].Columns.Add(Kpi.NomParts[key], typeof(float), Kpi.Expr1[Kpi.NomParts[key]].Replace("0", "null"));

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(MTTR, MTBF for 'Stops'): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["Gaps"].AcceptChanges();
				DataSet.Tables["Stops"].AcceptChanges();

				// time meter
				Debug.WriteLine(string.Format("{0}. BindDataset_(Gaps & Stops AcceptChanges): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				Utils.AddColumns(DataSet.Tables["Brands"], Kpi.Expr2, "rel", "");
				Utils.AddColumns(DataSet.Tables["BrandName"], Kpi.Expr2, "BrandListBrands", "");
				Utils.AddColumns(DataSet.Tables["FormatName"], Kpi.Expr2, "FormatListBrands", "");
				Utils.AddColumns(DataSet.Tables["Shifts"], Kpi.Expr2, "ShGaps", "");
				Utils.AddColumns(DataSet.Tables["ShiftName"], Kpi.Expr2, "ShiftListShifts", "");
				Debug.WriteLine(string.Format("{0}. BindDataset_(Columns for Compare Tables): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["BrandName"].Columns.Add("flt", typeof(string), "'Brand='+ID");
				DataSet.Tables["FormatName"].Columns.Add("flt", typeof(string), "'Format='+ID");
				DataSet.Tables["ShiftName"].Columns.Add("flt", typeof(string), "'Shift='+ID");
				Debug.WriteLine(string.Format("{0}. BindDataset_(Columns for Compare Tables): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				DataSet.Tables["Stops"].Columns.Add("Short", typeof(float), string.Format("iif(TD<{0}, TD, 0)", MinIdleTime));
				DataSet.Tables["StopList"].Columns.Add("TD", typeof(float), "SUM(child(StopListStops).TD)");
				DataSet.Tables["StopList"].Columns.Add("TDShort", typeof(float), "SUM(child(StopListStops).Short)");
				Debug.WriteLine(string.Format("{0}. BindDataset_(StopList TD column): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));

				if (Mins > minTrendMins) {
					#region | For Compare tab only  |

					//todone: здесь возникает CrossThread исключение!!!
					int days = (int)DtEnd.Subtract(DtStart).TotalDays;
					if (days >= 730) {
						TrendMode = "YearList";		//tsbYears.Checked = true;
					} else if (days >= 365) {
						TrendMode = "QuarterList";	//tsbQuarters.Checked = true;
					} else if (days >= 180) {
						TrendMode = "MonthList";		//tsbMonths.Checked = true;
					} else if (days >= 14) {
						TrendMode = "WeekList";		//tsbWeek.Checked = true;
					} else {
						TrendMode = "DayList";		//tsbDays.Checked = true;
					}

					BindTrendsTables(TrendMode);

					#endregion | For Compare tab only  |

					// time meter
					Debug.WriteLine(string.Format("{0}. BindDataset_(BindCompareTables): {0:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));
				}
				if (Mins <= maxEditMins && IsLine)
					PrepareForEditMode();

				if (DataSet.Tables["Gaps"].Columns.Contains("dt2")) {
					DataSet.Tables["Brands"].Columns.Add("dt1", typeof(DateTime), "min(child(rel).dt1)");
					DataSet.Tables["Brands"].Columns.Add("dt2", typeof(DateTime), "max(child(rel).dt2)");
					DataSet.Tables["Shifts"].Columns.Add("dt1", typeof(DateTime), "min(child(ShGaps).dt1)");
					DataSet.Tables["Shifts"].Columns.Add("dt2", typeof(DateTime), "max(child(ShGaps).dt2)");
				}

				//#if (!DEBUG)
			} catch (Exception ex) {
				try {
					Error = ex.Message;
					// если при связывании произошла ошибка, то велика вероятность что списки устарели поэтому очищаем списки
					DataSet.Relations.Clear();
					foreach (DataTable dtb in DataSet.Tables)
						dtb.Constraints.Clear();
					DataSet.Tables.Clear();
					CashValidDate = DateTime.Now; // empty_date;

					if (ListsCash.ContainsKey(ID))
						ListsCash.Remove(ID);
				} catch { }
			}
			//#endif
#if (DEBUG)
			// time meter
			DataTable dtb_g = DataSet.Tables["Gaps"];
			Debug.WriteLine(string.Format("{0}. BindDataset_(FINISH!!!): {1:0.000}", debugcnt++, sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		/// <summary>
		/// Добавляет таблицы для закладки 'Тренды' и устанавливает отношения между таблицами
		/// </summary>
		/// <param name="trendmode"></param>
		public void BindTrendsTables(string trendmode)
		{
			if (DataSet.Tables.Contains("SubDates")) {
				//grTrends.DataSource = null;

				if (DataSet.Relations.Contains("StopListSubDates"))
					DataSet.Relations.Remove("StopListSubDates");
				if (DataSet.Relations.Contains("SubDatesStop"))
					DataSet.Relations.Remove("SubDatesStop");

				if (DataSet.Tables["Stops"].Constraints.Contains("SubDatesStop"))
					DataSet.Tables["Stops"].Constraints.Remove("SubDatesStop");
				DataSet.Tables["SubDates"].Constraints.Clear();
				if (DataSet.Tables.Contains("SubDates"))
					DataSet.Tables.Remove("SubDates");
			}

			//~~~~~~~~~~~~~~~~~~ DayList|WeekList|MonthList|QuarterList|YearList ~~~~~~~~~~~~~~~~
			// Creating a table with the number of rows = number of selected days|weeks|months|years
			if (!DataSet.Tables.Contains(trendmode)) {
				DataTable tbDates;
				tbDates = DataSet.Tables["Gaps"].DefaultView.ToTable(true, new string[] { trendmode });
				tbDates.TableName = trendmode;
				DataSet.Tables.Add(tbDates);

				string rel1 = trendmode + "Gap";
				DataSet.Relations.Add(rel1, tbDates.Columns[trendmode], DataSet.Tables["Gaps"].Columns[trendmode]);
				tbDates.Columns.Add("mindt", typeof(DateTime), string.Format("MIN(child({0}).dt1)", rel1));

				string rel2 = trendmode + "Stop";
				DataSet.Relations.Add(rel2, tbDates.Columns[trendmode], DataSet.Tables["Stops"].Columns[trendmode]);

				Utils.AddColumns(tbDates, Kpi.Expr2, rel1, rel2);

				tbDates.Columns.Add("flt", typeof(string), string.Format("'{0}=!'+{0}+'!'", trendmode)); // "'DayList=!'+Daylist+'!'"
			}
			//~~~~~~~~~~~~~~~~~~ SubDates ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			DataTable tbSubT = DataSet.Tables["Gaps"].DefaultView.ToTable(true, new string[] { trendmode, "RD", "DownID" });
			tbSubT.TableName = "SubDates";
			DataSet.Tables.Add(tbSubT);

			DataSet.Relations.Add("SubDatesStop",
				new DataColumn[] { tbSubT.Columns[trendmode], tbSubT.Columns["DownID"] },
				new DataColumn[] { DataSet.Tables["Stops"].Columns[trendmode], DataSet.Tables["Stops"].Columns["DownID"] });
			tbSubT.Columns.Add("TF", typeof(float), "SUM(child(SubDatesStop).TF)");
			tbSubT.Columns.Add("TD", typeof(float), "SUM(child(SubDatesStop).TD)");

			DataSet.Relations.Add("StopListSubDates", DataSet.Tables["StopList"].Columns["DownID"], tbSubT.Columns["DownID"]);
			tbSubT.Columns.Add("DownName", typeof(string), "parent(StopListSubDates).EName");

			// если таблица уже есть то второй раз создавать не надо, выходим
			if (DataSet.Tables.Contains("SubBrands")) return;

			//~~~~~~~~~~~~~~~~~~ SubBrabds ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			DataTable tbSubB = DataSet.Tables["Gaps"].DefaultView.ToTable(true, new string[] { "Brand", "RD", "DownID" });
			tbSubB.TableName = "SubBrands";
			DataSet.Tables.Add(tbSubB);

			DataSet.Relations.Add("StopListSubBrands", DataSet.Tables["StopList"].Columns["DownID"], tbSubB.Columns["DownID"]);
			tbSubB.Columns.Add("DownName", typeof(string), "parent(StopListSubBrands).EName");

			//~~~~~~~~~~~~~~~~~~ SubFormats ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			DataTable tbSubF = DataSet.Tables["Gaps"].DefaultView.ToTable(true, new string[] { "Format", "RD", "DownID" });
			tbSubF.TableName = "SubFormats";
			DataSet.Tables.Add(tbSubF);

			DataSet.Relations.Add("StopListSubFormats", DataSet.Tables["StopList"].Columns["DownID"], tbSubF.Columns["DownID"]);
			tbSubF.Columns.Add("DownName", typeof(string), "parent(StopListSubFormats).EName");

			//~~~~~~~~~~~~~~~~~~ SubShifts ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			DataTable tbSubSh = DataSet.Tables["Gaps"].DefaultView.ToTable(true, new string[] { "Shift", "RD", "DownID" });
			tbSubSh.TableName = "SubShifts";
			DataSet.Tables.Add(tbSubSh);

			DataSet.Relations.Add("StopListSubShifts", DataSet.Tables["StopList"].Columns["DownID"], tbSubSh.Columns["DownID"]);
			tbSubSh.Columns.Add("DownName", typeof(string), "parent(StopListSubShifts).EName");
		}

		/// <summary>
		/// Устанавливает отношения между таблицами для датасета с данными по годам
		/// </summary>
		public void BindYearDataset()
		{
			if (IsEmpty() || YearsDS == null || !YearsDS.Tables.Contains("year"))
				return;
			DataSet dts = YearsDS;
			//dts.Tables.Add(this.DataSet.Tables["StopList"].Copy());
			dts.Relations.Add("RootStopList", dts.Tables["Root"].Columns["KpiID"], dts.Tables["StopList"].Columns["KpiID"]);
			dts.Relations.Add("StopListYear", dts.Tables["StopList"].Columns["DownID"], dts.Tables["Year"].Columns["DownID"]);
			dts.Tables["StopList"].Columns.Add("RD", typeof(string), "Parent(RootStopList).RD");
			dts.Tables["StopList"].Columns.Add("FullName", typeof(string));

			dts.Tables["FormatName"].Columns.Add("SpeedM", typeof(double), "Speed/60");
			dts.Relations.Add("FormatListYear", dts.Tables["FormatName"].Columns["ID"], dts.Tables["Year"].Columns["Format"]);
			dts.Relations.Add("BrandListYear", dts.Tables["BrandName"].Columns["ID"], dts.Tables["Year"].Columns["Brand"]);
			dts.Tables["Year"].Columns.Add("SpeedM", typeof(double), "Parent(FormatListYear).SpeedM");
			//dts.Tables["Year"].Columns.Add("Volume", typeof(float), "Parent(FormatListYear).Volume");

			foreach (DataTable dtb in dts.Tables)
				if (dtb.Columns.Contains("NameID") && dtb.Columns.Contains("Value") && !dtb.Columns.Contains("Name"))
					dtb.Columns.Add("Name", typeof(string));
			string expr = UseDefaultLanguage ? "EName" : "IIF(Value is NULL, EName, Value)";
			foreach (DataTable dtb in dts.Tables)
				if (dtb.Columns.Contains("NameID") && dtb.Columns.Contains("Value") && dtb.Columns.Contains("Name"))
					dtb.Columns["Name"].Expression = expr;

			FillFullNameColumn(dts.Tables["StopList"]);

			dts.Tables["Year"].Columns.Add("RD", typeof(string), "Parent(StopListYear).RD");
			foreach (string col in DownColumns.Values)
				dts.Tables["Year"].Columns.Add(col, typeof(int), "Parent(StopListYear)." + col);

			Utils.AddColumns(dts.Tables["Year"], Kpi.Expr1, "FormatListYear", "");
			Utils.AddColumns(dts.Tables["BrandName"], Kpi.Expr2, "BrandListYear", "");
			Utils.AddColumns(dts.Tables["FormatName"], Kpi.Expr2, "FormatListYear", "");
			
			_yearDataSetIsLoaded = true;
			YearsFixTime();
		}

		/// <summary>
		/// Легкая проверка - загружены ли текущие данные
		/// </summary>
		/// <returns></returns>
		public bool IsEmpty()
		{
			return DataSet == null || DataSet.Tables.Count < 10;
		}

        public bool IsEmpty_Counter()
        {

            return DataSet == null || DataSet.Tables.Count < 6 || DataSet.Tables[0].Rows.Count == 0; ;
        }

		/// <summary>
		/// Дополнение списка автозавершения из текущей таблицы простоев
		/// </summary>
		public void FillAutoComplete()
		{
			Stopwatch sw = Stopwatch.StartNew();

			if (!DataSet.Tables.Contains("Stops")) return;
			InitAutoCompletes();
			string val;

			////foreach (DataRow dr in lu[line].DataSet.Tables["Stops"].Select("", "", DataViewRowState.CurrentRows)) {
			////   val = dr["Comment"].ToString();
			////   if (!lu[line].AutoCompletes.Contains(val))
			////      lu[line].AutoCompletes.Add(val);
			////}

			DataRow[] DR = DataSet.Tables["Stops"].Select("", "", DataViewRowState.CurrentRows);
			string[] str = new string[DR.Length];
			int cnt = 0;
			foreach (DataRow dr in DR) {
				val = dr["Comment"].ToString();
				str[cnt] = val;
				cnt++;
			}
			AutoCompletes.AddRange(str);

			Debug.WriteLine(string.Format("FillAutoComplete() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}

		/// <summary>
		/// Вызываетя при смене языка в интерфейсе программы
		/// </summary>
		public void SetLocalization()
		{
			if (DataSet != null && DataSet.Tables.Contains("StopList")) {
				ChangeNameColumn();
				FillFullNameColumnOnly(0, "", DataSet.Tables["StopList"].Select("TopID is NULL"));
			}
		}

		public DataRow AddDownTime(DateTime dt1, DateTime dt2, int mark1, int mark2, int downid, string comment)
		{
			Stopwatch sw = Stopwatch.StartNew();

			// при изменении таблицы предотвращаем вызов Commit
			Debug.WriteLine(string.Format("AddDownTime_01 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			Application.DoEvents();

			// удаляем все выражения из таблиц
			UndoRedo.RemoveExpressions();
			Debug.WriteLine(string.Format("AddDownTime_03 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			DataRow drNew = DataSet.Tables["Stops"].NewRow();
			//if (mark1 != 0) drNew["Mark1"] = mark1;
			//if (downid != 0) drNew["DownID"] = downid;
			drNew["ID"] = _newId--;
			if (comment != "") drNew["Comment"] = comment;
			if (mark1 != 0) drNew["Mark1"] = mark1;
			if (mark2 != 0) drNew["Mark2"] = mark2;
			if (downid != 0) drNew["DownID"] = downid;

			DataSet.Tables["Stops"].Rows.Add(drNew);
			//if (downid == 0)
			//CheckRowForNulls(drNew);

			//todo: если выделена зона внутри простоя то надо у этой зоны менять dt2, т.е. делать равным dt1 выделенной зоны
			// для того чтобы не было разрывов внутри одного простоя и, как следствие, не будет кривой визуализации
			//////bool was_splitted = false;
			//////foreach (DataRow parr in lu[L].DataSet.Tables["Stops"].Select(String.Format("dt1<#{0:M/d/yy H:m}# AND dt2>#{1:M/d/yy H:m}#", dt1, dt2))) {
			//////   // ту строку которую режем переводим в состояние Modified; это для того чтобы смежные участки времени не попавшие
			//////   // в запрос не были изменены без ведома
			//////   was_splitted = true;
			//////   if (parr.RowState != DataRowState.Added) {
			//////      parr.AcceptChanges();
			//////      parr.SetModified();
			//////   }
			//////}

			DateTime dtG1, dtG2;
			int mins, minsNew, minsNew2;
			string flt = String.Format("dt1<=#{1:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", dt1, dt2);
			foreach (DataRow drG in DataSet.Tables["Gaps"].Select(flt)) {
				if (drG["dt1"] == DBNull.Value) continue;
				dtG1 = (DateTime)drG["dt1"];
				dtG2 = (DateTime)drG["dt2"];
				mins = (int)drG["TF"];
				if (dt1 > dtG2 || dt2 < dtG1)
					continue;											// выделенная зона лежит в стороне, идем дальше
				if (dt1 <= dtG1 && dt2 >= dtG2) {
					drG["Stop"] = drNew["ID"];						// the gap is inside the selected zone
					continue;
				}
				DataRow drNewGap = DataSet.Tables["Gaps"].NewRow();
				drNewGap["Stop"] = drNew["ID"];
				if (dt1 > dtG1 && dt2 < dtG2) {					// выделенная зона находится внутри гепа

					#region | the selected zone is inside the gap      |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dt1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dt1, dt2.AddMinutes(1));

					// новый геп после выбранной зоны
					DataRow drNewGap2 = DataSet.Tables["Gaps"].NewRow();
					drNewGap2["dt1"] = dt2.AddMinutes(1);
					drNewGap2["dt2"] = dtG2;
					minsNew2 = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drNewGap2["TF"] = minsNew2;
					drNewGap2["Stop"] = drG["Stop"];
					drNewGap2["Brands"] = drG["Brands"];
					drNewGap2["Shifts"] = drG["Shifts"];
					drNewGap2["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drNewGap2["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					DataSet.Tables["Gaps"].Rows.Add(drNewGap2);

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | the selected zone is inside the gap      |
				} else if (dt1 > dtG1 && dt1 <= dtG2) {		// начало выбранной зоны режет геп пополам

					#region | front of the selected zone split the gap |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dtG2;
					minsNew = Convert.ToInt32(dtG2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dt1, dtG2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dt1, dtG2.AddMinutes(1));

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | front of the selected zone split the gap |
				} else if (dt2 < dtG2 && dt2 >= dtG1) {		// конец выбранной зоны режет геп пополам

					#region | back of the selected zone split the gap  |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dtG1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dtG1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dtG1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dtG1, dt2.AddMinutes(1));

					// старый геп после выбранной зоной
					drG["dt1"] = dt2.AddMinutes(1);
					drG["TF"] = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drG["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drG["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));

					#endregion | back of the selected zone split the gap  |
				}
				DataSet.Tables["Gaps"].Rows.Add(drNewGap);
				Debug.WriteLine(string.Format("AddDownTime_row_ Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
			}
			Debug.WriteLine(string.Format("AddDownTime_1 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// Восстанавливаем выражения для таблиц
			UndoRedo.RestoreExpessions();
			Debug.WriteLine(string.Format("AddDownTime_2 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// Удаляем устаревших родителей
			foreach (DataRow r in DataSet.Tables["Stops"].Select("TF is NULL"))
				r.Delete();
			Debug.WriteLine(string.Format("AddDownTime_3 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// фиксируем изменения, разрешаем вызов Commit
			UndoRedo.Commit();

			Debug.WriteLine(string.Format("AddDownTime_4 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			return drNew;
		}

		public DataRow SplitProduction(DateTime dt1, DateTime dt2, int BrandID, int FormatID)
		{
			Stopwatch sw = Stopwatch.StartNew();

			int currbrend = 0, currformat = 0;

			//todone: определить какая запись делится пополам и установить FactorNP для нее =1
			foreach (DataRow dr in DataSet.Tables["Brands"].Select(String.Format("dt1<=#{1:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", dt1, dt2))) {
				dr["FactorNP"] = 1;
				// ту строку которую режем переводим в состояние Added; это для того чтобы смежные участки времени не попавшие
				// в запрос не были изменены безконтрольно
				dr.AcceptChanges();
				dr.SetAdded();

				currbrend = C.GetInt(dr["Brand"]);
				currformat = C.GetInt(dr["Format"]);
			}

			// удаляем все выражения из таблиц
			UndoRedo.RemoveExpressions();
			Debug.WriteLine(string.Format("AddProduction_1 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			bool addempty = false;
			if (dt2 == _dtEmpty) {
				addempty = true;
				dt2 = MU.DtEnd;
			}

			DataRow drNew = DataSet.Tables["Brands"].NewRow();

			//if (BrandID == 0 && lu[L].DefaultBrand != 0)
			//   drNew["Brand"] = (int)lu[L].DefaultBrand;
			//else if (BrandID != 0)
			//   drNew["Brand"] = BrandID;
			//if (FormatID == 0 && lu[L].DefaultFormat != 0)
			//   drNew["Format"] = (int)lu[L].DefaultFormat;
			//else if (FormatID != 0)
			//   drNew["Format"] = FormatID;

			drNew["Brand"] = currbrend;
			drNew["Format"] = currformat;

			drNew["FactorNP"] = 1;
			drNew["ID"] = _newId--;
			DataSet.Tables["Brands"].Rows.Add(drNew);
			int id = (int)drNew["ID"];

			DateTime dtG1, dtG2;
			int mins, minsNew, minsNew2;
			string flt = String.Format("dt1<=#{1:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", dt1, dt2);
			DataRow[] DTR = DataSet.Tables["Gaps"].Select(flt);
			foreach (DataRow drG in DTR) {
				if (drG["dt1"] == DBNull.Value) continue;
				dtG1 = (DateTime)drG["dt1"];
				dtG2 = (DateTime)drG["dt2"];
				mins = (int)drG["TF"];
				if (dt1 > dtG2 || dt2 < dtG1)
					continue;										// выделенная зона лежит в стороне от гепа
				if (dt1 <= dtG1 && dt2 >= dtG2) {
					drG["Brands"] = id;							// the gap located inside the selected zone
					continue;
				}
				DataRow drNewGap = DataSet.Tables["Gaps"].NewRow();
				drNewGap["Brands"] = id;
				if (dt1 > dtG1 && dt2 < dtG2) {				// выделенная зона находится внутри гепа

					#region | the selected zone is inside the gap      |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dt1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dt1, dt2.AddMinutes(1));

					// новый геп после выбранной зоны
					DataRow drNewGap2 = DataSet.Tables["Gaps"].NewRow();
					drNewGap2["dt1"] = dt2.AddMinutes(1);
					drNewGap2["dt2"] = dtG2;
					minsNew2 = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drNewGap2["TF"] = minsNew2;
					drNewGap2["Stop"] = drG["Stop"];
					drNewGap2["Brands"] = drG["Brands"];
					drNewGap2["Shifts"] = drG["Shifts"];
					drNewGap2["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drNewGap2["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					DataSet.Tables["Gaps"].Rows.Add(drNewGap2);

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | the selected zone is inside the gap      |
				} else if (dt1 > dtG1 && dt1 <= dtG2) {	// начало выделенной зоны режет геп на две части

					#region | front of the selected zone split the gap |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dtG2;
					minsNew = Convert.ToInt32(dtG2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dt1, dtG2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dt1, dtG2.AddMinutes(1));

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | front of the selected zone split the gap |
				} else if (dt2 < dtG2 && dt2 >= dtG1) {	// конец выделенной зоны режет геп на две части

					#region | back of the selected zone split the gap  |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dtG1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dtG1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["GP"] = GetGP(dtG1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dtG1, dt2.AddMinutes(1));

					// старый геп после выбранной зоны
					drG["dt1"] = dt2.AddMinutes(1);
					drG["TF"] = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drG["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drG["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));

					#endregion | back of the selected zone split the gap  |
				}
				DataSet.Tables["Gaps"].Rows.Add(drNewGap);
			}
			Debug.WriteLine(string.Format("AddProduction_2 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			UndoRedo.RestoreExpessions();
			Debug.WriteLine(string.Format("AddProduction_3 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			foreach (DataRow dr in DataSet.Tables["Brands"].Select("TF is NULL")) {
				dr.Delete();
				//dr.AcceptChanges();
			}
			Debug.WriteLine(string.Format("AddProduction_4 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// если был вызов для смены текущего SKU то возвращаемся в ChangeEntry()
			if (addempty)
				return drNew;

			// Фиксируем изменения
			UndoRedo.Commit();

			Debug.WriteLine(string.Format("AddProduction() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
			return drNew;
		}

		public DataRow SplitShift(DateTime dt1, DateTime dt2, int ShiftID, bool TwoSteps)
		{
			Stopwatch sw = Stopwatch.StartNew();

			// удаляем все выражения из таблиц
			UndoRedo.RemoveExpressions();
			Debug.WriteLine(string.Format("AddShift_1 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			DataRow drNew = DataSet.Tables["Shifts"].NewRow();
			drNew["Shift"] = ShiftID == 0 && DefaultShift != 0 ? DefaultShift : ShiftID;

			drNew["ID"] = _newId--;
			DataSet.Tables["Shifts"].Rows.Add(drNew);

			DateTime dtG1, dtG2;
			int mins, minsNew, minsNew2, GP;
			string flt = String.Format("dt1<=#{1:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", dt1, dt2);
			foreach (DataRow drG in DataSet.Tables["Gaps"].Select(flt)) {
				if (drG["dt1"] == DBNull.Value) continue;
				dtG1 = (DateTime)drG["dt1"];
				dtG2 = (DateTime)drG["dt2"];
				mins = (int)drG["TF"];
				//dtG2 = dtG1.AddMinutes(MonitorUnit.mins);
				GP = (int)drG["GP"];
				if (dt1 > dtG2 || dt2 < dtG1)
					continue;
				if (dt1 <= dtG1 && dt2 >= dtG2) {
					// the gap is inside the selected zone
					drG["Shifts"] = drNew["ID"];
					continue;
				}
				DataRow drNewGap = DataSet.Tables["Gaps"].NewRow();
				drNewGap["Shifts"] = drNew["ID"];
				if (dt1 > dtG1 && dt2 < dtG2) {					// the selected zone is inside the gap

					#region | the selected zone is inside the gap      |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["GP"] = GetGP(dt1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetGP(dt1, dt2.AddMinutes(1));

					// новый геп после выбранной зоны
					DataRow drNewGap2 = DataSet.Tables["Gaps"].NewRow();
					drNewGap2["dt1"] = dt2.AddMinutes(1);
					drNewGap2["dt2"] = dtG2;
					minsNew2 = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drNewGap2["TF"] = minsNew2;
					drNewGap2["Stop"] = drG["Stop"];
					drNewGap2["Brands"] = drG["Brands"];
					drNewGap2["Shifts"] = drG["Shifts"];
					drNewGap2["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drNewGap2["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					DataSet.Tables["Gaps"].Rows.Add(drNewGap2);

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | the selected zone is inside the gap      |
				} else if (dt1 > dtG1 && dt1 <= dtG2) {		// начало выбранной зоны режет геп пополам

					#region | front of the selected zone split the gap |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dt1;
					drNewGap["dt2"] = dtG2;
					minsNew = Convert.ToInt32(dtG2.Subtract(dt1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["GP"] = GetGP(dt1, dtG2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dt1, dtG2.AddMinutes(1));

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					#endregion | front of the selected zone split the gap |
				} else if (dt2 < dtG2 && dt2 >= dtG1) {		// конец выбранной зоны режет геп пополам

					#region | back of the selected zone split the gap  |

					// новый геп для выбранной зоны
					drNewGap["dt1"] = dtG1;
					drNewGap["dt2"] = dt2;
					minsNew = Convert.ToInt32(dt2.Subtract(dtG1).TotalMinutes + 1);
					drNewGap["TF"] = minsNew;
					drNewGap["Stop"] = drG["Stop"];
					drNewGap["Brands"] = drG["Brands"];
					drNewGap["GP"] = GetGP(dtG1, dt2.AddMinutes(1));
					drNewGap["Seconds"] = GetSeconds(dtG1, dt2.AddMinutes(1));

					// старый геп после выбранной зоной
					drG["dt1"] = dt2.AddMinutes(1);
					drG["TF"] = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drG["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drG["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));

					#endregion | back of the selected zone split the gap  |
				}
				DataSet.Tables["Gaps"].Rows.Add(drNewGap);
			}
			Debug.WriteLine(string.Format("AddShift_2 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			if (TwoSteps)
				return drNew;

			UndoRedo.RestoreExpessions();
			Debug.WriteLine(string.Format("AddShift_3 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// просто убрать пустые строки (без связей в l_gaps) удалять их из бд не обязательно, они не помешают
			foreach (DataRow dr in DataSet.Tables["Shifts"].Select("TF is NULL")) {
				dr.Delete();
				//dr.AcceptChanges();
			}
			Debug.WriteLine(string.Format("AddShift_4 Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));

			// Фиксируем изменения
			Debug.WriteLine(string.Format("AddShift() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
			UndoRedo.Commit();

			return drNew;
		}

		public void DeleteProduction(DateTime dt1, DateTime dt2)
		{
			DateTime dtG1, dtG2;
			int mins, minsNew, minsNew2, GP;
			DataRow drNewGap = DataSet.Tables["Gaps"].NewRow();

			foreach (DataRow drG in DataSet.Tables["Gaps"].Select("", "dt1")) {
				if (drG["dt1"] == DBNull.Value) continue;
				dtG1 = (DateTime)drG["dt1"];
				dtG2 = (DateTime)drG["dt2"];
				mins = (int)drG["TF"];
				GP = (int)drG["GP"];
				if (dt1 > dtG2 || dt2 < dtG1)
					continue;								// выделение лежит в стороне от гепа

				if (dt1 <= dtG1 && dt2 >= dtG2) {
					//drG["GP"] = 0;						// the gap is inside the selected zone
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["Brands"] = drG["Brands"];

					drG.Delete();
					drG.AcceptChanges();
					continue;
				}
				if (dt1 > dtG1 && dt2 < dtG2) {		// the selected zone is inside the gap
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["Brands"] = drG["Brands"];

					// новый геп после выбранной зоны
					DataRow drNewGap2 = DataSet.Tables["Gaps"].NewRow();
					drNewGap2["dt1"] = dt2.AddMinutes(1);
					drNewGap2["dt2"] = dtG2;
					minsNew2 = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drNewGap2["TF"] = minsNew2;
					drNewGap2["Stop"] = drG["Stop"];
					drNewGap2["Brands"] = drG["Brands"];
					drNewGap2["Shifts"] = drG["Shifts"];
					drNewGap2["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drNewGap2["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					DataSet.Tables["Gaps"].Rows.Add(drNewGap2);

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);

					break;
				} else if (dt1 > dtG1 && dt1 <= dtG2) {	// начало выделенной зоны режет геп на две части
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["Brands"] = drG["Brands"];

					// старый геп перед выбранной зоной
					drG["dt2"] = dt1.AddMinutes(-1);
					drG["TF"] = Convert.ToInt32(dt1.Subtract(dtG1).TotalMinutes);
					drG["GP"] = GetGP(dtG1, dt1);
					drG["Seconds"] = GetSeconds(dtG1, dt1);
				} else if (dt2 < dtG2 && dt2 >= dtG1) {	// конец выделенной зоны режет геп на две части
					drNewGap["Shifts"] = drG["Shifts"];
					drNewGap["Brands"] = drG["Brands"];

					// старый геп после выбранной зоной
					drG["dt1"] = dt2.AddMinutes(1);
					drG["TF"] = Convert.ToInt32(dtG2.Subtract(dt2).TotalMinutes);
					drG["GP"] = GetGP(dt2.AddMinutes(1), dtG2.AddMinutes(1));
					drG["Seconds"] = GetSeconds(dt2.AddMinutes(1), dtG2.AddMinutes(1));
				}
			}
			// новый геп для выбранной зоны
			drNewGap["dt1"] = dt1;
			drNewGap["dt2"] = dt2;
			minsNew = Convert.ToInt32(dt2.Subtract(dt1).TotalMinutes + 1);
			drNewGap["TF"] = minsNew;
			drNewGap["GP"] = 0;
			drNewGap["Seconds"] = minsNew;
			DataSet.Tables["Gaps"].Rows.Add(drNewGap);

			//todo: сделать метод более универсальным, т.е. не только удалять но и добавлять данные о продукции
			// для этого достаточно третий параметр передавать больше нуля
			string dates = string.Format("\'{0:yyyyMMdd H:mm}\', \'{1:yyyyMMdd H:mm}\', 0", dt1, dt2.AddMinutes(1));
			using (SqlConnection cn = new SqlConnection(Connection)) {
				try {
					cn.Open();
					using (SqlCommand cmd = new SqlCommand("v_deleteproduction " + dates, cn)) {
						cmd.ExecuteNonQuery();
					}

					// delete production for visualization
					dates = C.GetCDateFilter("dt", dt1, dt2.AddMinutes(1));
					foreach (DataRow dr in DataSet.Tables["Minutes"].Select(dates, "dt"))
						dr[1] = 0;
				} catch (Exception ex) {
					MessageBox.Show(ex.Message);
				} finally {
					////ChangeState(UndoRedo.Action.Clear);
					UndoRedo.ClearStack(); // Это вместо предыдущей строки; разобраться - надо ли ???
				}
			}
		}

		public void Save()
		{
			#region | downtimes    |

			using (SqlDataAdapter da = new SqlDataAdapter { UpdateBatchSize = 20 }) {
				using (SqlConnection cn = new SqlConnection(Connection)) {
					// need to duplicate some fields since the computed columns can't be updated
					foreach (DataRow r in DataSet.Tables["Stops"].Rows) {
						if (r.RowState == DataRowState.Added && r["TF"] != DBNull.Value) {
							r["_dt1"] = r["dt1"];
							r["_dt2"] = ((DateTime)r["dt2"]).AddMinutes(1);
						}
					}

					// ~~~~~~~~~~~~~~~~~~ insert simple ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.InsertCommand = new SqlCommand("v_addstopsimple", cn);
					da.InsertCommand.CommandType = CommandType.StoredProcedure;
					da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
					da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
					da.InsertCommand.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4, "_dt2");
					da.InsertCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID");
					da.InsertCommand.Parameters.Add("@Mark1", SqlDbType.Int, 4, "Mark1");
					da.InsertCommand.Parameters.Add("@Mark2", SqlDbType.Int, 4, "Mark2");
					da.InsertCommand.Parameters.Add("@Comment", SqlDbType.NVarChar, 200, "Comment");
					da.InsertCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(GetSimpleStops());

					// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.InsertCommand = new SqlCommand("v_addstop", cn);
					da.InsertCommand.CommandType = CommandType.StoredProcedure;
					da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
					da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
					da.InsertCommand.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4, "_dt2");
					da.InsertCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID");
					da.InsertCommand.Parameters.Add("@Mark1", SqlDbType.Int, 4, "Mark1");
					da.InsertCommand.Parameters.Add("@Mark2", SqlDbType.Int, 4, "Mark2");
					da.InsertCommand.Parameters.Add("@Comment", SqlDbType.NVarChar, 200, "Comment");
					da.InsertCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Stops"].Select("TF>0", "", DataViewRowState.Added));

					// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.UpdateCommand = new SqlCommand("v_updatestop", cn);
					da.UpdateCommand.CommandType = CommandType.StoredProcedure;
					da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
					da.UpdateCommand.Parameters.Add("@Mark1", SqlDbType.Int, 4, "Mark1");
					da.UpdateCommand.Parameters.Add("@Mark2", SqlDbType.Int, 4, "Mark2");
					da.UpdateCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID");
					da.UpdateCommand.Parameters.Add("@Comment", SqlDbType.NVarChar, 200, "Comment");
					da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
					// database round-trip
					da.Update(DataSet.Tables["Stops"].Select("TF>0", "", DataViewRowState.ModifiedOriginal));

					// ~~~~~~~~~~~~~~~~~~~ delete ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					// удаление нужно только для стопов!!!!!
					da.DeleteCommand = new SqlCommand("v_deletestop", cn);
					da.DeleteCommand.CommandType = CommandType.StoredProcedure;
					da.DeleteCommand.UpdatedRowSource = UpdateRowSource.None;
					da.DeleteCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Stops"].Select("TF is NULL", "", DataViewRowState.Deleted));
				}
			}

			#endregion | downtimes    |

			#region | production   |

			using (SqlDataAdapter da = new SqlDataAdapter { UpdateBatchSize = 20 }) {
				using (SqlConnection cn = new SqlConnection(Connection)) {
					DateTime dt1_last = (DateTime)DataSet.Tables["Brands"].Compute("Max(dt1)", "");

					// need to duplicate some fields since the computed columns can not be updated
					foreach (DataRow r in DataSet.Tables["Brands"].Rows) {
						if (r.RowState == DataRowState.Added && r["TF"] != DBNull.Value) {
							r["_dt1"] = r["dt1"];
							r["_dt2"] = ((DateTime)r["dt2"]).AddMinutes(1);
							//if (r["FactorNP"] == Single. )
						}
					}
					foreach (DataRow r in DataSet.Tables["Brands"].Rows) {
						if (r.RowState == DataRowState.Modified && r["TF"] != DBNull.Value) {
							if (MU.DtEnd > DateTime.Now && dt1_last == (DateTime)r["dt1"])
								continue;
							r["_dt1"] = r["dt1"];
							r["_dt2"] = ((DateTime)r["dt2"]).AddMinutes(1);
							r.AcceptChanges();
							r.SetAdded();
						}
					}

					// ~~~~~~~~~~~~~~~~~~ insert last entry ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					if (MU.DtEnd > DateTime.Now) {
						string flt = string.Format("TF>0 and dt1=#{0:M/d/yy H:m}#", dt1_last);
						DataRow[] DR = DataSet.Tables["Brands"].Select(flt, "", DataViewRowState.Added);
						if (DR.Length > 0) {
							da.InsertCommand = new SqlCommand("v_changebrand", cn);
							da.InsertCommand.CommandType = CommandType.StoredProcedure;
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

							da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
							da.InsertCommand.Parameters.Add("@brandid", SqlDbType.Int, 4, "Brand");
							da.InsertCommand.Parameters.Add("@formatid", SqlDbType.Int, 4, "Format");
							da.InsertCommand.Parameters.Add("@factornp", SqlDbType.Real, 4, "FactorNP");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4).Value = SessionData.SessionID;
							SqlParameter par = da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							par.Direction = ParameterDirection.Output;
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							// database round-trip
							da.Update(DR);
						}
					}

					// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.InsertCommand = new SqlCommand("v_addbrand", cn);
					da.InsertCommand.CommandType = CommandType.StoredProcedure;
					da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
					da.InsertCommand.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4, "_dt2");
					da.InsertCommand.Parameters.Add("@brandid", SqlDbType.Int, 4, "Brand");
					da.InsertCommand.Parameters.Add("@formatid", SqlDbType.Int, 4, "Format");
					da.InsertCommand.Parameters.Add("@factornp", SqlDbType.Real, 4, "FactorNP");
					da.InsertCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;
					da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
					// database round-trip
					//todone: проверить перед сохранением чтобы не было значений infinity
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Brands"].Select("TF>0", "", DataViewRowState.Added));

					// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.UpdateCommand = new SqlCommand("v_updatebrand", cn);
					da.UpdateCommand.CommandType = CommandType.StoredProcedure;
					da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
					da.UpdateCommand.Parameters.Add("@brandid", SqlDbType.Int, 4, "Brand");
					da.UpdateCommand.Parameters.Add("@formatid", SqlDbType.Int, 4, "Format");
					da.UpdateCommand.Parameters.Add("@factornp", SqlDbType.Real, 4, "FactorNP");
					da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Brands"].Select("TF>0", "", DataViewRowState.ModifiedOriginal));
				}
			}

			#endregion | production   |

			#region | shifts       |

			using (SqlDataAdapter da = new SqlDataAdapter { UpdateBatchSize = 20 }) {
				using (SqlConnection cn = new SqlConnection(Connection)) {
					DateTime dt1_last = (DateTime)DataSet.Tables["Shifts"].Compute("Max(dt1)", "");

					// need to duplicate some fields since the computed columns can not be updated
					foreach (DataRow r in DataSet.Tables["Shifts"].Rows) {
						if (r.RowState == DataRowState.Added && r["TF"] != DBNull.Value) {
							r["_dt1"] = r["dt1"];
							r["_dt2"] = ((DateTime)r["dt2"]).AddMinutes(1);
						}
					}
					foreach (DataRow r in DataSet.Tables["Shifts"].Rows) {
						if (r.RowState == DataRowState.Modified && r["TF"] != DBNull.Value) {
							if (MU.DtEnd > DateTime.Now && dt1_last == (DateTime)r["dt1"])
								continue;
							r["_dt1"] = r["dt1"];
							r["_dt2"] = ((DateTime)r["dt2"]).AddMinutes(1);
							r.AcceptChanges();
							r.SetAdded();
						}
					}

					// ~~~~~~~~~~~~~~~~~~ insert last entry ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					if (MU.DtEnd > DateTime.Now) {
						string flt = string.Format("TF>0 and dt1=#{0:M/d/yy H:m}#", dt1_last);
						DataRow[] DR = DataSet.Tables["Shifts"].Select(flt, "", DataViewRowState.Added);
						if (DR.Length > 0) {
							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("v_changeshift", cn);
							da.InsertCommand.CommandType = CommandType.StoredProcedure;
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;

							da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
							da.InsertCommand.Parameters.Add("@shiftid", SqlDbType.Int, 4, "Shift");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4).Value = SessionData.SessionID;
							SqlParameter par = da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							par.Direction = ParameterDirection.Output;
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							// database round-trip
							da.Update(DataSet.Tables["Shifts"].Select("TF>0", "", DataViewRowState.Added));
						}
					}

					// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.InsertCommand = new SqlCommand("v_addshift", cn);
					da.InsertCommand.CommandType = CommandType.StoredProcedure;
					da.InsertCommand.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4, "_dt1");
					da.InsertCommand.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4, "_dt2");
					da.InsertCommand.Parameters.Add("@ShiftID", SqlDbType.Int, 4, "Shift");
					da.InsertCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;
					da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Shifts"].Select("TF>0", "", DataViewRowState.Added));

					// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					da.UpdateCommand = new SqlCommand("v_updateshift", cn);
					da.UpdateCommand.CommandType = CommandType.StoredProcedure;
					da.UpdateCommand.UpdatedRowSource = UpdateRowSource.None;
					da.UpdateCommand.Parameters.Add("@ShiftID", SqlDbType.Int, 4, "Shift");
					da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int).Value = SessionData.SessionID;
					da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
					// database round-trip
					da.ContinueUpdateOnError = true;
					da.Update(DataSet.Tables["Shifts"].Select("TF>0", "", DataViewRowState.ModifiedOriginal));
				}
			}

			#endregion | shifts       |
		}

		public void AddFromToMatrix()
		{
			if (FromToDS != null)
				return;

			if (IsCoupledLine && Childs.Count == 2) {
				FromToDS = new DataSet();

				// На всякий случай заполняем детей
				Childs[0].AddFromToMatrix();
				Childs[1].AddFromToMatrix();
				
				// Копируем заполненных детей
				DataTable dtb0 = Childs[0].FromToDS.Tables["fmt"].DefaultView.ToTable(); 
				DataTable dtb1 = Childs[1].FromToDS.Tables["fmt"].DefaultView.ToTable();
				FromToDS.Tables.Add(dtb0);
				dtb0.Columns["sum"].Expression = Childs[0].FromToDS.Tables["fmt"].Columns["sum"].Expression;
				//dtb0.Columns["sum"].ReadOnly = false;
				
				// Переносим совпадающие строки из второй таблицы в основную
				foreach (DataRow dr in FromToDS.Tables["fmt"].Rows) 
					BuildFromToMatrixFromChilds(dr, dtb1);

				// Оставшиеся строки из второй таблицы тоже переносим в основную, но время делим на2
				string name;
				foreach (DataRow dr in dtb1.Rows) {
					DataRow dr0 = dtb0.NewRow();
					dr0.ItemArray = dr.ItemArray;
					foreach (DataColumn col in dtb0.Columns)
						if (col.ExtendedProperties.Contains("Visible")) {
							name = col.ColumnName;
							dr0[name] = (int)dr0[name] / 2;
						}
					dr0["sum"] = (int)dr0["sum"] / 2;
					dtb0.Rows.Add(dr0);
				}

				CreateFromToMatrixSummary(dtb0);

				return;
				
			}

			int bid11 = 0, fid11 = 0, bid12 = 0, fid12 = 0, bid01 = 0, fid01 = 0, bid02 = 0, fid02 = 0;

			//DataSet dts = new DataSet();
			FromToDS = new DataSet();
			DataTable fmt = new DataTable("fmt");
			fmt.Columns.Add("dt1", typeof(DateTime));
			fmt.Columns.Add("dt2", typeof(DateTime));

			fmt.Columns.Add("bname1", typeof(string));
			fmt.Columns.Add("fname1", typeof(string));
			fmt.Columns.Add("bname2", typeof(string));
			fmt.Columns.Add("fname2", typeof(string));

			fmt.Columns.Add("bid1", typeof(int));
			fmt.Columns.Add("bid2", typeof(int));
			fmt.Columns.Add("fid1", typeof(int));
			fmt.Columns.Add("fid2", typeof(int));

			// Заголовки столбцов
			fmt.Columns["dt1"].Caption = Res.strTime;
			fmt.Columns["dt2"].Caption = "_dt2";
			fmt.Columns["bname1"].Caption = "From brand";
			fmt.Columns["fname1"].Caption = "From format";
			fmt.Columns["bname2"].Caption = "To brand";
			fmt.Columns["fname2"].Caption = "To format";
			fmt.Columns["bid1"].Caption = "_bid1";
			fmt.Columns["bid2"].Caption = "_bid2";
			fmt.Columns["fid1"].Caption = "_fid1";
			fmt.Columns["fid2"].Caption = "_fid2";

			// По таблице простоев добавляем колонку для каждого типа Changeover
			//todo: сделать выборку RD для этого типа простоев из настроечной таблицы g_cfg
			Dictionary<int, string> scol = new Dictionary<int, string>();
			int id;
			foreach (DataRow dr in DataSet.Tables["StopList"].Select("RD='tt4'", "")) {
				id = (int)dr["DownID"];
				scol.Add(id, "s" + id);
				fmt.Columns.Add("s" + id, typeof(int));
				fmt.Columns[scol[id]].Caption = dr["Name"].ToString();
			}

			DataRow curr = null;
			foreach (DataRow dr in DataSet.Tables["Gaps"].Select("", "dt1")) {
				bid12 = (int)dr["brand"];
				fid12 = (int)dr["format"];
				if (bid11 != bid12 || fid11 != fid12) {
					if (bid11 == bid01 && bid12 == bid02 && fid11 == fid01 && fid12 == fid02)
						continue;
					curr = fmt.NewRow();
					curr["dt1"] = dr["dt1"];
					curr["dt2"] = dr["dt2"];
					curr["bid1"] = bid11;
					curr["fid1"] = fid11;
					curr["bid2"] = bid12;
					curr["fid2"] = fid12;
					
					bid01 = bid11;
					bid02 = bid12;
					fid01 = fid11;
					fid02 = fid12;

					bid11 = bid12;
					fid11 = fid12;
					fmt.Rows.Add(curr);
				} else {
					curr["dt2"] = dr["dt2"];
				}
			}

			// заполняем таблицу данными
			DateTime dt1, dt2;
			string flt;
			foreach (DataRow dr in fmt.Rows) {
				dt1 = (DateTime)dr["dt1"];
				dt2 = (DateTime)dr["dt2"];
				foreach (int s in scol.Keys) {
					flt = String.Format("DownID={0} AND {1}", s, C.GetCDateFilter("dt1", dt1, dt2.AddMinutes(1)));
					dr[scol[s]] = C.GetFloat(DataSet.Tables["Stops"].Compute("SUM(TD)", flt));
				}
			}


			string sid;
			scol.Clear();
			foreach (DataRow dr in DataSet.Tables["StopList"].Select("RD='tt4'", "")) {
				id = (int)dr["DownID"];
				sid = "s" + id;

				if (C.GetFloat(fmt.Compute(String.Format("SUM({0})", sid), "")) == 0)
					fmt.Columns[sid].ExtendedProperties.Add("Visible", false); //fmt.Columns.Remove(fmt.Columns[sid]);
				else
					fmt.Columns[sid].ExtendedProperties.Add("Visible", true);
				scol.Add(id, sid);
			}

			// добавляем суммарное значение по всем категориям
			string sumexpr = "";
			foreach (int s in scol.Keys)
				sumexpr = sumexpr == "" ? scol[s] : String.Format("{0}+{1}", sumexpr, scol[s]);
			fmt.Columns.Add("sum", typeof(int), sumexpr);
			fmt.Columns["sum"].Caption = "Total time";

			// дублируем суммарную колонку для вывода в процентах от основного KPI, 
			//		значения будут подставляться по OwnerDrawCell
			fmt.Columns.Add("sumPerc", typeof(int), "sum");
			fmt.Columns["sumPerc"].Caption = "Total %";

			// удаляем пустые строки
			for (int n = fmt.Rows.Count - 1; n >= 0; n--)
				if (C.GetInt(fmt.Rows[n]["sum"]) == 0)
					fmt.Rows[n].Delete();

	
			fmt.AcceptChanges();

			// Заполняем датасет таблицами
			FromToDS.Tables.Add(fmt);
			FromToDS.Tables.Add(DataSet.Tables["BrandName"].DefaultView.ToTable("BrandName", true, new string[] { "ID", "Name" }));
			FromToDS.Tables.Add(DataSet.Tables["FormatName"].DefaultView.ToTable("FormatName", true, new string[] { "ID", "Name" }));

			foreach (DataTable dtb in FromToDS.Tables)
				dtb.Locale = Thread.CurrentThread.CurrentCulture;

			// Для первой записи (т.е. неизвестно с какого бренда-формата был переход) добавляем Unknown
			DataRow unk = FromToDS.Tables["BrandName"].NewRow();
			unk["ID"] = 0;
			unk["Name"] = "Unknown";
			FromToDS.Tables["BrandName"].Rows.Add(unk);
			unk = FromToDS.Tables["FormatName"].NewRow();
			unk["ID"] = 0;
			unk["Name"] = "Unknown";
			FromToDS.Tables["FormatName"].Rows.Add(unk);

			// чтобы выводить названия брендов-форматов добавляем отношения к таблицам и далее названия из родительськой
			FromToDS.Relations.Add("fmt_brand_bid1", FromToDS.Tables["BrandName"].Columns["ID"], fmt.Columns["bid1"]);
			FromToDS.Relations.Add("fmt_format_fid1", FromToDS.Tables["FormatName"].Columns["ID"], fmt.Columns["fid1"]);
			FromToDS.Relations.Add("fmt_brand_bid2", FromToDS.Tables["BrandName"].Columns["ID"], fmt.Columns["bid2"]);
			FromToDS.Relations.Add("fmt_format_fid2", FromToDS.Tables["FormatName"].Columns["ID"], fmt.Columns["fid2"]);
			fmt.Columns["bname1"].Expression = "Parent(fmt_brand_bid1).Name";
			fmt.Columns["fname1"].Expression = "Parent(fmt_format_fid1).Name";
			fmt.Columns["bname2"].Expression = "Parent(fmt_brand_bid2).Name";
			fmt.Columns["fname2"].Expression = "Parent(fmt_format_fid2).Name";

			CreateFromToMatrixSummary(fmt);

			//Utils.CopyDatatableToClipboard(pvt);
		}

		private void CreateFromToMatrixSummary(DataTable fmt)
		{
			// Создаем сводную таблицу
			DataTable pvt = fmt.DefaultView.ToTable("pvt", true, new string[] { "bid1", "bid2", "fid1", "fid2", "bname1", "fname1", "bname2", "fname2" });
			pvt.Locale = Thread.CurrentThread.CurrentCulture;
			FromToDS.Tables.Add(pvt);
			FromToDS.Relations.Add("pvt_fmt",
				new DataColumn[] { pvt.Columns["bid1"], pvt.Columns["fid1"], pvt.Columns["bid2"], pvt.Columns["fid2"] },
				new DataColumn[] { fmt.Columns["bid1"], fmt.Columns["fid1"], fmt.Columns["bid2"], fmt.Columns["fid2"] });

			foreach (DataColumn col in fmt.Columns)
				if (col.ExtendedProperties.Contains("Visible")) {
					DataColumn scol = pvt.Columns.Add(col.ColumnName, typeof(int), String.Format("SUM(child.{0})", col.ColumnName));
					scol.Caption = col.Caption;
					scol.ExtendedProperties.Add("Visible", col.ExtendedProperties["Visible"]);
				}

			// Довавляем колонки со статистикой
			pvt.Columns.Add("sum", typeof(int), fmt.Columns["sum"].Expression);
			pvt.Columns["sum"].Caption = "Total time";
			pvt.Columns.Add("sumPerc", typeof(int), "sum");
			pvt.Columns["sumPerc"].Caption = "Total %";
			pvt.Columns.Add("cnt", typeof(int), "COUNT(child.bid1)");
			pvt.Columns["cnt"].Caption = "Events";
			pvt.Columns.Add("mean", typeof(int), "sum/cnt");
			pvt.Columns["mean"].Caption = "Average time";
			pvt.Columns.Add("max", typeof(int), "MAX(child.sum)");
			pvt.Columns["max"].Caption = "Max time";
			pvt.Columns.Add("min", typeof(int), "MIN(child.sum)");
			pvt.Columns["min"].Caption = "Min time";

		}

		/// <summary>
		/// Устанавливает набор описаний по простою, производству, сменам
		/// для текущего положения указателя мыши на графике 'Timeline'
		/// </summary>
		/// <param name="mouse_location"></param>
		/// <param name="usemins"></param>
		/// <returns>Возвращает true если данные установлены, т.е. Gap найден</returns>
		public bool BuildMouseData(DateTime mouse_location, bool usemins)
		{
			if (_mouseData.LastMouseLocation == mouse_location)
				return _mouseData.current_gap != null;
			_mouseData.LastMouseLocation = mouse_location;

			//DateTime mouse_location = this.mouse_location.AddMinutes(0.5f);

			DataRow[] DR;
			double speed = 0;
			string strspeed = "";

			// Define active dataset
			DataSet dts = DataSet;

			// first of all try to get speed for current minute:
			string dates = C.GetCDateFilter("dt", mouse_location, mouse_location.AddMinutes(1));
			if (dts.Tables.Contains("Minutes")) {
				//GetRowField(dts.Tables["Minutes"], dates, "Bottles", "dt")
				DR = dts.Tables["Minutes"].Select(dates, "dt");
				if (DR.Length > 0) {
					speed = Convert.ToDouble(DR[0]["Bottles"]);
					if (usemins)
						strspeed = string.Format("{0} {1}", Utils.GetFloatString((float)speed), Res.strUnitsPerMin);
					else
						strspeed = string.Format("{0} {1} ({2})", Utils.GetFloatString((float)speed * 60), Res.strUnitsPerHour, Utils.GetFloatString((float)speed));
					_mouseData.CurrentSpeed = string.Format("{0}: {1}", Res.strCurrSpeed, strspeed);
				}
			} else
				_mouseData.CurrentSpeed = "";

			// try to get data about gap
			DR = dts.Tables["Gaps"].Select(string.Format("dt1<=#{0:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", mouse_location), "dt1 DESC");
			if (DR.GetLength(0) > 0) {
				if (_mouseData.current_gap == DR[0])
					return true; // геп не поменялся, т.е. данные ещё актуальны, можно выходить
				_mouseData.current_gap = DR[0];
				_mouseData.DowntimeStart = (DateTime)_mouseData.current_gap["dt1"];
				_mouseData.DowntimeStop = (DateTime)_mouseData.current_gap["dt2"];
				_mouseData.DowntimeMins = _mouseData.DowntimeStop.Subtract(_mouseData.DowntimeStart).TotalMinutes + 1;
				_mouseData.DowntimeFullTime = Utils.GetTimeSpanString(Math.Round(_mouseData.DowntimeMins));
				_mouseData.DowntimeFullTime += usemins ? " " + Res.strMins : "";
				_mouseData.DowntimeInterval = string.Format("{0:t} - {1:t} ({2})", _mouseData.DowntimeStart,
					_mouseData.DowntimeStop, _mouseData.DowntimeFullTime);
				_mouseData.GapSpeed = (int)_mouseData.current_gap["GP"] / _mouseData.DowntimeMins;

				if (usemins)
					strspeed = string.Format("{0} {1}", Utils.GetFloatString((float)_mouseData.GapSpeed), Res.strUnitsPerMin);
				else
					strspeed = string.Format("{0} {1} ({2})", Utils.GetFloatString((float)_mouseData.GapSpeed * 60), Res.strUnitsPerHour, Utils.GetFloatString((float)_mouseData.GapSpeed));
				_mouseData.AverageSpeed = string.Format("{0}: {1}", Res.strAverageSpeed, strspeed);
			} else {
				_mouseData.DowntimeFullTime = "";
				_mouseData.DowntimeInterval = "";
				_mouseData.current_gap = null;
				return false;
			}

			DataRow dr = _mouseData.current_gap.GetParentRow("StopGap");
			if (dr != null) {
				_mouseData.current_downtime = dr;
				_mouseData.DowntimeID = (int)_mouseData.current_downtime["ID"];
				_mouseData.DowntimeStart = (DateTime)_mouseData.current_downtime["dt1"];
				_mouseData.DowntimeStop = (DateTime)_mouseData.current_downtime["dt2"];
				_mouseData.DowntimeMins = _mouseData.DowntimeStop.Subtract(_mouseData.DowntimeStart).TotalMinutes + 1;
				_mouseData.DowntimeFullTime = Utils.GetTimeSpanString((int)_mouseData.DowntimeMins);
				_mouseData.DowntimeFullTime += usemins ? " MonitorUnit.mins." : "";
				_mouseData.DowntimeFullName = dr["FullName"].ToString();
				_mouseData.DowntimeComment = dr["Comment"].ToString();
				_mouseData.DowntimeInterval = string.Format("{0:t} - {1:t} ({2})", _mouseData.DowntimeStart,
					_mouseData.DowntimeStop, _mouseData.DowntimeFullTime);
				_mouseData.DowntimeMark = "";
				foreach (string key in MU.marks_captions.Keys) {
					if (dr.Table.Columns.Contains(key) && dr[key] != DBNull.Value) {
						if (_mouseData.DowntimeMark == "")
							_mouseData.DowntimeMark = String.Format("{0}: {1}", MU.marks_captions[key], MU.marks[key][(int)dr[key]]);
						else
							_mouseData.DowntimeMark += String.Format("; {0}: {1}", MU.marks_captions[key], MU.marks[key][(int)dr[key]]);
					}
				}
			}
			if (dr == null) {
				_mouseData.current_downtime = null;
				_mouseData.DowntimeComment = "";
				_mouseData.DowntimeFullName = "";
				_mouseData.DowntimeMark = "";
				_mouseData.DowntimeID = -1;
			}

			dr = _mouseData.current_gap.GetParentRow("rel");
			if (dr != null && dr != _mouseData.current_production) {
				_mouseData.current_production = dr;
				DataRow drList = dr.GetParentRow("FormatListBrands");
				if (drList != null) _mouseData.ProductionName = drList["Name"].ToString();
				drList = dr.GetParentRow("BrandListBrands");
				if (drList != null) _mouseData.ProductionName += ", " + drList["Name"];
				_mouseData.ProductionID = (int)dr["ID"];
				_mouseData.ProductionStart = (DateTime)dr["dt1"];
				_mouseData.ProductionStop = (DateTime)dr["dt2"];//mouse_data.ProductionStart.AddMinutes(Convert.ToDouble(dr["TF"]));
				_mouseData.ProductionMins = _mouseData.ProductionStop.Subtract(_mouseData.ProductionStart).TotalMinutes + 1;
				_mouseData.ProductionFullTime = Utils.GetTimeSpanString(_mouseData.ProductionMins.ToString());
				_mouseData.ProductionInterval = string.Format("{0:t} - {1:t} ({2})",
					_mouseData.ProductionStart, _mouseData.ProductionStop, _mouseData.ProductionFullTime);
				_mouseData.ProductionGP = string.Format("{0}: {1:0.#} {2}", Res.strGP, dr["GP"], Res.strUnits);
				_mouseData.ProductionNP = string.Format("{0}: {1:0.#} {2}", Res.strNP, dr["NP"], Res.strUnits);
				_mouseData.ProductionVolume = string.Format("{0}: {1:0.#} {2}", Res.strVolume, dr["Vlm"], Res.strMeasure);
			}
			if (dr == null) {
				_mouseData.current_production = null;
				_mouseData.ProductionFullTime = "";
				_mouseData.ProductionName = "";
				_mouseData.ProductionInterval = "";
			}

			dr = _mouseData.current_gap.GetParentRow("ShGaps");
			if (dr != null && dr != _mouseData.current_shift) {
				_mouseData.current_shift = dr;
				DataRow drList = dr.GetParentRow("ShiftListShifts");
				if (drList != null) _mouseData.ShiftName = drList["Name"].ToString();
				_mouseData.ShiftID = (int)dr["ID"];
				_mouseData.ShiftStart = (DateTime)dr["dt1"];
				_mouseData.ShiftStop = (DateTime)dr["dt2"];//mouse_data.ProductionStart.AddMinutes(Convert.ToDouble(dr["TF"]));
				_mouseData.ShiftMins = _mouseData.ShiftStop.Subtract(_mouseData.ShiftStart).TotalMinutes + 1;
				_mouseData.ShiftFullTime = Utils.GetTimeSpanString(_mouseData.ShiftMins.ToString());
				_mouseData.ShiftInterval = string.Format("{0:t} - {1:t} ({2})",
					_mouseData.ShiftStart, _mouseData.ShiftStop, _mouseData.ShiftFullTime);
				_mouseData.ShiftVolume = string.Format("{0}: {1:0.#} {2}", Res.strVolume, dr["Vlm"], Res.strMeasure); // "Volume: " + dr["Vlm"].ToString();
			}
			if (dr == null) {
				_mouseData.current_shift = null;
				_mouseData.ShiftFullTime = "";
				_mouseData.ShiftName = "";
				_mouseData.ShiftInterval = "";
			}
			return true;
		}

        public bool BuildMouseData_Counter(DateTime mouse_location, bool usemins)
        {
            if (_mouseData.LastMouseLocation == mouse_location)
                return _mouseData.current_gap != null;
            _mouseData.LastMouseLocation = mouse_location;

            //DateTime mouse_location = this.mouse_location.AddMinutes(0.5f);

            DataRow[] DR;
            DataRow[] DR_brands;

            double speed = 0;
            string strspeed = "";

            // Define active dataset
            DataSet dts = DataSet;

            // first of all try to get speed for current minute:
            string dates = C.GetCDateFilter("dt", mouse_location, mouse_location.AddMinutes(1));
            /*
            if (dts.Tables.Contains("Minutes"))
            {
                //GetRowField(dts.Tables["Minutes"], dates, "Bottles", "dt")
                DR = dts.Tables["Minutes"].Select(dates, "dt");
                if (DR.Length > 0)
                {
                    speed = Convert.ToDouble(DR[0]["Bottles"]);
                    if (usemins)
                        strspeed = string.Format("{0} {1}", Utils.GetFloatString((float)speed), Res.strUnitsPerMin);
                    else
                        strspeed = string.Format("{0} {1} ({2})", Utils.GetFloatString((float)speed * 60), Res.strUnitsPerHour, Utils.GetFloatString((float)speed));
                    _mouseData.CurrentSpeed = string.Format("{0}: {1}", Res.strCurrSpeed, strspeed);
                }
            }
            else
                _mouseData.CurrentSpeed = "";
             */

            // try to get data about gap
           // DR = dts.Tables["Counters_value"].Select(string.Format("dt<=#{0:M/d/yy H:m}# AND dt>=#{0:M/d/yy H:m}#", mouse_location), "dt DESC");
            DR = dts.Tables["Counters_value"].Select(dates, "dt");
            if (DR.GetLength(0) > 0)
            {
                if (_mouseData.current_gap == DR[0])
                    return true; // геп не поменялся, т.е. данные ещё актуальны, можно выходить
                _mouseData.current_downtime = DR[0];

                _mouseData.current_gap = DR[0];
                _mouseData.DowntimeStart = (DateTime)_mouseData.current_gap["dt"];
                _mouseData.DowntimeStop = ((DateTime)_mouseData.current_gap["dt"]).AddMinutes(1);

                _mouseData.DowntimeMins = _mouseData.DowntimeStop.Subtract(_mouseData.DowntimeStart).TotalMinutes ;
                _mouseData.DowntimeFullTime = Utils.GetTimeSpanString(Math.Round(_mouseData.DowntimeMins));
                _mouseData.DowntimeFullTime += usemins ? " " + Res.strMins : "";
                _mouseData.DowntimeInterval = string.Format("{0:t} - {1:t} ({2})", _mouseData.DowntimeStart,     _mouseData.DowntimeStop, _mouseData.DowntimeFullTime);
                _mouseData.GapSpeed = (double)_mouseData.current_gap["value"] ; /// _mouseData.DowntimeMins;
               strspeed =  string.Format("{0}" , Utils.GetFloatString((float)_mouseData.GapSpeed) );
                _mouseData.CurrentSpeed = strspeed ;                                                           /// 

              //  if (usemins)
             //       strspeed = string.Format("{0} {1}", Utils.GetFloatString((float)_mouseData.GapSpeed), Res.strUnitsPerMin);
             //   else
             //       strspeed = string.Format("{0} {1} ({2})", Utils.GetFloatString((float)_mouseData.GapSpeed * 60), Res.strUnitsPerHour, Utils.GetFloatString((float)_mouseData.GapSpeed));
             //   _mouseData.AverageSpeed = string.Format("{0}: {1}", Res.strAverageSpeed, strspeed);
            }
            else
            {
                _mouseData.DowntimeFullTime = "";
                _mouseData.DowntimeInterval = "";
                _mouseData.current_gap = null;
                _mouseData.CurrentSpeed = "";
                _mouseData.GapSpeed = 0;
                _mouseData.DowntimeInterval = "";

                _mouseData.ProductionName = "";


                return false;
            }
            dates = string.Format("{0}<=#{1:M/d/yy H:m}# ", "dt_from", mouse_location);
            DR_brands = dts.Tables["Counters_Brands"].Select(dates);
            if (DR_brands.Length > 0)
            {
                _mouseData.ProductionName = (string)DR_brands[DR_brands.Length-1]["brand_name"];
            }
            else
            {
                _mouseData.ProductionName = "";
            }


            return true;
        }



		public int UpdateBW()
		{
			//DateTime dt1 = MonitorUnit.dtStart.Date;
			//dt1 = dt1.AddDays(-dt1.Day + 1);
			//DateTime dt2 = dt1.AddMonths(1);

			//int mnth = (dt1.Year - 2001) * 12 + dt1.Month;
			DateTime dt1 = DtStart.Date;
			dt1 = dt1.AddDays(-dt1.Day + 1);
			DateTime dt2 = dt1.AddMonths(1);

			int yr = dt1.Year;
			int mth = dt1.Month;
			int nres = -1;

			using (SqlConnection conn = new SqlConnection(SetConn(Connection))) {
				conn.Open();
				SqlCommand cmd = new SqlCommand("c_bw", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure };
				cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime, 4).Value = dt1;
				cmd.Parameters.Add("@dt2", SqlDbType.SmallDateTime, 4).Value = dt2;
				cmd.Parameters.Add("@year", SqlDbType.SmallInt, 2).Value = yr;
				cmd.Parameters.Add("@month", SqlDbType.SmallInt, 2).Value = mth;
				nres = cmd.ExecuteNonQuery();
			}
			return nres;
		}

		public void CalcTotalRow(string filter)
		{
			if (IsEmpty())
				return;
			if (Total != null && Total.Rows.Count > L) {
				Stopwatch sw1 = Stopwatch.StartNew(); 
				
				DataRow dr = Total.Rows[L];
				DataTable tb = DataSet.Tables["Gaps"];
				if (tb == null)
					return;
				dr["ID"] = L;
				string expr;
				foreach (string key in Kpi.Expr2.Keys) {
					if (Kpi.Expr2[key].Contains("child({0})."))
						expr = Kpi.Expr2[key].Replace("child({0}).", "");
					else if (key.StartsWith("m") && Kpi.Expr2[key].Contains("child({2}).")) {
						expr = Kpi.Expr2[key].Replace("child({2}).", "");
						dr[key] = C.GetFloat(DataSet.Tables["Stops"].Compute(expr, filter));//(expr, _filter));
						continue;
					} else
						continue;
					if (MU.Kpi.Expr2[key].Contains("{1}"))
						continue;
					dr[key] = C.GetFloat(tb.Compute(expr, filter));//(expr, _filter));
				}

				// Ищем цели
				string colname;
				foreach (DataColumn dc in Total.Columns)
					if (dc.ColumnName.StartsWith("t_")) {
						colname = dc.ColumnName.Replace("t_", "");
						if (TgtsYears.ContainsKey(Kpi.ID[colname]) && TgtsYears[Kpi.ID[colname]].ContainsKey(CurrYear))
							dr[dc.ColumnName] = TgtsYears[Kpi.ID[colname]][CurrYear];
					}

#if (DEBUG)
				// time meter
				Debug.WriteLine(string.Format("FillTbLinesRow: {0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif

			}
		}

		public void CalcTotalRowChilds()
		{

			// Обновляем "Вес" узла
			LineWeight = 0;
			foreach (MU mu in Childs)
				if (mu.IsOk)	//mu.IsOk
					LineWeight += mu.LineWeight;

			DataRow thisdr = C.GetRow(Total, "ID=" + L);

			float sum = 0;
			if (Kpi == null)
				return;
			foreach (string key in Kpi.Expr2.Keys) {
				if (!Kpi.Expr2[key].Contains("SUM"))
					continue;
				sum = 0;
				if (Kpi.Measure[key] == 1) {
					foreach (MU mu in Childs)
						if (mu.IsOk)
							sum += C.GetFloat(C.GetRowField(Total, "ID=" + mu.L, key)) * mu.LineWeight;
					thisdr[key] = sum > 0 ? sum / LineWeight : 0;
				} else {
					foreach (MU mu in Childs)
						if (mu.IsOk)	//mu.IsOk
							sum += C.GetFloat(C.GetRowField(Total, "ID=" + mu.L, key));
					if (sum > 0)
						thisdr[key] = sum;
				}
			}

			if (_dbWorker != null)
				_dbWorker.RunWorkerAsync(null);
		}

		public double GetTarget(string nominator, string denominator, int year)
		{
			string key = Kpi.FindKey(nominator, denominator);
			if (string.IsNullOrEmpty(key)) {
				foreach (var v in MU.Kpi.ID.Keys)
				{
					if (v.StartsWith("T")) {
						string key2 = Kpi.FindKey(nominator, v);
						if (string.IsNullOrEmpty(key2))
							continue;
						List<string> dif = Kpi.Difs(denominator, v);
						key = key2;
						break;
					}
				}
			}

			if (string.IsNullOrEmpty(key))
				return 0;

			int id = Kpi.ID[key];

			if (id > 0 && TgtsYears.ContainsKey(id))
				if (TgtsYears[id].ContainsKey(year))
					return TgtsYears[id][year];
			return 0;
		}
		public double GetTarget(string nominator, int year)
		{
			string key = "";
			foreach (var v in MU.Kpi.ID.Keys) {
				if (v.StartsWith("T")) {
					key = Kpi.FindKey(nominator, v);
					if (!string.IsNullOrEmpty(key))
						break;

				}
			}
			if (string.IsNullOrEmpty(key))
				return 0;

			int id = Kpi.ID[key];

			if (id > 0 && TgtsYears.ContainsKey(id))
				if (TgtsYears[id].ContainsKey(year))
					return TgtsYears[id][year];
			return 0;
		}
		public double GetBaseTimeDivider(string base1, string base2)
		{
			if (!MU.Kpi.DenomParts.ContainsKey(base1) || !MU.Kpi.DenomParts.ContainsKey(base2))
				return 1;
			string[] set1 = MU.Kpi.DenomParts[base1].Split(new string[] { ";" }, StringSplitOptions.None);
			string[] set2 = MU.Kpi.DenomParts[base2].Split(new string[] { ";" }, StringSplitOptions.None);

			List<string> res = new List<string>();
			if (set1.Length > set2.Length) {
				foreach (var v in set1)
					if (!Array.Exists<string>(set2, el => el == v))
						res.Add(v);
			} else {
				foreach (var v in set2)
					if (!Array.Exists<string>(set1, el => el == v))
						res.Add(v);
			}

			return 0;
		}

		internal bool CheckToValidDate()
		{
			if (DateTime.Now.Subtract(EndCorrected).TotalDays > DaysToLive) {
				MessageBox.Show(string.Format("Даннные за период до {0:d} закрыты для редактирования",
					DateTime.Now.AddDays(-DaysToLive)),
					"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			} else
				return true;
		}

		internal bool CheckDate(UserPermit permit, DateTime  dt)
		{
			DateTime proposed = GetEditValidStartDate(permit);
			return proposed < dt;


			if (DateTime.Now.Subtract(EndCorrected).TotalDays > DaysToLive) {
				MessageBox.Show(string.Format("Даннные за период до {0:d} закрыты для редактирования",
					DateTime.Now.AddDays(-DaysToLive)),
					"Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			} else
				return true;
		}

		internal DateTime GetEditValidStartDate(UserPermit permit)
		{
			DateTime proposed;
			if (permit == UserPermit.Foreman || permit == UserPermit.LocalForeman) {
				proposed = DateTime.Now.AddDays(-DaysToLive);
				DateTime proposedDate = proposed.Date;

				DateTime dt1, dt2;
				dt2 = proposed.Date;
				int mPrev = 0;
				foreach (int m in ShiftMins) {
					dt1 = dt2;
					dt2 = proposedDate.AddMinutes(m);
					mPrev = m;
					if (dt1 < proposed && proposed < dt2) {
						if (dt1 == proposed.Date)
							proposed = dt1.AddMinutes(-(1440 - ShiftMins[ShiftMins.Count - 1]));
						else
							proposed = dt1;
						return proposed;
					}
				}
				return dt2;
			} else if (permit == UserPermit.LocalAdmin) {
				proposed = DateTime.Now.Date;
				if (proposed.Day < 11)
					proposed.AddMonths(-1);
				proposed = proposed.AddDays(-proposed.Day + 1);
				return proposed;
			} else if (permit == UserPermit.Admin) {
				return DateTime.Now.AddYears(-5);
			}
			return DateTime.Now;
		}



		#endregion | public functions  |


		#region | private functions |


        private void LoadLists_Counter(bool reload)
        {
           // Stopwatch sw1 = Stopwatch.StartNew();
#if (DEBUG)

#endif
            int n = L;
            DataSet = new DataSet();
            if ( 1 == 2 ) //    (!ListsCash.ContainsKey(ID) || reload  )
            {
                DataSet lists = new DataSet(String.Format("{0}_{1}", ID, LineName));

                DataSet dts;
                dts = new DataSet();
                #region | locals           |

                //dts = Utils.DeserializeDataObject(ID.ToString(), Config.MonitorUnits[ID].CashValidDate) as DataSet;
                if (dts == null)
                {
                    using (SqlConnection conn = new SqlConnection(SetConn(Connection)))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("SelectCounters", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure })
                        {
                            dts = new DataSet();
                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "SelectCounters" });
                            }
                            //Utils.SerializeDataObject(ID.ToString(), dts);
                        }
                    }
                }
                lists.Merge(dts, true, MissingSchemaAction.AddWithKey);

                #endregion | locals           |



               // Debug.WriteLine("LoadLists_count7: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
            }
#if (DEBUG)
            //throw new Exception("Test exception from LoadLists()");
#endif

       //     DataSet.Merge(ListsCash[ID], true, MissingSchemaAction.AddWithKey);




#if (DEBUG)
           // Debug.WriteLine("LoadLists_coun: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
        }


		private void LoadLists(bool reload)
		{
			Stopwatch sw1 = Stopwatch.StartNew();
#if (DEBUG)

#endif
			int n = L;
			DataSet = new DataSet();
			if (!ListsCash.ContainsKey(ID) || reload) {
				DataSet lists = new DataSet(String.Format("{0}_{1}", ID, LineName));

				#region | kpi              |

				//Debug.WriteLine("LoadLists_01: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
				DataSet dts = Utils.DeserializeDataObject(ParentKpi, Config.MonitorUnits[ParentIdKpi].CashValidDate) as DataSet;
				//Debug.WriteLine("LoadLists_02: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
				if (dts == null) {
					using (SqlConnection conn = new SqlConnection(SetConn(Config.Units[ParentIdKpi].Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("gkpi", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 100 }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "KPI", "MarkList", "MarkBandList" });
							}
							Utils.SerializeDataObject(ParentKpi, dts);
						}
					}
				}
				lists.Merge(dts, true, MissingSchemaAction.AddWithKey);
				Debug.WriteLine("LoadLists_04: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | kpi              |

				#region | config           |

				dts = Utils.DeserializeDataObject(ParentConfig, Config.MonitorUnits[ParentIdConfig].CashValidDate) as DataSet;
				if (dts == null) {
					using (SqlConnection conn = new SqlConnection(SetConn(Config.Units[ParentIdConfig].Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("gcfg", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "Config" });
							}
							Utils.SerializeDataObject(ParentConfig, dts);
						}
					}
				}

				lists.Merge(dts, true, MissingSchemaAction.AddWithKey);
				Debug.WriteLine("LoadLists_1: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | config           |

				#region | stops            |

				dts = Utils.DeserializeDataObject(ParentStops, Config.MonitorUnits[ParentIdStops].CashValidDate) as DataSet;
				if (dts == null) {
					using (SqlConnection conn = new SqlConnection(SetConn(Config.Units[ParentIdStops].Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("gdwn", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "gstops" });
							}
							Utils.SerializeDataObject(ParentStops, dts);
						}
					}
				}
				if (dts == null || dts.Tables.Count == 0)
					throw new Exception("Downtimes list was not loaded");
				else
					lists.Merge(dts, true, MissingSchemaAction.AddWithKey);

				Debug.WriteLine("LoadLists_2: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | stops            |

				#region | sku              |

				dts = Utils.DeserializeDataObject(ParentSku, Config.MonitorUnits[ParentIdSku].CashValidDate) as DataSet;
				if (dts == null) {
					using (SqlConnection conn = new SqlConnection(SetConn(Config.Units[ParentIdSku].Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("gsku", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "gbrands", "gformats" });
							}
							Utils.SerializeDataObject(ParentSku, dts);
						}
					}
				}
				lists.Merge(dts, true, MissingSchemaAction.AddWithKey);
				Debug.WriteLine("LoadLists_3: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | sku              |

				#region | shifts           |

				dts = Utils.DeserializeDataObject(ParentShifts, Config.MonitorUnits[ParentIdShifts].CashValidDate) as DataSet;
				// Проверка (dts.Tables.Contains("gshifts")) нужна только временно
				if (dts == null || !dts.Tables.Contains("gshifts")) {
					using (SqlConnection conn = new SqlConnection(SetConn(Config.Units[ParentIdShifts].Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("gshf", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "gshifts" }); //ShiftName or gshifts
							}
							Utils.SerializeDataObject(ParentShifts, dts);
						}
					}
				}
				lists.Merge(dts, true, MissingSchemaAction.AddWithKey);
				Debug.WriteLine("LoadLists_4: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | shifts           |

				#region | locals           |

				dts = Utils.DeserializeDataObject(ID.ToString(), Config.MonitorUnits[ID].CashValidDate) as DataSet;
				if (dts == null) {
					using (SqlConnection conn = new SqlConnection(SetConn(Connection))) {
						conn.Open();
						using (SqlCommand cmd = new SqlCommand("v_lists", conn) { CommandTimeout = 100, CommandType = CommandType.StoredProcedure }) {
							dts = new DataSet();
							using (SqlDataReader dr = cmd.ExecuteReader()) {
								dts.Load(dr, LoadOption.OverwriteChanges, new string[] { "Targets", "rstops", "rbrands", "rformats", "rshifts" });
							}
							Utils.SerializeDataObject(ID.ToString(), dts);
						}
					}
				}
				lists.Merge(dts, true, MissingSchemaAction.AddWithKey);

				#endregion | locals           |

				#region | global relations |

				lists.Relations.Add("row1", lists.Tables["gformats"].Columns["ID"], lists.Tables["rformats"].Columns["id"]);
				lists.Tables["gformats"].Columns.Add("Speed", typeof(float), "MAX(Child.rate)");
				lists.Tables["gformats"].Columns.Add("rvalid", typeof(bool), "MAX(Child.valid)");
				DataView dv = new DataView(lists.Tables["gformats"]) { RowFilter = "count(Child(row1).id)>0" };
				DataTable dtf = dv.ToTable();
				dtf.TableName = "FormatName";
				lists.Tables.Add(dtf);

				lists.Relations.Add("row2", lists.Tables["gbrands"].Columns["ID"], lists.Tables["rbrands"].Columns["id"]);
				lists.Tables["gbrands"].Columns.Add("rvalid", typeof(bool), "MAX(Child.valid)");
				dv = new DataView(lists.Tables["gbrands"]) { RowFilter = "count(Child(row2).id)>0" };
				dtf = dv.ToTable();
				dtf.TableName = "BrandName";
				lists.Tables.Add(dtf);

				lists.Relations.Add("row3", lists.Tables["gstops"].Columns["DownID"], lists.Tables["rstops"].Columns["id"]);
				lists.Tables["gstops"].Columns.Add("rvalid", typeof(bool), "MAX(Child.valid)");
				dv = new DataView(lists.Tables["gstops"]) { RowFilter = "count(Child(row3).id)>0" };
				dtf = dv.ToTable();
				dtf.TableName = "StopList";
				//dtf.Columns.Add("Name", typeof(string));
				//string expr = defaultlanguage ? "EName" : "IIF(Value is NULL, EName, Value)";
				//dtf.Columns["Name"].Expression = expr;
				//dtf.Columns.Add("FullName", typeof(string));
				//DownLevels = 0;
				//FillFullNameColumn(0, 0, "", dtf.Select("TopID is NULL"));
				lists.Tables.Add(dtf);

				lists.Relations.Add("row4", lists.Tables["gshifts"].Columns["ID"], lists.Tables["rshifts"].Columns["id"]);
				lists.Tables["gshifts"].Columns.Add("rvalid", typeof(bool), "MAX(Child.valid)");
				dv = new DataView(lists.Tables["gshifts"]) { RowFilter = "count(Child(row4).id)>0" };
				dtf = dv.ToTable();
				dtf.TableName = "ShiftName";
				lists.Tables.Add(dtf);
				Debug.WriteLine("LoadLists_6: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				#endregion | global relations |

				if (ListsCash.ContainsKey(ID))
					ListsCash.Remove(ID);
				ListsCash.Add(ID, lists);

				#region | static fields    |
				// заполняем статические поля (единые для всех линий)
				if (tbKPI == null || tbKPI.Rows.Count == 0) {
					tbKPI = lists.Tables["KPI"];

					if (UseDefaultLanguage) {
						if (tbKPI.Columns.Contains("NameID") && tbKPI.Columns.Contains("Value") && !tbKPI.Columns.Contains("Name")) {
							tbKPI.Columns.Add("Name", typeof(string), "EName");
							lists.Tables["MarkList"].Columns.Add("Name", typeof(string), "EName");
							lists.Tables["MarkBandList"].Columns.Add("Name", typeof(string), "EName");
						}
					} else {
						if (tbKPI.Columns.Contains("NameID") && tbKPI.Columns.Contains("Value") && !tbKPI.Columns.Contains("Name")) {
							tbKPI.Columns.Add("Name", typeof(string), "IIF(Value is NULL, EName, Value)");
							lists.Tables["MarkList"].Columns.Add("Name", typeof(string), "IIF(Value is NULL, EName, Value)");
							lists.Tables["MarkBandList"].Columns.Add("Name", typeof(string), "IIF(Value is NULL, EName, Value)");
						}
					}

					Kpi = new KPI(tbKPI);

					#region | tbLines  |

					DataColumn dc;

					foreach (string key in Kpi.Expr2.Keys) {
						dc = Total.Columns.Add(key, typeof(float), "");
						dc.DefaultValue = 0;
						if (!Kpi.Expr2[key].Contains("SUM") && !Kpi.Expr2[key].Contains("COUNT"))
							dc.Expression = Kpi.Expr2[key];
						// добавляем колонку с целями если это проценты
						if (MU.Kpi.Measure.ContainsKey(key) && MU.Kpi.Measure[key] == 2)
							dc = Total.Columns.Add("t_" + key, typeof(float));
					}

					TotalFilt = Total.Clone();

					#endregion | tbLines  |

					#region | config   |

					// В этой ветке конфига заполняем только статические (единые для всех) переменные

					if (ListsCash[ID].Tables["Config"].Rows.Count > 0) {
						/* Пример таблицы g_cfg

		1	MinIdleTime				5	1	True
		2	CorrectionTime		-240	1	True
		3	ShiftsTime		480;960	1	True
		4	LastVersion	3.0.1001.1	1	False
		5	TimeToLive				7	1	False
		6	WinLogins				0	1	True
		7	CheckDayBefore			0	1	False
		8	Mark1						0	1	False
		9	Mark2						0	1	False
		10	NeedToComment	tt2;tt3	1	False

						 */

						int val = 5;
						////if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=1", "Value"), out val))
						////   MinIdleTime = val;
						////else
						////   MinIdleTime = 5;

						////if (int.TryParse(C.GetRowField(DataSet.Tables["Config"], "id=2", "Value"), out val))
						////   CorrectionTime = val;
						////else
						////   CorrectionTime = 0;

						////ShiftMins.Clear();
						////string shiftstime = C.GetRowField(DataSet.Tables["Config"], "id=3", "Value");
						////foreach (string sh in shiftstime.Split(new char[] { ';' }))
						////   if (int.TryParse(sh, out val))
						////      ShiftMins.Add(val);

						////// если время смен не извлекли то добавляем два значения по умолчанию 8:00 и 20:00
						////if (ShiftMins.Count == 0) {
						////   ShiftMins.Add(480);
						////   ShiftMins.Add(960);
						////}

						////if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=5", "Value"), out val))
						////   DaysToLive = val;
						////else
						////   DaysToLive = 10;

						if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=6", "Value"), out val))
							UseWinAuthentication = val == 1;
						else
							UseWinAuthentication = false;

						if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=8", "Value"), out val))
							UseMark1 = val == 1;
						else
							UseMark1 = true;

						if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=9", "Value"), out val))
							UseMark2 = val == 1;
						else
							UseMark2 = true;

						string needtocommentkeys = C.GetRowField(ListsCash[ID].Tables["Config"], "id=10", "Value");
						if (needtocommentkeys.Length > 0) {
							string[] keys = needtocommentkeys.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
							foreach (string key in keys)
								if (Kpi.Roots.ContainsKey(key) && !Kpi.NeedToComment.Contains(key))
									Kpi.NeedToComment.Add(key);
						}
					}

					#endregion | config   |

					#region | marks    |

					marks.Clear();
					marks_captions.Clear();
					Dictionary<int, string> mark = new Dictionary<int, string>();
					string mrk;
					int id;
					foreach (DataRow drMB in lists.Tables["MarkBandList"].Rows) {
						mark = new Dictionary<int, string>();
						if (int.TryParse(drMB["ID"].ToString(), out id)) {
							mrk = "Mark" + drMB["ID"];
							if ((mrk == "Mark1" && !UseMark1) || (mrk == "Mark2" && !UseMark2))
								continue;
							marks.Add(mrk, mark);
							//string name = defaultlanguage ? "EName" : "Value";
							marks_captions.Add(mrk, drMB["Name"].ToString());
							foreach (DataRow dr in lists.Tables["MarkList"].Select("BandID=" + drMB["ID"])) {
								//GetColorMarks(dr);
								mark.Add((int)dr["val"], dr["Name"].ToString());
							}
						}
					}

					#endregion | marks    |

					dv.Dispose();
				}
				#endregion

				Debug.WriteLine("LoadLists_7: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			}
#if (DEBUG)
			//throw new Exception("Test exception from LoadLists()");
#endif

			DataSet.Merge(ListsCash[ID], true, MissingSchemaAction.AddWithKey);

			#region | config    |

			// В этой ветке конфига заполняем поля экземляров (статические заполнили чуть выше)

			if (DataSet.Tables["Config"].Rows.Count > 0) {
				int val = 5;

				if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=1", "Value"), out val))
					MinIdleTime = val;
				else
					MinIdleTime = 5;

				if (int.TryParse(C.GetRowField(DataSet.Tables["Config"], "id=2", "Value"), out val))
					CorrectionTime = val;
				else
					CorrectionTime = 0;

				ShiftMins.Clear();
				string shiftstime = C.GetRowField(DataSet.Tables["Config"], "id=3", "Value");
				foreach (string sh in shiftstime.Split(new char[] { ';' }))
					if (int.TryParse(sh, out val))
						ShiftMins.Add(val);

				// если время смен не извлекли то добавляем два значения по умолчанию 8:00 и 20:00
				if (ShiftMins.Count == 0) {
					ShiftMins.Add(480);
					ShiftMins.Add(960);
				}

				if (int.TryParse(C.GetRowField(ListsCash[ID].Tables["Config"], "id=5", "Value"), out val))
					DaysToLive = val;
				else
					DaysToLive = 10;

			}

			#endregion | config    |

			#region | targets  |

			int kpiid;
			int year = DateTime.Now.Year;
			int firstyear;
			double target, currtarget;
			TgtsYears.Clear();
			DataTable dtb = ListsCash[ID].Tables["Targets"].DefaultView.ToTable(true, new string[] { "KpiID" });
			foreach (DataRow dr in dtb.Rows) {
				kpiid = (int)dr[0];
				//this.Targets.Add(kpiid, C.GetFloat(C.GetRowField(this.DataSet.Tables["Targets"], "KpiID=" + kpiid, "FromDate DESC", "Target")));
				DataRow[] DR = ListsCash[ID].Tables["Targets"].Select("KpiID=" + kpiid, "FromDate");
				if (DR.Length > 0) {
					Dictionary<int, double> d = new Dictionary<int, double>();
					firstyear = (int)DR[0]["Year"];
					target = Convert.ToDouble(DR[0]["Target"]);
					d.Add(firstyear, target);
					for (n = firstyear + 1; n <= year; n++) {
						currtarget = C.GetFloat(C.GetRowField(ListsCash[ID].Tables["Targets"], String.Format("KpiID={0} AND Year={1}", kpiid, n), "", "Target"));
						if (currtarget != 0)
							target = currtarget;
						d.Add(n, target);
					}
					TgtsYears.Add(kpiid, d);
				}
			}
			//dtb = ListsCash[ID].Tables["Targets"].DefaultView.ToTable(true, new string[] { "Year" });
			//foreach (DataRow dr in dtb.Rows) {
			//   year = (int)dr[0];
			//   YearsTgts.Add(year, new Dictionary<string, double>());
			//   DataRow[] DR = ListsCash[ID].Tables["Targets"].Select("Year=" + year, "FromDate");
			//   foreach (DataRow row in DR) {
			//      kpiid = (int)row["KpiID"];
			//      target = Convert.ToDouble(row["Target"]);
			//      YearsTgts[year].Add(Kpi.Sign[kpiid], target);
			//   }
			//}

			#endregion | targets  |

			DataSet.Tables.Remove("Config");
#if (DEBUG)
			Debug.WriteLine("LoadLists3: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private static string SetConn(string cn)
		{
#if (DEBUG)
			return cn + ";Connect timeout=5"; //";Packet Size=16384;Connect timeout=10";
#else
			//return cn + ";Connect timeout=10"; //";Packet Size=16384;Connect timeout=10";
			return cn;
#endif
		}

		private int GetGP(DateTime dt1, DateTime dt2)
		{
			//int res = 0;
			string dates = C.GetCDateFilter("dt", dt1, dt2);
			//Int32.TryParse(lu[L].DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates).ToString(), out res);
			//return res;
			return C.GetInt(DataSet.Tables["Minutes"].Compute("SUM(Bottles)", dates));
		}

		private float GetSeconds(DateTime dt1, DateTime dt2)
		{
			//int res = 0;
			string dates = C.GetCDateFilter("dt", dt1, dt2);
			//Int32.TryParse(lu[L].DataSet.Tables["Minutes"].Compute("SUM(Seconds)", dates).ToString(), out res);
			//return res/60F;
			return C.GetInt(DataSet.Tables["Minutes"].Compute("SUM(Seconds)", dates)) / 60F;
		}

		private bool YearsCheckTime()
		{
			return DateTime.Now.Subtract(_yearValidDate).TotalHours <= 1;
		}

		private void GetLightShorts()
		{
			return;
			if (!DataSet.Tables.Contains("Minutes"))
				return;
			DataTable dtb = DataSet.Tables["Minutes"];

			// создаем таблицу LightStops
			DataTable dtbl = new DataTable("LightStops");
			dtbl.Columns.Add("dt1", typeof(DateTime));
			dtbl.Columns.Add("dt2", typeof(DateTime));
			dtbl.Columns.Add("sec", typeof(int));
			dtbl.Columns.Add("valid", typeof(bool));
			DataSet.Tables.Add(dtbl);

			// заполняем таблицу LightStops
			DateTime dt = new DateTime(1, 1, 1), dt1, dt2;
			int sec = 0, sec1 = 1, sec2;
			bool islight = false;
			foreach (DataRow dr in dtb.Rows) {
				// определяем текущие значения
				dt2 = (DateTime)dr["DT"];
				sec2 = (byte)dr["Seconds"];
				if (sec2 > 0 && sec2 < 60) {
					if (sec1 == 0) {
						// выставляем триггер короткой остановки
						islight = true;
						dt = dt2;
					}
					if (islight)
						sec += sec2;
				} else if (sec2 == 60) {
					// полный останов > сбрасываем триггер
					islight = false;
					sec = 0;
				} else if (sec2 == 0 && islight) {
					// добавляем новую короткую остановку
					DataRow newrow = dtbl.NewRow();
					newrow["dt1"] = dt;
					newrow["dt2"] = dt2.AddMinutes(-1);
					newrow["sec"] = sec;
					newrow["valid"] = true;
					dtbl.Rows.Add(newrow);
					// сбрасываем триггер
					islight = false;
					sec = 0;
				}
				// запоминаем текущие значения для следующего прохода
				dt1 = dt2;
				sec1 = sec2;
			}
		}

		private void InitAutoCompletes()
		{
			if (_autoCompletes == null)
				_autoCompletes = new AutoCompleteStringCollection();
		}

		static private void NormalizeGaps(DataTable dtb)
		{
			List<GapRecord> gaps = new List<GapRecord>();
			List<DataRow> newrows = new List<DataRow>();
			DateTime dt1, dt2, dt1p, dt2p = _dtEmpty;
			DataRow[] DR = dtb.Select("", "dt1, dt2");
			DataRow prev = null, next;
			GapRecord dwnrec;

			if (Mins > maxDetailedMins)
				foreach (DataRow dr in dtb.Rows)
					GapRecord.CheckDt2(dr);

			foreach (DataRow dr in DR) {
				dt1 = (DateTime)dr["dt1"];
				dt2 = (DateTime)dr["dt2"];
				if (dt2p >= dt1) {
					dwnrec = new GapRecord(prev, dt1);
					gaps.Add(dwnrec);
				}
				// Если между строками есть зазор по времени
				while (dt1 > dt2p.AddMinutes(1) && gaps.Count > 0) {
					next = gaps[0].SplitRow(dt2p.AddMinutes(1), dt1);
					if (next != null) {
						newrows.Add(next);
						dt1p = (DateTime)next["dt1"];
						dt2p = (DateTime)next["dt2"];
						prev = next;
					}
					if (gaps[0].TF <= 0) gaps.RemoveAt(0);
				}
				prev = dr;
				dt1p = dt1;
				dt2p = dt2;
			}

			// Добавляем все остатки что не удалось распарсить
			while (gaps.Count > 0) {
				next = gaps[0].SplitRow(dt2p.AddMinutes(1), DtEnd);
				if (next != null) {
					dt2p = (DateTime)next["dt2"];
					newrows.Add(next);
				}
				gaps.RemoveAt(0);
			}

			foreach (DataRow dr in newrows)
				if (dr != null)
					dtb.Rows.Add(dr);
		}

		private void SetDateBack()
		{


		}

		private void PrepareForEditMode()
		{
			// dummy columns for update
			DataSet.Tables["Stops"].Columns.Add("_dt1", typeof(DateTime));
			DataSet.Tables["Brands"].Columns.Add("_dt1", typeof(DateTime));
			DataSet.Tables["Shifts"].Columns.Add("_dt1", typeof(DateTime));
			DataSet.Tables["Stops"].Columns.Add("_dt2", typeof(DateTime));
			DataSet.Tables["Brands"].Columns.Add("_dt2", typeof(DateTime));
			DataSet.Tables["Shifts"].Columns.Add("_dt2", typeof(DateTime));

			////dts.Tables["Stops"].Columns["ID"].AutoIncrement = true;
			//////dts.Tables["Stops"].Columns["ID"].AutoIncrementSeed = C.GetInt(dts.Tables["Stops"].Compute("Max(ID)", "")) + 100;		//1 + (int)dts.Tables["Stops"].Compute("MAX(ID)", "");
			////dts.Tables["Brands"].Columns["ID"].AutoIncrement = true;
			//////dts.Tables["Brands"].Columns["ID"].AutoIncrementSeed = C.GetInt(dts.Tables["Brands"].Compute("Max(ID)", "")) + 100;	//1 + (int)dts.Tables["Brands"].Compute("MAX(ID)", "");
			////dts.Tables["Shifts"].Columns["ID"].AutoIncrement = true;
			//////dts.Tables["Shifts"].Columns["ID"].AutoIncrementSeed = C.GetInt(dts.Tables["Shifts"].Compute("Max(ID)", "")) + 100;	//1 + (int)dts.Tables["Shifts"].Compute("MAX(ID)", "");

			DataSet.AcceptChanges();
		}

		private void PrepareForCompareMode()
		{
			if (Mins > minTrendMins) {
				//Monitorthis u = dts.ExtendedProperties["Mthis"] as Monitorthis;
				DataSet dts = DataSet;
				System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;
				string year, week;
				int tc = UseTimecorrection ? CorrectionTime : 0;
				//////////////////////////////////////////////////////////////
				DateTime start, dt2 = new DateTime(1, 1, 1);
				DataRow dr, newrow, tosplitrow;

				Stopwatch sw1 = Stopwatch.StartNew();

				//TableFreeze tf = new TableFreeze(dts.Tables["Gaps"]);

				// Делим слишком длинные записи
				DataRow[] DR = dts.Tables["Gaps"].Select("TF>1440");
				while (DR.Length > 0) {
					dr = DR[0];
					newrow = GapRecord.DateSplitRow(dr, tc);
					if (newrow != null) {
						dts.Tables["Gaps"].Rows.Add(newrow);
						DR = dts.Tables["Gaps"].Select("TF>1440");
					}
				}
				Debug.WriteLine("PrepareForCompareMode_1: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				// Делим записи по времени смены суток
				List<DataRow> rows = new List<DataRow>();
				if ((Mode & PeriodMode.Weeks) == PeriodMode.Weeks) {
					DR = dts.Tables["Gaps"].Select();
					foreach (DataRow row in DR) {
						newrow = GapRecord.DateSplitRow(row, tc);
						if (newrow != null)
							rows.Add(newrow);
					}
				} else {
					DateTime dt = DtStart.AddMinutes(tc);
					dt = DtStart.Date;
					dt = dt.AddDays(-dt.Day + 1);
					int ccnt = 0;
					while (dt < DtEnd) {
						tosplitrow = C.GetRow(dts.Tables["Gaps"], string.Format("dt1<#{0:M/d/yy H:m}# AND dt2>=#{0:M/d/yy H:m}#", dt.AddMinutes(tc)));
						Debug.WriteLine(String.Format("{0} PrepareForCompareMode: {1:0.000}", ccnt++, sw1.ElapsedMilliseconds / 1000F));
						dt = dt.AddMonths(1);
						if (tosplitrow == null)
							continue;
						newrow = GapRecord.DateSplitRow(tosplitrow, tc);
						if (newrow != null)
							rows.Add(newrow);
					}
				}

				foreach (DataRow row in rows)
					DataSet.Tables["Gaps"].Rows.Add(row);
				//tf.Worm();

				Debug.WriteLine("PrepareForCompareMode_2: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

				if ((int)(MU.Mode & MU.PeriodMode.Days) > 0) {
					dts.Tables["Gaps"].Columns.Add("DayList", typeof(string));
					dts.Tables["Stops"].Columns.Add("DayList", typeof(string));
				}
				if ((int)(MU.Mode & MU.PeriodMode.Weeks) > 0) {
					dts.Tables["Gaps"].Columns.Add("WeekList", typeof(string));
					dts.Tables["Stops"].Columns.Add("WeekList", typeof(string));
				}
				if ((int)(MU.Mode & MU.PeriodMode.Months) > 0) {
					dts.Tables["Gaps"].Columns.Add("MonthList", typeof(string));
					dts.Tables["Stops"].Columns.Add("MonthList", typeof(string));
				}
				if ((int)(MU.Mode & MU.PeriodMode.Quarters) > 0) {
					dts.Tables["Gaps"].Columns.Add("QuarterList", typeof(string));
					dts.Tables["Stops"].Columns.Add("QuarterList", typeof(string));
				}
				if ((int)(MU.Mode & MU.PeriodMode.Years) > 0) {
					dts.Tables["Gaps"].Columns.Add("YearList", typeof(string));
					dts.Tables["Stops"].Columns.Add("YearList", typeof(string));
				}

				foreach (DataRow r in dts.Tables["Gaps"].Rows) {
					start = Convert.ToDateTime(r["dt1"]).AddMinutes(-tc);
					week = ci.Calendar.GetWeekOfYear(start, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString();
					year = start.ToString("yyyy");
					if ((int)(MU.Mode & MU.PeriodMode.Days) > 0) r["DayList"] = start.ToString("d", ci); //start.ToString("yyyy.MM.dd", ci);
					if ((int)(MU.Mode & MU.PeriodMode.Weeks) > 0) r["WeekList"] = String.Format("{0} W{1}", year, week);
					if ((int)(MU.Mode & MU.PeriodMode.Months) > 0) r["MonthList"] = start.ToString("MMM yyyy");
					if ((int)(MU.Mode & MU.PeriodMode.Quarters) > 0) r["QuarterList"] = String.Format("{0} Q{1}", year, (start.Month - 1) / 3 + 1);
					if ((int)(MU.Mode & MU.PeriodMode.Years) > 0) r["YearList"] = year;
				}
				Debug.WriteLine("PrepareForCompareMode (Gaps): " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
				foreach (DataRow r in dts.Tables["Stops"].Rows) {
					start = Convert.ToDateTime(r["dt1"]).AddMinutes(-tc);
					week = ci.Calendar.GetWeekOfYear(start, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString();
					year = start.ToString("yyyy");
					if ((int)(MU.Mode & MU.PeriodMode.Days) > 0) r["DayList"] = start.ToString("d", ci); //start.ToString("yyyy.MM.dd", ci);
					if ((int)(MU.Mode & MU.PeriodMode.Weeks) > 0) r["WeekList"] = String.Format("{0} W{1}", year, week);
					if ((int)(MU.Mode & MU.PeriodMode.Months) > 0) r["MonthList"] = start.ToString("MMM yyyy");
					if ((int)(MU.Mode & MU.PeriodMode.Quarters) > 0) r["QuarterList"] = String.Format("{0} Q{1}", year, (start.Month - 1) / 3 + 1);
					if ((int)(MU.Mode & MU.PeriodMode.Years) > 0)  r["YearList"] = year;
				}
				Debug.WriteLine("PrepareForCompareMode (Stops): " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			}
		}

		private void AddDownLevel(int level, string col)
		{
			if (!DownColumns.Values.Contains(col)) {
				_downLevels++;
				DownColumns.Add(level, col);
			}
		}

		private void FillFullNameColumn(DataTable dtb)
		{
			DownLevels = 0;
			branchs.Clear();
			int topid;
			foreach (DataRow dr in dtb.Select("", "TopId, DownId")) {
				topid = dr["TopId"] == DBNull.Value ? -1 : (int)dr["TopId"];
				if (!branchs.ContainsKey(topid))
					branchs.Add(topid, new List<DataRow>());
				branchs[topid].Add(dr);
			}
			foreach (DataRow dr in branchs[-1])
				FillRow(0, dr, null);

		}
		
		private void FillRow(int level, DataRow row, DataRow parrow)
		{
			DataTable dtb = row.Table;

			string col = "D" + level.ToString();
			if (!dtb.Columns.Contains(col)) {
				dtb.Columns.Add(col, typeof(int));
				this.AddDownLevel(level, col);
			}
			string parentname = "", fname;
			if (parrow != null)
				parentname = parrow["FullName"].ToString() + " \\ ";
			fname = parentname + row["Name"].ToString();
			row["FullName"] = fname;
			row[col] = row["DownID"];
			for (int n = 0; n < level; n++) {
				string colD = "D" + n.ToString();
				row[colD] = (int)parrow[colD];
			}

			int downid = (int)row["DownId"];
			if (branchs.ContainsKey(downid))
				foreach (DataRow dr in branchs[downid])
					FillRow(level + 1, dr, row);
		}

		public Dictionary<string, float> GetFilteredKPI(string filter, DataTable dtb)
		{
			if (IsEmpty() || dtb == null)
				return null;

#if (DEBUG)
			//if (L == 1 && dtb.TableName == "year")
			//   throw new Exception("Test exception from 'GetFilteredKPI' to examine the program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");
#endif
			if (dtb.Select(filter).Length == 0)
				return null; //// new Dictionary<string, float>();

			DataRow dr = TotalFilt.NewRow();

			foreach (string key in Kpi.Gaps.Keys)
				if (IsLine || Kpi.Measure[key] != 1) //|| (dtb.TableName == "year" && IsCoupledLine)
					dr[key] = C.GetFloat(dtb.Compute(Kpi.Gaps[key], filter));
				else
				   dr[key] = C.GetFloat(dtb.Compute(string.Format(Kpi.GapsReduced[key], 2), filter));


			if (dtb.TableName == "year") {
				dr["ID"] = ID;
				dr["Line"] = filter;
				// если такая строка уже есть значит это обновление годового кеша и нужно очистить предыдущие значения
				DataRow delrow = C.GetRow(TotalFilt, string.Format("ID={0} AND Line='{1}'", ID, filter));
				if (delrow != null)
					TotalFilt.Rows.Remove(delrow);
			}
			TotalFilt.Rows.Add(dr);

			Dictionary<string, float> dict = new Dictionary<string, float>();
			foreach (DataColumn dc in TotalFilt.Columns)
				if (dc.DataType == typeof(float) && dr[dc.ColumnName] != DBNull.Value)
					dict.Add(dc.ColumnName, (float)dr[dc.ColumnName]);

			return dict;
		}

		private DataRow[] GetSimpleStops()
		{
			List<DataRow> rows = new List<DataRow>();

			int brand, shift, cnt = 0;
			bool issplitted;
			foreach (DataRow dr in DataSet.Tables["Stops"].Select("", "", DataViewRowState.Added)) {
				DataRow[] DR = dr.GetChildRows("StopGap");
				if (DR.Length > 0) {
					brand = (int)DR[0]["Brand"];
					shift = (int)DR[0]["Shift"];
					issplitted = false;
					foreach (DataRow drg in DR) {
						if (brand != (int)drg["Brand"] || shift != (int)drg["Shift"]) {
							issplitted = true;
							break;
						}
					}
					if (!issplitted)
						rows.Add(dr);
				}
				cnt++;
			}
			return rows.ToArray();
		}

		private void ChangeNameColumn()
		{
			string expr = UseDefaultLanguage ? "EName" : "IIF(Value is NULL, EName, Value)";
			foreach (DataTable dtb in DataSet.Tables)
				if (dtb.Columns.Contains("NameID") && dtb.Columns.Contains("Value") && dtb.Columns.Contains("Name"))
					dtb.Columns["Name"].Expression = expr;
		}

		private void FillFullNameColumnOnly(int level, string parentname, DataRow[] DR)
		{
			string fname;
			DataRow[] dr_childs;
			DataTable dtb = DataSet.Tables["StopList"];
			string col = "D" + level;
			foreach (DataRow dr in DR) {
				fname = parentname != "" && col != "D1" ? string.Format("{0} \\ {1}", parentname, dr["Name"]) : dr["Name"].ToString();
				dr["FullName"] = fname;
				dr_childs = dtb.Select("TopID=" + dr["DownID"]);
				if (dr_childs.Length > 0)
					FillFullNameColumnOnly(level + 1, fname, dr_childs);
			}
		}

		private void YearsFixTime()
		{
			_yearValidDate = DateTime.Now;
		}

		public void BuildDataFromChilds()
		{
			if (!IsCoupledLine)
				return;

			try {
				// Просто присваиваем цели первого чайлда
				TgtsYears = Childs[0].TgtsYears;

				#region | Строим основной датасет |

				if (DataSet == null) {
					DataSet = Childs[0].DataCash.Copy();
					NormalizeGaps(DataSet.Tables["Gaps"]);

					DataSet ds2 = Childs[1].DataCash.Copy();
					NormalizeGaps(ds2.Tables["Gaps"]);
					foreach (DataRow dr in ds2.Tables["Gaps"].Rows) {
						if (dr["Stop"] != DBNull.Value) dr["Stop"] = -(int)dr["Stop"];
						if (dr["Brands"] != DBNull.Value) dr["Brands"] = -(int)dr["Brands"];
						if (dr["Shifts"] != DBNull.Value) dr["Shifts"] = -(int)dr["Shifts"];
					}
					ds2.Tables["Stops"].Columns["ID"].ReadOnly = false;
					ds2.Tables["Brands"].Columns["ID"].ReadOnly = false;
					ds2.Tables["Shifts"].Columns["ID"].ReadOnly = false;

					foreach (DataRow dr in ds2.Tables["Stops"].Rows)
						if (dr["ID"] != DBNull.Value) dr["ID"] = -(int)dr["ID"];

					foreach (DataRow dr in ds2.Tables["Brands"].Rows)
						if (dr["ID"] != DBNull.Value) dr["ID"] = -(int)dr["ID"];

					foreach (DataRow dr in ds2.Tables["Shifts"].Rows)
						if (dr["ID"] != DBNull.Value) dr["ID"] = -(int)dr["ID"];

					ds2.AcceptChanges();
					DataSet.AcceptChanges();

					DataSet.Tables["Gaps"].Merge(ds2.Tables["Gaps"]);
					DataSet.Tables["Stops"].Merge(ds2.Tables["Stops"]);
					DataSet.Tables["Brands"].Merge(ds2.Tables["Brands"]);
					DataSet.Tables["Shifts"].Merge(ds2.Tables["Shifts"]);

					DataSet.AcceptChanges();

					DataSet.Merge(ListsCash[Childs[0].ID], true, MissingSchemaAction.AddWithKey);
					BindDataset();
				}

				#endregion


				if (YearsDS == null) {
					YearsDS = Childs[0].YearsDsCash;
					YearsDS.Tables["year"].Merge(Childs[1].YearsDsCash.Tables["year"].Copy());
					BindYearDataset();

					DateTime tm1 = DateTime.Now.Date.AddDays(-DateTime.Now.Date.DayOfYear + 1).AddYears(-10);
					for (int n = 0; n < 11; n++)
						GetFilteredKPI("Yr=" + tm1.AddYears(n).Year, YearsDS.Tables["year"]);
				}
				IsBinded = true;
			} catch {
                if (DataSet != null)
				    DataSet.Clear();
				IsBinded = false;
			}
		}

		#endregion | private functions |


		private void BuildFromToMatrixFromChilds(DataRow dr1, DataTable dtb2)
		{

			DataTable dtb1 = dr1.Table;

			DateTime dt1 = (DateTime)dr1["dt1"];
			DateTime dt2 = (DateTime)dr1["dt2"];

			int bid1 = (int)dr1["bid1"];
			int bid2 = (int)dr1["bid2"];
			int fid1 = (int)dr1["fid1"];
			int fid2 = (int)dr1["fid2"];

			DataRow[] DR = dtb2.Select(string.Format(
				@"bid1={0} AND bid2={1} AND fid1={2} AND fid2={3} 
				AND dt1>=#{4:M/d/yy H:m}# AND dt2<#{5:M/d/yy H:m}#", 
				new object[] { bid1, bid2, fid1, fid2, dt1.AddHours(-1), dt2.AddHours(2) }));

			// Переносим совпадающие строки из второй таблицы в основную
			string name;
			if (DR.Length > 0) {
				foreach (DataColumn col in dtb1.Columns )
					if (col.ExtendedProperties.Contains("Visible")) {
						name = col.ColumnName;
						dr1[name] = ((int)dr1[name] + (int)DR[0][name]);
					}
				dtb2.Rows.Remove(DR[0]);
			}

			// Уменьшаем время вдвое
			foreach (DataColumn col in dtb1.Columns) 
				if (col.ExtendedProperties.Contains("Visible")) {
					name = col.ColumnName;
					dr1[name] = (int)dr1[name] / 2;
				}
		}

		public class GapRecord
		{
			private DataRow row, newrow;

			public GapRecord() { }

			public GapRecord(DataRow dr, DateTime dt)
			{
				row = dr;
				date1 = (DateTime)dr["dt1"];
				date2 = (DateTime)dr["dt2"];
				St = dr["Stop"] == DBNull.Value ? 0 : (int)dr["Stop"];
				Br = (int)dr["Brands"];
				Sh = (int)dr["Shifts"];
				TF = (int)dr["TF"];
				GP = (int)dr["GP"] / (double)TF;
				Seconds = (float)dr["Seconds"] / (double)TF;

				int newTF = (int)dt.Subtract(date1).TotalMinutes;
				dr["dt2"] = dt.AddMinutes(-1);
				dr["TF"] = newTF;
				dr["GP"] = (int)Math.Round((GP * newTF));
				dr["Seconds"] = (float)Seconds * newTF;
				date1 = dt;
				TF = TF - newTF;
			}

			public DataRow GetRow
			{
				get
				{
					if (newrow == null)
						newrow = row.Table.NewRow();
					newrow["dt1"] = date2.AddMinutes(-TF + 1);
					newrow["dt2"] = date2;
					newrow["TF"] = TF;

					newrow["GP"] = (int)(GP * TF);
					newrow["Seconds"] = (int)(Seconds * TF);
					newrow["Stop"] = St;
					newrow["Brands"] = Br;
					newrow["Shifts"] = Sh;
					return newrow;
				}
			}

			public DataRow SplitRow(DateTime dt1n, DateTime dt2n)
			{
				if (TF <= 0)
					return null;
				DataRow newrow = row.Table.NewRow();
				int newTF = (int)dt2n.Subtract(dt1n).TotalMinutes;

				if (newTF > TF)
					newTF = TF;
				else if (newTF < 1)
					return null;
				TF = TF - newTF;

				newrow["dt1"] = dt1n;
				newrow["dt2"] = dt1n.AddMinutes(newTF - 1);
				newrow["TF"] = newTF;

				newrow["GP"] = (int)Math.Round((GP * newTF));
				newrow["Seconds"] = (float)(Seconds * newTF);
				if (St != 0) newrow["Stop"] = St;
				newrow["Brands"] = Br;
				newrow["Shifts"] = Sh;

				//date1 = date2.AddMinutes(-TF + 1);
				return newrow;
			}

			public static void CheckDt2(DataRow dr)
			{
				DateTime dt1 = (DateTime)dr["dt1"];
				DateTime dt2 = (DateTime)dr["dt2"];
				DateTime newdt2 = dt1.AddMinutes((int)dr["TF"] - 1);
				if (dt2 != newdt2)
					dr["dt2"] = newdt2;
			}

			public static DataRow DateSplitRow(DataRow dr, int timecorrection)
			{
				DateTime date1 = (DateTime)dr["dt1"];
				DateTime date2 = (DateTime)dr["dt2"];

				DateTime dt = date1.Date.AddMinutes(timecorrection + 1440);
				if (dt <= date1)
					dt = dt.AddDays(1);
				else if (dt > date2)
					dt = dt.AddDays(-1);
				if (dt <= date1 || dt > date2)
					return null;

				int TF = (int)dr["TF"];
				int TFold = (int)dt.Subtract(date1).TotalMinutes;
				int TFnew = TF > TFold ? TF - TFold : 0;
				double GP = (int)dr["GP"] / (double)TF;
				double Seconds = (float)dr["Seconds"] / (double)TF;

				dr["dt2"] = date1.AddMinutes(TFold - 1);
				dr["TF"] = TFold;

				dr["GP"] = (int)(GP * TFold);
				dr["Seconds"] = (int)(Seconds * TFold);

				if (TFnew > 0) {
					DataRow newrow = dr.Table.NewRow();
					newrow["dt1"] = dt;
					newrow["dt2"] = date2;
					newrow["TF"] = TFnew;

					newrow["GP"] = (int)(GP * TFnew);
					newrow["Seconds"] = (int)(Seconds * TFnew);
					newrow["Stop"] = dr["Stop"];
					newrow["Brands"] = dr["Brands"];
					newrow["Shifts"] = dr["Shifts"];
					return newrow;
				} else
					return null;
			}

			public double dt1;
			public double dt2;
			public DateTime date1;
			public DateTime date2;
			public double speed;
			public string RD;

			public int TF;
			public double GP;
			public double Seconds;
			public int St;
			public int Br;
			public int Sh;
		}

		public class StartComparer : IComparer<GapRecord>
		{
			public int Compare(GapRecord a, GapRecord b)
			{
				if (a.dt1 == b.dt1)
					return a.dt2 < b.dt2 ? -1 : 1;
				else
					return a.dt1 < b.dt1 ? -1 : 1;
			}
		}

		public class LevelComparer : IComparer<MU>
		{
			public int Compare(MU a, MU b)
			{
				if (a.Level == b.Level)
					return 0;
				else
					return a.Level > b.Level ? -1 : 1;
			}
		}
	
	}
}











