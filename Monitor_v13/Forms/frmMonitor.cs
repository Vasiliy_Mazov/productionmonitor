using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Resources;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using C1.C1Excel;
using C1.Win.C1Chart;
using C1.Win.C1FlexGrid;
using Res = ProductionMonitor.Properties.Resources;


namespace ProductionMonitor
{
	public sealed partial class frmMonitor : Form
	{
		[STAThread]
		public static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			#region | exception |

			// Create new instance of UnhandledExceptionDlg:
			UnhandledExceptionDlg exDlg = new UnhandledExceptionDlg();

			// Uncheck "Restart App" check box by default:
			exDlg.SendReport = false;

			// Add handling of OnShowErrorReport.
			// If you skip this then link to report details won't be showing.
			exDlg.OnShowErrorReport += delegate(object sender, SendExceptionClickEventArgs ar)
			{
				MessageBox.Show(
					String.Format(@"
OS Version: {0}
\nFramework: {1}
\nProgram Version: {2}
\nUserDomainName: {3}
\nMachineName: {4}
\nSystem started: {5:0}
\nCurrentDirectory: {6}
\nUnhandledException.Message:\n {7}
\nUnhandledException.StackTrace:\n {8}\n",
	Environment.OSVersion,
	Environment.Version,
	Application.ProductVersion,
	Environment.UserDomainName,
	Environment.MachineName,
	Environment.TickCount / 60000,
	Environment.CurrentDirectory,
	ar.UnhandledException.Message,
	ar.UnhandledException.StackTrace));
			};

			// Implement your sending protocol here. You can use any information from System.Exception
			exDlg.OnSendExceptionClick += delegate(object sender, SendExceptionClickEventArgs ar)
			{
				// User clicked on "Send Error Report" button:
				if (ar.SendReport) {
					try {
						string msg = "&body=";
						msg +=
							String.Format(@"
								OS Version: {0}
								\nFramework: {1}
								\nProgram Version: {2}
								\nUserDomainName: {3}
								\nMachineName: {4}
								\nSystem started: {5:0}
								\nCurrentDirectory: {6}
								\nUnhandledException.Message:\n {7}
								\nUnhandledException.StackTrace:\n {8}\n",
									Environment.OSVersion,
									Environment.Version,
									Application.ProductVersion,
									Environment.UserDomainName,
									Environment.MachineName,
									Environment.TickCount / 60000,
									Environment.CurrentDirectory,
									ar.UnhandledException.Message,
									ar.UnhandledException.StackTrace);

						System.Diagnostics.Process.Start("mailto:wlad7777@mail.ru?subject=ProductionMonitor Exception" + msg);
						//MessageBox.Show("mail Send");
					} catch (Exception ex) {
						Debug.WriteLine(ex.ToString());
					}
				}
				// User wants to restart the App:
				if (ar.DialogResult == DialogResult.Retry) {
					//Console.WriteLine("The App will be restarted...");
					System.Diagnostics.Process.Start(System.Windows.Forms.Application.ExecutablePath);
					Application.Exit();
				} else if (ar.DialogResult == DialogResult.Abort)
					Application.Exit();
			};
			#endregion | exception |

			// Check run state
			if (IsAlreadyRunning()) return;

			// Splash screen
			string splashcurrentinfo = "";
			string splashcaption = C.GetAssemblyShortInfo(Assembly.GetExecutingAssembly(), ref splashcurrentinfo);
			Splash.ShowSplash(100, splashcaption, splashcurrentinfo);

			// Language settings
			Thread.CurrentThread.CurrentCulture = new CultureInfo(ProductionMonitor.Properties.Settings.Default.Culture);
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			Res.Culture = Thread.CurrentThread.CurrentCulture;

			////Splash.CurrentState(Environment.NewLine + "Run the main form");
			Application.Run(new frmMonitor());
		}

		public frmMonitor()
		{
			_monitorSettings = ProductionMonitor.Properties.Settings.Default;
			//Application.DoEvents();

			////Splash.CurrentState(Environment.NewLine + "Init components");
			InitializeComponent();
			////Splash.CurrentState(Environment.NewLine + "Init controls");
			InitControls();
			////Splash.CurrentState(Environment.NewLine + "Init edit grids");
			InitEditGrids();
			////Splash.CurrentState(Environment.NewLine + "Init grids");
			InitGrids();
			////Splash.CurrentState(Environment.NewLine + "Init charts");
			InitCharts();
           // Splash.CurrentState(Environment.NewLine + "Init InitChartsCounters");
            InitChartsCounters();
		}
        private void InitChartsCounters()
        {
            #region | Timeline_counters   |
            // throw new NotImplementedException();
            _ch_ = new TimeLineChart[MaxLines];
            for (int n = 0; n < _ch_.Length; n++)
                _ch_[n] = new TimeLineChart();
            foreach (TimeLineChart c in _ch_)
            {
                ((System.ComponentModel.ISupportInitialize)c).BeginInit();
             //   c.Transform      += new C1.Win.C1Chart.TransformEventHandler(ch0_Transform);
                c.MouseMove      += new System.Windows.Forms.MouseEventHandler(ch0_MouseMove_Counter);
              //  c.MouseDown      += new MouseEventHandler(ch0_MouseDown_Counter);
             //   c.MouseUp        += new System.Windows.Forms.MouseEventHandler(ch0_MouseUp_Counter);
                c.Click          += new System.EventHandler(ch0_Click_Counter);
               // c.DoubleClick    += new System.EventHandler(ch0_DoubleClick);
                c.MouseLeave     += new System.EventHandler(ch0_MouseLeave);
                c.Enter          += new EventHandler(ch0_Enter_counters);
                c.VisibleChanged += new EventHandler(chartmap_VisibleChangedCounter);

                c.Visible = false;
                c.Dock = System.Windows.Forms.DockStyle.Top;
                ////	c.Size = new Size(100, ChMapHei);

                // Rotate = 0
                // Scale = 1
                // Translate = 2
                // Zoom = 3

                c.Interaction.Enabled = true;
                c.Interaction.Appearance = InteractionAppearance.FillSelectionArea; // тонирование зоны масштабирования
               // c.Interaction.Actions[0].Modifier = Keys.I;
                //c.Interaction.Actions[0].Axis = AxisFlagEnum.AxisX;

              //  c.Interaction.Actions[2].Modifier = Keys.S;
             //   c.Interaction.Actions[2].Axis = AxisFlagEnum.AxesAll;

              //  c.Interaction.Actions[3].Modifier = Keys.None;
             //   c.Interaction.Actions[3].Axis = AxisFlagEnum.AxisX;

               // c.Interaction.Actions["Scale"].Axis = AxisFlagEnum.AxesYY2;
               // c.Interaction.Actions["Translate"].Axis = AxisFlagEnum.AxesYY2;
               // c.Interaction.Actions["Zoom"].Axis = AxisFlagEnum.AxesYY2;

                c.Header.LocationDefault = new Point(13, 1);
                c.Header.Style.Border.Rounding.LeftTop  = 3;
                c.Header.Style.Border.Rounding.RightTop = 3;
                c.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
                c.Header.Style.Border.Color = SystemColors.ControlDark;
                c.Header.Style.BackColor = Color.White;

                c.ChartArea.Inverted           = false;
                c.ChartArea.PlotArea.BackColor = Color.White;
                c.ChartArea.Margins.SetMargins(7, 0, 17, 0);
                c.ChartArea.LocationDefault    = new Point(7, 17);
                c.ChartArea.SizeDefault        = new Size(-1, _chMapAreaHei);

                c.ChartArea.AxisX.Visible = true;
                c.ChartArea.AxisX.TickMinor = TickMarksEnum.None;
                c.ChartArea.AxisX.TickMajor = TickMarksEnum.Inside;
                c.ChartArea.AxisX.ForeColor = Color.Black;
                c.ChartArea.AxisX.AnnoFormat = FormatEnum.DateManual;
                c.ChartArea.AxisX.AnnoFormatString = " H:mm";  //" H:mm:ss";
                c.ChartArea.AxisX.ScrollBar.Appearance = ScrollBarAppearanceEnum.Flat;
                c.ChartArea.AxisX.ScrollBar.Size = 9;
                c.ChartArea.AxisX.ScrollBar.Visible = true;
              //  c.ChartArea.AxisX.ScrollBar.Buttons = AxisScrollBarButtonFlags.NoButtons;
                c.ChartArea.AxisX.ScrollBar.ScrollKeys = ScrollKeyFlags.Cursor;

               // c.ChartArea.AxisY.Visible = false;
                c.ChartArea.AxisY.GridMajor.Visible = true;
                c.ChartArea.AxisY.GridMajor.Color = Color.Gray;
                c.ChartArea.AxisY.ForeColor = Color.Black;
                c.ChartArea.AxisY.TickMinor = TickMarksEnum.None;
                c.ChartArea.AxisY.TickMajor = TickMarksEnum.Inside;
                //c.ChartArea.AxisY.TickLabels = TickLabelsEnum.None;
                c.ChartArea.AxisY.OnTop = false;
                c.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericManual;

                c.ChartArea.AxisY2.Visible = false;

                c.ChartGroups[0].ShowOutline = false;
                c.ChartGroups[1].ShowOutline = false;
                c.ChartGroups[0].ChartType = Chart2DTypeEnum.XYPlot;
                //c.ChartGroups[0].se
                //c.ChartGroups[1].ChartType = Chart2DTypeEnum.XYPlot;
                c.ChartGroups[1].ChartType = Chart2DTypeEnum.Area;

                //c.ChartGroups[2].ShowOutline = false;
               // c.ChartGroups[2].ChartType = Chart2DTypeEnum.XYPlot;

                // c.Style.BackColor = Color.White;
                // c.BackColor = Color.WhiteSmoke;
                // c.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Raised;
                ((ISupportInitialize)c).EndInit();
                pnMaps_counts.Controls.Add(c);
                //pnMaps.Controls.Add(c);

                c.TabStop = true;
                c.ContextMenuStrip = cmMapCounter;
            }
            SetChartMapHeight_counter(true);  //  _monitorSettings.BigMap
            //pnMaps.Controls.SetChildIndex(pnEdit, 0);
            //pnMaps_counts.Controls.SetChildIndex(pnEdit, 0);


            #endregion | Timeline_counters   |


        }

//******************
        private void SetChartMapHeight_counter(bool IsHight)
        {
            pnMaps_counts.SuspendLayout();
            if (IsHight)
            {
                foreach (C1Chart chart in _ch_)
                {
                    if (Visible)
                    {
                        chart.Height = _chMapHeiBig;
                        chart.ChartArea.AxisY.Visible = true;
                        chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
                    }
                }
            }
            else
            {
                foreach (C1Chart chart in _ch_)
                {
                    if (Visible)
                    {
                        chart.ChartArea.AxisY.Visible = false;
                        chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei); //new Size(-1, chMapAreaHei);
                        chart.Height = _chMapHei;
                    }
                }
            }
            Debug.WriteLine("SetChartMapHeight");
            pnMaps_counts.ResumeLayout();
        }

		#region | Code settings           |


        const int _gv_style_pr = 1;   //  Счетчики = 1 или простои
       internal const bool use_easy_counter = true;


        const bool use_threading    = true;	//	false;//false;//				// sometimes need to set to false for debug mode

        
        const bool show_trends_chart_with_parts = true;
		const bool show_program_logo = false;						// form background
		const bool use_steps_chart_in_grid_cell = true;			// for grLines & grTrends
		const bool use_signs_instead_names = false;				// for debug or test mode only
		const bool use_hatchbrushes_for_trends_chart = true;
		const bool allow_edit_several_units = true;				// feature for edtit mode
		const bool allow_empty_comments = true;
		const bool allow_empty_red_comments = false;
		const bool use_pdf = true;
		const bool use_map_click_to_select_line        = false;
		const bool use_smart_autofill                  = true;
		const bool show_dropdown_for_new_prod_or_shift = false;
		internal const bool use_one_column_for_excel   = false;

		private bool show_analysis_chart_with_steps = false;		//
		public enum TimeStyle
		{
			MMM = 0,
			HHxMM = 1,
			HHxHH = 2
		}
		private static TimeStyle _tmStyle;

		internal const int MaxLines = 50;				// max number of units
		internal const int MaxTimes = 50;				// max number of days or weeks or months
		internal const int MaxMapModeMins = 100800;	    //44640;

		#endregion | Code settings           |

		#region | Monitor declare         |

		private StripSelect _mnuSelect;
        private StripSelect _mnuSelect_counters;
		private PictureBox _pbError;

		private Button _splAnalyse;
		private Panel _pnAnalyse, _pnAnalyseChart;
		private CheckBox _chbShowTips, _chbShowBigMap;
		private ToolStripMenuItem _mnuFile, _mnuSett, _mnuHelp;
		private ToolStripMenuItem _mnuExit, _mnuExcel, _mnuCalculate; //, mnuCashRefr;
		private ToolStripMenuItem _mnuLang, _mnuShowLegend, _mnuAutoUpdate, _mnuAtStartup, _mnuUseTimeCorrection;
		private ToolStripMenuItem _mnuMinHour, _mnuSetMMM, _mnuSetHHxMM, _mnuSetHHxHH;
		private ToolStripMenuItem _mnuContent, _mnuAbout;
		private ToolStripMenuItem _mnuTrendMins, _mnuTrendPerc, _mnuTrendReport;
		private ToolStripMenuItem _mnuCopy;
		private ToolStripMenuItem _mnuMasterData;

		private TextBox _txtError;
		private GroupBox _gbError;

		static internal DataTable _tbStopList;
		private DataView _dvG, _dvS, _dvT;
		private DataSet _dsCols, _dsColsCash;

		private TimeLineChart[] _ch;
        private TimeLineChart[] _ch_;
		private C1Chart _chAnalyse, _chTrends, _chMarks, _chPrint;
		private LossesChart _chLosses;
		private C1FlexGrid _grDet, _grTrends, _grLosses, _grAnalyse, _grLines;
		private TimeLineChart _chs;							// selected 'Timeline' chart

		const int _chMapHei = 90;									// 76		// 'Timeline' chart control height
		const int _chMapAreaHei = _chMapHei - 20;				// 56		// 'Timeline' ChartArea height
		const int _chMapHeiBig = _chMapHei * 2;				// 150	// 'Timeline' chart control height with bottles per minute data
		const int _chMapAreaHeiBig = _chMapHei * 2 - 26;	// 126	// 'Timeline' ChartArea height with bottles per minute data

		//static internal int mins;							// amount of minutes for chart
		//static internal DateTime MonitorUnit.dtStart;				// Start date of selected time interval
		//static internal DateTime MonitorUnit.dtEnd;					// End date of selected time interval
		private List<MU> _lu;							// Selected units
        private List<MU> _lu_;							// Selected units couners  avg  03.07.2014
		private List<MU> _nodes = new List<MU>(); // Нет своих данных, есть только дети
        private List<MU> _nodes_ = new List<MU>(); // Нет своих данных, есть только дети
		private Queue<MU> _qu = new Queue<MU>();	// очередь, используется только в процессе загрузки данных
        private Queue<MU> _qu_ = new Queue<MU>();	// очередь, используется только в процессе загрузки данных
		private int L = -1;								// Current line in list of selected units

		private string _filter;							// String with filter for comboboxes (brands, formats, shifts)
		private C.State _state;							// Surrent state (operation) of application
		//static internal bool defaultlanguage = true;			// Flag for english interface
		private int _timeSleep = 1;					// Time constant for checkout
		private Stopwatch _swDb;
		private Stopwatch _swTotal;
		private int _idleCounter;						// Tme counter for user idle
		private string _loadingText;					// String for label at bottom of the form
		private bool _setLineSelectedInUse;


		//static internal bool bday = false, bweeks = false, bmonth = true, bquarter = false, byear = false;
		////private double sql_time_difference;
		private float _dpix;

		private bool _mapMode;							// mode of data presentation (days...months or Multiline)
		private MonitorUser _user;
		private Boolean _isEdit = false;

		static internal Color _colControl = SystemColors.Control;
		static internal Color _colHalfControl = Color.FromArgb(100, SystemColors.Control);
		static internal Color _colForeBold = Color.FromArgb(64, 64, 64);
		private Color _colWhite = Color.White;
		private Color _colRunLight = Color.FromArgb(120, 180, 120);
		private Color _colRunLightLight = Color.FromArgb(210, 220, 200);//(206, 220, 206);//(120, 180, 120);

		const float _oneMin = 1F / 1440;
		const float _oneMinS = 1F / 1450;
		private Dictionary<string, DataSet> _gCash = new Dictionary<string, DataSet>(); // пример ключа: 8_gkpi, где 8 - номер юнита (по сути бд), а gkpi - название sp из базы данных
		private Stack<string> _stackTrends = new Stack<string>();

		private Random rnd = new Random(DateTime.Now.Second * DateTime.Now.DayOfYear);
		//private CultureInfo ci = new CultureInfo("en-GB");

		private DataGridViewCellStyle _newRowStyle = new DataGridViewCellStyle();
		private DataGridViewCellStyle _editRowStyle = new DataGridViewCellStyle();
		private DataGridViewCellStyle _outdatedRowStyle = new DataGridViewCellStyle();

		private BindingSource _bsDnTms, _bsProds, _bsShifts;
		////DataViewManager dvm;
		////BindingSource bsBrandList, bsBrandsGaps, bsBrandListBrands;

		static internal Font _font;
		static internal Font _fontBold;
		//static internal Font fontlucida = new Font("Lucida Sans Unicode", 10F, FontStyle.Regular);
		static internal Font _fontBoldArial = new Font("Arial", 9F, FontStyle.Bold);

		private Dictionary<string, Dictionary<string, GridColumn>> _gridColumns = new Dictionary<string, Dictionary<string, GridColumn>>();
		internal ProductionMonitor.Properties.Settings _monitorSettings;
		private string _trendCol = "";			// selected column (=MonitorUnit.kpi) in trends grid
		Dictionary<int, string> _downs = new Dictionary<int, string>();
		////private TreeView tv;

		private BackgroundWorker _dbWorker;
        private BackgroundWorker _dbWorker_Counter;

		private BackgroundWorker _bindWorker;
		private BackgroundWorker _yearWorker;
		private MU _currUnit;
        private MU _currUnit_;
		private C1.C1Pdf.C1PdfDocument _pdf;
		private DateTime _dtEmpty = new DateTime(1, 1, 1);

		Stopwatch _sw1 = new Stopwatch();
		public static System.Threading.Mutex Mutex;
		private string _baseKpi = "K0";

		#endregion | Monitor declare         |

		#region | At start/stop events    |

		private void frmMonitor_Load(object sender, System.EventArgs e)
		{
			////Splash.CurrentState(Environment.NewLine + "Load the main form");

			Splash.Fadeout();
			//Application.DoEvents();
			Text = Application.ProductName; // Application.CompanyName + " / " + Application.ProductName;

			ToolStripManager.Renderer = new ToolStripProfessionalRenderer(new CustomProfessionalColors());
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			LoadSettings();

			MU.SetTimePeriod(2, DateTime.Now.Date.AddDays(1));
			string winuser = Environment.UserName.ToLower();

#if DEBUG
			if (string.Compare(winuser, "user", true) == 0)
				MU.SetTimePeriod(2, new DateTime(2013, 11, 01));
			if (Environment.MachineName == "xsbook")
				MU.SetTimePeriod(2, new DateTime(2011, 12, 19));
#endif

			_monitorSettings.Lines.Add(-1);

			// create dropdown control instance for date/line/mode selection
            if (!use_easy_counter)
			_mnuSelect = new StripSelect(_monitorSettings.Lines);
            _mnuSelect = new StripSelect(_monitorSettings.Lines_count);
           // _mnuSelect_counters = new StripSelect(_monitorSettings.Lines_count);

			_mnuSelect.DataChanged += new EventHandler(mnuSelect_DataChanged);
           // _mnuSelect_counters.DataChanged += new EventHandler(mnuSelect_DataChanged);
			_mapMode = true;
			MU.Config = _mnuSelect.Config;
         //   MU.Config_counter = _mnuSelect.Config_Counters;
          //  MU.Config_counter = _mnuSelect. ;
          //  MU.Config = _mnuSelect.Config;
			tssbMainDateLineSelector.DropDownItems.Add(_mnuSelect);
           // tssbMainDateLineSelector.DropDownItems.Add(_mnuSelect_counters);
			//Splash.Fadeout();

			// customize the user interface language
			ChangeLang(Thread.CurrentThread.CurrentUICulture.Name);

			if (_mnuSelect.SelectedUnits != null) _lu = _mnuSelect.SelectedUnits as List<MU>;
            if (_mnuSelect.SelectedUnits_Counters != null) _lu_ = _mnuSelect.SelectedUnits_Counters as List<MU>;
        //    if (_mnuSelect_counters.SelectedUnits_Counters != null) _lu_ = _mnuSelect_counters.SelectedUnits_Counters as List<MU>;
          // if (_mnuSelect_counters.SelectedUnits != null) _lu_ = _mnuSelect_counters.SelectedUnits as List<MU>;
		
            MU.UseTimecorrection = _monitorSettings.TimeCorrection;

			// downloading data from back end
           // if (_gv_style_pr == 1 )
               tabMain.SelectedTab = tpCounters;
          //  else 
            //    tabMain.SelectedTab = tpMap;

			if (!_monitorSettings.EmptyAtStart)
				LoadStart();

			// restoring last window state
			if (_monitorSettings.WindowState != WindowState)
				WindowState = _monitorSettings.WindowState;

			if (_monitorSettings.AutoUpdate) _tmActivity.Start();

#if DEBUG
			Debug.WriteLine("frmMain_Load 2 " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private void frmMonitor_Shown(object sender, EventArgs e)
		{
			_gbError.Size = new Size(Width - 30, 90);
			//this.Width = 1024;
			if (_monitorSettings.EmptyAtStart)
				tssbMainDateLineSelector.ShowDropDown();

			//Splash.Fadeout();
		}

		public static bool IsAlreadyRunning()
		{
			// Name should be unique
			const string UniqueString = "ProductionMonitorV3";

			bool createdNew = false;

			// Is there the mutex with this name?
			Mutex = new Mutex(false, UniqueString, out createdNew);
			return !createdNew;
		}

		private void InitControls()
		{
			_state = C.State.DataLoading;

			_font = Font;
			_fontBold = new Font(_font, FontStyle.Bold);

			_bindWorker = new BackgroundWorker { WorkerReportsProgress = true };
			_bindWorker.DoWork += bindworker_DoWork;

			_dbWorker = new BackgroundWorker { WorkerReportsProgress = true };
			_dbWorker.DoWork += dbworker_DoWork;
			_dbWorker.RunWorkerCompleted += dbworker_RunWorkerCompleted;

            _dbWorker_Counter = new BackgroundWorker { WorkerReportsProgress = true };
            _dbWorker_Counter.DoWork += dbworker_Counter_DoWork;
            _dbWorker_Counter.RunWorkerCompleted += dbworker_Counter_RunWorkerCompleted;

			_yearWorker = new BackgroundWorker { WorkerReportsProgress = true };
			_yearWorker.DoWork += yearworker_DoWork;

			_pnAnalyse      = new Panel();
			_splAnalyse     = new Button();
			_pnAnalyseChart = new Panel();
			_chbShowTips    = new CheckBox();
			_chbShowBigMap  = new System.Windows.Forms.CheckBox();

			tsbMainNext.Click += btNext_Click;
			tsbMainPrev.Click += btPrev_Click;
			tsbMainRefresh.Click += btRefresh_Click;
			tsbMainRefreshFailed.Click += btRefreshFailed_Click;
			btHideLegend.Click += btHideLegend_Click;

			tscbMainFormats.ComboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;
			tscbMainBrands.ComboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;
			tscbMainShifts.ComboBox.SelectedValueChanged += ComboBox_SelectedValueChanged;

			_txtError = new TextBox();
			_gbError = new GroupBox();

			_pnAnalyse.SuspendLayout();
			this.SuspendLayout();

			#region | Error visualisation |

			_pbError = new PictureBox
			{
				Image = SystemIcons.Error.ToBitmap(),
				Location = new Point(30, 30),
				Size = new Size(32, 32)
			};

			_gbError.Name = "pnError";
			_gbError.Location = new Point(10, this.Height - 150);
			_gbError.Size = new Size(this.Width - 40, 90);
			_gbError.Anchor = AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
			_gbError.Visible = false;
			_gbError.Controls.Add(_pbError);
			_gbError.Controls.Add(_txtError);

			_txtError.Name = "txtError";
			_txtError.Size = new Size(_gbError.Width - 105, _gbError.Height - 20);
			_txtError.Location = new Point(90, 13);
			_txtError.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
			_txtError.BorderStyle = BorderStyle.None;
			_txtError.BackColor = SystemColors.Control;
			_txtError.Margin = new Padding(10);
			_txtError.WordWrap = true;
			_txtError.Multiline = true;
			_txtError.TextChanged += new EventHandler(txtError_TextChanged);
			_txtError.DoubleClick += new EventHandler(txtError_DoubleClick);

			#endregion | Error visualisation |

			_chbShowTips.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			_chbShowTips.FlatStyle = FlatStyle.System;
			_chbShowTips.Checked = true;
			_chbShowTips.CheckState = CheckState.Checked;
			_chbShowTips.Dock = DockStyle.None;
			_chbShowTips.Size = new Size(200, 20);
			_chbShowTips.Name = "chShowTips";
			_chbShowTips.TabStop = false;

			_chbShowBigMap.Anchor = (System.Windows.Forms.AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			_chbShowBigMap.FlatStyle = FlatStyle.System;
			_chbShowBigMap.Checked = true;
			_chbShowBigMap.Dock = DockStyle.None;
			_chbShowBigMap.Size = new Size(200, 20);
			_chbShowBigMap.Name = "chbShowBigMap";
			_chbShowBigMap.TabStop = false;
			_chbShowBigMap.CheckStateChanged += new EventHandler(chbShowBigMap_CheckStateChanged);

			pnMaps.Controls.Add(_chbShowTips);
			pnMaps.Controls.Add(_chbShowBigMap);

			_pnAnalyse.BackColor = System.Drawing.SystemColors.ControlDark;
			_pnAnalyse.Dock = DockStyle.Fill;
			_pnAnalyse.Height = 400;
			_pnAnalyse.DockPadding.All = 1;

			_splAnalyse.Dock = DockStyle.Bottom;
			_splAnalyse.Size = new Size(100, 5);
			_splAnalyse.FlatStyle = FlatStyle.Flat;
			_splAnalyse.FlatAppearance.BorderSize = 0;
			_splAnalyse.UseVisualStyleBackColor = true;
			_splAnalyse.TabStop = false;
			_splAnalyse.Click += new EventHandler(splAnalyse_Click);

			_pnAnalyseChart.BackColor = System.Drawing.Color.White;
			_pnAnalyseChart.Dock = DockStyle.Bottom;

			tpAnalyse.Controls.Add(_pnAnalyse);
			tpAnalyse.Controls.Add(_splAnalyse);
			tpAnalyse.Controls.Add(_pnAnalyseChart);

			_pnAnalyse.ResumeLayout(false);

			if (show_program_logo) {
				using (ImageAttributes ia = new ImageAttributes()) {
					ColorMatrix cm = new ColorMatrix();
					cm.Matrix33 = 0.25f;
					ia.SetColorMatrix(cm);
					Image im = ProductionMonitor.C.ApplicationImage.Clone() as Image;
					this.BackgroundImage = im;
					this.BackgroundImageLayout = ImageLayout.Center;
					using (Graphics g = Graphics.FromImage(im)) {
						g.Clear(SystemColors.Control);
						g.DrawImage(ProductionMonitor.C.ApplicationImage, new Rectangle(0, 0, im.Width, im.Height), 0, 0, im.Width, im.Height, GraphicsUnit.Pixel, ia);
					}
				}
			}

			Controls.Add(_gbError);
			ResumeLayout(false);
		}

		private void InitMenu()
		{
			this.SuspendLayout();
			MainMenu.Items.Clear();
			EventHandler eh = new EventHandler(mnuClick);

			//////////////////// Main menu population /////////////////////////////////////
			_mnuFile = new ToolStripMenuItem(Res.mnuFile);
			_mnuSett = new ToolStripMenuItem(Res.mnuSettings);
			_mnuHelp = new ToolStripMenuItem(Res.mnuHelp);
			MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _mnuFile, _mnuSett, _mnuHelp });

			//////////////////// Menu 'File' population ///////////////////////////////////
			//mnuExcel1 = new ToolStripMenuItem(Res.mnuExcel1, null, eh, "mnuExcel1");
			_mnuExcel = new ToolStripMenuItem(Res.mnuExcel2, null, eh, "mnuExcel2");
			_mnuCalculate = new ToolStripMenuItem(Res.mnuCalculate, null, eh, "mnuCalculate");
			_mnuExit = new ToolStripMenuItem(Res.mnuExit, Res.exit, eh, "mnuExit");
			//mnuCashRefr = new ToolStripMenuItem(Res.mnuCash, null, eh, "mnuCashRefr");
			_mnuLang = new ToolStripMenuItem(Res.mnuLang);
			//mnuExcel1.ShortcutKeys = Keys.Alt | Keys.E;
			_mnuExcel.ShortcutKeys = Keys.Alt | Keys.E; //| Keys.A;
			//mnuCashRefr.ShortcutKeys = Keys.Alt | Keys.R;
			_mnuExit.ShortcutKeys = Keys.Alt | Keys.F4;
			_mnuCalculate.Visible = false;
			//mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {mnuExcel1, mnuExcel2, mnuCalculate, new ToolStripSeparator(), mnuExit });
			_mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _mnuExcel, _mnuCalculate, new ToolStripSeparator(), _mnuExit });

			//////////////////// Menu 'Settings' population ///////////////////////////////
			_mnuMasterData = new ToolStripMenuItem(Res.mnuMasterData, null, eh, "mnuMasterData");
			_mnuShowLegend = new ToolStripMenuItem(Res.mnuShowLegend, null, eh, "mnuShowLegend");
			_mnuAutoUpdate = new ToolStripMenuItem(Res.mnuAutoUpdate, null, eh, "mnuAutoUpdate");
			_mnuAtStartup  = new ToolStripMenuItem(Res.mnuAtStartup, null, eh, "mnuAtStartup");
			_mnuMinHour    = new ToolStripMenuItem(Res.mnuMinHour, null, eh, "mnuMinHour");
			_mnuSetMMM     = new ToolStripMenuItem(Res.mnuSetMMM, null, eh, "mnuSetMMM");
			_mnuSetHHxMM   = new ToolStripMenuItem(Res.mnuSetHHxMM, null, eh, "mnuSetHHxMM");
			_mnuSetHHxHH   = new ToolStripMenuItem(Res.mnuSetHHxHH, null, eh, "mnuSetHHxHH");
			_mnuUseTimeCorrection = new ToolStripMenuItem(Res.mnuUseTimeCorrection, null, eh, "mnuUseTimeCorrection");
			_mnuMasterData.Enabled = false;
			_mnuShowLegend.CheckOnClick = true;
			_mnuAutoUpdate.CheckOnClick = true;
			_mnuAtStartup.CheckOnClick = true;
			_mnuSetMMM.CheckOnClick = true;
			_mnuSetHHxMM.CheckOnClick = true;
			_mnuSetHHxHH.CheckOnClick = true;
			_mnuUseTimeCorrection.CheckOnClick = true;

			_mnuAutoUpdate.Checked = _monitorSettings.AutoUpdate;
			_mnuShowLegend.Checked = _monitorSettings.ColorLegend;
			_mnuAtStartup.Checked = _monitorSettings.EmptyAtStart;
			_mnuSetMMM.Checked = TmStyle == TimeStyle.MMM;
			_mnuSetHHxMM.Checked = TmStyle == TimeStyle.HHxMM;
			_mnuSetHHxHH.Checked = TmStyle == TimeStyle.HHxHH;
			_mnuUseTimeCorrection.Checked = _monitorSettings.TimeCorrection;
			_mnuShowLegend.ShortcutKeys = Keys.Control | Keys.L;

            _mnuSett.DropDownItems.AddRange(new ToolStripItem[] { _mnuShowLegend, _mnuAutoUpdate, _mnuAtStartup, _mnuMinHour, _mnuUseTimeCorrection, new ToolStripSeparator(), _mnuMasterData, _mnuLang });
			_mnuMinHour.DropDownItems.AddRange(new ToolStripItem[] { _mnuSetMMM, _mnuSetHHxMM, _mnuSetHHxHH });

			/////////////////// Menu 'Help' population ////////////////////////////////////
			_mnuContent = new ToolStripMenuItem(Res.mnuContent, null, eh, "mnuContent");
			_mnuAbout = new ToolStripMenuItem(Res.mnuAbout, null, eh, "mnuAbout");
			_mnuContent.ShortcutKeys = Keys.F1;
			//mnuAbout.ShortcutKeys = Keys.F2;
            if (!use_easy_counter)
			_mnuHelp.DropDownItems.AddRange(new ToolStripItem[] { _mnuContent, _mnuAbout });
            else
                _mnuHelp.DropDownItems.AddRange(new ToolStripItem[] { _mnuAbout });
			////////////////// Context menu for 'Detailed' grid ///////////////////////////
			_mnuCopy = new ToolStripMenuItem(Res.mnuCopy, null, eh, "mnuCopy");

			cmDetailed.Items.Clear();
			this.cmDetailed.Items.AddRange(new ToolStripItem[] { _mnuCopy });

			/////////////////// Menu for tab 'KPI Overview' population ////////////////////
			_mnuTrendPerc = new ToolStripMenuItem(Res.mnuTrendsPerc, null, eh, "mnuComparePerc");
			_mnuTrendMins = new ToolStripMenuItem(Res.mnuTrendsMins, null, eh, "mnuCompareMins");
			_mnuTrendReport = new ToolStripMenuItem(Res.mnuTrendsReport, null, eh, "mnuCompareReport");
			_mnuTrendPerc.Checked = _monitorSettings.Percents;
			_mnuTrendMins.Checked = !_monitorSettings.Percents;
			cmTrendsChart.Items.Clear();
			cmTrendsChart.Items.AddRange(new ToolStripMenuItem[] { _mnuTrendPerc, _mnuTrendMins, _mnuTrendReport });

			ToolStripMenuItem mnuCopyCompare = new ToolStripMenuItem(Res.mnuCopy, null, eh, "mnuCopyCompare");
			ToolStripMenuItem mnuSetCompareCols = new ToolStripMenuItem(Res.mnuGrSetupColumns, null, eh, "mnuSetCompareCols");
			cmTrendsGrig.Items.Clear();
			cmTrendsGrig.Items.Add(mnuCopyCompare);
			cmTrendsGrig.Items.Add(mnuSetCompareCols);

			ToolStripMenuItem mnuCopyLines = new ToolStripMenuItem(Res.mnuCopy, null, eh, "mnuCopyLines");
			ToolStripMenuItem mnuSetLinesCols = new ToolStripMenuItem(Res.mnuGrSetupColumns, null, eh, "mnuSetLinesCols");
			cmLinesGrid.Items.Clear();
			cmLinesGrid.Items.Add(mnuCopyLines);
			cmLinesGrid.Items.Add(mnuSetLinesCols);

			////////////////// Context menu for 'Timeline'
			ToolStripMenuItem mnuApply     = new ToolStripMenuItem(Res.mnuMapApplyTimeCorr, null, eh, "mnuApply");
			ToolStripMenuItem mnuChange    = new ToolStripMenuItem(Res.mnuMapChangeRunSKU,  null, eh, "mnuChange");
			ToolStripMenuItem mnuReset     = new ToolStripMenuItem(Res.mnuMapReset,         null, eh, "mnuReset");
			ToolStripMenuItem mnuDelProd   = new ToolStripMenuItem(Res.mnuMapDelete,        null, eh, "mnuDelProd");
			ToolStripMenuItem mnuAutoShift = new ToolStripMenuItem(Res.mnuMapAutoShift,     null, eh, "mnuAutoShift");
			ToolStripMenuItem mnuShowMins  = new ToolStripMenuItem(Res.mnuShowMins,         null, eh, "mnuShowMins");
			cmMap.Items.Clear(); 
			cmMap.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { mnuAutoShift, mnuApply, mnuChange, mnuReset, mnuDelProd, mnuShowMins });
            cmMapCounter.Items.Clear();
            //if (!use_easy_counter ) 
            cmMapCounter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { mnuAutoShift, mnuApply, mnuChange, mnuReset, mnuDelProd, mnuShowMins });
          //  else
           //     cmMapCounter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { mnuAutoShift, mnuReset, mnuApply, mnuChange, mnuDelProd, mnuShowMins });
           
           


			ToolStripLabel tsl = new ToolStripLabel("");
			tsl.Name = "testlabel";
			tsl.Alignment = ToolStripItemAlignment.Right;
			MainMenu.Items.Add(tsl);
#if !DEBUG
			tsl.Visible = false;
#endif

			InitLang();
			ResumeLayout(false);

            if (use_easy_counter)
            {
                _mnuMasterData.Visible        = false;
                _mnuShowLegend.Visible        = false;
                //_mnuAutoUpdate.Visible      = false;
                _mnuAtStartup.Visible         = false;
                _mnuMinHour.Visible           = false;
                _mnuSetMMM.Visible            = false;
                _mnuSetHHxMM.Visible          = false;
                _mnuSetHHxHH.Visible          = false;
                _mnuUseTimeCorrection.Visible = false;
                mnuAutoShift.Visible          = false;
            }

		}

		private void InitGrids()
		{
			////Splash.CurrentState(Environment.NewLine + "Init grid create instances");
			_grLines   = new C1FlexGrid();
			_grTrends  = new C1FlexGrid();
			_grLosses  = new C1FlexGrid();
			_grDet     = new C1FlexGrid();
			_grAnalyse = new C1FlexGrid();

			////Splash.CurrentState(Environment.NewLine + "Init grid BeginInit");

			_grLines.BeginInit();
			_grTrends.BeginInit();
			_grLosses.BeginInit();
			_grDet.BeginInit();
			_grAnalyse.BeginInit();

			////Splash.CurrentState(Environment.NewLine + "Init grid events");

			// Grids Events
			_grLines.AfterDragColumn    += new DragRowColEventHandler(gr_AfterDragColumn);
			_grLines.AfterResizeColumn  += new RowColEventHandler(gr_AfterResizeColumn);
			_grLines.OwnerDrawCell      += new C1.Win.C1FlexGrid.OwnerDrawCellEventHandler(gr_OwnerDrawCell);
			_grLines.DoubleClick        += new EventHandler(grLines_DoubleClick);
			_grLines.RowColChange       += new EventHandler(grLines_RowColChange);
			_grLines.BeforeRowColChange += new RangeEventHandler(grLines_BeforeRowColChange);
			_grLines.MouseMove          += new MouseEventHandler(gr_MouseMove);
			_grLines.MouseLeave         += new EventHandler(gr_MouseLeave);
			_grLines.MouseDown          += new MouseEventHandler(grLines_MouseDown);
			_grLines.MouseHover         += new EventHandler(grLines_MouseHover);

			_grTrends.AfterDragColumn   += new DragRowColEventHandler(gr_AfterDragColumn);
			_grTrends.AfterResizeColumn += new RowColEventHandler(gr_AfterResizeColumn);
			_grTrends.OwnerDrawCell     += new C1.Win.C1FlexGrid.OwnerDrawCellEventHandler(gr_OwnerDrawCell);
			_grTrends.AfterDataRefresh  += new System.ComponentModel.ListChangedEventHandler(grTrends_AfterDataRefresh);
			_grTrends.RowColChange      += new EventHandler(grTrends_RowColChange);
			_grTrends.MouseMove         += new MouseEventHandler(gr_MouseMove);
			_grTrends.MouseLeave        += new EventHandler(gr_MouseLeave);
			_grTrends.MouseDown         += new MouseEventHandler(grTrends_MouseDown);

			_grLosses.OwnerDrawCell      += new OwnerDrawCellEventHandler(grLosses_OwnerDrawCell);
			_grLosses.RowColChange       += new EventHandler(grLosses_RowColChange);
			_grLosses.BeforeRowColChange += new RangeEventHandler(grLosses_BeforeRowColChange);

			SuspendLayout();

			////Splash.CurrentState(Environment.NewLine + "Init grid 'lines'");

			#region | grLines    |

			//////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////// grLines /////////////////////////////////////////////////////////////
			_grLines.AccessibleName = "grLines";
			_grLines.TabStop = false;
			_grLines.ContextMenuStrip = cmLinesGrid;
			_grLines.Dock = System.Windows.Forms.DockStyle.Bottom;
			_grLines.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D;
			_grLines.SelectionMode = SelectionModeEnum.Row;
			_grLines.BackColor = _colWhite;
			_grLines.AllowEditing = false;
			_grLines.AllowSorting = AllowSortingEnum.None;
			_grLines.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Custom;
			_grLines.DrawMode = DrawModeEnum.OwnerDraw;
			_grLines.ExtendLastCol = true;

			_grLines.Rows[0].Height = _grLines.Rows.DefaultSize * 2;
			_grLines.Styles.Fixed.WordWrap = true;
			//grLines.Styles.Normal.WordWrap = true;
			_grLines.Cols.DefaultSize = 40;
			_grLines.Cols.MinSize = 40;
			_grLines.Cols.Fixed = 0;
			_grLines.Cols.Frozen = 2;
			_grLines.AutoResize = false;

			//grLines.Styles.Focus.BackColor = Color.White;
			//grLines.Styles.Focus.ForeColor = Color.Black;
			//grLines.Styles.Focus.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;

			_grLines.Styles.Highlight.BackColor = Color.White;//Color.Gainsboro;
			_grLines.Styles.Highlight.ForeColor = Color.Black;
			_grLines.Styles.Highlight.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;

			_grLines.Styles.Frozen.BackColor = _colHalfControl;
			_grLines.Rows.Count = 1;
			//grLines.Cols.Count = 0;

			C1.Win.C1FlexGrid.CellStyle s = _grLines.Styles.Add("Error");
			s.BackColor = _grLines.Styles.Normal.BackColor;
			s.ForeColor = Color.Red;

			s = _grLines.Styles.Add("FatalError");
			s.BackColor = _grLines.Styles.Normal.BackColor;
			s.ForeColor = Color.DarkRed;

			s = _grLines.Styles.Add("RowBoldCaption");
			s.BackColor = _colHalfControl;
			s.Font = _fontBoldArial;
			s.ForeColor = _colForeBold;

			s = _grLines.Styles.Add("RowBoldCaptionError");
			s.BackColor = _colHalfControl;
			s.Font = _fontBoldArial;
			s.ForeColor = Color.Red;

			s = _grLines.Styles.Add("RowCaption");
			s.BackColor = SystemColors.InactiveCaption;
			//s.Font = SystemFonts.CaptionFont;
			s.ForeColor = SystemColors.InactiveCaptionText; // ColForeBold;

			s = _grLines.Styles.Add("Target");
			s.BackColor = _colHalfControl;
			s.Font = _fontBoldArial;
			s.ForeColor = Color.Gray;
			s.TextAlign = TextAlignEnum.RightCenter;

			//MU.tbL = new DataTable("Lines");
			//MU.tbL.Columns.Add("ID", typeof(int));
			//MU.tbL.Columns.Add("Line", typeof(string));

			_grLines.Tree.Column = 0;

			#endregion | grLines    |

			////Splash.CurrentState(Environment.NewLine + "Init grid 'trends'");

			#region | grTrends   |

			//////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////// grCompare ///////////////////////////////////////////////////////////
			_grTrends.AccessibleName = "grCompare";
			_grTrends.ContextMenuStrip = cmTrendsGrig;
			_grTrends.Dock = DockStyle.Top;
			_grTrends.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D;
			_grTrends.SelectionMode = SelectionModeEnum.Cell;
			_grTrends.BackColor = _colWhite;
			_grTrends.AllowEditing = false;
			_grTrends.AllowSorting = AllowSortingEnum.None;
			_grTrends.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Custom;
			_grTrends.DrawMode = DrawModeEnum.OwnerDraw;
			_grTrends.ExtendLastCol = true;

			_grTrends.TabStop = false;
			_grTrends.Rows[0].Height = _grTrends.Rows.DefaultSize * 2;
			_grTrends.Styles.Fixed.WordWrap = true;
			_grTrends.Cols.DefaultSize = 40;
			_grTrends.Cols.MinSize = 40;
			_grTrends.Cols.Fixed = 0;
			_grTrends.Cols.Frozen = 1;
			_grTrends.AutoResize = false;
			_grTrends.Styles.Highlight.BackColor = Color.White;
			_grTrends.Styles.Highlight.ForeColor = Color.Black;
			_grTrends.Styles.Highlight.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;
			//grTrends.Styles.Focus.BackColor = grTrends.Styles.Highlight.BackColor;
			//grTrends.Styles.Focus.ForeColor = grTrends.Styles.Highlight.ForeColor;
			//grTrends.Styles.Focus.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;

			_grTrends.Styles.Frozen.BackColor = _colHalfControl;
			//grTrends.Styles.Frozen.ForeColor = grCompare.Styles.Normal.ForeColor;
			_grTrends.Styles.EmptyArea.BackColor = Color.White;
			_grTrends.BackColor = Color.White;

			#endregion | grTrends   |

			////Splash.CurrentState(Environment.NewLine + "Init grid 'losses'");

			#region | grLosses   |

			_grLosses.AccessibleName = "grLosses";
			//grLosses.ContextMenuStrip = cmCompareGrig;
			_grLosses.Dock = DockStyle.Top;
			_grLosses.Size = new Size(600, 300);
			_grLosses.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D;
			_grLosses.SelectionMode = SelectionModeEnum.Column;
			_grLosses.BackColor = _colWhite;
			_grLosses.AllowEditing = false;
			_grLosses.AllowSorting = AllowSortingEnum.None;
			_grLosses.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Custom;
			_grLosses.DrawMode = DrawModeEnum.OwnerDraw;
			_grLosses.ExtendLastCol = false;

			_grLosses.TabStop = false;
			_grLosses.Rows[0].Height = _grLosses.Rows.DefaultSize * 2;
			_grLosses.Styles.Fixed.WordWrap = true;
			_grLosses.Cols.DefaultSize = 60;
			_grLosses.Cols.MinSize = 5;
			_grLosses.Cols.Fixed = 0;
			_grLosses.AutoResize = false;
			_grLosses.Styles.Frozen.BackColor = _colHalfControl;
			_grLosses.Cols.Frozen = 3;
			//grLosses.Styles.Frozen.ForeColor = grReps.Styles.Normal.ForeColor;
			_grLosses.Styles.EmptyArea.BackColor = Color.White;
			_grLosses.BackColor = Color.White;
			_grLosses.Styles.EmptyArea.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;

			_grLosses.Styles.Highlight.Font = _fontBold;
			_grLosses.Styles.Highlight.ForeColor = _grLosses.Styles.Normal.ForeColor;
			_grLosses.Styles.Highlight.BackColor = _grLosses.Styles.Normal.BackColor;
			_grLosses.Styles.Focus.Font = _fontBold;
			//grLosses.Styles.Focus.ForeColor = grLosses.Styles.Normal.ForeColor;

			CellStyle rstyle;

			rstyle = _grLosses.Styles.Add("right");
			rstyle.TextAlign = TextAlignEnum.RightCenter;

			rstyle = _grLosses.Styles.Add("blanc");
			rstyle.Border.Direction = BorderDirEnum.Vertical;

			#endregion | grLosses   |

			////Splash.CurrentState(Environment.NewLine + "Init grid 'detailes'");

			#region | grDet      |

			/////////////////////////////////////////////////////////////////////
			///////////////// grDet /////////////////////////////////////////////
			_grDet.AccessibleName = "grDet";
			_grDet.Dock = System.Windows.Forms.DockStyle.Fill;
			_grDet.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.Light3D;
			_grDet.BackColor = _colWhite;
			_grDet.ContextMenuStrip = cmDetailed;

			_grDet.AutoResize = false;
			_grDet.DrawMode = C1.Win.C1FlexGrid.DrawModeEnum.OwnerDraw;
			_grDet.OwnerDrawCell += new C1.Win.C1FlexGrid.OwnerDrawCellEventHandler(grDet_OwnerDrawCell);

			_grDet.Cols.Fixed = 0;
			_grDet.Rows.Fixed = 1;
			_grDet.Rows[0].Height = _grDet.Rows.DefaultSize * 2;
			_grDet.Styles.Fixed.WordWrap = true;

			_grDet.Tree.Style = TreeStyleFlags.Complete;
			_grDet.AllowMerging = AllowMergingEnum.Nodes;
			_grDet.AllowEditing = false;
			_grDet.SelectionMode = SelectionModeEnum.Cell;

			s = _grDet.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal0];
			s.BackColor = _colHalfControl;
			s.ForeColor = _grDet.Styles.Normal.ForeColor;

			s = _grDet.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal1];
			s.BackColor = _colHalfControl;
			s.ForeColor = _grDet.Styles.Normal.ForeColor;

			s = _grDet.Styles[C1.Win.C1FlexGrid.CellStyleEnum.Subtotal2];
			s.BackColor = _colHalfControl;
			s.ForeColor = _grDet.Styles.Normal.ForeColor;

			#endregion | grDet      |

			////Splash.CurrentState(Environment.NewLine + "Init grid losses'");

			#region | grAnalyse  |

			_grAnalyse.AccessibleName = "fg2";
			_grAnalyse.Dock = DockStyle.Fill;
			_grAnalyse.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.None;
			_grAnalyse.BackColor = System.Drawing.Color.White;
			_grAnalyse.Size = new System.Drawing.Size(600, 400);
			_grAnalyse.TabStop = false;
			_grAnalyse.Styles.EmptyArea.BackColor = Color.White;
			_grAnalyse.Styles.EmptyArea.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;
			_grAnalyse.Rows[0].Height = _grAnalyse.Rows.DefaultSize * 2;
			_grAnalyse.Styles.Fixed.WordWrap = true;
			_grAnalyse.DrawMode = DrawModeEnum.OwnerDraw;
			_grAnalyse.MouseMove += _grAnalyse_MouseMove;

			#endregion | grAnalyse  |

			_grLines.EndInit();
			_grTrends.EndInit();
			_grLosses.EndInit();
			_grDet.EndInit();
			_grAnalyse.EndInit();

			pnLines.Controls.Add(_grLines);
			tpDetail.Controls.Add(_grDet);
			tpDetail.Controls.SetChildIndex(_grDet, 0);
			_pnAnalyse.Controls.Add(_grAnalyse);

			ResumeLayout(false);
		}



		private void InitGridsColumns()
		{
			_gridColumns.Clear();
			Dictionary<string, GridColumn> columns;
			columns = new Dictionary<string, GridColumn>();
			columns.Add("RD", new GridColumn(" ", "", 52));
			columns.Add("FullName", new GridColumn(Res.strCause, "", 240));
			columns.Add("dt1", new GridColumn(Res.strTime, "g", 95));
			columns.Add("TF", new GridColumn(Res.SelMins + ", " + (TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours), "", 60));
			columns.Add("TD", new GridColumn(Res.RealMins + ", " + (TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours), "0.0", 60));
			foreach (string key in MU.marks.Keys)
				columns.Add(key, new GridColumn(MU.marks_captions[key], "", 90));
			columns.Add("Comment", new GridColumn(Res.strComment, "", 200));
			_gridColumns.Add("Stops", columns);

			columns = new Dictionary<string, GridColumn>();
			columns.Add("Date",   new GridColumn("Date", "", 100));
			columns.Add("dt1",    new GridColumn(Res.strFrom, "t", 60));
			columns.Add("End",    new GridColumn(Res.strTo, "t", 60));
			columns.Add("Vlm",    new GridColumn(Res.strVolume, "", 45));
			columns.Add("TF",     new GridColumn(Res.strTime, "", 60));
			columns.Add("GP",     new GridColumn(Res.strGP, "", 85));
			columns.Add("NP",     new GridColumn(Res.strNP, "0", 85));
			columns.Add("Volume", new GridColumn(Res.strMeasure, "0.#", 60));
			columns.Add("Speed",  new GridColumn(Res.strAverageSpeed, "0.#", 60));
			columns.Add("FormatName", new GridColumn(Res.strFormats, "", 200));
			columns.Add("BrandName", new GridColumn(Res.strBrands, "", 300));
			_gridColumns.Add("ProductionExtended", columns);
			_gridColumns.Add("ProductionShort", columns);
		}

		private void InitCharts()
		{
			#region | Timeline   |

			_ch = new TimeLineChart[MaxLines];
			for (int n = 0; n < _ch.Length; n++)
				_ch[n] = new TimeLineChart();
			foreach (TimeLineChart c in _ch) {
				((System.ComponentModel.ISupportInitialize)c).BeginInit();
				c.Transform   += new C1.Win.C1Chart.TransformEventHandler(ch0_Transform);
				c.MouseMove   += new System.Windows.Forms.MouseEventHandler(ch0_MouseMove);
				c.MouseDown   += new MouseEventHandler(ch0_MouseDown);
				c.MouseUp     += new System.Windows.Forms.MouseEventHandler(ch0_MouseUp);
				c.Click       += new System.EventHandler(ch0_Click);
				c.DoubleClick += new System.EventHandler(ch0_DoubleClick);
				c.MouseLeave  += new System.EventHandler(ch0_MouseLeave);
				c.Enter       += new EventHandler(ch0_Enter);
				c.VisibleChanged += new EventHandler(chartmap_VisibleChanged);

				c.Visible = false;
				c.Dock = System.Windows.Forms.DockStyle.Top;
				////	c.Size = new Size(100, ChMapHei);

				// Rotate = 0
				// Scale = 1
				// Translate = 2
				// Zoom = 3

				c.Interaction.Enabled = true;
				//c.Interaction.Appearance = InteractionAppearance.FillSelectionArea; // тонирование зоны масштабирования
				c.Interaction.Actions[0].Modifier = Keys.I;
				//c.Interaction.Actions[0].Axis = AxisFlagEnum.AxisX;
				//c.Interaction.Actions[2].Modifier = Keys.S;
				//c.Interaction.Actions[2].Axis = AxisFlagEnum.AxesAll;
				c.Interaction.Actions[3].Modifier = Keys.None;
				c.Interaction.Actions[3].Axis = AxisFlagEnum.AxisX;

				//c.Interaction.Actions["Scale"].Axis = AxisFlagEnum.AxesYY2;
				//c.Interaction.Actions["Translate"].Axis = AxisFlagEnum.AxesYY2;
				//c.Interaction.Actions["Zoom"].Axis = AxisFlagEnum.AxesYY2;

				c.Header.LocationDefault = new Point(13, 1);
				c.Header.Style.Border.Rounding.LeftTop = 1;
				c.Header.Style.Border.Rounding.RightTop = 1;
				c.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
				c.Header.Style.Border.Color = SystemColors.ControlDark;
				c.Header.Style.BackColor = Color.White;

				c.ChartArea.Inverted = false;
				c.ChartArea.PlotArea.BackColor = Color.White;
				c.ChartArea.Margins.SetMargins(7, 0, 17, 0);
				c.ChartArea.LocationDefault = new Point(7, 17);
				c.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei);

				c.ChartArea.AxisX.Visible = true;
				c.ChartArea.AxisX.TickMinor = TickMarksEnum.None;
				c.ChartArea.AxisX.TickMajor = TickMarksEnum.Inside;
				c.ChartArea.AxisX.ForeColor = Color.Black;
				c.ChartArea.AxisX.AnnoFormat = FormatEnum.DateManual;
				c.ChartArea.AxisX.AnnoFormatString = " H:mm";
				c.ChartArea.AxisX.ScrollBar.Appearance = ScrollBarAppearanceEnum.Flat;
				c.ChartArea.AxisX.ScrollBar.Size = 9;
				c.ChartArea.AxisX.ScrollBar.Visible = true;
				c.ChartArea.AxisX.ScrollBar.Buttons = AxisScrollBarButtonFlags.NoButtons;
				c.ChartArea.AxisX.ScrollBar.ScrollKeys = ScrollKeyFlags.Cursor;

				c.ChartArea.AxisY.Visible = false;
				c.ChartArea.AxisY.GridMajor.Visible = true;
				c.ChartArea.AxisY.GridMajor.Color = Color.Gray;
				c.ChartArea.AxisY.ForeColor = Color.Black;
				c.ChartArea.AxisY.TickMinor = TickMarksEnum.None;
				c.ChartArea.AxisY.TickMajor = TickMarksEnum.Inside;
				c.ChartArea.AxisY.TickLabels = TickLabelsEnum.None;
				c.ChartArea.AxisY.OnTop = false;
				c.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericManual;

				c.ChartArea.AxisY2.Visible = false;

				c.ChartGroups[0].ShowOutline = false;
				c.ChartGroups[1].ShowOutline = false;
				c.ChartGroups[0].ChartType = Chart2DTypeEnum.XYPlot;
				c.ChartGroups[1].ChartType = Chart2DTypeEnum.Area;

				// c.Style.BackColor = Color.White;
				// c.BackColor = Color.WhiteSmoke;
				// c.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Raised;
				((ISupportInitialize)c).EndInit();
				pnMaps.Controls.Add(c);

				c.TabStop = true;
				c.ContextMenuStrip = cmMap;
			}
			SetChartMapHeight(_monitorSettings.BigMap);
			pnMaps.Controls.SetChildIndex(pnEdit, 0);

			#endregion | Timeline   |

			#region | Trends     |

			_chTrends = new C1Chart();
			((System.ComponentModel.ISupportInitialize)_chTrends).BeginInit();
			_chTrends.Dock = System.Windows.Forms.DockStyle.Fill;
			_chTrends.TabStop = false;

			_chTrends.MouseMove += chTrends_MouseMove;
			_chTrends.MouseDown += chTrends_MouseDown;
			_chTrends.MouseLeave += chTrends_MouseLeave;
			if (use_hatchbrushes_for_trends_chart)
				_chTrends.DrawDataSeries += chTrends_DrawDataSeries;

			InitChartTrends(_chTrends);

			((System.ComponentModel.ISupportInitialize)_chTrends).EndInit();

			#endregion | Trends     |

			#region | Losses     |

			_chLosses = new LossesChart();
			((System.ComponentModel.ISupportInitialize)_chLosses).BeginInit();
			_chLosses.Dock = System.Windows.Forms.DockStyle.Fill;
			_chLosses.TabStop = false;
			_chLosses.Visible = true;

			_chLosses.MouseMove += new MouseEventHandler(chLosses_MouseMove);
			_chLosses.MouseDown += new MouseEventHandler(chLosses_MouseDown);
			_chLosses.MouseLeave += new EventHandler(chLosses_MouseLeave);
			_chLosses.DrawDataSeries += new DrawDataSeriesEventHandler(chLosses_DrawDataSeries);
			InitChartLosses(_chLosses);

			((System.ComponentModel.ISupportInitialize)_chLosses).EndInit();

			#endregion | Losses     |

			#region | Marks      |

			// chCompare initialization
			_chMarks = new C1Chart();
			((ISupportInitialize)_chMarks).BeginInit();
			_chMarks.Dock = DockStyle.Right;
			_chMarks.TabStop = false;
			_chMarks.Visible = true;
			_chMarks.Size = new Size(250, 100); //new Size(550, 100);
			_chMarks.ForeColor = Color.Transparent;

			_chMarks.MouseMove += new MouseEventHandler(chMarks_MouseMove);
			_chMarks.MouseDown += new MouseEventHandler(chMarks_MouseDown);
			//chMarks.MouseLeave += new EventHandler(chReps_MouseLeave);
			//chMarks.DrawDataSeries += new DrawDataSeriesEventHandler(chLosses_DrawDataSeries);

			_chMarks.ChartGroups[0].ChartType = Chart2DTypeEnum.Pie;
			_chMarks.ColorGeneration = ColorGeneration.Verve;
			_chMarks.ChartArea.PlotArea.View3D.Depth = 20;
			_chMarks.ChartArea.PlotArea.View3D.Elevation = 45;
			_chMarks.UseAntiAliasedGraphics = true;

			_chMarks.ChartArea.AxisY.AutoMinor = false;
			_chMarks.ChartArea.AxisX.AutoMajor = false;
			_chMarks.ChartArea.AxisY.GridMajor.Pattern = LinePatternEnum.Dash;
			_chMarks.ChartArea.AxisY.GridMajor.Color = Color.DarkGray;
			_chMarks.ChartArea.AxisY.GridMajor.Visible = true;
			_chMarks.ChartArea.AxisY.TickMajor = TickMarksEnum.None;
			_chMarks.ChartArea.AxisY.OnTop = false;

			//chMarks.ChartArea.AxisX.TickMajor = TickMarksEnum.None;
			_chMarks.ChartArea.AxisX.AutoMinor = false;
			_chMarks.ChartArea.AxisX.AutoMajor = true;
			_chMarks.ChartArea.AxisX.Thickness = 1;
			_chMarks.ChartArea.AxisX.AnnoMethod = AnnotationMethodEnum.ValueLabels;

			_chMarks.Legend.Compass = CompassEnum.East;
			_chMarks.Style.BackColor = Color.White;

			////chMarks.Header.Style.Font = Font;
			////chMarks.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
			////chMarks.Header.Style.Border.Color = Color.FromArgb(255, 127, 127, 127);
			////chMarks.Header.Style.Border.Rounding.All = 3;

			//chMarks.ChartLabels.AutoArrangement.Options = AutoLabelArrangementOptions.Default;
			//chMarks.ChartLabels.AutoArrangement.Method = AutoLabelArrangementMethodEnum.FindingOptimum;

			((System.ComponentModel.ISupportInitialize)_chMarks).EndInit();

			#endregion | Marks      |

			#region | Analyse    |

			_chAnalyse = new C1Chart();
			((System.ComponentModel.ISupportInitialize)_chAnalyse).BeginInit();

			_chAnalyse.MouseMove += new System.Windows.Forms.MouseEventHandler(chAnalyse_MouseMove);
			_chAnalyse.DoubleClick += new System.EventHandler(chAnalyse_DoubleClick);
			_chAnalyse.MouseUp += new System.Windows.Forms.MouseEventHandler(chAnalyse_MouseUp);

			InitChartAnalyse(_chAnalyse);
			((System.ComponentModel.ISupportInitialize)_chAnalyse).EndInit();

			#endregion | Analyse    |

			#region | Print      |

			_chPrint = new C1Chart();
			((System.ComponentModel.ISupportInitialize)_chPrint).BeginInit();
			//chPrint.Dock = System.Windows.Forms.DockStyle.Right;
			_chPrint.TabStop = false;
			_chPrint.Visible = true;
			_chPrint.Size = new Size(250, 100); //new Size(550, 100);

			_chPrint.UseAntiAliasedGraphics = false;
			_chPrint.ChartArea.PlotArea.View3D.Depth = 0;
			_chPrint.ChartArea.PlotArea.View3D.Elevation = 0;
			_chPrint.ColorGeneration = ColorGeneration.Custom;
			_chPrint.ChartGroups[0].ChartType = Chart2DTypeEnum.Bar;
			_chPrint.ChartGroups[0].Stacked = false;
			_chPrint.ChartGroups[0].Bar.ClusterWidth = 70;	//
			//chPrint.ChartGroups[0].Bar.Appearance = BarAppearanceEnum.Cylinder;
			_chPrint.ChartGroups[0].LegendReversed = true;

			_chPrint.ChartArea.AxisY.AutoMinor = false;
			_chPrint.ChartArea.AxisY.GridMajor.Pattern = LinePatternEnum.Dash;
			_chPrint.ChartArea.AxisY.GridMajor.Color = Color.DarkGray;
			_chPrint.ChartArea.AxisY.GridMajor.Visible = true;
			_chPrint.ChartArea.AxisY.TickMajor = TickMarksEnum.None;
			_chPrint.ChartArea.AxisY.OnTop = false;

			_chPrint.ChartArea.AxisX.AutoMinor = false;
			_chPrint.ChartArea.AxisX.AutoMajor = true;
			_chPrint.ChartArea.AxisX.Thickness = 1;
			_chPrint.ChartArea.AxisX.AnnoMethod = AnnotationMethodEnum.ValueLabels;

			_chPrint.Legend.Visible = false;
			_chPrint.Legend.Compass = CompassEnum.East;
			_chPrint.Style.BackColor = Color.White;

			_chPrint.Header.Style.Font = Font;
			_chPrint.Header.Style.ForeColor = _chLosses.ForeColor;
			_chPrint.Header.Style.BackColor = _chLosses.BackColor;
			_chPrint.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
			_chPrint.Header.Style.Border.Color = Color.Black;
			_chPrint.Header.Style.Border.Rounding.All = 3;

			// Нижняя подпись нужна только чтобы добавить место под нижней осью, поэтому делаем прозрачной
			_chPrint.Footer.Text = "Just for ...";
			_chPrint.Footer.Style.ForeColor = Color.Transparent;

			((ISupportInitialize)_chPrint).EndInit();

			#endregion | Print      |

			pnTrends.Controls.Add(_chTrends);
			pnTrends.Controls.Add(_grTrends);

			pnLosses.Controls.Add(_chLosses);
			pnLosses.Controls.Add(_grLosses);
			pnLosses.Controls.Add(_chMarks);

			_pnAnalyseChart.Controls.Add(_chAnalyse);
			_pnAnalyseChart.BackColor = Color.White;
		}

		private void InitChartTrends(C1Chart c1ch)
		{
			c1ch.Style.ForeColor = Color.Transparent;
			c1ch.Style.BackColor = Color.White;
			
			c1ch.ChartGroups[0].ChartType = Chart2DTypeEnum.Bar;
			c1ch.ChartGroups[0].Stacked = true;
			c1ch.ChartGroups[0].Bar.ClusterWidth = 70;	//
			c1ch.ChartGroups[0].LegendReversed = true;
			c1ch.ChartGroups[1].ChartType = Chart2DTypeEnum.Step;
			//c1ch.ChartArea.Inverted = true;
			//c1ch.ChartArea.LocationDefault = new Point(0, 22);
			//c1ch.ChartArea.Margins.Top = 14;
			//c1ch.ChartArea.Margins.Bottom = 10;
			//c1ch.ChartArea.Margins.Left = 10;
			//c1ch.ChartArea.Margins.Right = 10;

			////// 3D:
			//////c1ch.ChartGroups[0].Bar.Appearance = BarAppearanceEnum.Cylinder;
			////c1ch.ChartArea.PlotArea.View3D.Depth = 5;
			////c1ch.ChartArea.PlotArea.View3D.Elevation = 10;
			////c1ch.ChartArea.PlotArea.View3D.Shading = ShadingEnum.ColorLight;
			////c1ch.ChartArea.PlotArea.View3D.Rotation = 20;
			////c1ch.UseAntiAliasedGraphics = true;

			c1ch.ChartArea.AxisY.AutoMinor = false;
			c1ch.ChartArea.AxisX.AutoMajor = false;
			c1ch.ChartArea.AxisY.GridMajor.Pattern = LinePatternEnum.Dash;
			c1ch.ChartArea.AxisY.GridMajor.Color = Color.DarkGray;
			c1ch.ChartArea.AxisY.GridMajor.Visible = true;
			c1ch.ChartArea.AxisY.TickMajor = TickMarksEnum.None;
			c1ch.ChartArea.AxisY.OnTop = false;
			c1ch.ChartArea.AxisY.ForeColor = Color.DimGray;

			//c1ch.ChartArea.AxisX.TickMajor = TickMarksEnum.None;
			c1ch.ChartArea.AxisX.AutoMinor = false;
			c1ch.ChartArea.AxisX.AutoMajor = true;
			c1ch.ChartArea.AxisX.Thickness = 1;
			c1ch.ChartArea.AxisX.AnnoMethod = AnnotationMethodEnum.ValueLabels;
			//c1ch.ChartArea.AxisX.ForeColor = Color.DimGray;

			c1ch.ChartArea.AxisY2.Visible = false;
			c1ch.ChartArea.AxisY2.AutoMinor = false;
			c1ch.ChartArea.AxisY2.AutoMajor = false;

			c1ch.Legend.Compass = CompassEnum.East;
			c1ch.Legend.Visible = true;
			c1ch.Legend.Style.ForeColor = Color.Black;

			c1ch.Header.Style.ForeColor = Color.Black;
			c1ch.Header.Style.Font = Font;
			c1ch.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
			c1ch.Header.Style.Border.Color = Color.Black;
			c1ch.Header.Style.Border.Rounding.All = 3;

			//c1ch.ChartLabels.AutoArrangement.Options = AutoLabelArrangementOptions.Default;
			//c1ch.ChartLabels.AutoArrangement.Method = AutoLabelArrangementMethodEnum.FindingOptimum;

			////c1ch.Style.GradientStyle = GradientStyleEnum.VerticalCenter;
			////c1ch.Style.BackColor2 = SystemColors.Control;
			////c1ch.Style.BackColor = Color.White;
		}

		private void InitChartLosses(C1Chart c1ch)
		{
			c1ch.UseAntiAliasedGraphics = false;
			c1ch.ChartArea.PlotArea.View3D.Depth = 0;
			c1ch.ChartArea.PlotArea.View3D.Elevation = 0;
			c1ch.ChartArea.PlotArea.UseAntiAlias = true;
			c1ch.Style.ForeColor = Color.Transparent;
			c1ch.Style.BackColor = Color.White;
			c1ch.ColorGeneration = ColorGeneration.Custom;
			c1ch.ChartGroups[0].ChartType = Chart2DTypeEnum.Bar;
			c1ch.ChartGroups[0].Stacked = false;
			c1ch.ChartGroups[0].Bar.ClusterWidth = 70;
			c1ch.ChartGroups[0].LegendReversed = true;

			c1ch.ChartArea.AxisY.AutoMinor = false;
			c1ch.ChartArea.AxisY.GridMajor.Pattern = LinePatternEnum.Dash;
			c1ch.ChartArea.AxisY.GridMajor.Color = Color.DarkGray;
			c1ch.ChartArea.AxisY.GridMajor.Visible = true;
			c1ch.ChartArea.AxisY.TickMajor = TickMarksEnum.None;
			c1ch.ChartArea.AxisY.OnTop = false;
			c1ch.ChartArea.AxisY.ForeColor = Color.DimGray;

			c1ch.ChartArea.AxisX.AutoMinor = false;
			c1ch.ChartArea.AxisX.AutoMajor = true;
			c1ch.ChartArea.AxisX.Thickness = 1;
			c1ch.ChartArea.AxisX.AnnoMethod = AnnotationMethodEnum.ValueLabels;
			//c1ch.ChartArea.AxisX.ForeColor = Color.Black;


			c1ch.Legend.Visible = true;
			c1ch.Legend.Compass = CompassEnum.East;
			c1ch.Legend.Style.ForeColor = Color.Black;
			c1ch.Legend.Reversed = false;

			c1ch.Header.Style.Font = Font;
			c1ch.Header.Style.ForeColor = Color.Black;
			c1ch.Header.Style.BackColor = c1ch.BackColor;
			c1ch.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
			c1ch.Header.Style.Border.Color = Color.Black;
			c1ch.Header.Style.Border.Rounding.All = 3;

			// Нижняя подпись нужна только чтобы добавить место под нижней осью, поэтому делаем прозрачной
			c1ch.Footer.Text = "Just for ...";
			c1ch.Footer.Style.ForeColor = Color.Transparent;

			c1ch.ChartGroups[1].ChartType = Chart2DTypeEnum.XYPlot;
			c1ch.ChartGroups[1].LegendReversed = true;
			c1ch.ChartArea.AxisY2.Visible = true;
			c1ch.ChartArea.AxisY2.OnTop = true;
			c1ch.ChartArea.AxisY2.ForeColor = Color.DimGray;

			//c1ch.ChartLabels.AutoArrangement.Options = AutoLabelArrangementOptions.Default;
			//c1ch.ChartLabels.AutoArrangement.Method = AutoLabelArrangementMethodEnum.FindingOptimum;
		}

		private void InitChartAnalyse(C1Chart c1ch)
		{
			c1ch.Name = "chCalc";
			c1ch.TabStop = false;
			c1ch.Dock = System.Windows.Forms.DockStyle.Fill;
			c1ch.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.None;
			c1ch.Style.Border.Color = SystemColors.ControlDark;
			c1ch.Style.BackColor = Color.White;
			c1ch.ChartGroups[0].ChartType = Chart2DTypeEnum.Bar;
			c1ch.ChartGroups[0].Stacked = true;
			c1ch.ChartGroups[0].Bar.ClusterWidth = 100;
			c1ch.ChartArea.Inverted = true;

			//c1ch.ChartArea.LocationDefault = new Point(15, 15);
			//c1ch.ChartArea.SizeDefault = New Size(-1, 400)
			//c1ch.ChartArea.Margins.SetMargins(0, 0, 0, 0);
			//c1ch.ChartArea.AxisX.Visible = false;

			//c1ch.ChartArea.LocationDefault = new Point(1, 1);
			//c1ch.ChartArea.SizeDefault = new Size(-1, -1);
			c1ch.ChartArea.Margins.SetMargins(1, 1, 1, 1);
			c1ch.ChartArea.AxisX.Visible = false;

			//c1ch.ChartArea.AxisX.AnnoMethod = AnnotationMethodEnum.ValueLabels;
			c1ch.ChartArea.AxisY.Visible = false;
			c1ch.ChartArea.AxisY.Min = 0;
			c1ch.ChartArea.AxisY.Max = 100.0;
			c1ch.ChartArea.AxisY.AutoMinor = false;
			c1ch.ChartArea.AxisY.GridMajor.Pattern = LinePatternEnum.Dot;
			c1ch.ChartArea.AxisY.GridMajor.Color = Color.LightGray;
			c1ch.ChartArea.AxisY.GridMajor.Visible = true;
			c1ch.ChartArea.AxisY.ForeColor = Color.Black;
			c1ch.ChartArea.AxisY.OnTop = true;
			c1ch.ChartArea.AxisY.Compass = CompassEnum.North;
			c1ch.ChartArea.AxisY.Alignment = StringAlignment.Far;
			c1ch.ChartArea.AxisY.TickLabels = TickLabelsEnum.NextToAxis;

			c1ch.Interaction.Enabled = true;
			c1ch.Interaction.Actions[0].Modifier = Keys.I;
			c1ch.Interaction.Actions[0].Axis = AxisFlagEnum.AxisX;
			c1ch.Interaction.Actions[3].Modifier = Keys.None;
			c1ch.Interaction.Actions[3].Axis = AxisFlagEnum.AxesXY;

			// легенда не нужна, просто не смог найти способ, как оставить свободное место справа от графика для подписей.
			c1ch.Legend.Compass = CompassEnum.East;
			c1ch.Legend.Visible = true;
			c1ch.Legend.Text = new string(' ', 40) + ".";
			c1ch.Legend.Style.ForeColor = c1ch.BackColor;

			// заголовок тоже не нужен, исмользуется для создания пустого пространства слева от графика
			c1ch.Header.Compass = CompassEnum.West;
			//c1ch.Header.Text = new string(' ', 25) + ".";
			c1ch.Header.Style.ForeColor = _colForeBold;
			c1ch.Header.Style.Font = _fontBoldArial;
			c1ch.Header.Style.HorizontalAlignment = AlignHorzEnum.Near;
			c1ch.ForeColor = Color.White;

			////c1ch.Footer.Compass = CompassEnum.North;
			////c1ch.Footer.Text = "Actual time composition";
			////c1ch.Footer.Visible = true;
			////c1ch.ForeColor = Color.White;

			c1ch.ChartLabels.AutoArrangement.Options = AutoLabelArrangementOptions.Default;
			c1ch.ChartLabels.AutoArrangement.Method = AutoLabelArrangementMethodEnum.FindingOptimum;
		}

		private void LoadSettings()
		{
			if (Properties.Settings.Default.CallUpgrade) {
				_monitorSettings.Upgrade();
				_monitorSettings.CallUpgrade = false;
			}

			//Associate settings property event handlers.
			_monitorSettings.SettingsSaving += new System.Configuration.SettingsSavingEventHandler(monitorsettings_SettingsSaving);

			Location = _monitorSettings.Location;
            Size = new Size(1280, 720); //_monitorSettings.Size;
			_chbShowTips.Checked   = _monitorSettings.Tips;
			_chbShowBigMap.Checked = _monitorSettings.BigMap;

            tsbBigChartCounter.Checked = cbBigChartCounter.Checked = _monitorSettings.Tips_counter;
            //tsbBigChartCounter.Checked = _monitorSettings.Tips_counter;
            set_tsbBigChartCounter(tsbBigChartCounter);

            tsbShowTipsCounter.Checked = cbShowTipsCounter.Checked = _monitorSettings.BigMap_counter;
            tsbShowLimitsCounter.Checked = _monitorSettings.ShowOverLimits ;
            set_tsbBigChartCounter(tsbShowTipsCounter);
            set_tsbBigChartCounter(tsbShowLimitsCounter);  

			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			pnLegend.Visible = _monitorSettings.ColorLegend;
			if (_monitorSettings.Height1 > 10) {
				_pnAnalyseChart.Visible = true;
				_splAnalyse.Image = Res.s_hide;
			} else {
				_pnAnalyseChart.Visible = false;
				_splAnalyse.Image = Res.s_show;
			}

			// customizing of mode of data presentation according to settings
			MU.UseTimecorrection = _monitorSettings.TimeCorrection;
			_mapMode = true;

			if (_monitorSettings.Lines == null)
				_monitorSettings.Lines = new ArrayList();
            if (_monitorSettings.Lines_count == null)
                _monitorSettings.Lines_count = new ArrayList();

			TmStyle = (TimeStyle)_monitorSettings.Mins;

			// Get grid columns settings from 'Applications settings'
			_dsCols = Utils.DeserializeDataObject("cols", -1) as DataSet;
			if (_dsCols != null) _dsColsCash = _dsCols.Copy();


            ///////////////////////////////////////
            if (use_easy_counter)
            {
             tpDetail.Visible          = false;
             tpMap.Visible             = false;
             tpTrends.Visible          = false;
             tpLosses.Visible          = false;
             tpAnalyse.Visible         = false;
             tabMain.TabPages.Remove(tpDetail);
             tabMain.TabPages.Remove(tpMap);
             tabMain.TabPages.Remove(tpTrends);
             tabMain.TabPages.Remove(tpLosses);
             tabMain.TabPages.Remove(tpAnalyse);

            _pnAnalyseChart.Visible    = false;
            tsbEdit.Visible            = false;
            tsbMainHideFilters.Visible = false;
            tscbMainShifts.Visible     = false;
            tscbMainFormats.Visible    = false;
            tscbMainBrands.Visible     = false;
            tsbMainFiltReset.Visible   = false;
            pnLegend.Visible           = false;
		//	_monitorSettings.ColorLegend.Visible = false;
			pnLegend.Visible           = false;

            }


 


		}

		private void monitorsettings_SettingsSaving(object sender, CancelEventArgs e)
		{
			_monitorSettings.WindowState = WindowState;
			if (this.WindowState == FormWindowState.Normal) {
				_monitorSettings.Size = Size;
				_monitorSettings.Location = Location;
			}
			_monitorSettings.UnitsMode      = 1;
			_monitorSettings.Height1        = _pnAnalyseChart.Visible ? 50 : 0;
			_monitorSettings.Tips           = _chbShowTips.Checked;
			_monitorSettings.BigMap         = _chbShowBigMap.Checked;
			_monitorSettings.AutoUpdate     = _mnuAutoUpdate.Checked;
			_monitorSettings.EmptyAtStart   = _mnuAtStartup.Checked;
			_monitorSettings.Mins           = (int)TmStyle;
            _monitorSettings.Tips_counter   = cbBigChartCounter.Checked ;
            _monitorSettings.BigMap_counter = cbShowTipsCounter.Checked ;
            _monitorSettings.ShowOverLimits = tsbShowLimitsCounter.Checked;


			if (_lu != null) {
				_monitorSettings.Lines.Clear();
				foreach (MU l in _lu)
					_monitorSettings.Lines.Add(l.ID);
			}
            if (_lu_ != null)
            {
                _monitorSettings.Lines_count.Clear();
                foreach (MU l in _lu_)
                    _monitorSettings.Lines_count.Add(l.ID);
            }
		}

		private void frmMonitor_Closing(object sender, CancelEventArgs e)
		{
			_state = C.State.Closing;
		}

		private void frmMonitor_Closed(object sender, EventArgs e)
		{
			_monitorSettings.Save();

			// Save grids columns settings
			if (_dsCols != null) {
				if (_dsCols.Tables["narrow"].Columns.Contains("FullCaption")) _dsCols.Tables["narrow"].Columns.Remove("FullCaption");
				if (_dsCols.Tables["wide"].Columns.Contains("FullCaption")) _dsCols.Tables["wide"].Columns.Remove("FullCaption");
				_dsCols.AcceptChanges();
				if (_dsColsCash == null || _dsCols.GetXml() != _dsColsCash.GetXml())
					Utils.SerializeDataObject("cols", _dsCols);
			}
		}

		/// <summary>
		/// Выполняется только один раз при первом успешном вызове
		/// </summary>
		/// <param name="n"></param>
		private void InitOnce()
		{
			// настраиваем колонки для закладки Detailed List

			if (_gridColumns.Count == 0) InitGridsColumns();

			// выполняется только один раз при первом запросе
			if (_grLines.Cols.Contains("chart") || MU.Total == null || MU.Kpi == null)
				return;

			#region | grLines           |

			//grLines.BeginInit();
			_grLines.DataSource = MU.Total.DefaultView;
			//grLines.EndInit();
			SetupGrid(_grLines, true);

			// add unbound column
			Column col = _grLines.Cols.Add();
			col.Name = "chart";
			col.Caption = "Short review";

			#endregion | grLines           |

			#region | grDet styles      |

			CellStyle cs;
			string l;

			foreach (string k in MU.Kpi.Roots.Keys) {
				cs = _grDet.Styles.Add(k);
				cs.BackColor = MU.Kpi.Colors[k];
				cs.ForeColor = MU.Kpi.Colors[k];
				l = "l" + k;
				cs = _grDet.Styles.Add(l);
				cs.BackColor = MU.Kpi.Colors[k];
				cs.ForeColor = MU.Kpi.Colorsfont[k];
			}

			#endregion | grDet styles      |

			InitLegend();

			// установка источника данных для комбобокса на закладке "распределение потерь"
			tscbLossesBaseTime.ComboBox.DataSource = new DataView(MU.Kpi.TableKPI, "Agenda<0", "Agenda DESC", DataViewRowState.CurrentRows);
			tscbLossesBaseTime.ComboBox.ValueMember = "Sign";
			tscbLossesBaseTime.ComboBox.DisplayMember = "Name";				//defaultlanguage ? "EName" : "Value";
			tscbLossesBaseTime.ComboBox.SelectedValue = MU.Kpi.MainKpiKey;
			tscbLossesBaseTime.SelectedIndexChanged -= tscbLossesBaseTime_SelectedIndexChanged;
			tscbLossesBaseTime.SelectedIndexChanged += tscbLossesBaseTime_SelectedIndexChanged;
		}

		private void InitLegend()
		{
			if (MU.Kpi == null)
				return;

			pnLegend.Controls.Clear();
			pnLegend.Controls.Add(btHideLegend);
			Padding marg = new Padding(0, 1, 1, 0); //(2, 0, 2, 0);
			Padding padd = new Padding(0, 1, 0, 3);
			foreach (string k in MU.Kpi.Colors.Keys) {
				if (MU.Kpi.ViewWeight[k] < 0 || MU.Kpi.ViewWeight[k] > 1) continue;
				System.Windows.Forms.Label lb = new System.Windows.Forms.Label
				{
					AutoSize = true,
					Margin = marg,
					Padding = padd
				};
				//lb.Text = MonitorUnit.kpi.Names[k];
				if (!MU.UseDefaultLanguage)
					lb.Text = C.GetRowField(MU.tbKPI, string.Format("Sign='{0}'", k), "Value");
				if (MU.UseDefaultLanguage || lb.Text == "")
					lb.Text = C.GetRowField(MU.tbKPI, string.Format("Sign='{0}'", k), "EName");
				lb.BackColor = MU.Kpi.Colors[k];
				lb.ForeColor = MU.Kpi.Colorsfont[k];
				pnLegend.Controls.Add(lb);
			}
		}

		#endregion | At start/stop events    |

		private void mnuSelect_DataChanged(object sender, EventArgs e)
		{
			StripSelect.DataChangesArgs arg = e as StripSelect.DataChangesArgs;
			MU.SetTimePeriod(arg.DateStart, arg.DateEnd);

			// если дата не с 00:00 то возможно используем коррекцию времени
			MU.UseTimecorrection = MU.DtStart.Date != MU.DtStart ? false : _monitorSettings.TimeCorrection;
              for (int c = 0; c < _ch_.Length; c++) 			//	hide 'Timeline' charts
                 _ch_[c].Visible = false;
			LoadStart();
		}

		private void LoadStart()
		{
			if (!_lu.Exists(el => el.Kind == UnitKind.Line))
				//return;
                if (!_lu_.Exists(el => el.Kind == UnitKind.Line))
                    return;

			_swTotal = Stopwatch.StartNew();
			_swDb    = Stopwatch.StartNew();

            if (_lu == null || _lu.Count < 1)
            {

                if (_lu_ == null || _lu_.Count < 1)
                {
                    SetLineSelected(-1);
                    return;
                }
            }

			_mapMode = MU.Mins <= MaxMapModeMins;
			_state = C.State.DataLoading;

			L = -1;
			_trendCol = "";		// for trends mode

			// Если до этого было состояние редактирования то сбрасываем
			_isEdit         = false;
			tsbEdit.Checked = false;
			pnEdit.Visible  = false;
            button2.Enabled = false;

			#region | controls settings |

			// Dropdown line selector text
			MU.UseDefaultLanguage         = Thread.CurrentThread.CurrentCulture.Name == "en-GB";
			tssbMainDateLineSelector.Text = String.Format("{0:g}   -   {1:g}", MU.DtStart, MU.DtEnd.AddMinutes(-1));

			SetControlsBeforeLoad();

			// Start "running colored zones" at statusbar
			_loadingText = string.Empty;
			_tmLoading.Start();
			//sb.CreateGraphics().FillRectangle(Brushes.DarkGreen, new Rectangle(0, 0, sb.Width, sb.Height));

			// tabs handling
            if (!use_easy_counter)
			if (_mapMode) {
				if (!tabMain.TabPages.Contains(tpMap)) tabMain.TabPages.Insert(1, tpMap);
			} else {
				if (tabMain.TabPages.Contains(tpMap))  tabMain.TabPages.Remove(tpMap);
            }
            
            if (!use_easy_counter)
			if (MU.Mins <= MU.minTrendMins) {
				if (tabMain.TabPages.Contains(tpTrends))
					tabMain.TabPages.Remove(tpTrends);
			} else if (tabMain.TabPages.Contains(tpLosses)) {
				// вставить перед закладкой Reports
				if (!tabMain.TabPages.Contains(tpTrends)) tabMain.TabPages.Insert(tabMain.TabCount - 2, tpTrends);
			} else {
				// вставить на предпоследнюю позицию
				if (!tabMain.TabPages.Contains(tpTrends)) tabMain.TabPages.Insert(tabMain.TabCount - 1, tpTrends);
			}

			// chart 'Map' handling
            if (_mapMode)
            {
                for (int c = 0; c < _ch.Length; c++) 			//	hide 'Timeline' charts
                    _ch[c].Visible = false;
              //  for (int c = 0; c < _ch_.Length; c++) 			//	hide 'Timeline' charts
               //     _ch_[c].Visible = false;
            }
			#endregion | controls settings |

			MU.Total.Clear();
			MU.ResetTopUnitLevel();
			_qu.Clear();
			_nodes.Clear();

			int n = 0;
                foreach (var unit in _lu)
                {
                    unit.Clear();
                    DataRow dr = MU.Total.NewRow();
                    MU.Total.Rows.Add(dr);
                    dr["Line"] = unit.GetLongUnitName(false);
                    dr["ID"] = n;
                    unit.L = n;
                    n++;
                    MU.SetTopUnitLevel(unit);
                    if (unit.Kind == UnitKind.Line)
                        _qu.Enqueue(unit);
                    else
                        _nodes.Add(unit);
                }


			//grLines.Update();
			_grLines.Redraw = false;

			// переменные для трассировки при последовательной загрузке данных
			_currUnit = null;
            _currUnit_ = null;
			loadfailed = false;
            if  (!(_lu == null || _lu.Count < 1))
            {
                if (!use_threading)
                {
                    // in debug mode request to db in the same thread
                    _qu.Peek().LoadData();
                    LoadStep(_qu.Peek());
                }
                else
                {
                    // run the database query in another thread
                    _dbWorker.RunWorkerAsync(_qu.Peek());
                }
            }

        //    return;

            _qu_.Clear();
            _nodes_.Clear();
            n = 0;
            foreach (var unit in _lu_)
            {
                unit.Clear();
                DataRow dr = MU.Total.NewRow();
                MU.Total.Rows.Add(dr);
                dr["Line"] = unit.GetLongUnitName(false);
                dr["ID"] = n;
                unit.L = n;
                n++;
                MU.SetTopUnitLevel(unit);
                if (unit.Kind == UnitKind.Line)
                    _qu_.Enqueue(unit);
                else
                    _nodes_.Add(unit);

             //   BuildChartMapCounter(unit ,true);
              //  tabMain.Visible = true;
              //  tpCounters.Visible = true;

            }
          //  return;
            if (!(_lu_ == null || _lu_.Count < 1))
            {

                if (!use_threading)    //(1==1) // 
                {
                    // in debug mode request to db in the same thread
                    _qu_.Peek().LoadData_Counters();
                    LoadStep_Counters(_qu_.Peek());
                }
                else
                {
                    // run the database query in another thread
                  //  _dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
                  //  if (_qu.Count == 0)
                    _tm_counter.Enabled = true;
                }



                // tabMain.Visible = true;
                // tpCounters.Visible = true;
                // Finish();
            }




		}

		private void SetControlsBeforeLoad()
		{
			// Clear Controls
			_txtError.Text = "";
			sbpLine.Text = "";
			_grDet.DataSource = null;
			_grDet.Redraw = false;

			// Compare tab
			_grTrends.DataSource = null;
			_dvT = null;
			_chTrends.ChartGroups[0].ChartData.SeriesList.Clear();
			_chTrends.ChartGroups[1].ChartData.SeriesList.Clear();
			_chTrends.Refresh();
			_chAnalyse.ChartGroups[0].ChartData.SeriesList.Clear();
			_chAnalyse.Refresh();

			_grTrends.DataSource = null;
			_chMarks.ChartGroups[0].ChartData.SeriesList.Clear();
			_chLosses.ChartGroups[0].ChartData.SeriesList.Clear();
			_chLosses.Header.Visible = false;
			_chLosses.Header.Text = "";
			_chMarks.Refresh();
			_chLosses.Refresh();

			dgvDnTms.DataSource = null;
			dgvProds.DataSource = null;
			dgvShifts.DataSource = null;

			////foreach (C1DataColumn c in grLines.Columns)
			////   c.FooterText = "";

			// Hide controls
			//tabMain.Visible = false;
			pnEdit.Visible  = false;
			tsbEdit.Checked = false;
			tsbMainRefreshFailed.Visible = false;

			tsbMainNext.Visible = _mapMode;
			tsbMainPrev.Visible = _mapMode;
			tsbMainNow.Visible = _mapMode;

			tsMain.Enabled = false;
			tsTrends.Enabled = false;

			// trends mode settings
			tsbTrendsYears.Enabled    = (int)(MU.Mode & MU.PeriodMode.Years) > 0;
			tsbTrendsQuarters.Enabled = (int)(MU.Mode & MU.PeriodMode.Quarters) > 0;
			tsbTrendsMonths.Enabled   = (int)(MU.Mode & MU.PeriodMode.Months) > 0;
			tsbTrendsWeek.Enabled     = (int)(MU.Mode & MU.PeriodMode.Weeks) > 0;
			tsbTrendsDays.Enabled     = (int)(MU.Mode & MU.PeriodMode.Days) > 0;

			// Отрисовываем изменения контролов
			Application.DoEvents();
		}

		private void LoadFailedStart()
		{
			_qu.Clear();
			_nodes.Clear();
			
			for (int n = 0; n < _lu.Count; n++) {
				if (_lu[n].Kind != UnitKind.Line)
					_nodes.Add(_lu[n]);
				else if (_lu[n].IsEmpty() || _lu[n].Error != "") {
					_qu.Enqueue(_lu[n]);
					_lu[n].Clear();
					_lu[n].L = n;
				}
			}
			if (_qu.Count == 0)
				return;

			_state = C.State.DataLoading;

			// Clear Controls
			_txtError.Text = "";

			tsMain.Enabled = false;
			tsbMainRefreshFailed.Visible = false;
			Application.DoEvents();

			// Start "running colored zones" at statusbar
			_loadingText = string.Empty;
			_tmLoading.Start();
			sb.CreateGraphics().FillRectangle(Brushes.DarkGreen, new Rectangle(0, 0, sb.Width, sb.Height));

			_swTotal = Stopwatch.StartNew();
			_swDb = Stopwatch.StartNew();
			//sw_db.Stop();

			loadfailed = true;
			_currUnit = null;
			if (!use_threading) {
				// in debug mode request to db in the same thread
				//LoadData(qu.Peek());
				_qu.Peek().LoadData();
				LoadStep(_qu.Peek());
			} else {
				// run the database query in another thread
				_dbWorker.RunWorkerAsync(_qu.Peek());
			}
		}

		private void dbworker_DoWork(object sender, DoWorkEventArgs e)
		{
			MU unit = e.Argument as MU;
			_swDb.Stop();
			unit.LoadData();
			//Thread.Sleep(500 * (unit.Order % 4));
			_swDb.Start();
			e.Result = unit;
		}

        private void dbworker_Counter_DoWork(object sender, DoWorkEventArgs e)
        {

          //  if (_qu.Count == 0)  //state == C.State.Idle  && 
            
                MU unit = e.Argument as MU;
                _swDb.Stop();
                unit.LoadData_Counters();
                _swDb.Start();
                e.Result = unit;
           

        }



		private void dbworker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			MU unit = e.Result as MU;
			LoadStep(unit);
		}
        private void dbworker_Counter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			MU unit = e.Result as MU;
			LoadStep_Counters(unit);
		}


        private void LoadStep_Counters(MU unit)
        {
            while ( _dbWorker_Counter.IsBusy)
                Application.DoEvents();

            //Thread.Sleep(1000);

            // при первом проходе надо пропустить
            if (_currUnit_ != null)
                Completed_Counter(_currUnit_);

            _currUnit_ = _qu_.Dequeue();


#if DEBUG
            //if (unit.Order == 1)
            //   throw new Exception("Test exception to examine program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");
#endif
            if (_qu_.Count > 0)
            {
                if (use_threading)
                {
                    _dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
                }
                else
                {
                    _qu_.Peek().LoadData_Counters();
                    LoadStep_Counters(_qu_.Peek());
                }
            }
            else
            {
                // на последнем проходе делает то, что на первом пропустили
                while ( _dbWorker_Counter.IsBusy)
                    Application.DoEvents();
                Completed_Counter(_currUnit_);

                Finish_counters();
            }
        }

		private void LoadStep(MU unit)
		{
            while (_bindWorker.IsBusy || _dbWorker.IsBusy )
				Application.DoEvents();

			//Thread.Sleep(1000);

			// при первом проходе надо пропустить
			if (_currUnit != null)
				Completed(_currUnit);

			_currUnit = _qu.Dequeue();

			if (use_threading) {
				_bindWorker.RunWorkerAsync(unit);
			} else {
				bindworker_DoWork(null, new DoWorkEventArgs(_currUnit));
			}
#if DEBUG
			//if (unit.Order == 1)
			//   throw new Exception("Test exception to examine program behavior. \r\n\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\nПроверять будем долго и упорно.\r\n");
#endif
			if (_qu.Count > 0) {
				if (use_threading) {
					_dbWorker.RunWorkerAsync(_qu.Peek());
				} else {
					_qu.Peek().LoadData();
					LoadStep(_qu.Peek());
				}
			} else {
				// на последнем проходе делает то, что на первом пропустили
                while (_bindWorker.IsBusy)
					Application.DoEvents();
				Completed(_currUnit);

				Finish();
                
			}
		}

		private void bindworker_DoWork(object sender, DoWorkEventArgs e)
		{
			MU unit = e.Argument as MU;
			unit.BindDataset();

			if (!_yearWorker.IsBusy && use_threading)
				_yearWorker.RunWorkerAsync(unit);
			else {
				Debug.WriteLine("Yearworker is busy!!!");
				yearworker_DoWork(null, new DoWorkEventArgs(unit));  
			}

			e.Result = unit;
		}
        private void Completed_Counter(MU unit)
        {
            try
            {
#if DEBUG
                //if (unit.L == 1)
                //   throw new Exception("Exception from 'Completed()'");
#endif
                if (unit.IsEmpty_Counter())
                    tsbMainRefreshFailed.Visible = true;
                _loadingText = string.Format("{0},    {1}", unit.GetLongUnitName(false), Res.splRender);

                 BuildChartMapCounter(unit, true);
             //   if (first >= 0) _chs = _ch_[first];

                // нужно только когда не используется threading
            }
            catch (Exception ex)
            {
                unit.Error = ex.Message;
                MU.Exception = ex;
            }
            finally
            {
              //  _grLines.Redraw = true;
               // _grLines.Refresh();
                _ch_[unit.L].Refresh();
                Application.DoEvents();
            }
            unit.IsBinded = true;
        }
		private void Completed(MU unit)
		{
			try {
#if DEBUG
				//if (unit.L == 1)
				//   throw new Exception("Exception from 'Completed()'");
#endif
				if (unit.IsEmpty())
					tsbMainRefreshFailed.Visible = true;

				_loadingText = string.Format("{0},    {1}", unit.GetLongUnitName(false), Res.splRender);

				unit.CalcTotalRow(_filter);

				// после первой успешной загрузки показываем
				int first = -1;
				if (!tabMain.Visible && unit.Error == "") {
					InitOnce();
					//throw new Exception("Test Exception from line 1756");
					SetGridLinesHeight();
					unit.IsBinded = true;
					if (_grLines.RowSel != unit.L + _grLines.Rows.Fixed)
						_grLines.Select(unit.L + _grLines.Rows.Fixed, 0);
					else
						SetLineSelected(unit.L);

					if (MU.Mins >= MU.minTrendMins) {
						tsbTrendsYears.Checked    = MU.TrendMode == "YearList";
						tsbTrendsQuarters.Checked = MU.TrendMode == "QuarterList";
						tsbTrendsMonths.Checked   = MU.TrendMode == "MonthList";
						tsbTrendsWeek.Checked     = MU.TrendMode == "WeekList";
						tsbTrendsDays.Checked     = MU.TrendMode == "DayList";
						tsbTrendsBrands.Checked   = false;
						tsbTrendsFormats.Checked  = false;
						tsbTrendsShifts.Checked   = false;
					}
					first = unit.L;
				}


				BuildChartMap(unit, true);
           //     BuildChartMapCounter(unit, true);


				if (first >= 0) _chs = _ch[first];

				// нужно только когда не используется threading
			} catch (Exception ex) {
				unit.Error = ex.Message;
				
                MU.Exception = ex;
			} finally {
				_grLines.Redraw = true;
				_grLines.Refresh();
				_ch[unit.L].Refresh();
				Application.DoEvents();
			}
			unit.IsBinded = true;
		}

		private void Finish()
		{
			SetGridLinesHeight();

			_nodes.Sort(new MU.LevelComparer());

			foreach (MU mu in _nodes)
				mu.CalcTotalRowChilds();

			// status bar handling
            toolStripProgressBar1.Visible = false ;
			_tmLoading.Stop();
			_idleCounter = 0;
			MU.CashNeedToUpdate = false;
			sb.Refresh();

			// если не было ни одной успешной загрузки то показываем что есть
			if (!tabMain.Visible) {
				InitOnce();
				if (_grLines.Rows.Count > _grLines.Rows.Fixed)
					_grLines.Select(1, 0);
				else if (_lu.Count > 0 && _lu[0] != null && _lu[0].Error != "") {
					_gbError.Visible = true;
					_txtError.Text = _lu[0].Error;
				}
			} else // была успешная загрузка > известен способ авторизации > делаем доступным меню справочников
				_mnuMasterData.Enabled = true;

			// разрешаем заблокированные кнопки
			tsMain.Enabled = true;
			tsTrends.Enabled = true;
			tsbEdit.Enabled = MU.Mins <= MU.maxEditMins;

			_state = C.State.Idle;
			SetGridLineBoldColumn();

			// если запрос был вызван кнопкой Refresh в режиме редактирования:
			if (_isEdit) {
				SetDataReferences();
				_lu[L].LoadDataMinutes();
				SetEditGrids();
				BuildChartMapMinutes(_chs);
				SetCursDefault();
			}
			//throw new Exception("Test Exception from Finish()");
			_swTotal.Stop();
            //_tm_counter.Enabled = true;
#if DEBUG
			MainMenu.Items["testlabel"].Text = string.Format("Time spent:  {0:0.00} + {1:0.00}",
							(_swTotal.ElapsedMilliseconds - _swDb.ElapsedMilliseconds) / 1000F, _swTotal.ElapsedMilliseconds / 1000F);
#endif
		}
        private void Finish_counters()
        {
          //  SetGridLinesHeight();

            _nodes_.Sort(new MU.LevelComparer());

         //   foreach (MU mu in _nodes_)
         //       mu.CalcTotalRowChilds();

            // status bar handling
            toolStripProgressBar1.Visible = false;
            _tmLoading.Stop();
          //  _idleCounter = 0;
         //   MU.CashNeedToUpdate = false;
            sb.Refresh();

            // если не было ни одной успешной загрузки то показываем что есть
            /*
            if (!tabMain.Visible)
            {
              //  InitOnce();
                if (_grLines.Rows.Count > _grLines.Rows.Fixed)
                    _grLines.Select(1, 0);
                else if (_lu_.Count > 0 && _lu_[0] != null && _lu_[0].Error != "")
                {
                    _gbError.Visible = true;
                    _txtError.Text = _lu_[0].Error;
                }
            }
            */


            create_grid_counters_kpi();
            tabMain.Visible = true;
            // разрешаем заблокированные кнопки
            tsMain.Enabled = true;
            button2.Enabled = true;
            _state = C.State.Idle;

          
            //throw new Exception("Test Exception from Finish()");
            _swTotal.Stop();
#if DEBUG
            MainMenu.Items["testlabel"].Text = string.Format("Time spent:  {0:0.00} + {1:0.00}",
                            (_swTotal.ElapsedMilliseconds - _swDb.ElapsedMilliseconds) / 1000F, _swTotal.ElapsedMilliseconds / 1000F);
#endif
        }
		private void yearworker_DoWork(object sender, DoWorkEventArgs e)
		{
			MU unit = e.Argument as MU;
			if (unit.YearsDS != null && !unit.YearsDS.Tables["FormatName"].Columns.Contains("tQl")) {
#if (DEBUG)
				Stopwatch sw1 = Stopwatch.StartNew();
#endif
				try {
					unit.LoadYearData();
					unit.BindYearDataset();

					DateTime tm1 = DateTime.Now.Date.AddDays(-DateTime.Now.Date.DayOfYear + 1).AddYears(-10);
					for (int n = 0; n < 11; n++)
						unit.GetFilteredKPI("Yr=" + tm1.AddYears(n).Year, unit.YearsDS.Tables["year"]);

					e.Result = unit;
#if (DEBUG)
					Debug.WriteLine("yearworker_DoWork: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
				} catch (Exception ex) {
					//unit.Error = ex.Message;
					MU.Exception = ex;
				}
			}
		}


        private bool IsEmpty_Counter()
        {
           // return L < 0 || _lu_[L].IsEmpty() || (_lu_[L].IsCoupledLine && !_lu_[L].IsBinded);
            return L < 0 || _lu_[L].IsEmpty_Counter();
        }
		private bool IsEmpty()
		{
			return L < 0 || _lu[L].IsEmpty() || (_lu[L].IsCoupledLine && !_lu[L].IsBinded);
		}

		private void SetDataReferences()
		{
			if (IsEmpty()) {
				tscbMainBrands.ComboBox.DataSource  = null;
				tscbMainFormats.ComboBox.DataSource = null;
				tscbMainShifts.ComboBox.DataSource  = null;
				return;
			}

			//dvm = lu[L].DataSet.DefaultViewManager;
			//bsBrandList = new BindingSource(dvm, "BrandName");
			//bsBrandListBrands = new BindingSource(bsBrandList, "BrandListBrands");
			//bsBrandsGaps = new BindingSource(bsBrandListBrands, "rel");

			_tbStopList = _lu[L].DataSet.Tables["StopList"];

			_dvS = _lu[L].DataSet.Tables["Stops"].DefaultView;
			_dvG = _lu[L].DataSet.Tables["Gaps"].DefaultView;

			DataView dvBrand = new DataView(_lu[L].DataSet.Tables["BrandName"]);
			DataView dvFormat = new DataView(_lu[L].DataSet.Tables["FormatName"]);
			DataView dvShift = new DataView(_lu[L].DataSet.Tables["ShiftName"]);
			dvBrand.RowFilter = "chl>0";
			dvFormat.RowFilter = "chl>0";
			dvShift.RowFilter = "chl>0";

			tscbMainBrands.ComboBox.DataSource = dvBrand;
			tscbMainBrands.ComboBox.DisplayMember = "Name";
			tscbMainBrands.ComboBox.ValueMember = "ID";

			tscbMainFormats.ComboBox.DataSource = dvFormat;
			tscbMainFormats.ComboBox.DisplayMember = "Name";
			tscbMainFormats.ComboBox.ValueMember = "ID";

			tscbMainShifts.ComboBox.DataSource = dvShift;
			tscbMainShifts.ComboBox.DisplayMember = "Name";
			tscbMainShifts.ComboBox.ValueMember = "ID";

			_filter = "";
			ResetFiltLabels();
		}

		private void SetLineSelected(int linenumber)
		{
			if (_isEdit && !allow_edit_several_units && CheckToSave()) {
				_grLines.Row = L + _grLines.Rows.Fixed;
				return;
			}
			if (_isEdit && !GetCredentials(_lu[linenumber])) {
				_grLines.Row = L + _grLines.Rows.Fixed;
				return;
			}

			_idleCounter = 0;
			if (_setLineSelectedInUse)
				return;
			else
				_setLineSelectedInUse = true;

			try {
				L = linenumber;
				SetDataReferences();
				// если есть выбранная линия то надо отобразить её название в статусбаре
				if (L >= 0 && _state != C.State.DataLoading && MU.Total.Rows.Count > 0)
					sbpLine.Text = MU.Total.Rows[L]["Line"].ToString();

				if (IsEmpty()) {
					tabMain.Visible = false;
					if (_lu.Count == 0 || L < 0) {
						// не выбрано ни одной линии
						pnLines.Visible = false;
						_gbError.Visible = true;
						_txtError.Text = Res.txtThereisNoLines;
						tssbMainDateLineSelector.Text = Res.btDateLineSelector;
						_pbError.Image = SystemIcons.Information.ToBitmap();
					} else if (_lu[linenumber].Error != "") {
						// выбранная линия имеет ошибки
						_gbError.Visible = true;
						_pbError.Image = SystemIcons.Error.ToBitmap();
						_txtError.Text = _lu[linenumber].Error;
					} else {
						_gbError.Visible = false;
					}
					_setLineSelectedInUse = false;
					return;
				} else {
					// состояние линии нормальное
					tabMain.Visible = true;
					_gbError.Visible = false;
				}

				SetCursWait();

				BuildAnalysis();
				Application.DoEvents();
				LoadGridDetail();
				Application.DoEvents();
				BuildLosses();
				if (MU.Mins > MU.minTrendMins) {
					BuildGridTrends();
					BuildChartTrends();
				}
				if (_isEdit)
					SetEditState(true);
			} catch (Exception ex) {
			   _setLineSelectedInUse = false;
			   SetCursDefault();
			   throw ex;
			} finally {
				_setLineSelectedInUse = false;
				SetCursDefault();
			}
		}

		#region | Detailed tab            |

		private void LoadGridDetail()
		{
			if (IsEmpty()) return;
#if (DEBUG)
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			_grDet.Redraw = false;
			Cursor.Current = Cursors.WaitCursor;
			Application.DoEvents();

			if (tsbDetDwDate.Checked || tsbDetDw.Checked) {
				////////////////////////////// Downtimes //////////////////
				if (_grDet.DataSource != _dvS) {
					_grDet.DataSource = _dvS;
					_grDet.Cols["Day"].Format = "D";
				}
				Utils.SetupColumns(_grDet, _gridColumns["Stops"]);
				LoadGridDetailGroups();
				_grDet.AllowSorting = AllowSortingEnum.None;
				_grDet.ExtendLastCol = true;
			} else if (tsbDetProdExt.Checked || tsbDetProdShort.Checked) {
				//////////////////////// Production ///////////////////////

				if (!_lu[L].DataSet.Tables.Contains("ProductionExtended"))
					AddProductionTables();

				DataView dv = tsbDetProdExt.Checked ?
					_lu[L].DataSet.Tables["ProductionExtended"].DefaultView :
					_lu[L].DataSet.Tables["ProductionShort"].DefaultView;
				if (_grDet.DataSource != dv)
					_grDet.DataSource = dv;

				Utils.SetupColumns(_grDet, _gridColumns["ProductionExtended"]);
				LoadGridDetailGroups();
				_grDet.AllowSorting = AllowSortingEnum.None;
				_grDet.ExtendLastCol = false;
			} else if (tsbDetFromTo.Checked || tsbDetFromToPvt.Checked) {
				//////////////////////// From-To Matrix ///////////////////////

				_lu[L].AddFromToMatrix();
				DataTable dtb = tsbDetFromTo.Checked ? _lu[L].FromToDS.Tables["fmt"] : _lu[L].FromToDS.Tables["pvt"];
				if (_grDet.DataSource != dtb) {
					_grDet.DataSource = dtb.DefaultView;
					foreach (C1.Win.C1FlexGrid.Column col in _grDet.Cols) {
						if (col.Caption.StartsWith("_"))
							col.Visible = false;
						if (col.DataType == typeof(string))
							col.Width = 120;
						if (col.Name.StartsWith("sum"))
							col.Visible = true;
						else if (col.Name.StartsWith("s") && !(bool)dtb.Columns[col.Name].ExtendedProperties["Visible"])
							col.Visible = false;
						if (col.DataType == typeof(int))
							col.Width = 60;
					}
					_grDet.ExtendLastCol = false;
					_grDet.AllowSorting = AllowSortingEnum.SingleColumn;
				}
			}

			if (_grDet.Cols.Contains("dt1")) {
				_grDet.Cols["dt1"].Width = 120;
				_grDet.Cols["dt1"].TextAlign = TextAlignEnum.RightCenter;
				if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName == "ru")
					_grDet.Cols["dt1"].Format = "dd.MM.yyyy HH:mm";
				else
					_grDet.Cols["dt1"].Format = "g";
			}

			SetDetailedRowsCount();
			_grDet.Redraw = true;
			Cursor.Current = Cursors.Default;
#if (DEBUG)
			Debug.WriteLine("LoadGridDetail_: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private void LoadGridDetailGroups()
		{
			if (IsEmpty()) return;
#if (DEBUG)
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			if (tsbDetDwDate.Checked) {
				_dvS.Sort = "dt1";

				string detailed_group_mode = "";
				if (_grDet.Cols.Contains("DayList")) detailed_group_mode = "DayList";
				else if (_grDet.Cols.Contains("WeekList")) detailed_group_mode = "WeekList";
				else if (_grDet.Cols.Contains("MonthList")) detailed_group_mode = "MonthList";
				else if (_grDet.Cols.Contains("QuarterList")) detailed_group_mode = "QuarterList";
				else detailed_group_mode = "YearList";

				if (_grDet.Cols.Contains(detailed_group_mode)) {
					ColumnToFront(_grDet, "RD", 0);
					ColumnToFront(_grDet, detailed_group_mode, 0);
					_grDet.Cols[detailed_group_mode].Width = 15;

					_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Clear);
					_grDet.Tree.Column = 0;

					_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, detailed_group_mode, "TF", "{0}");
					_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, detailed_group_mode, "TD", "{0}");
				}
				foreach (Column col in _grDet.Cols)
					if (col.Name.StartsWith("Mark") && MU.marks.ContainsKey(col.Name))
						col.DataMap = MU.marks[col.Name];
			} else if (tsbDetDw.Checked) {
				_dvS.Sort = "RD, FullName";	//"RD, Stop2, DownID";
				ColumnToFront(_grDet, "FullName", 0);
				ColumnToFront(_grDet, "RD", 0);

				//if (grDet.Cols.Contains("Date")) ColumnToEnd(grDet, "Date", 0);

				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Clear);
				_grDet.Tree.Column = 0;

				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, "RD", "TF");
				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, "RD", "TD");
				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, "FullName", "TF", "{0}");
				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 1, "FullName", "TD", "{0}");

				foreach (Column col in _grDet.Cols)
					if (col.Name.StartsWith("Mark") && MU.marks.ContainsKey(col.Name))
						col.DataMap = MU.marks[col.Name];
			} else if (tsbDetProdExt.Checked || tsbDetProdShort.Checked) {
				DataView dv = _grDet.DataSource as DataView;
				dv.Sort = "Date";
				ColumnToFront(_grDet, "Date", 0);

				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Clear);
				_grDet.Tree.Column = 0;

				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, "Date", "GP", "{0}");
				_grDet.Subtotal(C1.Win.C1FlexGrid.AggregateEnum.Sum, 0, "Date", "NP", "{0}");
			}
		}

		private void AddProductionTables()
		{
			DataTable tbProdExt, tbProdShort;

			#region | create table for extended view  |

			tbProdExt = new DataTable("ProductionExtended");
			tbProdExt.Columns.Add("Brands", typeof(int));
			tbProdExt.Columns.Add("dt1", typeof(DateTime));
			tbProdExt.Columns.Add("Date", typeof(string)); // for grouping in grid
			tbProdExt.Columns.Add("End", typeof(DateTime));
			tbProdExt.Columns.Add("TF", typeof(int));
			tbProdExt.Columns.Add("GP", typeof(int));
			tbProdExt.Columns.Add("NP", typeof(float));
			tbProdExt.Columns.Add("Volume", typeof(float));
			tbProdExt.Columns.Add("Speed", typeof(int), "NP/TF");
			tbProdExt.Columns.Add("Format", typeof(int));
			tbProdExt.Columns.Add("Brand", typeof(int));

			_lu[L].DataSet.Tables.Add(tbProdExt);

			_lu[L].DataSet.Relations.Add("FormatProductionExt", _lu[L].DataSet.Tables["FormatName"].Columns["ID"], tbProdExt.Columns["Format"]);
			tbProdExt.Columns.Add("Vlm", typeof(float), "Parent(FormatProductionExt).Volume");
			tbProdExt.Columns.Add("FormatName", typeof(string), "Parent(FormatProductionExt).EName");

			_lu[L].DataSet.Relations.Add("BrandProductionExt", _lu[L].DataSet.Tables["BrandName"].Columns["ID"], tbProdExt.Columns["Brand"]);
			tbProdExt.Columns.Add("BrandName", typeof(string), "Parent(BrandProductionExt).EName");

			#endregion | create table for extended view  |

			#region | fill extended table             |

			if (_lu[L].DataSet.Tables["Gaps"].Rows.Count > 0) {
				string prevSKU = "", currSKU;
				DateTime prevEnd = _dtEmpty, currStart;
				DataRow dr = tbProdExt.NewRow();
				foreach (DataRow r in _lu[L].DataSet.Tables["Gaps"].Select("GP>0 OR RD is NULL", "dt1")) {
					currSKU = r["Format"].ToString() + r["Brand"].ToString();
					currStart = Convert.ToDateTime(r["dt1"]);
					if (prevSKU == currSKU && prevEnd == currStart) {
						dr["TF"] = Convert.ToInt32(dr["TF"]) + Convert.ToInt32(r["TF"]);
						dr["GP"] = Convert.ToInt32(dr["GP"]) + Convert.ToInt32(r["GP"]);
						dr["NP"] = Convert.ToSingle(dr["NP"]) + Convert.ToSingle(r["NP"]);
					} else {
						dr = tbProdExt.NewRow();
						dr["Brands"] = r["Brands"];
						dr["dt1"] = r["dt1"];
						dr["Date"] = ((DateTime)r["dt1"]).ToString("d");  // for grouping in grid
						dr["TF"] = r["TF"];
						dr["GP"] = r["GP"];
						dr["NP"] = r["NP"];
						dr["Volume"] = r["Vlm"];
						dr["Format"] = r["Format"];
						dr["Brand"] = r["Brand"];
						tbProdExt.Rows.Add(dr);
					}
					prevSKU = currSKU;
					prevEnd = Convert.ToDateTime(r["dt1"]).AddMinutes(Convert.ToInt32(r["TF"]));
					dr["End"] = prevEnd.AddMinutes(-1);
				}
			}

			#endregion | fill extended table             |

			#region | create table for short view     |

			tbProdShort = tbProdExt.DefaultView.ToTable(true, new string[] { "Brands" });
			tbProdShort.TableName = "ProductionShort";
			_lu[L].DataSet.Tables.Add(tbProdShort);
			_lu[L].DataSet.Relations.Add("ShortExtended", tbProdShort.Columns["Brands"], tbProdExt.Columns["Brands"]);

			tbProdShort.Columns.Add("dt1", typeof(DateTime), "MIN(child(ShortExtended).dt1)");
			tbProdShort.Columns.Add("End", typeof(DateTime), "MAX(child(ShortExtended).End)");
			tbProdShort.Columns.Add("Date", typeof(string), "MIN(child(ShortExtended).Date)");
			tbProdShort.Columns.Add("TF", typeof(int), "SUM(child(ShortExtended).TF)");
			tbProdShort.Columns.Add("GP", typeof(int), "SUM(child(ShortExtended).GP)");
			tbProdShort.Columns.Add("NP", typeof(int), "SUM(child(ShortExtended).NP)");
			tbProdShort.Columns.Add("Speed", typeof(float), "GP/TF");
			tbProdShort.Columns.Add("Format", typeof(int), "MAX(child(ShortExtended).Format)");
			tbProdShort.Columns.Add("Brand", typeof(int), "MAX(child(ShortExtended).Format)");
			tbProdShort.Columns.Add("Vlm", typeof(float), "MAX(child(ShortExtended).Vlm)");
			tbProdShort.Columns.Add("Volume", typeof(float), "SUM(child(ShortExtended).Volume)");
			tbProdShort.Columns.Add("FormatName", typeof(string), "MAX(child(ShortExtended).FormatName)");
			tbProdShort.Columns.Add("BrandName", typeof(string), "MAX(child(ShortExtended).BrandName)");

			tbProdExt.DefaultView.Sort = "dt1";
			tbProdExt.DefaultView.ApplyDefaultSort = true;

			tbProdShort.DefaultView.Sort = "dt1";
			tbProdShort.DefaultView.ApplyDefaultSort = true;

			#endregion | create table for short view     |
		}

		#endregion | Detailed tab            |

		#region | Analysis tab            |

		private PointF pf = new Point(0, 0);
		private string accumulated_label = "";

		private void BuildAnalysis()
		{
			string basetime = MU.Kpi.Denominators[_baseKpi];
			if (L < 0 || IsEmpty())
				return;
			//Application.DoEvents();
#if DEBUG
			Stopwatch sw = Stopwatch.StartNew();
#endif
			Dictionary<string, float> kpiflt = _lu[L].GetFilteredKPI(_filter, _lu[L].DataSet.Tables["Gaps"]);
			if (kpiflt == null) {
				_grAnalyse.Clear();
				return;
			}
			CellRange range;
			int cnt = 0, r = 0;
			SetStyles();

			#region | grid         |

			_grAnalyse.Redraw = false;
			_grAnalyse.Clear();
			_grAnalyse.Styles.Normal.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.None;
			_grAnalyse.Rows.Fixed = 1;
			_grAnalyse.Rows.Count = _tbStopList.Rows.Count + 100;
			_grAnalyse.Cols.Count = MU.Kpi.AnalyseColumns.Count + 10;
			_grAnalyse.Cols.Fixed = 0;
			_grAnalyse.AllowMerging = AllowMergingEnum.None; //Spill
			_grAnalyse.AllowEditing = false;
			_grAnalyse.FocusRect = C1.Win.C1FlexGrid.FocusRectEnum.None;
			_grAnalyse.HighLight = HighLightEnum.Never;
			_grAnalyse.ScrollOptions = ScrollFlags.ShowScrollTips;
			//grAnalyse.SelectionMode = SelectionModeEnum.Column | SelectionModeEnum.Row;

			// variables for columns
			int cTr, cSm;
			r = 0;
			_grAnalyse.Tree.Style = TreeStyleFlags.Symbols | TreeStyleFlags.ButtonBar;
			_grAnalyse.Tree.Indent = 15;
			_grAnalyse.Tree.Column = 0;//1;
			cTr = _grAnalyse.Tree.Column;	// tree column
			cSm = cTr + 1;//+ 2;				// sum column

			cnt = cSm; //+ 1;
			_grAnalyse[0, cSm] = Res.strTime;
			foreach (string key in MU.Kpi.AnalyseColumns.Keys) {
				_grAnalyse.Cols[++cnt].Name = key;
				_grAnalyse.Cols[key].Width = 60;
				_grAnalyse.Cols[key].Format = "0.00 %";
				_grAnalyse[0, key] = use_signs_instead_names ? key : MU.Kpi.Names[key];
			}

			_grAnalyse.Cols.Count = cnt + 1;// -1;

			_grAnalyse.Rows[0].Height = _grAnalyse.Rows.DefaultSize * 3;
			_grAnalyse.Styles.Fixed.WordWrap = true;

			_grAnalyse.Cols[cTr].Width = 300;
			_grAnalyse.Cols[cSm].Width = 60;
			//fg2.Cols[cSm].Format = "0";
			_grAnalyse.Cols[cSm].TextAlign = TextAlignEnum.RightCenter;
			_grAnalyse.Cols[cTr].Name = "Tree";
			_grAnalyse.Cols[cSm].Name = "TF";

			float val;
			int col_cnt = 0;

			List<string> keys = new List<string>();
			foreach (string key in MU.Kpi.AnalyseRows.Keys) 
				keys.Add(key);
			string[] keysArr = new string[keys.Count];
			MU.Kpi.AnalyseRows.Keys.CopyTo(keysArr, 0);

			AnalyseTtSubtree(cTr, cSm, ref r, 0, "", keysArr, kpiflt);
			////foreach (string key in MU.Kpi.AnalyseRows.Keys) {
			////   SetGroup(++r, 0);

			////   if (_grAnalyse.Styles.Contains(key) && key != "TR") {
			////      // раскрашиваем колонку со временем
			////      range = _grAnalyse.GetCellRange(r, cSm, r, cSm);
			////      range.Style = _grAnalyse.Styles[key];
			////   } else {
			////      // раскрашиваем светлозеленым строки с ключевыми временами
			////      _grAnalyse.Rows[r].Style = _grAnalyse.Styles["TimeLight"];
			////   }

			////   _grAnalyse.Rows[r].UserData = key;
			////   _grAnalyse[r, cTr] = use_signs_instead_names ? key : MU.Kpi.Names[key];
			////   _grAnalyse[r, cSm] = Utils.GetTimeSpanString((int)kpiflt[key]);
			////   col_cnt = 0;
			////   if (key.StartsWith("t")) {

			////      foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
			////         col_cnt++;
			////         if (!MU.Kpi.DenomPartsDiff[k].Contains(ShortStopCheker(key)))
			////            continue;
			////         if (MU.Kpi.NomParts[MU.Kpi.AnalyseColumns[k]].Contains(ShortStopCheker(key))) {
			////            val = kpiflt[key];
							
			////            // Костыль для Хайнекена 
			////            if (key == "tt7" && MU.Kpi.Expr1[key].Contains("tSh")) {
			////               val -= kpiflt["tSh"];
			////               _grAnalyse[r, cSm] = Utils.GetTimeSpanString((int)val);
			////            }
							
			////            val = -val / kpiflt[MU.Kpi.AnalyseColumns[k]];
			////            if (!float.IsNaN(val))
			////               _grAnalyse[r, k] = val;
			////         }
			////      }
			////      foreach (DataRow dr in _tbStopList.Select("TopID=" + MU.Kpi.ID[key].ToString()))
			////         AddTreeGroup((int)dr["DownID"], ref r, 1, key, _filter, kpiflt);
			////   } else {
			////      col_cnt = 0;
			////      foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
			////         col_cnt++;

			////         if (MU.Kpi.NomParts[key] != MU.Kpi.DenomParts[k] && MU.Kpi.NomParts[key] != MU.Kpi.NomParts[k])
			////            continue;

			////         val = kpiflt[key] / kpiflt[MU.Kpi.AnalyseColumns[k]];
			////         if (!float.IsNaN(val))
			////            _grAnalyse[r, k] = val;
			////      }
			////   }
			////}
			_grAnalyse.Tree.Show(1);
			_grAnalyse.Rows[r].Node.Collapsed = true;

			// Рисуем жирным итоговую строку и добавляем цели
			_grAnalyse[r + 1, 0] = Res.strTarget;
			_grAnalyse[r + 2, 0] = Res.strDifference;
			_grAnalyse.Rows[r + 1].Style = _grAnalyse.Styles["TimeLight"];
			_grAnalyse.Rows[r + 2].Style = _grAnalyse.Styles["TimeLight"];

			cnt = 1;
			double targval, cellval;
			foreach (string key in MU.Kpi.AnalyseColumns.Keys) {
				cnt++;
				if (_lu[L].TgtsYears.ContainsKey(MU.Kpi.ID[key]) &&
						_lu[L].TgtsYears[MU.Kpi.ID[key]].ContainsKey(MU.DtEnd.Year)) {
					// Рисуем цель
					targval = _lu[L].TgtsYears[MU.Kpi.ID[key]][MU.DtEnd.Year] / 100;
					_grAnalyse[r + 1, cnt] = targval;

					// Перерисовываем актуальное значение
					cellval = C.GetFloat(_grAnalyse[r, cnt] ?? 0);
					if (cellval == 0) _grAnalyse[r, cnt] = 0;
					range = _grAnalyse.GetCellRange(r, cnt);
					range.Style = targval < cellval ? _grAnalyse.Styles["TimeBoldGreen"] : _grAnalyse.Styles["TimeBoldRed"];

					// Рисуем разницу
					_grAnalyse[r + 2, cnt] = cellval - targval;
					range = _grAnalyse.GetCellRange(r + 2, cnt);
					range.Style = targval < cellval ? _grAnalyse.Styles["TimeBoldGreen"] : _grAnalyse.Styles["TimeBoldRed"];
				} else {
					_grAnalyse[r + 1, cnt] = "NA";
					//range = grAnalyse.GetCellRange(r, cnt, r+1, cnt);
					//range.Style = grAnalyse.Styles["TimeLight2"];
				}
			}

			//// попытка удалить узлы там, где нет подузлов
			//foreach (C1.Win.C1FlexGrid.Row fr in fg2.Rows)
			//   if (fr.Node != null && fr.Node.Children == 0 && fr.Node.Level > 1 )
			//      fr.IsNode = false ;// = false;

			_grAnalyse.Rows.Count = r + 3;
			_grAnalyse.Redraw = true;

			// create a frozen area at the bottom, with 3 row
			if (ffbAnalysis == null)
				ffbAnalysis = new FlexFreezeBottom(_grAnalyse, 3);

			#endregion | grid         |

			#region | chart        |

			_chAnalyse.ChartGroups[0].ChartData.SeriesList.Clear();
			if (kpiflt["TF"] > 0) {
				LoadAnalysisChart(_chAnalyse, L);
				SetChAnalyseHeight();
			} else
				_chAnalyse.Refresh();

			#endregion | chart        |

#if DEBUG
			Debug.WriteLine(string.Format("LoadAnalysis() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
#endif
		}

		private void AnalyseTtSubtree(int cTr, int cSm, ref int r, int treeLevel, string parentKey, string[] keys, Dictionary<string, float> kpiflt)
		{
			float val;
			CellRange range;

			foreach (string key in keys) {
				SetGroup(++r, treeLevel);

				if (_grAnalyse.Styles.Contains(key) && key != "TR") {
					// раскрашиваем колонку со временем
					range = _grAnalyse.GetCellRange(r, cSm, r, cSm);
					range.Style = _grAnalyse.Styles[key];
				} else {
					// раскрашиваем светлозеленым строки с ключевыми временами
					_grAnalyse.Rows[r].Style = _grAnalyse.Styles["TimeLight"];
				}

				_grAnalyse.Rows[r].UserData = key;
				_grAnalyse[r, cTr] = use_signs_instead_names ? key : MU.Kpi.Names[key];
				_grAnalyse[r, cSm] = Utils.GetTimeSpanString((int)kpiflt[key]);
				
				if (key.StartsWith("t")) {
					foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
						if (!MU.Kpi.DenomPartsDiff[k].Contains(ShortStopCheker(key)))
							continue;
						if (MU.Kpi.NomParts[MU.Kpi.AnalyseColumns[k]].Contains(ShortStopCheker(key))) {
							val = kpiflt[key];

							val = -val / kpiflt[MU.Kpi.AnalyseColumns[k]];
							if (!float.IsNaN(val)) _grAnalyse[r, k] = val;
						}
					}

					if (MU.Kpi.NomParts[key].Contains(";"))
						AnalyseTtSubtree(cTr, cSm, ref r, treeLevel+1, key, MU.Kpi.NomParts[key].Split(new char[] {';'}), kpiflt);

					foreach (DataRow dr in _tbStopList.Select("TopID=" + MU.Kpi.ID[key].ToString()))
						AddTreeGroup((int)dr["DownID"], ref r, 1, treeLevel + 1, key, _filter, kpiflt);
				} else {
					foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
						if (MU.Kpi.NomParts[key] != MU.Kpi.DenomParts[k] && MU.Kpi.NomParts[key] != MU.Kpi.NomParts[k])
							continue;

						val = kpiflt[key] / kpiflt[MU.Kpi.AnalyseColumns[k]];
						if (!float.IsNaN(val)) _grAnalyse[r, k] = val;
					}
				}
			}
		}

		private string ShortStopCheker(string key)
		{
			// Костыль для Хайнекена 
			if (key == "tSp" || key == "tt7" || key == "tSh")
				return "ttS";
			else return key;
		}

		FlexFreezeBottom ffbAnalysis;

		private void SetStyles()
		{
			CellStyle style;

			// создаем свой стиль для каждого типа простоя (начинается с "tt")
			foreach (string key in MU.Kpi.Colors.Keys) {
				style = _grAnalyse.Styles.Add(key);
				style.BackColor = MU.Kpi.Colors[key];
				style.ForeColor = MU.Kpi.Colorsfont[key];
				style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
				style.Border.Color = Color.White;
			}
			// стиль для выделенной колонки
			style = _grAnalyse.Styles.Add("Bold");
			style.Font = _fontBold;

			// стиль для пробелов
			style = _grAnalyse.Styles.Add("Blanc");
			style.BackColor = Color.Transparent;
			style.Border.Color = Color.White;

			style = _grAnalyse.Styles.Add("Grid");
			style.BackColor = Color.Transparent;
			style.Border.Color = _colHalfControl;

			style = _grAnalyse.Styles.Add("Time");
			style.ForeColor = Color.Black;
			style.BackColor = _colRunLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;

			style = _grAnalyse.Styles.Add("TimeLight");
			style.BackColor = _colRunLightLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;
			style.TextAlign = TextAlignEnum.RightCenter;

			style = _grAnalyse.Styles.Add("TimeBold");
			style.ForeColor = Color.Black;
			style.BackColor = _colRunLightLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;
			style.Font = _fontBold;

			style = _grAnalyse.Styles.Add("TimeBoldRed");
			style.ForeColor = Color.Red;
			style.BackColor = _colRunLightLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;
			style.Font = _fontBold;

			style = _grAnalyse.Styles.Add("TimeBoldGreen");
			style.ForeColor = Color.Green;
			style.BackColor = _colRunLightLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;
			style.Font = _fontBold;

			style = _grAnalyse.Styles.Add("TimeLight2");
			style.ForeColor = Color.Black;
			style.BackColor = _colRunLightLight;
			style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
			style.Border.Color = Color.White;

			// стиль для пробелов
			style = _grAnalyse.Styles.Add("BlancRight");
			style.TextAlign = TextAlignEnum.RightCenter;
			style.BackColor = _colHalfControl;
			style.Border.Color = Color.White;

			if (!_grAnalyse.Styles.Contains("tQl")) {
				style = _grAnalyse.Styles.Add("tQl");
				style.BackColor = Color.Orange;
				style.ForeColor = Color.White;
				style.Border.Style = C1.Win.C1FlexGrid.BorderStyleEnum.Flat;
				style.Border.Color = Color.White;
			}
		}

		private void SetGroup(int Row, int lvl)
		{
			_grAnalyse.Rows[Row].IsNode = true;
			_grAnalyse.Rows[Row].Node.Level = lvl;
			////if (lvl == 1)
			////   fg2.Rows[Row].Style = fg2.Styles["TimeLight"];
			_grAnalyse.Rows[Row].Style = _grAnalyse.Styles["Grid"];
		}

		private void AddTreeGroup(int downid, ref int r, int level, int treeLevel, string key, string filter, Dictionary<string, float> kpiflt)
		{
			float down = C.GetFloat(_lu[L].DataSet.Tables["Stops"].Compute("Sum(TD)", Utils.StrJoin(string.Format("D{0}={1}", level, downid), filter)));
			if (down != 0) {
				r++;

				DataRow[] DR = _tbStopList.Select("TopID=" + downid.ToString());
				//if (DR.Length > 0)
				SetGroup(r, treeLevel);
				_grAnalyse.Rows[r].UserData = downid.ToString();

				_grAnalyse[r, "Tree"] = Utils.GetRowField(_tbStopList, "DownID=" + downid.ToString(), "", "Name"); //"FullName");
				_grAnalyse[r, "TF"] = Utils.GetTimeSpanString((int)down);

				CellRange range = _grAnalyse.GetCellRange(r, 1, r, 1);
				if (_grAnalyse.Styles.Contains(key) )
					range.Style = _grAnalyse.Styles[key];

				foreach (string k in MU.Kpi.AnalyseColumns.Keys) {
					if (!MU.Kpi.DenomPartsDiff[k].Contains(ShortStopCheker(key)))
						continue;
					if (MU.Kpi.NomParts[MU.Kpi.AnalyseColumns[k]].Contains(ShortStopCheker(key)))
						_grAnalyse[r, k] = -down / kpiflt[MU.Kpi.AnalyseColumns[k]];
				}

				foreach (DataRow dr in DR)
					AddTreeGroup((int)dr["DownID"], ref r, level + 1, treeLevel + 1, key, filter, kpiflt);
			}
		}

		private void LoadAnalysisChart(C1Chart ch, int line)
		{
			string basetime = MU.Kpi.Denominators[_baseKpi];
			Dictionary<string, float> kpiflt = _lu[line].GetFilteredKPI(_filter, _lu[line].DataSet.Tables["Gaps"]);

			float down;
			ch.ChartGroups[0].ChartData.SeriesList.Clear();
			if (kpiflt["TF"] <= 0)
				return;
			Color col;
			ch.ChartLabels.LabelsCollection.Clear();
			ch.ChartArea.AxisX.ValueLabels.Clear();
			foreach (string key in MU.Kpi.AnalyseRowsReverse.Keys) {
				col = MU.Kpi.Colors.ContainsKey(key) ? MU.Kpi.Colors[key] : _colRunLightLight;
				if (ch == _chPrint) col = ReplaceAlpha(col);
				if (key.StartsWith("tt") && key != "ttS") {
					DataRow[] DR = _tbStopList.Select(string.Format("RD='{0}' and TopID is NULL", key));
					foreach (DataRow dr in DR) {
						string rootdown = dr["RD"].ToString();
						down = C.GetFloat(_lu[line].DataSet.Tables["Gaps"].Compute("Sum(TD)", Utils.StrJoin("RD='" + rootdown + "'", _filter)));
						if (_lu[line].DataSet.Tables["Gaps"].Columns[rootdown].Expression.Contains("+tSh"))
							down += C.GetFloat(_lu[line].DataSet.Tables["Gaps"].Compute("SUM(tSh)", ""));
						AddSeries(ch, MU.Kpi.Names[key], true, down, col, kpiflt[basetime]);
					}
				} else {
					AddSeries(ch, MU.Kpi.Names[key], !MU.Kpi.AnalyseRowsReverse[key], kpiflt[key], col, kpiflt[basetime]);
				}
				if (MU.Kpi.Denominators[_baseKpi] == key)
					break;
			}

			ch.ChartArea.AxisX.Max = pf.X + 0.5;
			ch.ChartArea.AxisX.Min = 0;
			ch.ChartArea.AxisY.Max = kpiflt["TR"] > kpiflt["TF"] ? kpiflt["TR"] * 100 / kpiflt[basetime] : 100.5;

			// требуется только для свободного места слева от графика
			ch.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.None;
			if (!show_analysis_chart_with_steps) {
				//int space = 0;
				//if (kpiflt["K0"] < 10) space = 60;
				//else if (kpiflt["K0"] < 20) space = 30;
				//else if (kpiflt["K0"] < 30) space = 15;
				//ch.Header.Text = new string(' ', space) + ".";
				ch.Header.Text = "Actual Time\r\nComposition      .\r\n\r\n\r\n.";
			} else
				ch.Header.Text = "";
		}

		private void AddSeries(C1Chart ch, string label, bool Stacked, double yIn, Color col, float FT)
		{
			if (show_analysis_chart_with_steps)
				AddSeriesWithStep(ch, label, Stacked, yIn * 100 / FT, col, FT);
			else
				AddSeriesCompact(ch, label, Stacked, yIn * 100 / FT, col, FT);
		}

		private void AddSeriesCompact(C1Chart ch, string label, bool Stacked, double yIn, Color col, float FT)
		{
			// если Stacked == true   -  значит это tt... то есть разноцветные всякие простои
			if (yIn == 0) return;
			C1.Win.C1Chart.Label lab = null;
			string loc_tag = "";

			if (ch.ChartGroups[0].ChartData.SeriesList.Count == 0) {
				pf.X = Stacked ? 1 : 0;
				pf.Y = 0;
			}
			if (!Stacked && pf.Y != 0)
				yIn = 0;

			float YPrev = pf.Y + Convert.ToSingle(yIn);
			C1.Win.C1Chart.ChartDataSeries chdat;
			if (Stacked) {
				if (!accumulated_label.Contains(label))
					accumulated_label += ",  " + label;

				pf.Y = Convert.ToSingle(yIn);
				loc_tag = string.Format("{0};   {1}%\r\n{2}", Utils.GetTimeSpanString(FT * pf.Y / 100), Utils.GetFloatString(pf.Y), label);
			} else {
				lab = ch.ChartLabels.LabelsCollection.AddNewLabel();
				lab.Style.ForeColor = Color.Black;

				lab.AttachMethod = AttachMethodEnum.DataIndex;
				lab.AttachMethodData.GroupIndex = 0;
				lab.AttachMethodData.SeriesIndex = ch.ChartGroups[0].ChartData.SeriesList.Count - 1;
				lab.AttachMethodData.PointIndex = 0;

				lab.Style.Font = _font;
				if (accumulated_label != null && accumulated_label.Length > 2)
					lab.Text = accumulated_label.Substring(1);
				lab.Visible = true;

				pf.X++;
				accumulated_label = "";
				pf.Y = YPrev;
				loc_tag = string.Format("{0};   {1}%\r\n{2}", Utils.GetTimeSpanString(FT * pf.Y / 100), Utils.GetFloatString(pf.Y), label);
			}

			chdat = new C1.Win.C1Chart.ChartDataSeries { Tag = loc_tag };
			chdat.LineStyle.Color = col;
			//chdat.Label = " ";
			chdat.LegendEntry = false;
			ch.ChartGroups[0].ChartData.SeriesList.Add(chdat);
			chdat.PointData.Add(pf);
			pf.Y = YPrev;

			if (!Stacked) {
				lab = ch.ChartLabels.LabelsCollection.AddNewLabel();
				lab.Style.ForeColor = Color.Black;

				lab.Compass = LabelCompassEnum.West;
				//lab.Offset = 5;

				lab.AttachMethod = AttachMethodEnum.DataIndex;
				lab.AttachMethodData.GroupIndex = 0;
				lab.AttachMethodData.SeriesIndex = ch.ChartGroups[0].ChartData.SeriesList.Count - 1;
				lab.AttachMethodData.PointIndex = 0;

				lab.Style.Font = _font;
				lab.Text = label;
				lab.Visible = true;
			}
		}

		private void AddSeriesWithStep(C1Chart ch, string label, bool Stacked, double yIn, Color col, float FT)
		{
			if (yIn == 0) return;
			C1.Win.C1Chart.Label lab = null;
			string loc_tag = "";

			if (ch.ChartGroups[0].ChartData.SeriesList.Count == 0) {
				pf.X = 0;
				pf.Y = 0;
			}
			if (!Stacked && pf.Y != 0)
				yIn = 0;
			float YPrev = pf.Y + Convert.ToSingle(yIn);
			ChartDataSeries chdat;
			pf.X++;
			if (Stacked) {
				chdat = new ChartDataSeries();
				//loc_tag = tag;
				chdat.Display = SeriesDisplayEnum.Hide;
				chdat.LegendEntry = false;
				ch.ChartGroups[0].ChartData.SeriesList.Add(chdat);
				chdat.PointData.Add(pf);

				pf.Y = Convert.ToSingle(yIn);
				loc_tag = string.Format("{0};   {1}%\r\n{2}", Utils.GetTimeSpanString(FT * pf.Y / 100), Utils.GetFloatString(pf.Y), label);
			} else {
				lab = ch.ChartLabels.LabelsCollection.AddNewLabel();
				lab.Style.ForeColor = Color.Black;

				lab.AttachMethod = AttachMethodEnum.DataIndex;
				lab.AttachMethodData.GroupIndex = 0;
				lab.AttachMethodData.SeriesIndex = ch.ChartGroups[0].ChartData.SeriesList.Count;
				lab.AttachMethodData.PointIndex = 0;

				lab.Style.Font = _font;
				lab.Text = label;
				lab.Visible = true;
				//lab.Compass = LabelCompassEnum.West;

				pf.Y = YPrev;

				loc_tag = string.Format("{0};   {1}%\r\n{2}", Utils.GetTimeSpanString(FT * pf.Y / 100), Utils.GetFloatString(pf.Y), label);
			}

			chdat = new ChartDataSeries { Tag = loc_tag };
			chdat.LineStyle.Color = col;
			chdat.Label = " ";
			chdat.LegendEntry = false;
			int s = ch.ChartGroups[0].ChartData.SeriesList.Add(chdat);
			chdat.PointData.Add(pf);
			pf.Y = YPrev;
			if (Stacked) {
				lab = ch.ChartLabels.LabelsCollection.AddNewLabel();
				lab.Compass = LabelCompassEnum.East;
				lab.Offset = 5;
				lab.AttachMethod = AttachMethodEnum.DataIndex;
				lab.AttachMethodData.GroupIndex = 0;
				lab.AttachMethodData.SeriesIndex = s;
				lab.AttachMethodData.PointIndex = 0;
				lab.Style.Font = _font;
				lab.Style.ForeColor = Color.Black;
				lab.Text = label;
				lab.Visible = true;
			}
		}

		private Color ReplaceAlpha(Color c)
		{
			if (c.A != 255) {
				float grade;
				grade = (255 - c.A) / 255f;
				int r = (int)(c.R + (255 - c.R) * grade);
				int g = (int)(c.G + (255 - c.G) * grade);
				int b = (int)(c.B + (255 - c.B) * grade);
				return Color.FromArgb(r, g, b);
			}
			return c;
		}

		#endregion | Analysis tab            |

		#region | Charts Analise handling |

		private void chAnalyse_MouseMove(object sender, MouseEventArgs e)
		{
			//return;
			if (IsEmpty()) return;

			// to reduce CPU workload
			Thread.Sleep(20);
			string tip = "";

			ChartGroup grp = _chAnalyse.ChartGroups[0];
			int s = -1, p = -1, d = -1;
			if (grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 10)
				tip = _chAnalyse.ChartGroups[0].ChartData[s].Tag.ToString();

			Tt.SetToolTip(e.X, e.Y, _chAnalyse, tip);	 //Tt.SetToolTip(chAnalyse, tip);
		}

		private void chAnalyse_DoubleClick(object sender, EventArgs e)
		{
			show_analysis_chart_with_steps = !show_analysis_chart_with_steps;
			BuildAnalysis();
		}

		private void chAnalyse_MouseUp(object sender, MouseEventArgs e)
		{
			//if (e.Button == MouseButtons.Right) chAnalyse_DoubleClick(null, null);
			_chAnalyse.ChartArea.AxisX.Min = 0;
			_chAnalyse.ChartArea.AxisX.Max = pf.X + 0.5;
			_chAnalyse.ChartArea.AxisY.Min = 0;
			_chAnalyse.ChartArea.AxisY.Max = 100.0;
		}

		#endregion | Charts Analise handling |

		#region | Grids handling          |

		private void grLines_BeforeRowColChange(object sender, RangeEventArgs e)
		{
			int line = e.NewRange.r1 - _grLines.Rows.Fixed;
			if (line < 0 || (!_lu[line].IsCompleted && !_lu[line].IsNode)) // || Control.ModifierKeys == Keys.Control
				e.Cancel = true;
		}

		private void grLines_RowColChange(object sender, EventArgs e)
		{
			//MouseEventArgs arg = new MouseEventArgs(MouseButtons.Left, 1, 1, 1, 0);
			//grLines.Rows.Fixed
			int line = _grLines.Row - _grLines.Rows.Fixed;
			if (line >= 0 && line != L && (_lu[line].IsBinded || _lu[line].IsNode))
				SetLineSelected(line);
		}

		private void grLines_DoubleClick(object sender, EventArgs e)
		{
			if (Control.ModifierKeys == Keys.Shift && L >= 0) {
				using (frmInfo f = new frmInfo(_lu[L])) {
					f.ShowDialog();
				}
			}
		}

		private void grDet_OwnerDrawCell(object sender, C1.Win.C1FlexGrid.OwnerDrawCellEventArgs e)
		{
			if (_grDet.DataSource == null || e.Row < _grDet.Rows.Fixed || e.Col < _grDet.Cols.Fixed)
				return;

			// get column
			int col = _grDet.Cols[e.Col].DataIndex;
			string colname;
			colname = col > -1 ? (_grDet.DataSource as DataView).Table.Columns[col].ColumnName : "";
			float val = C.GetFloat(e.Text);
			if (tsbDetFromTo.Checked || tsbDetFromToPvt.Checked) {
				if (colname == "sumPerc") {
					e.Text = Utils.GetFloatString
						(val * 100 / (float)_lu[L].LineRow[MU.Kpi.Denominators[MU.Kpi.MainKpiKey]]);
				} else if (colname.StartsWith("s") || colname.StartsWith("m"))
					e.Text = Utils.GetTimeSpanString(val);
				return;
			}

			if (colname == "TF" || colname == "TD")
				e.Text = Utils.GetTimeSpanString(val);

			// get underlying DataRow
			int row = _grDet.Rows[e.Row].DataIndex;
			if (row < 0) {
				if (_grDet.Rows[e.Row].Node != null & _grDet.Rows[e.Row].Node.Data != null) {
					string n = _grDet.Rows[e.Row].Node.Data.ToString();
					if (_grDet.Styles.Contains("l" + n)) {
						e.Style = _grDet.Styles["l" + n];
						if (_grDet.Cols[e.Col].Name == "RD") e.Text = MU.Kpi.Names[n];
					}
				}
				return;
			}
			CurrencyManager cm = (CurrencyManager)BindingContext[_grDet.DataSource, _grDet.DataMember];
			DataRowView drv = cm.List[row] as DataRowView;

			if (colname == "RD")
				if (_grDet.Styles.Contains(drv["RD"].ToString()))
					e.Style = _grDet.Styles[drv["RD"].ToString()];

			//   if (MonitorUnit.kpi.Colors.ContainsKey(rd)) {
			//      Rectangle rc = e.Bounds;
			//      rc.Inflate(-1, -1);
			//      rc.Offset(-1, -1);
			//      Pen p = new Pen(MonitorUnit.kpi.Brushs[rd]);
			//      e.Graphics.DrawRectangle(p, rc);
			//      //e.Graphics.DrawLine(p, rc.Left, rc.Bottom - 1, rc.Right, rc.Bottom - 1);
			//      p.Dispose();
			//   }
			//}
		}

		private void SetGridLineBoldColumn()
		{
			if (IsEmpty())
				return;
			try {
				if (_grLines.Cols.Contains(MU.Kpi.MainKpiKey)) {
					Column c = _grLines.Cols[MU.Kpi.MainKpiKey];
					if (c.Visible && c.Style != null)
						c.Style.Font = _fontBold;
				}
			} catch { }
		}

		private void SetupGrid(C1FlexGrid gr, bool narrow)
		{
			//try {
			//throw new Exception("Test Exception from SetupGrid");

			DataTable dtb, dtbcols = null;
			dtb = Utils.GetDataTable(gr);
			if (dtb == null) return;

			Column column;
			string colname;
			string tbname = narrow ? "narrow" : "wide";
			bool containes = _dsCols != null && _dsCols.Tables.Contains(tbname);

			if (!containes) {
				if (_dsCols == null) _dsCols = new DataSet("Cols");
				if (!_dsCols.Tables.Contains("narrow")) {
					dtbcols = new DataTable("narrow");
					dtbcols.Columns.Add("Name", typeof(string));
					dtbcols.Columns.Add("Caption", typeof(string));
					//dtbcols.Columns.Add("FullCaption", typeof(string));
					dtbcols.Columns.Add("CustomCaption", typeof(string));
					dtbcols.Columns.Add("Weight", typeof(int));
					dtbcols.Columns.Add("Width", typeof(int));
					dtbcols.Columns.Add("SortOrder", typeof(int));
					dtbcols.Columns.Add("Target", typeof(bool));
					_dsCols.Tables.Add(dtbcols);
					dtbcols.PrimaryKey = new DataColumn[] { dtbcols.Columns["Name"] };
				}
				//if (tbname != "narrow") {
				if (!_dsCols.Tables.Contains("wide")) {
					dtbcols = _dsCols.Tables["narrow"].Clone();
					dtbcols.TableName = "wide";
					_dsCols.Tables.Add(dtbcols);
				}
			}

			dtbcols = _dsCols.Tables[tbname];
			//	-1 - не показывать ни при каких условиях, это служебные колонки
			//	0 - самые основные, только их отображать по умолчанию для grLines
			//	1 - менее важные, их вдобавок к '0' отображать по умолчанию для grTrends
			//	2 - второстепенные колонки, по умолчанию не показывать
			// 100 - главный KPI

			if (narrow && !dtbcols.Columns.Contains("Target"))
				dtbcols.Columns.Add("Target", typeof(bool));

			DataRow dr1;
			DataRow[] DR;
			int width, order, weight, sort = 0;
			bool visible;
			string short_caption, long_caption;
			if (!_dsCols.Tables["narrow"].Columns.Contains("FullCaption")) _dsCols.Tables["narrow"].Columns.Add("FullCaption", typeof(string));
			if (!_dsCols.Tables["wide"].Columns.Contains("FullCaption")) _dsCols.Tables["wide"].Columns.Add("FullCaption", typeof(string));

			List<string> targets = new List<string>();

			foreach (DataColumn dc in dtb.Columns) {
				colname = dc.ColumnName;
				column = gr.Cols[colname];
				DR = dtbcols.Select(String.Format("Name='{0}'", colname));

				if (!containes || DR.Length == 0) {
					// если не нашли сохраненных данных о колонке то создаем заново
					dr1 = dtbcols.NewRow();						//
					dr1["Name"] = colname;						//
					dtbcols.Rows.Add(dr1);						//
					weight = MU.Kpi.ViewWeight.ContainsKey(colname) ? MU.Kpi.ViewWeight[colname] : -1;
					if (colname.Contains("List")) weight = 0;
					if (colname == "Name") weight = 0;

					dr1["Weight"] = weight;

					short_caption = "";
					long_caption = "";
					if (MU.Kpi.Short.ContainsKey(colname))
						short_caption = MU.Kpi.Short[colname];
					else if (MU.Kpi.Names.ContainsKey(colname))
						long_caption = MU.Kpi.Names[long_caption];
					column.Caption = short_caption != "" ? short_caption : long_caption;
					dr1["Caption"] = short_caption;				//
					//dr1["FullCaption"] = long_caption;		//
					dr1["CustomCaption"] = short_caption;		//
					dr1["SortOrder"] = sort;
					sort++;
				} else {
					dr1 = DR[0];
					weight = C.GetInt(dr1["Weight"]);
					width = C.GetInt(dr1["Width"]);				//
					if (width > 0) column.Width = width;		//
					if (MU.Kpi.Short.ContainsKey(colname)) {
						short_caption = MU.Kpi.Short[colname];
						column.Caption = short_caption;
						dr1["Caption"] = short_caption;
					}
					if (narrow && dr1["Target"] != DBNull.Value && (bool)dr1["Target"])
						targets.Add(colname);
					column.Caption = dr1["CustomCaption"].ToString();
					sort++;
				}

				if (MU.Kpi.Names.ContainsKey(colname))
					dr1["FullCaption"] = MU.Kpi.Names[colname];

				visible = narrow ? (weight == 0) : (weight == 0 || weight == 1);
				if (colname == "Name" || colname.Contains("List") ||
					colname.Contains("Line") || colname.Contains("chart")) {
					column.Visible = true;
					if ((int)dr1["Weight"] != 0) dr1["Weight"] = 0;
					dr1["SortOrder"] = 0;
				} else if (!MU.Kpi.ViewWeight.ContainsKey(colname)) {
					dr1["Weight"] = -1;
					column.Visible = false;
				} else
					column.Visible = visible;

				if (MU.Kpi.Measure.ContainsKey(colname)) {
					column.Caption += " ";   // Environment.NewLine;
					switch (MU.Kpi.Measure[colname]) {
						case 1: column.Caption += TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours; break;
						case 2: column.Caption += "%"; break;  //column.Format = "0.0"; 
						case 3: column.Caption += Res.strUnits; column.Format = "0"; break;
						case 4: column.Caption += Res.strMeasure; column.Format = "0.0"; break;
					}
				}
			}

			dtbcols.AcceptChanges();

			if (containes) {
				string id;
				foreach (DataRow r in dtbcols.Select("SortOrder is not null", "SortOrder")) {
					id = r["Name"].ToString();
					order = (int)r["SortOrder"];
					if (dtb.Columns.Contains(id) &&
							gr.Cols.Contains(id) &&
							gr.Cols[id].Visible &&
							order < gr.Cols.Count)
						gr.Cols[id].Move(order);
				}
			}

			foreach (string col in targets) {
				string targetcol = "t_" + col;
				if (gr.Cols.Contains(targetcol)) {
					gr.Cols[targetcol].Visible = true;
					gr.Cols[targetcol].Caption = gr.Cols[col].Caption + " Target";
					gr.Cols[targetcol].Move(gr.Cols[col].SafeIndex + 1);
				}
			}

			//} catch { }
		}

		private void SetupColumns(C1FlexGrid gr)
		{
			DataTable dtb, dtbcols;
			dtb = Utils.GetDataTable(gr);
			if (dtb == null) return;
			string colname = gr == _grLines ? "narrow" : "wide";

			dtbcols = _dsCols.Tables[colname];
			DataTable dtbcopy = dtbcols.Copy();
			frmColumns frm = new frmColumns(dtbcopy, dtb);
			if (frm.ShowDialog() == DialogResult.OK) {
				dtbcols.Merge(dtbcopy);
				dtbcols.AcceptChanges();
				SetupGrid(gr, gr == _grLines);
			}
		}

		private void RefreshGrids()
		{
			if (_gridColumns.ContainsKey("Stops") && _gridColumns["Stops"].ContainsKey("TF")) {
				_gridColumns["Stops"]["TF"].Name = Res.SelMins + ", " + (TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours);
				_gridColumns["Stops"]["TD"].Name = Res.RealMins + ", " + (TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours);
			}
			_grLines.Refresh();
			_grDet.Refresh();

			// Простой рефреш грида не поможет, перезагружаем вместе с графиками
			BuildAnalysis();
			LoadGridDetail();
			BuildLosses();
			if (MU.Mins > MU.minTrendMins) {
				BuildGridTrends();
				BuildChartTrends();
			}
		}

		private void _grAnalyse_MouseMove(object sender, MouseEventArgs e)
		{
			string tip;
			C1FlexGrid gr = sender as C1FlexGrid;

			// просто использовать gr.MouseCol нельзя,
			// т.к. иногда успевает смениться внутри процедуры!!!
			int col = gr.MouseCol;
			if (col < 0 || gr.MouseRow > 0) {
				Tt.SetToolTip();
				return;
			}

			tip = (string)gr.Cols[col].UserData;
			string colname = gr.Cols[col].Name;

			if (MU.Kpi != null && MU.Kpi.Names.ContainsKey(colname)) {
				tip = MU.Kpi.Names[colname];
				if (MU.Kpi.Measure.ContainsKey(colname) && (MU.Kpi.Measure[colname] == 2))
					tip += ", %";

				if (MU.Kpi.Denominators.ContainsKey(colname)) 
					if (MU.Kpi.Nominators.ContainsKey(colname))
						tip += String.Format("{0} = {1} / {2}", Environment.NewLine, MU.Kpi.Names[MU.Kpi.Nominators[colname]], MU.Kpi.Names[MU.Kpi.Denominators[colname]]);
				
				Tt.SetToolTip(e.X, e.Y, gr, tip);
			} else
				Tt.SetToolTip();
		}

		#endregion | Grids handling          |

		#region | Menus and buttons       |

		private void mnuClick(object sender, EventArgs e)
		{
			ToolStripMenuItem mi = (ToolStripMenuItem)sender;
			switch (mi.Name) {
				case "mnuExcel2":
					//Excel();
                    if (!use_easy_counter)
                    {
                        if (_state != C.State.Idle)
                            return;
                        List<string> cols = new List<string>();
                        foreach (Column col in _grLines.Cols)
                            if (col.Visible)
                                cols.Add(col.Name);
                        ExcelReport er = new ExcelReport(_lu, cols, _filter);
                    }
                    else
                    {
                        if (_state != C.State.Idle)
                            return;
                        List<string> cols = new List<string>();
                        foreach (Column col in c1FlexGrid1.Cols)
                            if (col.Visible)
                                cols.Add(col.Name);
                        ExcelReport er = new ExcelReport(_lu_, cols, "");
                    }
					break;
				case "mnuAutoUpdate":
					_monitorSettings.AutoUpdate = _mnuAutoUpdate.Checked;
					if (_monitorSettings.AutoUpdate)
						_tmActivity.Start();
					else
						_tmActivity.Stop();
					break;
				case "mnuShowLegend":
					_monitorSettings.ColorLegend = _mnuShowLegend.Checked;
					pnLegend.Visible = _mnuShowLegend.Checked ? true : false;
					break;
				case "mnuAtStartup":
					_monitorSettings.EmptyAtStart = _mnuAtStartup.Checked;
					break;
				case "mnuUseTimeCorrection":
					MU.UseTimecorrection = _mnuUseTimeCorrection.Checked;
					_monitorSettings.TimeCorrection = _mnuUseTimeCorrection.Checked;
					break;
				case "mnuSetMMM":
					_mnuSetHHxMM.Checked = false;
					_mnuSetHHxHH.Checked = false;
					TmStyle = TimeStyle.MMM;
					RefreshGrids();
					break;
				case "mnuSetHHxMM":
					_mnuSetMMM.Checked = false;
					_mnuSetHHxHH.Checked = false;
					TmStyle = TimeStyle.HHxMM;
					RefreshGrids();
					break;
				case "mnuSetHHxHH":
					_mnuSetMMM.Checked = false;
					_mnuSetHHxMM.Checked = false;
					TmStyle = TimeStyle.HHxHH;
					RefreshGrids();
					break;
				case "mnuComparePerc":
					_mnuTrendPerc.Checked = true;
					_mnuTrendMins.Checked = false;
					_monitorSettings.Percents = true;
					//LoadChartCompare(chCompare);
					chTrends_MouseDown(null, null);
					break;
				case "mnuCompareMins":
					_mnuTrendPerc.Checked = false;
					_mnuTrendMins.Checked = true;
					_monitorSettings.Percents = false;
					//LoadChartCompare(chCompare);
					chTrends_MouseDown(null, null);
					break;
				case "mnuCompareReport":
					////PrintDocument pd = new PrintDocument();
					////pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
					////pd.DefaultPageSettings.Landscape = true;
					////pd.DefaultPageSettings.Margins.Left = 50;
					////pd.DefaultPageSettings.Margins.Top = 100;
					////pd.DefaultPageSettings.Margins.Right = 50;
					////pd.DefaultPageSettings.Margins.Bottom = 50;
					////PrintPreviewDialog pp = new PrintPreviewDialog();
					////pp.Document = pd;
					////pp.ShowDialog();
					break;
				case "mnuContent":
					Help.ShowHelp(this, "ProductionMonitor.chm");
					break;
				case "mnuAbout":
					//////formSplash.ShowDescription();
					string cpr = "";
					Splash.ShowSplash(300, C.GetAssemblyShortInfo(Assembly.GetExecutingAssembly(), ref cpr), cpr);
					break;
				case "mnuCashRefr":
					foreach (MU u in _lu)
						u.CashValidDate = DateTime.Now;

					//cash_valid_date = DateTime.Now;
					MU.CashNeedToUpdate = true;
					if (!_isEdit)
						LoadStart();
					break;
				case "mnuExit":
					Close();
					break;
				case "mnuCopy":
					CopyDatatableToClipboard(_grDet);
					break;
				case "mnuCopyCompare":
					CopyDatatableToClipboard(_grTrends);
					break;
				case "mnuCopyLines":
					CopyDatatableToClipboard(_grLines);
					break;
				case "mnuSetCompareCols":
					SetupColumns(_grTrends);
					break;
				case "mnuSetLinesCols":
					SetupColumns(_grLines);
					break;
				case "mnuReset":
					ResetChartMap();
					ch0_ResetScale(_chs , true);
					break;
				case "mnuMasterData":
					frmMaster masterdata;
					masterdata = new frmMaster(MU.Config);
					masterdata.ShowDialog();
					if (masterdata.NeedToUpdateLists) {
						MU.ListsCash.Clear();
						foreach (MU mu in MU.Config.MonitorUnits.Values)
							mu.CashValidDate = _dtEmpty;
						pnLines.Visible = false;
						_gbError.Visible = false;
						tabMain.Visible = false;
						MU.Total.Clear();
					}
					break;
				case "mnuApply":
					AddNewEntry();
					break;
				//case "mnuChange":
				//   ChangeEntry(dt_empty);
				//   break;
				case "mnuDelProd":
					DeleteProduction();
					break;
				case "mnuShowMins":
					if (_lu != null && _chs != null && _lu[_chs.L].DataSet.Tables.Contains("Minutes")) {
						try {
							frmMins frm = new frmMins(_lu[_chs.L].DataSet.Tables["Minutes"]);
							frm.ShowDialog();
						} catch (Exception ex) {
							MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
					}
					break;
				case "mnuAutoShift":
					AddShiftByTemplate();
					break;
			}
		}

		private void btDateLineSelector_DropDownOpening(object sender, EventArgs e)
		{
			try {
				_mnuSelect.Select();
				_mnuSelect.ShowWindow(MU.DtStart, MU.DtEnd);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, this.Text + " btDateLineSelector_DropDownOpening", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}
		}

		private void btDateLineSelector_DropDownOpened(object sender, EventArgs e)
		{
			_mnuSelect._btOK.Focus();
		}

		private void btRefresh_Click(Object sender, EventArgs e)
		{
			if (!_isEdit)
				LoadStart();
			else {
				ReloadDataInEditMode();
			}
		}

		private bool loadfailed = false;

		private void btRefreshFailed_Click(Object sender, EventArgs e)
		{
			LoadFailedStart();
		}

		private void btHideLegend_Click(Object sender, EventArgs e)
		{
			_mnuShowLegend.Checked = false;
			mnuClick(_mnuShowLegend, null);
		}

		private void btPrev_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(MU.DtStart);
			LoadStart();
		}

		private void btNext_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(MU.DtEnd.AddMinutes(MU.Mins));
			LoadStart();
		}

		private void btNow_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(1, DateTime.Now.Date.AddDays(1));
			LoadStart();
		}

		private void txtError_DoubleClick(object sender, EventArgs e)
		{
			if (MU.Exception != null)
				MessageBox.Show(MU.Exception.ToString());
		}

		private void tabs_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (tabMain.SelectedTab == tpTrends && tsbMainFiltReset.Visible)
				btHide_Click(null, null);

			tsbMainHideFilters.Enabled = tabMain.SelectedTab != tpTrends;

			SetDetailedRowsCount();
		}

		private void splAnalyse_Click(object sender, EventArgs e)
		{
			_pnAnalyseChart.Visible = !_pnAnalyseChart.Visible;
			if (_pnAnalyseChart.Visible)
				_splAnalyse.Image = Res.s_hide;
			else
				_splAnalyse.Image = Res.s_show;
		}

		private void cmMap_MouseLeave(object sender, EventArgs e)
		{
			//cmMap.Hide();
		}

		#endregion | Menus and buttons       |

		#region | Reports PDF             |

		private bool nowDrawReport = false;

		private class ReportPageItem
		{
			public Image Chart;
			public Image Chart2;
			public Image Grid;
			public string Header;
			public bool Landscape;
			public string TabPage;
		}

		private Dictionary<string, ReportPageItem> reps = new Dictionary<string, ReportPageItem>();

		private void tsbTrendsAddToPrint_Click(object sender, EventArgs e)
		{
			SetCursWait();
			nowDrawReport = true;
			string key = _chTrends.Header.Text + MU.TrendMode;
			if (!reps.ContainsKey(key)) {
				//reports.Add(chCompare.Header.Text, chCompare.GetImage(ImageFormat.Emf));

				// Обходим глюк для С1.PDF который неправильно рисует лейблы
				foreach (C1.Win.C1Chart.Label lab in _chTrends.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.White;
				_chTrends.Header.Style.Border.Color = Color.White;

				ReportPageItem rep = new ReportPageItem();
				reps.Add(key, rep);
				rep.TabPage = "tpTrends";
				rep.Landscape = _grTrends.Rows.Count < 30;

				Image im;
				if (rep.Landscape)
					im = _chTrends.GetImage(ImageFormat.Emf, new Size(1200, 400));
				else
					im = _chTrends.GetImage(ImageFormat.Emf, new Size(1000, 500));

				// Возвращаем назад
				foreach (C1.Win.C1Chart.Label lab in _chTrends.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.Black;
				_chTrends.Header.Style.Border.Color = Color.Black;

				rep.Chart = im;
				rep.Grid = _grTrends.CreateImage();
				rep.Header = _lu[L].GetLongUnitName(true) + ",      " + tssbMainDateLineSelector.Text;
			}
			nowDrawReport = false;
			SetCursDefault();
		}

		private void tsbTrendsPrint_Click(object sender, EventArgs e)
		{
			tsbTrendsAddToPrint_Click(null, null);
			if (!use_pdf) {
				PrintDocument pd = new PrintDocument();
				pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
				pd.DefaultPageSettings.Landscape = true;
				pd.DefaultPageSettings.Margins.Left = 50;
				pd.DefaultPageSettings.Margins.Top = 100;
				pd.DefaultPageSettings.Margins.Right = 50;
				pd.DefaultPageSettings.Margins.Bottom = 50;
				PrintPreviewDialog pp = new PrintPreviewDialog();
				pp.Document = pd;
				pp.ShowDialog();
			} else {
				PdfPrint();
			}
		}

		private void tsbLossesAddToPrint_Click(object sender, EventArgs e)
		{
			LossesAddToPrint();
		}

		private void tsbLossesPrint_Click(object sender, EventArgs e)
		{
			LossesAddToPrint();
			PdfPrint();
		}

		private void LossesAddToPrint()
		{
			SetCursWait();
			nowDrawReport = true;
			string key = _chLosses.Header.Text;
			if (!reps.ContainsKey(key)) {
				// Обходим глюк в С1Pdf который неправильно рисует лейблы
				foreach (C1.Win.C1Chart.Label lab in _chLosses.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.White;
				foreach (C1.Win.C1Chart.Label lab in _chMarks.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.White;
				_chLosses.Header.Style.Border.Color = Color.White;

				ReportPageItem rep = new ReportPageItem();
				reps.Add(key, rep);
				rep.TabPage = "tpLosses";
				rep.Landscape = false;

				// Для pdf меняем расположение легенды, иначе график узкий получается
				_chLosses.Legend.Compass = CompassEnum.North;
				rep.Chart = _chLosses.GetImage(ImageFormat.Emf, new Size(700, 500));
				// Восстанавливаем расположение легенды
				_chLosses.Legend.Compass = CompassEnum.East;
				if (_chMarks.ChartGroups[0].ChartData.SeriesList.Count > 0)
					rep.Chart2 = _chMarks.GetImage(ImageFormat.Emf, new Size(700, 300));
				rep.Grid = _grLosses.CreateImage();
				rep.Header = _lu[L].GetLongUnitName(true) + ",      " + tssbMainDateLineSelector.Text;

				// Возвращаем назад (Обходили глюк в С1Pdf)
				foreach (C1.Win.C1Chart.Label lab in _chLosses.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.Black;
				foreach (C1.Win.C1Chart.Label lab in _chMarks.ChartLabels.LabelsCollection)
					lab.Style.Border.Color = Color.Black;
				_chLosses.Header.Style.Border.Color = Color.Black;
			}
			nowDrawReport = false;
			SetCursDefault();
		}

		private void AnalyseAddToPrint()
		{
			SetCursWait();
			nowDrawReport = true;
			string key = _lu[L].ID.ToString() + "_Analyse";
			if (!reps.ContainsKey(key)) {
				////// Обходим глюк в С1Pdf который неправильно рисует лейблы
				////foreach (C1.Win.C1Chart.Label lab in chLosses.ChartLabels.LabelsCollection)
				////   lab.Style.Border.Color = Color.White;
				////foreach (C1.Win.C1Chart.Label lab in chMarks.ChartLabels.LabelsCollection)
				////   lab.Style.Border.Color = Color.White;
				////chLosses.Header.Style.Border.Color = Color.White;

				ReportPageItem rep = new ReportPageItem();
				reps.Add(key, rep);
				rep.TabPage = "tpAnalyse";
				rep.Landscape = false;

				rep.Chart = _chAnalyse.GetImage(ImageFormat.Emf, new Size(_chAnalyse.Width, _chAnalyse.Height));
				rep.Grid = _grAnalyse.CreateImage();
				rep.Header = _lu[L].GetLongUnitName(true) + ",      " + tssbMainDateLineSelector.Text;

				////// Возвращаем назад (Обходили глюк в С1Pdf)
				////foreach (C1.Win.C1Chart.Label lab in chLosses.ChartLabels.LabelsCollection)
				////   lab.Style.Border.Color = Color.Black;
				////foreach (C1.Win.C1Chart.Label lab in chMarks.ChartLabels.LabelsCollection)
				////   lab.Style.Border.Color = Color.Black;
				////chLosses.Header.Style.Border.Color = Color.Black;
			}
			nowDrawReport = false;
			SetCursDefault();
		}

		private void PdfPrint()
		{
			////Image im;
			SetCursWait();
			//AnalyseAddToPrint();

			// настраиваем файл
			if (_pdf == null) {
				this._pdf = new C1.C1Pdf.C1PdfDocument();
				_pdf.PaperKind = PaperKind.A4;
				_pdf.ImageQuality = C1.C1Pdf.ImageQualityEnum.High;
				_pdf.DocumentInfo.Author = Environment.UserName;
				_pdf.DocumentInfo.Creator = Application.ProductName;
			} else
				_pdf.Clear();

			RectangleF rc;
			RectangleF rcChart;
			RectangleF rcHeader;
			bool first = true;
			foreach (ReportPageItem rep in reps.Values) {
				if (!first) _pdf.NewPage();
				else first = false;

				_pdf.Landscape = rep.Landscape;

				rc = _pdf.PageRectangle;
				rc.Height = rc.Height / 2;
				rc.Inflate(-10, -18); // << 1" margin
				rc.Offset(0, 18);

				rcChart = rc;

				// рисуем шапку
				rcHeader = rc;
				rcHeader.Offset(36, -10);
				_pdf.DrawString(rep.Header, this.Font, Brushes.Black, rcHeader);

				if (rep.TabPage == "tpAnalyse") {
					rcChart.Height *= 1.5f;
					rcChart.Inflate(-18, 0);
					_pdf.DrawImage(rep.Grid, rcChart, ContentAlignment.TopCenter, C1.C1Pdf.ImageSizeModeEnum.Scale);

					rcChart.Offset(0, rcChart.Height + 10);
					rcChart.Height /= 3;
					_pdf.DrawImage(rep.Chart, rcChart, ContentAlignment.TopCenter, C1.C1Pdf.ImageSizeModeEnum.Scale);
					continue;
				}

				// рисуем график
				_pdf.DrawImage(rep.Chart, rcChart, ContentAlignment.MiddleCenter, C1.C1Pdf.ImageSizeModeEnum.Scale);

				// рисуем второй график если есть
				if (rep.Chart2 != null) {
					rcChart.Offset(0, rcChart.Height);
					rcChart.Height = rcChart.Height / 2;
					_pdf.DrawImage(rep.Chart2, rcChart, ContentAlignment.TopCenter, C1.C1Pdf.ImageSizeModeEnum.Scale);
				}

				// рисуем таблицу
				rcChart.Offset(0, rcChart.Height);
				rcChart.Inflate(-36, 0);
				if (rep.Grid != null)
					_pdf.DrawImage(rep.Grid, rcChart, ContentAlignment.TopCenter, C1.C1Pdf.ImageSizeModeEnum.Scale);

				// рисуем футер
				rcChart.Offset(0, rcChart.Height);
				rcChart.Height = 10;
				_pdf.DrawString("Perfomed by " + Environment.UserName + " at " + DateTime.Now.ToString(), this.Font, Brushes.Black, rcChart);
			}

			SetCursDefault();
			try {
				using (SaveFileDialog sd = new SaveFileDialog()) {
					//sd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					sd.FileName = "OPI_" + _lu[L].LineName.Replace("/", "_") + ".pdf";
					sd.Filter = "Acrobat Reader|*.pdf";
					if (sd.ShowDialog() == DialogResult.OK && sd.FileName != "") {
						reps.Clear();
						_pdf.Save(sd.FileName);
						System.Diagnostics.Process.Start(sd.FileName);
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}

		private void pd_PrintPage(object sender, PrintPageEventArgs e)
		{
			//////////Rectangle r = e.MarginBounds;
			//////////r.Height = r.Width * chCompare.Height / chCompare.Width;
			//////////e.Graphics.DrawImage(chCompare.GetImage(ImageFormat.Emf), r);

			////Rectangle r = e.MarginBounds;
			////Image im;
			////string repname = "";
			////foreach (string rep in rep_charts.Keys) {
			////   repname = rep;
			////   im = rep_charts[rep];
			////   r.Height = r.Width * im.Height / im.Width;
			////   e.Graphics.DrawImage(im, r);
			////   break;
			////}

			////rep_charts.Remove(repname);
			////e.HasMorePages = rep_charts.Count != 0;
		}

		#endregion | Reports PDF             |

		#region | CultureInfo settings    |

		private void InitLang()
		{
#if (DEBUG)
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			// Этот язык добавляем ручками т.к. он по умолчанию
			AddLang("en-GB");

			string[] list = Directory.GetDirectories(Application.StartupPath);
			string subflder;

			// Все остальные языки определяем по имеющимся названиям папок с локализациями
			foreach (string str in list) {
				subflder = str.Remove(0, Directory.GetCurrentDirectory().Length + 1);
				try {
					string language = CultureInfo.GetCultureInfo(subflder).NativeName;
					AddLang(subflder);
				} catch { }
			}

			//AddLang("ru-RU");
			//AddLang("de-DE");
			//AddLang("hr-HR");
			//AddLang("bg-BG");
			//AddLang("cs-CZ");
			//AddLang("hu-HU");
			//AddLang("ro-RO");
			//AddLang("ko-KR");
#if (DEBUG)
			Debug.WriteLine(" | | | InitLang: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		private void AddLang(string strLang)
		{
			//CultureInfo ci = new CultureInfo(strLang);
			CultureInfo ci = CultureInfo.GetCultureInfo(strLang);
			ToolStripMenuItem mnu = new ToolStripMenuItem(ci.Parent.NativeName.ToLower());
			mnu.Tag = strLang;
            if (strLang == "en-GB")
            {
                mnu.Image = Res.eng;
            }
            if (strLang == "ru-RU")   
            {
                mnu.Image = Res.rus;
            }

			mnu.Click += new EventHandler(mnuLang_Click);
            _mnuLang.DropDownItems.Add(mnu);  //Res.exit, eh, "mnuExit"
			if (Thread.CurrentThread.CurrentUICulture.ToString() == strLang)
				mnu.Checked = true;
		}

		private void mnuLang_Click(object sender, System.EventArgs e)
		{
			ToolStripMenuItem mnu = (ToolStripMenuItem)sender;
			foreach (ToolStripMenuItem m in _mnuLang.DropDownItems)
				m.Checked = false;
			mnu.Checked = true;
			ChangeLang(mnu.Tag.ToString());
		}

		private void ChangeLang(string l)
		{
			//CultureInfo ci = new CultureInfo(l, false);
			CultureInfo ci = CultureInfo.GetCultureInfo(l);
			Thread.CurrentThread.CurrentCulture = ci;
			Thread.CurrentThread.CurrentUICulture = ci;
			MU.UseDefaultLanguage = ci.Name == "en-GB";
			_monitorSettings.Culture = ci.ToString();
			Res.Culture = ci;

			tssbMainDateLineSelector.Text = Res.btDateLineSelector;
			//btDateLineSelector.ToolTipText = Res.lbDateLineTT;
			tssbMainDateLineSelector.ToolTipText = Res.btChangeLineDateTT;
			tsbMainRefresh.ToolTipText = Res.btRefreshTT;
			tsbMainPrev.ToolTipText = Res.btPrevDayTT;
			tsbMainNext.ToolTipText = Res.btNextDayTT;
			tsbMainNow.ToolTipText = Res.btNowTT;
			tsbEdit.ToolTipText = Res.btEditTT;

			tsbMainFiltReset.Text = Res.btFiltReset;
			tsbMainFiltReset.ToolTipText = Res.btFiltResetTT;
			tsbMainHideFilters.ToolTipText = Res.btHideTT;

			InitMenu();

			tpDetail.Text   = Res.tpDetail;
            if (!use_easy_counter)
			 tpMap.Text      = Res.tpMap;
			tpAnalyse.Text  = Res.tpAnalyse;
			tpTrends.Text   = Res.tpCompare;
			tpLosses.Text   = Res.tpLosses;
            tpCounters.Text = Res.tpCounters;

			_chbShowTips.Text      = Res.chShowTips;
			_chbShowBigMap.Text    = Res.chbShowBigMap;
            tsbBigChartCounter.ToolTipText =   tsbBigChartCounter.Text = cbBigChartCounter.Text = Res.chbShowBigMap;
            tsbShowTipsCounter.ToolTipText =   tsbShowTipsCounter.Text = cbShowTipsCounter.Text = Res.chShowTips;
            tsbShowLimitsCounter.ToolTipText = tsbShowLimitsCounter.Text = tsbShowLimitsCounter.Text = Res.tsbShowLimitsCounter;

			using (Graphics gr = _chbShowTips.CreateGraphics()) {
				_dpix = gr.DpiX;
				_chbShowTips.Location = new Point(tpMap.Width - gr.MeasureString(Res.chShowTips, _chbShowTips.Font).ToSize().Width - 35, 2);
				_chbShowBigMap.Location = new Point(_chbShowTips.Location.X - 150, 2);
			}

			tpEditDnTms.Text = Res.strDownTimes;
			tpEditProds.Text = Res.strProduction;
			tpEditShifts.Text = Res.strShifts;

			tsbDetDwDate.Text = Res.tsbDetDwDate;
			tsbDetDw.Text = Res.tsbDetDw;
			tsbDetProdExt.Text = Res.tsbDetProdExt;
			tsbDetProdShort.Text = Res.tsbDetProdShort;
			tsbDetFromTo.Text = Res.tsbDetFromTo;
			tsbDetFromToPvt.Text = Res.tsbDetFromToPvt;

			tsbTrendsDays.Text = Res.strDays;
			tsbTrendsWeek.Text = Res.strWeeks;
			tsbTrendsMonths.Text = Res.strMonths;
			tsbTrendsQuarters.Text = Res.strQuarters;
			tsbTrendsYears.Text = Res.strYears;
			tsbTrendsBrands.Text = Res.strBrands;
			tsbTrendsFormats.Text = Res.strFormats;
			tsbTrendsShifts.Text = Res.strShifts;
			tsbTrendsPrint.Text = Res.tsbShowReport;
			tsbTrendsAddToPrint.Text = Res.tsbAddToReport;

			tsbLossesPrint.Text = Res.tsbShowReport;
			tsbLossesAddToPrint.Text = Res.tsbAddToReport;

			// in dropdown selector
			_mnuSelect._btCancel.Text = Res.btCancel;
			_mnuSelect._btOK.Text = Res.btApply;
			_mnuSelect._rbDays.Text = Res.strDays;
			_mnuSelect._rbWeeks.Text = Res.strWeeks;
			_mnuSelect._rbMonths.Text = Res.strMonths;
			_mnuSelect._rbTime.Text = Res.strAnyTime;

			if (MU.Kpi != null) MU.Kpi.ChangeLanguage(MU.UseDefaultLanguage);

			if (_state != C.State.Idle) return;

			int nn = _lu == null || _lu.Count < 1 ? 0 : _lu.Count;
			if ( nn != 0 )  SetCursWait();
			for (int n = 0; n < nn; n++)
				_lu[n].SetLocalization();

			#region | MonitorUnit.marks    |

            if ( nn != 0 && _lu[0].DataSet != null)
            {
				Dictionary<int, string> mark;
				string id;
				foreach (string key in MU.marks.Keys) {
					mark = MU.marks[key];
					mark.Clear();
					id = "ID=" + key.Substring(4);
					MU.marks_captions[key] = C.GetRowField(_lu[0].DataSet.Tables["MarkBandList"], id, "Name");
					foreach (DataRow dr in _lu[0].DataSet.Tables["MarkList"].Select("BandID=" + key.Substring(4))) {
						MU.GetColorMarks(dr);
						mark[(int)dr["val"]] = dr["Name"].ToString();
					}
				}
				_grDet.Refresh();
			}

			#endregion | MonitorUnit.marks    |

            if (nn != 0)
            {
                // grids
                InitGridsColumns();
                GridLang();
                InitLegend();

                // Перерисовываем все закладки
                BuildAnalysis();
                LoadGridDetail();
                BuildLosses();
                if (MU.Mins > MU.minTrendMins)
                {
                    BuildGridTrends();
                    BuildChartTrends();
                }
            }
		}

		private void GridLang()
		{
			if (_dvS == null)
				return;
			Dictionary<string, GridColumn> columns = _gridColumns["Stops"];
			foreach (string col in columns.Keys)
				if (_grDet.Cols.Contains(col))
					_grDet.Cols[col].Caption = columns[col].Name;
		}

		#endregion | CultureInfo settings    |

		#region | Filter                  |

		private void ComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			if (IsEmpty())
				return;		// dataset is empty
			if (tsbMainFiltReset.Selected)
				return;		// 'Reset filter' was pressed
			if (!(sender as Control).Focused)
				return;		// datasourse was changed

			string newvalue;
			string strFiltB = string.Empty;
			string strFiltF = string.Empty;
			string strFiltS = string.Empty;

			if (tscbMainBrands.SelectedItem != null)
				strFiltB = "Brand=" + tscbMainBrands.ComboBox.SelectedValue.ToString();
			if (tscbMainFormats.SelectedItem != null)
				strFiltF = "Format=" + tscbMainFormats.ComboBox.SelectedValue.ToString();
			if (tscbMainShifts.SelectedItem != null)
				strFiltS = "Shift=" + tscbMainShifts.ComboBox.SelectedValue.ToString();

			strFiltB = Utils.StrJoin(strFiltB, strFiltF);
			newvalue = Utils.StrJoin(strFiltB, strFiltS);

			if (_filter != newvalue) {
				_filter = newvalue;
				ApplyFilter();
			}
		}

		private void btFiltReset_Click(System.Object sender, System.EventArgs e)
		{
			ResetFiltLabels();
		}

		private void btHide_Click(System.Object sender, System.EventArgs e)
		{
			bool vis = !tsbMainFiltReset.Visible;
			if (sender == null)
				vis = false;

			tscbMainShifts.Visible = vis;
			tscbMainFormats.Visible = vis;
			tscbMainBrands.Visible = vis;
			tsbMainFiltReset.Visible = vis;
			ResetFiltLabels();

			if (vis) {
				tsbMainHideFilters.Text = "›";
				tsbMainHideFilters.Image = Res.hide_right;
			} else {
				tsbMainHideFilters.Text = "‹";
				tsbMainHideFilters.Image = Res.hide_left;
			}
		}

		private void ResetFiltLabels()
		{
			tscbMainShifts.SelectedItem = null;
			tscbMainFormats.SelectedItem = null;
			tscbMainBrands.SelectedItem = null;
			// Another one time since does not work properly
			tscbMainShifts.SelectedItem = null;
			tscbMainFormats.SelectedItem = null;
			tscbMainBrands.SelectedItem = null;

			tscbMainShifts.Text = Res.strShifts;
			tscbMainFormats.Text = Res.strFormats;
			tscbMainBrands.Text = Res.strBrands;

			if (_filter != "") {
				_filter = "";
				ApplyFilter();
			}
		}

		private void ApplyFilter()
		{
			if (IsEmpty()) return;
			SetCursWait();

			_dvG.RowFilter = _filter;
			BuildAnalysis();
			_lu[L].CalcTotalRow(_filter);
            _lu_[L].CalcTotalRow(_filter);
			BuildChartMap(_lu[L], false);
            //BuildChartMapCounter(_lu_[L], false);

			SetLabelsVisible(_ch[L], MU.Mins / 1440D);
            SetLabelsVisible(_ch_[L], MU.Mins / 1440D);
			BuildLosses();

			string parts = "";
			if (_trendCol != "K0" && MU.Kpi.DenomParts.ContainsKey(_trendCol))
				parts = "RD IN ('" + MU.Kpi.DenomParts[_trendCol].Replace(";", "','") + "')";

			if (_dvS != null) _dvS.RowFilter = Utils.StrJoin(parts, _filter);
			LoadGridDetailGroups();

			string flt = _filter;
			if (_filter != "") {
				// removing Shift condition from the filter since the production tables
				// and PlanSpeed table does not contains Shift column
				int pos = flt.IndexOf(" and Shift");
				if (pos >= 0)
					flt = flt.Remove(pos);
				else {
					pos = flt.IndexOf("Shift");
					if (pos >= 0) flt = flt.Remove(pos);
				}
			}
			if (_lu[L].DataSet.Tables.Contains("ProductionExtended")) {
				_lu[L].DataSet.Tables["ProductionExtended"].DefaultView.RowFilter = flt;
				_lu[L].DataSet.Tables["ProductionShort"].DefaultView.RowFilter = flt;
			}
			if (_lu[L].DataSet.Tables.Contains("PlanSpeed"))
				_lu[L].DataSet.Tables["PlanSpeed"].DefaultView.RowFilter = flt;

            if (_lu_[L].DataSet.Tables.Contains("ProductionExtended"))
            {
                _lu_[L].DataSet.Tables["ProductionExtended"].DefaultView.RowFilter = flt;
                _lu_[L].DataSet.Tables["ProductionShort"].DefaultView.RowFilter = flt;
            }
            if (_lu_[L].DataSet.Tables.Contains("PlanSpeed"))
                _lu_[L].DataSet.Tables["PlanSpeed"].DefaultView.RowFilter = flt;

			////mnuClick(mnuSpread, null);
			SetCursDefault();
		}

		#endregion | Filter                  |

		#region | Sizes & Painting        |

		private void frmMonitor_SizeChanged(object sender, System.EventArgs e)
		{
			if (Visible && _lu != null) {
				SetGridLinesHeight();
				//_chLosses.Legend.LocationDefault = new Point(_chLosses.Width - 200, 80);
			}
		}

		private void SetGridLinesHeight()
		{
			if (_lu == null)
				return;
			HitTestInfo hti = _grLines.HitTest();
			bool mouseover = hti.Row >= 0;

			int row_height = _grLines.Rows.DefaultSize;
			//int rows_to_show = monitorsettings.grLinesCollapsed && !force ? (lu.Count > 1 ? 3 : 2) : lu.Count > 10 ? 11 : lu.Count + 2;
			int rows_to_show = !_monitorSettings.grLinesCollapsed || mouseover ? (_lu.Count > 10 ? 11 : _lu.Count + 2) : 3;
			if (hti.Row == 0 && hti.Column == 0) rows_to_show = !_monitorSettings.grLinesCollapsed ? (_lu.Count > 10 ? 11 : _lu.Count + 2) : 3;
			int height = row_height * (rows_to_show) + 2;
			if ((_grLines.ScrollBarsVisible & ScrollBars.Horizontal) > 0) height += SystemInformation.HorizontalScrollBarHeight;

			pnLines.SuspendLayout();
			if (_grLines.Height != height) _grLines.Height = height;
			pnLines.Height = height + 5;
			pnLines.ResumeLayout();
            if (!use_easy_counter)
                pnLines.Visible = MU.Total != null && MU.Total.Rows.Count != 0;
            else
                pnLines.Visible = false;
		}

		private void SetGridTrendsHeight()
		{
			//chTrends.Height = 70 + (dv.Count > 24 ? 24 : dv.Count + 1) * 22;
			int row_cnt = _grTrends.Rows.Count + 1;
			row_cnt = _monitorSettings.grTrendsCollapsed ? (row_cnt > 1 ? 4 : 3) : (row_cnt > 12 ? 12 : row_cnt);

			_chTrends.BeginUpdate();
			//pnTrends.SuspendLayout();
			//chTrends.Visible = false;

			_grTrends.Height = row_cnt * _grTrends.Rows.DefaultSize + 2 + SystemInformation.HorizontalScrollBarHeight;

			_chTrends.EndUpdate();
			//chTrends.Visible = true;
			//pnTrends.ResumeLayout();
		}

		private void SetChAnalyseHeight()
		{
			_pnAnalyseChart.Height = C.GetInt(_chAnalyse.ChartArea.AxisX.Max) * 15 + 10;
			_chAnalyse.ChartArea.LocationDefault = new Point(-1, 5);
			_chAnalyse.ChartArea.SizeDefault = new Size(-1, _pnAnalyseChart.Height);
		}

		#endregion | Sizes & Painting        |

		#region | Utils                   |

		private string SetConn(string cn)
		{
#if (DEBUG)
			return cn + ";Connect timeout=5"; //";Packet Size=16384;Connect timeout=10";
#else
         //return cn + ";Connect timeout=10"; //";Packet Size=16384;Connect timeout=10";
         return cn;
#endif
		}

		private void ColumnToFront(C1FlexGrid fg, string columnName, int positionFront)
		{
			if (!fg.Cols[columnName].Visible) fg.Cols[columnName].Visible = true;
			fg.Cols[columnName].Move(positionFront);
		}

		private void ColumnToEnd(C1FlexGrid fg, string columnName, int positionFront)
		{
			fg.Cols[columnName].Move(fg.Cols.Count - 1);
		}

		private void SetCursWait()
		{
			this.Cursor = Cursors.WaitCursor;
			Application.DoEvents();
		}

		private void SetCursDefault()
		{
			this.Cursor = Cursors.Default;
			Application.DoEvents();
		}

		private void CopyDatatableToClipboard(C1FlexGrid gr)
		{
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			SetCursWait();
			if (gr == null) return;

			StringBuilder sb = new StringBuilder(1000000);
			string val = "";

			string colname;
			float cellValue;
			for (int r = 0; r < gr.Rows.Count; r++) {
				for (int c = 0; c < gr.Cols.Count; c++) {
					colname = gr.Cols[c].Name;
					if (!gr.Cols[c].Visible)
						continue;
					val = "";
					if (r < gr.Rows.Fixed) {
						// Определяем заголовки колонок
						if (gr.Cols[c].DataMap != null && gr[r, c] != null && gr.Cols[c].DataMap.Contains(gr[r, c]))
							val = gr.Cols[c].DataMap[gr[r, c]].ToString();
					} else if (gr[r, c] == null) {
						// ветка нужна! но делать ничего не надо val и так пустой
					} else if (MU.Kpi.Measure.ContainsKey(colname)) {
						// По стандартным именам колонок определяем размерности и представление
						switch (MU.Kpi.Measure[colname]) {
							case 1: val = Utils.GetTimeSpanString(gr.GetData(r, c)); break;	//Utils.GetTimeSpanString(o)
							case 2:
							case 4: val = Utils.GetFloatString(C.GetFloat(gr[r, c])); break;
							case 3: val = C.GetInt(gr[r, c]).ToString(); break;
							default:
								if (gr.Cols[c].DataMap != null && gr.Cols[c].DataMap.Contains(gr[r, c]))
									val = gr.Cols[c].DataMap[gr[r, c]].ToString();
								break;
						}
					} else if (gr == _grLosses) {
						// Для грида "Распределение потерь"
						float fval;
						fval = C.GetFloat(gr[r, c]);
						if (fval == 0)
							val = "";
						else if (colname == "Events")
							val = fval.ToString();
						else if (colname == "Time" || colname.StartsWith("MT"))
							val = Utils.GetTimeSpanString(gr[r, c]);
						else
							val = Utils.GetFloatString(fval * 100);
					} else if (gr == _grDet) {
						// Часть колонок грида "Подробный список"
						if (colname == "RD") {
							val = "";
						} else if (MU.marks.ContainsKey(colname)) {
							int id = C.GetInt(gr[r, c]);
							if (id > 0)
								val = MU.marks[colname][id];
						} else if (tsbDetFromTo.Checked || tsbDetFromToPvt.Checked) {
							cellValue = C.GetFloat(gr[r, c]);
							if (colname == "sumPerc") {
								val = Utils.GetFloatString
									(cellValue * 100 / (float)_lu[L].LineRow[MU.Kpi.Denominators[MU.Kpi.MainKpiKey]]);
							} else if (colname.StartsWith("s") || colname.StartsWith("m"))
								val = Utils.GetTimeSpanString(cellValue);
						}
					}
					if (val == "")
						val = gr[r, c] == null ? "" : gr[r, c].ToString();

					sb.Append(val);
					if (c < gr.Cols.Count - 1) sb.Append("\t");
				}
				if (r < gr.Rows.Count - 1) sb.Append("\n");
			}

			//Set clipboard
			//Clipboard.SetDataObject(sb.ToString());								// Попытка раз 
			//Clipboard.SetText(sb.ToString(), TextDataFormat.Text);			// Попытка два
			Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);	// Попытка три
			
			SetCursDefault();
#if DEBUG
			Debug.WriteLine("CopyDatatableToClipboard: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}


		#endregion | Utils                   |


		private void tmActivity_Tick(object sender, EventArgs e)
		{
			if (MU.DtStart == DateTime.Now.Date && _idleCounter++ >= 1 && _state == C.State.Idle)  // 5
				LoadStart();
		}

		private void tmLoading_Tick(object sender, EventArgs e)
		{
			if (this.sb.Width > 0) {
				int w;
				Brush br;
				switch (rnd.Next(20)) {
					case 0:
						br = Brushes.Black;
						w = rnd.Next(this.Width / 9) ;
						break;
					case 1:
					case 6:
					case 7:
						br = Brushes.DarkGreen;
						w = rnd.Next(Width);
						break;
					case 2:
						br = Brushes.Gray;
						w = rnd.Next(Width / 3);
						break;
					case 3:
						br = Brushes.Gold;
						w = rnd.Next(Width / 3);
						break;
					case 4:
						br = Brushes.Red;
						w = rnd.Next(Width / 12);
						break;
					case 5:
						br = Brushes.Red;
						w = rnd.Next(Width / 6);
						break;
					default:
						w = rnd.Next(2) + 1;
						br = Brushes.Black;
						break;
				}
				try {
					using (Graphics g = sb.CreateGraphics()) {
						float dbwait = (_swTotal.ElapsedMilliseconds - _swDb.ElapsedMilliseconds) / 1000F;
						// Create string and measure width for graphics
						MainMenu.Items["testlabel"].Text = string.Format("Time spent:  {0:0.00} + {1:0.00}", dbwait, _swTotal.ElapsedMilliseconds / 1000F);

						string loadingstr = MU.LoadingText == "" ? _loadingText : MU.LoadingText;
						string str = string.Format("  {0:0.0} + {1,-8:0.0}{2}", dbwait, _swTotal.ElapsedMilliseconds / 1000F, loadingstr);
						int strwidth = Convert.ToInt32(g.MeasureString(str, sb.Font).Width) + 10;

						// Fill random rectangle wiht randowm color
						//todone: Next бывает отрицательным что приводит к исключению
						int next = this.sb.Width - strwidth;
						if (next < 0)
							return;
						//g.FillRectangle(br, new Rectangle(rnd.Next(next) + strwidth, 0, w + 1, sb.Height));

						// Create a StringFormat object to align text in the panel.
						StringFormat textFormat = new StringFormat
						{
							LineAlignment = StringAlignment.Center,
							Alignment = StringAlignment.Near
						};

						// Fill background for string and then draw string
						Rectangle r = new Rectangle(0, 0, strwidth, sb.Height);
					//	g.FillRectangle(Brushes.DarkGreen, r);
                        g.FillRectangle(Brushes.LightBlue, r);
						g.DrawString(str, sb.Font, Brushes.White, r, textFormat);

                        toolStripProgressBar1.Visible = true;
                        toolStripProgressBar1.Maximum = 100; // MU.Config.Units.Count;
                        if (toolStripProgressBar1.Maximum <= toolStripProgressBar1.Value)
                            toolStripProgressBar1.Value = 0;
                        toolStripProgressBar1.Value      += 1 ; //MU.marks.Count;// 
					}
				} catch { }
			}
		}

		private void txtError_TextChanged(object sender, EventArgs e)
		{
			_gbError.Visible = _txtError.Text == "" ? false : true;
		}

		private void frmMonitor_KeyDown(object sender, KeyEventArgs e)
		{
			_idleCounter = 0;

			if ((_state & C.State.Idle) == 0)
				return;

			bool mod = e.Modifiers == Keys.Control || e.Modifiers == Keys.Alt;

			if (e.KeyCode == Keys.PageDown && e.Modifiers == Keys.Control) {		// call the dropdown form for date&line selection
				tssbMainDateLineSelector.ShowDropDown();
			} else if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.Alt) {		// FormWindowState
				if (WindowState == FormWindowState.Maximized)
					WindowState = FormWindowState.Normal;
				else if (WindowState == FormWindowState.Normal)
					WindowState = FormWindowState.Maximized;
			} else if (e.KeyCode == Keys.PageDown) {										// move to next day
				btNext_Click(tsbMainNext, null);
			} else if (e.KeyCode == Keys.PageUp) {											// move to previous day
				btPrev_Click(tsbMainPrev, null);
			} else if (e.KeyCode == Keys.F3) {												// move to today
				btNow_Click(tsbMainNow, null);
			} else if (e.KeyCode == Keys.F4) {												// edit mode
				tsbEdit.Checked = !tsbEdit.Checked;
				if (_isEdit) e.SuppressKeyPress = true;
				SetEditState(tsbEdit.Checked);
			} else if (e.KeyCode == Keys.F5) {												// refresh data
				btRefresh_Click(tsbMainRefresh, null);
			} else if (e.KeyCode == Keys.F6) {												// refresh data only for failed
				LoadFailedStart();
			} else if (e.KeyCode == Keys.F7) {												// about
				string cpr = "";
				Splash.ShowSplash(300, C.GetAssemblyShortInfo(Assembly.GetExecutingAssembly(), ref cpr), cpr);
			} else if (e.KeyCode == Keys.X && e.Modifiers == Keys.Alt) {			// reset chart
				ResetChartMap();
				if (_chs != null) _chs.ChartArea.AxisX.ScrollBar.Scale = 1;
			} else if (e.KeyCode == Keys.W && e.Modifiers == Keys.Control) {		// window size to 1024x740
				this.WindowState = FormWindowState.Normal; this.Size = new Size(1024, 720);
			} else if (e.KeyCode == Keys.D1 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[_chs.L].ChartArea.AxisX.ScrollBar.Scale = 1;
				_chs.ChartArea.AxisX.ScrollBar.Step = 1;
			} else if (e.KeyCode == Keys.D2 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[_chs.L].ChartArea.AxisX.ScrollBar.Scale = 0.5;
				_chs.ChartArea.AxisX.ScrollBar.Step = 0.5;
			} else if (e.KeyCode == Keys.D3 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[_chs.L].ChartArea.AxisX.ScrollBar.Scale = 0.2;
				_chs.ChartArea.AxisX.ScrollBar.Step = 0.1;
			} else if (e.KeyCode == Keys.D4 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[_chs.L].ChartArea.AxisX.ScrollBar.Scale = 0.1;
				_chs.ChartArea.AxisX.ScrollBar.Step = 0.05;
			} else if (e.KeyCode == Keys.D5 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[_chs.L].ChartArea.AxisX.ScrollBar.Scale = 0.05;
				_chs.ChartArea.AxisX.ScrollBar.Step = 0.025;
			} else if (e.KeyCode == Keys.D6 && _mapMode && mod && _chs.L >= 0) {	// timeline scale
				_ch[L].ChartArea.AxisX.ScrollBar.Scale = 0.02;
				_chs.ChartArea.AxisX.ScrollBar.Step = 0.01;
			} else if (e.KeyCode == Keys.C && e.Modifiers == Keys.Control) {
				ProcessCopy();																		//Копирование в клипборд 
			} else if (e.KeyCode == Keys.Down && e.Modifiers == Keys.Control) {
				tabMain.Select();																	// выбор следующей линии
				_grLines.Row = _grLines.Row == _grLines.Rows.Count - 1 ? 1 : _grLines.Row + 1;
			} else if (e.KeyCode == Keys.Up && e.Modifiers == Keys.Control) {
				tabMain.Select();																	// выбор предыдущей линии
				_grLines.Row = _grLines.Row == 1 ? _grLines.Rows.Count - 1 : _grLines.Row - 1;
			} else if (e.KeyCode == Keys.G && e.Modifiers == Keys.Control) {		// переключение вида grLines с развернутого на сокращенный
				_monitorSettings.grLinesCollapsed = !_monitorSettings.grLinesCollapsed;
				SetGridLinesHeight();
			} else if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control) {		// переключение вида grLines с развернутого на сокращенный
				if (tsbMainHideFilters.Enabled)
					btHide_Click(tsbMainFiltReset, null);
			} else if (_isEdit) {
				if (e.KeyCode == Keys.Delete) {
					ProcessDeleteKey();
				} else if (_isEdit && e.KeyCode == Keys.S && e.Modifiers == Keys.Control && _lu[L].UndoRedo.ThereIsUndo) {
					Save();																			// save
				} else if (e.KeyCode == Keys.Z && e.Modifiers == Keys.Control && _lu[L].UndoRedo.ThereIsUndo) {
					ChangeState(UndoRedo.Action.Undo);										// undo
				} else if (e.KeyCode == Keys.Y && e.Modifiers == Keys.Control) {
					ChangeState(UndoRedo.Action.Redo);										// redo
				} else if (e.KeyCode == Keys.N && mod && tabEdit.SelectedTab == tpEditDnTms) {
					FindEmptyGap(false);															// autofill empty downtime
				} else if (e.KeyCode == Keys.M && mod && tabEdit.SelectedTab == tpEditDnTms) {
					FindEmptyGap(true);															// autofill short stops
				} else if (e.KeyCode == Keys.Enter) {
					//Debug.WriteLine(ActiveControl.Name.ToString());
				}
			} else if (e.KeyCode == Keys.F && mod) {
				//GetFromToMatrix(L);
			} else if (e.KeyCode == Keys.F8 && e.Modifiers == Keys.Alt) {
				UpdateBW();
			}
			Debug.WriteLine(String.Format("e.KeyCode: {0}   e.Modifiers: {1}", e.KeyCode, e.Modifiers));
		}

		private void ProcessDeleteKey()
		{
			if (ActiveControl is DataGridViewTreeViewComboBoxEditingControl) {
				DeleteEntry(_bsDnTms);													// delete downtime row
				if (dgvDnTms.CurrentCell != null) dgvDnTms.CurrentCell = dgvDnTms[0, dgvDnTms.CurrentRow.Index];
			} else if (ActiveControl is C1Chart && tabEdit.SelectedTab == tpEditDnTms) {
				DeleteEntry(_bsDnTms);													// delete downtime row
			} else if (ActiveControl == dgvProds) {
				DeleteEntry(_bsProds);													// delete production row
			} else if (ActiveControl == dgvShifts) {
				DeleteEntry(_bsShifts);													// delete shift row
			} else if (dgvDnTms.CurrentCell != null && dgvDnTms.CurrentCell.IsInEditMode) {
				// Удаление значения Mark 
				string colname = dgvDnTms.Columns[dgvDnTms.CurrentCell.ColumnIndex].Name;
				if (colname == "Mark1" || colname == "Mark2") {
					dgvDnTms.CurrentCell.Value = null;
					DataRowView drv = dgvDnTms.Rows[dgvDnTms.CurrentCell.RowIndex].DataBoundItem as DataRowView;
					drv[colname] = DBNull.Value;
					drv.EndEdit();
				}	
			}
		}

		private void ProcessCopy()
		{
			if (ActiveControl == _grAnalyse) {
				Clipboard.SetDataObject(_grAnalyse.GetCellRange(0, 0, _grAnalyse.Rows.Count - 1, _grAnalyse.Cols.Count - 1).Clip);
			} else if (ActiveControl is C1FlexGrid) {
				CopyDatatableToClipboard(ActiveControl as C1FlexGrid);
			} else if (ActiveControl is C1Chart) {
				C1Chart c1ch = ActiveControl as C1Chart;
				c1ch.SaveImage(ImageFormat.Png, c1ch.Size);
			}
		}

		private void UpdateBW()
		{
			SetCursWait();
			string res = "Updated rows: ";

			foreach (MU u in _lu)
				res = String.Format("{0}{1}{2}: {3}", res, Environment.NewLine, u.LineName, u.UpdateBW());

			SetCursDefault();
			MessageBox.Show(res);
		}

		public static TimeStyle TmStyle
		{
			get { return _tmStyle; }
			set { _tmStyle = value; }
		}

		private void SetDetailedRowsCount()
		{
			if (tabMain.SelectedTab == tpDetail) {
				if (_grDet.DataSource != null && _grDet.DataSource is DataView)
					sbpDesc.Text = (_grDet.DataSource as DataView).Count + " Rows  ";
				sbpDesc.TextAlign = ContentAlignment.MiddleRight;
			} else {
				sbpDesc.Text = "";
				sbpDesc.TextAlign = ContentAlignment.MiddleLeft;
			}
		}

        private void MainMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cmMapCounter_Opening(object sender, CancelEventArgs e)
        {
            // прячем тултип
            Tt.RemoveAll();

            //todone: сделать пункт меню "Удалить" доступным только для админа и локального админа
            cmMapCounter.Items["mnuApply"].Visible = false;
            cmMapCounter.Items["mnuDelProd"].Visible = false;
            cmMapCounter.Items["mnuChange"].Visible = false;
            cmMapCounter.Items["mnuAutoShift"].Visible = true;
            if (use_easy_counter)
            cmMapCounter.Items["mnuAutoShift"].Visible = false;

            cmMapCounter.Items["mnuShowMins"].Visible = false;// _chs != null && _lu[_chs.L].DataSet.Tables.Contains("Minutes");

            if (!_isEdit)
                return;	// нет режима редактирования

            ////if (!selzone.Visible)
            ////   return;	// зона редактирования не видна

           // if (!_sz.Visible) GetCurrentBlackZone();
           // if (!_sz.Visible || !_sz.MouseInside(mouse_location) ||
           //     (_sz.DownID != 0 && tabEdit.SelectedTab == tpEditDnTms))
           //     return;

            DateTime dtFrom = _sz.Start;
            DateTime dtTo = _sz.Stop;
            bool mouse_is_inside_seln = _sz.MouseInside(mouse_location);

            if (dtFrom == dtTo)
                return;
            if (tabEdit.SelectedTab == tpEditDnTms)
            {
                if (mouse_is_inside_seln)
                {
                    cmMapCounter.Items["mnuApply"].Visible = true;
                    cmMapCounter.Items["mnuApply"].Text = Res.mnuMapAddDwnTm;										// "Add downtime entry"
            //        cmMap.Items["mnuDelProd"].Visible = _lu[L].SessionData.Permission == UserPermit.LocalAdmin || _lu[L].SessionData.Permission == UserPermit.Admin;
                }
                else
                {
                    if (_sz.IsEmptyDowntime && _sz.MouseInside(mouse_location))
                    {
                        //todone: определить нет ли рядом еще черных зон, и если есть, добавить их к интервалу
                        cmMapCounter.Items["mnuApply"].Visible = true;
                        DateTime dt1 = _dtEmpty;
                        DateTime dt2 = _dtEmpty;
                        GetAroundBlacks(ref dt1, ref dt2);
                        _sz.Start = dt1;
                        _sz.Stop = dt2.AddMinutes(1);
                        cmMapCounter.Items["mnuApply"].Text = Res.mnuMapAddDwnTm;									// "Add the downtime entry instead of the black zone";
                    }
                }
            }
            else if (mouse_is_inside_seln && tabEdit.SelectedTab == tpEditProds)
            {
                cmMapCounter.Items["mnuApply"].Visible = true;
                cmMapCounter.Items["mnuApply"].Text = string.Format(Res.mnuMapSplit, mouse_location);	// "Split the production entry";
            }
            else if (mouse_is_inside_seln && tabEdit.SelectedTab == tpEditShifts)
            {
                cmMapCounter.Items["mnuApply"].Visible = true;
                cmMapCounter.Items["mnuApply"].Text = string.Format(Res.mnuMapSplit, mouse_location);	// "Split the shift entry";
                DateTime dt1, dt2;
                if (GetShiftTimeByTemplate(out dt1, out dt2))
                {
                    if (!use_easy_counter)
                    {
                        cmMapCounter.Items["mnuAutoShift"].Visible = true;
                        cmMapCounter.Items["mnuAutoShift"].Text = string.Format(Res.mnuMapAutoShift, dt1, dt2.AddMinutes(-1));
                    }
                }
            }
        }

        private void cbBigChartCounter_CheckStateChanged(object sender, EventArgs e)
        {
            SetChartMapHeight_counter(cbBigChartCounter.Checked);
        }

        private void _tm_counter_Tick(object sender, EventArgs e)
        {
            if (_qu.Count == 0)
            {
                _dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
                _tm_counter.Enabled = false;
            }
        }

        private void create_grid_counters_kpi()
        {
            try
            {

        
            ds_counters.Tables["Counters_kpi"].Rows.Clear();
            foreach (var unit in _lu_)
            {
                DataRow[] DR = unit.DataSet.Tables["Counters_kpi"].Select();
                foreach (DataRow dr in DR)
                {
                    DataRow row1 = ds_counters.Tables["Counters_kpi"].NewRow();
                    row1["Name"] = (string)dr["name"];
                    row1["id"] = (int)dr["id"];
                    try
                    {
                        row1["Max_"] = (double)dr["max_"];
                        row1["Min_"] = (double)dr["min_"];
                        row1["Avg_"] = (double)dr["avg_"];
                        c1FlexGrid1.Cols["error"].Visible = false;

                    }
                       
                    catch (Exception)
                    {
                        c1FlexGrid1.Cols["error"].Visible = true ;
                        row1["error"] = (string)Res.mapNotAvalb;
                      //  throw;
                    }


                    // _lu_[1].DataSet.Tables["Counters_kpi"].Rows.Add(row1);
                    ds_counters.Tables["Counters_kpi"].Rows.Add(row1);

                }
            }

            }
            catch (Exception)
            {

                // throw;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            create_grid_counters_kpi();

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //tsbBigChartCounter.Checked = !tsbBigChartCounter.Checked;

            set_tsbBigChartCounter(tsbBigChartCounter);
            cbBigChartCounter.Checked = tsbBigChartCounter.Checked;
           // SetChartMapHeight_counter(tsbBigChartCounter.Checked);

        }

        private void set_tsbBigChartCounter( ToolStripButton tsb )
        {
            if (!tsb.Checked)
            {
                tsb.Image = ProductionMonitor.Properties.Resources.checkbox_empty;// ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
            }
            else
            {
                tsb.Image = ProductionMonitor.Properties.Resources.checkbox_full;
            }

        }

        private void tsbShowTipsCounter_Click(object sender, EventArgs e)
        {

            set_tsbBigChartCounter(tsbShowTipsCounter);
            cbShowTipsCounter.Checked = tsbShowTipsCounter.Checked;
        }

        private void tsbShowLimitsCounter_Click(object sender, EventArgs e)
        {
            set_tsbBigChartCounter(tsbShowLimitsCounter);
            btRefresh_Click( sender,  e);

        }

        private void button2_Click(object sender, EventArgs e)
        {

            label1.Text = String.Format("{0:g}   -   {1:g}", MU.DtStart, MU.DtEnd.AddMinutes(-1));
            if (_state == C.State.Idle)
            {
                _state = C.State.Reporting;
                int lv_row_count = c1FlexGrid2.Cols.Count;
 
            try
            {

                ds_counters.Tables["Counters_rep2"].Rows.Clear();

                c1FlexGrid2.DataSource = null;  // 
                c1FlexGrid2.DataMember = "";
                c1FlexGrid2.Cols.RemoveRange(0, lv_row_count);

                button3.Enabled = false;
                button2.Enabled = false;
                //ds_counters.Tables["Counters_rep2"].Rows.Clear();
                
                DataColumnCollection lv_dcc =  this.ds_counters.Tables[1].Columns ;//.Remove(;
             string[]   array1 = new string[this.ds_counters.Tables[1].Columns.Count];
                int i;
                i = 0;
                foreach ( DataColumn unit in lv_dcc)

                {
                    array1[i] =  unit.ColumnName;
                    i++;

                }
                foreach (string item in array1)
                {
                    if (!(item == "Date" || item == "Time" || item == "dt" || item == "Counters" || item == "Brand"))
                    {
                        this.ds_counters.Tables[1].Columns.Remove(item);
                    }
                }

                string name_s;
                string name_s1;
                string name_s2;
                object[] findTheseVals = new object[2];

              //  toolStripProgressBar1
                int lv_count = _lu_.Count;


                int lv_step = toolStripProgressBar1.Maximum / lv_count;
                toolStripProgressBar1.Value = 0;
                toolStripProgressBar1.Visible = true;
                foreach (var unit in _lu_)
                {

                    TimeLineChart chart = _ch_[unit.L];
                    name_s = chart.Header.Text;//unit.GetLongUnitName_counters2(true, " ", unit.Parent_Counters);
                    if (name_s == "")
                    {
                        continue;
                    }
                    name_s1 = unit.GetLongUnitName_counters2(true, " ", unit.Parent_Counters);
                    name_s2 = unit.GetLongUnitName_counters2(false, " ", unit.Parent_Counters);
                    name_s1.Replace(name_s2, "");

                   if ( ! this.ds_counters.Tables[1].Columns.Contains(name_s))
                    this.ds_counters.Tables[1].Columns.Add(name_s);

                   //DataColumn[] keys = new DataColumn[1];
                   //DataColumn column;
                   //column = new DataColumn();
                   //column.DataType = System.Type.GetType("System.String");
                   //column.ColumnName = "dt_from";
                   //table.Columns.Add(column);
                   //keys[0] = column;
                   //unit.DataSet.Tables["Counters_brands"].PrimaryKey = keys;

                   DataRow[] DR1 = unit.DataSet.Tables["Counters_brands"].Select();

                   foreach (DataRow dr in DR1)
                   {
                      // string sss = string.Format(" dt = #{0:M/d/yy H:m}#", (DateTime)dr["dt"]);
                       bool lv_bool1;
           //  dates = string.Format("{0}<=#{1:M/d/yy H:m}# ", "dt_from", mouse_location);
           //  DR_brands = dts.Tables["Counters_Brands"].Select(dates);
                       findTheseVals[0] = (DateTime)dr["dt_from"];
                       findTheseVals[1] = name_s1;

                       DataRow DR_sel1 = ds_counters.Tables["Counters_rep2"].Rows.Find(findTheseVals);// ((DateTime)dr["dt_from"]);// Select(sss);
                      DataRow row1;
                      if (DR_sel1 == null)
                      {
                         row1 = ds_counters.Tables["Counters_rep2"].NewRow();
                         lv_bool1 = true;
                      }
                      else
                      {
                           row1 = DR_sel1;
                           lv_bool1 = false;
                      }



                            row1["Dt"]       = ((DateTime)dr["dt_from"]);
                            row1["Date"]     = ((DateTime)dr["dt_from"]).ToLongDateString();
                            row1["Time"]     = ((DateTime)dr["dt_from"]).ToLongTimeString();
                            row1["Brand"]    = (string)dr["brand_name"];
                            row1["Counters"] = name_s1; 
                            if (lv_bool1)
                                ds_counters.Tables["Counters_rep2"].Rows.Add(row1);

                   }


                    DataRow[] DR = unit.DataSet.Tables["Counters_value"].Select();

                    foreach (DataRow dr in DR)
                     {

                         string ss = string.Format(" dt = #{0:M/d/yy H:m}#", (DateTime)dr["dt"]);
                         bool lv_bool;

 
                         findTheseVals[0] = (DateTime)dr["dt"];
                         findTheseVals[1] = name_s1;

                         DataRow DR_sel = ds_counters.Tables["Counters_rep2"].Rows.Find(findTheseVals); // ((DateTime)dr["dt"]);// Select(ss);
                      DataRow row1;
                      if (DR_sel == null)
                      {
                         row1 = ds_counters.Tables["Counters_rep2"].NewRow();
                         lv_bool = true;
                      }
                      else
                      {
                           row1 = DR_sel;
                           lv_bool = false;
                      }
                        
                        try
                        {
                            row1["Dt"] = ((DateTime)dr["dt"]);
                            row1["Date"] = ((DateTime)dr["dt"]).ToLongDateString();
                            row1["Time"] = ((DateTime)dr["dt"]).ToLongTimeString();

                            row1["Counters"] = name_s1;  // name_s;
                            double lv_doub_pars;
                            double.TryParse(((double)dr["value"]).ToString("F3"), out lv_doub_pars);
                            row1[name_s] =  lv_doub_pars ; //double.TryParse((double)dr["value"]).ToString("F3");
                            //row1["Avg_"] = (double)dr["avg_"];
                            // c1FlexGrid2.Cols["error"].Visible = false;

                        }

                        catch (Exception)
                        {
                            c1FlexGrid2.Cols["error"].Visible = true;
                            row1["error"] = (string)Res.mapNotAvalb;
                            //  throw;
                        }


                        // _lu_[1].DataSet.Tables["Counters_kpi"].Rows.Add(row1);
                        if (lv_bool)
                        ds_counters.Tables["Counters_rep2"].Rows.Add(row1);

                    }
                 //   c1FlexGrid2.DataSource = this.ds_counters;
                //    c1FlexGrid2.DataMember = "Counters_rep2";
                //    return;
                    if (toolStripProgressBar1.Value + lv_step <= toolStripProgressBar1.Maximum)
                    {
                        toolStripProgressBar1.Value = toolStripProgressBar1.Value + lv_step;

                    }
                    else toolStripProgressBar1.Value = 0;

                }


            }
           finally  // catch (Exception)
            {
                button2.Enabled = true;
                _state = C.State.Idle;
                // throw;
            }

            toolStripProgressBar1.Visible = false;
            //  c1FlexGrid2.Enabled = true;
            if (ds_counters.Tables["Counters_rep2"].Rows.Count > 0)
            {
                button3.Enabled = true;
            }

  //ds_counters.Tables["Counters_rep2"].AcceptChanges();
  //ds_counters.Tables["Counters_rep2"].DefaultView.Sort = "Counters ,Date ASC  ,Time ASC";

  DataRow[] DR_sort = ds_counters.Tables["Counters_rep2"].Select("","Counters ,Date ASC  ,Time ASC");
  string lv_brand_str;
  lv_brand_str = "";
        foreach (DataRow dr in DR_sort)
        {
            if (! string.IsNullOrEmpty(dr["Brand"].ToString()))
            {
                lv_brand_str = (string)dr["Brand"];
            }
            else
                  dr["Brand"] = lv_brand_str ;
       }
  //foreach (var item in ds_counters.Tables["Counters_rep2"])
  //{
      
  //}

            c1FlexGrid2.DataSource = this.ds_counters;
            c1FlexGrid2.DataMember = "Counters_rep2";
                SortFlags  order;

                order = SortFlags.UseColSort;// Descending;  // .Equals(
               // c1FlexGrid2.Sort(order, rg )  ;
                c1FlexGrid2.Cols[1].Sort = SortFlags.Ascending;
                c1FlexGrid2.Cols[2].Sort = SortFlags.Ascending;
                 c1FlexGrid2.Sort(order, 1,2)  ;
            c1FlexGrid2.Cols.Remove("dt");
           // c1FlexGrid2.Cols.Remove("error");




            } 

        }

        private void button3_Click(object sender, EventArgs e)
        {

            //foreach (DataRow row in this.ds_counters.Tables["Counters_rep2"].Rows )
            //{

            //}

            //if (_state != C.State.Idle)
            //    return;
            //List<string> cols = new List<string>();
            //foreach (Column col in c1FlexGrid2.Cols)
            //    if (col.Visible)
            //        cols.Add(col.Name);
            ds_counters.Tables["Counters_rep2"]. DefaultView.Sort = "Date ASC  ,Time ASC";
            ds_counters.Tables["Counters_rep2"].AcceptChanges();
         //   DataView _dv = new DataView(ds_counters.Tables["Counters_rep2"]);
         //   _dv.Sort = "Date ASC  ,Time ASC";
        //    DataTable NewTable = _dv.ToTable();
            ExcelReport_Counters er = new ExcelReport_Counters(ds_counters.Tables["Counters_rep2"], ds_counters.Tables["Counters_rep2"].Columns);
            //ExcelReport_Counters er = new ExcelReport_Counters(ds_counters.Tables["Counters_rep2"]);
        }

        private void c1FlexGrid1_Click(object sender, EventArgs e)
        {

        }

        private void Tt_Popup(object sender, PopupEventArgs e)
        {

        }


	}
}

