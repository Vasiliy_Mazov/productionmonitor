using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Diagnostics;

namespace ProductionMonitor
{
	//[SecurityPermissionAttribute(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
	public class DataGridViewTreeViewComboBoxEditingControl : ComboBox, IDataGridViewEditingControl
	{
		public DataGridViewTreeViewComboBoxEditingControl(): base()
		{
			tv = new TreeView();
			tv.BorderStyle = BorderStyle.None;
			tv.AfterSelect += new TreeViewEventHandler(tv_AfterSelect);
			tv.KeyDown += new KeyEventHandler(tv_KeyDown);
			tv.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(tv_NodeMouseDoubleClick);
			tv.NodeMouseClick += new TreeNodeMouseClickEventHandler(tv_NodeMouseClick);

			treeViewHost = new ToolStripControlHost(tv);
			treeViewHost.AutoSize = false;
			treeViewHost.KeyDown += new KeyEventHandler(treeViewHost_KeyDown);
			treeViewHost.GotFocus += new EventHandler(treeViewHost_GotFocus);
			treeViewHost.Enter += new EventHandler(treeViewHost_Enter);

			dropDown = new ToolStripDropDown();
			dropDown.Items.Add(treeViewHost);


			//dropDown.KeyDown += new KeyEventHandler(dropDown_KeyDown);

			this.DataSourceChanged += new EventHandler(DataGridViewTreeViewComboBoxEditingControl_DataSourceChanged);
		}

		public TreeView TreeView
		{
			get { return tv; }
		}

		#region | private fields                           |
		
		private ToolStripControlHost treeViewHost;
		private ToolStripDropDown dropDown;
		private DataGridView dgView;
		private bool valueChanged = false;
		private TreeView tv;
		private int rowIndex;
		private const int WM_USER = 0x0400,
						WM_REFLECT = WM_USER + 0x1C00,
						WM_COMMAND = 0x0111,
						CBN_DROPDOWN = 7,
						CB_SHOWDROPDOWN = 0x14F;

		#endregion

		#region | Implementing IDataGridViewEditingControl |

		public object EditingControlFormattedValue
		{
			get {
				return this.Text; //SelectedValue; //this.Text; //
			}
			set { 
				if (value is String) 
					this.Text = value.ToString(); }
		}
		public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
		{
			return EditingControlFormattedValue;
		}
		public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
		{
			this.Font = dataGridViewCellStyle.Font;
		}
		public int EditingControlRowIndex
		{
			// Implements the IDataGridViewEditingControl.EditingControlRowIndex 
			// property.
			get { return rowIndex; }
			set { rowIndex = value; }
		}
		public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
		{
			Debug.WriteLine("EditingControlWantsInputKey");
			switch (key & Keys.KeyCode) {		
				case Keys.Enter:
				case Keys.Space:
					return true;
				default:
					Debug.WriteLine("EditingControl_WANTS_InputKey");
					return !dataGridViewWantsInputKey;
			}

			// Let the DateTimePicker handle the keys listed.
			switch (key & Keys.KeyCode) {
				case Keys.Left:
				case Keys.Up:
				case Keys.Down:
				case Keys.Right:
				case Keys.Home:
				case Keys.End:
				case Keys.PageDown:
				case Keys.PageUp:
				case Keys.Enter:
					return true;
				default:
					return !dataGridViewWantsInputKey;
			}
		}
		public void PrepareEditingControlForEdit(bool selectAll)
		{
			// ���� ������ �������� �� �������� ������ �� ����, ������ �������
			if (dgView.CurrentCell.Value == DBNull.Value || this.SelectedValue == null)
				return;

			foreach (TreeNode n in tv.Nodes)
				n.Collapse();
			if (this.SelectedValue.ToString() == "1") return;
			TreeNode[] TN = tv.Nodes.Find(this.SelectedValue.ToString(), true);
			if (TN.Length > 0)
				tv.SelectedNode = TN[0];

		}
		public bool RepositionEditingControlOnValueChange
		{
			get { return false; }
		}
		public DataGridView EditingControlDataGridView
		{
			get { return dgView; }
			set { dgView = value; }
		}
		public bool EditingControlValueChanged
		{
			get { return valueChanged; }
			set { valueChanged = value; }
		}
		public Cursor EditingPanelCursor
		{
			get { return base.Cursor; }
		}

		#endregion

		////protected override void OnSelectedValueChanged(EventArgs e)
		////{
		////   valueChanged = true;
		////   this.EditingControlDataGridView.NotifyCurrentCellDirty(true);

		////   ////// ��� ��� ����������� ����; � ���������� ���������� ����������� ��� �� �����
		////   ////dgView.EndEdit();
		////   ////if (dgView.CurrentRow != null && dgView.CurrentRow.DataBoundItem != null) {
		////   ////   DataRowView drv = dgView.CurrentRow.DataBoundItem as DataRowView;
		////   ////   if (drv != null) {
		////   ////      //drv["DownID"] = SelectedValue; // ���� ������ ���������������� �� "valueChanged = true;" � "NotifyCurrentCellDirty(true);" �� �����
		////   ////      drv.EndEdit();
		////   ////   }
		////   ////}

		////   base.OnSelectedValueChanged(e);
		////}
		
		private void OnValueChanged()
		{
			this.valueChanged = true;
			if (dgView != null && dgView.ColumnCount > 0) {
				dgView.NotifyCurrentCellDirty(true);

				//////// ��� ��� ����������� ����; � ���������� ���������� ����������� ��� �� �����
				//////dgView.EndEdit();
				//////if (dgView.CurrentRow != null && dgView.CurrentRow.DataBoundItem != null) {
				//////   DataRowView drv = dgView.CurrentRow.DataBoundItem as DataRowView;
				//////   if (drv != null) {
				//////      //drv["DownID"] = SelectedValue; // ���� ������ ���������������� �� "valueChanged = true;" � "NotifyCurrentCellDirty(true);" �� �����
				//////      drv.EndEdit();
				//////      //drv.all
				//////   }
				//////}

				// ��� ��� ����������� ����; � ���������� ���������� ����������� ��� �� �����
				dgView.CurrentCell.Value = this.SelectedValue;
				//dgView.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange);
			}
		}
		private void DataGridViewTreeViewComboBoxEditingControl_DataSourceChanged(object sender, EventArgs e)
		{
			string ds = this.DataSource == null ? "null" : this.DataSource.ToString();
			Debug.WriteLine("DataGridViewTreeViewComboBoxEditingControl_DataSourceChanged " + ds);
		}
		
		private void tv_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Parent != null && e.Action == TreeViewAction.ByMouse && !this.DroppedDown) {
				SelectedValue = C.GetInt(e.Node.Name);
				HideDropDown();
				this.OnValueChanged();		// ���� ������������ OnSelectedValueChanged(EventArgs e) �� ��� ������ ���������������� � ����� OnValueChanged() ����
			}
		}
		private void tv_KeyDown(object sender, KeyEventArgs e)
		{
			TreeNode tn;
			if (e.KeyCode == Keys.Enter && tv.SelectedNode != null && tv.SelectedNode.Parent != null) {
				tn = tv.SelectedNode;
				e.SuppressKeyPress = true;
				HideDropDown();
				string oldval = dgView.CurrentCell.Value.ToString();
				if (oldval == DBNull.Value.ToString() || oldval != tn.Tag.ToString()) {
					SelectedValue = (int)tn.Tag;
					this.OnValueChanged();		// ���� ������������ OnSelectedValueChanged(EventArgs e) �� ��� ������ ���������������� � ����� OnValueChanged() ����
				}
			}
		}
		private void tv_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			TreeNode tn = tv.SelectedNode;
			if (tn != null && tn.Parent != null && dgView.CurrentCell.Value == DBNull.Value && e.Node == tn) {
				SelectedValue = (int)tn.Tag;
				HideDropDown();
				this.OnValueChanged();		// ���� ������������ OnSelectedValueChanged(EventArgs e) �� ��� ������ ���������������� � ����� OnValueChanged() ����
			}
		}
		private void tv_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			HideDropDown();
		}

		private void ShowDropDown()
		{
			if (dropDown != null && !dropDown.Visible) {
				DropDownWidth = Width + 100;
				DropDownHeight = 300;
				treeViewHost.Width = DropDownWidth;
				treeViewHost.Height = DropDownHeight;
				dropDown.Show(this, 0, this.Height);
				TreeView.Focus();
			}
		}
		private void HideDropDown()
		{
			if (dropDown != null) {
				dropDown.Visible = false;
				dropDown.Hide();
			}
		}

		private void dropDown_KeyDown(object sender, KeyEventArgs e)
		{
			// ������������ ��� ���������� ����������� �����
			if (e.KeyCode == Keys.Enter)
				e.SuppressKeyPress = true;
		}
		private void treeViewHost_KeyDown(object sender, KeyEventArgs e)
		{
			// ��� ���������� ���������
			if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Tab) {
				dgView.Focus();
			}
			Debug.WriteLine("___________treeViewHost_KeyDown");
		}
		private void treeViewHost_GotFocus(object sender, EventArgs e)
		{
			//if (!dropDown.Visible || !treeViewHost.Visible)
			//   dgView.Focus();
			//Debug.WriteLine("___________treeViewHost_GotFocus");
		}
		private void treeViewHost_Enter(object sender, EventArgs e)
		{
			// Debug.WriteLine("___________treeViewHost_Enter");
		}

		public static int HIWORD(int n)
		{
			return (n >> 16) & 0xffff;
		}
		protected override void WndProc(ref Message m)
		{
			// How it was in original sample with using  'static class NativeMethods' (see bottom of this file)
			////if (m.Msg == (NativeMethods.WM_REFLECT + NativeMethods.WM_COMMAND)) {
			////   if (NativeMethods.HIWORD((int)m.WParam) == NativeMethods.CBN_DROPDOWN) {
			////      // Prevent ComboBox from dropping down its list
			////      RecreateHandle();
			////      NativeMethods.SendMessage(Handle, NativeMethods.CB_SHOWDROPDOWN, 0, 0);

			// next two row instead of above sample
			if (m.Msg == (WM_REFLECT + WM_COMMAND)) {
				if (HIWORD((int)m.WParam) == CBN_DROPDOWN) {
					// Show/Hide dropped down based on current state
					if (dropDown != null && !dropDown.Visible)
						ShowDropDown();
					else
						HideDropDown();
					return;
				}
				//if (HIWORD((int)m.WParam) == CBN_DROPDOWN) {
				//   ShowDropDown();
				//   return;
				//}
			}
			base.WndProc(ref m);

		}
		protected override void Dispose(bool disposing)
		{
			// remember to dispose the dropdown as it's not in the control collection. 
			if (disposing) {
				if (dropDown != null) {
					dropDown.Dispose();
					dropDown = null;
				}
			}
			base.Dispose(disposing);
		}
		protected override bool ProcessDialogKey(Keys keyData)
		{
			Keys key = (keyData & Keys.KeyCode);
			Keys ctrl = (keyData & Keys.Modifiers);

			if (key == Keys.Space || (key == Keys.Down && ctrl == Keys.Alt)) {
				this.ShowDropDown();
				return true; // base.ProcessDialogKey(keyData); ;
			}
			return base.ProcessDialogKey(keyData);
		}
	}
	public class DataGridViewTreeViewComboBoxCell : DataGridViewComboBoxCell
	{
		public DataGridViewTreeViewComboBoxCell(): base()
		{
			//base.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
		}

		#region | private fields                           |

		private DataGridViewTreeViewComboBoxEditingControl ctl;
		private TreeView treeview;
		private DataTable dtb;
		private DataGridViewTreeViewComboBoxColumn column;
		
		#endregion

		public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			////// Set the value of the editing control to the current cell value.
			////base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);

			////column = this.OwningColumn as DataGridViewTreeViewComboBoxColumn;
			////ctl = DataGridView.EditingControl as DataGridViewTreeViewComboBoxEditingControl;
			////if (this.DataSource != null) {
			////   treeview = ctl.TreeView;
			////   dtb = this.DataSource as DataTable;
			////   treeview.Nodes.Clear();
			////   AddTreeNodes(dtb.Select(column.TreeParentMember + " is NULL AND Valid=1", "RD DESC"), treeview.Nodes);
			////   if (column.TreeImageList != null)
			////      treeview.ImageList = column.TreeImageList;
			////}


			column = this.OwningColumn as DataGridViewTreeViewComboBoxColumn;
			ctl = DataGridView.EditingControl as DataGridViewTreeViewComboBoxEditingControl;
			if (ctl.SelectedValue == null) {
				if (this.DataSource != null) { //if (this.DataSource != null && ctl.TreeView.Nodes.Count == 0) {
					treeview = ctl.TreeView;
					dtb = this.DataSource as DataTable;
					// 
					if (dtb.GetHashCode() != C.GetInt(treeview.Tag)) {
						treeview.Tag = dtb.GetHashCode();
						treeview.Nodes.Clear();
						AddTreeNodes(dtb.Select(column.TreeParentMember + " is NULL AND Valid=1", "RD"), treeview.Nodes);
						if (column.TreeImageList != null)
							treeview.ImageList = column.TreeImageList;
						Debug.WriteLine("InitializeEditingControl!!!!!!!!!!!!!!!!!!!!!!!");
					}
				}
				base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
			}
			////if (this.Value == null) {
			////   ctl.SelectedValue = null;
			////} else {
			////   foreach (TreeNode n in treeview.Nodes)
			////      n.Collapse();
			////   //if (this.SelectedValue.ToString() == "1") return;
			////   TreeNode[] TN = treeview.Nodes.Find(this.Value.ToString(), true);
			////   if (TN.Length > 0)
			////      treeview.SelectedNode = TN[0];
			////}
			
			//if (this.DataSource != null)
			//   base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
		}
		private void AddTreeNodes(DataRow[] DR, TreeNodeCollection nodes)
		{
			TreeNode n;
			foreach (DataRow dr in DR) {
				n = new TreeNode(dr[column.TreeDisplayMember].ToString());
				n.Tag = dr[column.TreeMember];
				n.Name = dr[column.TreeMember].ToString();
				n.ImageKey = dr[column.TreeTagMember].ToString();
				n.SelectedImageKey = dr[column.TreeTagMember].ToString();
				nodes.Add(n);
				AddTreeNodes(dtb.Select(string.Format("{0}={1} AND Valid=1", column.TreeParentMember, dr[column.TreeMember])), n.Nodes);
			}
		}
		
		#region | public override props                    |

		public override Type EditType
		{
			get { return typeof(DataGridViewTreeViewComboBoxEditingControl); }
		}
		public override Type ValueType
		{
			get { return typeof(int); }
		}
		public override object DefaultNewRowValue
		{
			get { return DBNull.Value; }
		}

		#endregion
		
	}
	public class DataGridViewTreeViewComboBoxColumn : DataGridViewComboBoxColumn
	{
		public DataGridViewTreeViewComboBoxColumn()
		{
			this.CellTemplate = new DataGridViewTreeViewComboBoxCell();
		}

		#region | private fields                           |

		private string treedisplaymember;
		private string treeparentmember;
		private string treemember;
		private string treetagmember;
		private ImageList treeimagelist;

		#endregion

		#region | public props                             |
		
		public string TreeDisplayMember
		{
			get { return treedisplaymember; }
			set { treedisplaymember = value; }
		}
		public string TreeParentMember
		{
			get { return treeparentmember; }
			set { treeparentmember = value; }
		}
		public string TreeMember
		{
			get { return treemember; }
			set { treemember = value; }
		}
		public string TreeTagMember
		{
			get { return treetagmember; }
			set { treetagmember = value; }
		}
		public ImageList TreeImageList
		{
			get { return treeimagelist; }
			set { treeimagelist = value; }
		}

		#endregion

	}
}
