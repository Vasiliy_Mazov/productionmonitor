﻿using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using C1.Win.C1Chart;
using C1.Win.C1FlexGrid;
using ProductionMonitor;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{
	public sealed partial class frmMonitor
	{
		#region | Declare                 |

		private readonly Color _colTranspWhite = Color.FromArgb(160, Color.White);
		private bool _repShorts;
		private bool _repSpeed;
		private float _losBaseTime = 1;
		FlexFreezeBottom _ffbLosses;

		#endregion 

		private void SetTrends()
		{
			if (IsEmpty())
				return;
			_idleCounter = 0;

			// find what column was clicked
			string caption = _grTrends.Cols[_grTrends.ColSel].Name;
			if (!MU.Kpi.NomParts.ContainsKey(caption) && MU.Kpi.NomParts.ContainsKey(MU.Kpi.MainKpiKey))
				caption = MU.Kpi.MainKpiKey;

			// if row or column was changed
			if (_trendCol != caption) {
				_trendCol = caption;

				foreach (Column c in _grLines.Cols)
					if (c.Visible && c.Style != null)
						c.Style.Font = caption == c.Name ? _fontBold : _font;

				foreach (Column c in _grTrends.Cols)
					if (c.Visible && c.Style != null)
						c.Style.Font = caption == c.Name ? _fontBold : _font;

				BuildChartTrends();
			}
			Cursor = Cursors.Default;
		}

		private void BuildGridTrends()
		{
			_grTrends.Redraw = false;

			if (!_lu[L].DataSet.Tables.Contains(MU.TrendMode))
				_lu[L].BindTrendsTables(MU.TrendMode);

			_dvT = _lu[L].DataSet.Tables[MU.TrendMode].DefaultView;

			_dvT.RowFilter = "chl>0";
			//grTrends.BeginInit();
			_grTrends.DataSource = _dvT;

			SetupGrid(_grTrends, false);

			// add 'into grid' chart column
			Column col = _grTrends.Cols.Add();
			col.Name = "chart";
			col.Caption = "Short review";

			//grTrends.EndInit();
			_grTrends.Redraw = true;
		}


		#region | Grid events             |


		private void grTrends_AfterDataRefresh(object sender, System.ComponentModel.ListChangedEventArgs e)
		{
			SetGridTrendsHeight();
			//Debug.WriteLine("grTrends_AfterDataRefresh: " + "TableName is " + dv.Table.TableName);
		}

		private void grTrends_RowColChange(object sender, EventArgs e)
		{
			if (_state == C.State.Idle && _grTrends.ColSel == 0)
				return;
			if (_grTrends.Cols[_grTrends.ColSel].Name != _trendCol) {
				_stackTrends.Clear();
				SetTrends();
			}
		}

		private void grTrends_MouseDown(object sender, MouseEventArgs e)
		{
			HitTestInfo hti = _grTrends.HitTest();
			if (hti.Column == 0 && hti.Row == 0) {
				Rectangle rc = _grTrends.GetCellRect(0, 0);
				if (hti.X > rc.Width - 16 && hti.Y > (rc.Height - 16) / 2) {
					//MessageBox.Show(hti.X.ToString() + "   " + hti.Y.ToString());
					_monitorSettings.grTrendsCollapsed = !_monitorSettings.grTrendsCollapsed;
					SetGridTrendsHeight();

					//grTrends.Select(L + grTrends.Rows.Fixed - 1, 0);
				}
			}
		}


		private void grLines_MouseDown(object sender, MouseEventArgs e)
		{
			HitTestInfo hti = _grLines.HitTest();
			if (hti.Type == HitTestTypeEnum.ColumnHeader) {
				Rectangle rc = _grLines.GetCellRect(0, 0);
				if (hti.X > rc.Width - 16 && hti.Y > (rc.Height - 16) / 2) {
					//MessageBox.Show(hti.X.ToString() + "   " + hti.Y.ToString());
					_monitorSettings.grLinesCollapsed = !_monitorSettings.grLinesCollapsed;
					SetGridLinesHeight();
					//чтобы значок свернуть-развернуть перерисовался вызываем Refresh
					_grLines.Refresh();
				}
			}
		}

		private void grLines_MouseHover(object sender, EventArgs e)
		{
			SetGridLinesHeight();
		}


		private void gr_OwnerDrawCell(object sender, OwnerDrawCellEventArgs e)
		{
			C1FlexGrid gr = sender as C1FlexGrid;

			// Рисуем заголовки колонок и выходим
			if (e.Row < gr.Rows.Fixed) {
				//if (gr.Cols[e.Col].Visible && e.Row == 0 && e.Col == 0 && gr.Rows.Count > 3) {
				if (e.Bounds.X == 0 && gr.Rows.Count > 2) {
					Rectangle rec = e.Bounds;
					//rec.Inflate(-1, -1);
					//rec.Offset(-1, -1);

					rec.Width = 16;
					rec.Height = 16;
					e.DrawCell(DrawCellFlags.All);

					//e.Graphics.DrawRectangle(Pens.Black, rec);
					Image im = (gr == _grLines ? _monitorSettings.grLinesCollapsed : _monitorSettings.grTrendsCollapsed) ? Res.hide_down : Res.hide_up;
					e.Graphics.DrawImage(im, new Point(e.Bounds.Width - 16, (e.Bounds.Height - 16) / 2));
				}
				return;
			}

			string colname = gr.Cols[e.Col].Name;
			if (colname == "")
				return;

			e.DrawCell(DrawCellFlags.Background);

			#region | In cell charts handling |

			if (gr.Cols[e.Col].Name == "chart" && gr.DataSource != null && gr.Cols.Contains("TF")) {
				CurrencyManager cm = (CurrencyManager)BindingContext[gr.DataSource, gr.DataMember];
				DataRowView drv = cm.List[e.Row - gr.Rows.Fixed] as DataRowView;

				double fulltime, value;
				int shift = 0, width;
				//if (double.TryParse(gr.GetDataDisplay(e.Row, "TF"), out fulltime) && fulltime > 0) {
				fulltime = C.GetFloat((drv["TF"]));
				if (fulltime > 0) {
					// calculate bar extent
					Rectangle rc = e.Bounds;

					//rc.Inflate(0, -1);
					//rc.Offset(0, 1);
					if (use_steps_chart_in_grid_cell) rc.Height -= (MU.Kpi.Colors.Count);
					width = rc.Width;

					rc.Offset(0, MU.Kpi.Colors.Count - 1);

					//// draw background
					e.DrawCell(DrawCellFlags.Background | DrawCellFlags.Border);

					foreach (string key in MU.Kpi.FullTimeParts) {
						//double.TryParse(gr.GetDataDisplay(e.Row, key), out value);
						value = Convert.ToDouble(drv[key]);
						//rc.Offset(shift, 1);
						shift = (int)Math.Round((width * value / fulltime));
						rc.Width = shift;
						e.Graphics.FillRectangle(MU.Kpi.Brushs[key], rc);
						if (use_steps_chart_in_grid_cell)
							rc.Offset(shift, -1);
						else
							rc.Offset(shift, 0);
					}
					//// draw background
					//e.DrawCell(DrawCellFlags.Border);

					////// draw cell content
					////e.DrawCell(DrawCellFlags.Content);
				}
			}

			#endregion | In cell charts handling |

			if (MU.Kpi == null)
				return;

			if (MU.Kpi.Measure.ContainsKey(colname) && MU.Kpi.Measure[colname] == 1)
				e.Text = Utils.GetTimeSpanString(e.Text);

			if (MU.Kpi.Measure.ContainsKey(colname) && MU.Kpi.Measure[colname] == 2)
				e.Text = Utils.GetFloatString(e.Text);

			#region | Row captions styles     |

			if (gr == _grLines && colname == "Line") {
				int r = e.Row - gr.Rows.Fixed;
				if (r >= _lu.Count)
					return;
				if (r == L && _lu[r].Error != "") {
					e.Style = gr.Styles["RowBoldCaptionError"]; //RowBoldCaption
				} else if (r == L) {
					e.Style = gr.Styles["RowBoldCaption"]; //RowBoldCaption
				} else if (_lu[r].Error == "") {
					e.Style = gr.Styles.Normal; //RowBoldCaption
				} else if (_lu[r].Error.StartsWith("-2:")) {
					e.Style = gr.Styles["FatalError"];
				} else if (_lu[r].Error != "") {
					e.Style = gr.Styles["Error"];
				}

				////// Либо так
				////e.Text = new string('.', lu[r].Level * 3) + e.Text;

				// Либо так
				Rectangle rect = e.Bounds;
				rect.X += _lu[r].ReducedLevel * 10;
				rect.Width -= _lu[r].ReducedLevel * 10;
				rect.Y += 1;
				//StringFormat sf = new StringFormat(StringFormatFlags.LineLimit);
				//sf.Trimming = StringTrimming.EllipsisWord;

				e.Graphics.DrawString(e.Text, e.Style.Font, new SolidBrush(e.Style.ForeColor), rect);
			} else if (colname.StartsWith("t_")) {
				e.Style = gr.Styles["Target"];
				e.Graphics.DrawString(e.Text, e.Style.Font, new SolidBrush(e.Style.ForeColor), e.Bounds, new StringFormat() { Alignment = StringAlignment.Far });
			} else
				e.DrawCell(DrawCellFlags.Content);

			#endregion | Row captions styles     |

			// draw background
			//e.DrawCell(DrawCellFlags.Border);
			if ((colname.StartsWith("t") || colname.StartsWith("k")) && MU.Kpi.NomParts.ContainsKey(colname) && MU.Kpi.Colors.ContainsKey(MU.Kpi.NomParts[colname])) {
				Rectangle rc = e.Bounds;
				rc.Inflate(-1, -1);
				rc.Offset(-1, -1);
				e.Graphics.DrawRectangle(MU.Kpi.Pens[MU.Kpi.NomParts[colname]], rc);
			} else if (MU.Kpi.Colors.ContainsKey(colname)) {
				Rectangle rc = e.Bounds;
				rc.Inflate(-1, -1);
				rc.Offset(-1, -1);
				e.Graphics.DrawRectangle(MU.Kpi.Pens[colname], rc);
			} else
				e.DrawCell(DrawCellFlags.Border);
		}

		private void gr_AfterDragColumn(object sender, DragRowColEventArgs e)
		{
			C1FlexGrid gr = sender as C1FlexGrid;
			////if (gr == null) return;
			string colname = gr == _grLines ? "narrow" : "wide";

			DataTable dtb = Utils.GetDataTable(sender);
			if (dtb == null) return;

			DataTable dtbcols = _dsCols.Tables[colname];
			DataRow[] DR;
			foreach (Column col in gr.Cols) {
				if (col.Name == "") continue;
				DR = dtbcols.Select(String.Format("Name='{0}'", col.Name));
				if (DR.Length > 0)
					DR[0]["SortOrder"] = col.Index;
			}
		}

		private void gr_AfterResizeColumn(object sender, RowColEventArgs e)
		{
			C1FlexGrid gr = sender as C1FlexGrid;
			if (gr == null) return;
			string colname = gr == _grLines ? "narrow" : "wide";

			DataTable dtb = Utils.GetDataTable(sender);
			if (dtb == null) return;

			DataTable dtbcols = _dsCols.Tables[colname];
			if (dtbcols == null)
				return;
			string id;
			foreach (DataRow r in dtbcols.Rows) {
				id = r["Name"].ToString();
				if (id == gr.Cols[e.Col].Name)
					r["Width"] = gr.Cols[id].Width;
			}
		}

		private void gr_MouseMove(object sender, MouseEventArgs e)
		{
			string tip;
			C1FlexGrid gr = sender as C1FlexGrid;

			// просто использовать gr.MouseCol нельзя,
			// т.к. иногда успевает смениться внутри процедуры!!!
			int col = gr.MouseCol;
			if (col < 0 || gr.Redraw == false)
				return;

			tip = (string)gr.Cols[col].UserData;
			string colname = gr.Cols[col].Name;

			if (MU.Kpi != null && MU.Kpi.Names.ContainsKey(colname)) {
				tip = MU.Kpi.Names[colname];
				if (MU.Kpi.Measure.ContainsKey(colname)) {
					tip += ", ";   // Environment.NewLine;
					switch (MU.Kpi.Measure[colname]) {
						case 1: tip += TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours; break;
						case 2: tip += "%"; break;
						case 3: tip += Res.strUnits; break;
						case 4: tip += Res.strMeasure; break;
					}
				}
				if (MU.Kpi.Denominators.ContainsKey(colname)) {
					if (MU.Kpi.Nominators.ContainsKey(colname))
						tip += String.Format("{0} = {1} / {2}", Environment.NewLine, MU.Kpi.Names[MU.Kpi.Nominators[colname]], MU.Kpi.Names[MU.Kpi.Denominators[colname]]);
					else if (MU.Kpi.Names.ContainsKey(MU.Kpi.Denominators[colname]))
						tip += String.Format("{0} = {1} / {2}", Environment.NewLine, MU.Kpi.Names[colname], MU.Kpi.Names[MU.Kpi.Denominators[colname]]);
				}
				Tt.SetToolTip(e.X, e.Y, gr, tip); //Tt.SetToolTip(gr, tip);
			} else
				Tt.SetToolTip(); //Tt.SetToolTip(gr, "");
		}

		private void gr_MouseLeave(object sender, EventArgs e)
		{
			C1FlexGrid gr = sender as C1FlexGrid;
			gr.Cursor = Cursors.Default;
			Tt.SetToolTip();
			if (sender == _grLines) {
				SetGridLinesHeight();
				_grLines.ShowCell(L + _grLines.Rows.Fixed, _grLines.Col);
			}
		}


		private void grLosses_OwnerDrawCell(object sender, OwnerDrawCellEventArgs e)
		{
			C1FlexGrid gr = sender as C1FlexGrid;
			if (e.Row < gr.Rows.Fixed) return;

			string colname = gr.Cols[e.Col].Name;
			if (e.Text == "0.0 %" || e.Text == "0,0 %")
				e.Text = "";

			float val = 0;
			if (float.TryParse(e.Text, out val)) {
				if (val == 0) 
					e.Text = "";
				else if (colname == "Events")
					e.Text = val.ToString();
				else if (colname == "Time" || colname.StartsWith("MT"))
					e.Text = Utils.GetTimeSpanString(val);
				else 
					e.Text = Utils.GetFloatString(val * 100);
			}
		}

		private void grLosses_RowColChange(object sender, EventArgs e)
		{
			string name = _grLosses.Cols[_grLosses.ColSel].Name;
			// если колонка не та, то выходим
			if (_grLosses.ColSel <= 2 || name == "" || name == "Name") return;

			if (name == "Time") name = "Perc";

			BuildChartLossess(name);

			string h = _chLosses.Header.Text;
			int pos = h.LastIndexOf(',');
			if (pos > 0)
				_chLosses.Header.Text = h.Substring(0, pos);
			if (name != "Perc")
				_chLosses.Header.Text = String.Format("{0}, {1}", _chLosses.Header.Text, _grLosses.Cols[_grLosses.ColSel].Caption);

			foreach (ChartDataSeries ser in _chMarks.ChartGroups[0].ChartData.SeriesList) {
				if (ser.Tag.ToString() == name) {
					MarksSelect(name);
					return;
				}
			}
			MarksSelect("Perc");
		}

		private void grLosses_BeforeRowColChange(object sender, RangeEventArgs e)
		{
			if (_grLosses.Cols[e.NewRange.c1].Name == "Name")
				e.Cancel = true;
		}


		#endregion | Grid events             |


		#region | tabTrends toolstrip     |

		private void tsbTrendsMode_Click(object sender, EventArgs e)
		{
			ToolStripButton tsb = sender as ToolStripButton;
			if (tsb.Checked || _state != C.State.Idle) return;

			if (tsb != tsbTrendsDays) tsbTrendsDays.Checked = false;
			if (tsb != tsbTrendsWeek) tsbTrendsWeek.Checked = false;
			if (tsb != tsbTrendsMonths) tsbTrendsMonths.Checked = false;
			if (tsb != tsbTrendsQuarters) tsbTrendsQuarters.Checked = false;
			if (tsb != tsbTrendsYears) tsbTrendsYears.Checked = false;
			if (tsb != tsbTrendsBrands) tsbTrendsBrands.Checked = false;
			if (tsb != tsbTrendsFormats) tsbTrendsFormats.Checked = false;
			if (tsb != tsbTrendsShifts) tsbTrendsShifts.Checked = false;
			tsb.Checked = true;

			SetCursWait();

			if (tsb == tsbTrendsDays) {
				MU.TrendMode = "DayList";
				_lu[L].BindTrendsTables(MU.TrendMode);
			} else if (tsb == tsbTrendsWeek) {
				MU.TrendMode = "WeekList";
				_lu[L].BindTrendsTables(MU.TrendMode);
			} else if (tsb == tsbTrendsMonths) {
				MU.TrendMode = "MonthList";
				_lu[L].BindTrendsTables(MU.TrendMode);
			} else if (tsb == tsbTrendsQuarters) {
				MU.TrendMode = "QuarterList";
				_lu[L].BindTrendsTables(MU.TrendMode);
			} else if (tsb == tsbTrendsYears) {
				MU.TrendMode = "YearList";
				_lu[L].BindTrendsTables(MU.TrendMode);
			} else if (tsb == tsbTrendsBrands) {
				MU.TrendMode = "BrandName";
			} else if (tsb == tsbTrendsFormats) {
				MU.TrendMode = "FormatName";
			} else if (tsb == tsbTrendsShifts) {
				MU.TrendMode = "ShiftName";
			}

			if ((tsbDetDwDate.Checked || tsbDetDw.Checked) && MU.TrendMode.Contains("List")) {
				Utils.SetupColumns(_grDet, _gridColumns["Stops"]);
				LoadGridDetailGroups();
			}

			BuildGridTrends();
			BuildChartTrends();

			SetCursDefault();
		}

		private void tsbDetDw_Click(object sender, EventArgs e)
		{
			ToolStripButton tsb = sender as ToolStripButton;
			if (tsb.Checked) return;

			if (tsb != tsbDetDwDate) tsbDetDwDate.Checked = false;
			if (tsb != tsbDetDw) tsbDetDw.Checked = false;
			if (tsb != tsbDetProdExt) tsbDetProdExt.Checked = false;
			if (tsb != tsbDetProdShort) tsbDetProdShort.Checked = false;
			if (tsb != tsbDetFromTo) tsbDetFromTo.Checked = false;
			if (tsb != tsbDetFromToPvt) tsbDetFromToPvt.Checked = false;
			tsb.Checked = true;

			LoadGridDetail();
		}

		#endregion | tabTrends toolstrip     |


		#region | Chart Trends            |

		private void BuildChartTrends()
		{
			if (MU.Mins < MU.minTrendMins) return;

			// Замораживаем внешний вид
			_chTrends.BeginUpdate();

			// Очищаем график от предыдущих данных
			_chTrends.ChartArea.AxisX.ValueLabels.Clear();
			_chTrends.ChartLabels.LabelsCollection.Clear();
			_chTrends.ChartGroups[0].ChartData.SeriesList.Clear();
			_chTrends.ChartGroups[1].ChartData.SeriesList.Clear();
			if (_stackTrends.Count > 0 && _stackTrends.Peek() != "ttS") 
				_stackTrends.Clear();

			int dim = _dvT.Count;
			int dimcnt = dim;
			int measure = MU.Kpi.Measure.ContainsKey(this._trendCol) ? MU.Kpi.Measure[this._trendCol] : 2;
			int dimext = (!MU.Kpi.Measure.ContainsKey(this._trendCol) || MU.Kpi.Measure[this._trendCol] == 2) ? dim + 1 : dim;
			float[] y = new float[dim + 3];
			float[] x = new float[dim + 3];
			double MinY = 0, MaxY = 0;
			float[] max_y = new float[dim + 3];
			int k = 0;
			float min_x = -0.5f;
			float max_x = dim != dimext ? dimext + 0.5F : dim - 0.5F;
			Color series_color;

			// если всего один столбик, то надо его "заузить"
			if (dim == 1 && dim == dimext) { //dim == 1
				min_x = -2;
				max_x = 2;
			}
			string lastkey = "";

			_chTrends.ChartArea.AxisY.AutoMax = false;
			_chTrends.ChartArea.AxisY.AutoMin = false;
			_chTrends.ChartArea.AxisY2.AutoMax = false;
			_chTrends.ChartArea.AxisY2.AutoMin = false;

			DataRowView drv;
			ChartDataSeries ChartDat = new ChartDataSeries();
			DateTime tm1 = MU.DtStart.Date.AddDays(-MU.DtStart.Date.DayOfYear + 1).AddYears(-1);
			string _trendCol = _stackTrends.Count > 0 && _stackTrends.Peek() == "ttS" ? "ttS" : this._trendCol;
			//string _trendCol = this._trendCol;
			
			if (show_trends_chart_with_parts && _trendCol.StartsWith("K")) {
				#region | Главные KPI  в процентах |

				for (int i = 0; i < dim; i++) {
					drv = _dvT[i];
					foreach (string k0 in MU.Kpi.NomParts[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None))
						y[i] += C.GetFloat(drv[k0]);
					y[i] = y[i] / GetTimeForTrends(drv);
					max_y[i] += y[i];
					if (MinY > y[i]) MinY = y[i];
					x[i] = i;
					// основные лейблы
					if (y[i] != 0) AddValueLabel(_chTrends, Utils.GetFloatString(y[i]), 0, i, true);
				}

				// итоговые значения
				if (dim != dimext) {
					// среднее значение
					DataRow dr = MU.Total.Rows[L];
					foreach (string k0 in MU.Kpi.NomParts[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None))
						y[dimcnt] += C.GetFloat(dr[k0]);
					y[dimcnt] = y[dimcnt] / GetTimeForTrends(dr);
					x[dimcnt] = dimcnt + 1;
					// основные лейблы
					AddValueLabel(_chTrends, Utils.GetFloatString(y[dim]), 0, dim, true);

					// предыдущий год
					dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
					if (dr != null) {
						max_x++;
						dimcnt++;
						foreach (string k0 in MU.Kpi.NomParts[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None))
							y[dimcnt] += C.GetFloat(dr[k0]);
						y[dimcnt] = y[dimcnt] / GetTimeForTrends(dr);
						x[dimcnt] = dimcnt + 1;
						// основной лейбл
						AddValueLabel(_chTrends, Utils.GetFloatString(y[dimcnt]), 0, dimcnt, true);
						// лейбл по оси
						AddTrendsAxisLabel(_chTrends, tm1.Year.ToString(), dimcnt, MinY, dimext, true);
					}

					// текущий год
					dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
					if (dr != null) {
						max_x++;
						dimcnt++;
						foreach (string k0 in MU.Kpi.NomParts[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None))
							y[dimcnt] += C.GetFloat(dr[k0]);
						y[dimcnt] = y[dimcnt] / GetTimeForTrends(dr);
						x[dimcnt] = dimcnt + 1;
						// основной лейбл
						AddValueLabel(_chTrends, Utils.GetFloatString(y[dimcnt]), 0, dimcnt, true);
						// лейбл по оси
						AddTrendsAxisLabel(_chTrends, tm1.AddYears(1).Year.ToString(), dimcnt, MinY, dimext, true);
					}
				}

				ChartDat = new ChartDataSeries();
				ChartDat.LineStyle.Color = _colRunLight;
				_chTrends.Header.Style.BackColor = ChartDat.LineStyle.Color;
				_chTrends.Header.Style.ForeColor = Color.Black;

				ChartDat.Y.CopyDataIn(y);
				ChartDat.X.CopyDataIn(x);
				ChartDat.Tag = "end";
				ChartDat.Label = MU.Kpi.Names[_trendCol];

				_chTrends.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

				k = 1;
				foreach (string key in MU.Kpi.DenomPartsDiff[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None)) {
					series_color = MU.Kpi.Colors.ContainsKey(key) ? MU.Kpi.Colors[key] : Color.Azure;
					for (int i = 0; i < dim; i++) {
						drv = _dvT[i];
						y[i] = C.GetFloat(drv[key]) / GetTimeForTrends(drv);
						max_y[i] += y[i];
						if (MinY > y[i]) MinY = y[i];
						x[i] = i;
					}

					// итоговые значения
					if (dim != dimext) {
						dimcnt = dim;
						// среднее значение
						DataRow dr = MU.Total.Rows[L];
						y[dim] = C.GetFloat(dr[key]) / GetTimeForTrends(dr);
						if (MU.Kpi.Measure[_trendCol] != 2)
							y[dim] = y[dim] / dim;
						x[dim] = dim + 1;

						// предыдущий год
						dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
						if (dr != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(dr[key]) / GetTimeForTrends(dr);
							x[dimcnt] = dimcnt + 1;
						}
						// текущий год
						dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
						if (dr != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(dr[key]) / GetTimeForTrends(dr);
							x[dimcnt] = dimcnt + 1;
						}
					}

					ChartDat = new ChartDataSeries();
					ChartDat.LineStyle.Color = series_color;
					ChartDat.Y.CopyDataIn(y);
					ChartDat.X.CopyDataIn(x);
					ChartDat.Label = MU.Kpi.Names[key];
					ChartDat.Tag = _lu[L].DataSet.Tables["Gaps"].Columns.Contains(key) ? key : "end";

					_chTrends.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
					k++;
				}

				// get values for manual Min & Max values for Y axis
				if (k > 1) MinY = 0;
				foreach (double d in max_y)
					if (MaxY < d)
						MaxY = d;

				// если только один график то меняем название
				if (k == 1 && MU.Kpi.Names.ContainsKey(_trendCol)) {
					ChartDat.Label = MU.Kpi.Names[_trendCol];
					ChartDat.Tag = "end";
				}

				#endregion | Главные KPI  в процентах |
			} else if (MU.Kpi.NomParts.ContainsKey(_trendCol)) {
				#region | Для всех остальных KPI   |

				string[] parts = MU.Kpi.NomParts[_trendCol].Split(new string[] { ";" }, StringSplitOptions.None);
				MaxY = C.GetFloat(_dvT.Table.Compute(string.Format("MAX({0})", _trendCol), ""));
				if (MU.Kpi.Measure[_trendCol] == 1 && TmStyle != TimeStyle.MMM) MaxY = MaxY / 60f;
				if (MU.Kpi.Measure[_trendCol] == 2) max_x += 2;

				// предыдущий год
				DataRow drPrev = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
				if (drPrev != null && MU.Kpi.Measure[this._trendCol] == 2) max_x++;
				DataRow drCurr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
				if (drCurr != null && MU.Kpi.Measure[this._trendCol] == 2) max_x++;

				foreach (string key in parts) {
					lastkey = key;
					series_color = MU.Kpi.Colors.ContainsKey(key) ? MU.Kpi.Colors[key] : Color.Azure;

					// основные данные
					for (int i = 0; i < dim; i++) {
						drv = _dvT[i];
						y[i] = C.GetFloat((float)drv[key] / GetTimeForTrends(drv));
						max_y[i] += y[i];
						if (MinY > y[i]) MinY = y[i];
						x[i] = i;
					}

					// итоговые значения
					if (dim != dimext) {
						dimcnt = dim;

						// среднее значение
						DataRow dr = MU.Total.Rows[L];
						y[dim] = C.GetFloat(dr[key]) / GetTimeForTrends(dr);
						max_y[dim] += y[dim];
						//if (MU.Kpi.Measure[_trendCol] != 2)
						//   y[dim] = y[dim] / dim;
						x[dim] = dim + 1;

						// предыдущий год
						if (drPrev != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(drPrev[key]) / GetTimeForTrends(drPrev);
							max_y[dimcnt] += y[dimcnt];
							x[dimcnt] = dimcnt + 1;
							// лейбл по оси
							AddTrendsAxisLabel(_chTrends, tm1.Year.ToString(), dimcnt, MinY, dimext, true);
						}

						// текущий год
						if (drCurr != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(drCurr[key]) / GetTimeForTrends(drCurr);
							max_y[dimcnt] += y[dimcnt];
							x[dimcnt] = dimcnt + 1;
							// лейбл по оси
							AddTrendsAxisLabel(_chTrends, tm1.AddYears(1).Year.ToString(), dimcnt, MinY, dimext, true);
						}
					}

					ChartDat = new ChartDataSeries();
					ChartDat.LineStyle.Color = MU.Kpi.Colors.ContainsKey(key) ? MU.Kpi.Colors[key] : _colRunLight;
					ChartDat.Y.CopyDataIn(y);
					ChartDat.X.CopyDataIn(x);
					ChartDat.Tag = _lu[L].DataSet.Tables["Gaps"].Columns.Contains(key) ? key : "end";
					ChartDat.Label = MU.Kpi.Names[key];
					_chTrends.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

					k++;
				}

				// основные лейблы
				int seriesamount = k - 1;
				k = 0;
				foreach (float f in y) {
					if (max_y[k] != 0 && !float.IsNaN(max_y[k]))
						AddValueLabel(_chTrends, GetTrendValueStringLabel(max_y[k]), seriesamount, k, false);
					k++;
				}

				MaxY = 0;
				foreach (double d in max_y)
					if (MaxY < d)
						MaxY = d;

				// если только один график то меняем название
				if (k == 1 && MU.Kpi.Names.ContainsKey(_trendCol)) {
					ChartDat.Label = MU.Kpi.Names[_trendCol];
					ChartDat.Tag = "end";
					_chTrends.Header.Style.BackColor = ChartDat.LineStyle.Color;
					if (MU.Kpi.Colorsfont.ContainsKey(lastkey)) _chTrends.Header.Style.ForeColor = MU.Kpi.Colorsfont[lastkey];
					_chTrends.Header.Style.Border.Color = Color.Black;
				} else {
					_chTrends.Header.Style.BackColor = _chTrends.BackColor;
					_chTrends.Header.Style.ForeColor = Color.Black;
				}

				#endregion | Для всех остальных KPI   |
			}

			#region | Пустой график для целей |

			DataRow[] DR;
			ChartDat = null;
			if (_lu[L].DataSet.Tables.Contains("Targets") && MU.Kpi.ID.ContainsKey(_trendCol)) {
				DR = _lu[L].DataSet.Tables["Targets"].Select("KpiID=" + MU.Kpi.ID[_trendCol], "FromDate");
				if (DR.Length > 0) {
					ChartDat = new ChartDataSeries();
					ChartDat.LineStyle.Color = Color.Red;
					ChartDat.LineStyle.Thickness = 2;
					ChartDat.SymbolStyle.Shape = SymbolShapeEnum.None;
					//chTrends.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);

					ChartDat.PointData.Clear();
					ChartDat.Label = "Target "; // +target.ToString();
				}
			}

			#endregion | Пустой график для целей |

			#region | Лейблы по оси и цели    |

			int lb;
			_chTrends.ChartArea.AxisX.ValueLabels.Clear();
			DateTime dt;
			float tgt = 0;
			for (int i = 0; i < dim; i++) {
				//if (dim > 60 && i % 2 == 1) continue;
				string label = _dvT[i][MU.TrendMode.EndsWith("List") ? MU.TrendMode : "Name"].ToString();

				#region | Ищем цели |

				if (ChartDat != null && _dvT.Table.Columns.Contains("mindt")) {
					if (MU.UseTimecorrection)
						dt = Convert.ToDateTime(_dvT[i]["mindt"]).AddMinutes(-_lu[L].CorrectionTime);
					else
						dt = Convert.ToDateTime(_dvT[i]["mindt"]);
					tgt = C.GetFloat(C.GetRowField(_lu[L].DataSet.Tables["Targets"],
						string.Format("KpiID={0} AND FromDate<=#{1:M/d/yy H:m}#", MU.Kpi.ID[_trendCol], dt), "FromDate DESC", "Target"));
					if (measure == 1 && TmStyle != TimeStyle.MMM)
						tgt = tgt / 60f;

					ChartDat.PointData.Add(new PointF(i - 0.5f, tgt));
					if (MaxY <= tgt)
						MaxY = tgt;
				}

				#endregion | Ищем цели |

				// добавляем стандартный лейбл но делаем его прозрачным т.к. криво реализован и нужен только для места
				lb = _chTrends.ChartArea.AxisX.ValueLabels.Add(i, label);
				_chTrends.ChartArea.AxisX.ValueLabels[lb].Color = Color.Transparent;

				// добавляем нестандартный лейбл который и показываем
				AddTrendsAxisLabel(_chTrends, label, i, MinY, dimext, false);
			}

			if (dimext > 15) _chTrends.ChartArea.AxisX.AnnotationRotation = 90;
			else _chTrends.ChartArea.AxisX.AnnotationRotation = 0;

			#endregion | Лейблы по оси и цели    |

			_chTrends.ChartArea.AxisX.Min = min_x;
			_chTrends.ChartArea.AxisX.Max = max_x;
			_chTrends.ChartArea.AxisY.Min = MinY;
			if (MinY == MaxY) MaxY += 1;
			_chTrends.ChartArea.AxisY.Max = MaxY * 1.02;

			#region | Визуализируем цели      |

			if (ChartDat != null) {
				_chTrends.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
				if (ChartDat.PointData.Length > 0)
					ChartDat.PointData.Add(new PointF(dimext + 2.5f, tgt));
				_chTrends.ChartArea.AxisY2.Min = MinY;
				_chTrends.ChartArea.AxisY2.Max = MaxY * 1.02;
			}

			#endregion | Визуализируем цели      |

			// добавляем среднее значение последним значением
			if (dim != dimext)
				AddTrendsAxisLabel(_chTrends, Res.strAverage, dim, MinY, dimext, true);

			#region | размерность по оси Y    |

			switch (measure) {
				case 1:	// время
					_chTrends.ChartArea.AxisY.Text = TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours;
					break;
				case 2:	// проценты
					_chTrends.ChartArea.AxisY.Text = Res.strPercents;
					break;
				case 3:	// штуки
					_chTrends.ChartArea.AxisY.Text = Res.strUnits;
					break;
				case 4:	// объем
					_chTrends.ChartArea.AxisY.Text = "Hl";
					break;
				default:
					_chTrends.ChartArea.AxisY.Text = "";
					break;
			}

			#endregion | размерность по оси Y    |

			string header = MU.Kpi.Names.ContainsKey(_trendCol) ? "  :  " + MU.Kpi.Names[_trendCol] : "";
			_chTrends.Header.Text = string.Format(" {0}{1} ", _lu[L].GetLongUnitName(false, " \\ "), header);

			// Размораживаем внешний вид
			// Если в результате Exception код EndUpdate() не будет выполнен то график в дальнейшем
			// не будет корректно обновляться на экране, т.е. BeginUpdate() не должен вызываться чаще чем EndUpdate()
			// По правильному надо всю процедуру заключить в try-catch чтобы вызывать EndUpdate()
			// в блоке finally, или, вызывать EndUpdate() для страховки дважды:
			_chTrends.EndUpdate();
			_chTrends.EndUpdate();

			// the tag is used for color (while mouse interact)
			_grTrends.Tag = null;

			SetupTrendLabels();
		}

		private void BuildChartTrendsParts()
		{
			string header = "";
			if (_grTrends.Tag == null)
				return;
			Color c = (Color)_grTrends.Tag;

			int colorcounter = 0;
			Color cl = Color.White;
			//chCompare.ChartLabels.LabelsCollection.Clear();

			C1.Win.C1Chart.Label lb;
			for (int l = _chTrends.ChartLabels.LabelsCollection.Count - 1; l >= 0; l--) {
				lb = _chTrends.ChartLabels.LabelsCollection[l];
				// убираем всё кроме подписей на оси
				if (lb.AttachMethod != AttachMethodEnum.DataIndexY)
					_chTrends.ChartLabels.LabelsCollection.Remove(lb);
			}

			bool thereisvalue = false;

			int dim = _dvT.Count, dimcnt;
			int dimext = (!MU.Kpi.Measure.ContainsKey(_trendCol) || MU.Kpi.Measure[_trendCol] == 2) ? dim + 3 : dim;

			DataRow[] DR;
			float[] y = new float[dim + 3];
			float[] x = new float[dim + 3];
			float[] sum_y = new float[dim + 3];
			float min_y = 0;
			_chTrends.ChartGroups[0].ChartData.SeriesList.Clear();
			_chTrends.ChartGroups[1].ChartData.SeriesList.Clear();
			int k;
			string flt = "", nextflt = "", legend;
			string[] trendsflt = new string[_dvT.Count];
			for (k = 0; k < dim; k++) {
				trendsflt[k] = _dvT[k]["flt"].ToString().Replace("!", "'");
				x[k] = k;
			}

			DataRow dryear;
			DateTime tm1 = MU.DtStart.Date.AddDays(-MU.DtStart.Date.DayOfYear + 1).AddYears(-1);
			if (_stackTrends.Peek() != "ttS" && (_stackTrends.Peek().StartsWith("tt") || _stackTrends.Peek().Length > 3)) {
				#region | parts mode    |

				string column = "";
				int ncolumn = -1, downid = 0, downidnext = 0;
				foreach (string col in _lu[L].DownColumns.Values) {
					if (_stackTrends.Peek().Contains(col)) {
						column = col;
						string[] cols = _stackTrends.Peek().Split(new char[] { 'D', '=' });
						int.TryParse(cols[1], out ncolumn);
						int.TryParse(cols[2], out downid);
						if (downid != 0)
							header = Utils.GetRowField(_tbStopList, _stackTrends.Peek(), "", "FullName");
						break;
					}
				}
				if (ncolumn == -1) {
					if (_stackTrends.Peek().StartsWith("tt")) {
						ncolumn = 0;
						downid = MU.Kpi.ID[_stackTrends.Peek()];
						flt = "D0=" + downid;
						header = Utils.GetRowField(_tbStopList, String.Format("DownID={0}", downid), "", "FullName");
					} else
						flt = String.Format("RD='{0}'", _stackTrends.Peek());
				} else
					flt = _stackTrends.Peek();

				if (_lu[L].DownColumns.ContainsKey(ncolumn + 1) || _lu[L].DownColumns.ContainsKey(ncolumn)) {
					//column = lu[L].DownColumns[ncolumn + 1];
					column = _lu[L].DownColumns.ContainsKey(ncolumn + 1) ? _lu[L].DownColumns[ncolumn + 1] : _lu[L].DownColumns[ncolumn];

					DataView dtv = new DataView(_tbStopList, flt + " AND TD>0", "", DataViewRowState.CurrentRows);
					DR = dtv.ToTable(true, new string[] { column }).Select();
					foreach (DataRow dr in DR) {
						//colorcounter++;
						thereisvalue = false;
						downidnext = dr[column] == DBNull.Value ? downid : (int)dr[column];
						nextflt = dr[column] == DBNull.Value ? "DownID=" + downid : String.Format("{0}={1}", column, dr[column]);
						for (k = 0; k < dim; k++) {
							flt = String.Format("{0} AND {1}", nextflt, trendsflt[k]);
							y[k] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute("sum(TD)", flt)) / GetTimeForTrends(_dvT[k]); //flt + " AND TF<" + lu[L].MinIdleTime
							sum_y[k] += y[k];
							if (!thereisvalue && !float.IsNaN(y[k]) && y[k] != 0) {
								thereisvalue = true;
								colorcounter++;
								//cl = Color.FromArgb(255 * colorcounter / DR.Length, c);
								cl = GetColorTrends(colorcounter, DR.Length, c);
							}
						}

						// подсчет средних значений
						if (dim != dimext) {
							dimcnt = dim;
							y[dim] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute("sum(TD)", nextflt)) / GetTimeForTrends(MU.Total.Rows[L]); //flt + " AND TF<" + lu[L].MinIdleTime;
							if (MU.Kpi.Measure[_trendCol] != 2)
								y[dim] = y[dim] / dim;
							x[dim] = dim + 1;
							sum_y[dim] += y[dim];

							if (_lu[L].YearDataSetIsLoaded) {
								// предыдущий год
								dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
								if (dryear != null) {
									dimcnt++;
									y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute("sum(TD)", String.Format("{0} AND {1}", nextflt, dryear["Line"]))) / GetTimeForTrends(dryear);
									x[dimcnt] = dimcnt + 1;
									sum_y[dimcnt] += y[dimcnt];
								}
								// текущий год
								dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
								if (dryear != null) {
									dimcnt++;
									y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute("sum(TD)", String.Format("{0} AND {1}", nextflt, dryear["Line"]))) / GetTimeForTrends(dryear);
									x[dimcnt] = dimcnt + 1;
									sum_y[dimcnt] += y[dimcnt];
								}
							}
						}

						legend = dr[column] == DBNull.Value ? downid.ToString() : dr[column].ToString();
						if (dr[column] == DBNull.Value) nextflt = "end";
						if (thereisvalue) {
							GetChartTrendsSeries(cl, y, nextflt, Utils.GetRowField(_tbStopList, "DownID=" + legend, "", "Name"));
						}
					}
					dtv.Dispose();
					// Проверка нет ли в составе коротких остановов
					string RD = C.GetRowField(_lu[L].DataSet.Tables["StopList"], String.Format("DownID={0}", downid), "RD");
					if (_lu[L].DataSet.Tables["Gaps"].Columns.Contains(RD) &&
						_lu[L].DataSet.Tables["Gaps"].Columns[RD].Expression.Contains("+tSh") &&
						column == "D1") {
						for (k = 0; k < dim; k++) {
							y[k] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute("sum(tSh)", trendsflt[k])) / GetTimeForTrends(_dvT[k]);
							if (min_y > y[k]) min_y = y[k];
							sum_y[k] += y[k];
						}
						if (dim != dimext) {
							dimcnt = dim;
							y[dim] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute("sum(tSh)", "")) / GetTimeForTrends(MU.Total.Rows[L]);
							x[dim] = dim + 1;
							sum_y[dim] += y[dim];

							if (_lu[L].YearDataSetIsLoaded) {
								// предыдущий год
								dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
								if (dryear != null) {
									dimcnt++;
									y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute("sum(tSh)", dryear["Line"].ToString())) / GetTimeForTrends(dryear);
									x[dimcnt] = dimcnt + 1;
									sum_y[dimcnt] += y[dimcnt];
								}
								// текущий год
								dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
								if (dryear != null) {
									dimcnt++;
									y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute("sum(tSh)", dryear["Line"].ToString())) / GetTimeForTrends(dryear);
									x[dimcnt] = dimcnt + 1;
									sum_y[dimcnt] += y[dimcnt];
								}
							}
						}
						if (MU.Kpi.Colors.ContainsKey("tSh")) c = MU.Kpi.Colors["tSh"];
						GetChartTrendsSeries(c, y, "tSh", MU.Kpi.Names["tSh"]);
					}

					// основные лейблы
					for (k = 0; k < dimext; k++)
						if (sum_y[k] != 0)
							AddValueLabel(_chTrends, GetTrendValueStringLabel(sum_y[k]), _chTrends.ChartGroups[0].ChartData.SeriesList.Count - 1, k, false);
				}

				#endregion | parts mode    |
			} else if (_stackTrends.Peek().Length <= 3) {
				#region | tSh, tSp, tQl |

				for (k = 0; k < dim; k++) {
					y[k] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute(string.Format("sum({0})", _stackTrends.Peek()), trendsflt[k])) / GetTimeForTrends(_dvT[k]);
					if (min_y > y[k]) min_y = y[k];
				}
				if (dim != dimext) {
					dimcnt = dim;
					x[dim] = dim + 1;
					y[dim] = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute(string.Format("sum({0})", _stackTrends.Peek()), "")) / GetTimeForTrends(MU.Total.Rows[L]);
					
					if (_lu[L].YearDataSetIsLoaded) {
						// предыдущий год
						dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.Year));
						if (dryear != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(string.Format("sum({0})", _stackTrends.Peek()), dryear["Line"].ToString())) / GetTimeForTrends(dryear);
							x[dimcnt] = dimcnt + 1;
							sum_y[dimcnt] += y[dimcnt];
						}
						// текущий год
						dryear = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, tm1.AddYears(1).Year));
						if (dryear != null) {
							dimcnt++;
							y[dimcnt] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(string.Format("sum({0})", _stackTrends.Peek()), dryear["Line"].ToString())) / GetTimeForTrends(dryear);
							x[dimcnt] = dimcnt + 1;
							sum_y[dimcnt] += y[dimcnt];
						}
					}
				}
				if (MU.Kpi.Colors.ContainsKey(_stackTrends.Peek())) c = MU.Kpi.Colors[_stackTrends.Peek()];
				GetChartTrendsSeries(c, y, _stackTrends.Peek() == "ttS" ? "ttS" : "end", MU.Kpi.Names[_stackTrends.Peek()]);
				for (k = 0; k < dimext; k++)
					if (y[k] != 0)
						AddValueLabel(_chTrends, GetTrendValueStringLabel(y[k]), _chTrends.ChartGroups[0].ChartData.SeriesList.Count - 1, k, false);

				#endregion | tSh, tSp, tQl |
			}

			string selcolheader = MU.Kpi.Names.ContainsKey(_trendCol) ? "  :  " + MU.Kpi.Names[_trendCol] : "";
			_chTrends.Header.Text = string.Format(" {0}{1} ", _lu[L].GetLongUnitName(false, " \\ "), selcolheader);

			if (header != "" && !_chTrends.Header.Text.Contains(header))
				_chTrends.Header.Text += "  \\  " + header;

			_chTrends.Header.Style.ForeColor = Color.Black;
			_chTrends.Header.Style.BackColor = _chTrends.BackColor;

			foreach (ChartDataSeries ChDat in _chTrends.ChartGroups[0].ChartData.SeriesList)
				ChDat.X.CopyDataIn(x);

			_chTrends.ChartArea.AxisY.AutoMax = true;
			_chTrends.ChartArea.AxisY.Min = min_y;
			_chTrends.Refresh();

			SetupTrendLabels();
		}

		private ChartDataSeries GetChartTrendsSeries(Color color, float[] data, string tag, string label)
		{
			ChartDataSeries ChartDat = new ChartDataSeries();
			ChartDat.LineStyle.Color = color;
			ChartDat.Y.CopyDataIn(data);
			ChartDat.Tag = tag;
			ChartDat.Label = label;
			_chTrends.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
			return ChartDat;
		}

		private void SetupTrendLabels()
		{
			// если только один график то выходим
			if (_chTrends.ChartGroups[0].ChartData.SeriesList.Count <= 1)
				return;

			int series = -1;
			float y;
			double MaxY = _chTrends.ChartArea.AxisY.Max;

			foreach (ChartDataSeries ChartDat in _chTrends.ChartGroups[0].ChartData.SeriesList) {
				series++;
				if (ChartDat.Tag.ToString() == "end" || ChartDat.PointData.Length > 30)
					continue;

				Color series_color = ChartDat.LineStyle.Color;
				Color font_color = series_color.R + series_color.G + series_color.B > 384 ? Color.Black : Color.White;

				for (int n = 0; n < ChartDat.PointData.Length; n++) {
					y = ((PointF)ChartDat.PointData[n]).Y;
					if ((y * 100 / MaxY) > 3)
						AddTrendsValueLabel(_chTrends, GetTrendValueStringLabel(y), 0, series, n, series_color, font_color);
				}
			}
		}

		private string GetTrendsYearPointLabel(int p)
		{
			////foreach (C1.Win.C1Chart.Label l in chTrends.ChartLabels.LabelsCollection) {
			////   if (l.AttachMethod == AttachMethodEnum.DataIndexY && l.AttachMethodData.PointIndex == p) {
			////      return l.Text;
			////   }
			////}
			////return "";

			if (_chTrends.ChartArea.AxisX.ValueLabels.Count > p)
				return _chTrends.ChartArea.AxisX.ValueLabels[p].Text;
			else
				foreach (C1.Win.C1Chart.Label l in _chTrends.ChartLabels.LabelsCollection)
					if (l.AttachMethod == AttachMethodEnum.DataIndexY && l.AttachMethodData.PointIndex == p)
						return l.Text;
			return "";
		}

		private string GetTrendValueStringLabel(float f)
		{
			if (MU.Kpi.Measure.ContainsKey(_trendCol)) {
				if (MU.Kpi.Measure[_trendCol] == 1)			// time
					return Utils.GetTimeSpanString(f * (TmStyle == TimeStyle.MMM ? 1 : 60));
				if (MU.Kpi.Measure[_trendCol] == 2)			// percents
					return Utils.GetFloatString(f);
				if (MU.Kpi.Measure[_trendCol] == 3)			// units
					return f.ToString("0");
				else
					return Utils.GetFloatString(f);
			}
			return Utils.GetFloatString(f);
		}

		private void AddTrendsValueLabel(C1Chart c1ch, string val, int g, int s, int p, Color BackCol, Color ForeCol)
		{
			C1.Win.C1Chart.Label lab = c1ch.ChartLabels.LabelsCollection.AddNewLabel();
			lab.Compass = LabelCompassEnum.South;
			//lab.Compass = LabelCompassEnum.Auto;

			lab.Offset = 1;
			lab.Connected = false;

			lab.AttachMethod = AttachMethodEnum.DataIndex;
			lab.AttachMethodData.GroupIndex = g;
			lab.AttachMethodData.SeriesIndex = s;
			lab.AttachMethodData.PointIndex = p;
			////lab.AttachMethod = AttachMethodEnum.DataCoordinate;
			////lab.AttachMethodData.X = k;
			////lab.AttachMethodData.Y = 0;

			lab.Text = val;
			lab.Visible = true;
			lab.Style.ForeColor = ForeCol;
			lab.Style.BackColor = BackCol;

			//////// Работает в PDF: !!!!!
			////// lab.Style.Opaque = false; //

			//lab.Style.Border.Thickness = 1;
			//lab.Style.Border.Color = Color.Black;
			//lab.Style.Border.Rounding.All = 2;
			//lab.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
		}

		private void AddTrendsAxisLabel(C1Chart c1ch, string label, int p, double y, int amount, bool forsed)
		{
			C1.Win.C1Chart.Label lab = c1ch.ChartLabels.LabelsCollection.AddNewLabel();

			if (amount > 15) lab.Style.Rotation = RotationEnum.Rotate270;
			if (amount > 30 && p % 2 == 0 && !forsed) return;

			lab.Offset = 5;
			if (amount > 8 && amount <= 15)
				lab.Offset = p % 2 == 0 || forsed ? 5 : 18;

			lab.Text = label;
			lab.Visible = true;
			lab.Compass = LabelCompassEnum.South;

			lab.AttachMethod = AttachMethodEnum.DataIndexY;
			lab.AttachMethodData.GroupIndex = 0;
			lab.AttachMethodData.SeriesIndex = 0;
			lab.AttachMethodData.PointIndex = p;
			lab.AttachMethodData.Y = y;

			lab.Style.ForeColor = Color.Black; 
			lab.Style.BackColor = _colTranspWhite;
			lab.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.None;
			lab.Style.Autowrap = true;
			if (forsed) lab.Style.Font = _fontBold;
		}

		#endregion


		#region | Chart Losses            |

		private void BuildLosses()
		{
			SetCursWait();

			_chLosses.NavigationStack.Clear();
			_chLosses.NavigationStack.Push("TF");

			BuildTabLosses();
			BuildChartLossessMarks();

			SetCursDefault();
		}

		private void BuildTabLosses()
		{
			_grLosses.Redraw = false;
			_chLosses.BeginUpdate();

			// заполняем таблицу для grLosses вручную
			string header = "";
			if (_chLosses.CurrentTag.StartsWith("t", StringComparison.OrdinalIgnoreCase)) {
				BuildGridLossesTop();
				_chMarks.Visible = false;
			} else {
				BuildGridLossesParts(out header);
			}

			// настраиваем вид grLosses
			DataView dtv = _grLosses.DataSource as DataView;
			foreach (Column col in _grLosses.Cols) {
				if (col.Name == "RD")
					col.Visible = false;
				else if (col.Name.StartsWith("D"))
					col.Visible = false;
				else if (col.Name == "Name") {
					col.Width = 200;
					col.Caption = Res.strCause;
				} else if (col.Name == "Time") {
					col.Caption = Res.strTime;
				} else if (col.Name == "end")
					col.Caption = " ";
			}
			BuildChartLossess("Perc");

			// Заголовок
			if (header != "" && _chLosses.Header.Text.Contains(header)) {
				int pos = _chLosses.Header.Text.IndexOf(header);
				_chLosses.Header.Text = _chLosses.Header.Text.Substring(0, pos + header.Length);
			} else
				_chLosses.Header.Text += header;
			_chLosses.Header.Visible = true;

			//#if DEBUG
			//if (state == C.State.Idle && lu[L].ID == 10) throw new Exception("Exception from LoadTabLosses()");
			//#endif

			// ограничиваем размер грида
			_chLosses.EndUpdate();
			_grLosses.Height = (dtv.Count > 10 ? 10 : dtv.Count) * _grLosses.Rows.DefaultSize + 2
				+ _grLosses.Rows[0].Height + SystemInformation.HorizontalScrollBarHeight;
			_grLosses.Redraw = true;
		}

		private void BuildGridLossesTop()
		{
			_chLosses.Header.Text = string.Format(" {0}", _lu[L].GetLongUnitName(false, " \\ "));

			DataTable dtb = null;

			_repShorts = false;
			_repSpeed = false;

			float time = 0, yearbasetime;

			_losBaseTime = C.GetFloat(MU.Total.Rows[L][LossesBaseTimeKey]);

			dtb = new DataTable();
			dtb.Columns.Add("RD", typeof(string));
			dtb.Columns.Add("D0", typeof(int));
			dtb.Columns.Add("Name", typeof(string));
			dtb.Columns.Add("Time", typeof(float));
			dtb.Columns.Add("Perc", typeof(float)).Caption = Res.strPercents;
			dtb.Columns.Add(MU.PrevYear.ToString(), typeof(float)).Caption = MU.PrevYear.ToString() + ", %"; //string.Format("{0}; {1}", MU.PrevYear, Res.strLastYear);
			dtb.Columns.Add(MU.CurrYear.ToString(), typeof(float)).Caption = MU.CurrYear.ToString() + ", %"; //string.Format("{0}; {1}", MU.CurrYear, Res.strYTD);
			dtb.Columns.Add("Target", typeof(float)).Caption = Res.strTarget + ", %";
			dtb.Columns.Add("Gap", typeof(float), "IIF(Target<>0,Target-Perc,0)").Caption = Res.strGap + ", %";

			string flt;
			string[] parts;
			if (_chLosses.NavigationStack.Count == 1)
				 parts = MU.Kpi.DenomPartsDiff[LossesBaseKpi].Split(new string[] { ";" }, StringSplitOptions.None);
			else
				parts = MU.Kpi.NomParts[_chLosses.CurrentTag].Split(new string[] { ";" }, StringSplitOptions.None);


			foreach (string colname in parts) {
				
				DataRow drLoss = dtb.NewRow();
				drLoss["RD"] = colname;
				drLoss["D0"] = MU.Kpi.ID[colname];
				drLoss["Name"] = MU.Kpi.Names[colname];
				if (_lu[L].IsLine)
					time = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute(String.Format("sum({0})", colname), _filter));
				else if (_lu[L].IsCoupledLine)
					time = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute(String.Format("sum({0})/2", colname), _filter));

				if (time == 0)
					continue;
				drLoss["Time"] = time;
				drLoss["Perc"] = time / _losBaseTime;
				dtb.Rows.Add(drLoss);

				if (_lu[L].YearDataSetIsLoaded) {

					#region | предыдущий год |
					DataRow dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, MU.PrevYear));
					if (dr != null) {
						flt = string.Format("Yr={0}", MU.PrevYear);
						yearbasetime = C.GetFloat(dr[LossesBaseTimeKey]);
						if (_lu[L].IsLine)
							drLoss[MU.PrevYear.ToString()] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", colname), flt)) / yearbasetime;
						else if (_lu[L].IsCoupledLine)
							drLoss[MU.PrevYear.ToString()] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", colname), flt)) / yearbasetime / 2;
					}
					#endregion

					#region | текущий год      |
					dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, MU.CurrYear));
					if (dr != null) {
						flt = string.Format("Yr={0}", MU.CurrYear);
						yearbasetime = C.GetFloat(dr[LossesBaseTimeKey]);
						if (_lu[L].IsLine)
							drLoss[MU.CurrYear.ToString()] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", colname), flt)) / yearbasetime;
						else if (_lu[L].IsCoupledLine)
							drLoss[MU.CurrYear.ToString()] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", colname), flt)) / yearbasetime / 2;
					}
					#endregion
				
				} 

				drLoss["Target"] = _lu[L].GetTarget(colname, LossesBaseTimeKey, MU.CurrYear)/100;
			}

			// сортируем и фильтруем таблицу
			dtb.DefaultView.Sort = "Time DESC";
			dtb.DefaultView.RowFilter = "Time IS NOT NULL";
			_grLosses.DataSource = dtb.DefaultView;

		}

		private void BuildGridLossesParts(out string header)
		{
			header = "";
			float markval, yval;
			string flt = "", markflt, column = "", colname;
			int ncolumn = -1, downid = 0, downidnext = 0;

			foreach (string col in _lu[L].DownColumns.Values) {
				if (_chLosses.CurrentTag.Contains(col)) {
					column = col;
					string[] cols = _chLosses.CurrentTag.Split(new char[] { 'D', '=' });
					int.TryParse(cols[1], out ncolumn);
					int.TryParse(cols[2], out downid);
					if (downid != 0)
						header = " \\ " + Utils.GetRowField(_tbStopList, String.Format("DownID={0}", downid), "", "Name");
					if (col == "D0")
						_chLosses.Header.Text = string.Format(" {0}", _lu[L].GetLongUnitName(false, " \\ "));
					break;
				}
			}

			string expression;
			if (_repShorts)
				expression = "sum(Seconds)";
			else if (_repSpeed)
				expression = "sum(TD)-sum(Seconds)";
			else
				expression = "sum(TD)";

			DataTable dtb = null;
			if (_lu[L].DownColumns.ContainsKey(ncolumn + 1) || _lu[L].DownColumns.ContainsKey(ncolumn)) {
				column = _lu[L].DownColumns.ContainsKey(ncolumn + 1) ? _lu[L].DownColumns[ncolumn + 1] : _lu[L].DownColumns[ncolumn];

				// Создаем таблицу для грида
				_tbStopList.AcceptChanges();
				DataView dtv = new DataView(_tbStopList, _chLosses.CurrentTag + " AND TD<>0", "TD DESC", DataViewRowState.CurrentRows);
				dtb = dtv.ToTable(true, new string[] { "RD", column });
				dtv.Dispose();
				dtb.Columns.Add("Name", typeof(string));
				dtb.Columns.Add("Time", typeof(float));
				dtb.Columns.Add("Perc", typeof(float)).Caption = Res.strPercents;
				dtb.Columns.Add("MTBF", typeof(float));
				dtb.Columns.Add("MTTR", typeof(float));
				dtb.Columns.Add("Events", typeof(int));
				dtb.Columns.Add("Ao", typeof(float), ("IIF(MTBF>0,MTBF/(MTBF+MTTR),0)"));

				// Проверка нет ли в составе коротких остановов
				string RD = C.GetRowField(_lu[L].DataSet.Tables["StopList"], String.Format("DownID={0}", downid), "RD");
				if (_lu[L].DataSet.Tables["Gaps"].Columns.Contains(RD) &&
					_lu[L].DataSet.Tables["Gaps"].Columns[RD].Expression.Contains("+tSh") &&
					column == "D1") {
					DataRow dr = dtb.NewRow();
					dr["RD"] = "tSh";
					dr["Name"] = MU.Kpi.Names["tSh"];
					float shorts = C.GetFloat(_lu[L].DataSet.Tables["Gaps"].Compute("SUM(tSh)", ""));
					if (!_lu[L].IsLine)
						shorts /= 2;
					_losBaseTime = shorts + _losBaseTime;

					dr["Time"] = shorts;
					dr["Perc"] = shorts / _losBaseTime;
					dtb.Rows.Add(dr);
					dtb.Columns["MTBF"].Caption = "MTBA";
				}

				// Добавляем колонки для Marks, но не все, а только где значение больше нуля
				foreach (string m in MU.marks_captions.Keys) {
					foreach (DataRow dr in _lu[L].DataSet.Tables["MarkList"].Select("BandID=" + m.Substring(4))) {
						markflt = string.Format(m + "={0} AND {1}={2}", dr["val"], _lu[L].DownColumns[ncolumn], downid);
						if (C.GetFloat(_lu[L].DataSet.Tables["Stops"].Compute(expression, markflt)) == 0)
							continue;
						DataColumn col = dtb.Columns.Add("M_" + dr["ID"], typeof(float));
						col.Caption = dr["Name"].ToString();
					}
				}

				// Заполняем таблицу данными
				bool b1 = dtb.Columns.Contains("D" + (ncolumn + 1));
				foreach (DataRow dr in dtb.Rows) {
					downidnext = dr[column] == DBNull.Value ? downid : (int)dr[column];
					flt = dr[column] == DBNull.Value ? "DownID=" + downid : String.Format("{0}={1}", column, dr[column]);
					flt = Utils.StrJoin(flt, _filter);

					if (_repShorts)
						flt += Utils.StrJoin(flt, _lu[L].MinIdleTime.ToString()); // " AND TD<" + lu[L].MinIdleTime.ToString();

					yval = C.GetFloat(_lu[L].DataSet.Tables["Stops"].Compute(expression, flt));
					if (yval != 0) {
						string mttflt;
						mttflt = b1 && dr[column] != DBNull.Value ? string.Format("D{0}={1}", ncolumn + 1, dr[column]) : "DownID=" + downid;
						string nameflt = dr[column] == DBNull.Value ? "DownID=" + downid : "DownID=" + dr[column];

						dr["Name"] = Utils.GetRowField(_tbStopList, nameflt, "", "Name");
						dr["Time"] = yval;
						dr["Perc"] = yval / _losBaseTime;
						int events = C.GetInt(_lu[L].DataSet.Tables["Stops"].Compute("Count(TD)", mttflt));
						dr["MTTR"] = yval / events;
						dr["Events"] = events;
						dr["MTBF"] = _losBaseTime / events; //C.GetFloat(MonitorUnit.tbL.Rows[L]["T4"]) / events;

						// Заполняем колонки для Marks
						foreach (string m in MU.marks_captions.Keys) {
							foreach (DataRow drm in _lu[L].DataSet.Tables["MarkList"].Select("BandID=" + m.Substring(4))) {
								if (!dtb.Columns.Contains(String.Format("M_{0}", drm["ID"])))
									continue;
								markflt = string.Format("{0}={1} AND {2}", m, drm["val"], flt);
								markflt = Utils.StrJoin(markflt, _filter);

								markval = C.GetFloat(_lu[L].DataSet.Tables["Stops"].Compute(expression, markflt));
								dr["M_" + drm["ID"]] = markval / _losBaseTime;
							}
						}
					}
				}

				// Добавляем итог
				DataRow summrow = dtb.NewRow();
				summrow["rd"] = "xx";
				summrow["Name"] = "Summary";
				summrow["Time"] = C.GetFloat(dtb.Compute("SUM(Time)", ""));
				summrow["Perc"] = C.GetFloat(dtb.Compute("SUM(Perc)", ""));
				summrow["Events"] = C.GetInt(dtb.Compute("SUM(Events)", ""));
				summrow["MTTR"] = (float)summrow["Time"] / (int)summrow["Events"];
				summrow["MTBF"] = _losBaseTime / (int)summrow["Events"];

				// Заполняем колонки для Marks
				foreach (string m in MU.marks_captions.Keys) {
					foreach (DataRow drm in _lu[L].DataSet.Tables["MarkList"].Select("BandID=" + m.Substring(4))) {
						if (!dtb.Columns.Contains("M_" + drm["ID"]))
							continue;
						colname = "M_" + drm["ID"];
						summrow[colname] = dtb.Compute(String.Format("SUM({0})", colname), "");
					}
				}

				dtb.Rows.Add(summrow);
			}
			if (dtb == null)
				return;

			dtb.DefaultView.Sort = "rd, Time DESC";
			dtb.DefaultView.RowFilter = "Time IS NOT NULL";
			_grLosses.DataSource = dtb.DefaultView;

			// create frozen area at the bottom, with 1 row
			if (_ffbLosses == null)
				_ffbLosses = new FlexFreezeBottom(_grLosses, 1);

			// стиль для выделенной колонки
			CellStyle style = _grAnalyse.Styles.Add("Bold");
			style.Font = _fontBold;
			style.ForeColor = _colForeBold;
			style.BackColor = SystemColors.Control;
			_grLosses.Rows[_grLosses.Rows.Count - 1].Style = style;
		}

		private void BuildChartLossess(string column)
		{
			bool needyears = true;
			_chLosses.BeginUpdate();
			_chLosses.ChartArea.AxisX.ValueLabels.Clear();
			_chLosses.ChartLabels.LabelsCollection.Clear();
			_chLosses.ChartGroups[0].ChartData.SeriesList.Clear();
			_chLosses.ChartGroups[1].ChartData.SeriesList.Clear();

			ChartDataSeries ChartDat;

			DataTable tb =	(_grLosses.DataSource as DataView).Table;
			DataView dtv = new DataView(tb);
			dtv.RowFilter = "RD<>'xx'";
			dtv.Sort = column + " DESC";

			bool bold;
			int row = 0;
			string rd = "", tag;
			string col1 = dtv.Table.Columns[1].ColumnName;
			float yval = 0, max_yval = 0, min_yval = 0, max_yval_pareto = 0;

			int dim = dtv.Count;

			float[] x = new float[dim];	// X - для всех один
			float[] y = new float[dim];	// Основной график
			float[] y0 = new float[dim];	// Предыдущий год
			float[] y1 = new float[dim];	// Текущий год
			float[] yt = new float[dim];	// Цели
			float[] yg = new float[dim];	// Расхождение с целью
			float[] yp = new float[dim];	// Парето

			bool year0 = false, year1 = false;
			float yearbasetime0 = 0, yearbasetime2 = 0;
			float yParSum = 0; // = C.GetFloat(tb.Compute(string.Format("SUM({0})", column), "RD<>'xx'"));
			if (!column.StartsWith("M_"))
				foreach (DataRow dr in  tb.Select("RD<>'xx'"))
					yParSum += dr[column] == DBNull.Value ? 0 : C.GetFloat(dr[column]);
			else
				yParSum = C.GetFloat(tb.Compute(string.Format("SUM({0})", column), "RD<>'xx'")) / GetTimeDivider();

			float yParAcc = 0;

			#region | основной график |

			// Перебираем все строки из грида
			foreach (DataRowView drv in dtv) {
				// назначаем тег
				rd = drv["RD"].ToString();
				if (rd == "xx") continue;
				if (rd == "ttS") tag = "ttS";
				else if (rd == "tSp" || rd == "tSh" || rd == "tNF" || drv[col1] == DBNull.Value) tag = "end";
				else tag = String.Format("{0}={1}", col1, drv[col1]);

				// определяем Y значение
				switch (column) {
					case "Ao":
					case "Events":
						yval = C.GetFloat(drv[column]);
						break;
					case "Time":
					case "MTBF":
					case "MTTR":
					case "MTBA":
						yval = C.GetFloat(drv[column]) / GetTimeDivider();
						break;
					default:
						if (!column.StartsWith("M_")) {
							yval = C.GetFloat(drv["Perc"]);
							if (needyears) {
								string flt;
								
								if (_lu[L].YearDataSetIsLoaded) {
									DataRow dr;
									#region | предыдущий год |
									dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, MU.PrevYear));
									if (dr != null) yearbasetime0 = C.GetFloat(dr[LossesBaseTimeKey]);
									if (!dtv.Table.Columns.Contains(MU.PrevYear.ToString())) {
										if (dr != null) {
											year0 = true;
											if (_chLosses.NavigationStack.Count == 1 || rd == "tSh")
												flt = string.Format("Yr={0}", MU.PrevYear);
											else if (tag == "end")
												flt = string.Format("Yr={0} AND {1}", MU.PrevYear, String.Format("{0} AND {1} IS NULL", _chLosses.CurrentTag, col1));
											else
												flt = string.Format("Yr={0} AND {1}", MU.PrevYear, tag);

											if (_lu[L].IsLine)
												y0[row] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", rd), flt)) / yearbasetime0;
											else if (_lu[L].IsCoupledLine)
												y0[row] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", rd), flt)) / yearbasetime0 / 2;

										} else
											year0 = false;
									} else {
										y0[row] = C.GetFloat(drv[MU.PrevYear.ToString()]);
										year0 = true;
									}
									//x0[row] = row;

									#endregion | предыдущий год |

									#region | текущий год    |
									dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, MU.CurrYear));
									if (dr != null) yearbasetime2 = C.GetFloat(dr[LossesBaseTimeKey]);
									if (!dtv.Table.Columns.Contains(MU.CurrYear.ToString())) {

										if (dr != null) {
											year1 = true;
											if (_chLosses.NavigationStack.Count == 1 || rd == "tSh")
												flt = string.Format("Yr={0}", MU.CurrYear);
											else if (tag == "end")
												flt = string.Format("Yr={0} AND {1}", MU.CurrYear, String.Format("{0} AND {1} IS NULL", _chLosses.CurrentTag, col1));
											else
												flt = string.Format("Yr={0} AND {1}", MU.CurrYear, tag);
											if (_lu[L].IsLine)
												y1[row] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", rd), flt)) / yearbasetime2;
											else if (_lu[L].IsCoupledLine)
												y1[row] = C.GetFloat(_lu[L].YearsDS.Tables["year"].Compute(String.Format("sum({0})", rd), flt)) / yearbasetime2 / 2;

										} else
											year1 = false;
									} else {
										y1[row] = C.GetFloat(drv[MU.CurrYear.ToString()]);
										year1 = true;
									}
									//x2[row] = row;

									#endregion | текущий год    |

								}
								#region | цель           |

								if (col1 == "D0") {
									yt[row] = C.GetFloat(drv["Target"]);
									yg[row] = C.GetFloat(drv["Gap"]);
									if (min_yval > yg[row])
										min_yval = yg[row];
								}

								#endregion | цель           |
							}
						} else
							yval = C.GetFloat(drv[column]) / GetTimeDivider();
						break;
				}
				x[row] = row;
				y[row] = yval;
				float colval;
				if (column == MU.PrevYear.ToString()) colval = y0[row] * 100;
				else if (column == MU.CurrYear.ToString()) colval = y1[row] * 100;
				else if (column == "Target") colval = yt[row];
				else if (column == "Gap") colval = yg[row];
				else colval = yval;

				yParAcc += colval * 100 / yParSum;
				if (max_yval_pareto < yParAcc) max_yval_pareto = yParAcc;
				yp[row] = yParAcc; 

				int serindex = needyears && year0 ? 1 : 0;

				#region | пределы осей и теги |

				int lblPos = 0;
				if (year0) lblPos++;
				if (year1) lblPos++;

				max_yval = max_yval < yval ? yval : max_yval;
				min_yval = min_yval > yval ? yval : min_yval;
				_chLosses.SetPointTag(0, lblPos, row, tag);
				_chLosses.SetPointRoot(0, lblPos, row, rd);
				if (year0) {
					max_yval = max_yval < y0[row] ? y0[row] : max_yval;
					min_yval = min_yval > y0[row] ? y0[row] : min_yval;
					_chLosses.SetPointTag(0, 0, row, tag);
					_chLosses.SetPointRoot(0, 0, row, rd);
				}
				if (year1) {
					max_yval = max_yval < y1[row] ? y1[row] : max_yval;
					min_yval = min_yval > y1[row] ? y1[row] : min_yval;
					_chLosses.SetPointTag(0, year0 ? 1 : 0, row, tag);
					_chLosses.SetPointRoot(0, year0 ? 1 : 0, row, rd);
				}
				if (needyears && col1 == "D0") {
					_chLosses.SetPointTag(0, lblPos + 1, row, string.Format("{0}: {1}%", Res.strTarget, yt[row] * 100));
					_chLosses.SetPointTag(0, lblPos + 2, row, string.Format("{0}: {1}%", Res.strGap, Utils.GetFloatString(yg[row] * 100)));
				}


				#endregion | пределы осей и теги |

				#region | определяем лейбл    |

				string lab, ylab1 = "", ylab2 = "";
				switch (column) {
					case "Ao":
						lab = C.GetFloat(drv[column]).ToString("0.0%");
						break;
					case "Events":
						lab = C.GetFloat(drv[column]).ToString("0.#");
						break;
					case "MTBF":
					case "MTTR":
					case "MTBA":
						lab = Utils.GetTimeSpanString(C.GetFloat(drv[column]));
						break;
					default:
						float val = C.GetFloat(drv[column]);
						lab = string.Format("{0}%\r\n{1}", Utils.GetFloatString(val * 100), Utils.GetTimeSpanString(val * _losBaseTime));
							if (year0) {
								ylab1 = string.Format("{0}%,  {1}\r{2}", Utils.GetFloatString(y0[row] * 100), Utils.GetTimeSpanString(y0[row] * yearbasetime0), 
									string.Format("{0} ({1})", Res.strLastYear, MU.PrevYear ));
								AddValueLabel(_chLosses, ylab1, 0, row, false).Visible = false;
							}
							if (year1) {
								ylab2 = string.Format("{0}%,  {1}\r{2} year", Utils.GetFloatString(y1[row] * 100), Utils.GetTimeSpanString(y1[row] * yearbasetime2),
									string.Format("{0} ({1})", Res.strYTD, MU.CurrYear));
								AddValueLabel(_chLosses, ylab2, year0 ? 1 : 0, row, false).Visible = false;
							}
						break;
				}

				#endregion | определяем лейбл    |

				// добавляем лейблы значения
				AddValueLabel(_chLosses, lab, lblPos, row, false);

				// добавляем лейблы к оси X
				bold = col1 != "D0" && drv[col1] == DBNull.Value;
				AddValueLabelToLossesAxis(drv["Name"].ToString(), lblPos, row, dtv.Count, false, bold);
				row++;
			}

			// предыдущий год
			if (year0) {
				ChartDat = new ChartDataSeries();
				ChartDat.Y.CopyDataIn(y0);
				ChartDat.X.CopyDataIn(x);
				ChartDat.Tag = "-1";
				ChartDat.Label = string.Format("{0} ({1})", Res.strLastYear, MU.PrevYear);
				_chLosses.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
			}

			// текущий год
			if (year1) {
				ChartDat = new ChartDataSeries();
				ChartDat.Y.CopyDataIn(y1);
				ChartDat.X.CopyDataIn(x);
				ChartDat.Tag = "1";
				ChartDat.Label = string.Format("{0} ({1})", Res.strYTD, MU.CurrYear);
				_chLosses.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
			}

			ChartDat = new ChartDataSeries();
			if (rd != string.Empty && MU.Kpi.Colors.ContainsKey(rd))
				ChartDat.LineStyle.Color = MU.Kpi.Colors[rd];
			ChartDat.Y.CopyDataIn(y);
			ChartDat.X.CopyDataIn(x);
			ChartDat.Tag = "0";
			ChartDat.Label = Res.strSelectedRange;
			_chLosses.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

			// цели + расхождение
			if (needyears && col1 == "D0") {
				ChartDat = new ChartDataSeries();
				ChartDat.Y.CopyDataIn(yt);
				ChartDat.X.CopyDataIn(x);
				ChartDat.LineStyle.Color = Color.Gray;
				ChartDat.Tag = "t";
				ChartDat.Label = Res.strTarget;
				_chLosses.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

				ChartDat = new ChartDataSeries();
				ChartDat.Y.CopyDataIn(yg);
				ChartDat.X.CopyDataIn(x);
				ChartDat.LineStyle.Color = Color.Navy;
				ChartDat.Tag = "g";
				ChartDat.Label = Res.strGap;
				_chLosses.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
			}

			if (tb.Rows.Count > 3 && yParSum > 0) {
				ChartDat = new ChartDataSeries();
				ChartDat.Y.CopyDataIn(yp);
				ChartDat.X.CopyDataIn(x);
				ChartDat.Tag = "p";
				ChartDat.LineStyle.Thickness = 2;
				ChartDat.LineStyle.Color = Color.Gray;
				ChartDat.SymbolStyle.Shape = SymbolShapeEnum.None;
				ChartDat.LegendEntry = true;
				ChartDat.Label = Res.strParetoCurve;
				_chLosses.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
			}

			#endregion | основной график |

			// Нижняя подпись (прозрачная) нужна только чтобы добавить место под осью
			_chLosses.Footer.Visible = row > 6;

			#region | настройка Y оси |

			switch (column) {
				case "Ao":
					_chLosses.ChartArea.AxisY.Text = Res.strPercents;
					_chLosses.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericPercentage;
					break;
				case "Events":
					_chLosses.ChartArea.AxisY.Text = "Events";
					_chLosses.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericManual;
					break;
				case "MTBF":
				case "MTTR":
				case "MTBA":
					_chLosses.ChartArea.AxisY.Text = TmStyle == TimeStyle.MMM ? Res.strMins : Res.strHours;
					_chLosses.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericManual;
					break;
				default:
					string kpikey = MU.Kpi.MainKpiKey;
					if (tscbLossesBaseTime.ComboBox.SelectedValue != null)
						kpikey = tscbLossesBaseTime.ComboBox.SelectedValue.ToString();

					string basetimekey = "";
					if (MU.Kpi.Denominators.ContainsKey(kpikey))
						basetimekey = MU.Kpi.Denominators[kpikey];

					_chLosses.ChartArea.AxisY.Text = string.Format("{0}{1}''{2}''", Res.strPercentsFrom, Environment.NewLine, MU.Kpi.Names[basetimekey]);
					_chLosses.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericPercentage;
					break;
			}

			#endregion | настройка Y оси |

			_chLosses.ChartArea.AxisY.Min = min_yval;
			_chLosses.ChartArea.AxisY.Max = max_yval;
			_chLosses.ChartArea.AxisY2.Min = 0;
			_chLosses.ChartArea.AxisY2.Max = max_yval_pareto;

			// обозначаем границы для оси Х
			if (dtv.Count > 1) {
				_chLosses.ChartArea.AxisX.Min = -0.5f;
				_chLosses.ChartArea.AxisX.Max = dtv.Count - 0.5;
			} else if (dtv.Count == 1) {
				_chLosses.ChartArea.AxisX.Min = -2;
				_chLosses.ChartArea.AxisX.Max = 2;
			}

			_chLosses.Legend.Visible = _chLosses.ChartGroups[0].ChartData.SeriesList.Count > 1; //!column.StartsWith("M_");

			// Если в результате Exception код EndUpdate() не будет выполнен то график в дальнейшем
			// не будет корректно обновляться на экране, т.е. BeginUpdate() не должен вызываться чаще чем EndUpdate()
			// По правильному надо всю процедуру заключить в try-catch чтобы вызывать EndUpdate()
			// в блоке finally, или, вызывать EndUpdate() для страховки дважды:
			_chLosses.EndUpdate();
			_chLosses.EndUpdate();
		}

		private void BuildChartLossessMarks()
		{
			_chMarks.ChartLabels.LabelsCollection.Clear();
			ChartDataSeriesCollection coll = _chMarks.ChartGroups[0].ChartData.SeriesList;
			coll.Clear();

			if (!_chLosses.CurrentTag.StartsWith("D"))
				return;

			string labels = "Name", labelval;
			int slice = -1;
			float val;
			bool thereisanydata = false;

			DataTable dtb = (_grLosses.DataSource as DataView).Table;
			foreach (DataRow dr in _lu[L].DataSet.Tables["MarkList"].Select("BandID=1")) {
				// слайсы считаем в любом случае чтобы цвета признаков не менялись в зависимости от их числа
				slice++;
				ChartDataSeries sers = coll.AddNewSeries();
				sers.Tag = string.Format("M_{0}", dr["ID"]);

				sers.PointData.Length = 2;
				if (!dtb.Columns.Contains(sers.Tag.ToString()))
					continue;

				val = C.GetFloat(C.GetRowField(dtb, "RD='xx'", sers.Tag.ToString()));
				sers.PointData[0] = new PointF(0, val);
				sers.LineStyle.Color = MU.GetColorMarks(dr);
				sers.TooltipText = dr[labels].ToString();

				labelval = string.Format("{2}{0:0.0%}, {1}", val, Utils.GetTimeSpanString(val * _losBaseTime), Environment.NewLine);
				AddValueLabelToMarks(_chMarks, 0, slice, String.Format("{0}{1}", dr[labels], labelval), sers.Tag.ToString());
				thereisanydata = true;
			}

			foreach (DataRow dr in _lu[L].DataSet.Tables["MarkList"].Select("BandID=2")) {
				slice++;
				ChartDataSeries sers = coll.AddNewSeries();
				sers.Tag = string.Format("M_{0}", dr["ID"]);
				sers.PointData.Length = 2;

				if (!dtb.Columns.Contains(sers.Tag.ToString()))
					continue;

				val = C.GetFloat(C.GetRowField(dtb, "RD='xx'", sers.Tag.ToString()));

				sers.LineStyle.Color = MU.GetColorMarks(dr);
				sers.PointData[1] = new PointF(1, val);
				sers.TooltipText = dr[labels].ToString();

				labelval = string.Format("{2}{0:0.0%}, {1}", val, Utils.GetTimeSpanString(val * _losBaseTime), Environment.NewLine);
				AddValueLabelToMarks(_chMarks, 1, slice, String.Format("{0}{1}", dr[labels], labelval), sers.Tag.ToString());
				thereisanydata = true;
			}

			// для Marks задаем расположение лейблов
			ChartLabels lbls = _chMarks.ChartLabels;
			lbls.AutoArrangement.Method = C1.Win.C1Chart.AutoLabelArrangementMethodEnum.FindingOptimum;
			lbls.AutoArrangement.Options = AutoLabelArrangementOptions.Top | AutoLabelArrangementOptions.Bottom;
			foreach (C1.Win.C1Chart.Label lbl in lbls.LabelsCollection)
				lbl.Compass = LabelCompassEnum.Auto;
			_chMarks.Visible = thereisanydata;
		}

		private void MarksSelect(string s)
		{
			ChartGroup grp = _chMarks.ChartGroups[0];

			foreach (ChartDataSeries ser in grp.ChartData.SeriesList)
				ser.Offset = ser.Tag.ToString() == s ? 30 : 0;

			////foreach (C1.Win.C1Chart.Label lab in chMarks.ChartLabels.LabelsCollection)
			////   lab.Style.ForeColor = s == "Perc" || lab.Name == s ? Color.Black : Color.LightGray;
		}

		private void AddValueLabelToLossesAxis(string label, int s, int p, int amount, bool forsed, bool bold)
		{
			C1.Win.C1Chart.Label lab = _chLosses.ChartLabels.LabelsCollection.AddNewLabel();
			lab.Name = string.Format("x{0}", p);

			if (amount > 6) {
				switch (p % 3) {
					case 0: lab.Offset = 5; break;
					case 1: lab.Offset = 18; break;
					case 2: lab.Offset = 31; break;
				}
			} else if (amount > 3) {
				switch (p % 2) {
					case 0: lab.Offset = 5; break;
					case 1: lab.Offset = 18; break;
				}
			} else
				lab.Offset = 5;

			lab.Text = label;
			lab.Visible = !(amount > 30 && p % 2 == 0 && !forsed);
			lab.Connected = true;
			lab.Compass = LabelCompassEnum.South;

			lab.AttachMethod = AttachMethodEnum.DataIndexY;
			lab.AttachMethodData.GroupIndex = 0;
			lab.AttachMethodData.SeriesIndex = s;
			lab.AttachMethodData.PointIndex = p;
			lab.AttachMethodData.Y = 0;

			lab.Style.ForeColor = Color.Black;
			lab.Style.BackColor = _colTranspWhite;
			lab.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.None;
			lab.Style.Autowrap = true;
			if (bold) lab.Style.Font = _fontBold;
		}

		private void AddValueLabelToMarks(C1Chart c1c, int p, int s, string text, string tag)
		{
			C1.Win.C1Chart.Label lab = c1c.ChartLabels.LabelsCollection.AddNewLabel();
			lab.AttachMethod = AttachMethodEnum.DataIndex;
			lab.Name = string.Format("s{0}p{1}", s, p);

			lab.Style.ForeColor = Color.Black;
			lab.Style.BackColor = Color.White;
			lab.Style.Border.Rounding.All = 1;
			lab.Style.Border.Thickness = 1;
			lab.Style.Border.Color = Color.Black;
			lab.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;

			AttachMethodData amd = lab.AttachMethodData;
			amd.GroupIndex = 0;
			amd.SeriesIndex = s;
			amd.PointIndex = p;

			////if (text.Contains("+"))
			////   text = text.Replace("+", Environment.NewLine);
			lab.Visible = true;
			lab.Text = text;
			//lab.Name = tag;
			//lab.Compass = LabelCompassEnum.Radial;
			lab.Connected = true;
			lab.Offset = 10;
		}

		#endregion | Charts handling         |


		#region | Chart events            |


		private void chTrends_MouseMove(object sender, MouseEventArgs e)
		{
			//return;
			if (IsEmpty()) return;

			// to reduce CPU workload
			Thread.Sleep(2);

			C1Chart chart = sender as C1Chart;
			string strTip = "", strMins = "";
			float Mins = 0, Perc = 0;

			int x = e.X, y = e.Y;
			int group = -1, s = -1, p = -1, d = -1;
			ChartGroup grp = chart.ChartGroups[0];

			//ChartRegionEnum region = chart.ChartRegionFromCoord(e.X, e.Y);
			if (chart.Legend.SeriesFromCoord(e.X, e.Y, ref group, ref s) && (group == 0 && s >= 0)) {
				if (chart.ChartGroups[group].ChartData[s].Tag.ToString() != "end")
					chart.Cursor = Cursors.Hand;
				chart.Refresh();
				return;
			} else if (grp.CoordToDataIndex(x, y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1) {
				// если индекс больше чем строк в таблице Trends значит это Average значение (берется из таблицы Lines)
				DataRow dr = null;
				string yeartip = GetTrendsYearPointLabel(p); ;
				if (p <= _dvT.Count) {
					dr = (p == _dvT.Count ? MU.Total.DefaultView[L] : _dvT[p]).Row;
					if (p == _dvT.Count) yeartip = Res.strTrendsAverageTip;
				} else if (p >= _dvT.Count) {
					dr = C.GetRow(MU.TotalFilt, string.Format("ID={0} AND Line='Yr={1}'", _lu[L].ID, yeartip));
					yeartip = string.Format(Res.strTrendsYearTip, yeartip);
				}
				yeartip = Environment.NewLine + yeartip;

				string chart_trends_tag = chart.ChartGroups[0].ChartData[s].Tag.ToString();
				strTip = chart.ChartGroups[0].ChartData[s].Label;

				if (strTip != "") strTip += Environment.NewLine;
				int measure = MU.Kpi.Measure.ContainsKey(_trendCol) ? MU.Kpi.Measure[_trendCol] : 2;
				switch (measure) {
					case 1: // минуты
						Mins = GetTimeDivider() * C.GetFloat(chart.ChartGroups[0].ChartData[s].Y[p]);
						strMins = Utils.GetTimeSpanString(Mins);
						if (TmStyle == TimeStyle.MMM) strMins += " minutes";
						Tt.SetToolTip(e.X, e.Y, chart, string.Format("{0}{1}{2}", strTip, strMins, yeartip));
						break;
					case 2: // проценты
						Perc = C.GetFloat(chart.ChartGroups[0].ChartData[s].Y[p]);
						Mins = Perc * GetTimeForTrends(dr);
						strMins = Utils.GetTimeSpanString(Mins);
						if (TmStyle == TimeStyle.MMM) strMins += " minutes";
						Tt.SetToolTip(e.X, e.Y, chart, string.Format("{0},    {1:0.0} %{2}", strTip + strMins, Perc, yeartip));
						break;
					case 3: // штуки
						Mins = C.GetFloat(chart.ChartGroups[0].ChartData[s].Y[p]);
						Tt.SetToolTip(e.X, e.Y, chart, string.Format("{0}\r\n{1:0} units{2}", strTip, Mins, yeartip));
						break;
					case 4: // объем

						break;
					case 5:

						break;
				}

				chart.Cursor = chart_trends_tag == "end" ? Cursors.Default : Cursors.Hand;
			} else {
				Tt.SetToolTip();
				////foreach (ChartDataSeries cds in chCompare.ChartGroups[0].ChartData.SeriesList)
				////   cds.Display = SeriesDisplayEnum.Show;
				chart.Cursor = Cursors.Default;
			}
		}

		private void chTrends_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			C1Chart chart = sender as C1Chart;
			int s = -1, p = -1, d = -1, group = -1;
			Color c = Color.White;

			if (e != null && e.Button == MouseButtons.Right) {
				// навигация назад (по стеку)
				if (_stackTrends.Count == 0) {
					BuildChartTrends();
					return;
				}

				_stackTrends.Pop();
				if (_stackTrends.Count == 0 || _stackTrends.Peek() == "ttS") 
					BuildChartTrends();
				else
					BuildChartTrendsParts();

			} else {
				ChartGroup grp = chart.ChartGroups[0];
				ChartRegionEnum region = chart.ChartRegionFromCoord(e.X, e.Y);

				if (((region.Equals(ChartRegionEnum.Legend) && chart.Legend.SeriesFromCoord(e.X, e.Y, ref group, ref s) && (group >= 0 && s >= 0))
					|| (grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1))
						&& grp.ChartData[s].Tag.ToString() != "end"
						&& !_stackTrends.Contains(grp.ChartData[s].Tag.ToString())) {

						_stackTrends.Push(grp.ChartData[s].Tag.ToString());
						if (_stackTrends.Peek() == "ttS") {
							BuildChartTrends();
							return;
						}
						if (_grTrends.Tag == null)
							_grTrends.Tag = grp.ChartData[s].LineStyle.Color;
						SetCursWait();
						BuildChartTrendsParts();
						SetCursDefault();
				} 
			}
		}

		private void chTrends_MouseLeave(object sender, System.EventArgs e)
		{
			_chTrends.Cursor = Cursors.Default;
		}

		private void chTrends_DrawDataSeries(object sender, DrawDataSeriesEventArgs e)
		{
			if (nowDrawReport && _stackTrends.Count > 0) {
				#region | эксперимент с расцветкой в PDF |

				////// в pdf отчете штриховка работает не корректно (все рисует серым), поэтому раскрашиваем с фантазией

				////Color backcolor = ((ChartDataSeries)sender).LineStyle.Color;
				//////ChartDataSeries s = (ChartDataSeries)sender;

				////Color c = Color.FromArgb(255 - backcolor.R, 255 - backcolor.G, 255 - backcolor.B);
				////switch (e.SeriesIndex % 3) {
				////   case 0:
				////      c = backcolor;
				////      break;
				////   case 1:
				////      c = Color.FromArgb(backcolor.G, backcolor.B, backcolor.R);
				////      break;
				////   case 2:
				////      c = Color.FromArgb(backcolor.B, backcolor.R, backcolor.G);
				////      break;
				////}
				////e.Brush = new SolidBrush(c);

				#endregion | эксперимент с расцветкой в PDF |
			} else {
				#region | эксперимент с градиентом       |

				//C1.Win.C1Chart.ChartDataSeries ds = (ChartDataSeries)sender;
				//Color clr1 = ds.LineStyle.Color;
				//Color clr2 = Color.White;
				//if (e.Bounds.Size.Height > 0 && e.Bounds.Size.Width > 0) {
				//   System.Drawing.Drawing2D.LinearGradientBrush lgb = new LinearGradientBrush(e.Bounds, clr1, clr2, LinearGradientMode.);
				//   e.Brush = lgb;
				//}

				#endregion | эксперимент с градиентом       |

				// каждую третью можно не штриховать
				if (e.SeriesIndex % 3 == 2 || _chTrends.ChartGroups[0].ChartData.SeriesList.Count <= 3)
					return;
				ChartDataSeries chartseries = sender as ChartDataSeries;
				if (chartseries.Tag != null && chartseries.Tag.ToString() == "tSh")
					return;
				Color backcolor = chartseries.LineStyle.Color;
				// если это основной цвет, то тоже не штриховать
				if (_stackTrends.Count == 0 || backcolor.Equals(_colRunLight))
					return;

				// для более светлых тонов цвет штриховки черный, для темных - белый
				Color forecolor = backcolor.R + backcolor.G + backcolor.B > 384 ? Color.Black : Color.White;

				// каждую первую из тройки рисуем мелкой штриховкой, каждую вторую  - обычной штриховкой
				HatchStyle hs = e.SeriesIndex % 3 == 1 ? HatchStyle.LightUpwardDiagonal : HatchStyle.BackwardDiagonal;
				e.Brush = new HatchBrush(hs, forecolor, backcolor);

				// хз надо или нет
				e.DisposeBrush = true;
			}
		}


		private void chLosses_MouseMove(object sender, MouseEventArgs e)
		{
			//return;
			if (IsEmpty()) return;

			// to reduce CPU workload
			Thread.Sleep(5);

			C1Chart chart = sender as C1Chart;
			int s = -1, p = -1, d = -1;
			ChartGroup grp = chart.ChartGroups[0];

			if (grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1) {
				string tag = _chLosses.GetPointTag(0, s, p);
				if (tag == "")
					return;
				chart.Cursor = tag == "end" || tag.StartsWith(Res.strTarget) || tag.StartsWith(Res.strGap) ? Cursors.Default : Cursors.Hand;
				string lbItem = string.Format("x{0}", p);
				//if (_chLosses.ChartLabels.LabelsCollection.
				string s1 = _chLosses.ChartLabels.LabelsCollection[string.Format("x{0}", p)].Text;
				string lname = string.Format("s{0}p{1}", s, p);
				string s2 = "";
				if (_chLosses.ChartLabels.LabelsCollection[lname] != null) {
					s2 = _chLosses.ChartLabels.LabelsCollection[lname].Text;
					s2 = s2.Replace(Environment.NewLine, ", ");
				} else if (tag.StartsWith(Res.strTarget) || tag.StartsWith(Res.strGap))
					s2 = tag;
				Tt.SetToolTip(e.X, e.Y, chart, string.Format("{0}{1}{2}", s1, Environment.NewLine, s2)); // Tt.SetToolTip(chart, string.Format("{0}{1}{2}", s1, Environment.NewLine, s2));
			} else {
				Tt.SetToolTip();//Tt.SetToolTip(chart, string.Empty);
				chart.Cursor = Cursors.Default;
			}
		}

		private void chLosses_MouseDown(object sender, MouseEventArgs e)
		{
			C1Chart chart = sender as C1Chart;
			int s = -1, p = -1, d = -1;
			Color c = Color.White;

			if (e != null && e.Button == MouseButtons.Right) {
				// навигация назад (по стеку)
				if (_chLosses.NavigationStack.Count == 0) {
					BuildLosses();
					return;
				}
				_chLosses.NavigationStack.Pop();
				if (_chLosses.NavigationStack.Count == 0) {
					BuildLosses();
					return;
				}
			} else {
				ChartGroup grp = chart.ChartGroups[0];
				if ((grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1)) {
					string tag = _chLosses.GetPointTag(0, s, p);
					if (tag != "end" && !tag.StartsWith(Res.strTarget) && !tag.StartsWith(Res.strGap) && tag != "" && !_chLosses.NavigationStack.Contains(tag))
						_chLosses.NavigationStack.Push(tag);
					if (tag == "end" || tag.StartsWith(Res.strTarget) && tag.StartsWith(Res.strGap))
						return;
				} else
					return;
			}
			SetCursWait();
			////mark_filter = "";
			BuildTabLosses();
			BuildChartLossessMarks();
			SetCursDefault();
		}

		private void chLosses_MouseLeave(object sender, EventArgs e)
		{
			_chLosses.Cursor = Cursors.Default;
		}

		private void chLosses_DrawDataSeries(object sender, DrawDataSeriesEventArgs e)
		{
			// HatchBrush не работает в PDF отчете, поэтому просто выходим
			if (nowDrawReport)
				return;

			int x = (int)e.Bounds.X + 1;
			int y = (int)e.Bounds.Y + 1;
			int s = -1, p = -1, d = -1;
			ChartGroup grp = _chLosses.ChartGroups[0];
			ChartData chd = grp.ChartData;
			Color color1, color0 = Color.White;

			if (grp.CoordToDataIndex(x, y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1) {
				//Debug.WriteLine("chLosses_DrawDataSeries s: " + s.ToString() + " p: " + p.ToString());
				color1 = MU.Kpi.Colors[_chLosses.GetPointRoot(0, 0, p)];
				if (chd.SeriesList[s].Tag.ToString() == "0")
					e.Brush = new SolidBrush(color1);
				else if (chd.SeriesList[s].Tag.ToString() == "-1")
					e.Brush = new HatchBrush(HatchStyle.LightUpwardDiagonal, color0, color1);
				else if (chd.SeriesList[s].Tag.ToString() == "1")
					e.Brush = new HatchBrush(HatchStyle.BackwardDiagonal, color0, color1);
				else if (chd.SeriesList[s].Tag.ToString() == "t")
					e.Brush = new HatchBrush(HatchStyle.HorizontalBrick, color1, color0);
				else if (chd.SeriesList[s].Tag.ToString() == "g") 
					e.Brush = new HatchBrush(HatchStyle.HorizontalBrick, color0, color1);
				// хз надо или нет
				e.DisposeBrush = true;
			} else if (e.IsLegend && chd.SeriesList.Count > 0) {
				color1 = Color.DimGray;
				if (chd.SeriesList[e.SeriesIndex].Tag.ToString() == "0")
					e.Brush = new SolidBrush(color1);
				else if (chd.SeriesList[e.SeriesIndex].Tag.ToString() == "-1")
					e.Brush = new HatchBrush(HatchStyle.LightUpwardDiagonal, color0, color1);
				else if (chd.SeriesList[e.SeriesIndex].Tag.ToString() == "1")
					e.Brush = new HatchBrush(HatchStyle.BackwardDiagonal, color0, color1);
				else if (chd.SeriesList[e.SeriesIndex].Tag.ToString() == "t")
					e.Brush = new HatchBrush(HatchStyle.HorizontalBrick, color1, color0);
				else if (chd.SeriesList[e.SeriesIndex].Tag.ToString() == "g") 
					e.Brush = new HatchBrush(HatchStyle.HorizontalBrick, color0, color1);
				e.DisposeBrush = true;
			}
		}


		private void chMarks_MouseMove(object sender, MouseEventArgs e)
		{
			//return;
			if (IsEmpty()) return;

			// to reduce CPU workload
			Thread.Sleep(5);

			int s = -1, p = -1, d = -1;
			ChartGroup grp = _chMarks.ChartGroups[0];

			if (grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d) && d < 1 && s >= 0) {
				Tt.SetToolTip(e.X, e.Y, _chMarks, _chMarks.ChartLabels.LabelsCollection[string.Format("s{0}p{1}", s, p)].Text);
			} else {
				Tt.SetToolTip();
			}
		}

		private void chMarks_MouseDown(object sender, MouseEventArgs e)
		{
			int s = -1, p = -1, d = -1;
			ChartGroup grp = _chMarks.ChartGroups[0];
			if (grp.CoordToDataIndex(e.X, e.Y, CoordinateFocusEnum.XandYCoord, ref s, ref p, ref d)
							&& d < 1 && s >= 0) {
				string tag = _chMarks.ChartGroups[0].ChartData.SeriesList[s].Tag.ToString();
				// по событию выбора соответствующей колонки будет изменен вид chMarks
				if (_chMarks.ChartGroups[0].ChartData.SeriesList[s].Offset == 30)
					_grLosses.Col = _grLosses.Cols["Time"].SafeIndex;
				else if (_grLosses.Cols.Contains(tag))
					_grLosses.Col = _grLosses.Cols[tag].SafeIndex;
			}
		}


		#endregion | Chart events            |


		private float GetTimeForTrends(DataRow dr)
		{
			if (dr == null)
				return 0;
			if (MU.Kpi.Denominators.ContainsKey(_trendCol) && MU.Kpi.Measure.ContainsKey(_trendCol)) {
				if (MU.Kpi.Measure[_trendCol] == 2)
					return C.GetFloat(dr[MU.Kpi.Denominators[_trendCol]]) / 100;
				if (MU.Kpi.Measure[_trendCol] == 1)
					return GetTimeDivider() * C.GetFloat(dr[MU.Kpi.Denominators[_trendCol]]);
			} else if (!MU.Kpi.ID.ContainsKey(_trendCol))
				return C.GetFloat(dr["TF"]) / 100;
			else if (MU.Kpi.Measure.ContainsKey(_trendCol) && MU.Kpi.Measure[_trendCol] != 1)
				return 1F;
			return GetTimeDivider();
		}

		private float GetTimeForTrends(DataRowView drv)
		{
			return GetTimeForTrends(drv.Row);
		}

		private float GetTimeDivider()
		{
			return TmStyle == TimeStyle.MMM ? 1F : 60F;
		}


		private C1.Win.C1Chart.Label AddValueLabel(C1Chart c1ch, string val, int s, int p, bool south)
		{
			C1.Win.C1Chart.Label lab = c1ch.ChartLabels.LabelsCollection.AddNewLabel();
			lab.Compass = south ? LabelCompassEnum.South : LabelCompassEnum.North;
			lab.Name = string.Format("s{0}p{1}", s, p);

			lab.Text = val;
			lab.Visible = true;
			lab.Offset = 5;
			lab.Connected = true;

			lab.AttachMethod = AttachMethodEnum.DataIndex;
			lab.AttachMethodData.GroupIndex = 0;
			lab.AttachMethodData.SeriesIndex = s;
			lab.AttachMethodData.PointIndex = p;

			lab.Style.ForeColor = Color.Black;
			lab.Style.BackColor = _colTranspWhite;
			lab.Style.Border.Thickness = 1;
			lab.Style.Border.Color = Color.Black;
			lab.Style.Border.Rounding.All = 2;
			lab.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
			return lab;
		}

		private Color GetColorTrends(int grade, int count, Color col)
		{
			grade = count - grade;
			int r = col.R + (255 - col.R) * grade / count;
			int g = col.G + (255 - col.G) * grade / count;
			int b = col.B + (255 - col.B) * grade / count;
			return Color.FromArgb(r, g, b);
		}

		private void tscbLossesBaseTime_SelectedIndexChanged(object sender, EventArgs e)
		{
			BuildLosses();
		}

		private string LossesBaseKpi
		{
			get
			{
				string kpikey = "K0";
				if (tscbLossesBaseTime.ComboBox.SelectedValue != null)
					kpikey = tscbLossesBaseTime.ComboBox.SelectedValue.ToString();
				return kpikey;
			}
		}

		private string LossesBaseTimeKey
		{
			get
			{
				string basetimekey = "T0";
				if (MU.Kpi.Denominators.ContainsKey(LossesBaseKpi))
					basetimekey = MU.Kpi.Denominators[LossesBaseKpi];
				return basetimekey;
			}
		}
	}
}

/*

1. На Парето строить отсечку в 80%
2. В основном гриде рисовать цели!!!!!!!!! 
3. 



*/