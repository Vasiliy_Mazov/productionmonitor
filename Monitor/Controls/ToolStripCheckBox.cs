﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace ProductionMonitor.Controls
{
    [ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.ToolStrip | ToolStripItemDesignerAvailability.StatusStrip)]
    class ToolStripCheckBox : CustomToolStripControlHost
    {
        public CheckBox CheckBox { get; set; }

        static Font CheckBoxFont;

        public ToolStripCheckBox() : base(CreateControlInstance())
        {
            CheckBox = Control as CheckBox;
            CheckBox.Paint += CheckBox_Paint;

            if (CheckBoxFont == null)
                CheckBoxFont = new Font(Font.FontFamily, Font.Size - 2F);

            CheckBox.Appearance = Appearance.Button;
            CheckBox.FlatStyle = FlatStyle.Flat;
            CheckBox.TextAlign = ContentAlignment.MiddleRight;
            CheckBox.FlatAppearance.BorderSize = 0;
            //CheckBox.AutoSize = true;
            CheckBox.Height = 16;
        }

        private void CheckBox_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(BackColor);

            using (SolidBrush brush = new SolidBrush(CheckBox.ForeColor))
                e.Graphics.DrawString(Text.ToUpper(), CheckBoxFont ?? Font, brush, 20, 7);

            var pt = new Point(4, 6);
            var rect = new Rectangle(pt, new Size(14, 14)); 

            e.Graphics.FillRectangle(Brushes.White, rect);

            if (CheckBox.Checked) {
                using (var brush = new SolidBrush(CheckBox.BackColor))
                using (var wing = new Font("Wingdings", 14f, FontStyle.Bold))
                    e.Graphics.DrawString("ü", wing, brush, 1, 2);
            }
            //e.Graphics.DrawRectangle(Pens.DarkSlateBlue, rect);

            //Rectangle fRect = CheckBox.ClientRectangle;

            //if (Focused) {
            //    fRect.Inflate(-1, -1);
            //    using (Pen pen = new Pen(Brushes.Gray) { DashStyle = System.Drawing.Drawing2D.DashStyle.Dot })
            //        e.Graphics.DrawRectangle(pen, fRect);
            //}
        }

        private static Control CreateControlInstance()
        {
            var c = new CheckBox();
            return c;
        }
    }


    public class CustomToolStripControlHost : ToolStripControlHost
    {
        public CustomToolStripControlHost()
            : base(new Control()) { }
        public CustomToolStripControlHost(Control c)
            : base(c) { }
    }
}
