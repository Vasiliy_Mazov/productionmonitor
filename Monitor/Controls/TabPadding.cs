﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProductionMonitor.Controls
{
    public class TabPadding : NativeWindow
    {
        private const int WM_PAINT = 0xF;

        private TabControl tabControl;

        public TabPadding(TabControl tc)
        {
            tabControl = tc;
            tabControl.Selected += new TabControlEventHandler(tabControl_Selected);
            AssignHandle(tc.Handle);
        }

        void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            tabControl.Invalidate();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_PAINT) {
                using (Graphics g = Graphics.FromHwnd(m.HWnd)) {
                    //Replace the outside white borders:
                    if (tabControl.Parent != null) {
                        g.SetClip(new Rectangle(0, 0, tabControl.Width - 2, tabControl.Height - 1), System.Drawing.Drawing2D.CombineMode.Exclude);
                        using (var sb = new SolidBrush(tabControl.Parent.BackColor))
                            g.FillRectangle(sb, new Rectangle(0,
                                                              tabControl.ItemSize.Height + 2,
                                                              tabControl.Width,
                                                              tabControl.Height - (tabControl.ItemSize.Height + 2)));
                    }
                    //Replace the inside white borders:
                    if (tabControl.SelectedTab != null) {
                        g.ResetClip();
                        Rectangle r = tabControl.SelectedTab.Bounds;
                        g.SetClip(r, System.Drawing.Drawing2D.CombineMode.Exclude);
                        using (var sb = new SolidBrush(tabControl.SelectedTab.BackColor))
                            g.FillRectangle(sb, new Rectangle(r.Left - 4,
                                                              r.Top - 5,
                                                              r.Width + 6,
                                                              r.Height + 6));
                    }
                }
            }
        }
    }
}
