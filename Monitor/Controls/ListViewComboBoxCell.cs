using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Diagnostics;

namespace ProductionMonitor
{
	//[SecurityPermissionAttribute(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
	public class DataGridViewListViewComboBoxEditingControl : ComboBox, IDataGridViewEditingControl
	{
		public DataGridViewListViewComboBoxEditingControl() //: base()
		{
			lv = new ListView();
			lv.BorderStyle = BorderStyle.None;
			lv.View = View.Details;
			lv.FullRowSelect = true;
			lv.MultiSelect = false;
			//lv.DoubleClick += new EventHandler(lv_DoubleClick);
			//lv.SelectedIndexChanged += new EventHandler(lv_SelectedIndexChanged);
			lv.KeyDown += new KeyEventHandler(lv_KeyDown);
			//lv.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(lv_ItemSelectionChanged);
			lv.MouseClick += new MouseEventHandler(lv_MouseClick);
			
			// ���� �� ������� �����, �� ������ �� ��������� ������������ ����� (���� � ���������� ���� Windows ������ ������� �����)
			lv.Font = SystemFonts.MenuFont;

			listViewHost = new ToolStripControlHost(lv);
			listViewHost.AutoSize = false;

			dropDown = new ToolStripDropDown();
			dropDown.Items.Add(listViewHost);
			//KeyDown += new KeyEventHandler(DataGridViewListViewComboBoxEditingControl_KeyDown);

			base.DropDownStyle = ComboBoxStyle.DropDownList;

			this.DataSourceChanged += new EventHandler(DataGridViewListViewComboBoxEditingControl_DataSourceChanged);

		}

		public ListView ListView
		{
			get { return lv; }
		}

		#region | private fields                           |

		private ToolStripControlHost listViewHost;
		private ToolStripDropDown dropDown;
		private DataGridView dgView;
		private bool valueChanged = false;
		private ListView lv;
		private int rowIndex;
		private const int WM_USER = 0x0400,
						WM_REFLECT = WM_USER + 0x1C00,
						WM_COMMAND = 0x0111,
						CBN_DROPDOWN = 7,
						CB_SHOWDROPDOWN = 0x14F;
		//public DataGridViewListViewComboBoxCell owner;

		#endregion

		#region | Implementing IDataGridViewEditingControl |

		public object EditingControlFormattedValue
		{
			get { return this.Text; }
			set { if (value is String) this.Text = value.ToString(); }
		}
		public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context)
		{
			return EditingControlFormattedValue;
		}
		public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle)
		{
			this.Font = dataGridViewCellStyle.Font;
		}
		public int EditingControlRowIndex
		{
			// Implements the IDataGridViewEditingControl.EditingControlRowIndex 
			// property.
			get { return rowIndex; }
			set { rowIndex = value; }
		}
		public bool EditingControlWantsInputKey(Keys key, bool dataGridViewWantsInputKey)
		{
			switch (key & Keys.KeyCode) {
				case Keys.Space:
					return true;
				default:
					return !dataGridViewWantsInputKey;
			}

			// Let the DateTimePicker handle the keys listed.
			switch (key & Keys.KeyCode) {
				case Keys.Left:
				case Keys.Up:
				case Keys.Down:
				case Keys.Right:
				case Keys.Home:
				case Keys.End:
				case Keys.PageDown:
				case Keys.PageUp:
				case Keys.Enter:
					return true;
				default:
					return !dataGridViewWantsInputKey;
			}
		}
		public void PrepareEditingControlForEdit(bool selectAll)
		{
			if (this.SelectedValue == null)
				return;
			foreach (ListViewItem li in lv.Items)
				if (li.Tag.ToString() == this.SelectedValue.ToString()) {
					li.Selected = true;
					li.EnsureVisible();
					li.Focused = true;
					return;
				}
		}
		public bool RepositionEditingControlOnValueChange
		{
			get { return false; }
		}
		public DataGridView EditingControlDataGridView
		{
			get { return dgView; }
			set { dgView = value; }
		}
		public bool EditingControlValueChanged
		{
			get { return valueChanged; }
			set { valueChanged = value; }
		}
		public Cursor EditingPanelCursor
		{
			get { return base.Cursor; }
		}

		#endregion

		////protected override void OnSelectedValueChanged(EventArgs e)
		////{
		////   //Debug.WriteLine("Sec1: " + DateTime.Now.Second.ToString() + "," + DateTime.Now.Millisecond.ToString());
		////   if (dropDown.Visible) {
		////      valueChanged = true;
		////      EditingControlDataGridView.NotifyCurrentCellDirty(true);
		////      EditingControlValueChanged = true;
		////   }
		////   base.OnSelectedValueChanged(e);
		////   //Debug.WriteLine("Sec2: " + DateTime.Now.Second.ToString() + "," + DateTime.Now.Millisecond.ToString());
		////}
		
		private void OnValueChanged()
		{
			this.valueChanged = true;
			if (dgView != null && dgView.ColumnCount > 0) {
				dgView.NotifyCurrentCellDirty(true);
			}
		}
		private void DataGridViewListViewComboBoxEditingControl_DataSourceChanged(object sender, EventArgs e)
		{
			Debug.WriteLine("DataGridViewListViewComboBoxEditingControl_DataSourceChanged ");
		}

		private void lv_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && lv.SelectedItems.Count != 0) {
				SelectedValue = lv.SelectedItems[0].Tag;
				HideDropDown();
				this.OnValueChanged();
			}
		}
		private void lv_MouseClick(object sender, MouseEventArgs e)
		{
			if (lv.SelectedItems.Count != 0) {
				//return;
				if ((int)SelectedValue != (int)lv.SelectedItems[0].Tag) {
					SelectedValue = (int)lv.SelectedItems[0].Tag;
					this.OnValueChanged();
				}
				HideDropDown();
			}
		}

		public void ShowDropDown()
		{
			if (dropDown != null && !dropDown.Visible) {
				AutoSizeDropDown();
				//DropDownWidth = Width + 100;
				//DropDownHeight = 300;
				listViewHost.Width = DropDownWidth;
				listViewHost.Height = DropDownHeight;
				dropDown.Show(this, 0, this.Height);
				ListView.Focus();
			}
		}
		private void HideDropDown()
		{
			if (dropDown != null) {
				dropDown.Hide();
			}
		}
		private void AutoSizeDropDown()
		{
			if (lv.Items.Count == 0)
				return;
			int newdropdownwidth = 0;
			int newheight = 0;
			for (int i = 0; i < lv.Columns.Count; i++) 
				newdropdownwidth += lv.Columns[i].Width;
			if (lv.Items.Count > MaxDropDownItems) {
				newheight = MaxDropDownItems * lv.GetItemRect(0).Height + SystemInformation.MenuHeight;
				newdropdownwidth += SystemInformation.VerticalScrollBarWidth;
			} else {
				newheight = lv.Items.Count * lv.GetItemRect(0).Height + SystemInformation.MenuHeight;
			}
			if (newdropdownwidth < Width)
				newdropdownwidth = Width;
			//SystemInformation.
			DropDownWidth = newdropdownwidth;
			DropDownHeight = newheight;
		}

		public static int HIWORD(int n)
		{
			return (n >> 16) & 0xffff;
		}
		protected override void WndProc(ref Message m)
		{
			// next two row instead of above sample
			if (m.Msg == (WM_REFLECT + WM_COMMAND)) {
				if (HIWORD((int)m.WParam) == CBN_DROPDOWN) {
					// Show/Hide dropped down based on current state
					if (dropDown != null && !dropDown.Visible)
						ShowDropDown();
					else
						HideDropDown();
					return;
				}
			}
			base.WndProc(ref m);
		}
		protected override void Dispose(bool disposing)
		{
			// remember to dispose the dropdown as it's not in the control collection. 
			if (disposing) {
				if (dropDown != null) {
					dropDown.Dispose();
					dropDown = null;
				}
			}
			base.Dispose(disposing);
		}
		[UIPermission(SecurityAction.LinkDemand, Window = UIPermissionWindow.AllWindows)]
		protected override bool ProcessDialogKey(Keys keyData)
		{
			Keys key = (keyData & Keys.KeyCode);
			Keys ctrl = (keyData & Keys.Modifiers);

			if (key == Keys.Space || (key == Keys.Down && ctrl == Keys.Alt)) {
				this.ShowDropDown();
				return true;//base.ProcessDialogKey(keyData); ;
			}
			return base.ProcessDialogKey(keyData);
		}
	}
	public class DataGridViewListViewComboBoxCell : DataGridViewComboBoxCell
	{
		public DataGridViewListViewComboBoxCell()
			: base()
		{
			base.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
		}

		#region | private fields                           |

		private DataGridViewListViewComboBoxEditingControl ctl;
		private ListView listview;
		private DataTable dtb;
		public DataGridViewListViewComboBoxColumn column;

		#endregion

		public override void InitializeEditingControl(int rowIndex, object initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle)
		{
			ctl = DataGridView.EditingControl as DataGridViewListViewComboBoxEditingControl;
			if (ctl.SelectedValue == null) {
				if (this.DataSource != null) {
					column = this.OwningColumn as DataGridViewListViewComboBoxColumn;
					listview = ctl.ListView;
					DataView dtv = this.DataSource as DataView;
					dtb = dtv.Table;
					if (dtb.GetHashCode() != C.GetInt(listview.Tag)) {
						listview.Tag = dtb.GetHashCode();
						listview.Columns.Clear();
						listview.Items.Clear();
						//dtb = this.DataSource as DataTable;
						ColumnHeader ch;
						foreach (string key in column.ColumnsMember.Keys) {
							ch = listview.Columns.Add(key, column.ColumnsMember[key]);
						}
						ListViewItem li;

						foreach (DataRowView dr in dtv) {
							li = null;
							foreach (string key in column.ColumnsMember.Keys) {
								if (li == null) {
									li = listview.Items.Add(dr[key].ToString());
									li.Tag = dr[column.ValueMember];
								} else if (key == "SpeedM")
									li.SubItems.Add(((double)dr[key]).ToString("0.#"));
								else
									li.SubItems.Add(dr[key].ToString());
							}
						}

						//listview.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
						foreach (string key in column.ColumnsMember.Keys) {
							listview.Columns[key].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
							listview.Columns[key].Width += 10;
						}
					}
				}
				base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
				return;
			}
			//return;
			//// ���� �������� ������� ����� ���������� �� ������ (???) ���������� ����������� �������� 
			////base.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle);
		}

		#region | public override props                    |

		public override Type EditType
		{
			get { return typeof(DataGridViewListViewComboBoxEditingControl); }
		}
		public override Type ValueType
		{
			get { return typeof(int); }
		}
		public override object DefaultNewRowValue
		{
			get { return DBNull.Value; }
		}

		#endregion

	}
	public class DataGridViewListViewComboBoxColumn : DataGridViewComboBoxColumn
	{
		public DataGridViewListViewComboBoxColumn()
		{
			this.CellTemplate = new DataGridViewListViewComboBoxCell();
		}

		#region | private fields                           |

		private Dictionary<string, string> columns = new Dictionary<string, string>();

		#endregion

		#region | public props                             |

		public Dictionary<string, string> ColumnsMember
		{
			get { return columns; }
			set { columns = value; }
		}
		
		#endregion

	}
}
