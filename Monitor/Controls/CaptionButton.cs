﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ProductionMonitor.Controls
{
    class CaptionButton : Button
    {
        private ButtonState _state = ButtonState.Normal;
        private Size szCaptionButton = SystemInformation.CaptionButtonSize;
        public CaptionButton() : base()
        {
            this.Size = szCaptionButton;
            this.Location = new Point(50, 50);
            this.Font = SystemFonts.IconTitleFont;
        }

        private Brush Brush { get; set; } = new SolidBrush(SystemColors.Control);

        public System.Windows.Forms.CaptionButton Type { get; set; }

        private void DrawFocus()
        {
            Graphics g = Graphics.FromHwnd(this.Handle);
            Rectangle r = Rectangle.Inflate(this.ClientRectangle, -4, -4);
            ControlPaint.DrawFocusRectangle(g, r);
            g.Dispose();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Brush b = over ? Brush :
                _state == ButtonState.Pushed ? SystemBrushes.Highlight : Brushes.White;
                e.Graphics.FillRectangle(b, e.ClipRectangle);
            var pen = Pens.Gray;

            var x = Width / 2 - 6;
            var y = Height / 2 - 6;
            var x2 = x + 12;
            var y2 = y + 12;
            switch (Type) {
                case System.Windows.Forms.CaptionButton.Minimize:
                    e.Graphics.DrawLine(pen, x, y2, x2, y2);
                    e.Graphics.DrawLine(pen, x, y2 - 1, x2, y2 - 1);
                    break;
                case System.Windows.Forms.CaptionButton.Maximize:
                    e.Graphics.DrawRectangle(pen, x - 2, y, x + 6, y + 7);
                    break;
                case System.Windows.Forms.CaptionButton.Restore:
                    var r = new Rectangle(x, y, x + 5, y + 5);
                    e.Graphics.DrawRectangle(pen, r);
                    r.Offset(-2, 2);
                    e.Graphics.FillRectangle(b, r);
                    e.Graphics.DrawRectangle(pen, r);
                    break;
                case System.Windows.Forms.CaptionButton.Close:
                    e.Graphics.DrawLine(pen, x, y, x2, y2);
                    e.Graphics.DrawLine(pen, x - 1, y, x2 - 1, y2);
                    e.Graphics.DrawLine(pen, x, y2, x2, y);
                    e.Graphics.DrawLine(pen, x - 1, y2, x2 - 1, y);
                    break;
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            DrawFocus();
            base.OnGotFocus(e);
        }
        protected override void OnLostFocus(EventArgs e)
        {
            Invalidate();
            base.OnLostFocus(e);
        }
        protected override void OnMouseEnter(EventArgs e)
        {
            over = true;
            DrawFocus();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            Invalidate();
            over = false;
            base.OnMouseLeave(e);
        }

        bool over = false;
        protected override void OnMouseHover(EventArgs e)
        {
            over = true;
            base.OnMouseHover(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            _state = ButtonState.Pushed;
            Invalidate();
            base.OnMouseDown(e);
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            _state = ButtonState.Normal;
            Invalidate();
            base.OnMouseUp(e);
        }
    }
}
