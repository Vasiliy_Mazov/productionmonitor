using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using ProductionMonitor.Controls;

namespace ProductionMonitor
{
	public class StripSelect : UserControl 
	{
		#region | Form Designer     |

		public StripSelect():base()
        {
			InitializeComponent();
            _mouseOverCount.Tick += _mouseOverCount_Tick;
		}

        private void _mouseOverCount_Tick(object sender, EventArgs e)
        {
			_mouseOverCount.Stop();
			MouseIsOver?.Invoke(this, new MouseIsOverArgs() { MouseState = _mouseState });
			if (_mouseState == MouseState.ToHide)
				_mouseState = MouseState.None;
		}

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
				components.Dispose();
			base.Dispose(disposing);
		}


		private System.ComponentModel.IContainer components;
		internal TreeView _tree;
		internal TreeView _tree2;
		internal MonthCalendar _mc;
		internal ToolTip _tt;
		internal Panel _pn;
		private VScrollBar _sb;
		private TextBox _txtN;
		private DateTimePicker _dtPickerFrom;
		private DateTimePicker _dtPickerTo;
		internal RadioButton _rbDays;
		internal RadioButton _rbWeeks;
		internal RadioButton _rbMonths;
        private Panel label1;
        private Panel _slideHeader;
        private Panel _pnLogo;
        private Button _btLog;
        private Button _btNow;
        private Button _btReport;
        private Button _btRefresh;
        private FlowLayoutPanel flowLayoutPanel2;
        private Panel panel2;
        private Panel panel1;
        private Button _btChart;
        private Label _lbDate;
        internal RadioButton _rbTime;

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this._tree = new System.Windows.Forms.TreeView();
            this._tree2 = new System.Windows.Forms.TreeView();
            this._pn = new System.Windows.Forms.Panel();
            this._rbDays = new System.Windows.Forms.RadioButton();
            this._dtPickerFrom = new System.Windows.Forms.DateTimePicker();
            this._txtN = new System.Windows.Forms.TextBox();
            this._sb = new System.Windows.Forms.VScrollBar();
            this._dtPickerTo = new System.Windows.Forms.DateTimePicker();
            this._mc = new System.Windows.Forms.MonthCalendar();
            this._rbWeeks = new System.Windows.Forms.RadioButton();
            this._rbMonths = new System.Windows.Forms.RadioButton();
            this._rbTime = new System.Windows.Forms.RadioButton();
            this._tt = new System.Windows.Forms.ToolTip(this.components);
            this._slideHeader = new System.Windows.Forms.Panel();
            this._lbDate = new System.Windows.Forms.Label();
            this._pnLogo = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this._btLog = new System.Windows.Forms.Button();
            this._btNow = new System.Windows.Forms.Button();
            this._btReport = new System.Windows.Forms.Button();
            this._btRefresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this._btChart = new System.Windows.Forms.Button();
            this._pn.SuspendLayout();
            this._slideHeader.SuspendLayout();
            this.label1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tree
            // 
            this._tree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._tree.ForeColor = System.Drawing.SystemColors.WindowText;
            this._tree.FullRowSelect = true;
            this._tree.HideSelection = false;
            this._tree.Location = new System.Drawing.Point(6, 2);
            this._tree.Name = "_tree";
            this._tree.Size = new System.Drawing.Size(250, 1);
            this._tree.TabIndex = 3;
            this._tree.TabStop = false;
            this._tree.Visible = false;
            this._tree.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.Tree_BeforeCheck);
            this._tree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.Tree_AfterCheck);
            this._tree.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StripSelect_KeyDown);
            // 
            // _tree2
            // 
            this._tree2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._tree2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._tree2.CheckBoxes = true;
            this._tree2.ForeColor = System.Drawing.SystemColors.WindowText;
            this._tree2.FullRowSelect = true;
            this._tree2.HideSelection = false;
            this._tree2.Location = new System.Drawing.Point(106, 657);
            this._tree2.Margin = new System.Windows.Forms.Padding(0);
            this._tree2.Name = "_tree2";
            this._tree2.Size = new System.Drawing.Size(328, 520);
            this._tree2.TabIndex = 3;
            this._tree2.TabStop = false;
            this._tree2.BeforeCheck += new System.Windows.Forms.TreeViewCancelEventHandler(this.Tree_BeforeCheck);
            this._tree2.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.Tree_AfterCheck);
            this._tree2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StripSelect_KeyDown);
            // 
            // _pn
            // 
            this._pn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._pn.BackColor = System.Drawing.Color.White;
            this._pn.Controls.Add(this._rbDays);
            this._pn.Controls.Add(this._dtPickerFrom);
            this._pn.Controls.Add(this._txtN);
            this._pn.Controls.Add(this._sb);
            this._pn.Controls.Add(this._dtPickerTo);
            this._pn.Controls.Add(this._mc);
            this._pn.Controls.Add(this._rbWeeks);
            this._pn.Controls.Add(this._rbMonths);
            this._pn.Controls.Add(this._rbTime);
            this._pn.Location = new System.Drawing.Point(106, 178);
            this._pn.Name = "_pn";
            this._pn.Size = new System.Drawing.Size(328, 470);
            this._pn.TabIndex = 0;
            // 
            // _rbDays
            // 
            this._rbDays.Checked = true;
            this._rbDays.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._rbDays.Location = new System.Drawing.Point(36, 79);
            this._rbDays.Name = "_rbDays";
            this._rbDays.Size = new System.Drawing.Size(143, 33);
            this._rbDays.TabIndex = 2;
            this._rbDays.TabStop = true;
            this._rbDays.Text = "Days";
            this._rbDays.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
            // 
            // _dtPickerFrom
            // 
            this._dtPickerFrom.CustomFormat = "";
            this._dtPickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._dtPickerFrom.Location = new System.Drawing.Point(12, 10);
            this._dtPickerFrom.Name = "_dtPickerFrom";
            this._dtPickerFrom.ShowUpDown = true;
            this._dtPickerFrom.Size = new System.Drawing.Size(246, 26);
            this._dtPickerFrom.TabIndex = 3;
            this._dtPickerFrom.TabStop = false;
            this._dtPickerFrom.ValueChanged += new System.EventHandler(this._dtFrom_ValueChanged);
            // 
            // _txtN
            // 
            this._txtN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._txtN.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._txtN.Location = new System.Drawing.Point(211, 122);
            this._txtN.MaxLength = 3;
            this._txtN.Name = "_txtN";
            this._txtN.Size = new System.Drawing.Size(45, 39);
            this._txtN.TabIndex = 4;
            this._txtN.TabStop = false;
            this._txtN.Text = "0";
            this._txtN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this._txtN.TextChanged += new System.EventHandler(this._txtN_TextChanged);
            this._txtN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._txtN_KeyPress);
            // 
            // _sb
            // 
            this._sb.LargeChange = 1;
            this._sb.Location = new System.Drawing.Point(209, 94);
            this._sb.Maximum = -1;
            this._sb.Minimum = -12;
            this._sb.Name = "_sb";
            this._sb.Size = new System.Drawing.Size(47, 94);
            this._sb.TabIndex = 1;
            this._sb.TabStop = true;
            this._sb.Value = -1;
            this._sb.ValueChanged += new System.EventHandler(this._sb_ValueChanged);
            // 
            // _dtPickerTo
            // 
            this._dtPickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this._dtPickerTo.Location = new System.Drawing.Point(12, 44);
            this._dtPickerTo.Name = "_dtPickerTo";
            this._dtPickerTo.ShowUpDown = true;
            this._dtPickerTo.Size = new System.Drawing.Size(245, 26);
            this._dtPickerTo.TabIndex = 5;
            this._dtPickerTo.TabStop = false;
            this._dtPickerTo.ValueChanged += new System.EventHandler(this._dtTo_ValueChanged);
            // 
            // _mc
            // 
            this._mc.Location = new System.Drawing.Point(12, 216);
            this._mc.MaxDate = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
            this._mc.MaxSelectionCount = 1;
            this._mc.MinDate = new System.DateTime(2005, 1, 1, 0, 0, 0, 0);
            this._mc.Name = "_mc";
            this._mc.ShowWeekNumbers = true;
            this._mc.TabIndex = 4;
            this._mc.TitleBackColor = System.Drawing.SystemColors.GrayText;
            this._mc.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this._mc_DateChanged);
            this._mc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StripSelect_KeyDown);
            // 
            // _rbWeeks
            // 
            this._rbWeeks.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._rbWeeks.Location = new System.Drawing.Point(36, 112);
            this._rbWeeks.Name = "_rbWeeks";
            this._rbWeeks.Size = new System.Drawing.Size(143, 33);
            this._rbWeeks.TabIndex = 2;
            this._rbWeeks.Text = "Weeks";
            this._rbWeeks.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
            // 
            // _rbMonths
            // 
            this._rbMonths.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._rbMonths.Location = new System.Drawing.Point(36, 145);
            this._rbMonths.Name = "_rbMonths";
            this._rbMonths.Size = new System.Drawing.Size(143, 33);
            this._rbMonths.TabIndex = 6;
            this._rbMonths.Text = "Months";
            this._rbMonths.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
            // 
            // _rbTime
            // 
            this._rbTime.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this._rbTime.Location = new System.Drawing.Point(36, 178);
            this._rbTime.Name = "_rbTime";
            this._rbTime.Size = new System.Drawing.Size(143, 33);
            this._rbTime.TabIndex = 7;
            this._rbTime.Text = "Any time";
            this._rbTime.CheckedChanged += new System.EventHandler(this._rb_CheckedChanged);
            // 
            // _tt
            // 
            this._tt.AutoPopDelay = 1000;
            this._tt.InitialDelay = 500;
            this._tt.ReshowDelay = 100;
            // 
            // _slideHeader
            // 
            this._slideHeader.Controls.Add(this._lbDate);
            this._slideHeader.Controls.Add(this._pnLogo);
            this._slideHeader.Controls.Add(this.panel1);
            this._slideHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this._slideHeader.Location = new System.Drawing.Point(0, 0);
            this._slideHeader.Name = "_slideHeader";
            this._slideHeader.Size = new System.Drawing.Size(450, 100);
            this._slideHeader.TabIndex = 5;
            // 
            // _lbDate
            // 
            this._lbDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._lbDate.ForeColor = System.Drawing.Color.White;
            this._lbDate.Location = new System.Drawing.Point(118, 23);
            this._lbDate.Name = "_lbDate";
            this._lbDate.Size = new System.Drawing.Size(316, 57);
            this._lbDate.TabIndex = 2;
            this._lbDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _pnLogo
            // 
            this._pnLogo.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_qsm;
            this._pnLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._pnLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this._pnLogo.Location = new System.Drawing.Point(0, 0);
            this._pnLogo.Name = "_pnLogo";
            this._pnLogo.Size = new System.Drawing.Size(96, 97);
            this._pnLogo.TabIndex = 0;
            this._pnLogo.MouseEnter += new System.EventHandler(this._pnLogo_MouseEnter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(0, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(450, 3);
            this.panel1.TabIndex = 1;
            // 
            // _btLog
            // 
            this._btLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btLog.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_log_100;
            this._btLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btLog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btLog.Location = new System.Drawing.Point(3, 11);
            this._btLog.Name = "_btLog";
            this._btLog.Size = new System.Drawing.Size(64, 64);
            this._btLog.TabIndex = 1;
            this._btLog.UseVisualStyleBackColor = false;
            // 
            // _btNow
            // 
            this._btNow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btNow.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_live_100;
            this._btNow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btNow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btNow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btNow.Location = new System.Drawing.Point(213, 11);
            this._btNow.Name = "_btNow";
            this._btNow.Size = new System.Drawing.Size(64, 64);
            this._btNow.TabIndex = 1;
            this._btNow.UseVisualStyleBackColor = false;
            // 
            // _btReport
            // 
            this._btReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btReport.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_excel_100;
            this._btReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btReport.Location = new System.Drawing.Point(143, 11);
            this._btReport.Name = "_btReport";
            this._btReport.Size = new System.Drawing.Size(64, 64);
            this._btReport.TabIndex = 1;
            this._btReport.UseVisualStyleBackColor = false;
            // 
            // _btRefresh
            // 
            this._btRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btRefresh.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_refresh_100;
            this._btRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btRefresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btRefresh.Location = new System.Drawing.Point(283, 11);
            this._btRefresh.Name = "_btRefresh";
            this._btRefresh.Size = new System.Drawing.Size(60, 64);
            this._btRefresh.TabIndex = 1;
            this._btRefresh.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.label1.Controls.Add(this.panel2);
            this.label1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 100);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(20);
            this.label1.Size = new System.Drawing.Size(100, 1078);
            this.label1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::ProductionMonitor.Properties.Resources.white_qsm_full_2;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(20, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(60, 744);
            this.panel2.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this._btLog);
            this.flowLayoutPanel2.Controls.Add(this._btChart);
            this.flowLayoutPanel2.Controls.Add(this._btReport);
            this.flowLayoutPanel2.Controls.Add(this._btNow);
            this.flowLayoutPanel2.Controls.Add(this._btRefresh);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(100, 100);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 8, 0, 8);
            this.flowLayoutPanel2.Size = new System.Drawing.Size(350, 86);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // _btChart
            // 
            this._btChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btChart.BackgroundImage = global::ProductionMonitor.Properties.Resources.chart;
            this._btChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btChart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btChart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._btChart.Location = new System.Drawing.Point(73, 11);
            this._btChart.Name = "_btChart";
            this._btChart.Size = new System.Drawing.Size(64, 64);
            this._btChart.TabIndex = 1;
            this._btChart.UseVisualStyleBackColor = false;
            // 
            // StripSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(144F, 144F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._slideHeader);
            this.Controls.Add(this._tree);
            this.Controls.Add(this._tree2);
            this.Controls.Add(this._pn);
            this.Name = "StripSelect";
            this.Size = new System.Drawing.Size(450, 1178);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StripSelect_KeyDown);
            this._pn.ResumeLayout(false);
            this._pn.PerformLayout();
            this._slideHeader.ResumeLayout(false);
            this.label1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion | Form Designer     |

		#region | Declaration       |

		private DateTime _dtEnd;
		private DateTime _dtStart;
		private int _unitsCount = 1;  //2;

		private MonitorConfig _config;
		private MonitorConfig_Counters _config_counters;


		private ArrayList _unitsIntArr;
		private ArrayList _unitsIntArr_Counters;

		private bool _inUse;

		public event EventHandler<DataChangesArgs> DataChanged;
		public event EventHandler ExcelReport;
		public event EventHandler<MouseIsOverArgs> MouseIsOver;
		public event EventHandler<TabChangesArgs> TabChanged;


		private Timer _mouseOverCount = new Timer() { Interval = 500 };
		private Timer _dateTimer = new System.Windows.Forms.Timer() { Interval = 1000 };
		private MouseState _mouseState = MouseState.None;

		public class DataChangesArgs : EventArgs
		{
			public DateTime DateStart;
			public DateTime DateEnd;
			public IList<MU> Lines;
		}

		public class TabChangesArgs : EventArgs	{ public TabState Tab; }

		public enum MouseState
		{
			None = 0,
			ToOpen = 1,
			ToHide = 2
		}

		public enum TabState
		{
			Chart = 1,
			Log = 2
		}

		public class MouseIsOverArgs : EventArgs
		{
			public MouseState MouseState;
		}

		#endregion | Declaration       |

		internal void Init(ArrayList Lns)
		{
			_unitsIntArr_Counters = Lns;

			Text = "Quality System Monitor";
			BackColor = Color.FromArgb(0x42, 0x9c, 0xe9);

			if (MonitorConfig.Instance == null)
				//return;
				if (MonitorConfig_Counters.Instance == null)
					return;

			SelectedUnits = new List<MU>();
			SelectedUnits_Counters = new List<MU>();
			_config = MonitorConfig.Instance;
			_config_counters = MonitorConfig_Counters.Instance;

			BuildTree(_tree.Nodes, 0);
			BuildTreeCounters(_tree2.Nodes, 0);

			_sb.Minimum = -frmMonitor.MaxLines;
			var ci = System.Threading.Thread.CurrentThread.CurrentCulture;
			_dtPickerFrom.CustomFormat = ci.DateTimeFormat.ShortDatePattern + "   HH:mm";       //"dd.MM.yyyy   HH:mm";
			_dtPickerTo.CustomFormat = ci.DateTimeFormat.ShortDatePattern + "   HH:mm";     //"dd.MM.yyyy   HH:mm";
			int h = SystemInformation.VerticalScrollBarArrowHeight;
			_txtN.Top = _sb.Top + h;
			_sb.Height = _txtN.Height + h * 2;

			SetTreeChecked_counters(_tree2.Nodes);
			GetFromTree_Counters(_tree2.Nodes);

			#region | Подбираем ширину |

			// Ищем самое длинное название
			string ln = "";
			foreach (MU un in _config_counters.MonitorUnits.Values)
				if (ln.Length < un.LineName.Trim().Length)
					ln = un.LineName;

			// Вычисляем ширину самого длинного названия
			int width = 0;
			using (var graphics = Graphics.FromImage(new Bitmap(1, 1)))
				width = (int)graphics.MeasureString(ln, _tree.Font).Width;
			Width = width;

			BackColor = Color.FromArgb(0x42, 0x9c, 0xe9);
			#endregion

			_btRefresh.Click += (s, e) => btOK_Click();
			_btNow.Click += (s, e) => {
				var d = DateTime.Now.Date;
				ShowWindow(d, d.AddDays(1));
				btOK_Click();
			};
			_btReport.Click += (s, e) => ExcelReport?.Invoke(this, new EventArgs());
			_btLog.Click += (s, e) => TabChanged?.Invoke(this, new TabChangesArgs() { Tab = TabState.Log });
			_btChart.Click += (s, e) => TabChanged?.Invoke(this, new TabChangesArgs() { Tab = TabState.Chart });

			//_lbDate.Font = Utils.StyleFont();
			_dateTimer.Enabled = true;
			_dateTimer.Start();
			_dateTimer.Tick += (s, e) => { _lbDate.Text = $"Date: {DateTime.Now.Date:d}{Environment.NewLine}Time: {DateTime.Now:T}"; };
		}

        internal MonitorConfig Config => _config;

        internal IList<MU> SelectedUnits { get; private set; }

		internal IList<MU> SelectedUnits_Counters { get; private set; }

		public void ShowWindow(DateTime DateStart, DateTime DateEnd)
		{
			_dtPickerFrom.Width =
			_dtPickerTo.Width = _mc.Width;

			_inUse = true;
			_dtEnd = DateEnd;
			_dtStart = DateStart;
			_mc.SetDate(_dtEnd.AddMinutes(-1));
			_sb.Value = -_unitsCount;
			_txtN.Text = _unitsCount.ToString();
			_inUse = false;

			_sb.Minimum = -frmMonitor.MaxTimes;
			SetItems();
			SetTreeChecked_counters(_tree2.Nodes);
		}


		#region | Buttons events    |

		private void btOK_Click()
		{
			foreach (MU mu in SelectedUnits)
				mu.Clear();
			SelectedUnits.Clear();
			SelectedUnits_Counters.Clear();
			if (_unitsIntArr != null)
				_unitsIntArr.Clear();
			if (_unitsIntArr_Counters != null)
				_unitsIntArr_Counters.Clear();

			GetFromTree(_tree.Nodes);
			GetFromTree_Counters(_tree2.Nodes);
			var arg = new DataChangesArgs();
			arg.DateStart = _dtStart;
			arg.DateEnd = _dtEnd;
			arg.Lines = SelectedUnits;
			//if (unitsIntArr.Count > 0)
			DataChanged?.Invoke(this, arg);

			// Запустим таймер автообновления если стоит галка
			if (frmMonitor._monitorSettings.AutoUpdate)
				frmMonitor._tmActivity.Start();
			else
				frmMonitor._tmActivity.Stop();
		}

		private void StripSelect_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Right) {
				if (_rbDays.Checked)
					_rbWeeks.Checked = true;
				else if (_rbWeeks.Checked)
					_rbMonths.Checked = true;
				else if (_rbMonths.Checked)
					_rbTime.Checked = true;
				else if (_rbTime.Checked)
					_rbDays.Checked = true;
			} else if (e.KeyCode == Keys.Down) {
				if (_sb.Value < _sb.Maximum)
					_sb.Value++;
			} else if (e.KeyCode == Keys.Up) {
				if (_sb.Value > _sb.Minimum)
					_sb.Value--;
			} else
				return;
			e.Handled = true;
		}

		private void btCancel_Click(System.Object sender, System.EventArgs e)
		{
			// Запустим таймер автообновления если стоит галка
			if (frmMonitor._monitorSettings.AutoUpdate)
				//frmMonitor._tmActivity.Start();
				frmMonitor.start_timer();
			else
				frmMonitor.stop_timer();
			// frmMonitor._tmActivity.Stop();

			//this.PerformClick();
		}

		#endregion | Menus & Buttons   |

		#region | Tree              |

		private void Tree_BeforeCheck(object sender, TreeViewCancelEventArgs e)
		{
			TreeNode nd = (TreeNode)e.Node;
			//  nd.
			//int k = Tree_CountChecked(_tree.Nodes);
			int k = Tree_CountChecked(nd.Nodes);

			k += Convert.ToInt32(nd.Checked ? -1 : 1);
			if (k > frmMonitor.MaxLines && nd.Checked == false) {
				_tt.SetToolTip(_tree, string.Format("The selection more than {0} units are not allowed", frmMonitor.MaxLines));
				e.Cancel = true;
			} else
				_tt.RemoveAll();
		}

		private void Tree_AfterCheck(object sender, TreeViewEventArgs e)
		{
			if (_inUse)
				return;
			_inUse = true;
			TreeNode nn = e.Node;
			Tree_CheckChilds(nn);
			_inUse = false;
		}

		private void Tree_CheckChilds(TreeNode n)
		{
			// рекурсивная функция
			foreach (TreeNode nn in n.Nodes) {
				nn.Checked = n.Checked;
				if (nn.Checked != n.Checked)
					return;
				if (n.Checked)
					nn.EnsureVisible();
				else {
					n.Collapse();
					nn.Collapse();
				}
				Tree_CheckChilds(nn);
			}
		}

		private int Tree_CountChecked(TreeNodeCollection nn)
		{
			// рекурсивная функция
			int k = 0;
			foreach (TreeNode n in nn) {
				if (n.Nodes.Count > 0)
					k += Tree_CountChecked(n.Nodes);
				else
					if (n.Checked)
					k++;
			}
			return k;
		}
		private void SetTreeChecked_counters(TreeNodeCollection nn)
		{
			foreach (TreeNode n in nn)
				if (n.Nodes.Count > 0)
					SetTreeChecked_counters(n.Nodes);
				else
					if (_unitsIntArr_Counters.Contains(((MU)n.Tag).ID)) {
					n.Checked = true;
					n.EnsureVisible();
				}
		}
		private void SetTreeChecked(TreeNodeCollection nn)
		{
			foreach (TreeNode n in nn)
				if (n.Nodes.Count > 0)
					SetTreeChecked(n.Nodes);
				else
					if (_unitsIntArr.Contains(((MU)n.Tag).ID)) {
					n.Checked = true;
					n.EnsureVisible();
				}
		}

		private void BuildTree(TreeNodeCollection tnc, int id)
		{
			if (tnc != null) 
				foreach (MU un in _config.MonitorUnits.Values) 
					if (un.ParentID == id) {
						var tn = new TreeNode(un.LineName);
						tn.Tag = un;
						tnc.Add(tn);
						BuildTree(tn.Nodes, un.ID);
					}
			
		}

		private void BuildTreeCounters(TreeNodeCollection tnc, int id)
		{
			if (tnc != null) 
				foreach (MU un in _config_counters.MonitorUnits.Values) 
					if (un.ParentID == id) {
						var tn = new TreeNode(un.LineName);
						tn.Tag = un;
						tnc.Add(tn);
						BuildTreeCounters(tn.Nodes, un.ID);
					}
		}

		private void GetFromTree(TreeNodeCollection tnc)
		{
			if (_config != null) {
				int cnt = 0;
				foreach (TreeNode tn in tnc) {
					if (tn.Checked) {
						var l = tn.Tag as MU;
						if (l != null) {
							SelectedUnits.Add(l);
							_unitsIntArr.Add(l.ID);
						}
						cnt++;
					}
					if (tn.Nodes.Count > 0)
						GetFromTree(tn.Nodes);
				}
			}
		}

		private void GetFromTree_Counters(TreeNodeCollection tnc)
		{
			if (_config_counters != null) {
				int cnt = 0;
				foreach (TreeNode tn in tnc) {
					if (tn.Checked) {
						var l = tn.Tag as MU;
						if (l != null) {
							SelectedUnits_Counters.Insert(0, l);
							_unitsIntArr_Counters.Insert(0, l.ID);
						}
						cnt++;
					}
					if (tn.Nodes.Count > 0)
						GetFromTree_Counters(tn.Nodes);
				}
			}
		}


		#endregion | Tree events       |

		#region | Control events    |

		private void _mc_DateChanged(object sender, DateRangeEventArgs e)
		{
			if (Visible == false) {
				// Запустим таймер автообновления если стоит галка
				if (frmMonitor._monitorSettings.AutoUpdate)
					if (frmMonitor._tmActivity.Enabled == false)
						frmMonitor._tmActivity.Start();
					else
						frmMonitor._tmActivity.Stop();
			}

			if (_inUse)
				return;
			_dtEnd = _mc.SelectionEnd.Date;
			//	Debug.WriteLine("__mc_DateChanged");
			SetItems();
		}

		private void _sb_ValueChanged(object sender, System.EventArgs e)
		{
			if (_inUse)
				return;
			// Debug.WriteLine("__sb_ValueChanged");
			SetItems();
		}

		private void _rb_CheckedChanged(object sender, System.EventArgs e)
		{
			if (_inUse || (sender as RadioButton).Checked == false)
				return;
			// Debug.WriteLine("__rb_CheckedChanged");
			SetItems();
		}

		private void _dtFrom_ValueChanged(object sender, EventArgs e)
		{
			if (_inUse)
				return;
			if (_dtPickerFrom.Value >= _dtPickerTo.Value) {
				_dtPickerTo.Value = _dtPickerFrom.Value.AddMinutes(1);
				return;
			}
			SetItems();
		}

		private void _dtTo_ValueChanged(object sender, EventArgs e)
		{
			if (_inUse)
				return;
			if (_dtPickerFrom.Value >= _dtPickerTo.Value) {
				_dtPickerFrom.Value = _dtPickerTo.Value.AddMinutes(-1);
				return;
			}
			SetItems();
		}

		private void _txtN_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (Char.GetNumericValue(e.KeyChar) < 0)
				e.Handled = true;
		}

		private void _txtN_TextChanged(object sender, EventArgs e)
		{
			if (_inUse)
				return;
			if (_txtN.Text != "") {
				int intText = int.Parse(_txtN.Text);
				if (intText <= frmMonitor.MaxTimes && intText > 0)
					_sb.Value = -intText;
				else if (intText > frmMonitor.MaxTimes)
					_sb.Value = -frmMonitor.MaxTimes;
				else if (intText < 1)
					_sb.Value = -1;
			}
		}


		#endregion


		private void SetItems()
		{
			if (_inUse)
				return;
			_inUse = true;

			_tree.CheckBoxes = true;
			_tree2.CheckBoxes = true;
			_sb.Enabled = !_rbTime.Checked;
			_txtN.Enabled = !_rbTime.Checked;

			bool TimeMode = _rbTime.Checked;
			_mc.Enabled = !TimeMode;
			_mc.ForeColor = TimeMode ? SystemColors.GrayText : SystemColors.WindowText;
			_dtPickerFrom.Enabled = TimeMode;
			_dtPickerTo.Enabled = TimeMode;

			_unitsCount = -_sb.Value;
			_txtN.Text = _unitsCount.ToString();

			MU.FixedDates = !_rbTime.Checked;
			if (TimeMode) {
				_dtStart = _dtPickerFrom.Value;
				_dtEnd = _dtPickerTo.Value.AddMinutes(1);
			} else {
				Calendar Cal = CultureInfo.InvariantCulture.Calendar;
				_dtEnd = _mc.SelectionEnd.Date;
				if (_rbDays.Checked) {
					_dtStart = _dtEnd.AddDays(-_unitsCount + 1);
					_dtEnd = _dtEnd.AddDays(1);
				} else if (_rbWeeks.Checked) {
					_dtEnd = _dtEnd.AddDays(7 - Convert.ToInt32(Cal.GetDayOfWeek(_dtEnd.AddDays(-1))));
					_dtStart = _dtEnd.AddDays(-7 * _unitsCount);
				} else if (_rbMonths.Checked) {
					_dtStart = _dtEnd.AddDays(-_dtEnd.Day + 1).AddMonths(-_unitsCount + 1);
					_dtEnd = _dtStart.AddMonths(_unitsCount);
				}

				_dtPickerFrom.Value = _dtStart;
				_dtPickerTo.Value = _dtEnd.AddMinutes(-1);
			}
			_inUse = false;
		}

        private void _pnLogo_MouseEnter(object sender, EventArgs e)
        {
			if (_mouseState != MouseState.ToOpen) {
				_mouseOverCount.Enabled = true;
				_mouseOverCount.Start();
				_mouseState = MouseState.ToOpen;
			}
		}

		protected override void OnControlAdded(ControlEventArgs e)
		{
			e.Control.MouseLeave += DidMouseReallyLeave;
			base.OnControlAdded(e);
		}
		protected override void OnMouseLeave(EventArgs e)
		{
			DidMouseReallyLeave(this, e);
		}
		private void DidMouseReallyLeave(object sender, EventArgs e)
		{
			if (this.ClientRectangle.Contains(this.PointToClient(Control.MousePosition)))
				return;
            if (_mouseState == MouseState.ToOpen) {
                _mouseOverCount.Enabled = true;
                _mouseOverCount.Start();
                _mouseState = MouseState.ToHide;
            }
            base.OnMouseLeave(e);
		}
	}
}