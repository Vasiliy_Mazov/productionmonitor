using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Resources;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Counters")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyCompany("Viravix")]
[assembly: AssemblyProduct("Counters")]
[assembly: AssemblyCopyright("Copyright � 2014")]
[assembly: AssemblyTrademark("Viravix")]
[assembly: CLSCompliant(true)]

//The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("82748A0A-B288-438b-B717-994B1FC25128")]
[assembly: ComVisibleAttribute(false)]

[assembly: AssemblyVersion("3.1.1031.23")]
[assembly: NeutralResourcesLanguageAttribute("en-GB")]

