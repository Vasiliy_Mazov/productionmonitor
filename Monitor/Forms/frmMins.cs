﻿using System;
using System.Data;
using System.Windows.Forms;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{
	public partial class frmMins : ShadowForm
	{
		private DataTable _dtb;
		private DataView _dv;

		public frmMins(DataTable dtb)
		{
			InitializeComponent();
			this._dtb = dtb;
		}

		private void frmMins_Load(object sender, EventArgs e)
		{
			Init();

			_btCancel.Text = Res.btCancel;
			_btOk.Text = Res.btApply;
			_btCopy.Text = Res.mnuCopy;
			//Text = Properties.Resources.grColsFormCaption;
		}

		private void Init()
		{
			// настраиваем таблицу
			_dv = _dtb.DefaultView;
			_dv.Sort = "dt";

			// настраиваем грид
			_dgv.AutoGenerateColumns = false;
			_dgv.AllowUserToAddRows = false;
			_dgv.DataSource = _dv;

			// создаем колонки в гриде
			_dgv.Columns.Add(Utils.AddColumnToGrid("DT", Res.strTime, true, 120, false, ""));
			_dgv.Columns.Add(Utils.AddColumnToGrid("Bottles", Res.strProduction, true, 80, false, ""));
			_dgv.Columns.Add(Utils.AddColumnToGrid("Seconds", Res.idleSeconds, true, 70, false, ""));

			foreach (DataGridViewColumn v in _dgv.Columns)
				v.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
		}

		private void _btCopy_Click(object sender, EventArgs e)
		{
			DataTable dtb = _dtb.Copy();
			dtb.Columns.Remove("Gap");
			dtb.Columns["DT"].Caption = Res.strTime;
			dtb.Columns["Bottles"].Caption = Res.strProduction;
			dtb.Columns["Seconds"].Caption = Res.idleSeconds;
			Utils.CopyDatatableToClipboard(dtb);
		}
	}
}