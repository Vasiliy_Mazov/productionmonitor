﻿namespace ProductionMonitor
{
	partial class frmColumns
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.btOk = new System.Windows.Forms.Button();
			this.btDefault = new System.Windows.Forms.Button();
			this.btDown = new System.Windows.Forms.Button();
			this.dtUp = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.dgv = new System.Windows.Forms.DataGridView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btOk);
			this.panel1.Controls.Add(this.btDefault);
			this.panel1.Controls.Add(this.btDown);
			this.panel1.Controls.Add(this.dtUp);
			this.panel1.Controls.Add(this.btCancel);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(5, 455);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(791, 34);
			this.panel1.TabIndex = 0;
			// 
			// btOk
			// 
			this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btOk.Location = new System.Drawing.Point(716, 10);
			this.btOk.Name = "btOk";
			this.btOk.Size = new System.Drawing.Size(75, 23);
			this.btOk.TabIndex = 0;
			this.btOk.Text = "Ok";
			this.btOk.UseVisualStyleBackColor = true;
			// 
			// btDefault
			// 
			this.btDefault.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btDefault.Location = new System.Drawing.Point(0, 11);
			this.btDefault.Name = "btDefault";
			this.btDefault.Size = new System.Drawing.Size(100, 23);
			this.btDefault.TabIndex = 0;
			this.btDefault.Text = global::ProductionMonitor.Properties.Resources.Default;
			this.btDefault.UseVisualStyleBackColor = true;
			this.btDefault.Click += new System.EventHandler(this.btDefault_Click);
			// 
			// btDown
			// 
			this.btDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btDown.Location = new System.Drawing.Point(505, 10);
			this.btDown.Name = "btDown";
			this.btDown.Size = new System.Drawing.Size(48, 23);
			this.btDown.TabIndex = 0;
			this.btDown.Text = "Down";
			this.btDown.UseVisualStyleBackColor = true;
			this.btDown.Visible = false;
			// 
			// dtUp
			// 
			this.dtUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.dtUp.Location = new System.Drawing.Point(451, 10);
			this.dtUp.Name = "dtUp";
			this.dtUp.Size = new System.Drawing.Size(48, 23);
			this.dtUp.TabIndex = 0;
			this.dtUp.Text = "Up";
			this.dtUp.UseVisualStyleBackColor = true;
			this.dtUp.Visible = false;
			// 
			// btCancel
			// 
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Location = new System.Drawing.Point(635, 10);
			this.btCancel.Name = "btCancel";
			this.btCancel.Size = new System.Drawing.Size(75, 23);
			this.btCancel.TabIndex = 0;
			this.btCancel.Text = "Cancel";
			this.btCancel.UseVisualStyleBackColor = true;
			// 
			// dgv
			// 
			this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
			this.dgv.EnableHeadersVisualStyles = false;
			this.dgv.Location = new System.Drawing.Point(1, 1);
			this.dgv.Name = "dgv";
			this.dgv.RowTemplate.Height = 18;
			this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dgv.Size = new System.Drawing.Size(789, 448);
			this.dgv.TabIndex = 1;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
			this.panel2.Controls.Add(this.dgv);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(5, 5);
			this.panel2.Name = "panel2";
			this.panel2.Padding = new System.Windows.Forms.Padding(1);
			this.panel2.Size = new System.Drawing.Size(791, 450);
			this.panel2.TabIndex = 2;
			// 
			// frmColumns
			// 
			this.AcceptButton = this.btOk;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btCancel;
			this.ClientSize = new System.Drawing.Size(801, 494);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "frmColumns";
			this.Padding = new System.Windows.Forms.Padding(5);
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Columns Customization";
			this.Load += new System.EventHandler(this.frmColumns_Load);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmColumns_FormClosing);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGridView dgv;
		private System.Windows.Forms.Button btOk;
		private System.Windows.Forms.Button btCancel;
		private System.Windows.Forms.Button dtUp;
		private System.Windows.Forms.Button btDown;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Button btDefault;
	}
}