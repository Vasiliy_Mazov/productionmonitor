﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProductionMonitor
{
	public partial class frmInfo : ShadowForm
	{
		readonly string m_content;
		DataSet _ds;
		public frmInfo(MU unit)
		{
			this._ds = unit.DataSet;
			InitializeComponent();
			txtInfo.BackColor = SystemColors.Info;

			using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(unit.Connection)) {
				StringBuilder sb = new StringBuilder();
				sb.AppendLine(String.Format("{0,-20} {1}", "DataSource", cn.DataSource));
				sb.AppendLine(String.Format("{0,-20} {1}", "Database", cn.Database));
				sb.AppendLine(String.Format("{0,-20} {1}", "Line ID", unit.ID));
				sb.AppendLine(String.Format("{0,-20} {1}", "TimeMin", unit.MinIdleTime));
				sb.AppendLine(String.Format("{0,-20} {1}", "Time Correction", unit.CorrectionTime));
				m_content = sb.ToString();
				txtInfo.Text = m_content;
			}
			dg.DataSource = unit.DataSet.Copy();
			dgYear.DataSource = unit.YearsDS.Copy();
			dg.Expand(-1);
			dgYear.Expand(-1);
			dgKPI.DataSource = MU.tbKPI.Copy();

			AddGridStyles(unit.DataSet);
			pg.SelectedObject = unit;
			pgStatic.SelectedObject = MU.Config;

			dg.Navigate += dg_Navigate;
		}
		public frmInfo(string HeaderInfo, DataSet[] en)
		{
			InitializeComponent();
			txtInfo.BackColor = SystemColors.Info;
			m_content = HeaderInfo;
			txtInfo.Text = HeaderInfo;
			dg.DataSource = _ds;

			foreach (DataSet dts in en) {
				_ds = dts;
				AddGridStyles(_ds);
				break;
			}
			dg.Navigate += dg_Navigate;
		}
		private void AddGridStyles(DataSet dts)
		{
			dg.TableStyles.Clear();
			foreach (DataTable dtb in dts.Tables) {
				DataGridTableStyle ts = new DataGridTableStyle { MappingName = dtb.TableName };
				foreach (DataColumn dc in dtb.Columns) {
					if (dc.DataType.Equals(typeof(DateTime))) {
						DataGridTextBoxColumn col = new DataGridTextBoxColumn { MappingName = dc.ColumnName, HeaderText = dc.ColumnName, Format = "yyyy.MM.dd HH:mm", Width = 100 };
						ts.GridColumnStyles.Add(col);
					} else if (dc.DataType.Equals(typeof(Single))) {
						DataGridTextBoxColumn col = new DataGridTextBoxColumn { MappingName = dc.ColumnName, HeaderText = dc.ColumnName, Width = 50, Alignment = HorizontalAlignment.Right, Format = "0.00" };
						ts.GridColumnStyles.Add(col);
					} else if (dc.DataType.Equals(typeof(Int32)) || dc.DataType.Equals(typeof(Int16))) {
						DataGridTextBoxColumn col = new DataGridTextBoxColumn { MappingName = dc.ColumnName, HeaderText = dc.ColumnName, Alignment = HorizontalAlignment.Right, Width = 50 };
						ts.GridColumnStyles.Add(col);
					} else if (dc.DataType.Equals(typeof(String))) {
						DataGridTextBoxColumn col = new DataGridTextBoxColumn { MappingName = dc.ColumnName, HeaderText = dc.ColumnName };
						ts.GridColumnStyles.Add(col);
					} else if (dc.DataType.Equals(typeof(Boolean))) {
						DataGridBoolColumn col = new DataGridBoolColumn { HeaderText = dc.ColumnName, MappingName = dc.ColumnName };
						ts.GridColumnStyles.Add(col);
					}
				}
				dg.TableStyles.Add(ts);
			}

		}

		private void dg_Navigate(object sender, NavigateEventArgs ne)
		{
			dg.CaptionText = dg.DataMember;
			txtInfo.Focus();
		}
		private void frmInfo_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.C && e.Modifiers == Keys.Control) {
				if (_ds.Tables.Contains(dg.CaptionText))
					Utils.CopyDatatableToClipboard(_ds.Tables[dg.CaptionText]);
			}
		}
		private void frmInfo_Load(object sender, EventArgs e)
		{
			Location = Cursor.Position;
		}

		private void btClose_Click(object sender, EventArgs e)
		{
			Close();
		}
		private void btExpand_Click(object sender, EventArgs e)
		{
			if (WindowState == FormWindowState.Normal)
				WindowState = FormWindowState.Maximized;
			else
				WindowState = FormWindowState.Normal;

		}
	}
}
