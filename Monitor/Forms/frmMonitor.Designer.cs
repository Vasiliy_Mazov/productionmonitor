﻿using System.Data;
using System.Windows.Forms;
using ProductionMonitor.Controls;

namespace ProductionMonitor
{
	public partial class frmMonitor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMonitor));
            this.ds_counters = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.cmDetailed = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmLinesGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmMap = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tmLoading = new System.Windows.Forms.Timer(this.components);
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsbMainPrev = new System.Windows.Forms.ToolStripButton();
            this.tsbMainNext = new System.Windows.Forms.ToolStripButton();
            this.tsbMainNow = new System.Windows.Forms.ToolStripButton();
            this.tsbMainRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsMainSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.cmMapCounter = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._tm_counter = new System.Windows.Forms.Timer(this.components);
            this.Tt = new ProductionMonitor.ToolTipMemo(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this._sideBar = new ProductionMonitor.StripSelect();
            this._dockPanel = new System.Windows.Forms.Panel();
            this._tabMain = new System.Windows.Forms.TabControl();
            this.tpCounters = new System.Windows.Forms.TabPage();
            this.pnMaps_counts = new System.Windows.Forms.Panel();
            this.cbShowTipsCounter = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tBox_Error = new System.Windows.Forms.TextBox();
            this.tpCounters_kpi = new System.Windows.Forms.TabPage();
            this.tC_Reports_counter = new System.Windows.Forms.TabControl();
            this.tP_CounterRep1 = new System.Windows.Forms.TabPage();
            this.c1FlexGrid1 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tP_CounterRep2 = new System.Windows.Forms.TabPage();
            this.c1FlexGrid2 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tP_CounterRep3 = new System.Windows.Forms.TabPage();
            this.c1FlexGrid3 = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Button();
            this.pnLines = new System.Windows.Forms.Panel();
            this._pnMainFormButtons = new System.Windows.Forms.Panel();
            this._btSide = new System.Windows.Forms.CheckBox();
            this.closeButton = new ProductionMonitor.Controls.CaptionButton();
            this.maximizeButton = new ProductionMonitor.Controls.CaptionButton();
            this.minimizeButton = new ProductionMonitor.Controls.CaptionButton();
            this._ss = new System.Windows.Forms.StatusStrip();
            this._tsProgBar = new System.Windows.Forms.ToolStripProgressBar();
            this.sbpLine = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpMins = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpBrand = new System.Windows.Forms.ToolStripStatusLabel();
            this.sbpDesc = new System.Windows.Forms.ToolStripStatusLabel();
            this._tsLang = new System.Windows.Forms.ToolStripDropDownButton();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            ((System.ComponentModel.ISupportInitialize)(this.ds_counters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            this.tsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this._dockPanel.SuspendLayout();
            this._tabMain.SuspendLayout();
            this.tpCounters.SuspendLayout();
            this.pnMaps_counts.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tpCounters_kpi.SuspendLayout();
            this.tC_Reports_counter.SuspendLayout();
            this.tP_CounterRep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).BeginInit();
            this.panel3.SuspendLayout();
            this.tP_CounterRep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).BeginInit();
            this.panel1.SuspendLayout();
            this.tP_CounterRep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid3)).BeginInit();
            this.panel4.SuspendLayout();
            this._pnMainFormButtons.SuspendLayout();
            this._ss.SuspendLayout();
            this.SuspendLayout();
            // 
            // ds_counters
            // 
            this.ds_counters.DataSetName = "ds_counters";
            this.ds_counters.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1,
            this.dataTable2,
            this.dataTable3});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dataTable1.TableName = "Counters_kpi";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Название";
            this.dataColumn1.ColumnName = "Name";
            this.dataColumn1.ReadOnly = true;
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Max";
            this.dataColumn2.ColumnName = "Max_";
            this.dataColumn2.DataType = typeof(decimal);
            this.dataColumn2.ReadOnly = true;
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Min";
            this.dataColumn3.ColumnName = "Min_";
            this.dataColumn3.DataType = typeof(decimal);
            this.dataColumn3.ReadOnly = true;
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Average";
            this.dataColumn4.ColumnName = "Avg_";
            this.dataColumn4.DataType = typeof(decimal);
            this.dataColumn4.ReadOnly = true;
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "id";
            this.dataColumn5.ReadOnly = true;
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "error";
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn17});
            this.dataTable2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "dt",
                        "Counters"}, true)});
            this.dataTable2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn15,
        this.dataColumn14};
            this.dataTable2.TableName = "Counters_rep2";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Дата";
            this.dataColumn7.ColumnName = "Date";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Время";
            this.dataColumn8.ColumnName = "Time";
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Brand";
            this.dataColumn9.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn9.ColumnName = "Brand";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn13.ColumnName = "error";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.Caption = "Наименование";
            this.dataColumn14.ColumnName = "Counters";
            // 
            // dataColumn15
            // 
            this.dataColumn15.AllowDBNull = false;
            this.dataColumn15.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn15.ColumnName = "dt";
            this.dataColumn15.DataType = typeof(System.DateTime);
            // 
            // dataColumn17
            // 
            this.dataColumn17.Caption = "Режим Линии";
            this.dataColumn17.ColumnName = "Discharg";
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn16});
            this.dataTable3.TableName = "Counters_errors";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Название";
            this.dataColumn10.ColumnName = "Name";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "Дата/Время";
            this.dataColumn11.ColumnName = "dt_from";
            this.dataColumn11.DataType = typeof(System.DateTime);
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Результат ID";
            this.dataColumn12.ColumnName = "id_deviation";
            this.dataColumn12.DataType = typeof(int);
            // 
            // dataColumn16
            // 
            this.dataColumn16.Caption = "Результат";
            this.dataColumn16.ColumnName = "deviation_name";
            // 
            // cmDetailed
            // 
            this.cmDetailed.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmDetailed.Name = "cmGrid";
            this.cmDetailed.Size = new System.Drawing.Size(61, 4);
            // 
            // cmLinesGrid
            // 
            this.cmLinesGrid.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmLinesGrid.Name = "cmLines";
            this.cmLinesGrid.Size = new System.Drawing.Size(61, 4);
            // 
            // cmMap
            // 
            this.cmMap.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmMap.Name = "cmMap";
            this.cmMap.Size = new System.Drawing.Size(61, 4);
            this.cmMap.MouseLeave += new System.EventHandler(this.cmMap_MouseLeave);
            // 
            // _tmLoading
            // 
            this._tmLoading.Tick += new System.EventHandler(this.tmLoading_Tick);
            // 
            // tsMain
            // 
            this.tsMain.CanOverflow = false;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.ImageScalingSize = new System.Drawing.Size(22, 22);
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbMainPrev,
            this.tsbMainNext,
            this.tsbMainNow,
            this.tsbMainRefresh,
            this.tsMainSeparator,
            this.toolStripSeparator1,
            this.toolStripLabel1});
            this.tsMain.Location = new System.Drawing.Point(0, 24);
            this.tsMain.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.tsMain.Name = "tsMain";
            this.tsMain.Padding = new System.Windows.Forms.Padding(10, 0, 1, 0);
            this.tsMain.Size = new System.Drawing.Size(846, 29);
            this.tsMain.TabIndex = 4;
            this.tsMain.Text = "tools";
            // 
            // tsbMainPrev
            // 
            this.tsbMainPrev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainPrev.Image = global::ProductionMonitor.Properties.Resources.prev;
            this.tsbMainPrev.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainPrev.Name = "tsbMainPrev";
            this.tsbMainPrev.Size = new System.Drawing.Size(34, 24);
            this.tsbMainPrev.Text = "Previous";
            this.tsbMainPrev.ToolTipText = "to";
            // 
            // tsbMainNext
            // 
            this.tsbMainNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainNext.Image = global::ProductionMonitor.Properties.Resources.next;
            this.tsbMainNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainNext.Name = "tsbMainNext";
            this.tsbMainNext.Size = new System.Drawing.Size(34, 24);
            this.tsbMainNext.Text = "Next";
            // 
            // tsbMainNow
            // 
            this.tsbMainNow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainNow.Image = global::ProductionMonitor.Properties.Resources.first;
            this.tsbMainNow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainNow.Name = "tsbMainNow";
            this.tsbMainNow.Size = new System.Drawing.Size(34, 24);
            this.tsbMainNow.Text = "Now";
            this.tsbMainNow.Click += new System.EventHandler(this.btNow_Click);
            // 
            // tsbMainRefresh
            // 
            this.tsbMainRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbMainRefresh.Image = global::ProductionMonitor.Properties.Resources.refr;
            this.tsbMainRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMainRefresh.Name = "tsbMainRefresh";
            this.tsbMainRefresh.Size = new System.Drawing.Size(34, 24);
            this.tsbMainRefresh.Text = "Refresh";
            this.tsbMainRefresh.ToolTipText = "Refresh";
            // 
            // tsMainSeparator
            // 
            this.tsMainSeparator.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsMainSeparator.Name = "tsMainSeparator";
            this.tsMainSeparator.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 29);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(137, 24);
            this.toolStripLabel1.Text = "                         ";
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(846, 24);
            this.MainMenu.TabIndex = 6;
            this.MainMenu.Text = "menuStrip1";
            // 
            // cmMapCounter
            // 
            this.cmMapCounter.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmMapCounter.Name = "cmMap";
            this.cmMapCounter.Size = new System.Drawing.Size(61, 4);
            this.cmMapCounter.Opening += new System.ComponentModel.CancelEventHandler(this.cmMapCounter_Opening);
            // 
            // _tm_counter
            // 
            this._tm_counter.Tick += new System.EventHandler(this._tm_counter_Tick);
            // 
            // Tt
            // 
            this.Tt.AutoPopDelay = 32767;
            this.Tt.InitialDelay = 500;
            this.Tt.ReshowDelay = 100;
            this.Tt.UseAnimation = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(2, 2);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this._sideBar);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this._dockPanel);
            this.splitContainer1.Panel2.Controls.Add(this._pnMainFormButtons);
            this.splitContainer1.Size = new System.Drawing.Size(1974, 908);
            this.splitContainer1.SplitterDistance = 450;
            this.splitContainer1.TabIndex = 9;
            // 
            // _sideBar
            // 
            this._sideBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._sideBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sideBar.Location = new System.Drawing.Point(0, 0);
            this._sideBar.Name = "_sideBar";
            this._sideBar.Size = new System.Drawing.Size(450, 908);
            this._sideBar.TabIndex = 0;
            // 
            // _dockPanel
            // 
            this._dockPanel.BackColor = System.Drawing.Color.White;
            this._dockPanel.Controls.Add(this._tabMain);
            this._dockPanel.Controls.Add(this.splitter1);
            this._dockPanel.Controls.Add(this.pnLines);
            this._dockPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dockPanel.Location = new System.Drawing.Point(0, 43);
            this._dockPanel.Name = "_dockPanel";
            this._dockPanel.Size = new System.Drawing.Size(1520, 865);
            this._dockPanel.TabIndex = 9;
            // 
            // _tabMain
            // 
            this._tabMain.Controls.Add(this.tpCounters);
            this._tabMain.Controls.Add(this.tpCounters_kpi);
            this._tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabMain.ItemSize = new System.Drawing.Size(78, 22);
            this._tabMain.Location = new System.Drawing.Point(0, 6);
            this._tabMain.Multiline = true;
            this._tabMain.Name = "_tabMain";
            this._tabMain.Padding = new System.Drawing.Point(0, 0);
            this._tabMain.SelectedIndex = 0;
            this._tabMain.Size = new System.Drawing.Size(1520, 859);
            this._tabMain.TabIndex = 1;
            this._tabMain.Visible = false;
            // 
            // tpCounters
            // 
            this.tpCounters.BackColor = System.Drawing.Color.White;
            this.tpCounters.Controls.Add(this.pnMaps_counts);
            this.tpCounters.Location = new System.Drawing.Point(4, 26);
            this.tpCounters.Name = "tpCounters";
            this.tpCounters.Padding = new System.Windows.Forms.Padding(3);
            this.tpCounters.Size = new System.Drawing.Size(1512, 829);
            this.tpCounters.TabIndex = 6;
            this.tpCounters.Text = "Counters";
            this.tpCounters.UseVisualStyleBackColor = true;
            // 
            // pnMaps_counts
            // 
            this.pnMaps_counts.AutoScroll = true;
            this.pnMaps_counts.BackColor = System.Drawing.Color.Transparent;
            this.pnMaps_counts.Controls.Add(this.cbShowTipsCounter);
            this.pnMaps_counts.Controls.Add(this.panel2);
            this.pnMaps_counts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMaps_counts.Location = new System.Drawing.Point(3, 3);
            this.pnMaps_counts.Name = "pnMaps_counts";
            this.pnMaps_counts.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.pnMaps_counts.Size = new System.Drawing.Size(1506, 823);
            this.pnMaps_counts.TabIndex = 3;
            // 
            // cbShowTipsCounter
            // 
            this.cbShowTipsCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbShowTipsCounter.AutoSize = true;
            this.cbShowTipsCounter.Checked = true;
            this.cbShowTipsCounter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShowTipsCounter.Location = new System.Drawing.Point(1274, 3);
            this.cbShowTipsCounter.Name = "cbShowTipsCounter";
            this.cbShowTipsCounter.Size = new System.Drawing.Size(75, 24);
            this.cbShowTipsCounter.TabIndex = 2;
            this.cbShowTipsCounter.Text = "Show";
            this.cbShowTipsCounter.UseVisualStyleBackColor = true;
            this.cbShowTipsCounter.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.tBox_Error);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 20);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1506, 803);
            this.panel2.TabIndex = 0;
            this.panel2.Visible = false;
            // 
            // tBox_Error
            // 
            this.tBox_Error.AcceptsTab = true;
            this.tBox_Error.BackColor = System.Drawing.SystemColors.Info;
            this.tBox_Error.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tBox_Error.Location = new System.Drawing.Point(0, 745);
            this.tBox_Error.Multiline = true;
            this.tBox_Error.Name = "tBox_Error";
            this.tBox_Error.Size = new System.Drawing.Size(1506, 58);
            this.tBox_Error.TabIndex = 0;
            // 
            // tpCounters_kpi
            // 
            this.tpCounters_kpi.BackColor = System.Drawing.Color.White;
            this.tpCounters_kpi.Controls.Add(this.tC_Reports_counter);
            this.tpCounters_kpi.Location = new System.Drawing.Point(4, 26);
            this.tpCounters_kpi.Name = "tpCounters_kpi";
            this.tpCounters_kpi.Padding = new System.Windows.Forms.Padding(3);
            this.tpCounters_kpi.Size = new System.Drawing.Size(1512, 829);
            this.tpCounters_kpi.TabIndex = 7;
            this.tpCounters_kpi.Text = "Counters KPI";
            // 
            // tC_Reports_counter
            // 
            this.tC_Reports_counter.Controls.Add(this.tP_CounterRep1);
            this.tC_Reports_counter.Controls.Add(this.tP_CounterRep2);
            this.tC_Reports_counter.Controls.Add(this.tP_CounterRep3);
            this.tC_Reports_counter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tC_Reports_counter.Location = new System.Drawing.Point(3, 3);
            this.tC_Reports_counter.Name = "tC_Reports_counter";
            this.tC_Reports_counter.SelectedIndex = 0;
            this.tC_Reports_counter.Size = new System.Drawing.Size(1506, 823);
            this.tC_Reports_counter.TabIndex = 4;
            // 
            // tP_CounterRep1
            // 
            this.tP_CounterRep1.Controls.Add(this.c1FlexGrid1);
            this.tP_CounterRep1.Controls.Add(this.panel3);
            this.tP_CounterRep1.Location = new System.Drawing.Point(4, 29);
            this.tP_CounterRep1.Name = "tP_CounterRep1";
            this.tP_CounterRep1.Padding = new System.Windows.Forms.Padding(3);
            this.tP_CounterRep1.Size = new System.Drawing.Size(1498, 790);
            this.tP_CounterRep1.TabIndex = 0;
            this.tP_CounterRep1.Text = "Report1";
            this.tP_CounterRep1.UseVisualStyleBackColor = true;
            // 
            // c1FlexGrid1
            // 
            this.c1FlexGrid1.AllowEditing = false;
            this.c1FlexGrid1.AutoResize = false;
            this.c1FlexGrid1.ColumnInfo = resources.GetString("c1FlexGrid1.ColumnInfo");
            this.c1FlexGrid1.DataMember = "Counters_kpi";
            this.c1FlexGrid1.DataSource = this.ds_counters;
            this.c1FlexGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1FlexGrid1.Location = new System.Drawing.Point(3, 25);
            this.c1FlexGrid1.Name = "c1FlexGrid1";
            this.c1FlexGrid1.Rows.Count = 1;
            this.c1FlexGrid1.Rows.DefaultSize = 23;
            this.c1FlexGrid1.Size = new System.Drawing.Size(1492, 762);
            this.c1FlexGrid1.TabIndex = 2;
            this.c1FlexGrid1.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.button1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1492, 22);
            this.panel3.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Excel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tP_CounterRep2
            // 
            this.tP_CounterRep2.Controls.Add(this.c1FlexGrid2);
            this.tP_CounterRep2.Controls.Add(this.panel1);
            this.tP_CounterRep2.Location = new System.Drawing.Point(4, 29);
            this.tP_CounterRep2.Name = "tP_CounterRep2";
            this.tP_CounterRep2.Padding = new System.Windows.Forms.Padding(3);
            this.tP_CounterRep2.Size = new System.Drawing.Size(1498, 790);
            this.tP_CounterRep2.TabIndex = 1;
            this.tP_CounterRep2.Text = "Report2";
            this.tP_CounterRep2.UseVisualStyleBackColor = true;
            // 
            // c1FlexGrid2
            // 
            this.c1FlexGrid2.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.c1FlexGrid2.AllowEditing = false;
            this.c1FlexGrid2.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.c1FlexGrid2.AutoResize = false;
            this.c1FlexGrid2.ColumnInfo = resources.GetString("c1FlexGrid2.ColumnInfo");
            this.c1FlexGrid2.DataSource = this.ds_counters;
            this.c1FlexGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1FlexGrid2.Location = new System.Drawing.Point(3, 30);
            this.c1FlexGrid2.Name = "c1FlexGrid2";
            this.c1FlexGrid2.Rows.Count = 2;
            this.c1FlexGrid2.Rows.DefaultSize = 23;
            this.c1FlexGrid2.Size = new System.Drawing.Size(1492, 757);
            this.c1FlexGrid2.TabIndex = 4;
            this.c1FlexGrid2.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1492, 27);
            this.panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(180, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 24);
            this.label1.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(84, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Excel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Собрать";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tP_CounterRep3
            // 
            this.tP_CounterRep3.BackColor = System.Drawing.Color.Transparent;
            this.tP_CounterRep3.Controls.Add(this.c1FlexGrid3);
            this.tP_CounterRep3.Controls.Add(this.panel4);
            this.tP_CounterRep3.Location = new System.Drawing.Point(4, 29);
            this.tP_CounterRep3.Name = "tP_CounterRep3";
            this.tP_CounterRep3.Padding = new System.Windows.Forms.Padding(3);
            this.tP_CounterRep3.Size = new System.Drawing.Size(1498, 790);
            this.tP_CounterRep3.TabIndex = 2;
            this.tP_CounterRep3.Text = "Error_list";
            this.tP_CounterRep3.UseVisualStyleBackColor = true;
            // 
            // c1FlexGrid3
            // 
            this.c1FlexGrid3.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.c1FlexGrid3.AllowEditing = false;
            this.c1FlexGrid3.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.c1FlexGrid3.ColumnInfo = resources.GetString("c1FlexGrid3.ColumnInfo");
            this.c1FlexGrid3.DataMember = "Counters_errors";
            this.c1FlexGrid3.DataSource = this.ds_counters;
            this.c1FlexGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.c1FlexGrid3.Location = new System.Drawing.Point(3, 29);
            this.c1FlexGrid3.Name = "c1FlexGrid3";
            this.c1FlexGrid3.Rows.Count = 1;
            this.c1FlexGrid3.Rows.DefaultSize = 23;
            this.c1FlexGrid3.Size = new System.Drawing.Size(1492, 758);
            this.c1FlexGrid3.TabIndex = 5;
            this.c1FlexGrid3.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1492, 26);
            this.panel4.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(3, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Excel";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 1);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1520, 5);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            this.splitter1.Visible = false;
            // 
            // pnLines
            // 
            this.pnLines.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnLines.Location = new System.Drawing.Point(0, 0);
            this.pnLines.Name = "pnLines";
            this.pnLines.Padding = new System.Windows.Forms.Padding(3);
            this.pnLines.Size = new System.Drawing.Size(1520, 1);
            this.pnLines.TabIndex = 7;
            // 
            // _pnMainFormButtons
            // 
            this._pnMainFormButtons.Controls.Add(this._btSide);
            this._pnMainFormButtons.Controls.Add(this.closeButton);
            this._pnMainFormButtons.Controls.Add(this.maximizeButton);
            this._pnMainFormButtons.Controls.Add(this.minimizeButton);
            this._pnMainFormButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this._pnMainFormButtons.Location = new System.Drawing.Point(0, 0);
            this._pnMainFormButtons.Name = "_pnMainFormButtons";
            this._pnMainFormButtons.Size = new System.Drawing.Size(1520, 43);
            this._pnMainFormButtons.TabIndex = 10;
            // 
            // _btSide
            // 
            this._btSide.Appearance = System.Windows.Forms.Appearance.Button;
            this._btSide.BackgroundImage = global::ProductionMonitor.Properties.Resources.slider_button;
            this._btSide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this._btSide.Checked = true;
            this._btSide.CheckState = System.Windows.Forms.CheckState.Checked;
            this._btSide.Dock = System.Windows.Forms.DockStyle.Left;
            this._btSide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._btSide.ForeColor = System.Drawing.Color.White;
            this._btSide.Location = new System.Drawing.Point(0, 0);
            this._btSide.Name = "_btSide";
            this._btSide.Size = new System.Drawing.Size(43, 43);
            this._btSide.TabIndex = 1;
            this._btSide.UseVisualStyleBackColor = true;
            this._btSide.CheckedChanged += new System.EventHandler(this.SideChecked);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.BackColor = System.Drawing.Color.White;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.closeButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.closeButton.Location = new System.Drawing.Point(1478, 12);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(30, 20);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Click here";
            this.closeButton.Type = System.Windows.Forms.CaptionButton.Close;
            this.closeButton.UseVisualStyleBackColor = false;
            // 
            // maximizeButton
            // 
            this.maximizeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maximizeButton.BackColor = System.Drawing.Color.White;
            this.maximizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.maximizeButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.maximizeButton.Location = new System.Drawing.Point(1447, 12);
            this.maximizeButton.Name = "maximizeButton";
            this.maximizeButton.Size = new System.Drawing.Size(30, 20);
            this.maximizeButton.TabIndex = 0;
            this.maximizeButton.Text = "Click here";
            this.maximizeButton.Type = System.Windows.Forms.CaptionButton.Maximize;
            this.maximizeButton.UseVisualStyleBackColor = false;
            // 
            // minimizeButton
            // 
            this.minimizeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.minimizeButton.BackColor = System.Drawing.Color.White;
            this.minimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minimizeButton.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.minimizeButton.Location = new System.Drawing.Point(1416, 12);
            this.minimizeButton.Name = "minimizeButton";
            this.minimizeButton.Size = new System.Drawing.Size(30, 20);
            this.minimizeButton.TabIndex = 0;
            this.minimizeButton.Text = "_";
            this.minimizeButton.Type = System.Windows.Forms.CaptionButton.Minimize;
            this.minimizeButton.UseVisualStyleBackColor = false;
            // 
            // _ss
            // 
            this._ss.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(156)))), ((int)(((byte)(233)))));
            this._ss.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._ss.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsProgBar,
            this.sbpLine,
            this.sbpMins,
            this.sbpBrand,
            this.sbpDesc,
            this._tsLang});
            this._ss.Location = new System.Drawing.Point(2, 910);
            this._ss.Name = "_ss";
            this._ss.Size = new System.Drawing.Size(1974, 32);
            this._ss.SizingGrip = false;
            this._ss.TabIndex = 5;
            // 
            // _tsProgBar
            // 
            this._tsProgBar.Name = "_tsProgBar";
            this._tsProgBar.Size = new System.Drawing.Size(200, 24);
            this._tsProgBar.Step = 1;
            this._tsProgBar.Value = 1;
            this._tsProgBar.Visible = false;
            // 
            // sbpLine
            // 
            this.sbpLine.ForeColor = System.Drawing.Color.White;
            this.sbpLine.Name = "sbpLine";
            this.sbpLine.Size = new System.Drawing.Size(17, 25);
            this.sbpLine.Text = " ";
            // 
            // sbpMins
            // 
            this.sbpMins.ForeColor = System.Drawing.Color.White;
            this.sbpMins.Name = "sbpMins";
            this.sbpMins.Size = new System.Drawing.Size(17, 25);
            this.sbpMins.Text = " ";
            // 
            // sbpBrand
            // 
            this.sbpBrand.ForeColor = System.Drawing.Color.White;
            this.sbpBrand.Name = "sbpBrand";
            this.sbpBrand.Size = new System.Drawing.Size(17, 25);
            this.sbpBrand.Text = " ";
            // 
            // sbpDesc
            // 
            this.sbpDesc.ForeColor = System.Drawing.Color.White;
            this.sbpDesc.Name = "sbpDesc";
            this.sbpDesc.Size = new System.Drawing.Size(1844, 25);
            this.sbpDesc.Spring = true;
            // 
            // _tsLang
            // 
            this._tsLang.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._tsLang.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._tsLang.ForeColor = System.Drawing.Color.White;
            this._tsLang.Image = ((System.Drawing.Image)(resources.GetObject("_tsLang.Image")));
            this._tsLang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._tsLang.Name = "_tsLang";
            this._tsLang.Size = new System.Drawing.Size(64, 29);
            this._tsLang.Text = "ENG";
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "New item selection";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.miniToolStrip.Location = new System.Drawing.Point(204, 2);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(1512, 33);
            this.miniToolStrip.TabIndex = 0;
            // 
            // frmMonitor
            // 
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1978, 944);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this._ss);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.MainMenu;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "frmMonitor";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMonitor_Closing);
            this.Closed += new System.EventHandler(this.frmMonitor_Closed);
            this.Load += new System.EventHandler(this.frmMonitor_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMonitor_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.ds_counters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this._dockPanel.ResumeLayout(false);
            this._tabMain.ResumeLayout(false);
            this.tpCounters.ResumeLayout(false);
            this.pnMaps_counts.ResumeLayout(false);
            this.pnMaps_counts.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tpCounters_kpi.ResumeLayout(false);
            this.tC_Reports_counter.ResumeLayout(false);
            this.tP_CounterRep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tP_CounterRep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tP_CounterRep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.c1FlexGrid3)).EndInit();
            this.panel4.ResumeLayout(false);
            this._pnMainFormButtons.ResumeLayout(false);
            this._ss.ResumeLayout(false);
            this._ss.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}


        #endregion
		private ToolStrip tsMain;
		private ToolStripButton tsbMainPrev;
		private ToolStripButton tsbMainNext;
		private ToolStripButton tsbMainNow;
		private ToolStripButton tsbMainRefresh;
		private ToolStripSeparator tsMainSeparator;
		private MenuStrip MainMenu;
		private ContextMenuStrip cmDetailed, cmLinesGrid, cmMap;
        private ToolTipMemo Tt;
		internal System.Windows.Forms.Timer _tmLoading;
        private ContextMenuStrip cmMapCounter;
        private Timer _tm_counter;
        public DataSet ds_counters;
        private DataTable dataTable1;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripLabel toolStripLabel1;
        private DataTable dataTable2;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataColumn dataColumn13;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataTable dataTable3;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataColumn dataColumn12;
        private DataColumn dataColumn16;
        private DataColumn dataColumn17;
        public static Timer _tmActivity;
        private SplitContainer splitContainer1;
        private Panel _pnMainFormButtons;
        private Panel _dockPanel;
        private Button splitter1;
        private Panel pnLines;
        private StatusStrip _ss;
        private ToolStripStatusLabel sbpLine;
        private ToolStripStatusLabel sbpMins;
        private ToolStripStatusLabel sbpBrand;
        private ToolStripStatusLabel sbpDesc;
        private ToolStripProgressBar _tsProgBar;
        private StripSelect _sideBar;
        private ProductionMonitor.Controls.CaptionButton closeButton;
        private ProductionMonitor.Controls.CaptionButton maximizeButton;
        private ProductionMonitor.Controls.CaptionButton minimizeButton;
        private CheckBox _btSide;
        private ToolStripDropDownButton _tsLang;
        private TabControl _tabMain;
        private TabPage tpCounters;
        private Panel pnMaps_counts;
        private CheckBox cbShowTipsCounter;
        private Panel panel2;
        public TextBox tBox_Error;
        private TabPage tpCounters_kpi;
        private TabControl tC_Reports_counter;
        private TabPage tP_CounterRep1;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid1;
        private Panel panel3;
        private Button button1;
        private TabPage tP_CounterRep2;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid2;
        private Panel panel1;
        private Label label1;
        private Button button3;
        private Button button2;
        private TabPage tP_CounterRep3;
        private C1.Win.C1FlexGrid.C1FlexGrid c1FlexGrid3;
        private Panel panel4;
        private Button button4;
        private ToolStrip miniToolStrip;
    }
}