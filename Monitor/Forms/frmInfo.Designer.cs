﻿namespace ProductionMonitor
{
	partial class frmInfo
	{

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInfo));
			this.txtInfo = new System.Windows.Forms.TextBox();
			this.dg = new System.Windows.Forms.DataGrid();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.pg = new System.Windows.Forms.PropertyGrid();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.dgYear = new System.Windows.Forms.DataGrid();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.dgKPI = new System.Windows.Forms.DataGrid();
			this.tabPage6 = new System.Windows.Forms.TabPage();
			this.pgStatic = new System.Windows.Forms.PropertyGrid();
			((System.ComponentModel.ISupportInitialize)(this.dg)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgYear)).BeginInit();
			this.tabPage5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgKPI)).BeginInit();
			this.tabPage6.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtInfo
			// 
			this.txtInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtInfo.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.txtInfo.Location = new System.Drawing.Point(3, 3);
			this.txtInfo.Multiline = true;
			this.txtInfo.Name = "txtInfo";
			this.txtInfo.ReadOnly = true;
			this.txtInfo.Size = new System.Drawing.Size(786, 568);
			this.txtInfo.TabIndex = 0;
			this.txtInfo.TabStop = false;
			this.txtInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmInfo_KeyDown);
			// 
			// dg
			// 
			this.dg.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dg.DataMember = "";
			this.dg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dg.FlatMode = true;
			this.dg.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dg.Location = new System.Drawing.Point(3, 3);
			this.dg.Name = "dg";
			this.dg.PreferredColumnWidth = 50;
			this.dg.ReadOnly = true;
			this.dg.Size = new System.Drawing.Size(786, 568);
			this.dg.TabIndex = 2;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Controls.Add(this.tabPage6);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(800, 600);
			this.tabControl1.TabIndex = 3;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.dg);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(792, 574);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Dataset";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.txtInfo);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(792, 574);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Line info";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.pg);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(792, 574);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Properties";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// pg
			// 
			this.pg.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pg.Location = new System.Drawing.Point(0, 0);
			this.pg.Name = "pg";
			this.pg.Size = new System.Drawing.Size(792, 574);
			this.pg.TabIndex = 0;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.dgYear);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(792, 574);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "Year dataset";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// dgYear
			// 
			this.dgYear.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgYear.DataMember = "";
			this.dgYear.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgYear.FlatMode = true;
			this.dgYear.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgYear.Location = new System.Drawing.Point(3, 3);
			this.dgYear.Name = "dgYear";
			this.dgYear.PreferredColumnWidth = 50;
			this.dgYear.ReadOnly = true;
			this.dgYear.Size = new System.Drawing.Size(786, 568);
			this.dgYear.TabIndex = 3;
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.dgKPI);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(792, 574);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "KPI";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// dgKPI
			// 
			this.dgKPI.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dgKPI.DataMember = "";
			this.dgKPI.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dgKPI.FlatMode = true;
			this.dgKPI.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dgKPI.Location = new System.Drawing.Point(3, 3);
			this.dgKPI.Name = "dgKPI";
			this.dgKPI.PreferredColumnWidth = 50;
			this.dgKPI.ReadOnly = true;
			this.dgKPI.Size = new System.Drawing.Size(786, 568);
			this.dgKPI.TabIndex = 3;
			// 
			// tabPage6
			// 
			this.tabPage6.Controls.Add(this.pgStatic);
			this.tabPage6.Location = new System.Drawing.Point(4, 22);
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage6.Size = new System.Drawing.Size(792, 574);
			this.tabPage6.TabIndex = 5;
			this.tabPage6.Text = "Static members";
			this.tabPage6.UseVisualStyleBackColor = true;
			// 
			// pgStatic
			// 
			this.pgStatic.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pgStatic.Location = new System.Drawing.Point(3, 3);
			this.pgStatic.Name = "pgStatic";
			this.pgStatic.Size = new System.Drawing.Size(786, 568);
			this.pgStatic.TabIndex = 1;
			// 
			// frmInfo
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 600);
			this.Controls.Add(this.tabControl1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "frmInfo";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Info";
			this.TopMost = true;
			this.Load += new System.EventHandler(this.frmInfo_Load);
			((System.ComponentModel.ISupportInitialize)(this.dg)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgYear)).EndInit();
			this.tabPage5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgKPI)).EndInit();
			this.tabPage6.ResumeLayout(false);
			this.ResumeLayout(false);

		}



		#endregion

		private System.Windows.Forms.TextBox txtInfo;
		private System.Windows.Forms.DataGrid dg;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.PropertyGrid pg;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.DataGrid dgYear;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.DataGrid dgKPI;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.PropertyGrid pgStatic;
	}
}