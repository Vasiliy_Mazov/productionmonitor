﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
//using System.Runtime.InteropServices;
using System.Threading;
using System.Data.OleDb;
using C1.C1Excel;

namespace ProductionMonitor
{

	public partial class frmMaster : Form
	{

		#region | declare              |

		private bool _forceAppAccount = false;
		private MonitorConfig _config;
		private KPI _kpi;
		private MU _unit;
		private DataTable _tbKPI;
		private DataTable _metatbGlobal;
		private DataTable _metatbLocal;
		private DataTable _metatbLang;
		////private BindingSource bsGlobal;
		////private BindingSource bsLocal;
		////private BindingSource bsLang;
		private DataTable _dtbGlobal;
		private DataTable _dtbLocal;
		private DataTable _dtbLang;
		private DataTable _dtbTargets;
		private enum TbNames
		{
			Kpi,				//restricted
			Marks,			//restricted
			MarkBands,		//restricted
			Config,
			Stops,
			Brands,
			Formats,
			Shifts,
			Users,
            Counters_value
		}
		////private TbNames tbn;
		private Image _imSaveBlue = Properties.Resources.save_tiny_blue;
		private Image _imSaveRed = Properties.Resources.save_tiny_red;
		private int _newId = -1000;
		private MonitorUser _user;
		private Dictionary<int, SessData> _sessions = new Dictionary<int, SessData>();

		private Dictionary<string, string> _utMap = new Dictionary<string, string>();
		private Dictionary<string, DataTable> _tbMap = new Dictionary<string, DataTable>();
		private Dictionary<string, DataTable> _nmMap = new Dictionary<string, DataTable>();
		private Dictionary<int, DataSet> _locals = new Dictionary<int, DataSet>();
		private Dictionary<int, DataSet> _localsCash = new Dictionary<int, DataSet>();
		private List<int> ToUpdateCashDate = new List<int>();

		private TreeNode _node;
		private List<TreeNode> _nodes = new List<TreeNode>();
		////private Font f;
		////private Font fb;

		bool _ok = false;
		bool _none = false;
		bool _indexChangedInUse = false;
		bool _needToUpdate = false;
		bool _continueUpdateOnError = false;

		#endregion

		public frmMaster(MonitorConfig config)
		{
			InitializeComponent();
			this._config = config;
			Init();
		}
		public frmMaster()
		{
			InitializeComponent();
			_config = MonitorConfig.Instance;
			Init();
		}

		#region | at start             |

		private void Init()
		{
			foreach (MU u in _config.MonitorUnits.Values) {
				_utMap.Add(u.ID.ToString() + TbNames.Kpi.ToString(), u.ParentIdKpi.ToString() + TbNames.Kpi.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Marks.ToString(), u.ParentIdKpi.ToString() + TbNames.Marks.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.MarkBands.ToString(), u.ParentIdKpi.ToString() + TbNames.MarkBands.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Users.ToString(), u.ParentIdKpi.ToString() + TbNames.Users.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Config.ToString(), u.ParentIdConfig.ToString() + TbNames.Config.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Stops.ToString(), u.ParentIdStops.ToString() + TbNames.Stops.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Brands.ToString(), u.ParentSku.ToString() + TbNames.Brands.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Formats.ToString(), u.ParentIdSku.ToString() + TbNames.Formats.ToString());
				_utMap.Add(u.ID.ToString() + TbNames.Shifts.ToString(), u.ParentIdShifts.ToString() + TbNames.Shifts.ToString());

                _utMap.Add(u.ID.ToString() + TbNames.Counters_value.ToString(), u.Parent_Counters.ToString() + TbNames.Counters_value.ToString());
			}

			InitComboBox();

			cbGlobal.SelectedIndexChanged += new EventHandler(cbGlobal_SelectedIndexChanged);
			cbLocal.SelectedIndexChanged += new EventHandler(cbLocal_SelectedIndexChanged);
			cbLang.SelectedIndexChanged += new EventHandler(cbLang_SelectedIndexChanged);

			dgvGlobal.DataSourceChanged += new EventHandler(dgvGlobal_DataSourceChanged);
			dgvGlobal.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgvGlobal_RowPostPaint);
			dgvGlobal.RowEnter += new DataGridViewCellEventHandler(this.dgvGlobal_RowEnter);
			dgvGlobal.CellValidated += new DataGridViewCellEventHandler(this.dgvGlobal_CellValidated);
			dgvGlobal.CellValidating += new DataGridViewCellValidatingEventHandler(this.dgvGlobal_CellValidating);
			dgvGlobal.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvGlobal.Enter += new EventHandler(dgv_Enter);

			dgvLocal.CellFormatting += new DataGridViewCellFormattingEventHandler(dgvLocal_CellFormatting);
			dgvLocal.DataSourceChanged += new EventHandler(dgvLocal_DataSourceChanged);
			dgvLocal.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgvLocal_RowPostPaint);
			dgvLocal.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvLocal.Enter += new EventHandler(dgv_Enter);

			dgvLang.DataSourceChanged += new EventHandler(dgvLang_DataSourceChanged);
			dgvLang.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgvGlobal_RowPostPaint);
			dgvLang.CellValidated += new DataGridViewCellEventHandler(dgvLang_CellValidated);
			dgvLang.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvLang.RowEnter += new DataGridViewCellEventHandler(dgvLang_RowEnter);
			dgvLang.Enter += new EventHandler(dgv_Enter);

			dgvTargets.DataSourceChanged += new EventHandler(dgvTargets_DataSourceChanged);
			dgvTargets.RowPostPaint += new DataGridViewRowPostPaintEventHandler(dgvGlobal_RowPostPaint);
			dgvTargets.CellValidating += new DataGridViewCellValidatingEventHandler(dgvTargets_CellValidating);
			dgvTargets.CellValidated += new DataGridViewCellEventHandler(dgvTargets_CellValidated);
			dgvTargets.DataError += new DataGridViewDataErrorEventHandler(dgv_DataError);
			dgvTargets.RowEnter += new DataGridViewCellEventHandler(dgvTargets_RowEnter);
			dgvTargets.Enter += new EventHandler(dgv_Enter);

			dgvLocal.DefaultCellStyle.SelectionBackColor = dgvLocal.DefaultCellStyle.BackColor;
			dgvLocal.DefaultCellStyle.SelectionForeColor = dgvLocal.DefaultCellStyle.ForeColor;
			dgvLang.AllowUserToDeleteRows = false;
			dgvLang.AllowUserToAddRows = false;

			tpDnTm.Text = Properties.Resources.strDownTimes;
			tpGlobal.Text = Properties.Resources.mstGlobal;
			tpLocal.Text = Properties.Resources.mstLocal;
			tpLang.Text = Properties.Resources.mstLang;
			tpTargets.Text = Properties.Resources.mstTarget;
		}
		private void InitComboBox()
		{
			DataRow dr;

			#region | global |
			_metatbGlobal = new DataTable();
			_metatbGlobal.Columns.Add("id", typeof(string));
			_metatbGlobal.Columns.Add("caption", typeof(string));
			_metatbGlobal.Columns.Add("dt", typeof(DateTime));

			////DataRow dr = metatbGlobal.NewRow();
			////dr["id"] = TbNames.Kpi.ToString();
			////dr["caption"] = "KPI";
			////metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Stops.ToString();
			dr["caption"] = "Downtimes";
			_metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Formats.ToString();
			dr["caption"] = "Formats";
			_metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Brands.ToString();
			dr["caption"] = "Brands";
			_metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Shifts.ToString();
			dr["caption"] = "Shifts";
			_metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Users.ToString();
			dr["caption"] = "Users";
			_metatbGlobal.Rows.Add(dr);

			dr = _metatbGlobal.NewRow();
			dr["id"] = TbNames.Config.ToString();
			dr["caption"] = "Line settings";
			_metatbGlobal.Rows.Add(dr);


            dr = _metatbGlobal.NewRow();
            dr["id"] = TbNames.Counters_value.ToString();
            dr["caption"] = "Counters_value";
            _metatbGlobal.Rows.Add(dr);

			////dr = metatbGlobal.NewRow();
			////dr["id"] = TbNames.Marks.ToString();
			////dr["caption"] = "MarkList";
			////metatbGlobal.Rows.Add(dr);

			////dr = metatbGlobal.NewRow();
			////dr["id"] = TbNames.MarkBands.ToString();
			////dr["caption"] = "MarkBandList";
			////metatbGlobal.Rows.Add(dr);

			cbGlobal.ComboBox.DataSource = _metatbGlobal;
			cbGlobal.ComboBox.DisplayMember = "caption";
			cbGlobal.ComboBox.ValueMember = "id";
			#endregion

			#region | local  |
			_metatbLocal = new DataTable();
			_metatbLocal.Columns.Add("id", typeof(string));
			_metatbLocal.Columns.Add("caption", typeof(string));
			_metatbLocal.Columns.Add("dt", typeof(DateTime));

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Stops.ToString();
			dr["caption"] = "Downtimes";
			_metatbLocal.Rows.Add(dr);

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Formats.ToString();
			dr["caption"] = "Formats";
			_metatbLocal.Rows.Add(dr);

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Brands.ToString();
			dr["caption"] = "Brands";
			_metatbLocal.Rows.Add(dr);

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Shifts.ToString();
			dr["caption"] = "Shifts";
			_metatbLocal.Rows.Add(dr);

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Users.ToString();
			dr["caption"] = "Users";
			_metatbLocal.Rows.Add(dr);

			dr = _metatbLocal.NewRow();
			dr["id"] = TbNames.Config.ToString();
			dr["caption"] = "Line settings";
			_metatbLocal.Rows.Add(dr);

			cbLocal.ComboBox.DataSource = _metatbLocal;
			cbLocal.ComboBox.DisplayMember = "caption";
			cbLocal.ComboBox.ValueMember = "id";

			#endregion

			#region | lang   |

			_metatbLang = new DataTable();
			_metatbLang.Columns.Add("id", typeof(string));
			_metatbLang.Columns.Add("caption", typeof(string));
			_metatbLang.Columns.Add("dt", typeof(DateTime));

			dr = _metatbLang.NewRow();
			dr["id"] = TbNames.Kpi.ToString();
			dr["caption"] = "KPI";
			_metatbLang.Rows.Add(dr);

			dr = _metatbLang.NewRow();
			dr["id"] = TbNames.Stops.ToString();
			dr["caption"] = "Downtimes";
			_metatbLang.Rows.Add(dr);

			dr = _metatbLang.NewRow();
			dr["id"] = TbNames.Formats.ToString();
			dr["caption"] = "Formats";
			_metatbLang.Rows.Add(dr);

			dr = _metatbLang.NewRow();
			dr["id"] = TbNames.Brands.ToString();
			dr["caption"] = "Brands";
			_metatbLang.Rows.Add(dr);

			dr = _metatbLang.NewRow();
			dr["id"] = TbNames.Shifts.ToString();
			dr["caption"] = "Shifts";
			_metatbLang.Rows.Add(dr);

			//dr = metatbLang.NewRow();
			//dr["id"] = TbNames.Marks.ToString();
			//dr["caption"] = "MarkList";
			//metatbLang.Rows.Add(dr);

			//dr = metatbLang.NewRow();
			//dr["id"] = TbNames.MarkBands.ToString();
			//dr["caption"] = "MarkBandList";
			//metatbLang.Rows.Add(dr);

			cbLang.ComboBox.DataSource = _metatbLang;
			cbLang.ComboBox.DisplayMember = "caption";
			cbLang.ComboBox.ValueMember = "id";
			#endregion

		}
		private void frmMaster_Shown(object sender, EventArgs e)
		{
			Splash.Fadeout();
			SetCursWait();

			if (Control.ModifierKeys == Keys.Control)
				_forceAppAccount = true;
			LoadUnitTree(tv_units.Nodes, 0);
			tv_units.ExpandAll();
			if (tv_units.Nodes.Count > 0)
				tv_units.SelectedNode = tv_units.Nodes[0];

			bool extraadm = _user != null && _user.Admin && _user.Name.ToLower() == "admin";
			mnuFromExcel.Visible = extraadm;
			mnuToExcel.Visible = extraadm;

			SetCursDefault();
		}
		private void LoadUnitTree(TreeNodeCollection tnc, int id)
		{
			if (tnc == null) return;
			TreeNode tn;
			foreach (MU un in _config.MonitorUnits.Values) {
				if (un.ParentID == id) {
					tn = new TreeNode(un.LineName);
					tn.Tag = un;
					tnc.Add(tn);
					_nodes.Add(tn);
					LoadUnitTree(tn.Nodes, un.ID);
				}
			}
		}

		#endregion

		#region | controls events      |

		private void cbGlobal_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_indexChangedInUse)
				return;
			_indexChangedInUse = true;
			SetCursWait();

			SetGlobalSource();
			SetNodesColor();

			cbLocal.SelectedIndex = cbLocal.FindString(cbGlobal.Text);
			SetLocalSource();
			cbLang.SelectedIndex = cbLang.FindString(cbGlobal.Text);
			SetLangSource();

			_indexChangedInUse = false;
			SetCursDefault();
		}
		private void cbLocal_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_indexChangedInUse)
				return;

			_indexChangedInUse = true;
			SetCursWait();
			SetLocalSource();

			cbGlobal.SelectedIndex = cbGlobal.FindString(cbLocal.Text);
			SetGlobalSource();
			cbLang.SelectedIndex = cbLang.FindString(cbLocal.Text);
			SetLangSource();

			_indexChangedInUse = false;
			SetCursDefault();
		}
		private void cbLang_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (_indexChangedInUse)
				return;

			_indexChangedInUse = true;
			SetCursWait();
			SetLangSource();

			cbGlobal.SelectedIndex = cbGlobal.FindString(cbLang.Text);
			SetGlobalSource();
			cbLocal.SelectedIndex = cbLocal.FindString(cbLang.Text);
			SetLocalSource();

			_indexChangedInUse = false;
			SetCursDefault();
		}

		private void tv_units_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
		{
			//frmInfo f = new frmInfo("test", ds);
			//f.ShowDialog();
			//f.Dispose();
		}
		private void mnuExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		#endregion

		private void tv_units_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.IsSelected && e.Node != _node) {
				_node = e.Node;
				SetUnit(_node.Tag as MU);			// вызывается только здесь
				GetCredentials(_node.Tag as MU);	// вызывается только здесь			
				SetNodesColor();									// вызывается еще по смене таба
			}
		}
		private void SetUnit(MU u)
		{
			MU prevunit = _unit;
			_unit = u;

			// запрашиваем данные для юнита
			LoadData(false);

			// обновляем все закладки под выбранный юнит 
			if (!IsEqual(prevunit, _unit, TbNames.Stops))
				ReloadKpiTree();
			SetGlobalSource();	// также вызывается по событию комбобокса
			SetLocalSource();		// также вызывается по событию комбобокса и по кнопке рефреш
			SetLangSource();		// также вызывается по событию комбобокса
		}
		private void LoadData(bool reload)
		{
			Stopwatch sw1 = Stopwatch.StartNew();
			SetCursWait();

			DataSet lists = new DataSet(_unit.ID.ToString() + "_" + _unit.LineName);
			DataSet dts = null;
			string key, conn;
			int par_id;
			string u_id = _unit.ID.ToString();
			string tb0_key, tb1_key, tb2_key, tb3_key;

			#region | kpi & marks      |
			// загружаем глобальные KPI
			key = _unit.ParentKpi;
			par_id = _unit.ParentIdKpi;
			conn = _config.Units[par_id].Connection;
			tb0_key = _utMap[u_id + TbNames.Kpi.ToString()];
			tb1_key = _utMap[u_id + TbNames.Marks.ToString()];
			tb2_key = _utMap[u_id + TbNames.MarkBands.ToString()];
			tb3_key = _utMap[u_id + TbNames.Users.ToString()];

			if ((reload || !_tbMap.ContainsKey(tb0_key) && conn != "")) {
				if (_tbMap.ContainsKey(tb0_key))
					_tbMap.Remove(tb0_key);
				if (_tbMap.ContainsKey(tb1_key))
					_tbMap.Remove(tb1_key);
				if (_tbMap.ContainsKey(tb2_key))
					_tbMap.Remove(tb2_key);
				if (_tbMap.ContainsKey(tb3_key))
					_tbMap.Remove(tb3_key);
				if (_nmMap.ContainsKey(tb0_key))
					_nmMap.Remove(tb0_key);

				try {
					Debug.WriteLine("LoadLists_01: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
					using (SqlDataAdapter da = new SqlDataAdapter(
										" SELECT * FROM g_kpi " +
										" SELECT * FROM g_marks " +
										" SELECT * FROM g_markband " +
										" SELECT * FROM g_users " +
										" SELECT * FROM g_names ", conn)) {
						da.SelectCommand.CommandTimeout = 100;
						dts = new DataSet();
						//throw new Exception("Test exception");
						da.Fill(dts);
						Debug.WriteLine("LoadLists_02: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
						dts.Tables[0].TableName = TbNames.Kpi.ToString();
						dts.Tables[1].TableName = TbNames.Marks.ToString();
						dts.Tables[2].TableName = TbNames.MarkBands.ToString();
						dts.Tables[3].TableName = TbNames.Users.ToString();
						dts.Tables[4].TableName = "names";
						dts.Tables[0].ExtendedProperties.Add("id", par_id);
						dts.Tables[1].ExtendedProperties.Add("id", par_id);
						dts.Tables[2].ExtendedProperties.Add("id", par_id);
						dts.Tables[3].ExtendedProperties.Add("id", par_id);
						dts.Tables[4].ExtendedProperties.Add("id", par_id);
						_tbMap.Add(tb0_key, dts.Tables[0]);
						_tbMap.Add(tb1_key, dts.Tables[1]);
						_tbMap.Add(tb2_key, dts.Tables[2]);
						_tbMap.Add(tb3_key, dts.Tables[3]);
						dts.Tables[0].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						dts.Tables[1].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						dts.Tables[2].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						dts.Tables[3].Columns["Name"].MaxLength = 50;

					}
					if (!dts.Tables[0].Columns.Contains("display_name")) {
						dts.Tables[0].Columns.Add("display_name", typeof(string));
						int measure = 0;
						foreach (DataRow dr in dts.Tables[0].Rows) {
							dr["display_name"] = dr["EName"] + ", " + dr["Measure"];
							measure = C.GetInt(dr["Measure"]);
							switch (measure) {
								case 1:	// время
									dr["display_name"] = dr["EName"] + ", " + Properties.Resources.strMins;
									break;
								case 2:	// проценты
									dr["display_name"] = dr["EName"] + ", %";
									break;
								case 3:	// штуки
									dr["display_name"] = dr["EName"] + ", " + Properties.Resources.strUnits;
									break;
								case 4:	// объем
									dr["display_name"] = dr["EName"] + ", " + Properties.Resources.strVolume;
									break;
								default:
									dr["display_name"] = dr["EName"];
									break;
							}
						}
						dts.Tables[0].AcceptChanges();
					}
					if ((_kpi == null || reload) && dts.Tables[0].Rows.Count > 0) {
						_tbKPI = dts.Tables[0];
						//if (frmMonitor.defaultlanguage) {
						//   if (tbKPI.Columns.Contains("NameID") && tbKPI.Columns.Contains("Value") && !tbKPI.Columns.Contains("Name"))
						_tbKPI.Columns.Add("Name", typeof(string), "EName");
						//} else {
						//   if (tbKPI.Columns.Contains("NameID") && tbKPI.Columns.Contains("Value") && !tbKPI.Columns.Contains("Name"))
						//      tbKPI.Columns.Add("Name", typeof(string), "IIF(Value is NULL, EName, Value)");
						//}

						_kpi = new KPI(dts.Tables[0]);
					}
					Debug.WriteLine("LoadLists_03: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

					#region | users table |
					dts.Tables[3].Columns.Add("vrole", typeof(int));
					foreach (DataRow dr in dts.Tables[3].Rows)
						dr["vrole"] = frmLogin.GetRole(dr["role"].ToString(), C.GetInt(dr["salt"]));
					dts.Tables[3].AcceptChanges();
					dts.Tables[3].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);


					Debug.WriteLine("LoadLists_04: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

					DataTable dtroles = new DataTable("roles");
					dtroles.Columns.Add("id", typeof(int));
					dtroles.Columns.Add("role", typeof(string));
					DataRow drr;
					UserPermit p;
					string perm;
					for (int rr = 0; rr < 32; rr++) {
						p = (UserPermit)rr;
						perm = p.ToString();
						if (perm.Contains(" "))
							continue;
						drr = dtroles.NewRow();
						drr["id"] = p;
						drr["role"] = p.ToString();
						dtroles.Rows.Add(drr);
					}
					dts.Tables.Add(dtroles);
					#endregion

					#region | names |
					DataTable dtb = new DataTable(TbNames.Kpi.ToString());
					dtb.Columns.Add("ID", typeof(int));
					dtb.Columns.Add("NameID", typeof(int));
					dtb.Columns.Add("EName", typeof(string));
					dtb.Columns.Add("Culture", typeof(int));
					dtb.Columns.Add("Value", typeof(string));
					dtb.Columns.Add("Session", typeof(int));
					dtb.Columns["Value"].MaxLength = 50;
					DataRow ndr, namedr;
					int nameid;
					foreach (DataRow dr in dts.Tables[0].Rows) {
						ndr = dtb.NewRow();
						ndr["ID"] = dr["KpiID"];
						ndr["EName"] = dr["EName"];
						if (dr["NameID"] != DBNull.Value) {
							nameid = (int)dr["NameID"];
							namedr = C.GetRow(dts.Tables[4], "ID=" + nameid.ToString());
							if (namedr != null) {
								ndr["NameID"] = dr["NameID"];
								ndr["Value"] = namedr["Value"].ToString();
								ndr["Session"] = namedr["Session"].ToString();
							}
						}
						dtb.Rows.Add(ndr);
					}
					dtb.AcceptChanges();
					dtb.RowChanged += new DataRowChangeEventHandler(dtbLang_RowChanged);
					dtb.ExtendedProperties.Add("id", par_id);
					_nmMap.Add(tb0_key, dtb);
					#endregion

					// заполняем кипиаи - только раз
					if (_kpi == null && dts.Tables["KPI"].Rows.Count > 0) {
						_tbKPI = lists.Tables["KPI"];
						_kpi = new KPI(lists.Tables["KPI"]);
					}

				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from tables g_kpi & g_users", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			Debug.WriteLine("LoadLists_0: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			#endregion

			#region | config           |
			// загружаем глобальную таблицу конфигурации
			dts = null;
			key = _unit.ParentConfig;
			par_id = _unit.ParentIdConfig;
			conn = _config.Units[par_id].Connection;
			tb0_key = _utMap[u_id + TbNames.Config.ToString()];

			if ((reload || !_tbMap.ContainsKey(tb0_key) && conn != "")) {
				if (_tbMap.ContainsKey(tb0_key))
					_tbMap.Remove(tb0_key);
				try {
					using (SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM g_cfg", conn)) {
						da.SelectCommand.CommandTimeout = 100;
						dts = new DataSet();
						da.Fill(dts);
						dts.Tables["Table"].TableName = TbNames.Config.ToString();
						dts.Tables[0].ExtendedProperties.Add("id", par_id);
						dts.Tables[0].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						_tbMap.Add(tb0_key, dts.Tables[0]);
						dts.Tables[0].Columns["Name"].MaxLength = 50;
						dts.Tables[0].Columns["Value"].MaxLength = 50;
					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from table g_cfg", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			Debug.WriteLine("LoadLists_1: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			#endregion

			#region | stops            |
			// загружаем глобальную таблицу простоев
			dts = null;
			key = _unit.ParentStops;
			par_id = _unit.ParentIdStops;
			conn = _config.Units[par_id].Connection;
			tb0_key = _utMap[u_id + TbNames.Stops.ToString()];

			if ((reload || !_tbMap.ContainsKey(tb0_key)&& conn != "")) {
				if (_tbMap.ContainsKey(tb0_key))
					_tbMap.Remove(tb0_key);
				if (_nmMap.ContainsKey(tb0_key))
					_nmMap.Remove(tb0_key);
				try {
					using (SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM g_stops  SELECT * FROM g_names", conn)) {
						da.SelectCommand.CommandTimeout = 100;
						dts = new DataSet();
						da.Fill(dts);
						dts.Tables[0].TableName = TbNames.Stops.ToString();
						dts.Tables[1].TableName = "names";

						dts.Tables[0].Columns.Add("FullName", typeof(string));
						_unit.DownLevels = 0;
						FillFullNameColumn(_unit, 0, 0, "", dts.Tables[0], "TopID is NULL");
						FillParents(_unit, dts.Tables[0]);
						dts.Tables[0].AcceptChanges();

						dts.Tables[0].ExtendedProperties.Add("id", par_id);
						dts.Tables[0].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						dts.Tables[0].Columns["EName"].MaxLength = 100;
						_tbMap.Add(tb0_key, dts.Tables[0]);

						#region | names |
						DataTable dtb = new DataTable(TbNames.Stops.ToString());
						dtb.Columns.Add("ID", typeof(int));
						dtb.Columns.Add("NameID", typeof(int));
						dtb.Columns.Add("EName", typeof(string));
						dtb.Columns.Add("Culture", typeof(int));
						dtb.Columns.Add("Value", typeof(string));
						dtb.Columns.Add("Session", typeof(int));
						dtb.Columns["Value"].MaxLength = 50;
						DataRow ndr, namedr;
						int nameid;
						foreach (DataRow dr in dts.Tables[0].Rows) {
							ndr = dtb.NewRow();
							ndr["ID"] = dr["DownID"];
							ndr["EName"] = dr["EName"];
							if (dr["NameID"] != DBNull.Value) {
								nameid = (int)dr["NameID"];
								namedr = C.GetRow(dts.Tables[1], "ID=" + nameid.ToString());
								if (namedr != null) {
									ndr["NameID"] = dr["NameID"];
									ndr["Value"] = namedr["Value"].ToString();
									ndr["Session"] = namedr["Session"].ToString();
								}
							}
							dtb.Rows.Add(ndr);
						}
						dtb.AcceptChanges();
						dtb.RowChanged += new DataRowChangeEventHandler(dtbLang_RowChanged);
						dtb.ExtendedProperties.Add("id", par_id);
						_nmMap.Add(tb0_key, dtb);
						#endregion

					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from g_stops table", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			Debug.WriteLine("LoadLists_2: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			#endregion

			#region | brands & formats |
			// загружаем глобальные таблицы брендов-форматов
			dts = null;
			key = _unit.ParentSku;
			par_id = _unit.ParentIdSku;
			conn = _config.Units[par_id].Connection;
			tb0_key = _utMap[u_id + TbNames.Brands.ToString()];
			tb1_key = _utMap[u_id + TbNames.Formats.ToString()];
			if ((reload || !_tbMap.ContainsKey(tb0_key)) && conn != "") {
				if (_tbMap.ContainsKey(tb0_key))
					_tbMap.Remove(tb0_key);
				if (_tbMap.ContainsKey(tb1_key))
					_tbMap.Remove(tb1_key);
				if (_nmMap.ContainsKey(tb0_key))
					_nmMap.Remove(tb0_key);
				if (_nmMap.ContainsKey(tb1_key))
					_nmMap.Remove(tb1_key);
				try {
					using (SqlDataAdapter da = new SqlDataAdapter(
								" SELECT * FROM g_brands " +
								" SELECT * FROM g_formats " +
								" SELECT * FROM g_names ", conn)) {
						da.SelectCommand.CommandTimeout = 100;
						dts = new DataSet();
						da.Fill(dts);
						dts.Tables[0].TableName = TbNames.Brands.ToString();
						dts.Tables[1].TableName = TbNames.Formats.ToString();
						dts.Tables[2].TableName = "names";
						dts.Tables[0].ExtendedProperties.Add("id", par_id);
						dts.Tables[1].ExtendedProperties.Add("id", par_id);
						dts.Tables[0].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						dts.Tables[1].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						_tbMap.Add(tb0_key, dts.Tables[0]);
						_tbMap.Add(tb1_key, dts.Tables[1]);
						dts.Tables[0].Columns["EName"].MaxLength = 50;
						dts.Tables[1].Columns["EName"].MaxLength = 50;

						#region | brand names  |

						DataTable dtb = new DataTable(TbNames.Brands.ToString());
						dtb.Columns.Add("ID", typeof(int));
						dtb.Columns.Add("NameID", typeof(int));
						dtb.Columns.Add("EName", typeof(string));
						dtb.Columns.Add("Culture", typeof(int));
						dtb.Columns.Add("Value", typeof(string));
						dtb.Columns.Add("Session", typeof(int));
						dtb.Columns["Value"].MaxLength = 50;
						DataRow ndr, namedr;
						int nameid;
						foreach (DataRow dr in dts.Tables[0].Rows) {
							ndr = dtb.NewRow();
							ndr["ID"] = dr["ID"];
							ndr["EName"] = dr["EName"];
							if (dr["NameID"] != DBNull.Value) {
								nameid = (int)dr["NameID"];
								namedr = C.GetRow(dts.Tables[2], "ID=" + nameid.ToString());
								if (namedr != null) {
									ndr["NameID"] = dr["NameID"];
									ndr["Value"] = namedr["Value"].ToString();
									ndr["Session"] = namedr["Session"].ToString();
								}
							}
							dtb.Rows.Add(ndr);
						}
						dtb.AcceptChanges();
						dtb.RowChanged += new DataRowChangeEventHandler(dtbLang_RowChanged);
						dtb.ExtendedProperties.Add("id", par_id);
						_nmMap.Add(tb0_key, dtb);
						#endregion

						#region | format names |

						dtb = new DataTable(TbNames.Formats.ToString());
						dtb.Columns.Add("ID", typeof(int));
						dtb.Columns.Add("NameID", typeof(int));
						dtb.Columns.Add("EName", typeof(string));
						dtb.Columns.Add("Culture", typeof(int));
						dtb.Columns.Add("Value", typeof(string));
						dtb.Columns.Add("Session", typeof(int));
						dtb.Columns["Value"].MaxLength = 50;
						foreach (DataRow dr in dts.Tables[1].Rows) {
							ndr = dtb.NewRow();
							ndr["ID"] = dr["ID"];
							ndr["EName"] = dr["EName"];
							if (dr["NameID"] != DBNull.Value) {
								nameid = (int)dr["NameID"];
								namedr = C.GetRow(dts.Tables[2], "ID=" + nameid.ToString());
								if (namedr != null) {
									ndr["NameID"] = dr["NameID"];
									ndr["Value"] = namedr["Value"].ToString();
									ndr["Session"] = namedr["Session"].ToString();
								}
							}
							dtb.Rows.Add(ndr);
						}
						dtb.AcceptChanges();
						dtb.RowChanged += new DataRowChangeEventHandler(dtbLang_RowChanged);
						dtb.ExtendedProperties.Add("id", par_id);
						_nmMap.Add(tb1_key, dtb);
						#endregion

					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from tables g_brands & g_formats", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			Debug.WriteLine("LoadLists_3: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			#endregion

			#region | shifts           |
			// загружаем глобальную таблицу смен
			dts = null;
			key = _unit.ParentShifts;
			par_id = _unit.ParentIdShifts;
			conn = _config.Units[par_id].Connection;
			tb0_key = _utMap[u_id + TbNames.Shifts.ToString()];
			if ((reload || !_tbMap.ContainsKey(tb0_key)) && conn != "") {
				if (_tbMap.ContainsKey(tb0_key))
					_tbMap.Remove(tb0_key);
				if (_nmMap.ContainsKey(tb0_key))
					_nmMap.Remove(tb0_key);

				try {
					using (SqlDataAdapter da = new SqlDataAdapter(
								"SELECT * FROM g_shifts " +
								" SELECT * FROM g_names ", conn)) {
						da.SelectCommand.CommandTimeout = 100;
						dts = new DataSet();
						da.Fill(dts);
						dts.Tables[0].TableName = TbNames.Shifts.ToString();
						dts.Tables[1].TableName = "names";
						dts.Tables[0].ExtendedProperties.Add("id", par_id);
						dts.Tables[0].RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
						_tbMap.Add(tb0_key, dts.Tables[0]);
						dts.Tables[0].Columns["EName"].MaxLength = 50;

						#region | names |
						DataTable dtb = new DataTable(TbNames.Shifts.ToString());
						dtb.Columns.Add("ID", typeof(int));
						dtb.Columns.Add("NameID", typeof(int));
						dtb.Columns.Add("EName", typeof(string));
						dtb.Columns.Add("Culture", typeof(int));
						dtb.Columns.Add("Value", typeof(string));
						dtb.Columns.Add("Session", typeof(int));
						dtb.Columns["Value"].MaxLength = 50;
						DataRow ndr, namedr;
						int nameid;
						foreach (DataRow dr in dts.Tables[0].Rows) {
							ndr = dtb.NewRow();
							ndr["ID"] = dr["ID"];
							ndr["EName"] = dr["EName"];
							if (dr["NameID"] != DBNull.Value) {
								nameid = (int)dr["NameID"];
								namedr = C.GetRow(dts.Tables[1], "ID=" + nameid.ToString());
								if (namedr != null) {
									ndr["NameID"] = dr["NameID"];
									ndr["Value"] = namedr["Value"].ToString();
									ndr["Session"] = namedr["Session"].ToString();
								}
							}
							dtb.Rows.Add(ndr);
						}
						dtb.AcceptChanges();
						dtb.RowChanged += new DataRowChangeEventHandler(dtbLang_RowChanged);
						dtb.ExtendedProperties.Add("id", par_id);
						_nmMap.Add(tb0_key, dtb);
						#endregion

					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from g_shifts table", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			Debug.WriteLine("LoadLists_4: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			#endregion

			LoadLocalTables(reload);
			Debug.WriteLine("LoadLists_5: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));

			//throw new Exception("Test exception from LoadLists()");

			sw1.Stop();
			Debug.WriteLine("LoadLists: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
			SetCursDefault();
		}
		private void LoadLocalTables(bool reload)
		{

			// загружаем локальные списки и связи с глобальными таблицами
			DataSet lists = new DataSet(_unit.ID.ToString() + "_" + _unit.LineName);

			DataSet dts = null;
			string key = _unit.ID.ToString();
			int par_id = _unit.ID;
			string conn = _unit.Connection;
			if ((reload || !_locals.ContainsKey(_unit.ID)) && conn != "") {
				if (_locals.ContainsKey(_unit.ID))
					_locals.Remove(_unit.ID);
				if (_localsCash.ContainsKey(_unit.ID))
					_localsCash.Remove(_unit.ID);
				try {
					using (SqlConnection csqlonn = new SqlConnection(conn)) {
						csqlonn.Open();
						_unit.DatabaseName = csqlonn.Database;

						SqlCommand cmd = new SqlCommand("v_lists", csqlonn);
						cmd.CommandTimeout = 100;
						cmd.CommandType = CommandType.StoredProcedure;
						dts = new DataSet();
						using (SqlDataReader dr = cmd.ExecuteReader()) {
							//throw new Exception("Test Exception ");
							lists.Load(dr, LoadOption.OverwriteChanges, new string[] { "Targets", "rstops", "rbrands", "rformats", "rshifts", "rusers", TbNames.Config.ToString() });
						}
						lists.Tables[TbNames.Config.ToString()].AcceptChanges();
						lists.Tables[TbNames.Config.ToString()].RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Exception while retrieving data from 'local' tables", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}

				// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ //
				DataView dv = null;
				DataTable dtf = null;
				DataTable dtl = null;

				#region | targets  |
				string u_id = _unit.ID.ToString();
				string tb0_key = _utMap[u_id + TbNames.Kpi.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("Targets")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						//dtl = lists.Tables[TbNames.Kpi.ToString()];
						//lists.Relations.Add("row5", dtl.Columns["KpiID"], lists.Tables["Targets"].Columns["KpiID"]);
						lists.Tables["Targets"].RowChanged += new DataRowChangeEventHandler(dtbTargets_RowChanged);
						lists.Tables["Targets"].RowChanging += new DataRowChangeEventHandler(frmMaster_RowChanging);
						lists.Tables["Targets"].Columns["session"].AllowDBNull = true;
						UniqueConstraint c = new UniqueConstraint("un", new DataColumn[] { lists.Tables["Targets"].Columns["Year"], lists.Tables["Targets"].Columns["KpiID"] });
						lists.Tables["Targets"].Constraints.Add(c);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Table name: Targets", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				#region | stops    |
				tb0_key = _utMap[u_id + TbNames.Stops.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("rstops")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						dtl = lists.Tables[TbNames.Stops.ToString()];
						dtl.TableName = "s_stops";
						lists.Relations.Add("row0", dtl.Columns["DownID"], lists.Tables["rstops"].Columns["id"]);
						dtl.Columns.Add("chld_valid", typeof(bool), "max(Child(row0).valid)");
						dtl.Columns.Add("chld", typeof(bool), "count(Child(row0).id)");
						dv = new DataView(dtl);
						dtf = dv.ToTable();
						dtf.Columns["chld_valid"].ReadOnly = false;
						dtf.Columns["chld"].ReadOnly = false;
						dtf.TableName = TbNames.Stops.ToString();
						dtf.AcceptChanges();
						dtf.RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
						lists.Tables.Add(dtf);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception while local table binding: " + TbNames.Stops.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				#region | formats  |
				tb0_key = _utMap[u_id + TbNames.Formats.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("rformats")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						dtl = lists.Tables[TbNames.Formats.ToString()];
						dtl.TableName = "s_formats";
						lists.Relations.Add("row1", dtl.Columns["ID"], lists.Tables["rformats"].Columns["id"]);
						dtl.Columns.Add("Rate", typeof(float), "MAX(Child.rate)");
						dtl.Columns.Add("chld_valid", typeof(bool), "max(Child(row1).valid)");
						dtl.Columns.Add("chld", typeof(bool), "count(Child(row1).id)");
						dv = new DataView(dtl);
						dtf = dv.ToTable();
						dtf.Columns["Rate"].ReadOnly = false;
						dtf.Columns["chld_valid"].ReadOnly = false;
						dtf.Columns["chld"].ReadOnly = false;
						dtf.TableName = TbNames.Formats.ToString();
						dtf.AcceptChanges();
						dtf.RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
						lists.Tables.Add(dtf);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception while local table binding: " + TbNames.Formats.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				#region | brands   |
				tb0_key = _utMap[u_id + TbNames.Brands.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("rbrands")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						dtl = lists.Tables[TbNames.Brands.ToString()];
						dtl.TableName = "s_brands";
						lists.Relations.Add("row2", dtl.Columns["ID"], lists.Tables["rbrands"].Columns["id"]);
						dtl.Columns.Add("chld_valid", typeof(bool), "max(Child(row2).valid)");
						dtl.Columns.Add("chld", typeof(bool), "count(Child(row2).id)");
						dv = new DataView(dtl);
						dtf = dv.ToTable();
						dtf.Columns["chld_valid"].ReadOnly = false;
						dtf.Columns["chld"].ReadOnly = false;
						dtf.TableName = TbNames.Brands.ToString();
						dtf.AcceptChanges();
						dtf.RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
						lists.Tables.Add(dtf);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception while local table binding: " + TbNames.Brands.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				#region | shifts   |
				tb0_key = _utMap[u_id + TbNames.Shifts.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("rshifts")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						dtl = lists.Tables[TbNames.Shifts.ToString()];
						dtl.TableName = "s_shifts";
						lists.Relations.Add("row3", dtl.Columns["ID"], lists.Tables["rshifts"].Columns["id"]);
						dtl.Columns.Add("chld_valid", typeof(bool), "max(Child(row3).valid)");
						dtl.Columns.Add("chld", typeof(bool), "count(Child(row3).id)");
						dv = new DataView(dtl);
						dtf = dv.ToTable();
						dtf.Columns["chld_valid"].ReadOnly = false;
						dtf.Columns["chld"].ReadOnly = false;
						dtf.TableName = TbNames.Shifts.ToString();
						dtf.AcceptChanges();
						dtf.RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
						lists.Tables.Add(dtf);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception while local table binding: " + TbNames.Shifts.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				#region | users    |
				tb0_key = _utMap[u_id + TbNames.Users.ToString()];
				if (_tbMap.ContainsKey(tb0_key) && lists.Tables.Contains("rusers")) {
					try {
						lists.Tables.Add(_tbMap[tb0_key].Copy());
						dtl = lists.Tables[TbNames.Users.ToString()];
						dtl.TableName = "s_users";
						lists.Relations.Add("row4", dtl.Columns["ID"], lists.Tables["rusers"].Columns["id"]);
						dtl.Columns.Add("chld_valid", typeof(bool), "max(Child(row4).valid)");
						dtl.Columns.Add("chld", typeof(bool), "count(Child(row4).id)");
						dv = new DataView(dtl);
						dtf = dv.ToTable();
						dtf.Columns["chld_valid"].ReadOnly = false;
						dtf.Columns["chld"].ReadOnly = false;
						dtf.TableName = TbNames.Users.ToString();
						dtf.AcceptChanges();
						dtf.RowChanged += new DataRowChangeEventHandler(dtbLocal_RowChanged);
						lists.Tables.Add(dtf);
					} catch (Exception ex) {
						MessageBox.Show(ex.Message, "Exception while local table binding: " + TbNames.Users.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				#endregion

				//lists.AcceptChanges();
				_locals.Add(_unit.ID, lists);
				_localsCash.Add(_unit.ID, lists.Copy());
			}
		}

		private void SetGlobalSource()
		{
			if (_unit == null)
				return;
			DataRowView dr = cbGlobal.SelectedItem as DataRowView;
			if (dr == null)
				return;

			string strtb = dr["id"].ToString();
			if (_tbMap.ContainsKey(_utMap[_unit.ID.ToString() + strtb])) {
				//dgvGlobal.DataSource = TbMap[UtMap[unit.ID.ToString() + dr["id"].ToString()]];
				_dtbGlobal = _tbMap[_utMap[_unit.ID.ToString() + dr["id"].ToString()]];
				if (bnGlobal.BindingSource != null && _dtbGlobal == bnGlobal.BindingSource.DataSource)
					return;	// таблицы совпали, поэтому можно не обновлять
				bnGlobal.BindingSource = new BindingSource(_dtbGlobal, "");
				dgvGlobal.DataSource = bnGlobal.BindingSource;
			} else {
				_dtbGlobal = null;
				dgvGlobal.DataSource = null;
			}
		}
		private void SetLocalSource()
		{
			if (_unit == null)
				return;
			DataRowView dr = cbLocal.SelectedItem as DataRowView;
			if (dr == null)
				return;
			// локальные таблицы и цели находятся в одном датасете > управлять ими можно в одном месте
			string strtb = dr["id"].ToString();
			if (_locals.ContainsKey(_unit.ID) && _locals[_unit.ID].Tables.Contains(strtb)) {

				_dtbLocal = _locals[_unit.ID].Tables[strtb];

				// скрываем настройку 'Valid Date' от посторонних глаз
				if (_dtbLocal.TableName == TbNames.Config.ToString() && _user.Name.ToLower() != "admin" && _user.UserPermit != UserPermit.Admin)
					_dtbLocal.DefaultView.RowFilter = "Id<>9";

				bnLocal.BindingSource = new BindingSource(_dtbLocal.DefaultView, "");
				//((System.ComponentModel.ISupportInitialize)(this.dgvLocal)).BeginInit();
				dgvLocal.DataSource = bnLocal.BindingSource;
				//((System.ComponentModel.ISupportInitialize)(this.dgvLocal)).EndInit();

				_dtbTargets = _locals[_unit.ID].Tables["Targets"];
				bnTargets.BindingSource = new BindingSource(_dtbTargets, "");
				bnTargets.BindingSource.Filter = "";
				dgvTargets.Columns.Clear();
				dgvTargets.DataSource = bnTargets.BindingSource;
				dgvTargets.Columns["TargetId"].Visible = false;
				dgvTargets.Columns["Session"].Visible = false;
				dgvTargets.Columns["FromDate"].Visible = false;
				//dgvTargets.Columns["FromDate"].DefaultCellStyle.Format = "y";

				dgvTargets.Columns["Year"].HeaderCell = new DataGridViewAutoFilterColumnHeaderCell(dgvTargets.Columns["Year"].HeaderCell);
				// && !dgvTargets.Columns.Contains("KpiID")
				if (_dtbTargets.DataSet.Tables.Contains("Kpi")) {
					dgvTargets.Columns.Remove("KpiID");

					DataGridViewComboBoxColumn dgvcb = new DataGridViewComboBoxColumn();
					dgvcb.DisplayStyleForCurrentCellOnly = true;
					dgvcb.DataPropertyName = "KpiID";
					dgvcb.DataSource = _dtbTargets.DataSet.Tables["Kpi"];
					dgvcb.ValueMember = "KpiID";
					dgvcb.DisplayMember = "display_name";
					dgvcb.HeaderText = "KPI";
					dgvcb.Name = "KpiID";
					dgvcb.ReadOnly = false;
					dgvcb.Width = 250;
					dgvcb.MaxDropDownItems = 16;
					dgvTargets.Columns.Insert(0, dgvcb);

					dgvcb.HeaderCell = new
						 DataGridViewAutoFilterColumnHeaderCell(dgvcb.HeaderCell);
				}

				////// Add the AutoFilter header cell to each column.
				////foreach (DataGridViewColumn col in dgvTargets.Columns) {
				////   col.HeaderCell = new
				////       DataGridViewAutoFilterColumnHeaderCell(col.HeaderCell);
				////}

			} else {
				_dtbLocal = null;
				dgvLocal.DataSource = null;
				_dtbTargets = null;
				dgvTargets.DataSource = null;
			}
		}
		private void SetLangSource()
		{
			if (_unit == null)
				return;
			DataRowView dr = cbLang.SelectedItem as DataRowView;
			if (dr == null)
				return;

			string strtb = dr["id"].ToString();
			if (_nmMap.ContainsKey(_utMap[_unit.ID.ToString() + strtb])) {
				//dgvGlobal.DataSource = TbMap[UtMap[unit.ID.ToString() + dr["id"].ToString()]];
				_dtbLang = _nmMap[_utMap[_unit.ID.ToString() + dr["id"].ToString()]];
				if (bnLang.BindingSource != null && _dtbLang == bnLang.BindingSource.DataSource)
					return;	// таблицы совпали, поэтому можно не обновлять
				bnLang.BindingSource = new BindingSource(_dtbLang, "");
				dgvLang.DataSource = bnLang.BindingSource;
			} else {
				_dtbLang = null;
				dgvLang.DataSource = null;
			}
		}

		private void dtbGlobal_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			DataTable dtb = sender as DataTable;
			bool res = dtb != null && dtb.GetChanges() != null;

			if (res && dtb.ExtendedProperties.Contains("id")) {
				int db_id = (int)dtb.ExtendedProperties["id"];
				MU mu = _config.MonitorUnits[db_id];

				if (e != null && !ToUpdateCashDate.Contains(mu.ID))
					ToUpdateCashDate.Add(mu.ID);
			}

			btGlobalSave.Enabled = res;
			btGlobalUndo.Enabled = res;
		}
		private void dtbLocal_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			DataTable dtb = sender as DataTable;
			bool res = dtb == null || dtb.GetChanges() == null;

			if (e != null && !ToUpdateCashDate.Contains(_unit.ID))
				ToUpdateCashDate.Add(_unit.ID);

			btLocalSave.Enabled = !res;
			btLocalUndo.Enabled = !res;
		}
		private void dtbLang_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			DataTable dtb = sender as DataTable;
			bool res = dtb != null && dtb.GetChanges() != null;

			if (res && dtb.ExtendedProperties.Contains("id")) {
				int db_id = (int)dtb.ExtendedProperties["id"];
				MU mu = _config.MonitorUnits[db_id];

				if (e != null && !ToUpdateCashDate.Contains(mu.ID))
					ToUpdateCashDate.Add(mu.ID);
			}

			btLangSave.Enabled = res;
			btLangUndo.Enabled = res;
		}
		private void frmMaster_RowChanging(object sender, DataRowChangeEventArgs e)
		{
			DataRow dr = e.Row as DataRow;
			//e.Action
		}
		private void dtbTargets_RowChanged(object sender, DataRowChangeEventArgs e)
		{
			DataTable dtb = sender as DataTable;
			//e.Row.EndEdit();
			bool res = dtb == null || dtb.GetChanges() == null;

			if (e != null && !ToUpdateCashDate.Contains(_unit.ID))
				ToUpdateCashDate.Add(_unit.ID);

			btTargetsSave.Enabled = !res;
			btTargetsUndo.Enabled = !res;
		}

		#region | tree drag&drop       |

		// Temporary drop node for selection
		private TreeNode tempDropNode = null;
		// Node being dragged
		private TreeNode dragNode = null;

		private void treeKPI_DragEnter(object sender, DragEventArgs e)
		{
			// Set the visual effect
			e.Effect = DragDropEffects.Move;
		}
		private void treeKPI_ItemDrag(object sender, ItemDragEventArgs e)
		{
			if (!_ok)
				return;

			// Get drag node and select it
			TreeNode tn = (TreeNode)e.Item;

			// Root node can not be dragged
			if (tn.Parent == null)
				return;

			this.dragNode = tn;
			this.tv_kpi.SelectedNode = this.dragNode;

			// Initiate drag/drop
			DoDragDrop(e.Item, DragDropEffects.Move);
		}
		private void treeKPI_DragDrop(object sender, DragEventArgs e)
		{
			// Get drop node
			TreeNode dropNode = this.tv_kpi.GetNodeAt(this.tv_kpi.PointToClient(new Point(e.X, e.Y)));

			// If drop node isn't equal to drag node, add drag node as child of drop node
			if (this.dragNode != dropNode) {
				// Remove drag node from parent
				if (this.dragNode.Parent == null)
					this.tv_kpi.Nodes.Remove(this.dragNode);
				else
					this.dragNode.Parent.Nodes.Remove(this.dragNode);

				// Add drag node to drop node
				dropNode.Nodes.Add(this.dragNode);
				//dropNode.ExpandAll();

				SetTopID(dropNode, dragNode);

				// Set drag node to null
				this.dragNode = null;

				tv_kpi.Refresh();
			}
		}
		private void SetTopID(TreeNode tn1, TreeNode tn2)
		{
			DataRow dr1 = tn1.Tag as DataRow;
			DataRow dr2 = tn2.Tag as DataRow;
			dr2["TopID"] = dr1["DownID"];
			dr2["KpiID"] = dr1["KpiID"];
			tn2.ImageKey = tn1.ImageKey;
			tn2.SelectedImageKey = tn1.SelectedImageKey;
			foreach (TreeNode tn3 in tn2.Nodes)
				SetTopID(tn2, tn3);

		}
		private void treeKPI_GiveFeedback(object sender, GiveFeedbackEventArgs e)
		{
			if (e.Effect == DragDropEffects.Move) {
				// Show pointer cursor while dragging

				e.UseDefaultCursors = false;
				this.tv_kpi.Cursor = Cursors.Default;
			} else e.UseDefaultCursors = true;
		}
		private void treeKPI_DragOver(object sender, DragEventArgs e)
		{
			// Get actual drop node
			TreeNode dropNode = this.tv_kpi.GetNodeAt(this.tv_kpi.PointToClient(new Point(e.X, e.Y)));
			if (dropNode == null) {
				e.Effect = DragDropEffects.None;
				return;
			}
			if (dropNode.Parent == null)

				e.Effect = DragDropEffects.Move;

			// if mouse is on a new node select it
			if (this.tempDropNode != dropNode) {
				this.tv_kpi.SelectedNode = dropNode;
				tempDropNode = dropNode;
			}

			// Avoid that drop node is child of drag node 
			TreeNode tmpNode = dropNode;
			while (tmpNode.Parent != null) {
				if (tmpNode.Parent == this.dragNode) e.Effect = DragDropEffects.None;
				tmpNode = tmpNode.Parent;
			}
		}

		#endregion

		#region | widgets              |

		private void SetCursWait()
		{
			//Application.UseWaitCursor = true;
			this.Cursor = Cursors.WaitCursor;
			Application.DoEvents();
		}
		private void SetCursDefault()
		{
			//Application.UseWaitCursor = false;
			this.Cursor = Cursors.Default;
			Application.DoEvents();
		}

		private bool IsEqual(MU u1, MU u2, string tbn)
		{
			if (u1 == null || u2 == null)
				return false;
			if (!_tbMap.ContainsKey(_utMap[u1.ID.ToString() + tbn]) || !_tbMap.ContainsKey(_utMap[u2.ID.ToString() + tbn]))
				return false;
			return _tbMap[_utMap[u1.ID.ToString() + tbn]] == _tbMap[_utMap[u2.ID.ToString() + tbn]];
		}
		private bool IsEqual(MU u1, MU u2, TbNames tbn)
		{
			return IsEqual(u1, u2, tbn.ToString());
		}
		private void FillFullNameColumn(MU unit, int level, int topdownid, string parentname, DataTable dtb, string flt)
		{
			string fname;
			DataRow[] DR = dtb.Select(flt);
			if (DR.Length == 0)
				return;
			string col0 = "";
			if (topdownid > 0) col0 = "D" + (level - 1).ToString();
			string col = "D" + level.ToString();
			if (!dtb.Columns.Contains(col)) {
				dtb.Columns.Add(col, typeof(int));
				unit.DownLevels++;
				unit.DownColumns.Add(level, col);
			}
			foreach (DataRow dr in DR) {
				fname = parentname != "" && col != "D1" ? string.Format("{0} \\ {1}", parentname, dr["EName"]) : dr["EName"].ToString();
				dr["FullName"] = fname;
				dr[col] = dr["DownID"];
				if (topdownid > 0) dr[col0] = topdownid;
				FillFullNameColumn(unit, level + 1, (int)dr["DownID"], fname, dtb, "TopID=" + dr["DownID"].ToString());
			}
		}
		private void FillParents(MU unit, DataTable dtb)
		{
			string col0, col1, col2;
			for (int c1 = unit.DownLevels - 1; c1 >= 2; c1--) {
				col0 = "D" + (c1 - 2).ToString();
				col1 = "D" + (c1 - 1).ToString();
				col2 = "D" + c1.ToString();
				foreach (DataRow dr in dtb.Rows)
					if (dr[col2] != DBNull.Value)
						dr[col0] = dtb.Select("DownID=" + dr[col1])[0]["TopID"];
			}
		}

		#endregion

		private void tv_kpi_DoubleClick(object sender, EventArgs e)
		{
			if (!_ok)
				return;
			DataRow dr1 = tv_kpi.SelectedNode.Tag as DataRow;
			//if (dr1.RowState == DataRowState.Added) {
			//   MessageBox.Show("Для продолжения необходимо сохранить предыдущие изменения.", "Need to save before", MessageBoxButtons.OK, MessageBoxIcon.Information);
			//   return;
			//}
			InputForm f = new InputForm("New entry name", "");
			if (f.ShowDialog() == DialogResult.OK) {
				if (f.ResS.Length > dr1.Table.Columns["EName"].MaxLength) {
					MessageBox.Show("Превышено допустимое количество символов" + Environment.NewLine +
					"Действие будет отменено", "Length exceeded", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}

				object m = dr1.Table.Compute("MAX(DownID)", "");
				_newId = m == null ? 1 : Convert.ToInt32(m.ToString());

				DataRow dr2 = dr1.Table.NewRow();
				//dr2["DownID"] = newid--;
				dr2["DownID"] = ++_newId;
				dr2["TopID"] = dr1["DownID"];
				dr2["KpiID"] = dr1["KpiID"];
				dr2["EName"] = f.ResS;
				dr2["Valid"] = true;
				//todo: заполнять поле 'FullName'
				dr1.Table.Rows.Add(dr2);
				TreeNode tn = tv_kpi.SelectedNode.Nodes.Add(f.ResS);
				tn.Tag = dr2;
				tn.ImageKey = tv_kpi.SelectedNode.ImageKey;
				tn.SelectedImageKey = tv_kpi.SelectedNode.SelectedImageKey;
			}
		}

		private void ReloadKpiTree()
		{
			if (_kpi != null) {
				tv_kpi.SuspendLayout();
				SetCursWait();
				tv_kpi.Nodes.Clear();

				tv_kpi.ImageList = _kpi.ImageList;
				//AddKpiTreeNodes(ListsCash[unit.ID].Tables[TbNames.GlobalStops.ToString()], "TopID is NULL", tv_kpi.Nodes);
				if (_tbMap.ContainsKey(_utMap[_unit.ID.ToString() + TbNames.Stops.ToString()]))
					AddKpiTreeNodes(_tbMap[_utMap[_unit.ID.ToString() + TbNames.Stops.ToString()]], "TopID is NULL", tv_kpi.Nodes);

				tv_kpi.ResumeLayout();
				SetCursDefault();
			}
		}
		private void AddKpiTreeNodes(DataTable tb, string Select, TreeNodeCollection nodes)
		{
			if (tb == null)
				return;
			TreeNode n;
			DataRow[] DR = tb.Select(Select);
			foreach (DataRow dr in DR) {
				n = new TreeNode(dr["EName"].ToString());
				n.Tag = dr;
				n.Name = dr["DownID"].ToString();
				n.ImageKey = dr["KpiID"].ToString();
				n.SelectedImageKey = dr["KpiID"].ToString();
				nodes.Add(n);
				AddKpiTreeNodes(tb, string.Format("{0}={1}", "TopID", dr["DownID"]), n.Nodes);
			}
		}
		private void SetNodesColor()
		{
			bool res = true;
			string strtb = "";

			// на закладке локальных таблиц все узлы всегда черные
			if (tab.SelectedTab == tpLocal || tab.SelectedTab == tpTargets)
				res = false;

			BindingSource bs = dgvGlobal.DataSource as BindingSource;
			if (res && bs == null)
				res = false;

			// если таблицу не нашли, значит красим в черный
			if (res) {
				DataTable dtb = bs.DataSource as DataTable;
				if (dtb == null)
					res = false;
				else
					strtb = dtb.TableName;
			} else if (tab.SelectedTab == tpDnTm) {
				res = true;
				strtb = TbNames.Kpi.ToString();
			}

			bool resloop = false;
			string map1 = "";//UtMap[unit.ID.ToString() + strtb];
			string map2 = "";
			foreach (TreeNode tn in _nodes) {
				MU u = tn.Tag as MU;
				if (res) {
					map1 = _utMap[_unit.ID.ToString() + strtb];
					map2 = _utMap[u.ID.ToString() + strtb];
					if (((_tbMap.ContainsKey(map1)) && (_tbMap.ContainsKey(map2)))) {
						resloop = (_tbMap[map1] == _tbMap[map2]);
					} else
						resloop = false;
				}
				if (tab.SelectedTab == tpLocal || tab.SelectedTab == tpTargets)
					tn.ForeColor = Color.Black;
				else {
					if (res && resloop) {
						tn.ForeColor = Color.Blue;
						//tn.NodeFont = fb;
					} else {
						tn.ForeColor = Color.Gray;
						//tn.NodeFont = f;
					}
				}
			}

			tv_units.Refresh();
			GetCurrentStatePermission();
		}

		private void btInclude_Click(object sender, EventArgs e)
		{
			if (_dtbLocal == null)
				return;

			if (!_dtbLocal.Columns.Contains("chld"))
				return;

			this.Validate();

			DataRowView drv;
			foreach (DataGridViewRow dvr in dgvLocal.SelectedRows) {
				drv = dvr.DataBoundItem as DataRowView;
				if (drv.Row.RowState != DataRowState.Unchanged) {
					// уже правленные записи не обрабатываем, пусть сначала сохранит изменения
					continue;
				} if ((bool)drv["chld"] && !(bool)drv["chld_valid"]) {
					// если уже есть в таблице r_xxx, но r_xxx.valid = false
					drv.Row.SetModified();
					drv["chld_valid"] = true;
				} else if ((bool)drv["chld"]) {
					// все и так есть ничего не делаем, идем дальше
					continue;
				} else {
					drv.Row.SetAdded();
					drv["chld"] = true;
					drv["chld_valid"] = true;
				}
				drv.EndEdit();
			}
			dgvLocal.Refresh();
		}
		private void btRemove_Click(object sender, EventArgs e)
		{
			if (_dtbLocal == null)
				return;

			if (!_dtbLocal.Columns.Contains("chld_valid"))
				return;

			this.Validate();

			DataRowView drv;
			foreach (DataGridViewRow dvr in dgvLocal.SelectedRows) {
				drv = dvr.DataBoundItem as DataRowView;
				if (drv.Row.RowState == DataRowState.Unchanged && drv["chld_valid"] != DBNull.Value && (bool)drv["chld_valid"]) {
					drv["chld_valid"] = false;
					//drv["chld"] = false;
				} else if (drv.Row.RowState == DataRowState.Added) {
					drv["chld_valid"] = false;
					drv["chld"] = false;
					drv.Row.AcceptChanges();
				} else if (drv.Row.RowState == DataRowState.Modified && drv["chld_valid"] != DBNull.Value && (bool)drv["chld_valid"] == true)
					drv.Row.RejectChanges();
				drv.EndEdit();
			}

			dgvLocal.Refresh();
		}

		#region | grids events          |

		private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
			int maxlenth = 0;
			if (drv != null) {
				string col = dgv.Columns[e.ColumnIndex].Name;
				if (drv.Row.Table.Columns.Contains(col))
					maxlenth = drv.Row.Table.Columns[col].MaxLength;
			}
			if (e.Exception is FormatException) {
				MessageBox.Show(e.Exception.Message + Environment.NewLine +
					"Press 'Esc' to cancel input.", e.Exception.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
			} else if (e.Exception is ArgumentException) {
				string lenmessage = "";
				if (maxlenth > 0) lenmessage = "Maximum length is equal " + maxlenth.ToString() + Environment.NewLine;
				MessageBox.Show(e.Exception.Message + Environment.NewLine + lenmessage,
					e.Exception.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
			} else {
				MessageBox.Show(e.Exception.Message, e.Exception.GetType().ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		private void dgv_Enter(object sender, EventArgs e)
		{
			// Иногда по непонятным причинам курсор остается в состоянии ожидания (часики)
			// поэтому на всякий случай сбрасываем в нормальное состояние
			DataGridView dgv = sender as DataGridView;
			dgv.Cursor = Cursors.Default;

			//Debug.WriteLine("_________dgv_Enter_________");

		}
		private void dgv_CellValueChanged(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			if (dgv.CurrentRow == null)
				return;

			DataRowView drv = (dgv.CurrentRow.DataBoundItem as DataRowView);
			if (drv == null)
				return;

			foreach (DataGridViewColumn col in dgv.Columns)
				if (col.Visible && drv[col.Name] == DBNull.Value)
					return;

			if (e.RowIndex > 0)
				this.Validate();
		}

		private void dgvGlobal_DataSourceChanged(object sender, EventArgs e)
		{
			// рисуем кнопки
			dtbGlobal_RowChanged(_dtbGlobal, null);

			if (_dtbGlobal == null) {
				slUnitId.Text = "";
				return;
			}

			string col;
			foreach (DataGridViewColumn dgc in dgvGlobal.Columns) {
				col = dgc.Name.ToLower();
				switch (col) {
					case "session":
					case "nameid":
					case "salt":
					case "hash":
					case "role":
					case "visible":
					case "id":
						dgc.Visible = false;
						break;
				}
			}

			TbNames d = Utils.StrToEnum<TbNames>(_dtbGlobal.TableName);
			switch (d) {
				case TbNames.Users:
					dgvGlobal.Columns.Remove("vrole");

					DataGridViewComboBoxColumn dgvcb = new DataGridViewComboBoxColumn();
					dgvcb.DisplayStyleForCurrentCellOnly = true;
					dgvcb.DataPropertyName = "vrole";
					dgvcb.DataSource = _dtbGlobal.DataSet.Tables["roles"];
					dgvcb.ValueMember = "id";
					dgvcb.DisplayMember = "role";
					dgvcb.HeaderText = "Permission";
					dgvcb.Name = "vrole";
					dgvcb.ReadOnly = false;
					dgvcb.Width = 170;
					dgvcb.MaxDropDownItems = 10;
					dgvGlobal.Columns.Add(dgvcb);

					dgvGlobal.AllowUserToAddRows = true;
					break;
				case TbNames.Config:
					dgvGlobal.Columns["Name"].ReadOnly = true;
					dgvGlobal.AllowUserToAddRows = false;
					if (dgvGlobal.Columns.Contains("Visible")) {
						BindingSource bs = dgvGlobal.DataSource as BindingSource;
						if (bs != null)
							bs.Filter = "Visible=1";
					}
					break;
				case TbNames.Stops:
					// добавляем колонки для сортировки D0, D1, ...
					string flt = "D0";
					for (int n = 1; n < 10; n++) {
						if (_dtbGlobal.Columns.Contains("D" + n.ToString()))
							flt += ", " + "D" + n.ToString();
						else
							break;
					}
					_dtbGlobal.DefaultView.Sort = flt;

					foreach (DataGridViewColumn dgvc in dgvGlobal.Columns) {
						switch (dgvc.Name) {
							case "DownID":
								dgvc.HeaderText = "Id";
								dgvc.Width = 40;
								dgvc.ReadOnly = true;
								break;
							case "EName":
								dgvc.HeaderText = "Name";
								dgvc.Width = 200;
								break;
							case "FullName":
								dgvc.HeaderText = "Full Name";
								dgvc.Width = 300;
								dgvc.ReadOnly = true;
								break;
							case "Valid":
								dgvc.Width = 40;
								break;
							default:
								dgvc.Visible = false;
								break;
						}
					}
					dgvGlobal.AllowUserToAddRows = false;
					break;
				default:
					dgvGlobal.AllowUserToAddRows = true;
					break;
			}

			int db_id = (int)_dtbGlobal.ExtendedProperties["id"];
			MU mu = _config.MonitorUnits[db_id];
			slUnitId.Text = string.Format("{0} ({1}, {2})", mu.LineName, mu.ID, mu.DatabaseName);
			//slUnitId.Visible = true;
			//slUnitId.ToolTipText = mu.Connection;

			////// попытка перенести в последнюю колонку, почему то не работает
			////if (dgvGlobal.Columns.Contains("Valig")) {
			////   DataGridViewColumn dgc = dgvGlobal.Columns["Valig"];
			////   dgvGlobal.Columns.Remove(dgc);
			////   dgvGlobal.Columns.Insert(dgvGlobal.Columns.Count, dgc);
			////}
		}
		private void dgvGlobal_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			Rectangle r = e.RowBounds;
			DataGridView dgv = sender as DataGridView;
			CurrencyManager cm = (CurrencyManager)BindingContext[dgv.DataSource, dgv.DataMember];
			if (e.RowIndex >= cm.List.Count)
				return;
			DataRowView drv = cm.List[e.RowIndex] as DataRowView;
			switch (drv.Row.RowState) {
				case DataRowState.Added:
					// добавляем красную иконку 'Save' в селекторе строке 
					e.Graphics.DrawImage(_imSaveRed, r.Left + 15, r.Top);
					break;
				case DataRowState.Modified:
					// добавляем синюю иконку 'Save' в селекторе строке 
					e.Graphics.DrawImage(_imSaveBlue, r.Left + 15, r.Top);
					break;
				default:
					// for row numbers in row selector:
					string strRowNumber = (e.RowIndex + 1).ToString();
					while (strRowNumber.Length < dgv.RowCount.ToString().Length) strRowNumber = "0" + strRowNumber;
					SizeF size = e.Graphics.MeasureString(strRowNumber, this.Font);
					if (dgv.RowHeadersWidth < (int)(size.Width + 20)) dgv.RowHeadersWidth = (int)(size.Width + 20);
					e.Graphics.DrawString(strRowNumber, this.Font, SystemBrushes.ControlText, r.Left + 15, r.Top + (r.Height - size.Height) / 2);
					break;
			}
		}
		private void dgvGlobal_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//if (dgvGlobal.ReadOnly)
			//   return;
			//DataRowView drv = dgvGlobal.Rows[e.RowIndex].DataBoundItem as DataRowView;
			//if (drv != null && drv.Row.Table.TableName == TbNames.Users.ToString()) {
			//   if (dgvGlobal.Columns[e.ColumnIndex].Name == "vrole") {
			//      string formval = e.FormattedValue.ToString();
			//      UserPermit p = StrToEnum<UserPermit>(e.FormattedValue.ToString());
			//      if ((int)p >=(int)user.UserPermit) {
			//         e.Cancel = true;
			//         dgvGlobal.CancelEdit();
			//      }
			//   }
			//}
		}
		private void dgvGlobal_CellValidated(object sender, DataGridViewCellEventArgs e)
		{
			DataRowView drv = dgvGlobal.Rows[e.RowIndex].DataBoundItem as DataRowView;
			if (drv == null) return;
			if (drv.Row.Table.TableName == TbNames.Users.ToString()) {
				if (dgvGlobal.Columns[e.ColumnIndex].Name == "vrole") {
					int val = (int)dgvGlobal[e.ColumnIndex, e.RowIndex].Value;
					if (_user.Admin && _user.Name.ToLower() == "admin")
						return; // разрешаем все что угодно
					else if (val >= (int)_user.UserPermit) {
						dgvGlobal.CancelEdit();
					}
				}
			} else if (drv.Row.Table.TableName == TbNames.Users.ToString()) {


			}
		}
		private void dgvGlobal_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			DataRowView drv = dgvGlobal.Rows[e.RowIndex].DataBoundItem as DataRowView;
			if (drv != null && drv.Row.Table.TableName == TbNames.Users.ToString()) {
				//if (dgvGlobal.Columns[e.ColumnIndex].Name == "vrole") {
				string formval = drv["vrole"].ToString();
				UserPermit p = UserPermit.None;
				if (formval != "")
					p = Utils.StrToEnum<UserPermit>(drv["vrole"].ToString());
				if ((int)p >= (int)_user.UserPermit) {
					dgvGlobal.ReadOnly = true;
				} else
					dgvGlobal.ReadOnly = false;
				//}
			} else
				dgvGlobal.ReadOnly = !_ok;
		}

		private void dgvLocal_DataSourceChanged(object sender, EventArgs e)
		{
			dtbLocal_RowChanged(_dtbLocal, null);

			if (_dtbLocal == null)
				return;

			if (_dtbLocal.Columns.Contains("Valid")) {
				// эта колонка из родительской таблицы и если в ней Valid = false, то значит это глобальный запрет
				// и эту строку не надо показывать. Не путать с колонкой "chld_valid", которая юзеру отображается 
				// тоже с заголовком Valid
				_dtbLocal.DefaultView.RowFilter = "Valid=1";
			}

			string colname;
			foreach (DataGridViewColumn dgvcol in dgvLocal.Columns) {
				colname = dgvcol.Name.ToLower();
				switch (colname) {
					case "session":
					case "nameid":
					case "salt":
					case "hash":
					case "role":
					case "vrole":
					case "chld":
					case "valid":
					case "id":
					case "monitoringstart":
					case "linename":
						dgvcol.Visible = false;
						break;
					case "chld_valid":
						// это значение берется из r_xxx.valid
						dgvcol.HeaderText = "Valid";
						dgvcol.ReadOnly = true;
						break;
					case "ename":
					case "fullname":
						dgvcol.ReadOnly = true;
						break;
				}
			}

			switch (_dtbLocal.TableName) {
				case "Stops":
					// строим строку вида "D0, D1, D2 ..." для сортировки
					string flt = "D0";
					for (int n = 1; n < 10; n++) {
						if (_dtbLocal.Columns.Contains("D" + n.ToString()))
							flt += ", " + "D" + n.ToString();
						else
							break;
					}
					_dtbLocal.DefaultView.Sort = flt;
					foreach (DataGridViewColumn dgvcol in dgvLocal.Columns) {
						switch (dgvcol.Name) {
							case "DownID":
								dgvcol.HeaderText = "Id";
								dgvcol.Width = 40;
								break;
							case "EName":
								dgvcol.HeaderText = "Name";
								dgvcol.Width = 200;
								break;
							case "FullName":
								dgvcol.HeaderText = "Full Name";
								dgvcol.Width = 300;
								break;
							case "chld_valid":
								dgvcol.Width = 40;
								break;
							default:
								dgvcol.Visible = false;
								break;
						}
					}
					break;
				case "Config":
					if (dgvLocal.Columns.Contains("Name"))
						dgvLocal.Columns["Name"].ReadOnly = true;
					break;
				case "Users":
					//dgvLocal.Columns.Remove("vrole");

					//DataGridViewComboBoxColumn dgvcb = new DataGridViewComboBoxColumn();
					//dgvcb.DisplayStyleForCurrentCellOnly = true;
					//dgvcb.DataPropertyName = "vrole";
					//dgvcb.DataSource = dtbGlobal.DataSet.Tables["roles"];
					//dgvcb.ValueMember = "id";
					//dgvcb.DisplayMember = "role";
					//dgvcb.HeaderText = "Permission";
					//dgvcb.Name = "vrole";
					//dgvcb.ReadOnly = false;
					//dgvcb.Width = 170;
					//dgvcb.MaxDropDownItems = 10;
					//dgvcb.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
					//dgvLocal.Columns.Add(dgvcb);
					break;
				default:
					//dgc.ReadOnly = true;
					break;
			}
			dgvLocal.ReadOnly = !_ok;
		}
		private void dgvLocal_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
			//if (drv != null && drv.Row.Table.Columns.Contains("chld") && (!(bool)drv["chld"]  || (bool)drv["chld_valid"] == false)) {
			//   e.CellStyle.ForeColor = Color.Gray;
			//   e.CellStyle.BackColor = SystemColors.ControlLight;
			//}
			if (drv == null)
				return;
			if (drv.Row.Table.Columns.Contains("chld")) {

				if ((bool)drv["chld"] == false ||
						(drv["chld_valid"] != DBNull.Value && (bool)drv["chld_valid"] == false)) {
					e.CellStyle.ForeColor = Color.Gray;
				} else
					e.CellStyle.ForeColor = Color.Blue;
			}

		}
		private void dgvLocal_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
		{
			Rectangle r = e.RowBounds;
			DataGridView dgv = sender as DataGridView;


			//CurrencyManager cm = (CurrencyManager)BindingContext[dgv.DataSource, dgv.DataMember];
			//if (e.RowIndex >= cm.List.Count)
			//   return;
			//DataRowView drv = cm.List[e.RowIndex] as DataRowView;
			DataRowView drv = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
			switch (drv.Row.RowState) {
				case DataRowState.Added:
					// добавляем красную иконку 'Save' в селекторе строке 
					e.Graphics.DrawImage(_imSaveRed, r.Left + 15, r.Top);
					break;
				case DataRowState.Modified:
					// добавляем синюю иконку 'Save' в селекторе строке 
					e.Graphics.DrawImage(_imSaveBlue, r.Left + 15, r.Top);
					break;
			}

			// отрисовка цветной рамки выбраной строки
			if (dgv == dgvLocal && (e.State & DataGridViewElementStates.Selected) == DataGridViewElementStates.Selected) {
				//string rootdown = drv["RD"] == DBNull.Value ? "tNF" : drv["RD"].ToString();
				using (Pen p = new Pen(Color.DarkBlue)) {
					int hw = dgv.RowHeadersWidth;
					int totalwidth = 0;
					foreach (DataGridViewColumn dgvc in dgv.Columns)
						if (dgvc.Visible)
							totalwidth += dgvc.Width;
					Rectangle rct = new Rectangle(e.RowBounds.X + hw, e.RowBounds.Y, totalwidth - 1, e.RowBounds.Height - 2);
					e.Graphics.DrawRectangle(p, rct);
				}
			}
		}

		private void dgvLang_DataSourceChanged(object sender, EventArgs e)
		{
			////// рисуем кнопки
			////dtbGlobal_RowChanged(dtbGlobal, null);

			if (_dtbLang == null)
				return;

			string col;
			foreach (DataGridViewColumn dgc in dgvLang.Columns) {
				col = dgc.Name.ToLower();
				switch (col) {
					//case "id":
					//case "nameid":
					//case "culture":
					//case "session":
					//   dgc.Visible = false;
					//   break;
					case "ename":
						dgc.HeaderText = "English name";
						dgc.Width = 300;
						dgc.ReadOnly = true;
						break;
					case "value":
						dgc.HeaderText = "Name";
						dgc.Width = 300;
						break;
					default:
						dgc.Visible = false;
						break;
				}
			}

			int db_id = (int)_dtbLang.ExtendedProperties["id"];
			MU mu = _config.MonitorUnits[db_id];
			slUnitId.Text = string.Format("{0} ({1}, {2})", mu.LineName, mu.ID, mu.DatabaseName);
			slUnitId.Visible = true;
			//slUnitId.ToolTipText = mu.Connection;

		}
		private void dgvLang_CellValidated(object sender, DataGridViewCellEventArgs e)
		{
			if (dgvLang.Columns[e.ColumnIndex].Name == "EName")
				dgvLang.CancelEdit();
		}
		private void dgvLang_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			dgvLang.ReadOnly = !_ok;
		}

		private void dgvTargets_DataSourceChanged(object sender, EventArgs e)
		{
			//dgvTargets.ReadOnly = !ok;

			////// Continue only if the data source has been set.
			////if (dgvTargets.DataSource == null) {
			////   return;
			////}

			////// Add the AutoFilter header cell to each column.
			////foreach (DataGridViewColumn col in dgvTargets.Columns) {
			////   col.HeaderCell = new
			////       DataGridViewAutoFilterColumnHeaderCell(col.HeaderCell);
			////}

		}
		private void dgvTargets_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			dgvTargets.ReadOnly = !_ok;
		}
		private void dgvTargets_CellValidated(object sender, DataGridViewCellEventArgs e)
		{
			if (dgvTargets.Columns[e.ColumnIndex].Name == "Year") {
				//int year = (int)dgvTargets[e.ColumnIndex, e.RowIndex].Value;
				int year = 0;
				int.TryParse(dgvTargets[e.ColumnIndex, e.RowIndex].Value.ToString(), out year);
				if (year < 2006 || year > 2050)
					return;
				DateTime dt = new DateTime(year, 1, 1);
				DataRowView drv = dgvTargets.Rows[e.RowIndex].DataBoundItem as DataRowView;
				DateTime dt2 = new DateTime(1, 1, 1);
				if (drv["FromDate"] != DBNull.Value)
					dt2 = (DateTime)drv["FromDate"];
				if (dt != dt2)
					drv["FromDate"] = dt;
			}
		}
		private void dgvTargets_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (dgvTargets.Columns[e.ColumnIndex].Name == "Year") {
				int year = 0;
				int.TryParse(e.FormattedValue.ToString(), out year);
				if (year < 2006 || year > 2050)
					dgvTargets.CancelEdit();	//e.Cancel = true;
					
			}
		}

		#endregion

		#region | save & undo & refresh |

		private void mnuRefresh_Click(object sender, EventArgs e)
		{
			// Сбрасываем флаг чтобы показать текст при первой ошибке
			_continueUpdateOnError = false;

			_tbMap.Clear();
			_locals.Clear();
			_localsCash.Clear();

			//LoadData(true);
			SetUnit(_unit);
			ReloadKpiTree();
		}
		private void btGlobalUndo_Click(object sender, EventArgs e)
		{
			if (_dtbGlobal != null) {
				_dtbGlobal.RejectChanges();
				dtbGlobal_RowChanged(_dtbGlobal, null);
				dgvGlobal.Refresh();
				if (_dtbGlobal.TableName == TbNames.Stops.ToString())
					ReloadKpiTree();
			}
		}
		private void btGlobalSave_Click(object sender, EventArgs e)
		{
			SaveGlobal();
		}
		private void SaveGlobal()
		{
			if (_dtbGlobal == null)
				return;

			DataTable dtb = _dtbGlobal;
			int db_id = (int)dtb.ExtendedProperties["id"];
			string conn = _config.MonitorUnits[db_id].Connection;
			if (dtb.Columns.Contains("session")) {
				if (!_sessions.ContainsKey(db_id))
					GetCredentials(_config.MonitorUnits[db_id]);
				if (!_sessions.ContainsKey(db_id))
					return;

				foreach (DataRow r in dtb.Select("", "", DataViewRowState.Added))
					r["session"] = _sessions[db_id].SessionID;
				DataRow[] DR = dtb.Select("", "", DataViewRowState.ModifiedCurrent);
				DataRow[] DR2 = dtb.Select("", "", DataViewRowState.ModifiedOriginal); 
				foreach (DataRow r in DR)
					r["session"] = _sessions[db_id].SessionID;
			}
			SetCursWait();

			try {
				using (SqlConnection cn = new SqlConnection(conn)) {
					if (dtb.TableName == TbNames.Brands.ToString()) {
						#region | brands     |

						using (SqlDataAdapter da = new SqlDataAdapter()) {

							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							// хитрая вставка: сначала вставляем в таблицу глобальных ID, а полученное из нее SCOPE_IDENTITY
							// уже используем как Id для новой строки
							da.InsertCommand = new SqlCommand("INSERT INTO g_id(session) VALUES(@session) " +
									" SET @ID = SCOPE_IDENTITY() " +
									" INSERT INTO g_brands(ID, EName, Valid, Session) VALUES(@ID, @EName, @Valid, @Session)");

							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							da.InsertCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.InsertCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;
							da.InsertCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");
							//da.InsertCommand.Parameters["@session"].Value = session;

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
							da.UpdateCommand = new SqlCommand("UPDATE g_brands SET EName = @EName, Valid = @Valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.UpdateCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));

							////// ~~~~~~~~~~~~~~~~~~~ delete ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.DeleteCommand = new SqlCommand("DELETE FROM g_brands WHERE ID = @ID");
							da.DeleteCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

							da.DeleteCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Deleted));

							//Thread.Sleep(1000);
						}
						#endregion
					} else if (dtb.TableName == TbNames.Formats.ToString()) {
						#region | formats    |

						using (SqlDataAdapter da = new SqlDataAdapter()) {

							da.InsertCommand = new SqlCommand("INSERT INTO g_id(session) VALUES(@session) " +
								" SET @ID = SCOPE_IDENTITY() " +
								" INSERT INTO g_formats(ID, EName, Valid, Volume, PerCase, Session) " +
								"VALUES(@ID, @EName, @Valid, @Volume, @PerCase, @Session)");

							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							da.InsertCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 50, "EName");
							da.InsertCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.InsertCommand.Parameters.Add("@Volume", SqlDbType.Real, 4, "Volume");
							da.InsertCommand.Parameters.Add("@PerCase", SqlDbType.Int, 4, "PerCase");
							da.InsertCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
							da.UpdateCommand = new SqlCommand("UPDATE g_formats SET EName = @EName, Valid = @Valid, " +
								"Volume = @Volume, PerCase = @PerCase, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.UpdateCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.UpdateCommand.Parameters.Add("@Volume", SqlDbType.Real, 4, "Volume");
							da.UpdateCommand.Parameters.Add("@PerCase", SqlDbType.Int, 4, "PerCase");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
						}
						#endregion
					} else if (dtb.TableName == TbNames.Shifts.ToString()) {
						#region | shifts     |

						using (SqlDataAdapter da = new SqlDataAdapter()) {

							da.InsertCommand = new SqlCommand("INSERT INTO g_id(session) VALUES(@session) " +
								" SET @ID = SCOPE_IDENTITY() " +
								" INSERT INTO g_shifts(ID, EName, Valid, Session) " +
								" VALUES(@ID, @EName, @Valid, @Session)");

							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							da.InsertCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 50, "EName");
							da.InsertCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.InsertCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID").Direction = ParameterDirection.Output;

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
							da.UpdateCommand = new SqlCommand("UPDATE g_shifts SET EName = @EName, " +
								"Valid = @Valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.UpdateCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
						}
						#endregion
					} else if (dtb.TableName == TbNames.Stops.ToString()) {
						#region | stops      |
						using (SqlDataAdapter da = new SqlDataAdapter()) {
							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO g_id(session) VALUES(@session) " +
									" SET @DownID = SCOPE_IDENTITY() " +
									" INSERT INTO g_stops(DownID, TopID, KpiID, EName, Valid, Session) " +
									" VALUES(@DownID, @TopID, @KpiID, @EName, @Valid, @Session)");
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							da.InsertCommand.Parameters.Add("@TopID", SqlDbType.Int, 4, "TopID");
							da.InsertCommand.Parameters.Add("@KpiID", SqlDbType.Int, 4, "KpiID");
							da.InsertCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.InsertCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.InsertCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID").Direction = ParameterDirection.Output;
							//da.InsertCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "Session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							//da.Update(dtb.Select("", "D0, D1, D2", DataViewRowState.Added));
							Upd(tv_kpi.Nodes, da, 0);

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE g_stops SET TopID = @TopID, KpiID=@KpiID, " +
										"EName = @EName, Valid = @Valid, Session=@Session WHERE DownID = @DownID");
							da.UpdateCommand.Parameters.Add("@TopID", SqlDbType.Int, 4, "TopID");
							da.UpdateCommand.Parameters.Add("@KpiID", SqlDbType.Int, 4, "KpiID");
							da.UpdateCommand.Parameters.Add("@EName", SqlDbType.NVarChar, 100, "EName");
							da.UpdateCommand.Parameters.Add("@Valid", SqlDbType.Bit, 1, "Valid");
							da.UpdateCommand.Parameters.Add("@DownID", SqlDbType.Int, 4, "DownID");
							da.UpdateCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedCurrent));
						}
						#endregion
					} else if (dtb.TableName == TbNames.Users.ToString()) {
						#region | users      |
						using (SqlDataAdapter da = new SqlDataAdapter()) {
							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							int salt;
							foreach (DataRow r in dtb.Select("", "", DataViewRowState.Added)) {
								salt = Guid.NewGuid().GetHashCode();
								r["id"] = _newId--;
								r["salt"] = salt;
								r["hash"] = C.GetMd5Hash(frmLogin.user_first_login_psw, salt);
								r["role"] = C.GetMd5Hash(r["vrole"].ToString(), salt);
								r["session"] = _user.Session;
								r.EndEdit();
							}
							da.InsertCommand = new SqlCommand(
									" INSERT INTO g_users(Name, Salt, Hash, Role, Session) " +
									" VALUES(@Name, @Salt, @Hash, @Role, @Session) " +
									" SET @Id = SCOPE_IDENTITY() ");
							da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
							da.InsertCommand.Parameters.Add("@Name", SqlDbType.NVarChar, 50, "Name");
							da.InsertCommand.Parameters.Add("@Salt", SqlDbType.Int, 4, "Salt");
							da.InsertCommand.Parameters.Add("@Hash", SqlDbType.Char, 32, "Hash");
							da.InsertCommand.Parameters.Add("@Role", SqlDbType.Char, 32, "Role");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "Session");
							da.InsertCommand.Parameters.Add("@Id", SqlDbType.Int, 4, "Id").Direction = ParameterDirection.Output;

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							foreach (DataRow r in dtb.Select("", "", DataViewRowState.ModifiedOriginal)) {
								salt = Guid.NewGuid().GetHashCode();
								r["salt"] = salt;
								r["hash"] = C.GetMd5Hash(frmLogin.user_first_login_psw, salt);
								r["Role"] = C.GetMd5Hash(r["vrole"].ToString(), salt);
								r["session"] = _user.Session;
								r.EndEdit();
							}

							da.UpdateCommand = new SqlCommand("UPDATE g_users SET Name = @Name, Salt = @Salt, " +
										"Hash = @Hash, Role = @Role, Session = @Session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@Name", SqlDbType.NVarChar, 50, "Name");
							da.UpdateCommand.Parameters.Add("@Salt", SqlDbType.Int, 4, "Salt");
							da.UpdateCommand.Parameters.Add("@Hash", SqlDbType.Char, 32, "Hash");
							da.UpdateCommand.Parameters.Add("@Role", SqlDbType.Char, 32, "Role");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "Session");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));

						}
						#endregion
					} else if (dtb.TableName == TbNames.Config.ToString()) {
						#region | stops      |
						using (SqlDataAdapter da = new SqlDataAdapter()) {
							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE g_cfg SET Name=@Name, " +
										"Value = @Value, Session=@Session WHERE id=@id");
							da.UpdateCommand.Parameters.Add("@id", SqlDbType.Int, 4, "id");
							da.UpdateCommand.Parameters.Add("@Name", SqlDbType.NVarChar, 50, "Name");
							da.UpdateCommand.Parameters.Add("@Value", SqlDbType.NVarChar, 50, "Value");
							da.UpdateCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
						}
						#endregion
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				// при первой возникшей ошибке показывается текст ошибки, а потом устанавливается флаг 
				// для продолжения обновления несмотря на ошибки
				_continueUpdateOnError = true;
			}
			SaveDateCash();
			SetCursDefault();
			dgvGlobal.Refresh();
		}
		private void Upd(TreeNodeCollection TN, SqlDataAdapter da, int newTopId)
		{
			List<DataRow> DR = new List<DataRow>();
			foreach (TreeNode tn in TN) {
				DataRow dr = tn.Tag as DataRow;
				if (newTopId!= 0) dr["TopId"] = newTopId;
				if (dr.RowState == DataRowState.Added)
				DR.Add(dr);
			}
			da.Update(DR.ToArray());
			foreach (TreeNode tn in TN) {
				DataRow dr = tn.Tag as DataRow;
				int newId = Convert.ToInt32(dr["DownId"]);
				Upd(tn.Nodes, da, newId);
			}
		}

		private void btLocalRefresh_Click(object sender, EventArgs e)
		{
			if (_locals.ContainsKey(_unit.ID))
				_locals.Remove(_unit.ID);
			if (_localsCash.ContainsKey(_unit.ID))
				_localsCash.Remove(_unit.ID);
			
			LoadData(true);
			ReloadKpiTree();
			SetGlobalSource();	
			SetLocalSource();		
		}
		private void btLocalUndo_Click(object sender, EventArgs e)
		{
			if (_dtbLocal != null) {
				_dtbLocal.Clear();
				_dtbLocal.Merge(_localsCash[_unit.ID].Tables[_dtbLocal.TableName].Copy());
				//dtbLocal_RowChanged(dtbGlobal, null);
				dgvLocal.Refresh();
			}
		}
		private void btLocalSave_Click(object sender, EventArgs e)
		{
			if (_dtbLocal == null)
				return;

			this.Validate();
			SetCursWait();
			DataTable dtb = _dtbLocal;
			string conn = _unit.Connection;
			if (dtb.Columns.Contains("session")) {
				//todone: номер сессии надо подставлять другой!!!
				foreach (DataRow r in dtb.Select("", "", DataViewRowState.Added))
					r["session"] = _sessions[_unit.ID].SessionID; // user.Session;
				foreach (DataRow r in dtb.Select("", "", DataViewRowState.ModifiedOriginal))
					r["session"] = _sessions[_unit.ID].SessionID; // user.Session;
			}
			try {
				using (SqlConnection cn = new SqlConnection(conn)) {
					if (dtb.TableName == TbNames.Stops.ToString()) {
						#region | stops    |
						using (SqlDataAdapter da = new SqlDataAdapter()) {
							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO r_stops(id, valid, session) VALUES(@ID, 1, @session)");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "DownID");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE r_stops SET valid=@valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "DownID");
							da.UpdateCommand.Parameters.Add("@valid", SqlDbType.Bit, 1, "chld_valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
						}
						#endregion
					} else if (dtb.TableName == TbNames.Brands.ToString()) {
						#region | brands   |
						using (SqlDataAdapter da = new SqlDataAdapter()) {

							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO r_brands(id, valid, session) VALUES(@ID, 1, @session)");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE r_brands SET valid=@valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.UpdateCommand.Parameters.Add("@valid", SqlDbType.Bit, 1, "chld_valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));

							//Thread.Sleep(1000);
						}
						#endregion
					} else if (dtb.TableName == TbNames.Formats.ToString()) {
						#region | formats  |
						foreach (DataRow r in dtb.Rows)
							if (r.RowState != DataRowState.Unchanged && r["rate"] == DBNull.Value && (bool)r["valid"]) {
								MessageBox.Show("There is no any value in 'Rate' field." +
									Environment.NewLine + "Need to complete empty values before.",
									"Records are not completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
								SetCursDefault();
								return;
							}
						using (SqlDataAdapter da = new SqlDataAdapter()) {

							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO r_formats(id, valid, rate, session) VALUES(@ID, 1, @rate, @session)");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.InsertCommand.Parameters.Add("@rate", SqlDbType.Real, 4, "rate");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							//todone: проверить на NULL поле rate перед сохранением
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE r_formats SET rate=@rate, valid=@valid, session=@session  WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.UpdateCommand.Parameters.Add("@rate", SqlDbType.Real, 4, "rate");
							da.UpdateCommand.Parameters.Add("@valid", SqlDbType.Bit, 1, "chld_valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));

							//Thread.Sleep(1000);
						}
						#endregion
					} else if (dtb.TableName == TbNames.Shifts.ToString()) {
						#region | shifts   |
						using (SqlDataAdapter da = new SqlDataAdapter()) {

							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO r_shifts(id, valid, session) VALUES(@ID, 1, @session)");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE r_shifts SET valid=@valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.UpdateCommand.Parameters.Add("@valid", SqlDbType.Bit, 1, "chld_valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));

							//Thread.Sleep(1000);
						}
						#endregion
					} else if (dtb.TableName == TbNames.Users.ToString()) {
						#region | users    |
						using (SqlDataAdapter da = new SqlDataAdapter()) {

							// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.InsertCommand = new SqlCommand("INSERT INTO r_users(id, valid, session) VALUES(@ID, 1, @session)");
							da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.InsertCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.Added));

							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE r_users SET valid=@valid, session=@session WHERE ID = @ID");
							da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
							da.UpdateCommand.Parameters.Add("@valid", SqlDbType.Bit, 1, "chld_valid");
							da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
						}
						#endregion
					} else if (dtb.TableName == TbNames.Config.ToString()) {
						#region | config   |
						using (SqlDataAdapter da = new SqlDataAdapter()) {
							////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							da.UpdateCommand = new SqlCommand("UPDATE c_cfg SET Name=@Name, " +
								"Value = @Value, Session=@Session WHERE id=@id");
							da.UpdateCommand.Parameters.Add("@id", SqlDbType.Int, 4, "id");
							da.UpdateCommand.Parameters.Add("@Name", SqlDbType.NVarChar, 50, "Name");
							da.UpdateCommand.Parameters.Add("@Value", SqlDbType.NVarChar, 50, "Value");
							da.UpdateCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");

							da.UpdateCommand.Connection = cn;
							da.ContinueUpdateOnError = _continueUpdateOnError;
							da.Update(dtb);
						}
						#endregion
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Database update error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				// при первой возникшей ошибке показывается текст ошибки, а потом устанавливается флаг 
				// для продолжения обновления несмотря на ошибки
				_continueUpdateOnError = true;
			}
			SaveDateCash();
			SetCursDefault();
			dgvLocal.Refresh();
		}
		private void btLangUndo_Click(object sender, EventArgs e)
		{
			if (_dtbLang != null) {
				_dtbLang.RejectChanges();
				dtbLang_RowChanged(_dtbLang, null);
				dgvLang.Refresh();
			}
		}
		private void btLangSave_Click(object sender, EventArgs e)
		{
			if (_dtbLang == null)
				return;

			DataTable dtb = _dtbLang;
			int db_id = (int)dtb.ExtendedProperties["id"];
			string conn = _config.MonitorUnits[db_id].Connection;
			if (dtb.Columns.Contains("session")) {
				if (!_sessions.ContainsKey(db_id))
					GetCredentials(_config.MonitorUnits[db_id]);
				if (!_sessions.ContainsKey(db_id))
					return;

				foreach (DataRow r in dtb.Select("", "", DataViewRowState.Added))
					r["session"] = _sessions[db_id].SessionID;
				foreach (DataRow r in dtb.Select("", "", DataViewRowState.ModifiedOriginal))
					r["session"] = _sessions[db_id].SessionID;
			}
			SetCursWait();

			foreach (DataRow dr in dtb.Rows) {
				if (dr.RowState == DataRowState.Modified &&
					dr["NameID"] == DBNull.Value) {
					dr.AcceptChanges();
					dr.SetAdded();
				}
			}
			string upd = "";
			if (dtb.TableName == TbNames.Brands.ToString())
				upd = " UPDATE g_brands SET NameID = @ID WHERE ID = @ID ";
			else if (dtb.TableName == TbNames.Formats.ToString())
				upd = " UPDATE g_formats SET NameID = @ID WHERE ID = @ID ";
			else if (dtb.TableName == TbNames.Stops.ToString())
				upd = " UPDATE g_stops SET NameID = @ID WHERE DownID = @ID ";
			else if (dtb.TableName == TbNames.Kpi.ToString())
				upd = " UPDATE g_kpi SET NameID = @ID WHERE KpiID = @ID ";
			else if (dtb.TableName == TbNames.Shifts.ToString())
				upd = " UPDATE g_shifts SET NameID = @ID WHERE ID = @ID ";

			try {
				using (SqlConnection cn = new SqlConnection(conn)) {
					cn.Open();
					using (SqlDataAdapter da = new SqlDataAdapter()) {
						// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						da.InsertCommand = new SqlCommand(" INSERT INTO g_names(ID, Culture, Value, Session) VALUES(@ID, @Culture, @Value, @Session) " + upd);
						//da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
						da.InsertCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
						da.InsertCommand.Parameters.Add("@Culture", SqlDbType.Int, 4).Value = 1049;
						da.InsertCommand.Parameters.Add("@Value", SqlDbType.NVarChar, 100, "Value");
						da.InsertCommand.Parameters.Add("@Session", SqlDbType.Int, 4, "Session");
						da.InsertCommand.Connection = cn;
						da.ContinueUpdateOnError = _continueUpdateOnError;
						da.Update(dtb.Select("", "", DataViewRowState.Added));
						////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
						da.UpdateCommand = new SqlCommand("UPDATE g_names SET Culture = @Culture, Value = @Value, session=@session WHERE ID = @ID");
						da.UpdateCommand.Parameters.Add("@ID", SqlDbType.Int, 4, "NameID");
						da.UpdateCommand.Parameters.Add("@Culture", SqlDbType.Int, 4).Value = 1049;
						da.UpdateCommand.Parameters.Add("@Value", SqlDbType.NVarChar, 100, "Value");
						da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
						da.UpdateCommand.Connection = cn;
						da.ContinueUpdateOnError = _continueUpdateOnError;
						da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
					}

					//Thread.Sleep(1000);
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Database update error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				// при первой возникшей ошибке показывается текст ошибки, а потом устанавливается флаг 
				// для продолжения обновления несмотря на ошибки
				_continueUpdateOnError = true;
			}
			SaveDateCash();
			SetCursDefault();
			dgvLang.Refresh();
		}

		private void btTargetsSave_Click(object sender, EventArgs e)
		{
			if (_dtbTargets == null)
				return;

			SetCursWait();
			DataTable dtb = _dtbTargets;
			string conn = _unit.Connection;

			foreach (DataRow r in dtb.Select("", "", DataViewRowState.Added))
				r["session"] = _sessions[_unit.ID].SessionID;
			foreach (DataRow r in dtb.Select("", "", DataViewRowState.ModifiedOriginal))
				r["session"] = _sessions[_unit.ID].SessionID;

			try {
				using (SqlConnection cn = new SqlConnection(conn)) {
					using (SqlDataAdapter da = new SqlDataAdapter()) {

						// ~~~~~~~~~~~~~~~~~~ insert ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						da.InsertCommand = new SqlCommand("INSERT INTO l_targets(KpiID, FromDate, Target, session, [Year]) " +
							" VALUES(@KpiID, @FromDate, @Target, @session, @Year)" +
							" SET @TargetID = SCOPE_IDENTITY() ");
						da.InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
						da.InsertCommand.Parameters.Add("@KpiID", SqlDbType.Int, 4, "KpiID");
						da.InsertCommand.Parameters.Add("@FromDate", SqlDbType.SmallDateTime, 4, "FromDate");
						da.InsertCommand.Parameters.Add("@Target", SqlDbType.Real, 4, "Target");
						da.InsertCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
						da.InsertCommand.Parameters.Add("@Year", SqlDbType.Int, 4, "Year");
						da.InsertCommand.Parameters.Add("@TargetID", SqlDbType.Int, 4, "TargetID").Direction = ParameterDirection.Output;

						da.InsertCommand.Connection = cn;
						da.ContinueUpdateOnError = _continueUpdateOnError;
						da.Update(dtb.Select("", "", DataViewRowState.Added));

						////// ~~~~~~~~~~~~~~~~~~~ update ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						da.UpdateCommand = new SqlCommand("UPDATE l_targets SET KpiID=@KpiID, FromDate=@FromDate, " +
							"Target=@Target, session=@session, [Year]=@Year WHERE TargetID = @TargetID");
						da.UpdateCommand.Parameters.Add("@TargetID", SqlDbType.Int, 4, "TargetID");
						da.UpdateCommand.Parameters.Add("@FromDate", SqlDbType.SmallDateTime, 4, "FromDate");
						da.UpdateCommand.Parameters.Add("@KpiID", SqlDbType.Int, 4, "KpiID");
						da.UpdateCommand.Parameters.Add("@Target", SqlDbType.Real, 4, "Target");
						da.UpdateCommand.Parameters.Add("@session", SqlDbType.Int, 4, "session");
						da.UpdateCommand.Parameters.Add("@Year", SqlDbType.Int, 4, "Year");

						da.UpdateCommand.Connection = cn;
						da.ContinueUpdateOnError = _continueUpdateOnError;
						da.Update(dtb.Select("", "", DataViewRowState.ModifiedOriginal));
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Database update error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				// при первой возникшей ошибке показывается текст ошибки, а потом устанавливается флаг 
				// для продолжения обновления несмотря на ошибки
				_continueUpdateOnError = true;
			}
			SaveDateCash();
			SetCursDefault();
			// чтобы иконки в гриде убрались
			dgvTargets.Refresh();
		}
		private void btTargetsUndo_Click(object sender, EventArgs e)
		{
			if (_dtbTargets != null) {
				_dtbTargets.RejectChanges();
				dtbTargets_RowChanged(_dtbTargets, null);
				dgvTargets.Refresh();
			}
		}
		private void SaveDateCash()
		{
			MU unit;
			string cmd_upd = string.Format("Update c_cfg SET [Value] = '{0:yyyyMMdd HH:mm}'  WHERE id=9", DateTime.Now);
			string cmd_ins;
			foreach (int key in ToUpdateCashDate) {
				unit = _config.MonitorUnits[key];
				try {
					using (SqlConnection connection = new SqlConnection(unit.Connection)) {
						connection.Open();
						SqlCommand command = new SqlCommand(cmd_upd, connection);
						if (command.ExecuteNonQuery() == 0) {
							cmd_ins = string.Format("INSERT INTO c_cfg([id], [Name], [Value], [Session]) " +
								"Values (9, 'Valid Date', '{0:yyyyMMdd HH:mm}', {1})", DateTime.Now, _sessions[unit.ID].SessionID);
							command = new SqlCommand(cmd_ins, connection);
							command.ExecuteNonQuery();
						}
						_needToUpdate = true;
					}
				} catch (Exception ex) {
					MessageBox.Show(ex.ToString());
				}
			}
			ToUpdateCashDate.Clear();
		}

		#endregion

		#region | credentials           |
		/// <summary>
		/// Возвращает true только в том случае, если проверка для конечного юнита (не узла!!!) пройдена успешно,
		/// т.е. есть разрешение на редактирование этого юнита текущим юзером
		/// </summary>
		/// <param name="mu"></param>
		/// <returns></returns>
		private bool GetCredentials(MU mu)
		{
			bool res = false;
			SessData sd;

			#region | проходим один раз, но до первой успешной попытки |
			if (_user == null) {
				if (MU.UseWinAuthentication && !_forceAppAccount) {
					string username = Environment.UserDomainName + "\\" + Environment.UserName;
					DataTable dtb = frmLogin.LookupUser(username, _config.Units[mu.ParentIdKpi].Connection);
					DataRow r;
					if (dtb != null && dtb.Rows.Count > 0) {
						r = dtb.Rows[0];
						_user = new MonitorUser((int)r["ID"], username, C.GetInt(r["Session"].ToString()));
						_user.Permission = frmLogin.GetRole(r["Role"].ToString(), (int)r["Salt"]);
					} else if (dtb != null) {
						// таблица пустая т.е. юзера можно больше не искать, прав у него нет 
						// и мы сюда больше не попадем, и поэтому говорим юзеру о случившемся
						MessageBox.Show("You have no permission to handle any master data.", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						_user = new MonitorUser(0, username, 0);
						_user.Permission = 0;
						_user.RestrictedMessageWasShown = true;
					}
					if (_user == null) {
						// произошла ошибка при соединении с базой данных поэтому пользователя не создаем
						MessageBox.Show("Не удалось получить данные", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				} else {
					frmLogin f = new frmLogin(_config.Units[mu.ParentIdKpi].Connection);
					if (f.ShowDialog() == DialogResult.OK) {
						_user = f.User;
					}
					// если юзер найден, но прав у него нет то мы сюда больше не попадем, и поэтому говорим юзеру о случившемся
					if (_user != null && !_user.Admin && !_user.LocalAdmin) {
						if (!_user.RestrictedMessageWasShown) {
							MessageBox.Show("Your account has been verified, but you have no permission to handle the master data.", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							_user.RestrictedMessageWasShown = true;
						}
					}
				}
				if (_user != null) {
					slUser.Text = _user.Name;
					slPermission.Text = _user.UserPermit.ToString();
					////if (user.UserPermit == UserPermit.Disabled)
					////   Close();
				}
			}
			#endregion

			if (_user == null || mu.Connection == "" || (!_user.Admin && !_user.LocalAdmin))
				return false;

			// если у юнита есть свое соединение и нет в кэше то делаем запрос сессии
			if (!_sessions.ContainsKey(mu.ID)) {
				SetCursWait();

				int sess = 0;
				sd = new SessData();
				sess = GetSession(_user, mu.Connection);
				if (sess < 0) {
					SetCursDefault();
					return false; // произошла какая-то ошибка, надо пробовать еще раз, т.е. выходим без добавления сессии
				}
				sd.SessionID = sess;
				sd.Valid = sess > 0;
				if (sd.Valid) {
					sd.Permission = (UserPermit)_user.Permission;
					res = true;
				} else {
					sd.Permission = UserPermit.None;
					//sd.RestrictedMessageWasShown = true;
					//MessageBox.Show("You have no permission to handle this line", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				_sessions.Add(mu.ID, sd);

				SetCursDefault();
			} else
				res = _sessions[mu.ID].Valid;

			return res;
		}
		/// <summary>
		/// Если соединение прошло с ошибкой то вернет -1
		/// Если соединение прошло но либо юзера нет в таблице r_users либо есть но valid = false то возвращает ноль
		/// Если r_users.valid  = true и юзер является локальным админом то получаем номер сессии 
		/// или если глобальный админ то получаем номер сессии без всяких других условий
		/// </summary>
		/// <param name="User"></param>
		/// <param name="connStr"></param>
		/// <returns></returns>
		private int GetSession(MonitorUser User, string connStr)
		{
			// 
			DateTime dt1 = DateTime.Now;
			int sess = -1;
			string hash = C.GetMd5Hash(dt1.ToString("yyyyMMddHHmmss") + User.Name, dt1.Minute);
			DataTable result = new DataTable();

			const string query = "e_getsession";
			using (SqlConnection conn = new SqlConnection(connStr)) {
				try {
					conn.Open();
					SqlCommand cmd = new SqlCommand(query, conn);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@id", SqlDbType.Int).Value = User.Id;
					cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = User.Name;
					cmd.Parameters.Add("@dt1", SqlDbType.SmallDateTime).Value = dt1;
					cmd.Parameters.Add("@hash", SqlDbType.Char, 32).Value = hash;
					using (SqlDataReader dr = cmd.ExecuteReader()) {
						result.Load(dr);
						if (result.Rows.Count > 0) {
							sess = C.GetInt(result.Rows[0]["session"]);
							if ((C.GetBool(result.Rows[0]["valid"]) && User.LocalAdmin) || User.Admin)
								return sess;		// если соединение прошло и получен номер сессии и valid то возвращаем номер сессии или если глобальный админ 
							else
								return 0;			// соединение прошло но valid = false, поэтому возвращает ноль
						}
					}
				} catch (Exception ex) {
					MessageBox.Show(ex.Message, "Getting the session number failared", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			return sess;
		}

		private void GetCurrentStatePermission()
		{
			_ok = false;
			_none = false;
			bool sess_ok = _sessions.ContainsKey(_unit.ID) && _sessions[_unit.ID].Valid;

			if (_unit == null || _user == null)
				_none = true;
			else if (_user.Admin)
				_ok = true;
			else if (!_user.LocalAdmin)
				_ok = false;
			else if (tab.SelectedTab == tpLocal || tab.SelectedTab == tpTargets)
				_ok = sess_ok;
			else if (tab.SelectedTab == tpGlobal && _dtbGlobal != null && _dtbGlobal.TableName == TbNames.Users.ToString())
				_ok = sess_ok;		// на глобальной таблице можно если юзер в этом юните является локальным админон 
			else if (tab.SelectedTab == tpGlobal || tab.SelectedTab == tpLang || tab.SelectedTab == tpDnTm) {
				// если глобальная таблица таблица к которой запрашиваются права на 
				// редактирование лежит в той же базе что и таблица KPI то локальному админу
				// доступ не дается.
				int p1 = GetParentTbID(_unit, TbNames.Kpi);
				int p2 = GetParentTbID(_unit);
				if (p1 != p2)
					_ok = sess_ok;
			}
			if (_none)
				this.slAccess.Image = null;
			else if (_ok)
				this.slAccess.Image = global::ProductionMonitor.Properties.Resources.chekout;
			else
				this.slAccess.Image = global::ProductionMonitor.Properties.Resources.stop;

			btInclude.Enabled = _ok;
			btRemove.Enabled = _ok;
		}

		#endregion

		private void tab_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetNodesColor();
			//if (tab.SelectedTab == tpLocal || tab.SelectedTab == tpTargets)
			//slUnitId.Visible = user.Name.ToLower() == "admin" && user.UserPermit == UserPermit.Admin && tab.SelectedTab != tpLocal && tab.SelectedTab != tpTargets;
		}
		private int GetParentTbID(Unit u)
		{
			if (_dtbGlobal == null)
				return 0;
			TbNames n = Utils.StrToEnum<TbNames>(_dtbGlobal.TableName);
			if (tab.SelectedTab == tpDnTm)
				n = TbNames.Stops;
			return GetParentTbID(u, n);
		}
		private int GetParentTbID(Unit u, TbNames n)
		{
			if (u == null)
				return 0;
			switch (n) {
				case TbNames.Brands:
				case TbNames.Formats:
					if (u.Parent != null && u.Parent.ParentIdSku > 0)
						return GetParentTbID(u.Parent, n);
					else
						return u.ParentIdSku;
				case TbNames.Kpi:
				case TbNames.MarkBands:
				case TbNames.Marks:
				case TbNames.Users:
					if (u.Parent != null && u.Parent.ParentIdKpi > 0)
						return GetParentTbID(u.Parent, n);
					else
						return u.ParentIdKpi;
				case TbNames.Stops:
					if (u.Parent != null && u.Parent.ParentIdStops > 0)
						return GetParentTbID(u.Parent, n);
					else
						return u.ParentIdStops;
				case TbNames.Shifts:
					if (u.Parent != null && u.Parent.ParentIdShifts > 0)
						return GetParentTbID(u.Parent, n);
					else
						return u.ParentIdShifts;
				default:
					return 0;
			}
		}
		public bool NeedToUpdateLists
		{
			get { return _needToUpdate; }
		}
		private void frmMaster_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			//if (e.KeyData == Keys.Enter)
			//   this.Validate();
		}

		#region | Excel                 |
		int _maxId;
		private void mnuFromExcel_Click(object sender, EventArgs e)
		{
			DataTable dwn = null;
			if (_tbMap.ContainsKey(_utMap[_unit.ID.ToString() + TbNames.Stops.ToString()]))
				dwn = _tbMap[_utMap[_unit.ID.ToString() + TbNames.Stops.ToString()]];

			var fileName = string.Format("{0}\\kpi_tree.xls", Directory.GetCurrentDirectory());
			var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", fileName);

			string line = _unit.LineName.Replace(' ', '_');

			var adapter = new OleDbDataAdapter(string.Format("SELECT * FROM [{0}$]", line), connectionString);
			var ds = new DataSet();

			adapter.Fill(ds);

			DataTable data = ds.Tables[0];
			data.Columns.Add("DownID", typeof(int));
			data.Columns.Add("TopID", typeof(int));
			data.Columns.Add("KpiID", typeof(int));
			data.Columns.Add("EName", typeof(string));
			data.Columns.Add("Valid", typeof(bool));
			data.Columns.Add("NameID", typeof(int));
			data.Columns.Add("Session", typeof(int));
			data.Columns.Add("GID", typeof(int));

			_maxId = Convert.ToInt32(data.Compute("Max(c1)", ""));

			//data.Rows[0]["TopId"] = data.Rows[0][0]; 
			data.Rows[0]["DownId"] = data.Rows[0][0]; 
			data.Rows[0]["EName"] = data.Rows[0][1];
			data.Rows[0]["Valid"] = true;
			data.Rows[0]["KpiID"] = data.Rows[0][0];
			data.Rows[0]["Session"] = _user.Session;

			for (int r = 1; r < data.Rows.Count; r++) 
				FillRow(data.Rows[r - 1], data.Rows[r]);
			
			for (int r = data.Rows.Count - 1; r >= 0; r--) 
				if (data.Rows[r]["EName"] == DBNull.Value)
					data.Rows.Remove(data.Rows[r]);

			DataTable newTb = data.DefaultView.ToTable(TbNames.Stops.ToString(), false, new string[] { 
				"DownID", "TopId", "KpiID", "EName", "Valid", "NameID", "Session", "GID" });

			newTb.Columns.Add("FullName", typeof(string));
			_unit.DownLevels = 0;
			FillFullNameColumn(_unit, 0, 0, "", newTb, "TopID is NULL");
			FillParents(_unit, newTb);
			newTb.AcceptChanges();

			newTb.ExtendedProperties.Add("id", dwn.ExtendedProperties["id"]);
			newTb.RowChanged += new DataRowChangeEventHandler(dtbGlobal_RowChanged);
			newTb.Columns["EName"].MaxLength = 100;
			//TbMap.Add(tb0_key, dts.Tables[0]);

			_tbMap[_utMap[_unit.ID.ToString() + TbNames.Stops.ToString()]] = newTb;


			foreach (DataRow dr in newTb.Rows) {
				if (dwn.Select("DownId=" + dr["DownId"].ToString(), "").Length == 0)
					dr.SetAdded();
			}
		}
		private void FillRow(DataRow prev, DataRow curr)
		{
			string val;
			int wasvalue = 0;

			if (curr[0].ToString() == "")
				curr[0] = ++_maxId;
			curr["DownID"] = Convert.ToInt32(curr[0]);

			foreach (DataColumn col in prev.Table.Columns) {
				val = curr[col.Ordinal].ToString();
				if (col.Ordinal == 0)
					continue;
				if (col.Ordinal >= prev.Table.Columns["DownID"].Ordinal)
					continue;
				if (val == "") {
					if (!int.TryParse(prev[col.Ordinal].ToString(), out wasvalue))
						curr[col.Ordinal] = prev["DownId"];
					else
						curr[col.Ordinal] = prev[col.Ordinal];
				} else {
					if (col.Ordinal == 1)
						curr["KpiID"] = curr["DownID"];
					else {
						curr["KpiID"] = prev["KpiID"];
						curr["TopId"] = curr[col.Ordinal - 1];
					}
					curr["EName"] = val;
					curr["Valid"] = true;
					curr["Session"] = _user.Session;
					return;
				}
			}
		}

		Dictionary<string, XLStyle> _xlstyles;
		private void mnuToExcel_Click(object sender, EventArgs e)
		{
			try {
				using (SaveFileDialog sd = new SaveFileDialog()) {
					sd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
					sd.FileName = "Downtimes.xls";
					sd.Filter = "Excel|*.xls";
					if (sd.ShowDialog() == DialogResult.OK && sd.FileName != "") {
						this.ResumeLayout();
						Cursor.Current = Cursors.WaitCursor;
						Application.DoEvents();
						using (C1XLBook book = new C1XLBook()) {
							if (_kpi != null) {
								SetCursWait();
								foreach (MU u in _config.MonitorUnits.Values) {
									if (u.Kind != UnitKind.Line)
										continue;

									XLSheet sh = book.Sheets.Add(u.GetLongUnitName(false, "|"));
									int n = 1;
									sh.Columns[0].Width = 0;
									if (_tbMap.ContainsKey(_utMap[u.ID.ToString() + TbNames.Stops.ToString()]))
										AddKpiTree(_tbMap[_utMap[u.ID.ToString() + TbNames.Stops.ToString()]], sh, "TopID is NULL", ref n, 1);
								}
								SetCursDefault();
							}
							if (book.Sheets.Count > 1)
								book.Sheets.RemoveAt(0);
							book.Save(sd.FileName);
						}
						System.Diagnostics.Process.Start(sd.FileName);
						Cursor.Current = Cursors.Default;
					}
				}

			} catch (Exception ex) {
				MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Stop);
			}

		}
		private void AddKpiTree(DataTable tb, XLSheet sh, string Select, ref int row, int col)
		{
			if (tb == null)
				return;
			DataRow[] DR = tb.Select(Select);
			foreach (DataRow dr in DR) {
				sh[row, 0].Value = dr["DownID"].ToString();
				sh[row, col].Value = dr["EName"].ToString();
				row++;
				AddKpiTree(tb, sh, string.Format("{0}={1}", "TopID", dr["DownID"]), ref row, col + 1);
			}
		}
		#endregion
	}
}

/*
CultureInfo:

en-GB	0x0809 2057 English (United Kingdom) 
ru-RU 0x0419 1049	Russian (Russia) 
nl-NL 0x0413 1043 Dutch (Netherlands)
de-DE 0x0407 1031 German (Germany) 
 
*/








