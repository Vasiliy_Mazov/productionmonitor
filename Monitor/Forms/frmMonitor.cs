using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using C1.Win.C1Chart;
using C1.Win.C1FlexGrid;
using System.Runtime.InteropServices;
using ProductionMonitor.Forms;
using Res = ProductionMonitor.Properties.Resources;
using ProductionMonitor.Controls;

namespace ProductionMonitor
{
	public sealed partial class frmMonitor : Form
	{
		[STAThread]
		public static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			#region | exception |

			// Create new instance of UnhandledExceptionDlg:
			UnhandledExceptionDlg exDlg = new UnhandledExceptionDlg();

			// Uncheck "Restart App" check box by default:
			exDlg.SendReport = false;

			// Add handling of OnShowErrorReport.
			// If you skip this then link to report details won't be showing.
			exDlg.OnShowErrorReport += delegate (object sender, SendExceptionClickEventArgs ar)
			{
				MessageBox.Show(
					string.Format(@"
OS Version: {0}
\nFramework: {1}
\nProgram Version: {2}
\nUserDomainName: {3}
\nMachineName: {4}
\nSystem started: {5:0}
\nCurrentDirectory: {6}
\nUnhandledException.Message:\n {7}
\nUnhandledException.StackTrace:\n {8}\n",
	Environment.OSVersion,
	Environment.Version,
	Application.ProductVersion,
	Environment.UserDomainName,
	Environment.MachineName,
	Environment.TickCount / 60000,
	Environment.CurrentDirectory,
	ar.UnhandledException.Message,
	ar.UnhandledException.StackTrace));
			};

			// Implement your sending protocol here. You can use any information from System.Exception
			exDlg.OnSendExceptionClick += delegate (object sender, SendExceptionClickEventArgs ar)
			{
				// User clicked on "Send Error Report" button:
				if (ar.SendReport) {
					try {
						string msg = "&body=";
						msg +=
							String.Format(@"
								OS Version: {0}
								\nFramework: {1}
								\nProgram Version: {2}
								\nUserDomainName: {3}
								\nMachineName: {4}
								\nSystem started: {5:0}
								\nCurrentDirectory: {6}
								\nUnhandledException.Message:\n {7}
								\nUnhandledException.StackTrace:\n {8}\n",
									Environment.OSVersion,
									Environment.Version,
									Application.ProductVersion,
									Environment.UserDomainName,
									Environment.MachineName,
									Environment.TickCount / 60000,
									Environment.CurrentDirectory,
									ar.UnhandledException.Message,
									ar.UnhandledException.StackTrace);

						System.Diagnostics.Process.Start("mailto:wlad7777@mail.ru?subject=ProductionMonitor Exception" + msg);
						//MessageBox.Show("mail Send");
					} catch (Exception ex) {
						Debug.WriteLine(ex.ToString());
					}
				}
				// User wants to restart the App:
				if (ar.DialogResult == DialogResult.Retry) {
					//Console.WriteLine("The App will be restarted...");
					System.Diagnostics.Process.Start(System.Windows.Forms.Application.ExecutablePath);
					Application.Exit();
				} else if (ar.DialogResult == DialogResult.Abort)
					Application.Exit();
			};
			#endregion | exception |

			// Check run state
			if (IsAlreadyRunning()) return;

			// Splash screen
			Splash2.ShowSplash(100);

			// Language settings
			Thread.CurrentThread.CurrentCulture = new CultureInfo(Properties.Settings.Default.Culture);
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			Res.Culture = Thread.CurrentThread.CurrentCulture;

			////Splash.CurrentState(Environment.NewLine + "Run the main form");
			Application.Run(new frmMonitor());
		}

		public frmMonitor()
		{
			_monitorSettings = Properties.Settings.Default;
			Application.DoEvents();

			////Splash.CurrentState("Init components");
			InitializeComponent();
			////Splash.CurrentState("Init controls");
			InitControls();
			////Splash.CurrentState("Init InitChartsCounters");
			InitChartsCounters();

			// Настройки под "новый" стиль
			typeof(Control).GetMethod("SetStyle", BindingFlags.NonPublic | BindingFlags.Instance)
			   .Invoke(_tsProgBar.ProgressBar, new object[] { ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true });
			this.FormBorderStyle = FormBorderStyle.None;
			this.DoubleBuffered = true;
			this.SetStyle(ControlStyles.ResizeRedraw, true);
		}

		public static void start_timer()
		{
			_tmActivity.Start();
		}
		public static void stop_timer()
		{
			_tmActivity.Stop();
		}

		private void InitChartsCounters()
		{
			#region | Timeline_counters   |
			// throw new NotImplementedException();
			_ch_ = new TimeLineChart[MaxLines];
			for (int n = 0; n < _ch_.Length; n++)
				_ch_[n] = new TimeLineChart();
			foreach (TimeLineChart c in _ch_) {
				((System.ComponentModel.ISupportInitialize)c).BeginInit();
				c.Transform += new C1.Win.C1Chart.TransformEventHandler(ch0_Transform_Counters);  // pisk
				c.MouseMove += new System.Windows.Forms.MouseEventHandler(ch0_MouseMove_Counter);
				c.MouseDown += new MouseEventHandler(ch0_MouseDown_Counter); // pisk
				c.MouseUp += new System.Windows.Forms.MouseEventHandler(ch0_MouseUp_Counter); // pisk
				c.Click += new System.EventHandler(ch0_Click_Counter);
				// c.DoubleClick    += new System.EventHandler(ch0_DoubleClick);
				c.MouseLeave += new System.EventHandler(ch0_MouseLeave);
				c.Enter += new EventHandler(ch0_Enter_counters);
				c.VisibleChanged += new EventHandler(chartmap_VisibleChangedCounter);

				c.Visible = false;
				c.Dock = System.Windows.Forms.DockStyle.Top;
				////	c.Size = new Size(100, ChMapHei);

				// Rotate = 0
				// Scale = 1
				// Translate = 2
				// Zoom = 3

				c.Interaction.Enabled = true;
				c.Interaction.Appearance = InteractionAppearance.FillSelectionArea; // тонирование зоны масштабирования
																					// c.Interaction.Actions[0].Modifier = Keys.I;
																					//c.Interaction.Actions[0].Axis = AxisFlagEnum.AxisX;

				//  c.Interaction.Actions[2].Modifier = Keys.S;
				//   c.Interaction.Actions[2].Axis = AxisFlagEnum.AxesAll;

				c.Interaction.Actions[3].Modifier = Keys.Alt; // None;
				c.Interaction.Actions[3].Axis = AxisFlagEnum.AxisX;  //AxesAll; //

				c.Interaction.Actions["Scale"].Axis = AxisFlagEnum.AxesXY;// AxesYY2;
																		  //   c.Interaction.Actions["Translate"].Axis = AxisFlagEnum.AxesXY2 ;// AxisFlagEnum.AxesXY;// AxesYY2;
																		  //    c.Interaction.Actions["Rotate"].Axis = AxisFlagEnum.AxesAll;// AxesXY;
																		  // c.Interaction.Actions["Zoom"].Axis = AxisFlagEnum.AxesYY2;

				c.Header.LocationDefault = new Point(0, 0);
				c.Header.Style.Border.Rounding.LeftTop = 3;
				c.Header.Style.Border.Rounding.RightTop = 3;
				c.Header.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Solid;
				c.Header.Style.Border.Color = SystemColors.ControlDark;
				c.Header.Style.BackColor = Color.White;

				c.ChartArea.Inverted = false;
				c.ChartArea.PlotArea.BackColor = Color.White;
				c.ChartArea.Margins.SetMargins(7, 0, 17, 0);
				c.ChartArea.LocationDefault = new Point(7, 19);
				c.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei);

				c.ChartArea.AxisX.Visible = true;
				c.ChartArea.AxisX.TickMinor = TickMarksEnum.None;
				c.ChartArea.AxisX.TickMajor = TickMarksEnum.Inside;
				c.ChartArea.AxisX.ForeColor = Color.Black;
				c.ChartArea.AxisX.AnnoFormat = FormatEnum.DateManual;
				c.ChartArea.AxisX.AnnoFormatString = " H:mm";  //" H:mm:ss";
				c.ChartArea.AxisX.ScrollBar.Appearance = ScrollBarAppearanceEnum.Flat;
				c.ChartArea.AxisX.ScrollBar.Size = 9;
				c.ChartArea.AxisX.ScrollBar.Visible = true;
				c.ChartArea.AxisX.ScrollBar.Buttons = AxisScrollBarButtonFlags.ScaleAndScrollButtons;// NoButtons;
				c.ChartArea.AxisX.ScrollBar.ScrollKeys = ScrollKeyFlags.Cursor;

				// c.ChartArea.AxisY.Visible = false;
				c.ChartArea.AxisY.GridMajor.Visible = true;
				c.ChartArea.AxisY.GridMajor.Color = Color.Gray;
				c.ChartArea.AxisY.ForeColor = Color.Black;
				c.ChartArea.AxisY.TickMinor = TickMarksEnum.None;
				c.ChartArea.AxisY.TickMajor = TickMarksEnum.Inside;
				//c.ChartArea.AxisY.TickLabels = TickLabelsEnum.None;
				c.ChartArea.AxisY.OnTop = false;
				c.ChartArea.AxisY.AnnoFormat = FormatEnum.NumericManual;

				c.ChartArea.AxisY2.Visible = false;

				c.ChartGroups[0].ShowOutline = false;
				c.ChartGroups[1].ShowOutline = false;
				c.ChartGroups[0].ChartType = Chart2DTypeEnum.XYPlot;
				//c.ChartGroups[0].se
				//c.ChartGroups[1].ChartType = Chart2DTypeEnum.XYPlot;
				c.ChartGroups[1].ChartType = Chart2DTypeEnum.Area;

				//c.ChartGroups[2].ShowOutline = false;
				// c.ChartGroups[2].ChartType = Chart2DTypeEnum.XYPlot;

				// c.Style.BackColor = Color.White;
				// c.BackColor = Color.WhiteSmoke;
				// c.Style.Border.BorderStyle = C1.Win.C1Chart.BorderStyleEnum.Raised;
				((ISupportInitialize)c).EndInit();
				pnMaps_counts.Controls.Add(c);
				//pnMaps.Controls.Add(c);

				c.TabStop = true;
				c.ContextMenuStrip = cmMapCounter;
			}
			SetChartMapHeight_counter(true);  //  _monitorSettings.BigMap
											  //pnMaps.Controls.SetChildIndex(pnEdit, 0);
											  //pnMaps_counts.Controls.SetChildIndex(pnEdit, 0);


			#endregion | Timeline_counters   |


		}

		private void SetChartMapHeight_counter(bool IsHight)
		{
			pnMaps_counts.SuspendLayout();
			if (IsHight) {
				foreach (C1Chart chart in _ch_) {
					if (Visible) {
						chart.Height = _chMapHeiBig;
						chart.ChartArea.AxisY.Visible = true;
						chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
					}
				}
			} else {
				foreach (C1Chart chart in _ch_) {
					if (Visible) {
						chart.ChartArea.AxisY.Visible = false;
						chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei); //new Size(-1, chMapAreaHei);
						chart.Height = _chMapHei;
					}
				}
			}
			Debug.WriteLine("SetChartMapHeight");
			pnMaps_counts.ResumeLayout();
		}

		#region | Code settings           |

		const bool use_threading = true;                // sometimes need to set to false for debug mode

		internal const bool use_one_column_for_excel = false;
		public enum TimeStyle
		{
			MMM = 0,
			HHxMM = 1,
			HHxHH = 2
		}
		private static TimeStyle _tmStyle;

		internal const int MaxLines = 50;               // max number of units
		internal const int MaxTimes = 50;               // max number of days or weeks or months
		internal const int MaxMapModeMins = 100800;     //44640;

		#endregion | Code settings           |

		#region | Monitor declare         |

		private PictureBox _pbError;

		private ToolStripMenuItem _mnuFile, _mnuSett, _mnuHelp, _mnuReport, _mnuExcel3, _mnuExcel4;
		private ToolStripMenuItem _mnuExit, _mnuExcel, _mnuCalculate;
		private ToolStripMenuItem _mnuContent, _mnuAbout;
		private ToolStripMenuItem _mnuCopy;

		private TimeLineChart[] _ch_;
		private TimeLineChart _chs;                         // selected chart

		const int _chMapHei = 92;                           // chart control height
		const int _chMapAreaHei = _chMapHei - 22;           // ChartArea height
		const int _chMapHeiBig = 320;                       // chart control height with bottles per minute data
		const int _chMapAreaHeiBig = _chMapHeiBig - 30;     // ChartArea height with bottles per minute data

		private List<MU> _lu_;                              // couners 
		private List<MU> _nodes_ = new List<MU>();          // Нет своих данных, есть только дети
		private Queue<MU> _qu = new Queue<MU>();            // очередь, используется только в процессе загрузки данных
		private Queue<MU> _qu_ = new Queue<MU>();           // очередь, используется только в процессе загрузки данных
		private int L = -1;                                 // Current line in list of selected units

		private C.State _state;                             // Surrent state (operation) of application
															//static internal bool defaultlanguage = true;			// Flag for english interface
		private int _timeSleep = 1;                         // Time constant for checkout
		private Stopwatch _swDb;
		private Stopwatch _swTotal;
		private int _idleCounter;                           // Tme counter for user idle
		private string _loadingText;                        // String for label at bottom of the form

		static internal Color _colControl = SystemColors.Control;
		static internal Color _colHalfControl = Color.FromArgb(100, SystemColors.Control);
		static internal Color _colForeBold = Color.FromArgb(64, 64, 64);

		private Random rnd = new Random(DateTime.Now.Second * DateTime.Now.DayOfYear);

		static internal Font _font;
		static internal Font _fontBold;
		static internal Font _fontBoldArial = new Font("Arial", 9F, FontStyle.Bold);

		static internal Properties.Settings _monitorSettings;

		private BackgroundWorker _dbWorker_Counter;

		private MU _currUnit_;
		private DateTime _dtEmpty = new DateTime(1, 1, 1);

		Stopwatch _sw1 = new Stopwatch();
		public static Mutex Mutex;

		public const int WM_NCLBUTTONDOWN = 0xA1;
		public const int HTCAPTION = 0x2;
		[DllImport("User32.dll")]
		public static extern bool ReleaseCapture();
		[DllImport("User32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

		private System.Windows.Forms.Timer _animationTimer = new System.Windows.Forms.Timer() { Interval = 10 };
		public static Color StyleColor = Color.FromArgb(66, 156, 233);
		public static SolidBrush StyleBrush = new SolidBrush(StyleColor);
		private ToolStripCheckBox _tsHighMap, _tsTips, _tsLimits;

		#endregion | Monitor declare         |

		#region | At start/stop events    |
		private void frmMonitor_Load(object sender, System.EventArgs e)
		{
			Splash2.Fadeout();
			Application.DoEvents();
			Text = Application.ProductName; // Application.CompanyName + " / " + Application.ProductName;

			ToolStripManager.Renderer = new ToolStripProfessionalRenderer(new StatusStripColors());
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			LoadSettings();

			MU.SetTimePeriod(2, DateTime.Now.Date.AddDays(1));
			string winuser = Environment.UserName.ToLower();

#if DEBUG
			if (string.Compare(winuser, "user", true) == 0)
				MU.SetTimePeriod(2, new DateTime(2014, 09, 12));
			if (Environment.MachineName == "XS")
				MU.SetTimePeriod(2, new DateTime(2014, 09, 12));
#endif

			_monitorSettings.Lines.Add(-1);

			// create dropdown control instance for date/line/mode selection
			_sideBar.Init(_monitorSettings.Lines_count);
			_sideBar.ShowWindow(MU.DtStart, MU.DtEnd);
			_sideBar.DataChanged += mnuSelect_DataChanged;

			MU.Config = _sideBar.Config;
			ChangeLang(Thread.CurrentThread.CurrentUICulture.Name);

			if (_sideBar.SelectedUnits_Counters != null)
				_lu_ = _sideBar.SelectedUnits_Counters as List<MU>;
			_sideBar.MouseIsOver += _sideBar_MouseIsOver;

			MU.UseTimecorrection = _monitorSettings.TimeCorrection;
			_tabMain.SelectedTab = tpCounters;

			if (!_monitorSettings.EmptyAtStart)
				LoadStart();

			// restoring last window state
			if (_monitorSettings.WindowState != WindowState)
				WindowState = _monitorSettings.WindowState;

			if (_monitorSettings.AutoUpdate)
				_tmActivity.Start();

#if DEBUG
			Debug.WriteLine("frmMain_Load 2 " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}

		protected override void OnActivated(EventArgs e)
		{
			base.OnActivated(e);
			FormBorderStyle = FormBorderStyle.None;
		}

		public static bool IsAlreadyRunning()
		{
			// Name should be unique
			const string UniqueString = "ProductionMonitorV3";

			// Is there the mutex with this name?
			Mutex = new Mutex(false, UniqueString, out bool createdNew);
			return !createdNew;
		}

		private void InitControls()
		{
			minimizeButton.Click += (s, ev) =>
			{
				FormBorderStyle = FormBorderStyle.Sizable;
				WindowState = FormWindowState.Minimized;
			};
			maximizeButton.Click += (s, ev) =>
			{
				MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
				WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
				maximizeButton.Type = WindowState == FormWindowState.Maximized ?
					System.Windows.Forms.CaptionButton.Restore : System.Windows.Forms.CaptionButton.Maximize;
			};

			closeButton.Click += (s, ev) => Close();
			_pnMainFormButtons.MouseDown += (s, ev) =>
			{
				if (ev.Button == MouseButtons.Left) {
					ReleaseCapture();
					SendMessage(Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
				}
			};
			_sideBar.TabChanged += (s, ev) => _tabMain.SelectedTab = ev.Tab == StripSelect.TabState.Chart ? tpCounters : tpCounters_kpi;

			splitContainer1.SplitterDistance = 300;
			_tabMain.Appearance = TabAppearance.FlatButtons;
			_tabMain.ItemSize = new Size(0, 1);
			_tabMain.SizeMode = TabSizeMode.Fixed;
			var tab = new TabPadding(_tabMain);

			_animationTimer.Tick += animationTimer_Tick;
			_tsProgBar.ProgressBar.Paint += ProgressBar_Paint;

			_ss.Renderer = new StatusStripRenderer();
			_tsHighMap = new ToolStripCheckBox();
			_tsHighMap.CheckBox.ForeColor = Color.White;
			_tsHighMap.CheckBox.CheckedChanged += (s, e) => SetChartMapHeight_counter(_tsHighMap.CheckBox.Checked);
			_ss.Items.Insert(_ss.Items.IndexOfKey(sbpDesc.Name) + 1, _tsHighMap);
			_tsTips = new ToolStripCheckBox();
			_tsTips.CheckBox.ForeColor = Color.White;
			_ss.Items.Insert(_ss.Items.IndexOfKey(sbpDesc.Name) + 2, _tsTips);
			_tsLimits = new ToolStripCheckBox();
			_tsLimits.CheckBox.ForeColor = Color.White;
			_tsLimits.CheckBox.CheckedChanged += btRefresh_Click;
			_ss.Items.Insert(_ss.Items.IndexOfKey(sbpDesc.Name) + 2, _tsLimits);

			_sideBar.ExcelReport += (s, e) =>
			{
				get_rep_counters_result();
				export_counters_result2excel();
				var tb = ds_counters.Tables["Counters_errors"];
				var er = new ExcelReport_Counters_Errors(tb, tb.Columns);
			};

			_tmActivity = new System.Windows.Forms.Timer(this.components);
			_tmActivity.Interval = 1200;
			_tmActivity.Tick += new EventHandler(this.tmActivity_Tick);

			_state = C.State.DataLoading;

			_font = Font;
			_fontBold = new Font(_font, FontStyle.Bold);

			_dbWorker_Counter = new BackgroundWorker { WorkerReportsProgress = true };
			_dbWorker_Counter.DoWork += dbworker_Counter_DoWork;
			_dbWorker_Counter.RunWorkerCompleted += dbworker_Counter_RunWorkerCompleted;

			tsbMainNext.Click += btNext_Click;
			tsbMainPrev.Click += btPrev_Click;
			tsbMainRefresh.Click += btRefresh_Click;

		}

		private void ProgressBar_Paint(object sender, PaintEventArgs e)
		{
			var pb = sender as ProgressBar;
			var percent = (float)pb.Value / pb.Maximum;
			var valueLength = percent * pb.Width;
			e.Graphics.FillRectangle(Brushes.White, e.Graphics.ClipBounds);
			var chunkRect = new RectangleF(2, 2, valueLength, pb.Height - 4);
			e.Graphics.FillRectangle(StyleBrush, chunkRect);
			//ControlPaint.DrawBorder(e.Graphics, pb.ClientRectangle, Color.White, ButtonBorderStyle.Solid);
		}

		private void InitMenu()
		{
			this.SuspendLayout();
			MainMenu.Items.Clear();
			EventHandler eh = new EventHandler(mnuClick);

			//////////////////// Main menu population /////////////////////////////////////
			_mnuFile = new ToolStripMenuItem(Res.mnuFile);
			_mnuSett = new ToolStripMenuItem(Res.mnuSettings);
			_mnuHelp = new ToolStripMenuItem(Res.mnuHelp);
			_mnuReport = new ToolStripMenuItem(Res.mnuReport);

			MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _mnuFile, _mnuSett, _mnuHelp });

			//////////////////// Menu 'File' population ///////////////////////////////////
			//mnuExcel1 = new ToolStripMenuItem(Res.mnuExcel1, null, eh, "mnuExcel1");
			_mnuExcel = new ToolStripMenuItem(Res.mnuExcel2, null, eh, "mnuExcel2");
			_mnuExcel3 = new ToolStripMenuItem(Res.mnuExcel3, null, eh, "mnuExcel3");
			_mnuExcel4 = new ToolStripMenuItem(Res.mnuExcel4, null, eh, "mnuExcel4");


			_mnuCalculate = new ToolStripMenuItem(Res.mnuCalculate, null, eh, "mnuCalculate");
			_mnuExit = new ToolStripMenuItem(Res.mnuExit, Res.exit, eh, "mnuExit");
			//mnuCashRefr = new ToolStripMenuItem(Res.mnuCash, null, eh, "mnuCashRefr");
			//mnuExcel1.ShortcutKeys = Keys.Alt | Keys.E;
			_mnuExcel.ShortcutKeys = Keys.Alt | Keys.E; //| Keys.A;
														//mnuCashRefr.ShortcutKeys = Keys.Alt | Keys.R;
			_mnuExit.ShortcutKeys = Keys.Alt | Keys.F4;
			_mnuCalculate.Visible = false;
			//mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {mnuExcel1, mnuExcel2, mnuCalculate, new ToolStripSeparator(), mnuExit });
			_mnuFile.DropDownItems.AddRange(new ToolStripItem[] { _mnuReport, _mnuCalculate, new ToolStripSeparator(), _mnuExit });
			_mnuReport.DropDownItems.AddRange(new ToolStripItem[] { _mnuExcel, _mnuExcel3, _mnuExcel4 });

			/////////////////// Menu 'Help' population ////////////////////////////////////
			_mnuContent = new ToolStripMenuItem(Res.mnuContent, null, eh, "mnuContent");
			_mnuAbout = new ToolStripMenuItem(Res.mnuAbout, null, eh, "mnuAbout");
			_mnuContent.ShortcutKeys = Keys.F1;
			//mnuAbout.ShortcutKeys = Keys.F2;
			_mnuHelp.DropDownItems.AddRange(new ToolStripItem[] { _mnuAbout });
			////////////////// Context menu for 'Detailed' grid ///////////////////////////
			_mnuCopy = new ToolStripMenuItem(Res.mnuCopy, null, eh, "mnuCopy");

			cmDetailed.Items.Clear();
			this.cmDetailed.Items.AddRange(new ToolStripItem[] { _mnuCopy });

			ToolStripMenuItem mnuCopyLines = new ToolStripMenuItem(Res.mnuCopy, null, eh, "mnuCopyLines");
			ToolStripMenuItem mnuSetLinesCols = new ToolStripMenuItem(Res.mnuGrSetupColumns, null, eh, "mnuSetLinesCols");
			cmLinesGrid.Items.Clear();
			cmLinesGrid.Items.Add(mnuCopyLines);
			cmLinesGrid.Items.Add(mnuSetLinesCols);

			////////////////// Context menu for 'Timeline'
			ToolStripMenuItem mnuApply = new ToolStripMenuItem(Res.mnuMapApplyTimeCorr, null, eh, "mnuApply");
			ToolStripMenuItem mnuChange = new ToolStripMenuItem(Res.mnuMapChangeRunSKU, null, eh, "mnuChange");
			ToolStripMenuItem mnuReset = new ToolStripMenuItem(Res.mnuMapReset, null, eh, "mnuReset");
			ToolStripMenuItem mnuDelProd = new ToolStripMenuItem(Res.mnuMapDelete, null, eh, "mnuDelProd");
			ToolStripMenuItem mnuAutoShift = new ToolStripMenuItem(Res.mnuMapAutoShift, null, eh, "mnuAutoShift");
			ToolStripMenuItem mnuShowMins = new ToolStripMenuItem(Res.mnuShowMins, null, eh, "mnuShowMins");
			cmMap.Items.Clear();
			cmMap.Items.AddRange(new ToolStripItem[] { mnuAutoShift, mnuApply, mnuChange, mnuReset, mnuDelProd, mnuShowMins });
			cmMapCounter.Items.Clear();
			//if (!use_easy_counter ) 
			cmMapCounter.Items.AddRange(new ToolStripItem[] { mnuAutoShift, mnuApply, mnuChange, mnuReset, mnuDelProd, mnuShowMins });


			ToolStripLabel tsl = new ToolStripLabel("");
			tsl.Name = "testlabel";
			tsl.Alignment = ToolStripItemAlignment.Right;
			MainMenu.Items.Add(tsl);
#if !DEBUG
			tsl.Visible = false;
#endif
			InitLang();
			ResumeLayout(false);
			mnuAutoShift.Visible = false;
		}

		private void LoadSettings()
		{
			if (Properties.Settings.Default.CallUpgrade) {
				_monitorSettings.Upgrade();
				_monitorSettings.CallUpgrade = false;
			}

			//Associate settings property event handlers.
			_monitorSettings.SettingsSaving += new System.Configuration.SettingsSavingEventHandler(monitorsettings_SettingsSaving);

			Location = _monitorSettings.Location;
			Size = new Size(1280, 720); //_monitorSettings.Size;

			_tsHighMap.CheckBox.Checked = _monitorSettings.BigMap_counter;
			_tsTips.CheckBox.Checked = _monitorSettings.Tips_counter;
			_tsLimits.CheckBox.Checked = _monitorSettings.ShowOverLimits;
			////set_tsbBigChartCounter(tsbShowLimitsCounter, Res.limit1, Res.limit2);  

			MU.UseTimecorrection = _monitorSettings.TimeCorrection;

			if (_monitorSettings.Lines == null)
				_monitorSettings.Lines = new ArrayList();
			if (_monitorSettings.Lines_count == null)
				_monitorSettings.Lines_count = new ArrayList();

			TmStyle = (TimeStyle)_monitorSettings.Mins;
		}

		private void monitorsettings_SettingsSaving(object sender, CancelEventArgs e)
		{
			_monitorSettings.WindowState = WindowState;
			if (WindowState == FormWindowState.Normal) {
				_monitorSettings.Size = Size;
				_monitorSettings.Location = Location;
			}
			_monitorSettings.UnitsMode = 1;
			_monitorSettings.Mins = (int)TmStyle;
			_monitorSettings.Tips_counter = _tsTips.CheckBox.Checked;
			_monitorSettings.BigMap_counter = _tsHighMap.CheckBox.Checked;
			_monitorSettings.ShowOverLimits = _tsLimits.CheckBox.Checked;

			if (_lu_ != null) {
				_monitorSettings.Lines_count.Clear();
				foreach (MU l in _lu_)
					_monitorSettings.Lines_count.Add(l.ID);
			}
		}

		private void frmMonitor_Closing(object sender, CancelEventArgs e)
		{
			_state = C.State.Closing;
		}

		private void frmMonitor_Closed(object sender, EventArgs e)
		{
			_monitorSettings.Save();
		}

		#endregion | At start/stop events    |

		private void mnuSelect_DataChanged(object sender, StripSelect.DataChangesArgs e)
		{
			MU.SetTimePeriod(e.DateStart, e.DateEnd);

			// если дата не с 00:00 то возможно используем коррекцию времени
			MU.UseTimecorrection = MU.DtStart.Date != MU.DtStart ? false : _monitorSettings.TimeCorrection;
			for (int c = 0; c < _ch_.Length; c++)           //	hide 'Timeline' charts
				_ch_[c].Visible = false;
			LoadStart();
		}

		private void LoadStart()
		{
			if (_lu_ == null || _lu_.Count == 0)
				return;

			_swTotal = Stopwatch.StartNew();
			_swDb = Stopwatch.StartNew();
			_state = C.State.DataLoading;
			L = -1;

			// Если до этого было состояние редактирования то сбрасываем
			button2.Enabled = false;
			_mnuExcel3.Enabled = false;

			#region | controls settings |

			MU.UseDefaultLanguage = Thread.CurrentThread.CurrentCulture.Name == "en-GB";
			//tssbMainDateLineSelector.Text = String.Format("{0:g}   -   {1:g}", MU.DtStart, MU.DtEnd.AddMinutes(-1));

			sbpLine.Text = "";
			tsMain.Enabled = false;
			_loadingText = string.Empty;
			_tmLoading.Start();

			#endregion | controls settings |

			MU.Total.Clear();
			MU.ResetTopUnitLevel();
			_qu.Clear();

			// переменные для трассировки при последовательной загрузке данных
			_currUnit_ = null;

			_qu_.Clear();
			_nodes_.Clear();
			int n = 0;
			foreach (var unit in _lu_) {
				unit.Clear();
				var dr = MU.Total.NewRow();
				MU.Total.Rows.Add(dr);
				dr["Line"] = unit.GetLongUnitName(false);
				dr["ID"] = n;
				unit.L = n;
				n++;
				MU.SetTopUnitLevel(unit);
				if (unit.Kind == UnitKind.Line)
					_qu_.Enqueue(unit);
				else
					_nodes_.Add(unit);
			}

			if (!(_lu_ == null || _lu_.Count < 1)) {
				if (!use_threading) {
					// in debug mode request to db in the same thread
					_qu_.Peek().LoadData_Counters();
					LoadStep_Counters();
				} else {
					// run the database query in another thread
					//  _dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
					//  if (_qu.Count == 0)
					_tm_counter.Enabled = true;
				}
			}
		}

		private void dbworker_Counter_DoWork(object sender, DoWorkEventArgs e)
		{
			MU unit = e.Argument as MU;
			_swDb.Stop();
			unit.LoadData_Counters();
			_swDb.Start();
			e.Result = unit;
		}

		private void dbworker_Counter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			LoadStep_Counters();
		}

		private void LoadStep_Counters()
		{
			while (_dbWorker_Counter.IsBusy)
				Application.DoEvents();

			// при первом проходе надо пропустить
			if (_currUnit_ != null)
				Completed_Counter(_currUnit_);

			_currUnit_ = _qu_.Dequeue();

			if (_qu_.Count > 0) {
				if (use_threading) {
					_dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
				} else {
					_qu_.Peek().LoadData_Counters();
					LoadStep_Counters();
				}
			} else {
				// на последнем проходе делает то, что на первом пропустили
				while (_dbWorker_Counter.IsBusy)
					Application.DoEvents();
				Completed_Counter(_currUnit_);

				Finish_counters();
			}
		}

		private void Completed_Counter(MU unit)
		{
			try {
				_loadingText = string.Format("{0},    {1}", unit.GetLongUnitName(false), Res.splRender);
				BuildChartMapCounter(unit, true);
			} catch (Exception ex) {
				unit.Error = ex.Message;
				MU.Exception = ex;
			} finally {
				_ch_[unit.L].Refresh();
				Application.DoEvents();
			}
			unit.IsBinded = true;
		}

		private void Finish_counters()
		{
			_nodes_.Sort(new MU.LevelComparer());
			_tsProgBar.Visible = false;
			_tmLoading.Stop();
			_ss.Refresh();

			create_grid_counters_kpi();
			_tabMain.Visible = true;
			// разрешаем заблокированные кнопки
			tsMain.Enabled = true;
			button2.Enabled = true;
			_mnuExcel3.Enabled = true;
			_state = C.State.Idle;

			_swTotal.Stop();
#if DEBUG
			MainMenu.Items["testlabel"].Text = string.Format("Time spent:  {0:0.00} + {1:0.00}",
							(_swTotal.ElapsedMilliseconds - _swDb.ElapsedMilliseconds) / 1000F, _swTotal.ElapsedMilliseconds / 1000F);
#endif
		}

		private bool IsEmpty_Counter()
		{
			return L < 0 || _lu_[L].IsEmpty_Counter();
		}


		#region | Menus and buttons       |


		private void goto_excel_kpi_counters()
		{

			if (_state != C.State.Idle)
				return;
			List<string> cols = new List<string>();
			foreach (Column col in c1FlexGrid1.Cols)
				if (col.Visible)
					cols.Add(col.Name);
			ExcelReport er = new ExcelReport(_lu_, cols, "");
		}

		private void mnuClick(object sender, EventArgs e)
		{
			var mi = (ToolStripMenuItem)sender;
			switch (mi.Name) {
				case "mnuExcel2":
					goto_excel_kpi_counters();
					break;
				case "mnuExcel3":
					get_rep_counters_result();
					export_counters_result2excel();
					break;
				case "mnuExcel4":
					var er = new ExcelReport_Counters_Errors(ds_counters.Tables["Counters_errors"], ds_counters.Tables["Counters_errors"].Columns);
					break;
				case "mnuContent":
					Help.ShowHelp(this, "ProductionMonitor.chm");
					break;
				case "mnuCashRefr":
					MU.CashNeedToUpdate = true;
					LoadStart();
					break;
				case "mnuExit":
					Close();
					break;
				case "mnuReset":
					ResetChartMap();
					ch0_ResetScale(_chs, true);
					break;
				case "mnuMasterData":
					var masterdata = new frmMaster(MU.Config);
					masterdata.ShowDialog();
					if (masterdata.NeedToUpdateLists) {
						MU.ListsCash.Clear();
						foreach (MU mu in MU.Config.MonitorUnits.Values)
							mu.CashValidDate = _dtEmpty;
						pnLines.Visible = false;
						_tabMain.Visible = false;
						MU.Total.Clear();
					}
					break;
			}
		}

		private void btRefresh_Click(object sender, EventArgs e)
		{
			LoadStart();
		}

		private void btPrev_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(MU.DtStart);
			LoadStart();
		}

		private void btNext_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(MU.DtEnd.AddMinutes(MU.Mins));
			LoadStart();
		}

		private void btNow_Click(object sender, EventArgs e)
		{
			MU.SetTimePeriod(1, DateTime.Now.Date.AddDays(1));
			LoadStart();
		}

		private void txtError_DoubleClick(object sender, EventArgs e)
		{
			if (MU.Exception != null)
				MessageBox.Show(MU.Exception.ToString());
		}

		private void cmMap_MouseLeave(object sender, EventArgs e)
		{
			//cmMap.Hide();
		}

		#endregion | Menus and buttons       |

		#region | CultureInfo settings    |

		private void InitLang()
		{
			_tsLang.DropDownItems.Clear();
			_tsLang.BackColor = StyleColor;
			_tsLang.ForeColor = Color.White;
			// Этот язык добавляем ручками т.к. он по умолчанию
			AddLang("en-GB");

			// Все остальные языки определяем по имеющимся названиям папок с локализациями
			foreach (string str in Directory.GetDirectories(Application.StartupPath)) {
				var subflder = str.Remove(0, Directory.GetCurrentDirectory().Length + 1);
				try { AddLang(subflder); } catch { }
			}

			//AddLang("ru-RU");
			//AddLang("de-DE");
			//AddLang("hr-HR");
			//AddLang("bg-BG");
			//AddLang("cs-CZ");
			//AddLang("hu-HU");
			//AddLang("ro-RO");
			//AddLang("ko-KR");
			SetLangIcon();
		}

		private void AddLang(string strLang)
		{
			var ci = CultureInfo.GetCultureInfo(strLang);
			var tsmi = new ToolStripMenuItem(ci.Parent.TwoLetterISOLanguageName.ToUpper())
			{ Tag = strLang, ForeColor = Color.White };

			tsmi.Click += new EventHandler(mnuLang_Click);
			_tsLang.DropDownItems.Add(tsmi);
			if (Thread.CurrentThread.CurrentUICulture.ToString() == strLang)
				tsmi.Checked = true;
		}

		private void SetLangIcon()
		{
			foreach (ToolStripMenuItem m in _tsLang.DropDownItems)
				if (m.Checked)
					if (_tsLang.DisplayStyle == ToolStripItemDisplayStyle.Text)
						_tsLang.Text = m.Text;
		}

		private void mnuLang_Click(object sender, EventArgs e)
		{
			var mnu = (ToolStripMenuItem)sender;
			foreach (ToolStripMenuItem m in _tsLang.DropDownItems)
				mnu.Checked = mnu == m;
			ChangeLang(mnu.Tag.ToString());
		}

		private void ChangeLang(string l)
		{
			var ci = CultureInfo.GetCultureInfo(l);
			Thread.CurrentThread.CurrentCulture = ci;
			Thread.CurrentThread.CurrentUICulture = ci;
			MU.UseDefaultLanguage = ci.Name == "en-GB";
			_monitorSettings.Culture = ci.ToString();
			Res.Culture = ci;
			SetLangIcon();

			tsbMainRefresh.ToolTipText = Res.btRefreshTT;
			tsbMainPrev.ToolTipText = Res.btPrevDayTT;
			tsbMainNext.ToolTipText = Res.btNextDayTT;
			tsbMainNow.ToolTipText = Res.btNowTT;

			InitMenu();

			tpCounters.Text = Res.tpCounters;

			_tsHighMap.Text = Res.chbShowBigMap;
			_tsTips.Text = Res.chShowTips;
			_tsLimits.Text = Res.tsbShowLimitsCounter;

			// in dropdown selector
			_sideBar._rbDays.Text = Res.strDays;
			_sideBar._rbWeeks.Text = Res.strWeeks;
			_sideBar._rbMonths.Text = Res.strMonths;
			_sideBar._rbTime.Text = Res.strAnyTime;

			if (MU.Kpi != null)
				MU.Kpi.ChangeLanguage(MU.UseDefaultLanguage);

			if (_state != C.State.Idle)
				return;
		}


		#endregion | CultureInfo settings    |


		#region | Utils                   |

		private void SetCursWait()
		{
			this.Cursor = Cursors.WaitCursor;
			Application.DoEvents();
		}

		private void SetCursDefault()
		{
			this.Cursor = Cursors.Default;
			Application.DoEvents();
		}

		private void CopyDatatableToClipboard(C1FlexGrid gr)
		{
#if DEBUG
			Stopwatch sw1 = Stopwatch.StartNew();
#endif
			SetCursWait();
			if (gr == null) return;

			StringBuilder sb = new StringBuilder(1000000);
			string val = "";

			string colname;
			float cellValue;
			for (int r = 0; r < gr.Rows.Count; r++) {
				for (int c = 0; c < gr.Cols.Count; c++) {
					colname = gr.Cols[c].Name;
					if (!gr.Cols[c].Visible)
						continue;
					val = "";
					if (r < gr.Rows.Fixed) {
						// Определяем заголовки колонок
						if (gr.Cols[c].DataMap != null && gr[r, c] != null && gr.Cols[c].DataMap.Contains(gr[r, c]))
							val = gr.Cols[c].DataMap[gr[r, c]].ToString();
					} else if (gr[r, c] == null) {
						// ветка нужна! но делать ничего не надо val и так пустой
					} else if (MU.Kpi.Measure.ContainsKey(colname)) {
						// По стандартным именам колонок определяем размерности и представление
						switch (MU.Kpi.Measure[colname]) {
							case 1: val = Utils.GetTimeSpanString(gr.GetData(r, c)); break; //Utils.GetTimeSpanString(o)
							case 2:
							case 4: val = Utils.GetFloatString(C.GetFloat(gr[r, c])); break;
							case 3: val = C.GetInt(gr[r, c]).ToString(); break;
							default:
								if (gr.Cols[c].DataMap != null && gr.Cols[c].DataMap.Contains(gr[r, c]))
									val = gr.Cols[c].DataMap[gr[r, c]].ToString();
								break;
						}
					}
					if (val == "")
						val = gr[r, c] == null ? "" : gr[r, c].ToString();

					sb.Append(val);
					if (c < gr.Cols.Count - 1) sb.Append("\t");
				}
				if (r < gr.Rows.Count - 1) sb.Append("\n");
			}

			Clipboard.SetText(sb.ToString(), TextDataFormat.UnicodeText);   // Попытка три

			SetCursDefault();
#if DEBUG
			Debug.WriteLine("CopyDatatableToClipboard: " + string.Format("{0:0.000}", sw1.ElapsedMilliseconds / 1000F));
#endif
		}


		#endregion | Utils                   |


		private void tmActivity_Tick(object sender, EventArgs e)
		{
			if (MU.DtStart == DateTime.Now.Date && _idleCounter++ >= 1 && _state == C.State.Idle)  // 5
				LoadStart();
		}

		private void tmLoading_Tick(object sender, EventArgs e)
		{
			if (this._ss.Width > 0) {
				try {
					using (var g = _ss.CreateGraphics()) {
						float dbwait = (_swTotal.ElapsedMilliseconds - _swDb.ElapsedMilliseconds) / 1000F;
						// Create string and measure width for graphics
						MainMenu.Items["testlabel"].Text = string.Format("Time spent:  {0:0.00} + {1:0.00}", dbwait, _swTotal.ElapsedMilliseconds / 1000F);

						string loadingstr = MU.LoadingText == "" ? _loadingText : MU.LoadingText;
						string str = string.Format("  {0:0.0} + {1,-8:0.0}{2}", dbwait, _swTotal.ElapsedMilliseconds / 1000F, loadingstr);
						int strwidth = Convert.ToInt32(g.MeasureString(str, _ss.Font).Width) + 10;

						// Create a StringFormat object to align text in the panel.
						var textFormat = new StringFormat
						{
							LineAlignment = StringAlignment.Center,
							Alignment = StringAlignment.Near
						};

						// Fill background for string and then draw string
						var r = new Rectangle(_tsProgBar.Width + 10, 0, strwidth, _ss.Height);
						g.FillRectangle(StyleBrush, r);
						g.DrawString(str, _ss.Font, Brushes.White, r, textFormat);

						_tsProgBar.Visible = true;
						_tsProgBar.Maximum = 100; // MU.Config.Units.Count;
						if (_tsProgBar.Maximum <= _tsProgBar.Value)
							_tsProgBar.Value = 0;
						_tsProgBar.Value += 1; //MU.marks.Count;// 
					}
				} catch { }
			}
		}

		private void frmMonitor_KeyDown(object sender, KeyEventArgs e)
		{
			_idleCounter = 0;

			if ((_state & C.State.Idle) == 0)
				return;

			if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.Alt) {        // FormWindowState
				if (WindowState == FormWindowState.Maximized)
					WindowState = FormWindowState.Normal;
				else if (WindowState == FormWindowState.Normal)
					WindowState = FormWindowState.Maximized;
			} else if (e.KeyCode == Keys.PageDown) {                                        // move to next day
				btNext_Click(tsbMainNext, null);
			} else if (e.KeyCode == Keys.PageUp) {                                          // move to previous day
				btPrev_Click(tsbMainPrev, null);
			} else if (e.KeyCode == Keys.F3) {                                              // move to today
				btNow_Click(tsbMainNow, null);
			} else if (e.KeyCode == Keys.F5) {                                              // refresh data
				btRefresh_Click(tsbMainRefresh, null);
			} else if (e.KeyCode == Keys.X && e.Modifiers == Keys.Alt) {            // reset chart
				ResetChartMap();
				if (_chs != null)
					_chs.ChartArea.AxisX.ScrollBar.Scale = 1;
			} else if (e.KeyCode == Keys.W && e.Modifiers == Keys.Control) {        // window size to 1024x740
				this.WindowState = FormWindowState.Normal; this.Size = new Size(1024, 720);
			} else if (e.KeyCode == Keys.C && e.Modifiers == Keys.Control) {
				ProcessCopy();                                                                      //Копирование в клипборд 
			}
			Debug.WriteLine(String.Format("e.KeyCode: {0}   e.Modifiers: {1}", e.KeyCode, e.Modifiers));
		}

		private void ProcessCopy()
		{
			if (ActiveControl is C1FlexGrid) {
				CopyDatatableToClipboard(ActiveControl as C1FlexGrid);
			} else if (ActiveControl is C1Chart) {
				C1Chart c1ch = ActiveControl as C1Chart;
				c1ch.SaveImage(ImageFormat.Png, c1ch.Size);
			}
		}

		public static TimeStyle TmStyle
		{
			get { return _tmStyle; }
			set { _tmStyle = value; }
		}

		private void cmMapCounter_Opening(object sender, CancelEventArgs e)
		{
			// прячем тултип
			Tt.RemoveAll();

			//todone: сделать пункт меню "Удалить" доступным только для админа и локального админа
			cmMapCounter.Items["mnuApply"].Visible = false;
			cmMapCounter.Items["mnuDelProd"].Visible = false;
			cmMapCounter.Items["mnuChange"].Visible = false;
			cmMapCounter.Items["mnuAutoShift"].Visible = true;
			cmMapCounter.Items["mnuAutoShift"].Visible = false;

			cmMapCounter.Items["mnuShowMins"].Visible = false;

		}
		private void _tm_counter_Tick(object sender, EventArgs e)
		{
			if (_qu.Count == 0) {
				_dbWorker_Counter.RunWorkerAsync(_qu_.Peek());
				_tm_counter.Enabled = false;
			}
		}

		private void create_grid_counters_kpi()
		{
			try {
				ds_counters.Tables["Counters_kpi"].Rows.Clear();
				if (ds_counters.Tables.Contains("Counters_errors")) 
					ds_counters.Tables["Counters_errors"].Rows.Clear();

				foreach (var unit in _lu_) {
					if (unit.Connection == "" || unit.Error != "") 
						continue;

					DataRow[] DR = unit.DataSet.Tables["Counters_kpi"].Select();
					foreach (DataRow dr in DR) {
						DataRow row1 = ds_counters.Tables["Counters_kpi"].NewRow();
						row1["Name"] = (string)dr["name"];
						row1["id"] = (int)dr["id"];
						try {
							row1["Max_"] = (double)dr["max_"];
							row1["Min_"] = (double)dr["min_"];
							row1["Avg_"] = (double)dr["avg_"];
							c1FlexGrid1.Cols["error"].Visible = false;
						} catch (Exception) {
							c1FlexGrid1.Cols["error"].Visible = true;
							row1["error"] = (string)Res.mapNotAvalb;
							// throw;
						}

						ds_counters.Tables["Counters_kpi"].Rows.Add(row1);
					}

					if (unit.DataSet.Tables.Contains("Counters_Deviation") && ds_counters.Tables.Contains("Counters_errors")) {

						DataRow[] DR1 = unit.DataSet.Tables["Counters_Deviation"].Select();
						foreach (DataRow dr in DR1) {
							DataRow row1 = ds_counters.Tables["Counters_errors"].NewRow();
							row1["Name"] = (string)dr["name"];
							row1["id_deviation"] = (int)dr["id_deviation"];
							try {
								row1["deviation_name"] = (string)dr["deviation_name"];
								row1["dt_from"] = (DateTime)dr["dt_from"];
								// c1FlexGrid1.Cols["error"].Visible = false;
							} catch (Exception) {
								// c1FlexGrid1.Cols["error"].Visible = true;
								// row1["error"] = (string)Res.mapNotAvalb;
								// throw;
							}
							ds_counters.Tables["Counters_errors"].Rows.Add(row1);
						}
					}
				}
			} catch (Exception) {
				// throw;
			}
		}

		private void get_rep_counters_result()
		{
			label1.Text = String.Format("{0:g}   -   {1:g}", MU.DtStart, MU.DtEnd.AddMinutes(-1));
			if (_state == C.State.Idle) {
				_state = C.State.Reporting;
				int lv_row_count = c1FlexGrid2.Cols.Count;

				try {
					ds_counters.Tables["Counters_rep2"].Rows.Clear();

					c1FlexGrid2.DataSource = null;  // 
					c1FlexGrid2.DataMember = "";
					c1FlexGrid2.Cols.RemoveRange(0, lv_row_count);

					button3.Enabled = false;
					button2.Enabled = false;
					_mnuExcel3.Enabled = false;

					DataColumnCollection lv_dcc = this.ds_counters.Tables[1].Columns;//.Remove(;
					string[] array1 = new string[this.ds_counters.Tables[1].Columns.Count];
					int i;
					i = 0;
					foreach (DataColumn unit in lv_dcc) {
						array1[i] = unit.ColumnName;
						i++;
					}
					foreach (string item in array1)
						if (!(item == "Date" || item == "Time" || item == "dt" || item == "Counters" || item == "Brand" || item == "Discharg"))
							ds_counters.Tables[1].Columns.Remove(item);

					string name_s;
					string name_s1;
					string name_s2;
					object[] findTheseVals = new object[2];

					int lv_count = _lu_.Count;
					int lv_step = _tsProgBar.Maximum / lv_count;
					_tsProgBar.Value = 0;
					_tsProgBar.Visible = true;
					foreach (var unit in _lu_) {
						if (unit.Connection == "" || unit.Error != "")
							continue;

						var chart = _ch_[unit.L];
						name_s = chart.Header.Text;
						if (name_s == "")
							continue;

						name_s1 = unit.GetLongUnitName_counters2(true, " ", unit.Parent_Counters);
						name_s2 = unit.GetLongUnitName_counters2(false, " ", unit.Parent_Counters);
						name_s1.Replace(name_s2, "");

						if (!this.ds_counters.Tables[1].Columns.Contains(name_s))
							this.ds_counters.Tables[1].Columns.Add(name_s);

						DataRow[] DR1 = unit.DataSet.Tables["Counters_brands"].Select();

						foreach (DataRow dr in DR1) {
							bool lv_bool1;
							findTheseVals[0] = (DateTime)dr["dt_from"];
							findTheseVals[1] = name_s1;

							DataRow DR_sel1 = ds_counters.Tables["Counters_rep2"].Rows.Find(findTheseVals);
							DataRow row1;
							if (DR_sel1 == null) {
								row1 = ds_counters.Tables["Counters_rep2"].NewRow();
								lv_bool1 = true;
							} else {
								row1 = DR_sel1;
								lv_bool1 = false;
							}

							row1["Dt"] = ((DateTime)dr["dt_from"]);
							row1["Date"] = ((DateTime)dr["dt_from"]).ToLongDateString();
							row1["Time"] = ((DateTime)dr["dt_from"]).ToLongTimeString();
							row1["Brand"] = (string)dr["brand_name"];
							row1["Counters"] = name_s1;
							row1["Discharg"] = (string)dr["discharg_name"];
							if (lv_bool1)
								ds_counters.Tables["Counters_rep2"].Rows.Add(row1);
						}

						DataRow[] DR = unit.DataSet.Tables["Counters_value"].Select();
						foreach (DataRow dr in DR) {
							string ss = string.Format(" dt = #{0:M/d/yy H:m}#", (DateTime)dr["dt"]);
							bool lv_bool;

							findTheseVals[0] = (DateTime)dr["dt"];
							findTheseVals[1] = name_s1;

							DataRow DR_sel = ds_counters.Tables["Counters_rep2"].Rows.Find(findTheseVals);
							DataRow row1;
							if (DR_sel == null) {
								row1 = ds_counters.Tables["Counters_rep2"].NewRow();
								lv_bool = true;
							} else {
								row1 = DR_sel;
								lv_bool = false;
							}

							try {
								row1["Dt"] = ((DateTime)dr["dt"]);
								row1["Date"] = ((DateTime)dr["dt"]).ToLongDateString();
								row1["Time"] = ((DateTime)dr["dt"]).ToLongTimeString();
								row1["Counters"] = name_s1; 

								double.TryParse(((double)dr["value"]).ToString("F3"), out double lv_doub_pars);
								row1[name_s] = lv_doub_pars; 
							} catch (Exception) {
								c1FlexGrid2.Cols["error"].Visible = true;
								row1["error"] = (string)Res.mapNotAvalb;
								//  throw;
							}

							if (lv_bool)
								ds_counters.Tables["Counters_rep2"].Rows.Add(row1);
						}
						if (_tsProgBar.Value + lv_step <= _tsProgBar.Maximum) {
							_tsProgBar.Value = _tsProgBar.Value + lv_step;

						} else _tsProgBar.Value = 0;
					}


				} finally {
					button2.Enabled = true;
					_mnuExcel3.Enabled = true;
					_state = C.State.Idle;
					// throw;
				}

				_tsProgBar.Visible = false;
				if (ds_counters.Tables["Counters_rep2"].Rows.Count > 0) 
					button3.Enabled = true;

				DataRow[] DR_sort = ds_counters.Tables["Counters_rep2"].Select("", "Counters ,Date ASC  ,Time ASC");
				string lv_brand_str;
				lv_brand_str = "";

				string lv_discharg_str;
				lv_discharg_str = "";
				foreach (DataRow dr in DR_sort) {

					if (!string.IsNullOrEmpty(dr["Brand"].ToString())) {
						lv_brand_str = (string)dr["Brand"];
					} else
						dr["Brand"] = lv_brand_str;

					if (!string.IsNullOrEmpty(dr["Discharg"].ToString()))
						lv_discharg_str = (string)dr["Discharg"];
					else
						dr["discharg"] = lv_discharg_str;
				}

				c1FlexGrid2.DataSource = this.ds_counters;
				c1FlexGrid2.DataMember = "Counters_rep2";
				var order = SortFlags.UseColSort;
				c1FlexGrid2.Cols[1].Sort = SortFlags.Ascending;
				c1FlexGrid2.Cols[2].Sort = SortFlags.Ascending;
				c1FlexGrid2.Sort(order, 1, 2);
				c1FlexGrid2.Cols.Remove("dt");
			}
		}

		private void export_counters_result2excel()
		{
			ds_counters.Tables["Counters_rep2"].DefaultView.Sort = "Date ASC  ,Time ASC";
			ds_counters.Tables["Counters_rep2"].AcceptChanges();

			var er = new ExcelReport_Counters(ds_counters.Tables["Counters_rep2"], ds_counters.Tables["Counters_rep2"].Columns);
		}

		#region |     new style    |

		bool _animationToLeft;
		private void SideChecked(object sender, EventArgs e)
		{
			_animationTimer.Enabled = true;
			_animationToLeft = !_btSide.Checked;
			splitContainer1.IsSplitterFixed = !_btSide.Checked;
			_stepSize = (_animationRight - _animationLeft) / 1; // для анимации 3 или 4
		}

		private void _sideBar_MouseIsOver(object sender, StripSelect.MouseIsOverArgs e)
		{
			if (!_btSide.Checked) {
				if (e.MouseState == StripSelect.MouseState.ToOpen && splitContainer1.SplitterDistance == _animationLeft) {
					_animationTimer.Enabled = true;
					_animationToLeft = false;
				} else if (e.MouseState == StripSelect.MouseState.ToHide && splitContainer1.SplitterDistance > 100) {
					_animationTimer.Enabled = true;
					_animationToLeft = true;
				}
			}
		}

		private int _animationRight = 300;  // start position of the panel
		private int _animationLeft = 70;      // end position of the panel
		private int _stepSize = 230;     // pixels to move

		private void animationTimer_Tick(object sender, EventArgs e)
		{
			if (_animationToLeft) {
				if (splitContainer1.SplitterDistance - _stepSize > 0)
					splitContainer1.SplitterDistance -= _stepSize;
				else
					splitContainer1.SplitterDistance = _animationLeft;

				if (splitContainer1.SplitterDistance < _animationLeft)
					splitContainer1.SplitterDistance = _animationLeft;
				if (splitContainer1.SplitterDistance == _animationLeft)
					_animationTimer.Enabled = false;
			} else {
				splitContainer1.SplitterDistance += _stepSize;
				if (splitContainer1.SplitterDistance > _animationRight)
					splitContainer1.SplitterDistance = _animationRight;
				if (splitContainer1.SplitterDistance == _animationRight)
					_animationTimer.Enabled = false;
			}
		}

		protected override void OnPaint(PaintEventArgs e) // you can safely omit this method if you want
		{
			e.Graphics.FillRectangle(StyleBrush, Top);
			e.Graphics.FillRectangle(StyleBrush, Left);
			e.Graphics.FillRectangle(StyleBrush, Bottom);
			e.Graphics.FillRectangle(StyleBrush, Right);
		}

		private const int
			HTLEFT = 10,
			HTRIGHT = 11,
			HTTOP = 12,
			HTTOPLEFT = 13,
			HTTOPRIGHT = 14,
			HTBOTTOM = 15,
			HTBOTTOMLEFT = 16,
			HTBOTTOMRIGHT = 17;

		const int _ = 10; // you can rename this variable if you like

		Rectangle Top { get { return new Rectangle(0, 0, this.ClientSize.Width, _); } }
		Rectangle Left { get { return new Rectangle(0, 0, _, this.ClientSize.Height); } }
		Rectangle Bottom { get { return new Rectangle(0, this.ClientSize.Height - _, this.ClientSize.Width, _); } }
		Rectangle Right { get { return new Rectangle(this.ClientSize.Width - _, 0, _, this.ClientSize.Height); } }

		Rectangle TopLeft { get { return new Rectangle(0, 0, _, _); } }
		Rectangle TopRight { get { return new Rectangle(this.ClientSize.Width - _, 0, _, _); } }
		Rectangle BottomLeft { get { return new Rectangle(0, this.ClientSize.Height - _, _, _); } }
		Rectangle BottomRight { get { return new Rectangle(this.ClientSize.Width - _, this.ClientSize.Height - _, _, _); } }


		protected override void WndProc(ref Message message)
		{
			base.WndProc(ref message);

			if (message.Msg == 0x84) {
				var cursor = this.PointToClient(Cursor.Position);
				if (TopLeft.Contains(cursor)) message.Result = (IntPtr)HTTOPLEFT;
				else if (TopRight.Contains(cursor)) message.Result = (IntPtr)HTTOPRIGHT;
				else if (BottomLeft.Contains(cursor)) message.Result = (IntPtr)HTBOTTOMLEFT;
				else if (BottomRight.Contains(cursor)) message.Result = (IntPtr)HTBOTTOMRIGHT;

				else if (Top.Contains(cursor)) message.Result = (IntPtr)HTTOP;
				else if (Left.Contains(cursor)) message.Result = (IntPtr)HTLEFT;
				else if (Right.Contains(cursor)) message.Result = (IntPtr)HTRIGHT;
				else if (Bottom.Contains(cursor)) message.Result = (IntPtr)HTBOTTOM;
			}
		}

		#endregion
	}
}

