﻿using System.Windows.Forms;

namespace ProductionMonitor
{
	partial class frmMaster
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMaster));
			this.status = new StatusStrip();
			this.slUser = new ToolStripStatusLabel();
			this.slPermission = new ToolStripStatusLabel();
			this.slMain = new ToolStripStatusLabel();
			this.slUnitId = new ToolStripStatusLabel();
			this.slAccess = new ToolStripStatusLabel();
			this.split = new SplitContainer();
			this.tv_units = new TreeView();
			this.tab = new TabControl();
			this.tpDnTm = new TabPage();
			this.tv_kpi = new TreeView();
			this.tpGlobal = new TabPage();
			this.dgvGlobal = new DataGridView();
			this.bnGlobal = new BindingNavigator(this.components);
			this.bnAddNewItem = new ToolStripButton();
			this.bnCountItem = new ToolStripLabel();
			this.bnDeleteItem = new ToolStripButton();
			this.cbGlobal = new ToolStripComboBox();
			this.bnMoveFirstItem = new ToolStripButton();
			this.bnMovePreviousItem = new ToolStripButton();
			this.bnSeparator = new ToolStripSeparator();
			this.bnPositionItem = new ToolStripTextBox();
			this.bnSeparator1 = new ToolStripSeparator();
			this.bnMoveNextItem = new ToolStripButton();
			this.bnMoveLastItem = new ToolStripButton();
			this.bnSeparator2 = new ToolStripSeparator();
			this.btGlobalSave = new ToolStripButton();
			this.btGlobalUndo = new ToolStripButton();
			this.Redo = new ToolStripButton();
			this.tpLocal = new TabPage();
			this.dgvLocal = new DataGridView();
			this.bnLocal = new BindingNavigator(this.components);
			this.bnCountItem1 = new ToolStripLabel();
			this.cbLocal = new ToolStripComboBox();
			this.bnMoveFirstItem1 = new ToolStripButton();
			this.bnMovePreviousItem1 = new ToolStripButton();
			this.bnSeparator3 = new ToolStripSeparator();
			this.bnPositionItem1 = new ToolStripTextBox();
			this.bnSeparator4 = new ToolStripSeparator();
			this.bnMoveNextItem1 = new ToolStripButton();
			this.bnMoveLastItem1 = new ToolStripButton();
			this.bnSeparator5 = new ToolStripSeparator();
			this.btLocalSave = new ToolStripButton();
			this.btLocalUndo = new ToolStripButton();
			this.toolStripButton3 = new ToolStripButton();
			this.btInclude = new ToolStripButton();
			this.btRemove = new ToolStripButton();
			this.btLocalRefresh = new ToolStripButton();
			this.tpLang = new TabPage();
			this.dgvLang = new DataGridView();
			this.bnLang = new BindingNavigator(this.components);
			this.bnCountItem2 = new ToolStripLabel();
			this.cbLang = new ToolStripComboBox();
			this.bnMoveFirstItem2 = new ToolStripButton();
			this.bnMovePreviousItem2 = new ToolStripButton();
			this.bnSeparator6 = new ToolStripSeparator();
			this.bnPositionItem2 = new ToolStripTextBox();
			this.bnSeparator7 = new ToolStripSeparator();
			this.bnMoveNextItem2 = new ToolStripButton();
			this.bnMoveLastItem2 = new ToolStripButton();
			this.bnSeparator8 = new ToolStripSeparator();
			this.btLangSave = new ToolStripButton();
			this.btLangUndo = new ToolStripButton();
			this.toolStripButton6 = new ToolStripButton();
			this.tpTargets = new TabPage();
			this.dgvTargets = new DataGridView();
			this.bnTargets = new BindingNavigator(this.components);
			this.bindingNavigatorAddNewItem = new ToolStripButton();
			this.bindingNavigatorCountItem = new ToolStripLabel();
			this.bindingNavigatorDeleteItem = new ToolStripButton();
			this.toolStripComboBox1 = new ToolStripComboBox();
			this.bindingNavigatorMoveFirstItem = new ToolStripButton();
			this.bindingNavigatorMovePreviousItem = new ToolStripButton();
			this.bindingNavigatorSeparator = new ToolStripSeparator();
			this.bindingNavigatorPositionItem = new ToolStripTextBox();
			this.bindingNavigatorSeparator1 = new ToolStripSeparator();
			this.bindingNavigatorMoveNextItem = new ToolStripButton();
			this.bindingNavigatorMoveLastItem = new ToolStripButton();
			this.bindingNavigatorSeparator2 = new ToolStripSeparator();
			this.btTargetsSave = new ToolStripButton();
			this.btTargetsUndo = new ToolStripButton();
			this.imageListDrag = new ImageList(this.components);
			this.menuStrip1 = new MenuStrip();
			this.toolStripMenuItem1 = new ToolStripMenuItem();
			this.mnuRefresh = new ToolStripMenuItem();
			this.toolStripMenuItem3 = new ToolStripSeparator();
			this.mnuToExcel = new ToolStripMenuItem();
			this.mnuFromExcel = new ToolStripMenuItem();
			this.toolStripMenuItem2 = new ToolStripSeparator();
			this.mnuExit = new ToolStripMenuItem();
			this.status.SuspendLayout();
			this.split.Panel1.SuspendLayout();
			this.split.Panel2.SuspendLayout();
			this.split.SuspendLayout();
			this.tab.SuspendLayout();
			this.tpDnTm.SuspendLayout();
			this.tpGlobal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvGlobal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bnGlobal)).BeginInit();
			this.bnGlobal.SuspendLayout();
			this.tpLocal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLocal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bnLocal)).BeginInit();
			this.bnLocal.SuspendLayout();
			this.tpLang.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLang)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bnLang)).BeginInit();
			this.bnLang.SuspendLayout();
			this.tpTargets.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTargets)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bnTargets)).BeginInit();
			this.bnTargets.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// status
			// 
			this.status.Items.AddRange(new ToolStripItem[] {
            this.slUser,
            this.slPermission,
            this.slMain,
            this.slUnitId,
            this.slAccess});
			this.status.Location = new System.Drawing.Point(0, 477);
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size(923, 22);
			this.status.TabIndex = 0;
			this.status.Text = "statusStrip1";
			// 
			// slUser
			// 
			this.slUser.Name = "slUser";
			this.slUser.Padding = new Padding(0, 0, 10, 0);
			this.slUser.Size = new System.Drawing.Size(41, 17);
			this.slUser.Text = "User";
			this.slUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// slPermission
			// 
			this.slPermission.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.slPermission.Name = "slPermission";
			this.slPermission.Padding = new Padding(10, 0, 0, 0);
			this.slPermission.Size = new System.Drawing.Size(73, 17);
			this.slPermission.Text = "Permission";
			this.slPermission.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// slMain
			// 
			this.slMain.Name = "slMain";
			this.slMain.Size = new System.Drawing.Size(745, 17);
			this.slMain.Spring = true;
			// 
			// slUnitId
			// 
			this.slUnitId.AutoToolTip = true;
			this.slUnitId.Name = "slUnitId";
			this.slUnitId.Padding = new Padding(0, 0, 10, 0);
			this.slUnitId.Size = new System.Drawing.Size(39, 17);
			this.slUnitId.Text = "Unit";
			// 
			// slAccess
			// 
			this.slAccess.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.slAccess.Name = "slAccess";
			this.slAccess.Padding = new Padding(10, 0, 0, 0);
			this.slAccess.Size = new System.Drawing.Size(10, 17);
			// 
			// split
			// 
			this.split.Dock = DockStyle.Fill;
			this.split.FixedPanel = FixedPanel.Panel1;
			this.split.Location = new System.Drawing.Point(0, 24);
			this.split.Name = "split";
			// 
			// split.Panel1
			// 
			this.split.Panel1.Controls.Add(this.tv_units);
			// 
			// split.Panel2
			// 
			this.split.Panel2.Controls.Add(this.tab);
			this.split.Size = new System.Drawing.Size(923, 453);
			this.split.SplitterDistance = 249;
			this.split.TabIndex = 2;
			// 
			// tv_units
			// 
			this.tv_units.Dock = DockStyle.Fill;
			this.tv_units.HideSelection = false;
			this.tv_units.Location = new System.Drawing.Point(0, 0);
			this.tv_units.Name = "tv_units";
			this.tv_units.Size = new System.Drawing.Size(249, 453);
			this.tv_units.TabIndex = 0;
			this.tv_units.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(this.tv_units_NodeMouseDoubleClick);
			this.tv_units.AfterSelect += new TreeViewEventHandler(this.tv_units_AfterSelect);
			// 
			// tab
			// 
			this.tab.Controls.Add(this.tpDnTm);
			this.tab.Controls.Add(this.tpGlobal);
			this.tab.Controls.Add(this.tpLocal);
			this.tab.Controls.Add(this.tpLang);
			this.tab.Controls.Add(this.tpTargets);
			this.tab.Dock = DockStyle.Fill;
			this.tab.Location = new System.Drawing.Point(0, 0);
			this.tab.Name = "tab";
			this.tab.Padding = new System.Drawing.Point(20, 3);
			this.tab.SelectedIndex = 0;
			this.tab.Size = new System.Drawing.Size(670, 453);
			this.tab.TabIndex = 0;
			this.tab.SelectedIndexChanged += new System.EventHandler(this.tab_SelectedIndexChanged);
			// 
			// tpDnTm
			// 
			this.tpDnTm.Controls.Add(this.tv_kpi);
			this.tpDnTm.Location = new System.Drawing.Point(4, 22);
			this.tpDnTm.Name = "tpDnTm";
			this.tpDnTm.Padding = new Padding(3);
			this.tpDnTm.Size = new System.Drawing.Size(662, 427);
			this.tpDnTm.TabIndex = 0;
			this.tpDnTm.Text = "Downtimes";
			this.tpDnTm.UseVisualStyleBackColor = true;
			// 
			// tv_kpi
			// 
			this.tv_kpi.AllowDrop = true;
			this.tv_kpi.Dock = DockStyle.Fill;
			this.tv_kpi.Location = new System.Drawing.Point(3, 3);
			this.tv_kpi.Name = "tv_kpi";
			this.tv_kpi.Size = new System.Drawing.Size(656, 421);
			this.tv_kpi.TabIndex = 0;
			this.tv_kpi.GiveFeedback += new GiveFeedbackEventHandler(this.treeKPI_GiveFeedback);
			this.tv_kpi.DoubleClick += new System.EventHandler(this.tv_kpi_DoubleClick);
			this.tv_kpi.DragDrop += new DragEventHandler(this.treeKPI_DragDrop);
			this.tv_kpi.DragEnter += new DragEventHandler(this.treeKPI_DragEnter);
			this.tv_kpi.ItemDrag += new ItemDragEventHandler(this.treeKPI_ItemDrag);
			this.tv_kpi.DragOver += new DragEventHandler(this.treeKPI_DragOver);
			// 
			// tpGlobal
			// 
			this.tpGlobal.Controls.Add(this.dgvGlobal);
			this.tpGlobal.Controls.Add(this.bnGlobal);
			this.tpGlobal.Location = new System.Drawing.Point(4, 22);
			this.tpGlobal.Name = "tpGlobal";
			this.tpGlobal.Size = new System.Drawing.Size(662, 427);
			this.tpGlobal.TabIndex = 1;
			this.tpGlobal.Text = "Global Tables";
			this.tpGlobal.UseVisualStyleBackColor = true;
			// 
			// dgvGlobal
			// 
			this.dgvGlobal.AllowUserToDeleteRows = false;
			this.dgvGlobal.BorderStyle = BorderStyle.None;
			this.dgvGlobal.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvGlobal.Dock = DockStyle.Fill;
			this.dgvGlobal.Location = new System.Drawing.Point(0, 25);
			this.dgvGlobal.Name = "dgvGlobal";
			this.dgvGlobal.RowTemplate.Height = 20;
			this.dgvGlobal.Size = new System.Drawing.Size(662, 402);
			this.dgvGlobal.TabIndex = 2;
			this.dgvGlobal.CellValueChanged += new DataGridViewCellEventHandler(this.dgv_CellValueChanged);
			// 
			// bnGlobal
			// 
			this.bnGlobal.AddNewItem = this.bnAddNewItem;
			this.bnGlobal.CausesValidation = true;
			this.bnGlobal.CountItem = this.bnCountItem;
			this.bnGlobal.DeleteItem = this.bnDeleteItem;
			this.bnGlobal.GripStyle = ToolStripGripStyle.Hidden;
			this.bnGlobal.Items.AddRange(new ToolStripItem[] {
            this.cbGlobal,
            this.bnMoveFirstItem,
            this.bnMovePreviousItem,
            this.bnSeparator,
            this.bnPositionItem,
            this.bnCountItem,
            this.bnSeparator1,
            this.bnMoveNextItem,
            this.bnMoveLastItem,
            this.bnSeparator2,
            this.bnAddNewItem,
            this.bnDeleteItem,
            this.btGlobalSave,
            this.btGlobalUndo,
            this.Redo});
			this.bnGlobal.Location = new System.Drawing.Point(0, 0);
			this.bnGlobal.MoveFirstItem = this.bnMoveFirstItem;
			this.bnGlobal.MoveLastItem = this.bnMoveLastItem;
			this.bnGlobal.MoveNextItem = this.bnMoveNextItem;
			this.bnGlobal.MovePreviousItem = this.bnMovePreviousItem;
			this.bnGlobal.Name = "bnGlobal";
			this.bnGlobal.PositionItem = this.bnPositionItem;
			this.bnGlobal.Size = new System.Drawing.Size(662, 25);
			this.bnGlobal.TabIndex = 1;
			this.bnGlobal.Text = "bindingNavigator1";
			// 
			// bnAddNewItem
			// 
			this.bnAddNewItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bnAddNewItem.Image")));
			this.bnAddNewItem.Name = "bnAddNewItem";
			this.bnAddNewItem.RightToLeftAutoMirrorImage = true;
			this.bnAddNewItem.Size = new System.Drawing.Size(23, 22);
			this.bnAddNewItem.Text = "Add new";
			this.bnAddNewItem.Visible = false;
			// 
			// bnCountItem
			// 
			this.bnCountItem.Name = "bnCountItem";
			this.bnCountItem.Size = new System.Drawing.Size(41, 22);
			this.bnCountItem.Text = "of {0}";
			this.bnCountItem.ToolTipText = "Total number of items";
			// 
			// bnDeleteItem
			// 
			this.bnDeleteItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bnDeleteItem.Image")));
			this.bnDeleteItem.Name = "bnDeleteItem";
			this.bnDeleteItem.RightToLeftAutoMirrorImage = true;
			this.bnDeleteItem.Size = new System.Drawing.Size(23, 22);
			this.bnDeleteItem.Text = "Delete";
			this.bnDeleteItem.Visible = false;
			// 
			// cbGlobal
			// 
			this.cbGlobal.Margin = new Padding(1, 0, 10, 0);
			this.cbGlobal.MaxDropDownItems = 12;
			this.cbGlobal.Name = "cbGlobal";
			this.cbGlobal.Size = new System.Drawing.Size(121, 25);
			// 
			// bnMoveFirstItem
			// 
			this.bnMoveFirstItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem.Image")));
			this.bnMoveFirstItem.Name = "bnMoveFirstItem";
			this.bnMoveFirstItem.RightToLeftAutoMirrorImage = true;
			this.bnMoveFirstItem.Size = new System.Drawing.Size(23, 22);
			this.bnMoveFirstItem.Text = "Move first";
			// 
			// bnMovePreviousItem
			// 
			this.bnMovePreviousItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem.Image")));
			this.bnMovePreviousItem.Name = "bnMovePreviousItem";
			this.bnMovePreviousItem.RightToLeftAutoMirrorImage = true;
			this.bnMovePreviousItem.Size = new System.Drawing.Size(23, 22);
			this.bnMovePreviousItem.Text = "Move previous";
			// 
			// bnSeparator
			// 
			this.bnSeparator.Name = "bnSeparator";
			this.bnSeparator.Size = new System.Drawing.Size(6, 25);
			// 
			// bnPositionItem
			// 
			this.bnPositionItem.AccessibleName = "Position";
			this.bnPositionItem.AutoSize = false;
			this.bnPositionItem.Name = "bnPositionItem";
			this.bnPositionItem.Size = new System.Drawing.Size(50, 21);
			this.bnPositionItem.Text = "0";
			this.bnPositionItem.ToolTipText = "Current position";
			// 
			// bnSeparator1
			// 
			this.bnSeparator1.Name = "bnSeparator1";
			this.bnSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bnMoveNextItem
			// 
			this.bnMoveNextItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem.Image")));
			this.bnMoveNextItem.Name = "bnMoveNextItem";
			this.bnMoveNextItem.RightToLeftAutoMirrorImage = true;
			this.bnMoveNextItem.Size = new System.Drawing.Size(23, 22);
			this.bnMoveNextItem.Text = "Move next";
			// 
			// bnMoveLastItem
			// 
			this.bnMoveLastItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem.Image")));
			this.bnMoveLastItem.Name = "bnMoveLastItem";
			this.bnMoveLastItem.RightToLeftAutoMirrorImage = true;
			this.bnMoveLastItem.Size = new System.Drawing.Size(23, 22);
			this.bnMoveLastItem.Text = "Move last";
			// 
			// bnSeparator2
			// 
			this.bnSeparator2.Name = "bnSeparator2";
			this.bnSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btGlobalSave
			// 
			this.btGlobalSave.Alignment = ToolStripItemAlignment.Right;
			this.btGlobalSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btGlobalSave.Image = global::ProductionMonitor.Properties.Resources.save;
			this.btGlobalSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btGlobalSave.Name = "btGlobalSave";
			this.btGlobalSave.Size = new System.Drawing.Size(23, 22);
			this.btGlobalSave.Text = "Save";
			this.btGlobalSave.Click += new System.EventHandler(this.btGlobalSave_Click);
			// 
			// btGlobalUndo
			// 
			this.btGlobalUndo.Alignment = ToolStripItemAlignment.Right;
			this.btGlobalUndo.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btGlobalUndo.Image = global::ProductionMonitor.Properties.Resources.undo;
			this.btGlobalUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btGlobalUndo.Name = "btGlobalUndo";
			this.btGlobalUndo.Size = new System.Drawing.Size(23, 22);
			this.btGlobalUndo.Text = "Undo";
			this.btGlobalUndo.Click += new System.EventHandler(this.btGlobalUndo_Click);
			// 
			// Redo
			// 
			this.Redo.Alignment = ToolStripItemAlignment.Right;
			this.Redo.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.Redo.Image = global::ProductionMonitor.Properties.Resources.redo;
			this.Redo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Redo.Name = "Redo";
			this.Redo.Size = new System.Drawing.Size(23, 22);
			this.Redo.Text = "Redo";
			this.Redo.Visible = false;
			// 
			// tpLocal
			// 
			this.tpLocal.Controls.Add(this.dgvLocal);
			this.tpLocal.Controls.Add(this.bnLocal);
			this.tpLocal.Location = new System.Drawing.Point(4, 22);
			this.tpLocal.Name = "tpLocal";
			this.tpLocal.Size = new System.Drawing.Size(662, 427);
			this.tpLocal.TabIndex = 3;
			this.tpLocal.Text = "Local Tables";
			this.tpLocal.UseVisualStyleBackColor = true;
			// 
			// dgvLocal
			// 
			this.dgvLocal.AllowUserToAddRows = false;
			this.dgvLocal.AllowUserToDeleteRows = false;
			this.dgvLocal.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
			this.dgvLocal.BorderStyle = BorderStyle.None;
			this.dgvLocal.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLocal.Dock = DockStyle.Fill;
			this.dgvLocal.Location = new System.Drawing.Point(0, 25);
			this.dgvLocal.Name = "dgvLocal";
			this.dgvLocal.RowTemplate.Height = 20;
			this.dgvLocal.Size = new System.Drawing.Size(662, 402);
			this.dgvLocal.TabIndex = 1;
			this.dgvLocal.CellValueChanged += new DataGridViewCellEventHandler(this.dgv_CellValueChanged);
			// 
			// bnLocal
			// 
			this.bnLocal.AddNewItem = null;
			this.bnLocal.CausesValidation = true;
			this.bnLocal.CountItem = this.bnCountItem1;
			this.bnLocal.DeleteItem = null;
			this.bnLocal.GripStyle = ToolStripGripStyle.Hidden;
			this.bnLocal.Items.AddRange(new ToolStripItem[] {
            this.cbLocal,
            this.bnMoveFirstItem1,
            this.bnMovePreviousItem1,
            this.bnSeparator3,
            this.bnPositionItem1,
            this.bnCountItem1,
            this.bnSeparator4,
            this.bnMoveNextItem1,
            this.bnMoveLastItem1,
            this.bnSeparator5,
            this.btLocalSave,
            this.btLocalUndo,
            this.toolStripButton3,
            this.btInclude,
            this.btRemove,
            this.btLocalRefresh});
			this.bnLocal.Location = new System.Drawing.Point(0, 0);
			this.bnLocal.MoveFirstItem = this.bnMoveFirstItem1;
			this.bnLocal.MoveLastItem = this.bnMoveLastItem1;
			this.bnLocal.MoveNextItem = this.bnMoveNextItem1;
			this.bnLocal.MovePreviousItem = this.bnMovePreviousItem1;
			this.bnLocal.Name = "bnLocal";
			this.bnLocal.PositionItem = this.bnPositionItem1;
			this.bnLocal.Size = new System.Drawing.Size(662, 25);
			this.bnLocal.TabIndex = 0;
			this.bnLocal.Text = "bindingNavigator2";
			// 
			// bnCountItem1
			// 
			this.bnCountItem1.Name = "bnCountItem1";
			this.bnCountItem1.Size = new System.Drawing.Size(41, 22);
			this.bnCountItem1.Text = "of {0}";
			this.bnCountItem1.ToolTipText = "Total number of items";
			// 
			// cbLocal
			// 
			this.cbLocal.Margin = new Padding(1, 0, 10, 0);
			this.cbLocal.Name = "cbLocal";
			this.cbLocal.Size = new System.Drawing.Size(121, 25);
			// 
			// bnMoveFirstItem1
			// 
			this.bnMoveFirstItem1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem1.Image")));
			this.bnMoveFirstItem1.Name = "bnMoveFirstItem1";
			this.bnMoveFirstItem1.RightToLeftAutoMirrorImage = true;
			this.bnMoveFirstItem1.Size = new System.Drawing.Size(23, 22);
			this.bnMoveFirstItem1.Text = "Move first";
			// 
			// bnMovePreviousItem1
			// 
			this.bnMovePreviousItem1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem1.Image")));
			this.bnMovePreviousItem1.Name = "bnMovePreviousItem1";
			this.bnMovePreviousItem1.RightToLeftAutoMirrorImage = true;
			this.bnMovePreviousItem1.Size = new System.Drawing.Size(23, 22);
			this.bnMovePreviousItem1.Text = "Move previous";
			// 
			// bnSeparator3
			// 
			this.bnSeparator3.Name = "bnSeparator3";
			this.bnSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// bnPositionItem1
			// 
			this.bnPositionItem1.AccessibleName = "Position";
			this.bnPositionItem1.AutoSize = false;
			this.bnPositionItem1.Name = "bnPositionItem1";
			this.bnPositionItem1.Size = new System.Drawing.Size(50, 21);
			this.bnPositionItem1.Text = "0";
			this.bnPositionItem1.ToolTipText = "Current position";
			// 
			// bnSeparator4
			// 
			this.bnSeparator4.Name = "bnSeparator4";
			this.bnSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// bnMoveNextItem1
			// 
			this.bnMoveNextItem1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem1.Image")));
			this.bnMoveNextItem1.Name = "bnMoveNextItem1";
			this.bnMoveNextItem1.RightToLeftAutoMirrorImage = true;
			this.bnMoveNextItem1.Size = new System.Drawing.Size(23, 22);
			this.bnMoveNextItem1.Text = "Move next";
			// 
			// bnMoveLastItem1
			// 
			this.bnMoveLastItem1.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem1.Image")));
			this.bnMoveLastItem1.Name = "bnMoveLastItem1";
			this.bnMoveLastItem1.RightToLeftAutoMirrorImage = true;
			this.bnMoveLastItem1.Size = new System.Drawing.Size(23, 22);
			this.bnMoveLastItem1.Text = "Move last";
			// 
			// bnSeparator5
			// 
			this.bnSeparator5.Name = "bnSeparator5";
			this.bnSeparator5.Size = new System.Drawing.Size(6, 25);
			// 
			// btLocalSave
			// 
			this.btLocalSave.Alignment = ToolStripItemAlignment.Right;
			this.btLocalSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btLocalSave.Enabled = false;
			this.btLocalSave.Image = global::ProductionMonitor.Properties.Resources.save;
			this.btLocalSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLocalSave.Name = "btLocalSave";
			this.btLocalSave.Size = new System.Drawing.Size(23, 22);
			this.btLocalSave.Text = "Save";
			this.btLocalSave.Click += new System.EventHandler(this.btLocalSave_Click);
			// 
			// btLocalUndo
			// 
			this.btLocalUndo.Alignment = ToolStripItemAlignment.Right;
			this.btLocalUndo.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btLocalUndo.Enabled = false;
			this.btLocalUndo.Image = global::ProductionMonitor.Properties.Resources.undo;
			this.btLocalUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLocalUndo.Name = "btLocalUndo";
			this.btLocalUndo.Size = new System.Drawing.Size(23, 22);
			this.btLocalUndo.Text = "Undo";
			this.btLocalUndo.Click += new System.EventHandler(this.btLocalUndo_Click);
			// 
			// toolStripButton3
			// 
			this.toolStripButton3.Alignment = ToolStripItemAlignment.Right;
			this.toolStripButton3.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripButton3.Image = global::ProductionMonitor.Properties.Resources.redo;
			this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton3.Name = "toolStripButton3";
			this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton3.Text = "Redo";
			this.toolStripButton3.Visible = false;
			// 
			// btInclude
			// 
			this.btInclude.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btInclude.Image = ((System.Drawing.Image)(resources.GetObject("btInclude.Image")));
			this.btInclude.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btInclude.Name = "btInclude";
			this.btInclude.Size = new System.Drawing.Size(23, 22);
			this.btInclude.Text = "Add selected rows to local table";
			this.btInclude.Click += new System.EventHandler(this.btInclude_Click);
			// 
			// btRemove
			// 
			this.btRemove.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btRemove.Image = ((System.Drawing.Image)(resources.GetObject("btRemove.Image")));
			this.btRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btRemove.Name = "btRemove";
			this.btRemove.Size = new System.Drawing.Size(23, 22);
			this.btRemove.Text = "Set valid property to false";
			this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
			// 
			// btLocalRefresh
			// 
			this.btLocalRefresh.Alignment = ToolStripItemAlignment.Right;
			this.btLocalRefresh.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btLocalRefresh.Image = global::ProductionMonitor.Properties.Resources.refr;
			this.btLocalRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLocalRefresh.Name = "btLocalRefresh";
			this.btLocalRefresh.Size = new System.Drawing.Size(23, 22);
			this.btLocalRefresh.Text = "Refresh";
			this.btLocalRefresh.Click += new System.EventHandler(this.btLocalRefresh_Click);
			// 
			// tpLang
			// 
			this.tpLang.Controls.Add(this.dgvLang);
			this.tpLang.Controls.Add(this.bnLang);
			this.tpLang.Location = new System.Drawing.Point(4, 22);
			this.tpLang.Name = "tpLang";
			this.tpLang.Size = new System.Drawing.Size(662, 427);
			this.tpLang.TabIndex = 2;
			this.tpLang.Text = "Translation";
			this.tpLang.UseVisualStyleBackColor = true;
			// 
			// dgvLang
			// 
			this.dgvLang.BorderStyle = BorderStyle.None;
			this.dgvLang.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvLang.Dock = DockStyle.Fill;
			this.dgvLang.Location = new System.Drawing.Point(0, 25);
			this.dgvLang.Name = "dgvLang";
			this.dgvLang.RowTemplate.Height = 20;
			this.dgvLang.Size = new System.Drawing.Size(662, 402);
			this.dgvLang.TabIndex = 3;
			this.dgvLang.CellValueChanged += new DataGridViewCellEventHandler(this.dgv_CellValueChanged);
			// 
			// bnLang
			// 
			this.bnLang.AddNewItem = null;
			this.bnLang.CausesValidation = true;
			this.bnLang.CountItem = this.bnCountItem2;
			this.bnLang.DeleteItem = null;
			this.bnLang.GripStyle = ToolStripGripStyle.Hidden;
			this.bnLang.Items.AddRange(new ToolStripItem[] {
            this.cbLang,
            this.bnMoveFirstItem2,
            this.bnMovePreviousItem2,
            this.bnSeparator6,
            this.bnPositionItem2,
            this.bnCountItem2,
            this.bnSeparator7,
            this.bnMoveNextItem2,
            this.bnMoveLastItem2,
            this.bnSeparator8,
            this.btLangSave,
            this.btLangUndo,
            this.toolStripButton6});
			this.bnLang.Location = new System.Drawing.Point(0, 0);
			this.bnLang.MoveFirstItem = this.bnMoveFirstItem2;
			this.bnLang.MoveLastItem = this.bnMoveLastItem2;
			this.bnLang.MoveNextItem = this.bnMoveNextItem2;
			this.bnLang.MovePreviousItem = this.bnMovePreviousItem2;
			this.bnLang.Name = "bnLang";
			this.bnLang.PositionItem = this.bnPositionItem2;
			this.bnLang.Size = new System.Drawing.Size(662, 25);
			this.bnLang.TabIndex = 2;
			this.bnLang.Text = "bindingNavigator3";
			// 
			// bnCountItem2
			// 
			this.bnCountItem2.Name = "bnCountItem2";
			this.bnCountItem2.Size = new System.Drawing.Size(41, 22);
			this.bnCountItem2.Text = "of {0}";
			this.bnCountItem2.ToolTipText = "Total number of items";
			// 
			// cbLang
			// 
			this.cbLang.Margin = new Padding(1, 0, 10, 0);
			this.cbLang.Name = "cbLang";
			this.cbLang.Size = new System.Drawing.Size(121, 25);
			// 
			// bnMoveFirstItem2
			// 
			this.bnMoveFirstItem2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveFirstItem2.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveFirstItem2.Image")));
			this.bnMoveFirstItem2.Name = "bnMoveFirstItem2";
			this.bnMoveFirstItem2.RightToLeftAutoMirrorImage = true;
			this.bnMoveFirstItem2.Size = new System.Drawing.Size(23, 22);
			this.bnMoveFirstItem2.Text = "Move first";
			// 
			// bnMovePreviousItem2
			// 
			this.bnMovePreviousItem2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMovePreviousItem2.Image = ((System.Drawing.Image)(resources.GetObject("bnMovePreviousItem2.Image")));
			this.bnMovePreviousItem2.Name = "bnMovePreviousItem2";
			this.bnMovePreviousItem2.RightToLeftAutoMirrorImage = true;
			this.bnMovePreviousItem2.Size = new System.Drawing.Size(23, 22);
			this.bnMovePreviousItem2.Text = "Move previous";
			// 
			// bnSeparator6
			// 
			this.bnSeparator6.Name = "bnSeparator6";
			this.bnSeparator6.Size = new System.Drawing.Size(6, 25);
			// 
			// bnPositionItem2
			// 
			this.bnPositionItem2.AccessibleName = "Position";
			this.bnPositionItem2.AutoSize = false;
			this.bnPositionItem2.Name = "bnPositionItem2";
			this.bnPositionItem2.Size = new System.Drawing.Size(50, 21);
			this.bnPositionItem2.Text = "0";
			this.bnPositionItem2.ToolTipText = "Current position";
			// 
			// bnSeparator7
			// 
			this.bnSeparator7.Name = "bnSeparator7";
			this.bnSeparator7.Size = new System.Drawing.Size(6, 25);
			// 
			// bnMoveNextItem2
			// 
			this.bnMoveNextItem2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveNextItem2.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveNextItem2.Image")));
			this.bnMoveNextItem2.Name = "bnMoveNextItem2";
			this.bnMoveNextItem2.RightToLeftAutoMirrorImage = true;
			this.bnMoveNextItem2.Size = new System.Drawing.Size(23, 22);
			this.bnMoveNextItem2.Text = "Move next";
			// 
			// bnMoveLastItem2
			// 
			this.bnMoveLastItem2.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bnMoveLastItem2.Image = ((System.Drawing.Image)(resources.GetObject("bnMoveLastItem2.Image")));
			this.bnMoveLastItem2.Name = "bnMoveLastItem2";
			this.bnMoveLastItem2.RightToLeftAutoMirrorImage = true;
			this.bnMoveLastItem2.Size = new System.Drawing.Size(23, 22);
			this.bnMoveLastItem2.Text = "Move last";
			// 
			// bnSeparator8
			// 
			this.bnSeparator8.Name = "bnSeparator8";
			this.bnSeparator8.Size = new System.Drawing.Size(6, 25);
			// 
			// btLangSave
			// 
			this.btLangSave.Alignment = ToolStripItemAlignment.Right;
			this.btLangSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btLangSave.Enabled = false;
			this.btLangSave.Image = global::ProductionMonitor.Properties.Resources.save;
			this.btLangSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLangSave.Name = "btLangSave";
			this.btLangSave.Size = new System.Drawing.Size(23, 22);
			this.btLangSave.Text = "Save";
			this.btLangSave.Click += new System.EventHandler(this.btLangSave_Click);
			// 
			// btLangUndo
			// 
			this.btLangUndo.Alignment = ToolStripItemAlignment.Right;
			this.btLangUndo.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btLangUndo.Enabled = false;
			this.btLangUndo.Image = global::ProductionMonitor.Properties.Resources.undo;
			this.btLangUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btLangUndo.Name = "btLangUndo";
			this.btLangUndo.Size = new System.Drawing.Size(23, 22);
			this.btLangUndo.Text = "Undo";
			this.btLangUndo.Click += new System.EventHandler(this.btLangUndo_Click);
			// 
			// toolStripButton6
			// 
			this.toolStripButton6.Alignment = ToolStripItemAlignment.Right;
			this.toolStripButton6.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.toolStripButton6.Image = global::ProductionMonitor.Properties.Resources.redo;
			this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton6.Name = "toolStripButton6";
			this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
			this.toolStripButton6.Text = "Redo";
			this.toolStripButton6.Visible = false;
			// 
			// tpTargets
			// 
			this.tpTargets.Controls.Add(this.dgvTargets);
			this.tpTargets.Controls.Add(this.bnTargets);
			this.tpTargets.Location = new System.Drawing.Point(4, 22);
			this.tpTargets.Name = "tpTargets";
			this.tpTargets.Size = new System.Drawing.Size(662, 427);
			this.tpTargets.TabIndex = 4;
			this.tpTargets.Text = "Targets";
			this.tpTargets.UseVisualStyleBackColor = true;
			// 
			// dgvTargets
			// 
			this.dgvTargets.AllowUserToDeleteRows = false;
			this.dgvTargets.BorderStyle = BorderStyle.None;
			this.dgvTargets.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTargets.Dock = DockStyle.Fill;
			this.dgvTargets.Location = new System.Drawing.Point(0, 25);
			this.dgvTargets.Name = "dgvTargets";
			this.dgvTargets.Size = new System.Drawing.Size(662, 402);
			this.dgvTargets.TabIndex = 1;
			this.dgvTargets.CellValueChanged += new DataGridViewCellEventHandler(this.dgv_CellValueChanged);
			// 
			// bnTargets
			// 
			this.bnTargets.AddNewItem = this.bindingNavigatorAddNewItem;
			this.bnTargets.CausesValidation = true;
			this.bnTargets.CountItem = this.bindingNavigatorCountItem;
			this.bnTargets.DeleteItem = this.bindingNavigatorDeleteItem;
			this.bnTargets.Items.AddRange(new ToolStripItem[] {
            this.toolStripComboBox1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.btTargetsSave,
            this.btTargetsUndo});
			this.bnTargets.Location = new System.Drawing.Point(0, 0);
			this.bnTargets.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
			this.bnTargets.MoveLastItem = this.bindingNavigatorMoveLastItem;
			this.bnTargets.MoveNextItem = this.bindingNavigatorMoveNextItem;
			this.bnTargets.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
			this.bnTargets.Name = "bnTargets";
			this.bnTargets.PositionItem = this.bindingNavigatorPositionItem;
			this.bnTargets.Size = new System.Drawing.Size(662, 25);
			this.bnTargets.TabIndex = 0;
			this.bnTargets.Text = "bindingNavigator1";
			// 
			// bindingNavigatorAddNewItem
			// 
			this.bindingNavigatorAddNewItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
			this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
			this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorAddNewItem.Text = "Add new";
			this.bindingNavigatorAddNewItem.Visible = false;
			// 
			// bindingNavigatorCountItem
			// 
			this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
			this.bindingNavigatorCountItem.Size = new System.Drawing.Size(41, 22);
			this.bindingNavigatorCountItem.Text = "of {0}";
			this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
			// 
			// bindingNavigatorDeleteItem
			// 
			this.bindingNavigatorDeleteItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
			this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
			this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorDeleteItem.Text = "Delete";
			this.bindingNavigatorDeleteItem.Visible = false;
			// 
			// toolStripComboBox1
			// 
			this.toolStripComboBox1.Name = "toolStripComboBox1";
			this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
			this.toolStripComboBox1.Visible = false;
			// 
			// bindingNavigatorMoveFirstItem
			// 
			this.bindingNavigatorMoveFirstItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
			this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
			this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveFirstItem.Text = "Move first";
			// 
			// bindingNavigatorMovePreviousItem
			// 
			this.bindingNavigatorMovePreviousItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
			this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
			this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMovePreviousItem.Text = "Move previous";
			// 
			// bindingNavigatorSeparator
			// 
			this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
			this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorPositionItem
			// 
			this.bindingNavigatorPositionItem.AccessibleName = "Position";
			this.bindingNavigatorPositionItem.AutoSize = false;
			this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
			this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
			this.bindingNavigatorPositionItem.Text = "0";
			this.bindingNavigatorPositionItem.ToolTipText = "Current position";
			// 
			// bindingNavigatorSeparator1
			// 
			this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
			this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// bindingNavigatorMoveNextItem
			// 
			this.bindingNavigatorMoveNextItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
			this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
			this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveNextItem.Text = "Move next";
			// 
			// bindingNavigatorMoveLastItem
			// 
			this.bindingNavigatorMoveLastItem.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
			this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
			this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
			this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
			this.bindingNavigatorMoveLastItem.Text = "Move last";
			// 
			// bindingNavigatorSeparator2
			// 
			this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
			this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
			// 
			// btTargetsSave
			// 
			this.btTargetsSave.Alignment = ToolStripItemAlignment.Right;
			this.btTargetsSave.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btTargetsSave.Enabled = false;
			this.btTargetsSave.Image = global::ProductionMonitor.Properties.Resources.save;
			this.btTargetsSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btTargetsSave.Name = "btTargetsSave";
			this.btTargetsSave.Size = new System.Drawing.Size(23, 22);
			this.btTargetsSave.Text = "Save";
			this.btTargetsSave.Click += new System.EventHandler(this.btTargetsSave_Click);
			// 
			// btTargetsUndo
			// 
			this.btTargetsUndo.Alignment = ToolStripItemAlignment.Right;
			this.btTargetsUndo.DisplayStyle = ToolStripItemDisplayStyle.Image;
			this.btTargetsUndo.Enabled = false;
			this.btTargetsUndo.Image = global::ProductionMonitor.Properties.Resources.undo;
			this.btTargetsUndo.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btTargetsUndo.Name = "btTargetsUndo";
			this.btTargetsUndo.Size = new System.Drawing.Size(23, 22);
			this.btTargetsUndo.Text = "Undo";
			this.btTargetsUndo.Click += new System.EventHandler(this.btTargetsUndo_Click);
			// 
			// imageListDrag
			// 
			this.imageListDrag.ColorDepth = ColorDepth.Depth32Bit;
			this.imageListDrag.ImageSize = new System.Drawing.Size(16, 16);
			this.imageListDrag.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new ToolStripItem[] {
            this.toolStripMenuItem1});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(923, 24);
			this.menuStrip1.TabIndex = 3;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.DropDownItems.AddRange(new ToolStripItem[] {
            this.mnuRefresh,
            this.toolStripMenuItem3,
            this.mnuToExcel,
            this.mnuFromExcel,
            this.toolStripMenuItem2,
            this.mnuExit});
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(36, 20);
			this.toolStripMenuItem1.Text = "File";
			// 
			// mnuRefresh
			// 
			this.mnuRefresh.Name = "mnuRefresh";
			this.mnuRefresh.Size = new System.Drawing.Size(208, 22);
			this.mnuRefresh.Text = "Refresh";
			this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(205, 6);
			// 
			// mnuToExcel
			// 
			this.mnuToExcel.Name = "mnuToExcel";
			this.mnuToExcel.Size = new System.Drawing.Size(208, 22);
			this.mnuToExcel.Text = "Downtimes to Excel";
			this.mnuToExcel.Click += new System.EventHandler(this.mnuToExcel_Click);
			// 
			// mnuFromExcel
			// 
			this.mnuFromExcel.Name = "mnuFromExcel";
			this.mnuFromExcel.Size = new System.Drawing.Size(208, 22);
			this.mnuFromExcel.Text = "Downtimes from Excel";
			this.mnuFromExcel.Click += new System.EventHandler(this.mnuFromExcel_Click);
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(205, 6);
			// 
			// mnuExit
			// 
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Size = new System.Drawing.Size(208, 22);
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmMaster
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(923, 499);
			this.Controls.Add(this.split);
			this.Controls.Add(this.status);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "frmMaster";
			this.StartPosition = FormStartPosition.CenterParent;
			this.Text = "Line Configuration";
			this.Shown += new System.EventHandler(this.frmMaster_Shown);
			this.KeyDown += new KeyEventHandler(this.frmMaster_KeyDown);
			this.status.ResumeLayout(false);
			this.status.PerformLayout();
			this.split.Panel1.ResumeLayout(false);
			this.split.Panel2.ResumeLayout(false);
			this.split.ResumeLayout(false);
			this.tab.ResumeLayout(false);
			this.tpDnTm.ResumeLayout(false);
			this.tpGlobal.ResumeLayout(false);
			this.tpGlobal.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvGlobal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bnGlobal)).EndInit();
			this.bnGlobal.ResumeLayout(false);
			this.bnGlobal.PerformLayout();
			this.tpLocal.ResumeLayout(false);
			this.tpLocal.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLocal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bnLocal)).EndInit();
			this.bnLocal.ResumeLayout(false);
			this.bnLocal.PerformLayout();
			this.tpLang.ResumeLayout(false);
			this.tpLang.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvLang)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bnLang)).EndInit();
			this.bnLang.ResumeLayout(false);
			this.bnLang.PerformLayout();
			this.tpTargets.ResumeLayout(false);
			this.tpTargets.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTargets)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bnTargets)).EndInit();
			this.bnTargets.ResumeLayout(false);
			this.bnTargets.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}


		#endregion

		private StatusStrip status;
		private SplitContainer split;
		private TreeView tv_units;
		private TabControl tab;
		private TabPage tpDnTm;
		private TabPage tpGlobal;
		private TabPage tpLang;
		private TreeView tv_kpi;
		private ImageList imageListDrag;
		private TabPage tpLocal;
		private BindingNavigator bnGlobal;
		private ToolStripButton bnAddNewItem;
		private ToolStripLabel bnCountItem;
		private ToolStripButton bnDeleteItem;
		private ToolStripButton bnMoveFirstItem;
		private ToolStripButton bnMovePreviousItem;
		private ToolStripSeparator bnSeparator;
		private ToolStripTextBox bnPositionItem;
		private ToolStripSeparator bnSeparator1;
		private ToolStripButton bnMoveNextItem;
		private ToolStripButton bnMoveLastItem;
		private ToolStripSeparator bnSeparator2;
		private DataGridView dgvGlobal;
		private ToolStripButton btGlobalSave;
		private ToolStripButton btGlobalUndo;
		private ToolStripButton Redo;
		private ToolStripComboBox cbGlobal;
		private DataGridView dgvLocal;
		private BindingNavigator bnLocal;
		private ToolStripLabel bnCountItem1;
		private ToolStripButton bnMoveFirstItem1;
		private ToolStripButton bnMovePreviousItem1;
		private ToolStripSeparator bnSeparator3;
		private ToolStripTextBox bnPositionItem1;
		private ToolStripSeparator bnSeparator4;
		private ToolStripButton bnMoveNextItem1;
		private ToolStripButton bnMoveLastItem1;
		private ToolStripSeparator bnSeparator5;
		private ToolStripButton btLocalSave;
		private ToolStripButton btLocalUndo;
		private ToolStripButton toolStripButton3;
		private DataGridView dgvLang;
		private BindingNavigator bnLang;
		private ToolStripLabel bnCountItem2;
		private ToolStripButton bnMoveFirstItem2;
		private ToolStripButton bnMovePreviousItem2;
		private ToolStripSeparator bnSeparator6;
		private ToolStripTextBox bnPositionItem2;
		private ToolStripSeparator bnSeparator7;
		private ToolStripButton bnMoveNextItem2;
		private ToolStripButton bnMoveLastItem2;
		private ToolStripSeparator bnSeparator8;
		private ToolStripButton toolStripButton6;
		private MenuStrip menuStrip1;
		private ToolStripMenuItem toolStripMenuItem1;
		private ToolStripMenuItem mnuRefresh;
		private ToolStripSeparator toolStripMenuItem2;
		private ToolStripMenuItem mnuExit;
		private ToolStripComboBox cbLocal;
		private ToolStripComboBox cbLang;
		private ToolStripButton btInclude;
		private ToolStripButton btRemove;
		private ToolStripButton btLocalRefresh;
		private ToolStripStatusLabel slAccess;
		private ToolStripStatusLabel slMain;
		private ToolStripStatusLabel slUser;
		private ToolStripStatusLabel slPermission;
		private ToolStripButton btLangSave;
		private ToolStripButton btLangUndo;
		private TabPage tpTargets;
		private DataGridView dgvTargets;
		private BindingNavigator bnTargets;
		private ToolStripButton bindingNavigatorAddNewItem;
		private ToolStripLabel bindingNavigatorCountItem;
		private ToolStripButton bindingNavigatorDeleteItem;
		private ToolStripComboBox toolStripComboBox1;
		private ToolStripButton bindingNavigatorMoveFirstItem;
		private ToolStripButton bindingNavigatorMovePreviousItem;
		private ToolStripSeparator bindingNavigatorSeparator;
		private ToolStripTextBox bindingNavigatorPositionItem;
		private ToolStripSeparator bindingNavigatorSeparator1;
		private ToolStripButton bindingNavigatorMoveNextItem;
		private ToolStripButton bindingNavigatorMoveLastItem;
		private ToolStripSeparator bindingNavigatorSeparator2;
		private ToolStripButton btTargetsSave;
		private ToolStripButton btTargetsUndo;
		private ToolStripStatusLabel slUnitId;
		private ToolStripMenuItem mnuFromExcel;
		private ToolStripMenuItem mnuToExcel;
		private ToolStripSeparator toolStripMenuItem3;
	}
}