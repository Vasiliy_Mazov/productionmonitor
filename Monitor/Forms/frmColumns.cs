﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ProductionMonitor
{
	public partial class frmColumns : ShadowForm
	{
		private DataTable _dtbCols, _dtbSourse;
		private DataView _dv;

		public frmColumns(DataTable dtb, DataTable dtbsourse)
		{
			InitializeComponent();
			_dtbCols = dtb;
			this._dtbSourse = dtbsourse;
		}

		private void dgv_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
		{
			DataGridView dgv = sender as DataGridView;
			CurrencyManager cm = (CurrencyManager)BindingContext[dgv.DataSource, dgv.DataMember];
			DataRowView drv = cm.List[e.RowIndex] as DataRowView;
			string colnane = dgv.Columns[e.ColumnIndex].Name;
			if (drv == null)
				return;

			if (colnane == "Visible") {
				int val = C.GetInt(drv["Weight"]);
				if (drv.Row.Table.TableName == "narrow")
					e.Value = val == 0;
				else
					e.Value = val == 0 || val == 1;
			} else if (colnane == "Dimension") {
				string id = drv["Name"].ToString();
				if (MU.Kpi.Measure.ContainsKey(id)) {
					switch (MU.Kpi.Measure[id]) {
						case 1: e.Value = Properties.Resources.strTime; break;
						case 2: e.Value = Properties.Resources.strPercents; break;
						case 3: e.Value = Properties.Resources.strUnits; break;
						case 4: e.Value = Properties.Resources.strVolume; break;
						default: e.Value = ""; break;
					}
				}
			} //else if (colnane == "Target") {

			//}
		}

		private void dgv_CellValuePushed(object sender, DataGridViewCellValueEventArgs e)
		{
			if (dgv.Columns[e.ColumnIndex].Name == "Visible") {
				DataRowView dr = dgv.Rows[e.RowIndex].DataBoundItem as DataRowView;
				if (e.Value == null) e.Value = 0;
				bool visible = (bool)e.Value;
				dr["Weight"] = visible ? 0 : 2;
			}
		}

		private void frmColumns_Load(object sender, EventArgs e)
		{
			Init();

			btCancel.Text = Properties.Resources.btCancel;
			btOk.Text = Properties.Resources.btApply;
			Text = Properties.Resources.grColsFormCaption;
			//Width = 800;
			//Height = 600;
		}

		private void Init()
		{
			// настраиваем таблицу
			DataColumn dc = _dtbCols.Columns.Add("Valid", typeof(bool));
			dc.DefaultValue = false;
			_dv = _dtbCols.DefaultView;
			_dv.Sort = "SortOrder";
			_dv.ApplyDefaultSort = true;
			_dv.RowFilter = "Weight>=0 AND Valid=1";

			// настраиваем грид
			dgv.VirtualMode = true;
			dgv.AutoGenerateColumns = false;
			dgv.AllowUserToAddRows = false;
			dgv.DataSource = _dv;
			dgv.CellValueNeeded += dgv_CellValueNeeded;
			dgv.CellValuePushed += dgv_CellValuePushed;

			// создаем колонки в гриде
			//dgv.Columns.Add(Utils.AddColumnToGrid("Name", "Name", true, 70, false, ""));
			//dgv.Columns.Add(Utils.AddColumnToGrid("Weight", "Weight", true, 70, false, ""));
			dgv.Columns.Add(Utils.AddColumnToGrid("FullCaption", Properties.Resources.grColsFullCaption, true, 70, false, ""));
			DataGridViewCheckBoxColumn chcol = new DataGridViewCheckBoxColumn(false) { Name = "Visible", ReadOnly = false };
			dgv.Columns.Add(chcol);
			dgv.Columns.Add(Utils.AddColumnToGrid("", "Dimension", Properties.Resources.strDimension, true, 70, false, ""));
			dgv.Columns.Add(Utils.AddColumnToGrid("Caption", Properties.Resources.grColsOrgCaption, true, 70, false, ""));
			dgv.Columns.Add(Utils.AddColumnToGrid("CustomCaption", Properties.Resources.grColsCustCaption, false, 70, false, ""));
			//dgv.Columns.Add(Utils.AddColumnToGrid("CustomCaption", Properties.Resources.grColsCustCaption, false, 70, false, ""));

			if (_dv.Table.TableName == "narrow") {
				chcol = new DataGridViewCheckBoxColumn(false) { DataPropertyName = "Target", Name = "Target", ReadOnly = false };
				dgv.Columns.Add(chcol);
			}

			// заполняем таблицу
			DataRow[] DR;
			string name, shortname;
			foreach (DataRow dr in _dtbCols.Rows) {
				name = dr["Name"].ToString();
				shortname = name;
				if (name.Contains("List"))
					shortname = name.Replace("List", "");
				//if (name.Contains("Name"))
				//   shortname = name.Replace("Name", "");
				if (dr["FullCaption"] == DBNull.Value)
					dr["FullCaption"] = shortname;
				if (dr["Caption"] == DBNull.Value || dr["Caption"].ToString() == "")
					dr["Caption"] = shortname;
				switch (name.ToLower()) {
					case "linename":
					case "id": dr["weight"] = -1; break;
				}
				if (_dtbSourse.Columns.Contains(name)) {
					DR = _dtbCols.Select(String.Format("Name='{0}'", name));
					if (DR.Length > 0) DR[0]["Valid"] = true;
				}
			}
			_dtbCols.AcceptChanges();
			dgv.AutoResizeColumns();
		}

		private void frmColumns_FormClosing(object sender, FormClosingEventArgs e)
		{
			//todo: по непонятным причинам при закрытии через крестик на заголовке окна возникает null reference ошибка
			try {
				if (_dtbCols != null && _dtbCols.Columns.Contains("Valid"))
					_dtbCols.Columns.Remove("Valid");
			} catch { }
		}

		private void btDefault_Click(object sender, EventArgs e)
		{
			string name;
			foreach (DataRow dr in _dtbCols.Rows) {
				name = dr["Name"].ToString();
				dr["CustomCaption"] = dr["Caption"];
				if (MU.Kpi.ViewWeight.ContainsKey(name))
					dr["Weight"] = MU.Kpi.ViewWeight[name];
			}
			dgv.InvalidateColumn(dgv.Columns["Visible"].DisplayIndex);
		}
	}
}