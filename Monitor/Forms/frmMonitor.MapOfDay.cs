﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using C1.Win.C1Chart;
using Res = ProductionMonitor.Properties.Resources;

namespace ProductionMonitor
{
	public sealed partial class frmMonitor
	{
        private bool is_mouse_move_process = false;
		private SelectedZone _sz;
		// нативное масштабирование работает очень неточно > отслеживаем сами:
		private double _dMouseDownX = 0;
		private double _dMouseUpX = 0;
        private DateTime mouse_location;

        #region | Filling chart Map   |

        public static void DecreaseLength(ref double[] arr, int delta)
        {
            double[] tmp = new double[ delta];
            Array.Copy(arr, 0, tmp, 0, tmp.Length);
            arr = tmp;
        }

        public void CreateDeviationMap(ref TimeLineChart chart, MU unit, int id, Color color_, double m_MaxY) 
        {
            DateTime _BaseDateTime;
            _BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
            int lv_length;
            double[] xr_deviation;
            double[] yr_deviation;

            DataRow[] DR_deviation = _lu_[unit.L].DataSet.Tables["Counters_deviation"].Select();

            if (DR_deviation.Length > 0) 
                lv_length = DR_deviation.Length;
            else 
                return;

            xr_deviation = new double[lv_length * 4 + 8];
            yr_deviation = new double[lv_length * 4 + 8];

            int ind = 0;
            int lv_count_dev = 0;
            int i = 0;
            foreach (DataRow dr in DR_deviation) {

                if ((int)dr["id_deviation"] != id) {
                    lv_count_dev++;
                    continue;
                }
                ind = i * 4 + 1;

                xr_deviation[ind] = 
                xr_deviation[ind + 1] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                if (DR_deviation.Length > lv_count_dev + 1) {
                    xr_deviation[ind + 2] = 
                    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                } else {
                    xr_deviation[ind + 2] = 
                    xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
                }

                yr_deviation[ind] = 0;
                yr_deviation[ind + 1] = 
                yr_deviation[ind + 2] = m_MaxY;
                yr_deviation[ind + 3] = 0;

                lv_count_dev++;
                i++;
            }

            xr_deviation[0] = 0;				// поднимаем начало
            yr_deviation[0] = yr_deviation[1];

            var ChartDat = new ChartDataSeries();
            // ChartDat.LineStyle.Pattern = LinePatternEnum.DashDotDot;// None;// Solid;
            ChartDat.LineStyle.Color = color_;// Color.DarkRed;
            ChartDat.FillStyle.HatchStyle = HatchStyleEnum.DarkUpwardDiagonal;// DarkDownwardDiagonal; // SymbolStyle.Shape = SymbolShapeEnum.VerticalLine;
            ChartDat.FillStyle.FillType = FillTypeEnum.Hatch;
            ChartDat.LineStyle.Pattern = LinePatternEnum.DashDot;// Solid;
            ChartDat.X.CopyDataIn(xr_deviation);
            ChartDat.Y.CopyDataIn(yr_deviation);
            ChartDat.Tag = "ZZ_DEVIATION";// key;
            ChartDat.LineStyle.Thickness = 1;
            chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
        }

		public void CreateBrandsMap(ref TimeLineChart chart, MU unit, int id, Color color_)  
		{
			string s1, s2;
			s2 = s1 = "";

			C1.Win.C1Chart.Label lab;
			var _BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
			int lv_length;
			double[] xr_deviation;
			double[] yr_deviation;

			DataRow[] DR_deviation = _lu_[unit.L].DataSet.Tables["Counters_brands"].Select();// ("id_deviation = " + id.ToString());

			if (DR_deviation.Length > 0)
				lv_length = DR_deviation.Length;
			else
				lv_length = 1;

			xr_deviation = new double[lv_length * 4 + 2];
			yr_deviation = new double[lv_length * 4 + 2];

			int ind = 0;
			int lv_count_dev = 0;
			int i = 0;
			foreach (DataRow dr in DR_deviation) {
				if ((int)dr["is_discharg"] != id && id >= 0) {
					lv_count_dev++;
					continue;
				}
				ind = i * 4 + 1;
				xr_deviation[ind] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
				if (id < 0) {
					if (i == 0) {
						lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
						ch0_SetLabel(lab, "", "B", 0, xr_deviation[ind], true);
					} else {
						s1 = (string)DR_deviation[lv_count_dev - 1]["brand_name"];
						s2 = (string)DR_deviation[lv_count_dev]["brand_name"];
						if (s1 != s2) {
							lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
							ch0_SetLabel(lab, "", "B", 0, xr_deviation[ind], true);
						}
					}
				}

				if (id >= 0 || (id < 0 && s1 != s2)) {
					xr_deviation[ind + 1] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					xr_deviation[ind + 2] = xr_deviation[ind + 1];
					xr_deviation[ind + 3] = xr_deviation[ind];
					if (DR_deviation.Length > lv_count_dev + 1 && id >= 0) {
						xr_deviation[ind + 2] = 
						xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev + 1]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					} else {
						xr_deviation[ind + 2] = 
						xr_deviation[ind + 3] = (double)((DateTime)DR_deviation[lv_count_dev]["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					}

					yr_deviation[ind] = 0;
					yr_deviation[ind + 1] = 
					yr_deviation[ind + 2] = 800;
					if (id >= 0) {
						yr_deviation[ind + 1] = 
						yr_deviation[ind + 2] = 8000;
					}
					yr_deviation[ind + 3] = 0;
				}
				lv_count_dev++;
				i++;
			}

			xr_deviation[0] = xr_deviation[1];              // поднимаем начало
			yr_deviation[0] = yr_deviation[1];

			xr_deviation[ind + 4] = xr_deviation[ind + 3];  // опускаем конец
			yr_deviation[ind + 4] = 0;

			var ChartDat = new ChartDataSeries();
			// ChartDat.LineStyle.Pattern = LinePatternEnum.None;// DashDotDot;// None;// Solid;
			ChartDat.LineStyle.Color = color_;// Color.DarkRed;
			ChartDat.SymbolStyle.Shape = SymbolShapeEnum.None;// VerticalLine;
			ChartDat.LineStyle.Pattern = LinePatternEnum.Solid;
			ChartDat.X.CopyDataIn(xr_deviation);
			ChartDat.Y.CopyDataIn(yr_deviation);
			ChartDat.Tag = "ZZ_BRANDS";// key;

			if (id < 0) {
				ChartDat.LineStyle.Thickness = 0; // 1;
				chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);
			} else {
				ChartDat.LineStyle.Thickness = 0; // 3;
				chart.ChartGroups[1].ChartData.SeriesList.Add(ChartDat);
			}
		}

		private void BuildChartMapCounter(MU unit, bool reload)
		{
			var sw = Stopwatch.StartNew();

			var chart = _ch_[unit.L];
			if (reload) {
				chart.Header.Text = unit.GetLongUnitName_counters2(true, " ", unit.Parent_Counters);
				chart.Init(unit.StartCorrected, unit);
			}

			int i = 0;
			C1.Win.C1Chart.Label lab;
			double maxspeed = 100;
			ChartDataSeries ChartDat;
			double speedtreshold = _lu_[chart.L].LowSpeedTreshold * (TmStyle == TimeStyle.MMM ? 1 : 60);

			chart.BeginUpdate();
			chart.UseGrayscale = false;

			chart.ChartGroups[0].ChartData.SeriesList.Clear();
			chart.ChartGroups[1].ChartData.SeriesList.Clear();
			chart.ChartLabels.LabelsCollection.Clear();

			//chart.Header.Style.Border.Color = SystemColors.ControlDark;
			//chart.Header.Style.BackColor = Color.White;
			//chart.Header.Style.ForeColor = SystemColors.ControlText;
			//chart.Header.Style.Border.Thickness = 2;
			//chart.Header.Style.Border.Rounding.All = 0;
			//chart.Header.Style.Opaque = true;

			chart.Header.Style.Border.Color = StyleColor;
			chart.Header.Style.BackColor = StyleColor;
			chart.Header.Style.ForeColor = Color.White;
			chart.Header.Style.Border.Thickness = 2;
			chart.Header.Style.Border.Rounding.All = 0;
			chart.Header.Style.Font = _fontBold;

			//if (unit.DataSet.Tables["Gaps"].DefaultView.Count > 0)
			if ((unit.Error == "") && (unit.DataSet.Tables["Counters_value"].DefaultView.Count > 0)) { //(1 == 1)

				chart.Enabled = true;

				double[] xr;
				double[] yr;
				DataRow[] DR = _lu_[unit.L].DataSet.Tables["Counters_value"].Select();
				int iii = 0;

				xr = new double[DR.Length];
				yr = new double[DR.Length];
				int lv_length_DR;
				lv_length_DR = DR.Length;
				double dr_befo = 0;
				double dr_befo_d = 0;
				bool lv_is_add = false;
				int lv_count_empty = 0;
				int lv_count_foreach = 0;

				double m_MaxY = chart.MaxY;
				double m_MinY = 0;
				DateTime _BaseDateTime;
				_BaseDateTime = new DateTime(2000, 1, 1).AddDays(-1);
				foreach (DataRow dr in DR) {


					if ((iii != 0) && (lv_count_foreach < lv_length_DR - 2)) {
						if (dr_befo != (double)dr["value"]) //|| yr[iii] != yr[iii+1])
						{
							if (lv_count_empty > 0 && dr_befo_d > 0) {
								xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D; //dr_befo_d;
								yr[iii] = dr_befo;
								iii++;
								lv_count_empty = 0;
							}

							xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
							yr[iii] = (double)dr["value"];// rnd.Next(100);   
							lv_is_add = true;

						} else {
							lv_count_empty++;
						}

					} else {
						xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
						yr[iii] = (double)dr["value"];
						lv_is_add = true;
					}

					if (m_MaxY < yr[iii])
						m_MaxY = (int)(yr[iii]);
					if (m_MinY > yr[iii])
						m_MinY = (int)(yr[iii]);
					if (lv_is_add) {
						iii++;
						lv_is_add = false;
					}

					lv_count_foreach++;
					dr_befo = (double)dr["value"];
					dr_befo_d = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
				}

				if ((iii - 1) < lv_length_DR && lv_length_DR > 10) {
					DecreaseLength(ref yr, iii - 1);
					DecreaseLength(ref xr, iii - 1);
				}

				double[] xr_top;
				double[] yr_top;

				double[] xr_bottom;
				double[] yr_bottom;

				DataRow[] DR_lim = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select();

				int lv_length;
				if (DR_lim.Length > 0) lv_length = DR_lim.Length;
				else
					lv_length = 1;

				xr_bottom = new double[lv_length * 2];
				yr_bottom = new double[lv_length * 2];
				xr_top = new double[lv_length * 2];
				yr_top = new double[lv_length * 2];

				int ind = 0;
				foreach (DataRow dr in DR_lim) {
					xr_bottom[ind] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					xr_bottom[ind + 1] = (double)((DateTime)dr["dt_to"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					yr_bottom[ind] = (float)dr["LL_value"];
					yr_bottom[ind + 1] = (float)dr["LL_value"];

					xr_top[ind] = (double)((DateTime)dr["dt_from"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					xr_top[ind + 1] = (double)((DateTime)dr["dt_to"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
					yr_top[ind] = (float)dr["HH_value"];
					yr_top[ind + 1] = (float)dr["HH_value"];

					if (m_MaxY < yr_top[ind])
						m_MaxY = (float)dr["HH_value"];
					if (m_MinY > yr_bottom[ind])
						m_MinY = (float)dr["LL_value"];

					ind = ind + 2;
				}

				ChartDat = new ChartDataSeries();
				// ChartDat.LineStyle.  Pattern = LinePatternEnum.DashDotDot;// None;// Solid;
				ChartDat.LineStyle.Color = Color.DarkRed;

				DataRow[] DR1 = _lu_[unit.L].DataSet.Tables["Counters"].Select();
				foreach (DataRow dr in DR1) {
					ChartDat.LineStyle.Color = Color.FromArgb((int)dr["color"]); // (Color)dr["color"];
					chart.Header.Text = chart.Header.Text.Trim() + "," + ((string)dr["name"]).Trim() + "," + dr["ISO"];
				}

				ChartDat.SymbolStyle.Shape = SymbolShapeEnum.VerticalLine;
				ChartDat.SymbolStyle.Size = 1;
				/// ChartDat.LineStyle.Color = MU.Kpi.Colors[key];
				ChartDat.X.CopyDataIn(xr);
				ChartDat.Y.CopyDataIn(yr);
				ChartDat.Tag = "ZZ";// key;
				ChartDat.LineStyle.Thickness = 3;
				chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat);

				xr = new double[DR.Length];
				yr = new double[DR.Length];

				double[] xr_ = new double[DR.Length];
				double[] yr_ = new double[DR.Length];

				iii = 0;
				int iiii = 0;
				try {
					//лимиты верхний и нижний
					DateTime dtime_to = new DateTime(1, 1, 1); ;
					DataRow[] DR_lim_ = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select();
					foreach (DataRow dr in DR) {

						if (dtime_to < (DateTime)dr["dt"] && (_tsLimits.CheckBox.Checked)) {
							string ss = string.Format(" dt_from <= #{0:M/d/yy H:m}# and dt_to > #{0:M/d/yy H:m}# ", (DateTime)dr["dt"]);
							DR_lim_ = _lu_[unit.L].DataSet.Tables["Counters_limits"].Select(ss);
						}
						if (DR_lim_.Length > 0) {
							DataRow dr_ = DR_lim_[0];
							dtime_to = (DateTime)dr_["dt_to"];

							if ((double)dr["value"] > (float)dr_["HH_value"]) { 
								xr[iii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
								yr[iii] = (double)dr["value"];
								iii++;
							}

							if ((double)dr["value"] < (float)dr_["LL_value"]) {
								xr_[iiii] = (double)((DateTime)dr["dt"]).Subtract(_BaseDateTime).TotalMinutes / 1440D;
								yr_[iiii] = (double)dr["value"];
								iiii++;
							}
						}
					}
				} catch (Exception) { }

				if ((iii - 1) < DR.Length && iii > 1) {
					DecreaseLength(ref xr, iii - 1);
					DecreaseLength(ref yr, iii - 1);
				}
				if ((iiii - 1) < DR.Length && iiii > 1) {
					DecreaseLength(ref xr_, iiii - 1);
					DecreaseLength(ref yr_, iiii - 1);
				}

				if (iii > 0 && _tsLimits.CheckBox.Checked) {
					var ChartDat_err = new ChartDataSeries();
					//  ChartDat_err.AddNewSeries();
					ChartDat_err.SymbolStyle.Shape = SymbolShapeEnum.Tri;// VerticalLine;
					ChartDat_err.SymbolStyle.Color = Color.Red;
					ChartDat_err.SymbolStyle.Size = 3; //5
					ChartDat_err.LineStyle.Pattern = LinePatternEnum.None;// Dot;// Solid;
					ChartDat_err.X.CopyDataIn(xr);
					ChartDat_err.Y.CopyDataIn(yr);
					ChartDat_err.Tag = "ZZ_ERROR_TOP";// key;
					chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat_err);
				}

				if (iiii > 0 && _tsLimits.CheckBox.Checked) {
					var ChartDat_err_b = new ChartDataSeries();
					//  ChartDat_err.AddNewSeries();
					ChartDat_err_b.SymbolStyle.Shape = SymbolShapeEnum.InvertedTri;// VerticalLine;
					ChartDat_err_b.SymbolStyle.Color = Color.Blue;
					ChartDat_err_b.SymbolStyle.Size = 3;  //5
					ChartDat_err_b.LineStyle.Pattern = LinePatternEnum.None;// Dot;// Solid;
					ChartDat_err_b.X.CopyDataIn(xr_);
					ChartDat_err_b.Y.CopyDataIn(yr_);
					ChartDat_err_b.Tag = "ZZ_ERROR_BUTTON";// key;
					chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat_err_b);
				}

				///--------------------------------  граничные линии
				///
				if (xr_top.Length > 0) {
					var ChartDat1 = new ChartDataSeries();
					//ChartDat1.LineStyle.Pattern = LinePatternEnum.DashDot;
					ChartDat1.SymbolStyle.Shape = SymbolShapeEnum.Star;// VerticalLine;
					ChartDat1.SymbolStyle.Color = Color.GreenYellow;
					ChartDat1.LineStyle.Color = Color.Green; // Color.Blue;
					ChartDat1.LineStyle.Pattern = LinePatternEnum.Dash; //LinePatternEnum.Dot;// Solid;
					ChartDat1.X.CopyDataIn(xr_top);
					ChartDat1.Y.CopyDataIn(yr_top);
					ChartDat1.Tag = "ZZ_TOP";// key;
					ChartDat1.LineStyle.Thickness = 2;// 3;
					chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat1);
				};
				if (xr_bottom.Length > 0) {
					var ChartDat2 = new ChartDataSeries();
					//ChartDat2.LineStyle.Pattern = LinePatternEnum.DashDot;
					ChartDat2.SymbolStyle.Shape = SymbolShapeEnum.Star;//
					ChartDat2.SymbolStyle.Color = Color.GreenYellow;

					ChartDat2.LineStyle.Color = Color.Green; // Color.Blue;
					ChartDat2.LineStyle.Pattern = LinePatternEnum.Dash;
					ChartDat2.LineStyle.Thickness = 2;// 3;
					ChartDat2.X.CopyDataIn(xr_bottom);
					ChartDat2.Y.CopyDataIn(yr_bottom);
					ChartDat2.Tag = "ZZ_BOTTOM";// key;
					chart.ChartGroups[0].ChartData.SeriesList.Add(ChartDat2);
				}


				#region | Brands |
				if (1 == 1) {
					CreateBrandsMap(ref chart, unit, -1, Color.Blue);
					CreateBrandsMap(ref chart, unit, 2, Color.LightPink);  //Red
					CreateBrandsMap(ref chart, unit, 0, Color.Gray);  //black
					CreateBrandsMap(ref chart, unit, 4, Color.LightSkyBlue);
					CreateBrandsMap(ref chart, unit, 3, Color.LightSeaGreen);
					CreateBrandsMap(ref chart, unit, 1, Color.LightYellow);
				}
				#endregion | Brands |


				#region | Deviation |

				if (1 == 1) { 
					CreateDeviationMap(ref chart, unit, 2, Color.MediumPurple, m_MaxY / 2);
					CreateDeviationMap(ref chart, unit, 0, Color.OliveDrab, m_MaxY / 2);
					CreateDeviationMap(ref chart, unit, 4, Color.MediumBlue, m_MaxY / 2);
					CreateDeviationMap(ref chart, unit, 3, Color.MediumSeaGreen, m_MaxY / 2);
					CreateDeviationMap(ref chart, unit, 1, Color.Orange, m_MaxY / 2);
				}

				#endregion | Deviation |


				#region | Date labels        |

				DateTime datelabel = MU.DtStart.Date.AddDays(1);
				i = 0;
				while (datelabel < MU.DtEnd) {
					lab = chart.ChartLabels.LabelsCollection.AddNewLabel();
					ch0_SetLabel(lab, "D_" + i.ToString(), datelabel.ToShortDateString(), maxspeed, chart.GetDouble(datelabel), true);
					datelabel = datelabel.Date.AddDays(1);
					i++;
					lab.Connected = true;
				}

				#endregion | Date labels        |


				var sz = new SelectedZone(chart, 1, chart.BaseDateTime);
				chart.SelectedZone = sz;

				m_MaxY = m_MaxY * 1.1;// 1.04; 
				chart.ChartArea.AxisY.Max = m_MaxY;
				chart.ChartArea.AxisY2.Max = m_MaxY;
				chart.MaxY = m_MaxY;
				m_MinY = m_MinY * 0.95;
				chart.ChartArea.AxisY.Min = m_MinY;
				chart.ChartArea.AxisY2.Min = m_MinY;

			} else {
				#region | there is no data   |

				// when there is no any data should to add empty
				var dummy = new ChartDataSeries();
				dummy.X.CopyDataIn(new double[1]);
				dummy.Y.CopyDataIn(new double[1]);
				chart.ChartGroups[0].ChartData.SeriesList.Add(dummy);

				chart.ChartArea.PlotArea.Boxed = true;
				chart.Enabled = false;
				chart.ChartArea.Style.ForeColor = SystemColors.ControlDark;
				lab = chart.ChartLabels.LabelsCollection.AddNewLabel();

				chart.Header.Text = unit.GetLongUnitName_counters2(true, "-", unit.Parent_Counters) + "-" + unit.LineName;

				if (_lu_[chart.L].Error != "")
					lab.Text = _lu_[chart.L].Error;                 // in case error
				else
					lab.Text = Res.mapNotAvalb;
				lab.AttachMethod = AttachMethodEnum.Coordinate;
				lab.Visible = true;
				lab.Style.Font = new Font("Verdana", 12, FontStyle.Bold, GraphicsUnit.Pixel);
				lab.Style.Autowrap = true;
				lab.Style.ForeColor = SystemColors.ControlDark;
				lab.AttachMethodData.Y = 30;
				lab.AttachMethodData.X = 40;
				chart.ChartArea.AxisY.Visible = false;

				#endregion | there is no data   |
			}
			chart.EndUpdate();

			if (_state == C.State.DataLoading || !unit.IsBinded) 
				ch0_ResetScale(chart, MU.DtStart != DateTime.Now.Date);

			chart.Visible = true;
			chart.Enabled = true;

			sw.Stop();
			Debug.WriteLine(string.Format("LoadChartMap_N() Time spent:  {0:0.000}", sw.ElapsedMilliseconds / 1000F));
		}

		#endregion | Filling chart Map   |

		private void ch0_MouseMove_Counter(object sender, MouseEventArgs e)
		{

			if (is_mouse_move_process || !_tsTips.CheckBox.Checked)
				return;
			is_mouse_move_process = true;

			TimeLineChart chart = (TimeLineChart)sender;
			if (_lu_.Count < 1 || chart.ChartGroups[0].ChartData.SeriesList.Count == 0) {
				is_mouse_move_process = false;
				return; // chart is empty
			}

			if (e.Button == MouseButtons.Right) {
				is_mouse_move_process = false;
				return; // context menu
			}

			if (e.Button == MouseButtons.Left && (Control.ModifierKeys != Keys.Alt || Control.ModifierKeys != Keys.Shift || Control.ModifierKeys != Keys.Control)) {
				is_mouse_move_process = false;
				return; // scaling
			}

			// Устанавливаем фокус на график под мышкой (с целью чтобы ранее выбранный по клику не лез сам на глаза) 

			chart.Focus();

			double yd = 0, xd = 0;
			chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref xd, ref yd);
			_sz = chart.SelectedZone;

			L = chart.L;
			if (IsEmpty_Counter()) {
				is_mouse_move_process = false;
				return;
			}

			int currline = chart.L;
			if (!_lu_[currline].IsCompleted) {
				is_mouse_move_process = false;
				return;
			}

			try {
				if (xd < 0) xd = -1;
				mouse_location = chart.GetDateTime(Math.Floor(xd * 1440) / 1440);

				if (_lu_[currline].MouseData.LastMouseLocation == mouse_location) {
					is_mouse_move_process = false;
					return;
				}

				if (!_lu_[currline].BuildMouseData_Counter(mouse_location, TmStyle == TimeStyle.MMM))
					Tt.SetToolTip();

				// to reduce CPU workload
				Thread.Sleep(2);

				ChartRegionEnum region = chart.ChartRegionFromCoord(e.X, e.Y);

				string strTip = string.Empty;


				// выделяем соответствующую зону на графике
				ChangeSelectedZone_Counter(_lu_[currline].MouseData.current_downtime);

				// при загрузке данных статусбар отображает процесс загрузки
				sbpDesc.TextAlign = ContentAlignment.MiddleLeft;
				if (_state != C.State.DataLoading) {
					sbpMins.Text = _lu_[currline].MouseData.DowntimeInterval;
					sbpBrand.Text = "";

					sbpDesc.Text = "";
					if ((!string.IsNullOrEmpty(_lu_[currline].MouseData.DowntimeFullName) && !string.IsNullOrEmpty(_lu_[currline].MouseData.DowntimeComment)))
						sbpDesc.Text = string.Format("{0} \\ {1}", (_lu_[currline].MouseData.DowntimeFullName).Trim(), (_lu_[currline].MouseData.DowntimeComment).Trim());
					if (!string.IsNullOrEmpty(_lu_[currline].MouseData.CurrentSpeed))
						sbpDesc.Text = "|" + sbpDesc.Text + _lu_[currline].MouseData.CurrentSpeed.Trim();
					sbpDesc.Text = "|" + sbpDesc.Text + _lu_[currline].MouseData.ProductionName.Trim();
					if (!string.IsNullOrEmpty(_lu_[currline].MouseData.ProductionCounterTypeName))
						sbpDesc.Text = "|" + sbpDesc.Text + "|Реж Линии-" + _lu_[currline].MouseData.ProductionCounterTypeName.Trim();
					if (!string.IsNullOrEmpty(_lu_[currline].MouseData.ProductionCounterType))
						sbpDesc.Text = "|" + sbpDesc.Text + "|Реж Прибора-" + _lu_[currline].MouseData.ProductionCounterType.Trim();
				}

				StringBuilder sb = new StringBuilder();
				if (cbShowTipsCounter.Checked && yd >= 0) {
					sb.AppendLine(_lu_[currline].MouseData.LastMouseLocation.ToString("g"));

					try {
						if (_lu_[currline].MouseData.CurrentSpeed != string.Empty)
							sb.AppendLine(_lu_[currline].MouseData.CurrentSpeed);
						if (_lu_[currline].MouseData.ProductionName != string.Empty)
							sb.AppendLine(_lu_[currline].MouseData.ProductionName.Trim());

						if (!string.IsNullOrEmpty(_lu_[currline].MouseData.ProductionCounterTypeName))
							sb.AppendLine(("Реж Линии-" + _lu_[currline].MouseData.ProductionCounterTypeName.Trim()));

						if (!string.IsNullOrEmpty(_lu_[currline].MouseData.ProductionCounterType))
							sb.AppendLine(("Реж Прибора-" + _lu_[currline].MouseData.ProductionCounterType.Trim()));
					} catch (Exception) {
						// throw;
					}

					Tt.SetToolTip(chart, sb.ToString());
				} else
					Tt.SetToolTip(chart, string.Empty);

			} catch (Exception ex) {
				is_mouse_move_process = false;
#if DEBUG
				MessageBox.Show(ex.ToString());
#endif
			}
			is_mouse_move_process = false;
		}

        #region | Chart events        |

        private void ch0_Enter_counters(object sender, EventArgs e)
        {
            if (sender is TimeLineChart chart && chart.L >= 0 && chart.Unit != null) {
                SelectTimeLine_counters(chart.L);

                if (_monitorSettings.AutoUpdate)
                    if (_tmActivity.Enabled == false)
                        _tmActivity.Start();
            }
        }

		private void SelectTimeLine_counters(int line)
		{
			var chart = _ch_[line];
			if (chart == _chs)
				return;

			//if (_chs != null) {
			//	_chs.Header.Style.Border.Color = SystemColors.ControlDark;
			//	_chs.Header.Style.ForeColor = SystemColors.ControlText;	
			//}
			_chs = chart;
			//_chs.Header.Style.Border.Color = Color.Blue;
			//_chs.Header.Style.ForeColor = Color.Blue;

			Debug.WriteLine("SelectLine_counters");
		}

        private void ch0_Transform_Counters(object sender, TransformEventArgs e)
        {
            if (ModifierKeys != Keys.Alt) 
                return;
            
            double proposed = _dMouseUpX - _dMouseDownX;

            if ( proposed < 0.00005)   /// proposed < 0.005)
                e.Cancel = true;
            else {
                e.MinX = _dMouseDownX;
                e.MaxX = _dMouseUpX;
            }
        }

		private void SetLabelsVisible(TimeLineChart chart, double val)
		{
			bool vis = val <= 3;
			foreach (C1.Win.C1Chart.Label l in chart.ChartLabels.LabelsCollection) {
				if (l.Name.StartsWith("D"))
					l.Visible = val <= 4;
				else
					l.Visible = vis;
			}
			if (val <= 4)
				chart.ChartArea.AxisX.AnnoFormatString = " H:mm";
			else if (val <= 8)
				chart.ChartArea.AxisX.AnnoFormatString = " dd/MM H:mm";
			else
				chart.ChartArea.AxisX.AnnoFormatString = " dd/MM";
		}

		private void ch0_MouseLeave(object sender, EventArgs e)
		{
            is_mouse_move_process = false;
			sbpMins.Text = string.Empty;
			sbpBrand.Text = string.Empty;
			sbpDesc.Text = string.Empty;
			Tt.SetToolTip(sender as Control, string.Empty);
			if (_sz != null) 
				_sz.Visible = false;
		}

        private void ch0_Click_Counter(object sender, EventArgs e)
        {
            L = _chs.L;
        }

        private void ch0_MouseDown_Counter(object sender, MouseEventArgs e)
        {
            // Нативное масштабирование неточно работает > вычисляем маcштаб сами
            // Проолжение процедуры в ch0_MouseUp, а завершение в ch0_Transform
            C1Chart chart = (C1Chart)sender;
            double yd = 0;
            chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseDownX, ref yd);
        }

        private void ch0_MouseUp_Counter(object sender, MouseEventArgs e)
        {
            #region | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

            // Переводим положение на экране в координаты графика
            double yd = 0;
            C1Chart chart = (C1Chart)sender;
            chart.ChartGroups[0].CoordToDataCoord(e.X, e.Y, ref _dMouseUpX, ref yd);

            // Если выделяли в обратную сторону то меняем значения местами
            double buffX;
            if (_dMouseUpX < _dMouseDownX)
            {
                buffX = _dMouseUpX;
                _dMouseUpX = _dMouseDownX;
                _dMouseDownX = buffX;
            }

            // Если вылезли за пределы графика то корректируем по границе
            AxisScrollBar xsb = chart.ChartArea.AxisX.ScrollBar;
            if (xsb.Min > _dMouseDownX) _dMouseDownX = xsb.Min;
            if (xsb.Max < _dMouseUpX) _dMouseUpX = xsb.Max;

            #endregion | Нативное масштабирование не всегда правильно работает > вычисляем маcштаб сами |

            // деление записи после перетаскивания границы мышкой
/*
            if (_ch_[L].Cursor == Cursors.VSplit && _sz.NewValueIsValid())
            {
                _ch_[L].Cursor = Cursors.Default;

                if (tabEdit.SelectedTab == tpEditProds)
                    SplitProduction(_sz.Start, _sz.Stop.AddMinutes(-1), 0, 0, false);
                else if (tabEdit.SelectedTab == tpEditShifts)
                  SplitShift(_sz.Start, _sz.Stop.AddMinutes(-1), 0, false);
                else if (tabEdit.SelectedTab == tpEditDnTms)
                {
                    if (_sz.ID == 0) return;
                    Application.DoEvents();
                    DataRow dr = C.GetRow(_lu_[L].DataSet.Tables["Stops"], "ID=" + _sz.DownID);
                    //if (dr == null) dr = C.GetRow(lu[L].DataSet.Tables["Stops"], "dt2=" + C.GetCDateString(selzone.Stop.AddMinutes(-1)));
                    if (dr == null) return;

                    // можно только увеличивать зону
                    if (_sz.Stop.Subtract(_sz.Start).TotalMinutes <= C.GetInt(dr["TF"])) return;
                    int mark1 = C.GetInt(dr["mark1"]);
                    int mark2 = C.GetInt(dr["mark2"]);
                    int downid = C.GetInt(dr["DownID"]);
                    AddDownTime(_sz.Start, _sz.Stop.AddMinutes(-1), mark1, mark2, downid, dr["Comment"].ToString());
                }
            }
                */
            //else if (_sz != null && !_sz.MouseInside(mouse_location) &&
            //           // tabEdit.SelectedTab == tpEditDnTms &&
            //            Math.Abs(_iMouseDownX - e.X) < 5)
            //{
            //    // сбрасываем ручное выделение на графике
            //    _sz.Visible = false;
            //    _sz.DownID = 0;
            //    // выделяем запись в гриде редактирования простоев
            //    if (_bsDnTms != null && _lu_[L].MouseData.DowntimeID != -1) //&& highzone != null && highzone.DowntimeOrder != 0)
            //        _bsDnTms.Position = _bsDnTms.Find("ID", _lu_[L].MouseData.DowntimeID);
            //}
        }

		private void ch0_ResetScale(TimeLineChart chart, bool b)
		{
			if (b) {
				chart.ChartArea.AxisX.ScrollBar.Scale = 1;
				//m_MaxY = m_MaxY * 1.1;// 1.04; 
				//chart.ChartArea.AxisY.Max = m_MaxY;
				//chart.ChartArea.AxisY2.Max = m_MaxY;
				//chart.MaxY = m_MaxY;
				//// chart.Mi = 0;
				//m_MinY = m_MinY * 0.95;
				//chart.ChartArea.AxisY.Min = m_MinY;
				//chart.ChartArea.AxisY2.Min = m_MinY;
				chart.ChartArea.AxisY.Max = chart.MaxY;
				chart.ChartArea.AxisY2.Max = chart.MaxY;
				chart.ChartArea.AxisY.Min = chart.ChartArea.AxisY2.Min;
				// chart.ChartArea.AxisY2.Min = 0;

			}

			double minx = chart.MinX;
			double maxx = minx + MU.Mins / 1440F;
			chart.ChartArea.AxisX.SetMinMax(minx, maxx);
			chart.ChartArea.AxisX.ScrollBar.Min = minx;
			chart.ChartArea.AxisX.ScrollBar.Max = maxx;

			chart.Cursor = Cursors.Default;
			SetLabelsVisible(chart, MU.Mins / 1440D);
		}

		private void ch0_SetLabel(C1.Win.C1Chart.Label lab, string name, string txt, double valY, double valX, bool top)
		{
			lab.Name = name;
			lab.Text = txt;
			lab.AttachMethod = AttachMethodEnum.DataCoordinate;
			lab.Visible = true;
			lab.Style.BackColor = txt == "S" ? Color.LightSteelBlue :
				name.StartsWith("D_") ? Color.White : Color.FromArgb(100, Color.White);
			lab.Style.Font = name.StartsWith("D") ? _font : _fontBold;
			lab.Offset = name.StartsWith("D_") ? 1 : 2;
			lab.Compass = top && txt != "S" ? LabelCompassEnum.NorthEast : LabelCompassEnum.SouthEast;
			lab.Style.Border.BorderStyle = txt == "S" ? BorderStyleEnum.Solid : BorderStyleEnum.None;
			lab.AttachMethodData.X = valX;
			lab.AttachMethodData.Y = valY;
			lab.AttachMethodData.GroupIndex = 0;
		}

        #endregion | Chart events        |

        private void ChangeSelectedZone_Counter(DataRow drv)
        {
            if (_sz.DownID == 0 && _sz.Visible) {
                return;
            } else if (drv != null) //&& drv.RowState != DataRowState.Deleted && drv.RowState != DataRowState.Detached)
              {
                if (_sz.DownID != C.GetInt(drv["ID"])) {
                    _sz.Start = (DateTime)drv["dt"];
                    _sz.Stop = ((DateTime)drv["dt"]).AddMinutes(1); // = dt.AddMinutes((int)drv["TF"]);
                    _sz.Visible = true;
                    _sz.DownID = C.GetInt(drv["ID"]);
                    _sz.Kind = SelectedZone.TimeZoneKind.DowntimeHighlited;

                    _sz.ID = C.GetInt(drv["ID"]);
                    _sz.Chart.Update();
                }
            } else if (_sz.Visible) {
                _sz.Visible = false;
                _sz.Chart.Update();
            }
        }

		private void ResetChartMap()
		{
			if (_sz != null) 
				_sz.Reset();
			Cursor = Cursors.Default;
        }

        private void chartmap_VisibleChangedCounter(object sender, EventArgs e)
        {
            C1Chart chart = sender as C1Chart;
            if (!Visible)
                return;
            if (_tsHighMap.CheckBox.Checked) {
                chart.Height = _chMapHeiBig;
                chart.ChartArea.AxisY.Visible = true;
                chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHeiBig);
            } else {
                chart.Height = _chMapHei;
                chart.ChartArea.AxisY.Visible = false;
                chart.ChartArea.SizeDefault = new Size(-1, _chMapAreaHei);
            }
        }
	}
}
