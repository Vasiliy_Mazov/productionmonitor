﻿using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace ProductionMonitor
{
	public class KPI
	{
		private readonly Dictionary<int, string> _idKpi = new Dictionary<int,string>();					// все пары
		private readonly Dictionary<string, int> _kpiId = new Dictionary<string,int>();					// все пары 
		private readonly Dictionary<string, int> _kpiRoot = new Dictionary<string,int>();				// корневые KPI tt0...ttN 
		private readonly Dictionary<string, int> _kpiRootAll= new Dictionary<string,int>();				// тоже что и kpiroot но плюс потери скорости и качества 
		private readonly Dictionary<string, string> _kpiExpr1 = new Dictionary<string,string>();		// формулы для родительской таблицы первого уровня
		private readonly Dictionary<string, string> _kpiExpr2 = new Dictionary<string,string>();		// формулы для родительской таблицы второго уровня
		private readonly Dictionary<string, string> _kpiNames = new Dictionary<string,string>();		// названия
		private readonly Dictionary<string, string> _kpiShortNames = new Dictionary<string,string>();// сокращенные названия
		private readonly Dictionary<string, int> _kpiMeasure = new Dictionary<string,int>();			// обозначения размерности 1-время, 2-%, 3-штуки, 4-вес(объем), 5-события
		private readonly Dictionary<string, string> _kpiGaps = new Dictionary<string,string>();		// формулы со значениями типа 'SUM(tt1)'
		private readonly Dictionary<string, string> _kpiGapsReduced = new Dictionary<string, string>();		// формулы со значениями типа 'SUM(tt1)/{0}'
		private readonly Dictionary<string, int> _kpiWeight = new Dictionary<string, int>();			// KPI отображаемые по умолчанию
		private readonly Dictionary<string, int> _kpiLosses = new Dictionary<string,int>();				// Корневые KPI отображаемые на закладке "Losses deployment"

		private readonly Dictionary<string, bool> _kpiAnlRows = new Dictionary<string,bool>();			// строки для закладки Analysis
		private readonly Dictionary<string, bool> _kpiAnlRevRows = new Dictionary<string,bool>();		// тоже но в обратном порядке
		private readonly Dictionary<string, string> _kpiAnlCols = new Dictionary<string,string>();	// колонки для закладки Analysis

		private readonly Dictionary<string, string> _kpiNoms = new Dictionary<string,string>();		// время числителя KPI не разложенное на элементарные части
		private readonly Dictionary<string, string> _kpiNomParts = new Dictionary<string, string>();	// KPI разложенный на элементарные части
		private readonly Dictionary<string, string> _kpiNomPersistParts = new Dictionary<string, string>();	// KPI разложенный на элементарные части
		private readonly Dictionary<string, string> _kpiDenoms = new Dictionary<string,string>();		// знаменатель KPI
		private readonly Dictionary<string, string> _kpiDenomParts = new Dictionary<string,string>();// знаменатель KPI разложенный на элементарные части
		private readonly Dictionary<string, string> _kpiDenomDifs = new Dictionary<string,string>();	// разница числителя и знаменателя

		private readonly List<string> _kpifulltimeparts = new List<string>();								// Список корневых причин в полном времени, получен делением строку FullTime разделителем ';'
		private readonly List<string> _needtocomment = new List<string>();									// Список корневых причин обязательных к заполнению

		private readonly Dictionary<string, Color> _kpiColors = new Dictionary<string,Color>();		// цвета для корневых KPI
		private readonly Dictionary<string, Color> _kpiColorsFont = new Dictionary<string,Color>();	// цветные шрифты для заливок
		private readonly Dictionary<string, SolidBrush> _kpiBrushs = new Dictionary<string,SolidBrush>();	// заливки для корневых KPI
		private readonly Dictionary<string, Pen> _kpiPens = new Dictionary<string,Pen>();				// кисти для корневых KPI
		private readonly Dictionary<string, DataGridViewCellStyle> _kpiGridCellStyles = new Dictionary<string, DataGridViewCellStyle>();
																																	// цветные стили для DataGridView

		public class KpiRow
		{
			public int ID { get; set; }
			public string Sign { get; set; }
			public string Name { get; set; }
			public string Short { get; set; }
			public int Root { get; set; }
			public int Losses { get; set; }
			public Color Color { get; set; }
			public Color FontColor { get; set; }
			public SolidBrush Brush { get; set; }
			public string Expr1 { get; set; }
			public string Expr2 { get; set; }
			public string Expr3 { get; set; }
			public string ExprGaps { get; set; }
			public int ShowWeight { get; set; }
			public int Measure { get; set; }
			public int AnalyseRowOrder { get; set; }
			public int AnalyseColOrder { get; set; }
		}

		private DataTable _tbKPI;
		private DataTable _tbRoot;
		private ImageList _il;
		private readonly string _mainkpi = "K0";

		public KPI(DataTable tbKPI)
		{
			try {

				_il = new ImageList();
				_tbKPI = tbKPI.Copy();

				tbKPI.DefaultView.RowFilter = "Prev is not null";
				tbKPI.DefaultView.Sort = "Prev DESC";
				_tbRoot = tbKPI.DefaultView.ToTable(true, new string[] { "KpiID", "Name", "Sign", "NameID" });
				_tbRoot.TableName = "Root";
				_tbRoot.Columns[0].ColumnName = "KpiID";
				//tbRoot.Columns[1].ColumnName = "EName";
				_tbRoot.Columns[2].ColumnName = "RD";

				string key;
				foreach (DataRow dr in tbKPI.Rows) {
					key = dr["Sign"].ToString();
					_kpiId.Add(key, (int)dr["KpiID"]);
					_idKpi.Add((int)dr["KpiID"], key);
					if (dr["Root"] != DBNull.Value && (bool)dr["Root"]) _kpiRoot.Add(key, (int)dr["KpiID"]);
					if (dr["Name"] != DBNull.Value) _kpiNames.Add(key, dr["Name"].ToString());
					if (dr["Expr1"] != DBNull.Value) _kpiExpr1.Add(key, dr["Expr1"].ToString());
					if (dr["Expr2"] != DBNull.Value) _kpiExpr2.Add(key, dr["Expr2"].ToString());
					if (dr["Short"] != DBNull.Value) _kpiShortNames.Add(key, dr["Short"].ToString());
					if (dr["Measure"] != DBNull.Value) _kpiMeasure.Add(key, (int)dr["Measure"]);
					if (dr["Nominator"] != DBNull.Value) _kpiNomParts.Add(key, dr["Nominator"].ToString());
					if (dr["Denominator"] != DBNull.Value) _kpiDenoms.Add(key, dr["Denominator"].ToString());
					if (dr["Losses"] != DBNull.Value && (int)dr["Losses"] > 0) _kpiLosses.Add(key, (int)dr["Losses"]); 
					if (dr["Color"] != DBNull.Value) {
						_kpiColors.Add(key, Color.FromArgb((int)dr["Color"]));
						_kpiBrushs.Add(key, new SolidBrush(_kpiColors[key]));
						_kpiColorsFont.Add(key, (_kpiColors[key].R + _kpiColors[key].G + _kpiColors[key].B) > 400 ? Color.Black : Color.White);
						_kpiPens.Add(key, new Pen(_kpiColors[key]));
						#region | favorite colors |
						/*
						 * -3670016		red
						 * -8388608		broun
						 * -10496		yelow
						 * -9076597		gray
						 * 
						 * -603947008	green		//
						 * -14474461	tNF		// black
						 * -25856		tSh		// -23296
						 * -3768140		tSp		// -1056987904
						 * -4684277		tQl		// -11649 (Goldenrod: 184, 134, 11:  -4684277)
						 * 
						 * //heineken:
						 * -5192482		Color.LightSteelBlue // tt1 Non-team Maintenance
						 * -4343957		Color.DarkKhaki		// tt2 NONA
						 * -6598576		-2448096					// tt4 Changeover time
						 * 225, 161, 121 //-1990279
						 * 169, 182, 157 //-5654883
						 * 102, 102, 153 //-10066279
						 */
						Color col = Color.FromArgb(102, 102, 153);
						Debug.WriteLine(col.ToArgb());
						
						
						#endregion
						DataGridViewCellStyle cs = new DataGridViewCellStyle { 
							BackColor = _kpiColors[key], 
							SelectionBackColor = _kpiColors[key], 
							ForeColor = _kpiColorsFont[key], 
							SelectionForeColor = _kpiColorsFont[key], 
							Font = SystemFonts.DefaultFont };
						_kpiGridCellStyles.Add(key, cs);
					}
					if (dr["Weight"] != DBNull.Value) {
						_kpiWeight.Add(key, (int)dr["Weight"]);
						if (_kpiWeight[key] == 100) {
							_mainkpi = key;
							_kpiWeight[key] = 0;
						}
					}
				}

				// Denominator parts
				foreach (string k in _kpiId.Keys)
					if (_kpiDenoms.ContainsKey(k) && _kpiNomParts.ContainsKey(_kpiDenoms[k]))
						_kpiDenomParts.Add(k, _kpiNomParts[_kpiDenoms[k]]);

				// Числители
				foreach (string k in _kpiDenoms.Keys) {
					if (!_kpiNomParts.ContainsKey(k))
						continue;
					string s = _kpiNomParts[k];
					foreach (string s2 in _kpiNomParts.Keys) {
						if (_kpiMeasure.ContainsKey(s2) &&
								_kpiMeasure[s2] == 1 && _kpiNomParts[s2] == s) {
							_kpiNoms.Add(k, s2);
							break;
						}
					}
				}

				// Только действительно составные числители
				foreach (string k in _kpiNomParts.Keys) {
					if (k.Contains(";"))
						_kpiNomPersistParts.Add(k, _kpiNomParts[k]);
				}

				// Разница между знаменателем и числителем
				foreach (string k in _kpiDenomParts.Keys) {
					_kpiDenomDifs.Add(k, "");
					foreach (string k2 in _kpiDenomParts[k].Split(new string[] { ";" }, StringSplitOptions.None)) {
						if (!_kpiNomParts[k].Contains(k2))
							_kpiDenomDifs[k] += ";" + k2;
					}
					if (_kpiDenomDifs[k].Length > 0)
						_kpiDenomDifs[k] = _kpiDenomDifs[k].Substring(1);
				}

				foreach (string k in _kpiExpr2.Keys)
					if (_kpiExpr2[k].Contains("child({0}).") && !_kpiExpr2[k].Contains("{1}")) {
						_kpiGaps.Add(k, _kpiExpr2[k].Replace("child({0}).", ""));
						_kpiGapsReduced.Add(k, _kpiGaps[k]);
						_kpiGapsReduced[k] += "/{0}";
					}

				DataView dv = new DataView(tbKPI, "Agenda>0", "Agenda", DataViewRowState.OriginalRows);
				foreach (DataRowView drv in dv) {
					key = drv["Sign"].ToString();
					_kpiAnlRows.Add(key, key.StartsWith("T"));
					if (_kpiColors.ContainsKey(key))
						_kpiRootAll.Add(key, _kpiId[key]);
				}

				dv = new DataView(tbKPI, "Agenda IS NOT NULL", "Agenda DESC", DataViewRowState.OriginalRows);
				foreach (DataRowView drv in dv) {
					key = drv["Sign"].ToString();
					if ((int)drv["Agenda"] > 0)
						_kpiAnlRevRows.Add(key, key.StartsWith("T"));
					else
						_kpiAnlCols.Add(key, _kpiDenoms[key]);
				}
				dv.Dispose();

				Bitmap bmp;
				_il.Images.Clear();
				foreach (string k in _kpiRoot.Keys) {
					bmp = new Bitmap(16, 16);
					for (int x = 1; x < 16; x++)
						for (int y = 1; y < 16; y++)
							bmp.SetPixel(x, y, _kpiColors[k]);
					_il.Images.Add(_kpiId[k].ToString(), bmp);
				}

				if (_kpiDenomParts.ContainsKey("K0"))
					foreach (string k in _kpiDenomParts["K0"].Split(new string[] { ";" }, StringSplitOptions.None))
						_kpifulltimeparts.Add(k);

				tbKPI.DefaultView.RowFilter = "";
				_tbKPI.DefaultView.RowFilter = "";

			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "KPI table initialization exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
				throw new Exception("KPI table initialization exception");
			}
		}
		/// <summary>
		/// Содержит без исключения ВСЕ пары ID/Sign
		/// </summary>
		public Dictionary<int, string> Sign
		{
			get { return _idKpi; }
		}
		/// <summary>
		/// Содержит без исключения ВСЕ пары Sign/ID
		/// </summary>
		public Dictionary<string, int> ID
		{
			get { return _kpiId; }
		}
		/// <summary>
		/// Contain a set of root values for downtimes that can be displayed on 'Timeline' colored zones
		/// </summary>
		public Dictionary<string, int> Roots
		{
			get { return _kpiRoot; }
		}
		/// <summary>
		/// Корневые KPI типа tt0...ttN плюс потери скорости и качества 
		/// </summary>
		public Dictionary<string, int> RootsAll
		{
			get { return _kpiRootAll; }
		}
		/// <summary>
		/// Корневые KPI отображаемые на закладке "Losses deployment"
		/// </summary>
		public Dictionary<string, int> Losses
		{
			get { return _kpiLosses; }
		}
		/// <summary>
		/// Формулы для родительской таблицы первого уровня
		/// </summary>
		public Dictionary<string, string> Expr1
		{
			get { return _kpiExpr1; }
		}
		/// <summary>
		/// Формулы для родительской таблицы второго уровня
		/// </summary>
		public Dictionary<string, string> Expr2
		{
			get { return _kpiExpr2; }
		}
		/// <summary>
		/// Названия
		/// </summary>
		public Dictionary<string, string> Names
		{
			get { return _kpiNames; }
		}
		/// <summary>
		/// Сокращеные названия
		/// </summary>
		public Dictionary<string, string> Short
		{
			get { return _kpiShortNames; }
		}
		/// <summary>
		/// 1 - время
		/// 2 - проценты
		/// 3 - штуки
		/// 4 - volume
		/// 5 - счетчик событий
		/// </summary>
		public Dictionary<string, int> Measure
		{
			get { return _kpiMeasure; }
		}
		/// <summary>
		/// Формулы сумм для таблицы Gaps, например для ключа tt1 значение будет 'sum(tt1)'
		/// </summary>
		public Dictionary<string, string> Gaps
		{
			get { return _kpiGaps; }
		}
		/// <summary>
		/// Формулы сумм для таблицы Gaps, например для ключа tt1 значение будет 'sum(tt1)'
		/// </summary>
		public Dictionary<string, string> GapsReduced
		{
			get { return _kpiGapsReduced; }
		}
		/// <summary>
		/// Порядок строк для закладки Analysis
		/// </summary>
		public Dictionary<string, bool> AnalyseRows
		{
			get { return _kpiAnlRows; }
		}
		/// <summary>
		/// Обратный порядок строк для закладки Analysis
		/// </summary>
		public Dictionary<string, bool> AnalyseRowsReverse
		{
			get { return _kpiAnlRevRows; }
		}
		/// <summary>
		/// Колонки для закладки Analysis
		/// </summary>
		public Dictionary<string, string> AnalyseColumns
		{
			get { return _kpiAnlCols; }
		}
		/// <summary>
		/// Время числителя KPI не разложенное на элементарные части
		/// </summary>
		public Dictionary<string, string> Nominators
		{
			get { return _kpiNoms; }
		}
		/// <summary>
		/// Время числителя KPI разложенное на элементарные части (RootsAll)
		/// </summary>
		public Dictionary<string, string> NomParts
		{
			get { return _kpiNomParts; }
		}
		/// <summary>
		/// Только действительно составные числители
		/// </summary>
		public Dictionary<string, string> NomPersistParts
		{
			get { return _kpiNomPersistParts; }
		}
		/// <summary>
		/// Время знаменателя KPI не разложенное на элементарные части
		/// </summary>
		public Dictionary<string, string> Denominators
		{
			get { return _kpiDenoms; }
		}
		/// <summary>
		/// Время знаменателя KPI разложенное на элементарные части (RootsAll)
		/// </summary>
		public Dictionary<string, string> DenomParts
		{
			get { return _kpiDenomParts; }
		}
		/// <summary>
		/// Разница между знаменателем и числителем выраженная в 
		/// </summary>
		public Dictionary<string, string> DenomPartsDiff
		{
			get { return _kpiDenomDifs; }
		}
		/// <summary>
		/// Список составных частей полного времени
		/// </summary>
		public List<string> FullTimeParts
		{
			get { return _kpifulltimeparts; }
		}
		/// <summary>
		/// Список корневых причин обязательных к заполнению, задается
		/// с помощью таблицы g_cfg, в строке с id=10, название параметра - NeedToComment,
		/// значение содержит перечисление с разделителем в виде ';', например: 'tt2;tt3'
		/// </summary>
		public List<string> NeedToComment
		{
			get { return _needtocomment; }
		}
		/// <summary>
		/// -1 - не показывать ни при каких условиях, т.е. это служебные колонки
		/// 0 - самые основные, только их отображать по умолчанию для grLines
		/// 1 - менее важные, их вдобавок к '0' отображать по умолчанию для grCompare
		/// 2 - второстепенные колонки, по умолчанию не показывать
		/// </summary>
		public Dictionary<string, int> ViewWeight
		{
			get { return _kpiWeight; }
		}
		/// <summary>
		/// Цвета для корневых KPI
		/// </summary>
		public Dictionary<string, Color> Colors
		{
			get { return _kpiColors; }
		}
		/// <summary>
		/// Цвета шрифтов для корневых KPI
		/// </summary>
		public Dictionary<string, Color> Colorsfont
		{
			get { return _kpiColorsFont; }
		}
		/// <summary>
		/// Цвета заливок для корневых KPI
		/// </summary>
		public Dictionary<string, SolidBrush> Brushs
		{
			get { return _kpiBrushs; }
		}
		/// <summary>
		/// Кисти для корневых KPI
		/// </summary>
		public Dictionary<string, Pen> Pens
		{
			get { return _kpiPens; }
		}
		/// <summary>
		/// Стили для таблицы редактирования простоев
		/// </summary>
		public Dictionary<string, DataGridViewCellStyle> DataGridViewCellStyle
		{
			get { return _kpiGridCellStyles; }
		}
		
		public DataTable TableKPI
		{
			get { return _tbKPI; }
		}
		public DataTable TableRoot
		{
			get { return _tbRoot; }
		}
		public ImageList ImageList
		{
			get { return _il; }
		}
		public string MainKpiKey
		{
			get { return _mainkpi; }
		}
		public void ChangeLanguage(bool eng)
		{
			string key;
			foreach (DataRow dr in _tbKPI.Rows) {
				key = dr["Sign"].ToString();
				if (!eng) {
					_kpiNames[key] = dr["Value"].ToString();
				}
				if (eng || _kpiNames[key] == "")
					_kpiNames[key] = dr["EName"].ToString();
			}
		}
		public string FindKey(string nominator, string denominator)
		{
			foreach (var v in Nominators)
				if (v.Value == nominator)
					foreach (var d in Denominators)
						if (d.Value == denominator)
							if (v.Key == d.Key)
								return v.Key;
			return string.Empty;			
		}
		public bool Included(string bigKeySet, string smallKeySet)
		{
			if (!_kpiDenomParts.ContainsKey(bigKeySet) || (!_kpiDenomParts.ContainsKey(smallKeySet) && !smallKeySet.StartsWith("t")))
				return false;
			string bigSet = _kpiDenomParts[bigKeySet];
			string[] parts1 = bigSet.Split(new string[] { ";" }, StringSplitOptions.None);
			if (smallKeySet.StartsWith("t"))
				return Array.Exists<string>(parts1, el => el == smallKeySet);
			string smallSet = _kpiDenomParts[smallKeySet];
			string[] parts2 = smallSet.Split(new string[] { ";" }, StringSplitOptions.None);
			foreach (var v in parts2)
				if (!Array.Exists<string>(parts1, el => el == v))
					return false;
			return true;
		}
		public List<string> Difs(string bigKeySet, string smallKeySet)
		{
			string set1 = _kpiNomParts[bigKeySet];
			string[] parts1 = set1.Split(new string[] { ";" }, StringSplitOptions.None);
			string set2 = _kpiNomParts[smallKeySet];
			string[] parts2 = set2.Split(new string[] { ";" }, StringSplitOptions.None);
			List<string> res = new List<string>();
			if (parts1.Length > parts2.Length)
				Array.ForEach<string>(parts1, el => { if (!Array.Exists<string>(parts2, el2 => el2 == el)) res.Add(el); });
			else if (parts1.Length < parts2.Length)
				Array.ForEach<string>(parts2, el => { if (!Array.Exists<string>(parts1, el2 => el2 == el)) res.Add(el); });
			return res;
		}

		//public List<string> GetDif(string k1, string k2)
		//{


		//}
	}
}
